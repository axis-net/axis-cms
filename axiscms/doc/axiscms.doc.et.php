<?php #2014-09-28
return array(
	'about'=>array(
		'title'=>'Mis on AXIS CMS?',
		'content'=>'<p>AXIS CMS (<b>C</b>ontent <b>M</b>anagement <b>S</b>ystem e. sisuhalduss&uuml;steem) on tarkvara, mis v&otilde;imaldab kodulehe omanikul lehe sisu kiirelt ja lihtsalt muuta ilma et oleks vaja tunda mingeid muid programme peale internetibrauseri, olla kursis keeruliste veebitehnoloogiatega v&otilde;i kasutada spetsialisti abi. AXIS CMS t&ouml;&ouml;tab koos kodulehega veebiserveris, seega ei ole vaja midagi arvutisse installeerida ning seda saab kasutada suvalisest interneti&uuml;hendusega arvutist, olgu t&ouml;&ouml;l, kodus, internetikohvikus v&otilde;i mujal. Peamisteks eeliseks teiste analoogsete toodete ees on, et AXIS CMS on v&auml;ga lihtne paigaldada ja kasutada ning selle abil valmistatud kodulehed vastavad rahvusvaheliselt tunnustatud <a href="http://www.w3.org/" target="_blank">WWW Consortium (<abbr title="World Wide Web Consortium">W3C</abbr>)</a> veebistandarditele. Toote arendamise eesm&auml;rk on luua originaalkontseptsioonil p&otilde;hinev platvorm Axis Multimedia pakutavatele veebilahendustele, kuid saadaval on ka t&auml;iesti tasuta versioon. </p>
		<p>Kasutades AXIS CMS tasuta versiooni ja m&otilde;nd tasuta kodulehe majutusteenust, nagu <a href="http://www.freehostia.com" target="_blank">www.freehostia.com</a> v&otilde;i <a href="http://web.zone.ee" target="_blank">web.zone.ee</a>, saab iga soovija luua endale lihtsa, kuid tehniliselt korrektse ja informatiivse kodulehe loetud minutitega ja t&auml;iesti tasuta. Paigaldamine ega hilisem kasutamine ei n&otilde;ua mingeid eriteadmisi &mdash; piisab kui t&auml;ita paigaldamise lehel lihtne vorm ning CMS paigaldatakse Teie kodulehele automaatselt &mdash; ja v&otilde;itegi kasutamisega alustada, ei mingit installeerimist ega muid t&uuml;likaid protseduure. </p>
		<p>AXIS CMS kujutab endast universaalset veebiplatvormi (web framework), mis on heaks vundamendiks tahes millisele PHP keeles teostatavale kodulehele v&otilde;i muule veebilahendusele,  
  CMS on &uuml;ks selle komponentidest. Platvormi tugevateks k&uuml;lgedeks on  hea laiendatavus, paindlikkus ja kiirus. S&uuml;steem on oma olemuselt &uuml;les ehitatud plugin\'ite ja moodulite p&otilde;him&otilde;ttele, lisaks on olemas muid laiendusv&otilde;imalusi. </p>
<p>M&otilde;ned olulisemad AXIS CMSi v&otilde;imalused: </p>
<ul>
  <li>Sisu on organiseeritud puukujulise men&uuml;&uuml;na &mdash; pole vaja navigeerida mingis keerulises moodulite s&uuml;steemis, kogu sisu on toimetatav samast kohast</li>
  <li>Lehek&uuml;lgede ja alammen&uuml;&uuml;de lisamise ja kustutamise v&otilde;imalus</li>
  <li>Kustutataud andmete taastamise v&otilde;imalus</li>
  <li>Lehek&uuml;lgede sisu toimetamine visuaalse HTML tekstiredaktoriga, piltide, tabelite jpm lisamise v&otilde;imalus</li>
  <li>Lihtne lisada lehek&uuml;ljele videosid, heli- ja tahes mis muid faile. V&otilde;imaldab ka v&auml;liste teenuste integreerimist, nt YouTube videote, Google Map\'i, erinevate skriptide jne lisamist. </li>
  <li>K&otilde;iki t&ouml;&ouml;riistu saab kasutada just sellises punktis, kus selleks vajadus tekib, ei ole otseseid piiranguid, kuhu midagi lisada saab. Video- ja helifaile saab lisada kogu sisu ulatuses, ei ole vaja luua eraldi pildi- v&otilde;i videogaleriisid kui selleks pole soovi. </li>
  <li>Pildifailide automaatne konverteerimine veebiformaati</li>
  <li>V&otilde;imalus piirata ligip&auml;&auml;su lehek&uuml;lgedele v&otilde;i tervetele men&uuml;&uuml;dele parooli, login\'i ja/v&otilde;i kasutajagruppidega</li>
  <li>Mitmekeelsuse tugi - v&otilde;imalus lisada kodulehele piiramatu hulk keeli</li>
  <li>Ajastatud sisu avaldamine &mdash; igale lehek&uuml;ljele v&otilde;i selle osale v&otilde;ib m&auml;&auml;rata avaldamise ja peitmise kuup&auml;eva ja kellaaja</li>
  <li>V&otilde;imalus lisada kasutajaid ja kasutajagruppe</li>
  <li>Integreeritud intraneti keskkond &mdash; AXIS CMS sisaldab moodulip&otilde;hist intraneti platvormi, mis on m&otilde;eldud ennek&otilde;ike andmebaaside administreerimiseks, kuid vastavate moodulite abil on v&otilde;imalik anda intranetile tahes mis funktsioone ja luua &uuml;ksk&otilde;ik milliseid veebip&otilde;hiseid tarkvaralahendusi. Tasuta versioon &uuml;htegi intraneti moodulit ei sisalda. </li>
  <li>Klienditoe s&uuml;steem tagasiside vormi kujul &mdash; kasutaja saab tekkinud k&uuml;simused saata otse sealt, kus need tekkisid. K&uuml;simusega koos saadetakse ka taustainfo hetke situatsiooni kohta, mis v&otilde;imaldab leida lahendusi oluliselt efektiivsemalt kui see oleks v&otilde;imalik vaid telefoni v&otilde;i e-posti teel</li>
  <li>S&uuml;steemilogi  &mdash; registreerib olulisi s&uuml;ndmusi ja teavitab neist vajadusel e-posti teel (nt p&auml;ringud valedele aadressidele, h&auml;kkerite ja viiruste r&uuml;ndekatsed ning s&uuml;steemivead). Veebilahenduse administraator ja/v&otilde;i omanik saab otse s&uuml;steemilt  koheselt teate kui midagi on juhtunud, ei pea pidevalt ise kontrollima v&otilde;i ootama kuni m&otilde;ni k&uuml;lastaja sellest teada annab</li>
  <li>V&otilde;imalus kasutada &uuml;hte sisuhalduss&uuml;steemi mitme samas serveris oleva kodulehe administreerimiseks</li>
</ul>
<p>AXIS CMSi arendamisega tegeleb <a href="http://'.AXS_DOMAIN.'" target="_blank">AXIS MULTIMEDIA O&Uuml;</a>. Enne tasuta versiooni allalaadimist lugege ka litsentsi l&otilde;iku. </p>',
		),
	'install'=>array(
		'title'=>'Tasuta kodulehe paigaldamine',
		'content'=>'<p>Kui olete juba leidnud sobiva serveri kodulehe majutamiseks, on k&otilde;ige lihtsam kasutada kodulehe paigaldamiseks j&auml;rgnevat vormi &mdash; t&auml;itke vaid vajalikud v&auml;ljad ja AXIS CMS paigaldatakse Teie kodulehele automaatselt. Vormi sisestatud kasutajatunnust ja parooli ei salvestata kuhugi ega kasutata mingil muul otstarbel. <br />
		<strong>NB!</strong> Kui soovite paigaldada olemasoleva kodulehele asemele, peab k&otilde;igepealt vana kodulehe eemaldama!</p>	
		<p>Kui Teil ei ole veel kohta oma kodulehe majutamiseks, v&otilde;ite registreeruda n&auml;iteks &uuml;he j&auml;rgneva tasuta teenuse kasutajaks: </p>
		<dl>
			<dt><a href="http://www.000webhost.com" target="_blank">www.000webhost.com</a></dt><dd>t&auml;isfunktsionaalne tasuta teenus, v&auml;lismaist p&auml;ritolu</dd>
			<dt><a href="http://web.zone.ee" target="_blank">web.zone.ee</a> tasuta veebiruum</dt><dd>eestikeelne registreerimisvorm, kuid piiratud v&otilde;imalustega teenus (keerukas lisada pilte ja muid faile)</dd>
		</dl>'."\n",
		),
	'download'=>array(
		'title'=>'Laadi alla axiscms.zip',
		'content'=>'<p>AXIS CMS zip failina: <a href="?e=download" rel="nofollow">'.AXS_DOMAIN.'/axiscms/?e=download</a>. </p>
		<p>Faile &uuml;les pannes tuleb m&otilde;nedes serverites kirjutus&otilde;igused k&auml;sitsi seada. Kirjutus&otilde;igust on vaja kaustadele "axs_site" ja "axiscms/data". chmod 777 rekursiivselt koos k&otilde;igi neis olevate failide ja alamkaustadega. </p>'."\n",
		),
	'userguide'=>array(
		'title'=>'Kasutusjuhend',
		'content'=>array(
			'admin'=>array(
				'title'=>'Sisenemine veebip&otilde;hisesse administreerimiskeskkonda',
				'content'=>'<p>Oma kodulehe sisu muutmiseks sisenege AXIS CMS keskkonda oma kodulehe kaudu. Selleks leiate alumisest servast lingi "admin", samuti v&otilde;ib sisestada otseaadressi lisades oma kodulehe addressi l&otilde;ppu &quot;/axiscms&quot; ehk siis kujul &quot;http://[teie_kodulehe_aadress]/axiscms&quot;. </p>
		<p>J&otilde;udnud AXIS CMSi avalehele, sisestage login vormi oma e-posti aadress (kasutajatunnus) ja parool. Tasuta versioonile on  pandud vaikimisi e-postiks &quot;admin&quot; ja parooliks samuti &quot;admin&quot;. Esimest korda sisse logides tuleks esimese asjana  kindlasti muuta parooli ning soovitatavalt sisestada ka muud kasutaja andmed. Seda saab teha lingi &quot;Minu profiil&quot; kaudu. </p>
		<p>CMSi peamen&uuml;&uuml;: </p>
			<dl>
				<dt>Sisuhaldus</dt><dd>
				 <ul>
				  <li>koduleh(ted)e sisu ja failidega t&ouml;&ouml;tamine</li><li>intraneti keskkond</li>
				 </ul>
				</dd>
			    <dt>Minu profiil</dt><dd>kasutaja andmete muutmine</dd>
			    <dt>S&uuml;steem</dt><dd>
				 <ul><li>kasutajakontode haldus</li><li>s&uuml;steemi logiraamat</li><li>s&uuml;steemi info</li><li>s&uuml;steemi &uuml;ldseadete muutmine</li>
				 </ul>
				</dd>
			    <dt>Abiinfo</dt><dd>abimaterjalid</dd>
			    <dt>Tagasiside</dt><dd>kui tekib k&uuml;simusi v&otilde;i avastate s&uuml;steemis vea, saab selle vormi kaudu neist teada anda klienditoe tugiisikule. </dd>
			</dl>'."\n",
				),
			'edit-content'=>array(
				'title'=>'Sisuhaldus',
				'content'=>'<h3>Keele valik</h3>
				<p>Rippmen&uuml;&uuml;st valitud keel m&auml;&auml;rab, mis keelse sisuga t&ouml;&ouml; toimub. Erinevates keeltes sisu on &uuml;ksteisest s&otilde;ltumatu. </p>
				
				<h3>Kodulehe &uuml;ldseaded</h3>
				<dl>
				 <dt>Kodulehe &uuml;ldpealkiri</dt><dd>Pealkiri brauseri tiitliribal</dd>
				 <dt>Meedia seaded</dt><dd>M&auml;&auml;rab vaikimisi seaded pildifailide k&auml;sitlemisele serverisse &uuml;les laadimisel. M&auml;rgistades "piira pildi suurust", tehakse vaikimisi k&otilde;ik pildid m&auml;&auml;ratud suuruseks s&auml;ilitades proportsiooni. M&auml;rgistades "avaneb suuremana" tehakse pildist kaks eri suuruses versiooni. </dd>
				 <dt>Taustav&auml;rv, taustapilt</dt><dd>AXIS CMSi tasuta versiooni kasutajatele m&otilde;eldud funktsioon, mis v&otilde;imaldab kujundada kodulehe &uuml;ldtausta. </dd>
				</dl>
				
				<h3>Sisu men&uuml;&uuml;</h3>
				<p>Kogu kodulehe sisu struktuur on k&auml;ttesaadav &uuml;he puukujulise men&uuml;&uuml;na, analoogne kataloogipuuga arvutis. </p>
				
				<h4><em>Men&uuml;&uuml;</em> ja <em>lehek&uuml;lg</em> &mdash; p&otilde;him&otilde;te</h4>
				<p><em>Lehek&uuml;lg</em> on terviklik sisu osa, millel on men&uuml;&uuml;s pealkiri ja URL (aadress) ning mille sisuks on tekst, pildid, tabelid jne. <em>Men&uuml;&uuml;</em> on sisupuu haru, mille sisuks on lehek&uuml;ljed, mis sellesse kuuluvad. Kui men&uuml;&uuml; paikneb m&otilde;ne teise men&uuml;&uuml; sees, nimetatakse seda <em>alammen&uuml;&uuml;ks</em>. </p>
				<p>Men&uuml;&uuml;d ja lehek&uuml;ljed on sisupuus t&auml;histatud erinevalt. Men&uuml;&uuml;de ees on "+" m&auml;rk, mis men&uuml;&uuml; avamisel muutub "-" m&auml;rgiks. "+" m&auml;rgile v&otilde;i men&uuml;&uuml; pealkirjale vajutades men&uuml;&uuml; avaneb, "-" m&auml;rgile vajutades saab men&uuml;&uuml; taas sulgeda. </p>

				<h3>Men&uuml;&uuml; tee</h3>
				<p>&Uuml;lemises paremas servas kuvatakse men&uuml;&uuml; tee, mis n&auml;itab avatud lehek&uuml;lje paiknemist men&uuml;&uuml;des ehk sisupuus. Link "kontrolli" avab sama teekonna avalikul kodulehel, mis on mugavaim v&otilde;imalus kontrollimaks, kuidas sisestatud sisu k&uuml;lastajatele v&auml;lja n&auml;eb. </p>
								
				<h3>Lehek&uuml;lje men&uuml;&uuml; (paneelid)</h3>
				<p>Igal lehek&uuml;ljel ja alammen&uuml;&uuml;l on omakorda oma valikute men&uuml;&uuml;, mis paikneb paneelidena sisuala &uuml;laserval: </p>
				<dl>
				 <dt>lehek&uuml;lje seaded</dt><dd>
				  <dl>
				   <dt>pealkiri</dt><dd>Lehek&uuml;lje pealkiri men&uuml;&uuml;s ja brauseri tiitliribal. </dd>
				   <dt>ID</dt><dd>Unikaalne fraas, mida kasutatakse lehek&uuml;lje aadressis (URL) ja failinimedes. ID v&otilde;ib sisaldada ainult rahvusvahelisi s&uuml;mboleid, numbreid ja "-" ning "_". Rahvuslikke s&uuml;mboleid (&uuml;&otilde;&ouml;&auml;) ja erim&auml;rke kasutada ei tohi. Kui j&auml;tta ID v&auml;li t&uuml;hjaks, genereeritakse ID automaatselt pealkirja p&otilde;hjal. </dd>
				   <dt>v&auml;line URL</dt><dd>V&otilde;imaldab men&uuml;&uuml;sse lisada lingi m&otilde;nele v&auml;lisele aadressile, staatilisele failile v&otilde;i muule lehek&uuml;ljele. </dd>
				   <dt>ava uues aknas</dt><dd>Link men&uuml;&uuml;s avaneb uues aknas. </dd>
				   <dt>l&uuml;hikirjeldus</dt><dd>Otsingumootoritele m&otilde;eldud l&uuml;hike kokkuv&otilde;te lehek&uuml;lje sisust, mida n&auml;idatakse &lt;meta description&gt; HTML elemendina. See v&auml;li on vajalik ainult spetsialistidele, kes seda kasutada oskavad. </dd>
				   <dt>lehek&uuml;lje t&uuml;&uuml;p (plugin)</dt><dd>Lehek&uuml;lje t&uuml;&uuml;p ehk plugin m&auml;&auml;rab antud lehek&uuml;lje sisu kujunduse ja funktsionaalsuse &mdash; mis funktsioone antud lehek&uuml;lg t&auml;idab ja milline on selle paigutus ning kujundus. Plugin valitakse lehek&uuml;lje lisamisel ja hiljem seda muuta ei saa. Uut lehek&uuml;lge lisades tuleb valida plugin, mis sobib lisatava lehek&uuml;lje otstarbe v&otilde;i funktsiooniga k&otilde;ige paremini. Kui tegemist on tavalise lehek&uuml;ljega, mis sisaldab ainult teksti jmt, v&otilde;ib plugin\'i j&auml;tta m&auml;&auml;ramata. </dd>
				   <dt>avalda, peida, lukusta</dt><dd><p>Sisestades avaldamise ja/v&otilde;i peitmise kuup&auml;eva, saab m&auml;&auml;rata, millal antud lehek&uuml;lg n&auml;htavaks muutub ja mis ajani see k&uuml;lastajatele n&auml;htav on. Uue lehek&uuml;lje lisamisel v&otilde;ib need v&auml;ljad j&auml;tta t&uuml;hjaks, siis avaldatakse lehek&uuml;lg koheselt ja see j&auml;&auml;b avaldatuks t&auml;htajatult. </p>
				   <p>Kui juba salvestatud lehek&uuml;ljel j&auml;tta need v&auml;ljad t&uuml;hjaks, on lehek&uuml;lg lukustatud - sellisel juhul on see alati avaldatud ja seda ei saa kustutada. Kustutamiseks peab lehek&uuml;lje muutma n&auml;iteks peidetuks. </p></dd>
				   <dt>j&auml;rjekorra nr</dt><dd>Lehek&uuml;lje j&auml;rjekorra number oma alammen&uuml;&uuml;s. </dd>
				   <dt>piira ligip&auml;&auml;su parooliga</dt><dd>Kui sisestada siia parool, n&otilde;utakse k&uuml;lastajatelt antud lehek&uuml;lje sisu vaatamiseks selle parooli sisestamist. Alammen&uuml;&uuml; puhul kaitstakse selle parooliga k&otilde;ik selles sisalduvad lehek&uuml;ljed ja alammen&uuml;&uuml;d. </dd>
				   <dt>n&otilde;ua sisse logimist ja/v&otilde;i piira ligip&auml;&auml;su gruppidega</dt><dd>Soovi korral v&otilde;ib teha antud lehek&uuml;lje vaatamise v&otilde;imalikuks ainult neile, kellel on olemas oma kasutajakonto, ehk registreeritud kasutajatele. M&auml;rgistades "login piisab", n&auml;evad selle lehek&uuml;lje sisu k&otilde;ik kasutajad, kes sisse logivad, s&otilde;ltumata sellest, kas nad mingisse kasutajate gruppi kuuluvad v&otilde;i mitte. M&auml;rgistades m&otilde;ne gruppidest, p&auml;&auml;sevad ligi ainult need kasutajad, kes kuuluvad sellesse gruppi. </dd>
				   <dt>men&uuml;&uuml; pilt</dt><dd><p>S&otilde;ltuvalt kodulehe kujundusest v&otilde;ib igale lehek&uuml;ljele lisada teemapildi v&otilde;i ikooni. Kas see funktsioon on kasutuses v&otilde;i kuhu pilt paigutatakse, on iga veebilehe puhul individuaalne. AXIS CMSi tasuta versiooni kodulehel n&auml;itatakse seda men&uuml;&uuml; taustapildina. </p><p>Pildifaile saab lisada kaks: esimene on m&otilde;eldud vaikimisi pildiks, teist n&auml;idatakse siis kui antud lehek&uuml;lg on men&uuml;&uuml;s aktiivne. </p></dd>
				  </dl>
				 </dd>
				 <dt>sisu</dt><dd>Lehek&uuml;lje sisu (tekst pildid, videod jne). Sisu on ainult <em>lehek&uuml;lgedel</em>, alammen&uuml;&uuml;del sisu puudub, sest men&uuml;&uuml;de sisuks on selle sees olevad lehek&uuml;ljed, millest iga&uuml;hel on oma sisu. Lehek&uuml;lje sisu toimetamisest t&auml;psemalt punktis "Lehek&uuml;lje sisu toimetamine". </dd>
				 <dt>postitused</dt><dd>K&uuml;lalisteraamatu sissekanded. K&uuml;lastajad saavad teateid postitada nt sellistel lehek&uuml;lgedel, mille lisamisel on valitud plugin "Postitusvormiga lehek&uuml;lg". </dd>
				 <dt>failid</dt><dd>Failide &uuml;leslaadimine veebiserverisse jm failihaldus. Vaikimisi avatakse antud lehek&uuml;lje v&otilde;i alammen&uuml;&uuml;ga seotud kaust. </dd>
				 <dt>import/export</dt><dd>V&otilde;imaldab importida sisu CSV failist (nt Exceli tabeli). </dd>
				</dl>
				
				<h3>Men&uuml;&uuml; struktuuri muutmine &mdash; lehek&uuml;lgede ja alammen&uuml;&uuml;de lisamine, muutmine ja kustutamine</h3>
				<p>Lehek&uuml;lgede ja alammen&uuml;&uuml;de lisamiseks on sisu men&uuml;&uuml;s lingid "lisa uus: lehek&uuml;lg  - alammen&uuml;&uuml;". Vajutades "lehek&uuml;lg" peale, lisatakse uus lehek&uuml;lg sisupuusse t&auml;pselt sellesse kohta, kus lehek&uuml;lje lisamisele vajutasite. Vajutades "alammen&uuml;&uuml;", lisatakse vastavalt uus alammen&uuml;&uuml;. </p>
				<p>Avaneb lehek&uuml;lje seadete paneel. Enamasti piisab kui sisestada lehek&uuml;lje/alammen&uuml;&uuml; pealkiri. Uusi lehek&uuml;lgi lisades tasub vaadata, kas lehek&uuml;lje t&uuml;&uuml;bi valikutes on lisatava lehek&uuml;lje sisu jaoks sobiva eesm&auml;rgiga plugin. Seej&auml;rel vajutada "salvesta". T&auml;psemalt punktis "Lehek&uuml;lje men&uuml;&uuml; (paneelid) &gt; lehek&uuml;lje seaded". </p>
				<p>Lehek&uuml;lgede ja alammen&uuml;&uuml;de kustutamiseks on "lehek&uuml;lje seaded" all nupp "kustuta". </p>
				
				<h3>Lehek&uuml;lje sisu toimetamine</h3>
				<h4>Sisutabel</h4>
				<p>Tavalise lehek&uuml;lje sisu toimetamine toimub <em>sisutabeli</em> kujul osadeks jaotataud plokkide kaupa. Vaikimisi on tabelis ainult &uuml;ks rida ja selles enamasti &uuml;ksainus lahter, kuid ridu saab hakata t&ouml;&ouml; k&auml;igus juurde lisama. S&otilde;ltuvalt lehek&uuml;lje seadetes valitud plugin\'ist v&otilde;ib veergusid olla ka mitu v&otilde;i kasutatakse hoopis mingit teistsugust sisu toimetamise liidest juhul kui plugin seda sisaldab. Sisutabel on &uuml;ksnes abivahend sisu toimetamiseks, avalikul kodulehel &uuml;ldjuhul n&auml;idatakse sisu tavalise tekstina, mitte tabelina. Samuti ei ole sellel kontseptsioonil midagi pistmist HTML tabelitega, vaid sisemiselt kasutatakse spetsiaalset andmeformaati. </p>
				<p>Igal sisutabeli real on oma t&ouml;&ouml;riistad ning igal lahtril omakorda oma t&ouml;&ouml;ristariba. </p>
				
				<h5>Sisutabeli rea t&ouml;&ouml;riistad</h5>
				<ul>
				 <li>Vajutades rea numbrile saab korraga muuta k&otilde;igi lahtrite sisu. Lisaks saab m&auml;&auml;rata avaldamise ja peitmise ajad ning muuta rea j&auml;rjekorra numbrit tabelis. Vormi l&otilde;pus n&auml;idatakse, kes ja millal on antud tabeli rida viimati muutnud. </li>
				 <li>Ridade liigutamiseks on "liiguta" j&auml;rel nooled, mis liigutavad antud rida vastavalt &uuml;he v&otilde;rra &uuml;les v&otilde;i alla. </li>
				 <li>Rea lisamiseks on igal real nupp "lisa". Uus rida lisatakse t&auml;pselt sellele kohale, kus vajutati "lisa": n&auml;iteks kui vajutada 1. rea kohal, tuleb uus rida 1. rea kohale ehk uueks esimeseks reaks ning senine esimene rida nihkub teiseks; kui vajutada 1. ja 2. rea vahel, lisatakse uus rida 1. ja 2. rea vahele ehk uueks teiseks reaks jne. </li>
				 <li>Rea saab kustutada nupu "kustuta" abil. </li>
				 <li>Rea avaldamise ja/v&otilde;i peitmise aeg. Sellele vajutades avaneb uues aknas sama lehek&uuml;lg avalikul kodulehel. Tehtud muudatuste kontrollimiseks ei pea pikal lehek&uuml;ljel &uuml;les tagasi kerima et vajutada "kontrolli", vaid v&otilde;ib vajutada avaldamise ajale. </li>
				</ul>
				<h5>Sisutabeli lahtri t&ouml;&ouml;riistad</h5>
				<dl>
				 <dt>muuda html&gt;&gt;</dt><dd>Visuaalne HTML tekstiredaktor (WYSIWYG). V&otilde;imaldab tekstit&ouml;&ouml;tlust analoogselt tavaliste tekstit&ouml;&ouml;tlusprogrammidega, lisada pilte, loendeid, tabeleid jmt (vt ka <a href="#userguide_wordproc">T&ouml;&ouml;tamine visuaalse HTML tekstiredaktoriga</a>). </dd>
				 <dt>fail&amp;meedia&gt;&gt;</dt><dd><p>V&otilde;imaldab kodulehele &uuml;les panna tahes mis faile. Eriti kasulik videote ja helifailide lisamiseks kodulehele, kuid sobib suurep&auml;raselt ka piltide, dokumentide ja k&otilde;igi muude failide jaoks. </p>
				  <p><strong>NB!</strong> Faili lisades on tungivalt soovitatav kasutada iga faili jaoks eraldi t&uuml;hja tabeli lahtrit! Sisutabeli ridade lisamise kohta vt punktist "Sisutabeli rea t&ouml;&ouml;riistad". </p>
				  </dd>
				 <dt>txt&gt;&gt;</dt><dd>Formaatimata tekstikast. Sobib  mujalt teksti kleepimiseks (<span lang="en">paste</span>), samuti erinevate koodijuppide lisamiseks, nt YouTube video, Google map, erinevad skriptid jne. </dd>
				</dl>
				<p><strong>Soovitatav on jaotada lehek&uuml;lje sisu mitmele sisutabeli reale.</strong> On paratamatu, et t&ouml;&ouml; k&auml;igus v&otilde;ib midagi sassi minna, kuid see piirdub tavaliselt &uuml;he tabeli lahtriga. Seega, mida rohkemate ridade peale on sisu jaotatud, seda v&auml;iksem osa saab korraga sassi minna. Kindlasti peaks kasutama eraldi lihtrit iga pildid v&otilde;i muu faili jaoks, mis lisatakse fail&amp;meedia&gt;&gt; t&ouml;&ouml;riista abil. Ka pikemad tekstil&otilde;igud on soovitatav lisada iga&uuml;ks eraldi reale. Teksti sisestamine mitme osa kaupa v&otilde;tab rohkem aega kui k&otilde;ik korraga &uuml;hte lahtrisse paneku puhul, kuid v&auml;hendab tehtud t&ouml;&ouml; kaotsimineku riski. </p>
				
				<h4>T&otilde;lgitavad m&auml;rks&otilde;nad</h4>
				<p>Kui lehek&uuml;lje raamistik sisaldab mingeid m&auml;rks&otilde;nu v&otilde;i silte, mis ei ole sisuteksti osa, kuid mida on vaja eri keeltes t&otilde;lkida, asub sisuala alumises osas vorm t&otilde;lgitavate m&auml;rks&otilde;nadega. Samuti v&otilde;ib see vorm sisaldad m&otilde;ningaid muid muudetavaid parameetreid nagu nt e-posti aadressid. </p>'."\n",
				),
			'edit-db'=>array(
				'title'=>'Intranet',
				'content'=>array(
					'intranet'=>array(
						'title'=>'Intraneti keskkond',
						'content'=>'<p>Intraneti keskkond on iseseisvatest moodulitest koosnev t&ouml;&ouml;keskkond. M&otilde;eldud on see ennek&otilde;ike andmebaaside administreerimiseks, kuid iga moodul v&otilde;ib sisaldada tahes millist veebip&otilde;hist tarkvara. Intraneti moodulid valmistatakse ja lisatakse igale veebilahendusele individuaalselt, k&auml;esoleva seisuga AXIS CMSi standardkomplekt &uuml;htegi moodulit ei sisalda. Kui Teie veebilahendus sisaldab intraneti mooduleid, on p&otilde;hjalikum info nende kohta leitav punktis "Intraneti moodulid". </p>'."\n",
						),
					'modules'=>array(
						'title'=>'Intraneti moodulid',
						'content'=>'<p>Antud veebilahendus intraneti moodulite dokumentatsiooni ei sisalda. </p>'."\n",
						),
					),
				),
			'wordproc'=>array(
				'title'=>'T&ouml;&ouml;tamine visuaalse HTML tekstiredaktoriga',
				'content'=>'<p>T&ouml;&ouml;tades HTML tekstiredaktoriga peab j&auml;rgima klassikalise tekstit&ouml;&ouml;tluse p&otilde;him&otilde;tteid: </p>
				<ul>
				 <li>Mitte &uuml;ritada teksti ja tausta v&auml;rvida, seada fonte v&otilde;i  suurusi v&otilde;i muul moel formaatida! See toimub kodulehel automaatselt.  P&otilde;him&otilde;tteks on et mida lihtsam, seda parem, k&otilde;ige paremini toimivad  vaikimisi seaded. K&otilde;igepealt proovida lihtsamalt, kontrollida tulemust  avaliku kodulehe kaudu ja vajadusel keerukamaid detaile lisada-muuta  j&auml;rkj&auml;rgult hiljem. </li>
				 <li>Teksti tuleb formaatida selleks ette n&auml;htud  elementide abil, mitte fonti suurust, v&auml;rvi jmt omadusi muutes! N&auml;iteks  l&otilde;iguteksti puhul tuleb m&auml;&auml;rata stiil <em>paragraph</em>, pealkirju tuleb teha vastava <em>header</em> v&otilde;i <em>heading</em> stiili m&auml;&auml;rates, mitte fonti suurendades! Loendeid ei tehta nii, et pannakse rea algusesse kriips vmt m&auml;rk, vaid selleks on olemas eraldi t&ouml;&ouml;riistad, mis teevad t&auml;ppidega ja nummerdatud loendeid. </li>
				 <li><strong>HTML tekstiredaktorisse ei tohi kopeerida teksti otse MS Wordist ja teistelt kodulehtedelt</strong>, sest sellised allikad v&otilde;ivad lisaks tekstile kaasa kopeerida vigast HTML koodi. Teksti kopeerimisel tuleks kasutada "kleebi wordist" v&otilde;i "kleebi tekstina" funktsioone v&otilde;i k&otilde;igepealt salvestada hoopis txt&gt;&gt; t&ouml;&ouml;riista abil ja siis v&otilde;ib seda HTML tekstiredaktori abil kohendada ja muuta kuidas iganes vaja. </li>
				 <li>Hoiduge teksti joondamisest, sest seda teeb kodulehe disain automaatselt vastavalt vajadusele. </li>
				</ul>'."\n",
				),
			'user-profile'=>array(
				'title'=>'Oma kasutajakonto muutmine',
				'content'=>'<p>Iga kasutaja on kohustatud hoolitsema selle eest, et tema profiil sisaldaks korrektseid andmeid ja v&otilde;imaldaks administraatoritel v&otilde;i klienditoe kontaktisikul temaga igal ajal kontakti saada. </p>
				<dl>
				 <dt>e-post</dt><dd>E-posti aadress on &uuml;htlasi kasutajatunnuseks, millega logitakse sisse. Soovi korral v&otilde;ib e-posti asemel kasutada ka klassikalist kasutajatunnust, mis ei tohi sisaldada rahvuslikke s&uuml;mboleid ja kirjavahem&auml;rkidest on lubatud ainult "-" ja "_". </dd>
				 <dt>parool</dt><dd>Parool ei tohi sisaldada rahvuslikke s&uuml;mboleid ja kirjavahem&auml;rkidest on lubatud ainult "-" ja "_". </dd>
				 <dt>eesnimi</dt><dd>Teie eesnimi</dd>
				 <dt>perenimi</dt><dd>Teie perekonnanimi</dd>
				 <dt>e-post 2</dt><dd>Sekundaarne e-posti aadress, ei ole kohustuslik. </dd>
				 <dt>telefon</dt><dd>Teie kontakttelefon. </dd>
				</dl>'."\n",
				),
			'edit-system'=>array(
				'title'=>'S&uuml;steem',
				'content'=>array(
					'users'=>array(
						'title'=>'Kasutajad',
						'content'=>array(
							'list'=>array(
								'title'=>'Kasutajate haldus',
								'content'=>'<p>Igal inimesel, kes sisse logib, peab olema oma isiklik kasutajakonto. V&auml;ltimaks erinevaid probleeme, ei tohiks oma kasutajatunnust ja parooli mitte kunagi kellelegi teisele anda, vaid peaks igale kasutajale tegema oma konto. Siin saab seda kergesti teha ja hiljem v&otilde;ib mittevajaliku konto kustutada. </p>
						<p>Kasutajakontod v&otilde;ivad olla salvestatud veebiserveri failis&uuml;steemi v&otilde;i SQL andmebaasi kui see on paigaldatud. Valik "allikas" m&auml;&auml;rab, kumbast kasutajate nimekirja n&auml;idatakse. </p>
						<p>Kasutajate nimekirja saab m&otilde;ningate valikute abil filtreerida. "staatus" v&otilde;imaldab n&auml;idata ainult hetkel sisse v&otilde;i v&auml;lja logitud kasutajaid, samamoodi "lukustatud" kontode puhul. Failis&uuml;steemi salvestatud kasutajatel "staatus" teada ei ole. </p>'."\n",
								),
							'groups'=>array(
								'title'=>'Kasutajate grupid',
								'content'=>'<p>Kasutajaid saab jagada erinevatesse gruppidesse. Mingisse gruppi kuulumine m&auml;&auml;rab kasutaja rolli s&uuml;steemis ja seel&auml;bi tema &otilde;igused mingeid toiminguid l&auml;bi viia. Ehk teisis&otilde;nu, &otilde;igusi jagatakse grupip&otilde;hiselt. Erandjuhuks on kasutajatunnus "adm", kellel on teatud juhtudel ainu&otilde;igus mingit toimingut l&auml;bi viia s&otilde;ltumata grupikuuluvusest. Kui Teie veebilahendus sisaldab intraneti mooduleid, v&otilde;ib kasutajate gruppide kohta t&auml;psustavat infot leida punktist "Intraneti moodulid". </p>
								<dl>
								 <dt>Administraatorid (admin)</dt><dd>S&uuml;steemi&uuml;lemad ehk kasutajad, kellel on ligip&auml;&auml;s ja muutmise &otilde;igused kogu s&uuml;steemis. </dd>
								 <dt>Veebihaldurid (cms)</dt><dd>Sellesse gruppi kuulumine annab &otilde;iguse siseneda administreerimiskeskkonda. Kasutajad, kes ei kuulu "admin" gruppi, kuid vajavad ligip&auml;&auml;su kodulehe halduseks v&otilde;i intranetti, peavad kuuluma lisaks m&otilde;nele spetsiifilisemale grupile ka "cms" gruppi. </dd>
								 <dt>Muud</dt><dd>Kasutajad, kes ei kuulu &uuml;htegi gruppi. </dd>
								</dl>'."\n",
								),
							'add'=>array(
								'title'=>'Uue kasutaja lisamine',
								'content'=>'<p>Uue kasutaja lisamiseks on kasutajate nimekirja p&auml;ises link "Lisa uus kasutaja", mis avab kasutajakonto salvestamise vormi. Edasi vt punkti "Kasutajakonto muutmine". </p>'."\n",
								),
							'edit'=>array(
								'title'=>'Kasutajakonto muutmine',
								'content'=>'<p>Kasutajakonto salvestamise vorm. T&auml;rniga m&auml;rgistatud v&auml;ljade t&auml;itmine on kohustuslik. </p>
								<dl>
								 <dt>lukustatud</dt><dd>V&otilde;imaldab kasutaja blokeerida tema kontot kustutamata. </dd>
								 <dt>e-post</dt><dd>E-posti aadress on &uuml;htlasi kasutajatunnuseks, millega kasutaja logib sisse. Soovi korral v&otilde;ib e-posti asemel kasutada ka klassikalist kasutajatunnust, mis ei tohi sisaldada rahvuslikke s&uuml;mboleid ja kirjavahem&auml;rkidest on lubatud ainult "-" ja "_". Soovitatav on siiski kasutada e-posti aadressi. </dd>
								 <dt>parool</dt><dd>Parool ei tohi sisaldada rahvuslikke s&uuml;mboleid ja kirjavahem&auml;rkidest on lubatud ainult "-" ja "_".</dd>
								 <dt>eesnimi</dt><dd>Kasutaja eesnimi </dd>
								 <dt>perenimi</dt><dd>Kasutaja perekonnanimi </dd>
								 <dt>e-post 2</dt><dd>Sekundaarne e-posti aadress, ei ole kohustuslik </dd>
								 <dt>telefon</dt><dd>Kasutaja kontakttelefon </dd>
								 <dt>grupid</dt><dd>Grupp v&otilde;i grupid, millesse antud kasutaja kuulub </dd>
								 <dt>kodukataloogid</dt><dd><p>Kodulehed, mille sisu ja failidega antud kasutajal on &otilde;igus t&ouml;&ouml;tada. Veerus "koduleht" valitakse soovitud koduleht ning muudatuste j&otilde;ustumiseks vajutada "salvesta". Kodukatalooge v&otilde;ib lisada mitu. Kodukataloogi kustutamiseks tuleb vastaval real m&auml;rgistada "kustuta" ja vajutada nuppu "salvesta". </p>
								  <p>Vajadusel v&otilde;ib piirata kasutaja ligip&auml;&auml;su vaid mingi kindla alammen&uuml;&uuml;ga. Selleks tuleb veerus "kataloog" sisestada vastav kataloog. N&auml;iteks piiramaks kasutaja v&otilde;imalusi ainult "Koduleht 1>Men&uuml;&uuml;1>Mingi alammen&uuml;&uuml;" tuleb v&otilde;tta failihaldusest selle men&uuml;&uuml; kataloogitee ja sisestada see osa, mis on peale "content/", ehk antud n&auml;ites http://www.sait.eu/content/menu1/alammenuu/ j&auml;&auml;b j&auml;rgi "menu1/alammenuu/". </p>
								 </dd>
								</dl>
								<p>Et login sessiooni v&auml;rskendatakse kindla intervalli tagant, mitte iga lehe laadimisega, v&otilde;ib muudatuste j&otilde;ustumine kasutaja enda profiilis v&otilde;tta aega. Muudatuste j&otilde;ustumiseks koheselt aitab kui kasutaja logib v&auml;lja ja uuesti sisse. </p>'."\n",
								),
							'del'=>array(
								'title'=>'Kasutaja kustutamine',
								'content'=>'<p>Kasutaja kustutamiseks on kasutajakonto salvestamise vormi alumises servas nupp "kustuta kasutaja". Sisse loginud kasutaja ei saa kustutada seda kontot, millega ta parajasti sisse loginud on. </p>'."\n",
								),
							),
						),
					'log'=>array(
						'title'=>'Log',
						'content'=>'<p>S&uuml;steemi logifailide vaatamine. "msgtype" veerule vajutades avaneb detailne info sissekande kohta, v&otilde;imalus antud sissekanne saata e-posti aadressile. "host" n&auml;itab infot k&uuml;lastaja asukoha kohta tema IP aadressi p&otilde;hjal. </p>'."\n",
						),
					'sysinfo'=>array(
						'title'=>'S&uuml;steemi info',
						'content'=>'<p>N&auml;itab infot antud AXIS CMSi installatsiooni ja veebiserveri seadete kohta. </p>'."\n",
						),
					'cfg'=>array(
						'title'=>'S&uuml;steemi seaded',
						'content'=>'<p>S&uuml;steemi &uuml;ldseadete muutmine. <strong>NB!</strong> Nende parameetrite muutmine ei ole m&otilde;eldud tavakasutajale! Ligip&auml;&auml;s on piiratud ainult "admin" grupiga. </p>
						<dl>
						 <dt>S&uuml;steemiteadete saatmise aadress</dt><dd>Sellele aadressile saadetakse logi sissekanded jmt. Mitme aadressi puhul eraldada komaga. </dd>
						 <dt>&Auml;ra saada seda t&uuml;&uuml;pi teateid</dt><dd>Komadega eraldatud nimekiri logi sissekande t&uuml;&uuml;pidest, mida e-postiga ei saadeta. Nt kasutajate sisselogimised ("login") kirjutatakse logifailidesse, kuid enamasti e-postiga nendest teavitust ei soovita. </dd>
						 <dt>FTP URL</dt><dd><p>T&auml;ielik URL FTP serverisse koos parooliga ja kataloogitee kuni veebilehe juurkataloogini. Nt ftp://user:pass@server.com/home/public_html/</p><p>Vajalik on see serverites, kus on aktiveeritud PHP Safe Mode, mist&otilde;ttu ilma FTP &uuml;henduseta pole v&otilde;imalik alammen&uuml;&uuml;sid ja kaustu lisada. &Uuml;ks selline on olnud nt Planet.ee hostinguteenus. </p>
						 <p>Samuti on FTP linki v&otilde;imalik kasutada sisuhalduse kaudu kui on vaja serverisse laadida suuremas koguses faili korraga. Ligip&auml;&auml;s FTP kaudu on turvalisuse huvides v&otilde;imaldatud ainult "admin" gruppi kuuluvatele kasutajatele. </p>
						 <p><strong>NB!</strong> Kui FTP kasutamine ei ole h&auml;davajalik, tasuks FTP andmeid mitte sisestada, sest tegemist on turvariskiga! </p>
						 </dd>
						 <dt>Image convert utiliidi asukoht</dt><dd>Kui serverisse on paigaldatud ImageMagic, GraphicsMagic vmt utliit pildifailide k&auml;sitlesmiseks. Nt "/opt/zone/bin/gm convert" </dd>
						 <dt>IP aadressi asukoha m&auml;&auml;raja URL</dt><dd>S&uuml;steemilogis n&auml;idatakse selle abil k&uuml;lastaja IP aadressi geograafilist asukohta. </dd>
						 <dt>Kodulehed</dt><dd>Kodulehed, mida AXIS CMS on seadistatud haldama. Igal kodulehel on j&auml;rgmised parameetrid: 
						  <dl>
						   <dt>nimi sisuhalduse men&uuml;&uuml;s</dt><dd>Kodulehe pealkiri administreerimiskeskkonnas. </dd>
						   <dt>kataloog</dt><dd>Kui &uuml;hes AXIS CMSi installatsioonis on mitu kodulehte, asetsevad need iga&uuml;ks eraldi alamkataloogis. N&auml;iteid: 
						    <ol>
						     <li>kui kodulehe aadressiks on www.example.com ja CMSi asukoht www.exaple.com/axiscms, siis alamkataloog puudub</li>
							 <li>kui kodulehe aadressiks on web.zone.ee/mingileht ja CMSi asukoht web.zone.ee/mingileht/axiscms, siis alamkataloog puudub. </li>
							 <li>kui kodulehe aadressiks on web.zone.ee/mingileht/blog ja CMSi asukoht web.zone.ee/mingileht/axiscms, on kataloogiks "blog/"</li>
							 <li>kui kodulehe aadressiks on www.exaple.com/mingileht ja CMSi asukoht www.exaple.com/axiscms, on kataloogiks "mingileht/"</li>
							</ol>
						   </dd>
						   <dt>max men&uuml;&uuml; s&uuml;gavus</dt><dd>Mitu taset alammen&uuml;&uuml;sid on lubatud &uuml;ksteise sisse teha. 0=piiramatu s&uuml;gavus. </dd>
						   <dt>kasuta SQL andmebaasi</dt><dd>AXIS CMS v&otilde;ib olla seadistatud kasutama mitut andmebaasiserverit. See valik m&auml;&auml;rab, millist neist kasutab antud koduleht. AXIS CMSi saab kasutada ka ilma andmebaasiserverita. </dd>
						   <dt>SQL tabeli prefiks</dt><dd>Kui kasutatakse andmebaasi, v&otilde;ib tabelite nimedele anda prefiksi. Kui erinevad kodulehed kasutavad samu tabeleid, tuleb kodulehtedele anda sama prefiks, vastasel korral erinev. </dd>
						   <dt>ajav&ouml;&ouml;nd</dt><dd>Veebiserveri kasutatav ajav&ouml;&ouml;nd. Erinevatele regioonidele suunatud kodulehtedele v&otilde;ib anda kohaliku ajav&ouml;&ouml;ndi. Ka &uuml;he kodulehe puhul on soovitatav ajav&ouml;&ouml;nd sisestada. </dd>
						  </dl>
						 </dd>
						</dl>'."\n",
						),
					),
				),
			'edit-doc'=>array(
				'title'=>'Abiinfo',
				'content'=>'<p>Info AXIS CMSi kohta ja kasutusjuhendid. </p>'."\n",
				),
			'feedback'=>array(
				'title'=>'Tagasiside',
				'content'=>'<h2>Tagasiside vorm</h2>
<p>K&uuml;simuste ja ettepanekute esitamiseks ning v&otilde;imalikest s&uuml;steemi  vigadest teatamiseks on selleks ette n&auml;htud spetsiaalne tagasiside  vorm, mis avaneb peamen&uuml;&uuml;st lingi &quot;tagasiside&quot; kaudu. Kui tekib mingi  k&uuml;simus v&otilde;i m&otilde;ni asi ei paista t&ouml;&ouml;tavat ootusp&auml;raselt, tuleks seda  kindlasti kasutada. Kui tegemist on kiireloomulise probleemiga, tuleks  igal juhul k&otilde;igepealt saata taustainfo tagasiside vormi kaudu ning siis  vajadusel helistada. Kontaktandmed on toodud tagasiside vormi l&otilde;pus. </p>
<p>Tagasiside  saatmisel on oluline, et seda tehtaks koheselt ja samas kohas kus  probleem avaldub, mitte teha vahepeal mingeid muid toiminguid   v&otilde;i teatada probleemidest tagantj&auml;rele kui pole enam  adekvaatset taustainfot. Infovahetus on probleemide lahendamise  esmaseks eelduseks, seega tasub probleemide tekkimisel alati anda  tagasisidet ja k&uuml;sida n&otilde;u. </p>',
				),
			),
		),
	'licence'=>array(
		'title'=>'Litsents',
		'content'=>'<p>AXIS CMS, (edaspidi &quot;TARKVARA&quot;), on kirjutanud Ivar Tohvelmann (edaspidi &quot;AUTOR&quot;) - kontakt: IT.axis(&auml;t)hot.ee.
      K&auml;esolev TARKVARA on vabavara, mida  v&otilde;ib kasutada, muuta, ja levitada vastavalt GNU &Uuml;ldise Avaliku Litsentsi tingimustele. Lisaks sellele on kasutajal j&auml;rgmised kohustused: 
      </p>
	  <ul>
	   <li>teavitada TARKVARA AUTORit (soovitavalt e-kirja teel), millisel veebilehel TARKVARA kasutatakse </li>
	   <li>administreerimiskeskkonnast ei tohi eemaldada lehek&uuml;lje alumises osas asuvat viidet Axis Multimedia\'le </li>
	   <li>veebilehe HTML koodist ei tohi eemaldada ega deaktiveerida rida &lt;meta name="generator" content="AXIS CMS" /&gt; </li>
	  </ul>
	   <p>Veebilehe avalikes osades sisalduva viite "Powered by AXIS CMS" s&auml;ilitamine ei ole kohustuslik. Selle eemaldamine on lubatud. </p>
	   <p>Kuna tegemist on tasuta tarkvaraga, ei vastuta selle AUTOR(id) mitte mingite TARKVARA kasutamisega seotud tagaj&auml;rgede eest. </p>
  <p>Info uusima versiooni kohta: <a class="link" href="http://'.AXS_DOMAIN.'/axiscms">'.AXS_DOMAIN.'/axiscms</a></p>',
		),
	);
#2005 ?>