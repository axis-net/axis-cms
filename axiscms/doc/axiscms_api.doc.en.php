<?php #09.03.2012
return array(
	'api'=>array(
		'title'=>'AXIS CMS API documentation',
		'lang'=>'en',
		'content'=>array(
			'about'=>array(
				'title'=>'About',
				'content'=>'<p><img src="'.AXS_PATH_CMS.'doc/axiscms_api.png" alt="AXIS CMS framework drawing" /></p>
				<p>This page is under construction. </p>'."\n",
				),
			'sections'=>array(
				'title'=>'Sisualade (b&auml;nneri kohtade) lisamine',
				'content'=>'<ol>
 <li>Sisuhalduses lisada sisu juurikale (v&auml;ljaspoole men&uuml;&uuml;sid) lehek&uuml;lg, mille ID oleks &quot;banner3&quot;. Sisu juurikale muudatuste tegemiseks tuleb sisse logida kasutajaga &quot;adm&quot;, sellenimelise konto loomiseks vaata &quot;S&uuml;steem&gt;Kasutajad&quot;. </li>
 <li>Faili &quot;axs_plugins/index.tpl&quot; lisada sobivasse kohta rida <br />
 &lt;div id=&quot;banner3&quot;&gt;{$banner3}&lt;/div&gt;</li>
 <li>Uue sisuala paigutamiseks ja kujundamiseks viia sisse vastavad muudatused CSS koodi failides &quot;axs_plugins/index.css&quot; (ekraan) ja &quot;axs_plugins/index.print.css&quot; (print meedia) </li>
</ol>',
				),
			),
		),
	);
#2010 ?>