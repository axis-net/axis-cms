<?php #2021-08-11
return array(
	'css'=>'text/css',
	'csv'=>'application/csv',
	'doc'=>'application/msword',
	'gif'=>'image/gif',
	'html'=>'text/html',
	'jpg'=>'image/jpeg',
	'jpeg'=>'image/jpeg',
	'js'=>'text/javascript',
	'ods'=>'application/vnd.oasis.opendocument.spreadsheet',
	'odt'=>'application/vnd.oasis.opendocument.text',
	'pdf'=>'application/pdf',
	'png'=>'image/png',
	'txt'=>'text/plain',
	'xlsx'=>'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
	'zip'=>'application/zip',
	);
#2013-04-17 ?>