<?php #2022-03-29 utf-8
return array(
	'search.found.lbl'=>'leitud',
	'search.order.lbl'=>'järjesta',
	'search.submit.lbl'=>'otsi',
	'_submit.lbl'=>'salvesta',
	'msg_value_required'=>'Välja täitmine on kohustuslik!',
	'msg_value_invalid'=>'Väärtus ei ole sobiv!',
	'msg_value_unique'=>'Väärtus peab olema kordumatu!',
	);
#2022-03-29 ?>