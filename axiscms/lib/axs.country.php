<?php #2021-08-11
class axs_country {
	static $f='axs.dict.country';
	static $index=array('name'=>0, 'naitive'=>1);
	static function form_select() {
		$a=include(axs_dir('lib').self::$f.'.php');
		foreach ($a as $k=>$v) {
			$tmp=($v[self::$index['naitive']]) ? $v[self::$index['naitive']].' ('.$v[self::$index['name']].')':$v[self::$index['name']];
			$a[$k]=array('value'=>$k, 'label'=>$tmp, /*'label.html'=>htmlspecialchars($tmp),*/ );
			}
		return $a;
		}
	static function get($code=false, $get=false) {
		$a=include(axs_dir('lib').self::$f.'.php');
		if ($code===false) return $a;
		if (!$code) return '';
		if ($get===false) return $a[$code];
		if ($get==='') return ($a[$code][self::$index['naitive']]) ? $a[$code][self::$index['naitive']].' ('.$a[$code][self::$index['name']].')':$a[$code][self::$index['name']];
		return ($a[$code][$get]) ? $a[$code][self::$index[$get]]:$a[$code][self::$index['name']];
		}
	} #</class::axs_country>
#2017-12-14 ?>