<?php #2021-07-30 utf-8
class axs_valid {
	# a limited function to convert between charsets
	static function convert($subject, $to, $from=false) {
		global $axs;
		if ($from===false) $from=$axs['cfg']['charset'];
		if ($from===$to) return $subject;
		if ($to!=='standard') return mb_convert_encoding($subject, $to, $from);
		$subject=mb_convert_encoding($subject, 'utf-8', $from);
		$map=array(
			'find'=>array(
				1=>'ä', 2=>'Ä', 3=>'ö', 4=>'Ö', 5=>'ü', 6=>'Ü', 7=>'õ', 8=>'Õ', 9=>'ž', 10=>'Ž', 11=>'š', 12=>'Š', #et
				30=>'а', 31=>'А', 32=>'б', 33=>'Б', 34=>'в', 35=>'В', 36=>'г', 37=>'Г', 38=>'д', 39=>'Д', 
				40=>'е', 41=>'Е', 42=>'ё', 43=>'Ё', 44=>'ж', 45=>'Ж', 46=>'з', 47=>'З', 48=>'и', 49=>'И', 
				50=>'й', 51=>'Й', 52=>'к', 53=>'К', 54=>'л', 55=>'Л', 56=>'м', 57=>'М', 58=>'н', 59=>'Н', 
				60=>'о', 61=>'О', 62=>'п', 63=>'П', 64=>'р', 65=>'Р', 66=>'с', 67=>'С', 68=>'т', 69=>'Т', 
				70=>'у', 71=>'У', 72=>'ф', 73=>'Ф', 74=>'х', 75=>'Х', 76=>'ц', 77=>'Ц', 78=>'ч', 79=>'Ч', 
				80=>'ш', 81=>'Ш', 82=>'щ', 83=>'Щ', 84=>'ы', 85=>'Ы', 86=>'э', 87=>'Э', 88=>'ю', 89=>'Ю', 
				90=>'я', 91=>'Я', #ru
				),
			'replace'=>array(
				1=>'a', 2=>'A', 3=>'o', 4=>'O', 5=>'u', 6=>'U', 7=>'o', 8=>'O', 9=>'z', 10=>'Z', 11=>'s', 12=>'S',  #et
				30=>'a', 31=>'A', 32=>'b', 33=>'B', 34=>'v', 35=>'V', 36=>'g', 37=>'G', 38=>'d', 39=>'D', 
				40=>'e', 41=>'E', 42=>'e', 43=>'E', 44=>'z', 45=>'Z', 46=>'z', 47=>'Z', 48=>'i', 49=>'I', 
				50=>'i', 51=>'I', 52=>'k', 53=>'K', 54=>'l', 55=>'L', 56=>'m', 57=>'M', 58=>'n', 59=>'N', 
				60=>'o', 61=>'O', 62=>'p', 63=>'P', 64=>'r', 65=>'R', 66=>'s', 67=>'S', 68=>'t', 69=>'T', 
				70=>'u', 71=>'U', 72=>'f', 73=>'F', 74=>'h', 75=>'H', 76=>'c', 77=>'C', 78=>'tc', 79=>'TC', 
				80=>'sh', 81=>'SH', 82=>'tch', 83=>'TCH', 84=>'y', 85=>'Y', 86=>'je', 87=>'JE', 88=>'ju', 89=>'ju', 
				90=>'ja', 91=>'JA', #ru
				),
			);
		return str_replace($map['find'], $map['replace'], $subject);
		} # </convert()>
	static function email($string) { # function to check if an e-mail address is valid
		if ($string) {
			/*if (strpos($string, ' ') or strpos(' '.$string, ',')  or strpos(' '.$string, ';') or strlen($string)<5
			 or !strpos($string, '@')) return false;*/
			if (!preg_match( '#^'.
				//'[a-z0-9]+([_.\-+%][a-z0-9]+)*'.    //user
				'[a-z0-9]+([_.\-+%a-z0-9]+)*'.    //user
            	'@'.
				'([a-z0-9]+([.-][a-z0-9]+)*)+'.   //domain
				//'\\.[a-z]{2,}'.                    //sld, tld
				'$#i', $string, $regs)) return false;
			}
		return true;
		} # </email()>
	static function emails($string) { # function to check if an e-mail address is valid
		if ($string) {
			if (!is_array($string)) $string=explode(',', str_replace(' ', '', $string));
			foreach ($string as $v) if (!$v or !self::email($v)) return false;
			}
		return true;
		} # </emails()>
	static function f_ext($f) { # function to get file extension
		$tmp=explode('.', $f);
		return (strpos(' '.$f, '.')) ? strtolower(array_pop($tmp)):'';
		} # </f_ext()>
	# function to make a valid filename from a string
	static function f_name($f, $f_ext=false, $lmt=255) {
		global $axs;
		$f=basename($f);
		$f=self::convert($f, 'standard', $axs['cfg']['charset']);
		if (strlen($f)>$lmt) {
			if ($f_ext===false) $f_ext=self::f_ext($f);
			$f=substr($f, 0, $lmt-strlen($_fext)-4).'...'.$f_ext;
			}
		$f=preg_replace('/[^[:space:]\!a-zA-Z0-9*_.\-()]| |(\*)+/i', '_', $f);
		return $f;
		}# </f_name()>
	static function f_secure($f) { # function to secure file or directory name
		return preg_replace('#"|//|\'|\..\|\\\|:#', '', $f);
		} # </f_secure()>
	static function id($string) { #<function to make a valid id from a string />
		global $axs;
		$string=self::convert($string, 'standard', $axs['cfg']['charset']);
		$string=preg_replace('/ +/', '-', trim($string));
		$string=preg_replace('/[^[:space:]a-zA-Z0-9*_.-]|( )+|(\*)+|(\.)+/', '', $string);
		return $string;
		}
	# function to check if single-line input is valid
	static function line($string, $length=0) {
		if (strpos(' '.$string, "\n") or strpos(' '.$string, "\r")) return false;
		if (strlen($string)>$length) return false;
		return true;
		} # </lune()>
	static function shellencode($str) {
		return str_replace(array('%','&',), array('%%','^&',), $str);
		} #</shellencode()>
	static function user($string, $format='auto') {
		if (!$format or $format=='@') return self::email($string);
		if ($format=='id') return (self::id($string)==$string);
		# <Auto />
		if (strpos($string, '@')) return self::email($string);
		else return (self::id($string)==$string);
		} # </user()>
	} # </axs_valid>
#2005 ?>