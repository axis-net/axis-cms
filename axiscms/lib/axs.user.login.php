<?php #2021-02-01
class axs_user_login {
	# Return login form. Include message if login failed or access denied. 
	static function form($l=false, $tr=false) {
		global $axs;
		if (!$l) $l=(isset($axs['editor']->l)) ? $axs['editor']->l:axs_get('l', $axs);
		if (!$tr) {
			$tr=axs_tr::load_class_tr(__CLASS__);
			$tr=$tr->tr;
			}
		if (@file_exists('../')) $action='./'.((strlen($_SERVER['QUERY_STRING'])) ? '?'.$_SERVER['QUERY_STRING']:'');
		else {	$action=(@file_exists('./axiscms.php')) ? './axiscms.php?'.$_SERVER['QUERY_STRING']:'../axiscms.php?'.$_SERVER['QUERY_STRING'];	}
		#$url=(strlen($_SERVER['QUERY_STRING'])) ? $action.'&':$action.'?';
		
		//axs_user_login::logout($_GET['axs']['logout']);
		//if ($u['pass_send']) return self::login_pass_send($u['user']);
		# <If reset password />
		//elseif ($u['pass_reset']) return self::login_pass_reset($u['user']);
		//'    <input id="axs_pass_send" name="axs[pass_send]" type="checkbox" value="1"'.(($axs['mail()'])?'':' disabled="disabled"').' />'."\n".axs_get('submit.lbl', $tr, '$submit.lbl').'
		
		$msg=(isset($axs['msg']['axs_login'])) ? '<ul class="msg"><li>'.axs_get('msg_'.$axs['msg']['axs_login'], $tr, '$msg_'.$axs['msg']['axs_login']).'</li></ul>':'';
		
		/*if (isset($_GET['axs']['login']['reset'])) {
			return 
			'	<form id="axs_login_form" class="axs login" action="'.htmlspecialchars($action).'" method="post" lang="'.$l.'">'."\n".
			'		<fieldset><legend>'.axs_get('pass.reset.lbl', $tr, '$pass.reset.lbl').'</legend>'."\n".
			'			'.$msg."\n".
			'			<label>'."\n".
			'				'.axs_get('pass.lbl', $tr, '$pass.lbl')."\n".
			'				<input id="axs_pass" name="axs[pass]" type="password" />'."\n".
			'			</label>'."\n".
			'			<label>'."\n".
			'				'.axs_get('pass.confirm.lbl', $tr, '$pass.confirm.lbl')."\n".
			'				<input id="axs_pass" name="axs[pass]" type="password" />'."\n".
			'			</label>'."\n".
			'			<input id="axs_login" name="axs[login]" type="submit" value="'.axs_get('pass.reset.lbl', $tr, '$pass.reset.lbl').'" />'."\n".
			'		</fieldset>'."\n".
			'	</form>'."\n";
			}*/
		$user=(isset($_POST['axs']['user'])) ? $_POST['axs']['user']:axs_get('axs_user_remember', $_COOKIE);
		$user_remember=($user) ? ' checked="checked"':'';
		return ' <form id="axs_login_form" class="axs login" action="'.htmlspecialchars($action).'" method="post" lang="'.$l.'">'."\n".
		'  <fieldset><legend>'.axs_get('login.lbl', $tr, '$login.lbl').'</legend>'."\n".
		'   '.$msg."\n".
		self::form_input_preserve($_POST).
		'   <label>'."\n".
		'    '.axs_get('user.lbl', $tr, '$user.lbl')."\n".
		'    <input id="axs_user" name="axs[user]" type="text" value="'.htmlspecialchars($user).'" maxlength="'.axs_user::$user_length.'" />'."\n".
		'   </label>'."\n".
		'   <label>'."\n".
		'    '.axs_get('pass.lbl', $tr, '$pass.lbl')."\n".
		'    <input id="axs_pass" name="axs[pass]" type="password" />'."\n".
		'   </label>'."\n".
		'   <label>'."\n".
		'    <input id="axs_user_remember" name="axs[user_remember]" type="checkbox" value="1"'.$user_remember.' />'."\n".
		'    '.axs_get('user_remember.lbl', $tr, '$user_remember.lbl')."\n".
		'   </label>'."\n".
		'   <label>'."\n".
		'    <input id="axs_pass_send" name="axs[pass_send]" type="checkbox" value="1"'.(($axs['mail()'])?' disabled="disabled"':'').' />'."\n".
		'    '.axs_get('pass_send.lbl', $tr, '$pass_send.lbl')."\n".
		'   </label>'."\n".
		'   <input id="axs_login" name="axs[login]" type="submit" value="'.axs_get('submit.lbl', $tr, '$submit.lbl').'" />'."\n".
		'  </fieldset>'."\n".
		' </form>'."\n".
		' <script>axs.user.window_size_send("axs_login_form");</script>'."\n";
		} #</form()>
	static function form_input_preserve($post, $px='') {
		unset($post['axs']['user'], $post['axs']['pass'], $post['axs']['user_remember'], $post['axs']['pass_send'], $post['axs']['login']);
		$input='';
		foreach ($post as $k=>$v) {
			if ($px) $k='['.$k.']';
			if (is_array($v)) $input.=self::form_input_preserve($v, $px.$k);
			else {
				$input.='  <input name="'.htmlspecialchars($px.$k).'" type="hidden" value="'.axs_html_safe($v).'" />'."\n";
				}
			}
		return $input;
		} #</form_input_preserve()>
	static function login($post) {
		global $axs;
		$u=array('user'=>'user','pass'=>'pass_plain','pass_send'=>'pass_send','user_remember'=>'user_remember',);
		foreach ($u as $k=>$v) $u[$v]=(isset($post['axs'][$k])) ? trim($_POST['axs'][$k]):'';
		if ($u['user_remember']) setcookie('axs_user_remember', $u['user'], $axs['time']+90*86400, $axs['http_root']);
		else setcookie('axs_user_remember', '', $time=$axs['time']-42000, $axs['http_root']);
		if (defined('AXS_LOGINCHK')) return;
		# <If send password request />
		if ($u['pass_send']) return self::login_pass_send($u['user']);
		else { # <Check for login>
			axs_user::init($u);
			if (AXS_LOGINCHK===1) {
				foreach (array('user', 'pass', 'user_remember', 'login', 'screen_size', 'window_size', 'pixel_ratio', ) as $v) {	unset($_POST['axs'][$v]);	}
				if (empty($_POST['axs'])) unset($_POST['axs']);
				axs_log(__FILE__, __LINE__, 'login'); # log successful login
				//axs_user::update_online($axs['time']); # update users table to show user as online
				axs_admin::user_log('login');
				} 
			else {
				$axs['msg']['axs_login']='failed';
				axs_log(__FILE__, __LINE__, 'loginfail', 'Login attempt failed'); # log failed login attempt
				}
			} # </Check for login>
		} #</login()>
	static function login_pass_send($usr) {
		global $axs;
		$o=new axs_users_edit();
		$user=$o->user_get(trim($usr));
		if (empty($user['user'])) axs_log(__FILE__, __LINE__, 'loginsend.fail', 'User not found.'); # <log event />
		else {
			$user['pass_tmp']=bin2hex(random_bytes(10));
			$user['pass_tmp_expire']=$axs['time']+axs_user::$pass_tmp_expire;
			self::tmp_update($user, array('pass_tmp'=>password_hash($user['pass_tmp'], PASSWORD_DEFAULT), 'pass_tmp_expire'=>$user['pass_tmp_expire'], ));
			foreach (array($user['user'], $user['email']) as $v) if (strpos($v, '@')) {
				axs_mail(
					$v, $_SERVER['SERVER_NAME'].$axs['http_root'],
					$axs['http'].'://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']."\n".
					'	'.$user['user']."\n".
					$user['pass_tmp']."\n\n".
					'('.date('Y-m-d H:i:s', $axs['time']).' - '.date('Y-m-d H:i:s', $user['pass_tmp_expire']).')',
					'', $user['fstname'].' '.$user['lstname'], $v
					);
				axs_log(__FILE__, __LINE__, 'loginsend.ok', 'Temporary password sent.'.$axs['http'].'://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']."\n".
					'	'.$user['user']."\n".
					$user['pass_tmp']."\n\n".
					'('.date('Y-m-d H:i:s', $axs['time']).' - '.date('Y-m-d H:i:s', $user['pass_tmp_expire']).')'); # <log event />
				}
			}
		$axs['msg']['axs_login']='pass_send';
		} #</login_pass_send()>
	static function tmp_update(array $user, array $change) {
		$f=axs_user::tmp_get($user);
		foreach ($change as $k=>$v) {	$user[$k]=$v;	}
		$data=axs_user::$fields_tmp;
		foreach ($data as $k=>$v) {	$data[$k]=axs_get($k, $user, $v);	}
		$data='<?php return '.axs_fn::array_code($data).'; ?>';
		file_put_contents(AXS_PATH_CMS.'data/'.$f, $data);
		} #</update_tmp()>
	
	/*static function login_pass_reset(&$u, $token) {
		global $axs;
		$cl=array();
		$f=AXS_PATH_CMS.'data/tmp/u.'.axs_valid::f_secure(substr($token, 0, 20)).'.php';
		$t=substr($token, 20);
		if (file_exists($f)) $cl=(array)include($f);
		return $axs['msg']['axs_login']='pass_send';
		
		$o=new axs_users_edit();
		$user=$o->user_get(trim($usr));
		if (empty($user['user'])) axs_log(__FILE__, __LINE__, 'loginsend.fail', 'User not found.'); # <log event />
		else {
			
			foreach (array($user['user'], $user['email']) as $v) if (strpos($v, '@')) {
				axs_mail(
					$v, $_SERVER['SERVER_NAME'].$axs['http_root'],
					$_SERVER['SERVER_NAME'].$axs['http_root']."\n	".$user['user']."\n	".$user['pass'],
					'', $user['fstname'].' '.$user['lstname'], $v
					);
				axs_log(__FILE__, __LINE__, 'loginsend.ok', 'Password reset link sent.'); # <log event />
				}
			}
		if (file_exists($f)) unlink($f);
		}*/ #</login_pass_reset()>
	static function logout($redir) {
		global $axs, $axs_user;
		define('AXS_LOGOUT', true);
		if (!defined('AXS_LOGINCHK')) axs_user::init();
		if (AXS_LOGINCHK===1) {
			axs_user::update_online($axs['time']-$axs['cfg']['login_update']); # update users table to show user as offline
			axs_user::session_set(array(), $axs['time']-42000);
			}
		axs_ob_catch($axs['ob']); # log errors if any output
		exit(axs_redir($redir));
		} #</logout()>
	}
#2005-07 ?>