<?php #2017-05-24
class axs_filemonitor {
	public function __construct($base_dir='') {
		$this->path=$base_dir;
		$this->data_file=AXS_PATH_CMS.'data/index.filemonitor.php';
		} #</__construct()>
	public function check($path) {
		$files=$this->scan($path);
		$checklist=$this->checklist_load();
		$diffs=$this->compare($files, $checklist);
		return $this->compare_report_txt($diffs);
		} #</check()>
	public function check_save($path) {
		$files=$this->scan($path);
		if (!$this->checklist_save($files)) return false;
		return $this->checklist_display($files);
		} #</check()>
	public function checklist_display($files=false) {
		if ($files===false) $files=$this->checklist_load();
		return 'Files listed ('.count($files).'):'."\n".implode("\n", array_keys($files));
		} #</checklist_load()>
	public function checklist_load() {
		if (!file_exists($this->data_file)) $this->log(__FUNCTION__, __LINE__, 'Checklist file not found "'.$this->data_file.'"');
		return $this->data=unserialize(include($this->data_file));
		} #</checklist_load()>
	public function checklist_save($data) {
		$written=file_put_contents($this->data_file, "<?php return '".serialize($data)."'; ?>");
		if (!$written) $this->log(__FUNCTION__, __LINE__, 'Failed to save checklist file "'.$this->data_file.'"');
		return $written;
		} #</checklist_save()>
	public function compare($new, $old) { #<Compare Hashed Files with stored records. />
		$diffs=array('Added'=>array(), 'Altered'=>array(), 'Deleted'=>array());
		foreach ($new as $k=>$v) {
			if (isset($old[$k])) {
				if ($old[$k]===$v) continue;
				else $diffs['Altered'][$k]=filemtime($k);
				}
			else  $diffs['Added'][$k]=filemtime($k);
			}
		foreach ($old as $k=>$v) if (!isset($new[$k])) $diffs['Deleted'][$k]='';
		$this->compare_count_diff=0;
		foreach ($diffs as $k=>$v) $this->compare_count_diff+=count($v);
		return $diffs;
		} #</compare()>
	public function compare_count_diff($diffs) { 
		$count=0;
		foreach ($diffs as $k=>$v) $count+=count($v);
		return $count;
		} #</compare_count_diff()>
	public function compare_report_txt($diffs) { 
		if (!$this->compare_count_diff) return 'No filesystem changes found.';
		$txt="The following discrepancies were found:\n";
		foreach ($diffs as $k=>$v) {
			$txt.=$k.' ('.count($v).'):'."\n";
			foreach($v as $k=>$v) $txt.='	'.(($v)?date('Y-m-d H:i:s', $v):'????-??-?? ??:??:??').' '.$k."\n";
			}
		return $txt;
		} #</compare_report_txt()>
	function log($function, $line, $msg='') { #<Report errors />
		return axs_log($file, $line, 'class', __CLASS__.'::'.$function.'(): '.$msg.' (line '.$line.')');
		} # </log()>
	public function scan($path) {
		$files=array();
		$scan=array();
		foreach ((array)$path as $k=>$v) {
			//$k=str_replace('\\', '/', $v);
			$k=$v;
			if (!preg_match('#^/|^[a-zA-Z]\:#', $k)) $k=$this->path.$k; #Prepend basedir if not absolute path
			$sw=array('e'=>array(), 'x'=>array(), );
			if (strpos($k, ' ')) {
				$v=explode(' ', $k);
				$k=$v[0];
				foreach ($v as $kk=>$vv) if ($kk>0) $sw[$vv[0]][]=substr($vv,2);
				}
			foreach ($sw['x'] as $kk=>$vv) if ($vv!=='.') $sw['x'][$kk]=rtrim(str_replace('\\', '/', realpath($k.$vv)), '/');
			$k=str_replace('\\', '/', realpath($k));
			$scan[$k]=$sw;
			if (in_array('.', $sw['x'])) unset($scan[$k]);
			}
		foreach ($scan as $k=>$v) {
			if (is_dir($k)) $files+=$this->scan_dir($k, $v['e'], $v['x']);
			else $files+=$this->scan_file($k);
			}
		return $files;
		} #</scan()>
	public function scan_dir($path, $ext=array(), $skip=array()) {
		$files=array();
		if (!file_exists($path)) return $files;
		$dir=new RecursiveDirectoryIterator($path);
		$iter=new RecursiveIteratorIterator($dir);
		while ($iter->valid()) {
			$f=str_replace('\\', '/', $iter->key());
			foreach ($skip as $k=>$v) if (strncmp($f, $v, strlen($v))===0) $f=false;
			//if ($skip) dbg(str_replace('\\', '/', $path.$iter->getSubPath()), $skip);
			if ((!$iter->isDot()) && ($f)) { // 	skip unwanted directories
				if (!empty($ext)) { #<get specific file extensions only />
					// 	PHP 5.3.4: if (in_array($iter->getExtension(), $ext)) 
					if (!in_array(pathinfo($f, PATHINFO_EXTENSION), $ext)) $files+=$this->scan_file($f);
					}
				else $files+=$this->scan_file($f); #<ignore file extensions />
				}
			$iter->next();
			}
		return $files;
		} #</scan_dir()>
	public function scan_file($f) {
		if (!file_exists($f)) return array();
		return array($f=>hash_file("sha1", $f), );
		} #</scan_file()>
	} #</class::axs_filemonitor>
#2013-09-03 ?>