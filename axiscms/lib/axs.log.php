<?php #class::axs_log - logs and reports system messages
class axs_log {
	static $ver='2022-03-30';
	static $file_format=array(
		# CMS
		'time'=>array('k'=>1,'l'=>19), 'msgtype'=>array('k'=>2,'l'=>25), 'msg'=>array('k'=>3,'l'=>110000), 'usr'=>array('k'=>4,'l'=>255), 
		'fname'=>array('k'=>5,'l'=>30000), 'line'=>array('k'=>6,'l'=>30000), 'cms_ver'=>array('k'=>7,'l'=>50),
		# User
		'request'=>array('k'=>8,'l'=>10000), 
		'cookies'=>array('k'=>9,'l'=>30000,'urlenc'=>true), 'post'=>array('k'=>10,'l'=>30000,'urlenc'=>true), 'files'=>array('k'=>11,'l'=>30000,'urlenc'=>true), 'referer'=>array('k'=>12,'l'=>10000), 'user_agent'=>array('k'=>13,'l'=>1024), 
		'host'=>array('k'=>14,'l'=>1024), 'ipaddr'=>array('k'=>15,'l'=>255), 
		# Server
		'server'=>array('k'=>16,'l'=>1024), 
		);
	static $mail_wordwrap=70;
	public $msg=array();
	public $write=true;
	function __construct($cfg=false) {
		//return;
		global $axs;
		$this->cfg=($cfg) ? $cfg:$axs['cfg'];
		if (!defined('AXS_LOGINCHK')) axs_user::init();
		$this->dir=rtrim(dirname(dirname(__FILE__)), '/').'/'.$axs['dir_cfg'];
		$this->httproot=$axs['http_root'];
		# get IP and hostname name of the sender
		$this->ipaddr=$_SERVER['REMOTE_ADDR'];
		$this->hostname=@gethostbyaddr($this->ipaddr);
		} #</__construct()>
	public function msg($file_name='', $line='', $msgtype='', $msg='') {
		global $axs;
		# prepare log entry
		$file_name=str_replace('\\', '/', $file_name);
		$msgtype=axs_valid::f_name($msgtype);
		if (is_array($msg)) $msg=self::array_dump($msg);
		if (!empty($axs['dbg'])) $msg.='(Debug data: '.implode("\n", $axs['dbg']).")\n\n";
		$axs['dbg']=array();
		# check if password submitted, secure session id and password
		$post=$_POST;
		if (isset($post['axs']['pass'])) {
			if ((strlen($post['axs']['pass'])) && (!preg_match('/(\=)|(")|(\')|( or )/i', $post['axs']['pass']))) $post['axs']['pass']='*';
			}
		$cookie=$_COOKIE;
		if (isset($cookie['axs_user_pass'])) {
			if ((strlen($cookie['axs_user_pass'])) && (!preg_match('/(\=)|(")|(\')|( or )/i', $cookie['axs_user_pass']))) $cookie['axs_user_pass']='*';
			}
		if ($session_name=ini_get('session.name')) {
			if (isset($cookie[$session_name])) $cookie[$session_name]='*';
			}
		# make cookie, post and uploaded files data a string for debugging
		//$cookie=$this->array_dump($cookie);
		//$post=$this->array_dump($post);
		//$files=$this->array_dump($_FILES);
		$files=$_FILES;
		
		$user_agent=array();
		foreach (array('screen_size'=>'screen-size', 'window_size'=>'window-size', 'pixel_ratio'=>'px-ratio') as $k=>$v) {
			$user_agent[]=$v.':'.((isset($_REQUEST['axs'][$k])) ? htmlspecialchars($_REQUEST['axs'][$k]):'?');
			}
		$user_agent=$_SERVER['HTTP_USER_AGENT'].' ('.implode(' ', $user_agent).')';
		$entry=array(
			# CMS
			'time'=>date('Y-m-d H:i:s', $axs['time']), 'msgtype'=>$msgtype, 'msg'=>$msg, 'usr'=>axs_user::get('user'), 'fname'=>$file_name, 'line'=>$line, 'cms_ver'=>AXS_CMS_VER, 
			# User
			'request'=>$axs['http'].'://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'],
			'cookies'=>$cookie, 'post'=>$post, 'files'=>$files, 'referer'=>axs_get('HTTP_REFERER', $_SERVER), 'user_agent'=>$user_agent, 
			'host'=>$this->hostname, 'ipaddr'=>$this->ipaddr, 
			# Server
			'server'=>'HTTP Server: '.axs_get('SERVER_SOFTWARE', $_SERVER).'; OS: '.PHP_OS.'; PHP: '.phpversion().' ('.PHP_SAPI.')',
			);
		
		$this->write($entry);
		if ($msgtype==='feedback') $this->mail($entry);
		else $this->msg[]=$entry;
		} #</msg()>
	public function mail($msg=false) {
		global $axs;
		if ((!$axs['mail()']) or (empty($this->cfg['emails']))) return;
		if ($msg===false) $msg=&$this->msg;
		else $msg=array($msg);
		$logmail=$replyto=$this->cfg['emails'];
		$logsubject=array();
		$logmsg='';
		$count=0;
		foreach ($msg as $k=>$v) if (!in_array($v['msgtype'], $this->cfg['dontmail'])) {
			$logsubject[$k]=$v['msgtype'];
			if ($v['msgtype']==='feedback') {
				$replyto=array(axs_user::get('@'), );
				if ($tmp=axs_user::get('email')) $replyto[]=$tmp;
				$logmail=array_merge($logmail, $replyto);
				$logsubject[$k].=' - "'.$v['msg'].'"';
				}
			$txt=
			str_repeat('-', self::$mail_wordwrap)."\n".
			$v['time']."\n".
			'Message type: '.$v['msgtype']."\n".
			'Message: '.$v['msg']."\n".
			'Script file name: '.$v['fname'].' ('.$v['line'].')'."\n".
			'Request: '.$v['request']."\n".
			'Cookies: '."\n".
			self::array_dump($v['cookies']).
			'Post: '."\n".
			self::array_dump($v['post']).
			'Files: '."\n".
			self::array_dump($v['files']).
			'Referer: '.$v['referer']."\n".
			'User: '.$v['usr'].' (last online check: '.((!empty($_COOKIE['axs_lastcheck'])) ? date('d.m.Y H:i:s', $_COOKIE['axs_lastcheck']):'').")\n".
			'Host: '.$v['host'].' ('.$v['ipaddr'].') Geographical location: '.str_replace('{$ip}', $v['ipaddr'], axs_file_array_get(axs_dir('lib').'axs.dict.php', 'ip_location_url'))."\n".
			'User agent: '.$v['user_agent']."\n\n";
			$logmsg.=wordwrap($txt, self::$mail_wordwrap);
			$count++;
			}
		if (empty($logmsg)) return;
		$logmsg=
			'AXIS CMS log @ '.$axs['http'].'://'.$_SERVER['SERVER_NAME'].$this->httproot."\n".
			$logmsg.
			str_repeat('-', self::$mail_wordwrap)."\n".
			'AXIS CMS v'.AXS_CMS_VER.' (log ver '.self::$ver.') http://'.AXS_DOMAIN.'/axiscms';
		$logmail=implode(', ', array_unique($logmail));
		$replyto=implode(', ', array_unique($replyto));
		$logsubject='CMS log message ('.$count.'): '.implode(', ', $logsubject);
		foreach ($msg as $v) if ($v['msg']) $logsubject.='; '.$v['msg'];
		if (strlen($logsubject)>60) $logsubject=substr($logsubject, 0, 60).'..."';
		$logsubject=preg_replace("/((\r\n)+|(\n\r)+|\n+)/", '', $logsubject);
		$logheaders=array(
			'From: AXIS CMS log(at)'.preg_replace('/^www\./', '', $_SERVER['SERVER_NAME']).' <'.current($axs['cfg']['emails']).">\r\n".
			'MIME-Version: 1.0'."\r\n".
			'Content-type: text/plain; charset='.$axs['cfg']['charset']."\r\n".
			'Reply-To'=>$replyto,
			//'X-Priority: 3'."\r\n".
			//'X-MSMail-Priority: Normal'."\r\n".
			'X-Mailer'=>'php/'.phpversion(),
			);
		//exit("<pre> $logmail, $logsubject, $logmsg, $logheaders </pre>");
		axs_mail($logmail, $logsubject, $logmsg, '', 'AXIS CMS log(at)'.preg_replace('/^www\./', '', $_SERVER['SERVER_NAME']).$this->httproot, current($this->cfg['emails']), array(), $logheaders);
		} #</mail()>
	public function write($msg) {
		if (!$this->write) return false;
		# prepare logfile entry
		$entry=array();
		foreach (self::$file_format as $k=>$v) {
			if (is_array($msg[$k])) $entry[$k]=substr(http_build_query(self::array_esc($msg[$k]), '', '&', PHP_QUERY_RFC3986), 0, $v['l']);
			else $entry[$k]=axs_tab_edit::esc(htmlspecialchars(substr($msg[$k], 0, $v['l']), ENT_QUOTES));
			}
		$entry=implode('|', $entry);
		
		# find logfile
		$logfiles=self::file_list($this->dir);
		$year=date('y');
		# if no logfiles found, create new
		if (empty($logfiles)) {
			$logfiles[0]='log.'.$year.'0001.php';
			if (!$this->write_file($this->dir.$logfiles[0],'')) return;
			}
		$logfcount=count($logfiles);
		$logfile=$logfiles[$logfcount-1];
		
		# Write to logfile
		$this->write_file($this->dir.$logfile,'<?php #|'.$entry.'|?>'."\n");
		
		# if logfile over 1MiB, create next file
		if (filesize($this->dir.$logfile)>1048576) {
			$lognr=($year==(substr($logfile, 4, 2))) ? substr($logfile, 6, 4):0;
			$logfile='log.'.$year.substr($lognr+10001, 1).'.php';
			if (!file_exists($this->dir.$logfile)) {
				if ($this->write_file($this->dir.$logfile,'')) {
					$logfiles[]=$logfile;
					$logfcount++;
					}
				}
			}
		# rotate logfiles
		$logsize=($this->cfg['logsize']) ? $this->cfg['logsize']+1:2;
		if ($logfcount>$logsize) {
			$delete=$logfcount-$logsize;
			for ($nr=0; $nr<$delete; $nr++) {
				if (!@unlink($this->dir.$logfiles[$nr])) $this->msg(__FILE__, __LINE__, 'writelog', 'Failed to delete logfile "'.$this->dir.$logfiles[$nr].'"');
				}
			}
		} #</write()>
	public function write_file($f, $data) {
		$fexist=file_exists($f);
		# check logfile permissions
		if (($fexist) && (!is_writable($f))) {
			#if (!$axs['mail']) exit(dbg(axs_ob_catch($axs['ob']),$msg));
			//@chmod($fspath, 0644);
			@chmod(dirname($f), 0644);
			@chmod($f, 0644);
			}
		# open logfile end, lock, write, close logfile
		$tmp=file_put_contents($f, $data, FILE_APPEND|LOCK_EX);
		if ($tmp===false) {
			$this->write=false;
			$msg=($fexist) ? 'Failed to write logfile "'.$f.'"':'Failed to create logfile "'.$f.'"';
			$this->msg(__FILE__, __LINE__, 'writelog', $msg);
			return false;
			}
		else return true;
		} #</write_file()>
	static function array_decode($str) {
		parse_str($str, $array);
		return $array;
		} #</array_decode()>
	static function array_dump($array, $space='') { # <recursive function to output array as a string />
		$space.='	';
		$txt='';
		if (is_string($array)) $array=self::array_decode($array);
		foreach((array)$array as $k=>$v) {
			$txt.=$space.'['.$k.']=';
			if (is_string($v)) $txt.='"'.$v.'"'."\n";
			elseif (is_bool($v)) {	$txt.=($v) ? 'TRUE'."\n":'FALSE'."\n";	}
			elseif (is_null($v)) $txt.='NULL'."\n";
			elseif (is_array($v)) $txt.='array('."\n".self::array_dump($v, $space).$space.')'."\n";
			else $txt.=$v."\n";
			}
		return $txt;
		} #</array_dump()>
	static function array_esc($array) {
		$a=array();
		foreach($array as $k=>$v) $a[htmlspecialchars($k)]=(is_array($v)) ? self::array_esc($v):axs_tab_edit::esc(htmlspecialchars($v), ENT_QUOTES);
		return $a;
		} #</array_encode()>
	static function file_list($dir=false) {
		global $axs;
		if ($dir===false) $dir=AXS_PATH_CMS.$axs['dir_cfg'];
		$logfiles=array();
		$handle=opendir($dir);
		while (($file=readdir($handle))!==false) {	if (strncmp($file, 'log.', 4)==0) $logfiles[]=$file;	}
		closedir($handle);
		sort($logfiles);
		return $logfiles;
		} #</file_list()>
	} #</class::axs_log>
?>