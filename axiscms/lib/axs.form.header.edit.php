<?php #2019-07-13
class axs_form_header_edit extends axs_form_edit {
	var $key_add='header_add';
	var $key_site='header_site';
	var $key_module='header_module';
	var $key_form='header_form';
	var $key_parent='header_parent';
	var $key_id='id';
	var $key_edit='header_edit';
	function __construct($url, $site_nr=false, $module=false, $form=false, $fieldset=false, $fields=array()) {
		global $axs;
		axs_form::__construct(array('user_input'=>$_POST));
		$this->editor=new axs_admin(axs_dir('lib'), preg_replace('/\.php$/', '', basename(__FILE__)), '', false);
		$this->editor->tr=new axs_tr(false, array('section'=>'edit-form'));//$this->editor->tr_load(axs_tr::$lang, 'edit-form');
		$this->site_nr=($site_nr!==false) ? $site_nr:axs_get($this->key_site, $_GET);
		$this->module=($module!==false) ? $module:axs_get($this->key_module, $_GET);
		$this->form=($form!==false) ? $form:axs_get($this->key_form, $_GET);
		$this->fieldset=$fieldset;
		if ($this->fieldset) {	if (!is_array($this->fieldset)) $this->fieldset=array($this->fieldset=>'', );	}
		$this->fields=$fields;
		$this->url_root='?';
		$this->url=$url;
		$this->url[$this->key_site]=$this->site_nr;
		$this->url[$this->key_module]=$this->module;
		$this->url[$this->key_form]=$this->form;
		$this->url[$this->key_structure_edit]=(isset($_GET[$this->key_structure_edit])) ? $_GET[$this->key_structure_edit]:'';
		$this->table=$this->editor->px.'header';
		if ($this->site_nr) {
			$this->content_langs=$axs['cfg']['site'][$this->site_nr]['langs'];
			$this->l_content=key($this->content_langs);
			}
		$this->permission=array('add'=>false, 'alter'=>false, );
		if (axs_user::permission_get('dev')) $this->permission['add']=$this->permission['alter']=true;
		$this->tpl=array(
			'form'=>
				'     <h2 class="breadcrumb">{$breadcrumb}</h2>'."\n".
				'     <form id="header_edit" class="form" action="{$action_url}" method="post" enctype="multipart/form-data">'."\n".
				'      <p class="tools"><a href="{$back_url}" class="back">&lt;{$back_lbl}</a></p>'."\n".
				'{$msg}{$elements}     <span class="clear both"></span>'."\n".
				'     </form>'."\n".
				'     <script>axs.form.init(\'header_edit\')</script>',
			);
		$this->tr=&$this->editor->tr;
		} #</__construct()>
	function editor($edit=false) {
		global $axs;
		$axs['page']['head'][$this->editor->name.'.css']='<link href="'.axs_dir('lib','h').$this->editor->name.'.css'.'" rel="stylesheet" type="text/css" media="screen" />'."\n";
		if ($this->site_nr) {
			if (!$axs['cfg']['site'][$this->site_nr]['db']) return '<p class="msg" lang="en">SQL database not configured on site "'.$this->site_nr.'".</p>';
			}
		if ($edit===false) $edit=axs_get($this->key_structure_edit, $_GET, '');
		$this->editor->vars['breadcrumb']=$this->breadcrumb();
		switch ($edit) {
			case 'ee':
				return $this->list_form_elements_edit(axs_get($this->key_id, $_GET));
				break;
			case 'me':
				return $this->list_modules_edit(axs_get($this->key_id, $_GET));
				break;
			case 'tr':
				return $this->list_form_elements_edit_translations();
				break;
			case '':
			default:
				if ($this->form) return $this->list_form_elements();
				if ($this->site_nr) return $this->list_modules();
				return $this->list_sites();
				#break;
			}
		if (!$return) axs_log(__FILE__, __LINE__, 'class', 'Error @ '.get_class($this), true);
		return $return;
		} #</editor()>
	function breadcrumb() {
		global $axs;
		if ($this->site_nr) $this->header(($this->module) ? false:'list');
		$url=array($this->key_structure_edit=>'', $this->key_site=>false, $this->key_module=>false, $this->key_form=>false, );
		$html=array('sites'=>'<a href="'.$this->url_root.axs_url($this->url, $url).'">'.$this->editor->tr('sites_lbl').'</a>', );
		if ($this->site_nr) {
			$url[$this->key_site]=$this->site_nr;
			//$url[$this->key_structure_edit]='ml';
			$html['site']='<a href="'.$this->url_root.axs_url($this->url, $url).'">'.axs_html_safe($axs['cfg']['site'][$this->site_nr]['title']).' '.$this->editor->tr('modules_lbl').'</a>';
			}
		if ($this->module) {
			$url[$this->key_module]=$this->module;
			//$url[$this->key_structure_edit]='ml';
			$html['module']='<a href="'.$this->url_root.axs_url($this->url, $url).'">'.axs_html_safe($this->header->sql_header_cfg_get($this->module, '', 'label')).'</a>';
			}
		if ($this->form) {
			$url[$this->key_form]=$this->form;
			//$url[$this->key_structure_edit]='el';
			$html['form']='<a href="'.$this->url_root.axs_url($this->url, $url).'">'.axs_html_safe($this->header->sql_header_cfg_get($this->module, $this->form, 'label')).'</a>';
			}
		return implode('&nbsp;&gt;&nbsp;', $html);
		} #</breadcrumb()>
	
	#<Editor functions ----------------------------------------------- >
	function list_sites() {
		global $axs;
		$html='     <table id="header_edit_sites_list" class="table selectable">'."\n".
		'      <caption>'.$this->breadcrumb().'</caption>'."\n".
		'      <tr><th class="nr">'.$this->editor->tr('list.site.nr.lbl').'</th><th class="forms">'.$this->editor->tr('list.site.site.lbl').'</th></tr>'."\n";
		foreach ($axs['cfg']['site'] as $k=>$v) {
			$v['url']=$this->url_root.axs_url($this->url, array($this->key_site=>$k, ));
			$html.='      <tr class="top"><th class="nr" scope="row">'.$k.'</th><td class="site"><a href="'.$v['url'].'">'.axs_html_safe($v['title']).'</a></td></tr>'."\n";
			}
		return $html.'     </table>';
		} #</list_modules()>
	function list_modules() {
		if ($this->header->header) {
			$modules=(!$this->module) ? $this->header->header:array($this->module=>$this->header->header[$this->module], );
			}
		else $modules=array();
		$url_add=$this->url_root.axs_url($this->url, array($this->key_structure_edit=>'me', $this->key_id=>false, 'nr'=>count($modules)+1, 'b'=>$this->url_root.$_SERVER['QUERY_STRING'].'#id', ));
		$html='     <table id="header_edit_modules_list" class="table selectable">'."\n".
		'      <caption>'.$this->breadcrumb().'</caption>'."\n".
		'      <tr><th class="module" colspan="2">'.$this->editor->tr('module_lbl').'</th><th class="forms">'.$this->editor->tr('forms_lbl').'</th><th class="edit"><a href="'.$url_add.'">'.$this->editor->tr('module_add_lbl').'</a></th></tr>'."\n";
		foreach ($modules as $k=>$v) {
			$cl=$v[''];
			unset($v['']);
			$url=array_merge($this->url, array($this->key_structure_edit=>'ml', $this->key_form=>false, $this->key_module=>$k, $this->key_form=>false, ));
			$url_edit=array_merge($url, array($this->key_structure_edit=>'me', $this->key_module=>$this->module, $this->key_form=>$this->form, ));
			$cl['url']=$this->url_root.axs_url($url);
			$cl['url_add']=$this->url_root.axs_url($url_edit, array($this->key_id=>false, 'nr'=>count($v)+1, 'b'=>$this->url_root.$_SERVER['QUERY_STRING'].'#id'.$cl['id'], ));
			$cl['url_edit']=$this->url_root.axs_url($url_edit, array($this->key_module=>$k, $this->key_id=>$cl['id'], 'b'=>$this->url_root.$_SERVER['QUERY_STRING'].'#id'.$cl['id'], ));
			$html.='      <tr class="top"><th>'.$cl['nr'].'</th><td class="m"><a href="'.$cl['url'].'">'.$cl['module'].' ('.$cl['label'].')</a></td><td class="forms">';
			foreach ($v as $kk=>$vv) {
				$vv=$vv[''];
				$vv['url']=$this->url_root.axs_url($url, array($this->key_structure_edit=>'el', $this->key_form=>$kk));
				$vv['url_edit']=$this->url_root.axs_url($url_edit, array($this->key_module=>$k, $this->key_form=>$kk, $this->key_id=>$vv['id'], 'b'=>$this->url_root.$_SERVER['QUERY_STRING'].'#id'.$vv['id'], ));
				$vv['url_tr']=$this->url_root.axs_url($url, array($this->key_structure_edit=>'tr', $this->key_form=>$kk));
				$v[$kk]='        <tr><th>'.$vv['nr'].'</th><td><a href="'.$vv['url'].'">'.$kk.' ('.$vv['label'].')</a></td><td><a href="'.$vv['url_edit'].'">'.$this->editor->tr('edit_lbl').'</a> <a href="'.$vv['url_tr'].'">'.$this->editor->tr('tr.lbl').'</a></td></tr>'."\n";
				}
			$html.="\n".'       <table class="table selectable">'."\n".
			'        <tr><th scope="col" colspan="2" class="form">'.$this->editor->tr('form_lbl').'</th><th scope="col" class="edit"><a href="'.$cl['url_add'].'">'.$this->editor->tr('form_add_lbl').'</a></th></tr>'."\n".implode('', $v).'        </table>'."\n";
			$html.='      </td><td><a href="'.$cl['url_edit'].'">'.$this->editor->tr('edit_lbl').'</a></td></tr>'."\n";
			}
		return $html.'     </table>';
		} #</list_modules()>
	function list_modules_edit($id) {
		global $axs;
		$id+=0;
		$cl=axs_db_query('SELECT * FROM `'.$this->table.'` WHERE `site`=\''.$this->site_nr.'\' AND `id`=\''.$id.'\'', 'row', $this->editor->db, __FILE__, __LINE__);
		$this->b=(isset($_GET['b'])) ? $_GET['b']:$this->url_root.axs_url($this->url, array($this->key_structure_edit=>''), false);
		$this->url['b']=$this->b;
		//$this->save=axs_get('save', $_POST);
		$this->structure=axs_form_header::$fields_cfg[''];
		foreach(axs_form_header::$fields_cfg['cfg'] as $k=>$v) $this->structure[$k]=$v;
		$this->structure+=array(
			'cfg/'=>array('type'=>'fieldset_end', ),
			'save'=>array('type'=>'submit', 'add_fn'=>true, 'comment'=>$this->editor->tr('del_lbl'), ),
			'updated'=>array('type'=>'timestamp-updated', 'size'=>50, 'add_fn'=>1, ),
			);
		foreach ($this->structure as $k=>$v) {
			$this->structure[$k]['label']=$this->editor->tr($k.'_lbl'); 
			if (($k=='module') or ($k=='form')) $this->structure[$k]['options']+=$this->list_form_elements_edit_structure_options($k);
			if (!empty($v['lang-multi'])) $this->structure[$k]['lang-multi']=$this->content_langs;
			}
		$this->structure_set($this->structure);
		$this->cl=$this->form_input(array_merge($_REQUEST, array('id'=>$id)));
		#<Delete />
		if (isset($_POST['save_delete']) && isset($_POST['save_delete_commit'])) {
			axs_db_query('DELETE FROM `'.$this->table.'` WHERE `id`=\''.$this->cl['id'].'\'', '', $this->editor->db, __FILE__, __LINE__);
			exit(axs_redir($this->b));
			}
		# <Error check>
		if ($this->save) $this->validate($this->cl);
		if (!empty($this->msg)) $this->save=false;
		# </Error check>
		# <Save>
		if ($this->save) {
			$this->cl['cfg']=array();
			foreach(axs_form_header::$fields_cfg['cfg'] as $k=>$v) {	$this->cl['cfg'][$k]=$this->cl[$k];	unset($this->structure[$k]);	}
			$this->cl['cfg']=serialize($this->cl['cfg']);
			foreach (array('module'=>'module', 'form'=>'form') as $k=>$v) if (empty($this->structure[$k]['options'][$this->cl[$k]])) {
				$this->sql_structure->table_alter_change($this->table, $k, array('_value'=>$this->cl[$k]), 'add');
				}
			//table_install(array($this->table=>array($k=>$this->structure[$k])));;
			if (!$this->cl['id']) {
				$this->cl['id']=axs_db_query(array('INSERT INTO `'.$this->table.'` SET `site`=\''.$this->site_nr.'\', `field`=\'cfg\', `type`=\'\', '.$this->sql_values_set($this->cl, ''), $this->sql_p), 'insert_id', $this->editor->db, __FILE__, __LINE__);
				}
			#<Set order>
			$rank_where='`site`=\''.$this->site_nr.'\' AND `field`=\'cfg\' AND `type`=\'\' AND ';
			$rank_where.=(!$this->cl['form']) ? '`form`=\'\'':'`module`=\''.addslashes($this->cl['module']).'\' AND `form`!=\'\'';
			$this->cl['nr']=$this->sql_structure->rank_set($this->table, 'nr', $this->cl['id'], $this->cl['nr'], $rank_where);
			#</Set order>
			axs_db_query(array('UPDATE `'.$this->table.'` SET '.$this->sql_values_set($this->cl, '').', `cfg`=\''.addslashes($this->cl['cfg']).'\''."\n".
			'	WHERE `site`=\''.$this->site_nr.'\' AND `id`=\''.$this->cl['id'].'\'', $this->sql_p), '', $this->editor->db, __FILE__, __LINE__);
			exit(axs_redir($this->b));
			} # </Save>
		# <Read editor>
		if (empty($_POST) && $this->cl['id'] && empty($this->msg)) {
			$this->cl=axs_db_query("SELECT t.*, u.`user` AS `updated_text`\n".
			"	FROM `".$this->table."` AS t LEFT JOIN `".axs_user::$table."` AS u ON u.`id`=t.`updated_uid`\n".
			'	WHERE t.`site`=\''.$this->site_nr.'\' AND t.`id`=\''.$this->cl['id'].'\'', 'row', $this->editor->db, __FILE__, __LINE__);
			$this->cl+=(array)unserialize($this->cl['cfg']);
			$this->cl=$this->sql_values_get($this->cl);
			}
		$this->editor->tpl_set($this->editor->name, $this->tpl['form']);
		$this->editor->tpl_set('input_required', $this->templates('required'));
		$this->editor->vars['action_url']=$this->url_root.axs_url($this->url, array($this->key_id=>$this->cl['id'], 'axs['.$this->key_form_id.']'=>$this->form_id));
		$this->editor->vars['back_url']=htmlspecialchars($this->b);
		$this->editor->vars['msg']=$this->msg_html();
		$this->editor->vars['elements']='';
		foreach ($this->structure_get() as $k=>$v) $this->editor->vars['elements'].=$this->element_html($k, $v, $this->cl);
		$this->css_js();
		$this->editor->vars+=$this->editor->tr->tr;
		return $this->editor->finish(); #</Read editor>
		} #</list_modules_edit()>
	function list_fieldset($fieldset=false) {
		$array=array(''=>array('label'=>'', 'value'=>''));
		foreach (axs_db_query('SELECT `fieldset` AS `value`, CONCAT(`fieldset`,\' (\',`label_'.$this->l_content.'`,\')\') AS `label` FROM `'.$this->table.'` WHERE `site`=\''.$this->site_nr.'\' AND `module`=\''.$this->module.'\' AND `form`=\''.$this->form.'\' AND `type`=\'fieldset\' ORDER BY `nr`, `label`', 1, $this->editor->db, __FILE__, __LINE__) as $cl) $array[$cl['value']]=$cl;
		return $array;
		} #</list_fieldset()>
	function list_form_elements() {
		//$this->header();
		$this->b=$this->url_root.$_SERVER['QUERY_STRING'];
		$breadcrumb=$this->breadcrumb();
		$menu=array(
			''=>array('url'=>$this->url_root.axs_url($this->url, array()), 'label'=>$this->editor->tr('article_structure.lbl'), 'act'=>axs_get($this->key_t, $_GET, ''), ),
			);
		$this->url[$this->key_structure_edit]='ee';
		$add_form=$this->list_form_elements_addform('element_add', $this->permission['add'], array(
				$this->key_add=>'element', 'after'=>'', 'b'=>$this->b,
				));
		$html=
		'		<nav>'."\n".
		$breadcrumb.'<br />'."\n".
		'			<a class="right" href="'.$this->url_root.axs_url($this->url, array($this->key_structure_edit=>'tr')).'">'.$this->editor->tr('tr.lbl').'</a>'."\n".
		axs_admin::menu_build($menu, ' ', array('class'=>'tabs')).
		'		</nav>'."\n".
		'     <table id="header_edit" class="table selectable">'."\n".
		'      <caption class="visuallyhidden">'.$breadcrumb.'</caption>'."\n".
		'      <tr><th class="fieldset">'.$this->editor->tr('fieldset_lbl').'</th><th class="id">'.$this->editor->tr('id_lbl').'</th><th class="nr">'.$this->editor->tr('nr_lbl').'</th><th class="field">'.$this->editor->tr('field_lbl').'</th><th class="label">'.$this->editor->tr('label_lbl').'</th><th class="type">'.$this->editor->tr('type_lbl').'</th><th class="options">'.$this->editor->tr('options_lbl').'</th><th class="visible">'.$this->editor->tr('visible_lbl').'</th><th class="export">'.$this->editor->tr('export_lbl').'</th><th class="comment">'.$this->editor->tr('comment_lbl').'</th><th class="txt">'.$this->editor->tr('txt_lbl').'</th><th class="edit">'.$add_form.'</th></tr>'."\n";
		foreach ($this->header->structure_get() as $k=>$v) {
			$v['field']=$k;
			foreach (array('label','comment','msg') as $vv) $v[$vv]=axs_html_safe(axs_get($vv, $v, ''));
			if ($v['type']=='fieldset_end') continue;
			$fieldset='';
			if (!$v['fieldset']) $fieldset='<th>'.$v['fieldset'].'</th>';
			else {
				if ($v['type']==='fieldset') {
					$fieldset='<th rowspan="'.(count($this->header->structure_get($k))+1).'" class="left">'.$v['nr'].'.'.$v['label'].'</th>';
					$v['nr']='';
					}
				}
			$v['url']=$this->url_root.axs_url($this->url, array($this->key_id=>$v['id'], 'b'=>$this->b.'#id'.$v['id']));
			$v['options']=$this->list_form_elements_options($v);
			$v['visible']=implode(', ', $v['visible']);
			$add_form=$this->list_form_elements_addform('element_add', $this->permission['add'], array(
				$this->key_id=>0, $this->key_add=>'element', 
				'nr'=>intval($v['nr'])+1,
				'after'=>$k, 'fieldset'=>$v['fieldset'], 'type'=>$v['type'], 'b'=>$this->b.'#id'.$v['id'], 
				));
			$v['disable']=($v['disable']) ? ' disable':'';
			if (!$v['export']) $v['export']='';
			$html.='      <tr id="id'.$v['id'].'" class="top'.$v['disable'].'">'.$fieldset.'<td class="id"><a href="'.$v['url'].'">'.$v['id'].'</a></td><td class="nr">'.$v['nr'].'</td><td class="field">'.$k.'</td><td class="label">'.$v['label'].'</td><td class="type">'.$v['type'].'</td><td class="options">'.$v['options'].'</td><td class="visible">'.$v['visible'].'</td><td class="export">'.$v['export'].'</td><td class="comment">'.$v['comment'].'</td><td class="txt">'.$v['txt'].'</td><td class="edit">'.$add_form.'</td></tr>'."\n";
			}
		return $html.'     </table>';
		} #</list_form_elements()>
	function list_form_elements_addform($name, $enabled, $url=array()) {
		$url['axs']['form_id']=$this->form_id;
		$add_url=$this->url_root.axs_url($this->url, $url).'#header_edit';
		$enabled=($enabled) ? '':' disabled="disabled"';
		return '<form action="'.$add_url.'" method="post" class="addform"><input name="'.$name.'" type="submit" value="'.$this->editor->tr($name.'_lbl').'"'.$enabled.' /></form>';
		} #</list_form_elements_addform()>
	function list_form_elements_options(&$v, $level=1) {
		$options='';
		if (!empty($v['lang-multi'])) $options=implode(', ', array_keys((array)$v['lang-multi']));
		if (!isset($this->types[$v['type']]['options_max'])) return $options;
		
		if (!$this->types[$v['type']]['options_max']) $v['options_add']=true;
		else {
			$v['options_add']=(count((array)$v['options'])>=$this->types[$v['type']]['options_max']) ? false:true;
			}
		if ($v['type']=='category') {
			$v['url_add']=array('level'=>$level, );
			$this->header->values[$v['field'].'_0']=$v['id'];
			for ($nr=1; $nr<=$level; $nr++) $v['url_add']['level'.$nr]=$this->header->values[$v['field'].'_'.($nr-1)];
			$v['url_add']=array_merge(
				array($this->key_id=>0, $this->key_add=>'option', $this->key_parent=>$v['id']),
				$v['url_add'],
				array('nr'=>1, 'b'=>$this->b.'#id'.$v['id'], )
				);
			$v['add_form']=$this->list_form_elements_addform('option_add', true, $v['url_add']);
			$add_form=(!isset($v['options'][$level][$this->header->values[$v['field'].'_'.$level]])) ? true:false;
			if ($add_form) $options.='<li>'.$v['add_form'].'</li>'."\n";
			foreach ((array)$v['options'][$level] as $kkk=>$vvv) {//foreach ((array)$vv as $kkk=>$vvv) {
				foreach (array('label','comment') as $vvvv) $vvv[$vvvv]=axs_html_safe($vvv[$vvvv]);
				$vvv['url']=$this->url_root.axs_url($this->url, array($this->key_id=>$vvv['id'], 'b'=>$this->b.'#id'.$v['id']));
				$vvv['url_open']=$this->url;
				for ($nr=1; $nr<$level; $nr++) $vvv['url_open'][$vvv['field'].'_'.$nr]=$vvv['level'.($nr+1)];
				$vvv['url_open']=$this->url_root.axs_url($vvv['url_open'], array($vvv['field'].'_'.$level=>$vvv['id'], )).'#id'.$vvv['id'];
				$vvv['class']=array();
				if ($vvv['disable']) $vvv['class'][]='disabled';
				$vvv['sub']='';
				if ($level<$this->header->category_levels) {
					if ($this->header->values[$vvv['field'].'_'.$level]==$vvv['id']) {
						$vvv['class'][]='act';
						$vvv['label']='<em>'.$vvv['label'].'</em>';
						$vvv['sub']=$this->list_form_elements_options($v, $level+1);
						}
					$vvv['label']='<a href="'.$vvv['url_open'].'">'.$vvv['label'].'</a>';
					}
				$vvv['class']=(!empty($vvv['class'])) ? ' class="'.implode(' ', $vvv['class']).'"':'';
				$v['url_add']['nr']=$vvv['nr']+1;
				$vvv['add_form']=$this->list_form_elements_addform('option_add', true, $v['url_add']);
				$options.='<li id="id'.$vvv['id'].'"'.$vvv['class'].'>'.$vvv['nr'].'.(<a href="'.$vvv['url'].'">id#'.$vvv['id'].'</a>) '.$vvv['label'].$vvv['sub'].'</li>'."\n";
				if ($add_form) $options.='<li>'.$vvv['add_form'].'</li>'."\n";
				}
			return '<ul>'.$options.'</ul>'."\n";
			}
		foreach ((array)$v['options'] as $kk=>$vv) {
			foreach (array('id','comment','nr','field','disable',) as $vvv) if (!isset($vv[$vvv])) $vv[$vvv]='';
			foreach (array('label','comment') as $vvv) $vv[$vvv]=axs_html_safe($vv[$vvv]);
			$vv['class']=($vv['disable']) ? ' class="disabled"':'';
			$vv['url']=$this->url_root.axs_url($this->url, array($this->key_id=>$vv['id'], 'b'=>$this->b.'#id'.$v['id']));
			$vv['url_add']=array($this->key_id=>0, $this->key_add=>'option', $this->key_parent=>$v['id'], 'nr'=>intval($vv['nr'])+1, 'b'=>$this->b.'#id'.$v['id'], );
			$vv['add_form']=$this->list_form_elements_addform('option_add', $v['options_add'], $vv['url_add']);
			$options.='        <tr id="id'.$vv['id'].'"'.$vv['class'].'><td class="id"><a href="'.$vv['url'].'">'.$vv['id'].'</a></td><td class="nr">'.$vv['nr'].'</td><td class="field">'.$vv['field'].'</td><td class="label">'.$vv['label'].'</td><td class="edit">'.$vv['add_form'].'</td></tr>'."\n";
			}
		$vv['url_add']=array($this->key_id=>0, $this->key_add=>'option', $this->key_parent=>$v['id'], 'nr'=>1, 'b'=>$this->b.'#id'.$v['id'], );
		$vv['add_form']=$this->list_form_elements_addform('option_add', $v['options_add'], $vv['url_add']);
		$options="\n".
		'       <table class="table selectable">'."\n".
		'        <tr><th class="id">'.$this->editor->tr('id_lbl').'</th><th class="nr">'.$this->editor->tr('nr_lbl').'</th><th class="col">'.$this->editor->tr('col_lbl').'</th><th class="label">'.$this->editor->tr('label_lbl').'</th><th class="edit">'.$vv['add_form'].'</th></tr>'."\n".$options.'        </table>'."\n".'      ';
		return $options;
		} #</list_form_elements_options()>
	
	function list_form_elements_edit($edit) {
		$edit+=0;
		$tmp=($edit) ? $edit:axs_get($this->key_parent, $_GET)+0;
		$this->header();
		$this->sql_structure_init();
		$this->cl=$cl=axs_db_query("SELECT * FROM `".$this->table."` WHERE `site`='".$this->site_nr."' AND `id`='".$tmp."'", 'row', $this->editor->db, __FILE__, __LINE__);
		$this->b=(isset($_GET['b'])) ? $_GET['b']:$this->url_root.axs_url($this->url, array($this->key_structure_edit=>''), false);
		$this->url['id']=$this->url[$this->key_add]='';
		$this->url['b']=$this->b;
		$this->structure=$this->list_form_elements_edit_structure($cl);
		$this->structure_set($this->structure);
		$this->save=axs_get('save', $_POST);
		$this->cl=$this->form_input(array_merge($_REQUEST, array('id'=>$edit)));
		# <Elements ordering>
		$this->sql_rank_where="`site`='".$this->site_nr."' AND `module`='".addslashes($this->module)."' AND `form`='".addslashes($this->form)."'";
		if ($cl['level']==0) { #<Order root elements>
			$this->sql_rank_where.=' AND `level`=0';
			if ($this->cl['fieldset'] && $this->cl['type']!=='fieldset') { #<Reorder elements inside fieldset but not the fieldset itself />
				$this->sql_rank_where.=" AND `fieldset`='".addslashes($this->cl['fieldset'])."' AND (`type`!='fieldset' AND `field`!='' AND `field` IS NOT NULL)";
				}
			else $this->sql_rank_where.=" AND (`fieldset`='' OR `fieldset` IS NULL OR `type`='fieldset')";
			} #</Order root elements>
		else { #<Order options>
			$this->sql_rank_where.=" AND `parent`='".$cl['parent']."'";
			//if ($cl['type']=='set-checkbox') $this->sql_rank_where.=' AND `level`>0';
			//else $this->sql_rank_where.=' AND `level`='.($cl['level']+0).'';
			for ($nr=1; $nr<=$this->header->category_levels; $nr++) {
				$this->sql_rank_where.=' AND `level'.$nr.'`=\''.(axs_get('level'.$nr, $this->cl)+0).'\'';
				}
			} #</Order options>
		# </Elements ordering>
		# <Error check>
		if ($this->save) $this->validate($this->cl);
		if (!empty($this->msg)) $this->save=false;
		# </Error check>
		#<Add element or option>
		$this->add=(!empty($_GET[$this->key_add])) ? $_GET[$this->key_add]:false;
		if (($this->add) && (!$this->cl['id'])) $this->url[$this->key_add]=$this->add;
		if ($this->add==='element') $this->list_form_elements_edit_element_add();
		if ($this->add==='option') $this->list_form_elements_edit_option_add($cl);
		if (isset($_POST['_delete']) && isset($_POST['_select'][$this->cl['id']])) $this->list_form_elements_edit_element_del($cl);
		#<Add element or option>
		# <Save>
		if ($this->save) {
			$this->cl['nr']=$this->sql_structure->rank_set($this->table, 'nr', $this->cl['id'], $this->cl['nr'], $this->sql_rank_where);
			//exit(dbg($this->table, 'nr', $this->cl['id'], $this->cl['nr'], $this->sql_rank_where));
			axs_db_query($q=array("UPDATE `".$this->table."` SET ".$this->sql_values_set($this->cl, '')."\n".
			"	WHERE `site`='".$this->site_nr."' AND `id`='".$this->cl['id']."'", $this->sql_p), '', $this->editor->db, __FILE__, __LINE__);
			if (empty($this->msg)) exit(axs_redir($this->b));
			} # </Save>
		# <Read editor>
		if (empty($_POST) && $this->cl['id'] && empty($this->msg)) {
			$this->cl=axs_db_query($q="SELECT t.*, u.`user` AS `updated_text`\n".
			"	FROM `".$this->table."` AS t LEFT JOIN `".axs_user::$table."` AS u ON u.`id`=t.`updated_uid`\n".
			"	WHERE t.`site`='".$this->site_nr."' AND t.`id`='".$this->cl['id']."'", 'row', $this->editor->db, __FILE__, __LINE__);
			$this->cl=$this->sql_values_get($this->cl);
			}
		$this->editor->tpl_set($this->editor->name, $this->tpl['form']);
		$this->editor->tpl_set('input_required', $this->templates('required'));
		$this->editor->vars['action_url']=$this->url_root.axs_url($this->url, array($this->key_id=>$this->cl['id'], 'axs'=>[$this->key_form_id=>$this->form_id]));
		$this->editor->vars['back_url']=htmlspecialchars($this->b);
		$this->editor->vars['msg']=$this->msg_html();
		$this->editor->vars['elements']='';
		foreach ($this->structure_get() as $k=>$v) $this->editor->vars['elements'].=$this->element_html($k, $v, $this->cl);
		$this->css_js();
		$this->editor->vars+=$this->editor->tr->tr;
		return $this->editor->finish(); #</Read editor>
		} #</list_form_elements_edit()>
	function list_form_elements_edit_structure(&$cl) {
		global $axs;
		$this->disabled='';
		$f=array();
		foreach (array('level'=>0) as $k=>$v) $cl[$k]=axs_get($k, $cl, $v);
		if ($cl['level']==0) $f['fieldset']=array('type'=>'select', 'size'=>255, 'label'=>$this->editor->tr('fieldset_lbl'), 'value_type'=>'s', 'options'=>$this->list_fieldset(), );
		//else $f['col']=array('type'=>'text', 'label'=>$this->editor->tr('col_lbl'), 'size'=>25, 'readonly'=>'readonly', );
		$element_types=$this->list_form_elements_edit_structure_options('type');
		//foreach ($element_types as $k=>$v) $element_types[$k]=array('value'=>$v, 'label'=>$v, );
		$f+=array(
			'id'=>array('type'=>'number', 'label'=>$this->editor->tr('id_lbl'), 'size'=>10, 'readonly'=>'readonly', ),
			'disable'=>array('type'=>'checkbox', 'label'=>$this->editor->tr('disable_lbl'), 'size'=>1, 'value'=>1, ),
			'nr'=>array('type'=>'number', 'label'=>$this->editor->tr('nr_lbl'), 'size'=>2, ),
			'field'=>array('type'=>'text', 'label'=>$this->editor->tr('field_lbl'), 'size'=>25, 'maxlength'=>255, ),
			'type'=>array('type'=>'select', 'label'=>$this->editor->tr('type_lbl'), 'size'=>255, 'value_type'=>'s', 'options'=>array(''=>array('value'=>'','label'=>''))+$element_types, ),
			'size'=>array('type'=>'number', 'label'=>$this->editor->tr('size_lbl'), 'size'=>3, ),
			'maxlength'=>array('type'=>'number', 'label'=>$this->editor->tr('maxlength_lbl'), 'size'=>5, ),
			);
		if ($cl['level']==0) $f['nr']['required']=1;
		if (empty($cl['id'])) { #<Add new element>
			unset($f['type']['disabled'], $f['size'], $f['maxlength']);
			} #</Add new element>
		else {
			//if (!$this->permission['alter']) foreach (array('field'=>'readonly','type'=>'disabled') as $k=>$v) $f[$k][$v]=$v;
			if ($cl['type']==='number') $f['size2']=array('type'=>'number', 'label'=>$this->editor->tr('number_min_lbl'), 'size'=>3, );
			if ($cl['type']==='password') $f['size2']=array('type'=>'number', 'label'=>$this->editor->tr('pwd_minlength_lbl'), 'size'=>3, );
			if (($cl['type']==='textarea') or ($cl['type']==='wysiwyg')) $f['size2']=array('type'=>'number', 'label'=>$this->editor->tr('rows_lbl'), 'size'=>3, );
			$f['required']=array('type'=>'number', 'label'=>$this->editor->tr('required_lbl'), 'size'=>3, );
			$f['size']=array('type'=>'number', 'label'=>$this->editor->tr('size_lbl'), 'size'=>3, );
			$visible=$this->list_form_elements_edit_structure_options('visible');
			$f['visible']=array('type'=>'set-checkbox', 'label'=>$this->editor->tr('visible_lbl'), 'size'=>255, 'options'=>$visible, );
			//foreach ($attribs as $k=>$v) $f['attributes']['options'][$k]=array('value'=>$k, 'label'=>$k.' ('.$this->editor->tr->t('attributes_'.$k.'_lbl').')', );
			$f['label']=array('type'=>'text', 'label'=>$this->editor->tr('label_lbl'), 'size'=>50, 'maxlength'=>255, 'lang-multi'=>$this->content_langs, );
			$f['abbr']=array('type'=>'text', 'label'=>$this->editor->tr('abbr_lbl'), 'size'=>25, 'maxlength'=>255, 'lang-multi'=>$this->content_langs, );
			$f['comment']=array('type'=>'textarea', 'label'=>$this->editor->tr('comment_lbl'), 'size'=>25, 'maxlength'=>255, 'lang-multi'=>$this->content_langs, );
			$f['msg']=array('type'=>'textarea', 'label'=>$this->editor->tr('msg_lbl'), 'size'=>25, 'maxlength'=>255, 'lang-multi'=>$this->content_langs, );
			$f['txt']=array('type'=>'wysiwyg', 'label'=>$this->editor->tr('txt_lbl'), 'size'=>25, 'rows'=>3, 'lang-multi'=>$this->content_langs, );
			$f['search']=array('type'=>'select', 'label'=>$this->editor->tr('search_lbl'), 'size'=>3, 'maxlength'=>3, 'options'=>array(0=>array('value'=>0, 'label'=>'')), );
			$f['export']=array('type'=>'select', 'label'=>$this->editor->tr('export_lbl'), 'size'=>3, 'options'=>array(''=>array('value'=>'', 'label'=>''), ), );
			$structure=$this->header->structure_get();
			$count=count($structure);
			for ($nr=1; $nr<$count; $nr++) {
				$f['export']['options'][$nr]=array('value'=>$nr, 'label'=>$nr.'.', );
				foreach ($structure as $k=>$v) if (($k!==$this->cl['field']) && (axs_get('export', $v).''===$nr.'')) {
					$f['export']['options'][$nr]['label'].=' '.$v['label'];
					$f['export']['options'][$nr]['disabled']=true;
					}
				}
			$this->search=new axs_form_search();
			foreach ($this->search->search_types as $k=>$v) {
				$f['search']['options'][$k]=array('value'=>$k.'', 'label'=>$k.'. '.$v['fn'].'('.$v['description'].')');
				}
			$attribs=$this->list_form_elements_edit_structure_options('attributes');
			$f['attributes']=array('type'=>'set-checkbox', 'label'=>$this->editor->tr('attributes_lbl'), 'size'=>255, 'options'=>array(), );
			foreach ($attribs as $k=>$v) $f['attributes']['options'][$k]=array('value'=>$k, 'label'=>$k.' ('.$this->editor->tr->t('attributes_'.$k.'_lbl').')', );
			$f['value_type']=array('type'=>'text', 'label'=>$this->editor->tr('value_type_lbl'), 'size'=>1, 'maxlength'=>1,
				'txt'=>' (i=integer, f=float, s=string)');
			$f['value']=array('type'=>'textarea', 'label'=>$this->editor->tr('value_lbl'), 'size'=>70, 'rows'=>3, );
			$f['cfg']=array('type'=>'textarea', 'label'=>$this->editor->tr('cfg_lbl'), 'size'=>100, 'rows'=>3,
				'txt'=>
				'<dl>'.
				'<dt>*</dt>'.
				'<dd>link=m%3Dart_users%26form%3Dart_users%26id%3D{$user_id}&link_target=_blank</dd>'.
				'<dt>file</dt>'.
				'<dd>name_file={&dollar;id}-{&dollar;name_valid}.{&dollar;ext}&amp;name_save={&dollar;name}&amp;accept=image.*&link=_&link_target=popup&<br />'.
				't[t1][type]=jpg&amp;t[t1][q]=90&amp;t[t1][name_base]={&dollar;id}|{&dollar;_parent_id}_{&dollar;_field}.{&dollar;_key}&amp;t[t1][w]=100&amp;t[t1][h]=100&amp;t[t1][dw]=0&amp;t[t1][dh]=0&amp;t[t1][crop_pos]=50%25&amp;t[t1][crop_size]=%3A&amp;t[t1][alt]={$id}.t1.{$ext}</dd>'.
				'<dt>text-join</dt>'.
				'<dd>f[field1][module]=art_users&amp;f[field1][form]=form&amp;f[field1][table]=table&amp;f[field1][table_alias]=t&amp;f[fstname][join_col]=[alias.col]&amp;<br />'.
				'f[field2][module]=module&amp;f[field2][form]=form&amp;f[field2][table]=table&amp;f[field2][table_alias]=t&amp;f[field2][join_col]=[alias.col]&amp;<br />'.
				'concat=t.field1,\' \',t.field2&amp;limit=25&<br />'.
				'data-copy=name%3Dkl.fstname,\' \',kl.lstname%26address%3Dkl.address%26bank%3Dkl.bank&<br />'.
				'data-cols=aut%3Daut.name%tech%3Dt.tech%26year%3Dt.year_1,t.year_2&<br />'.
				'</dd>'.
				'<dt>radio</dt>'.
				'<dt>select</dt>'.
				'<dd>options_get[class]=className&amp;options_get[fn]=classMethod<br />'.
				'options_get[sql][t]={&dollar;px}table&amp;options_get[sql][value]=col_value&amp;options_get[sql][label]=col_label_{&dollar;l}<br /><em>Optional</em>: &amp;options_get[sql][id]=col_id&amp;options_get[sql][abbr]=col_abbr&amp;options_get[sql][db]=2</dd>'.
				'</dl>', );
			}
		$f['save']=array('type'=>'submit', 'label'=>$this->editor->tr('save_lbl'), 'comment'=>$this->editor->tr('del_lbl'), 'add_fn'=>(!empty($this->cl['id'])) ? true:false, );
		if ($this->disabled) $f['save']['disabled']='disabled';
		#<Delete functionality>
		$tmp=' disabled="disabled"';
		if ($this->permission['add'] or $cl['level']) $tmp='';
		if ($this->disabled) $tmp='';
		#</Delete functionality>
		$f['updated']=array('type'=>'timestamp-updated', 'label'=>$this->editor->tr('updated_lbl'), );
		return $f;
		} #</list_form_elements_edit_structure()>
	function list_form_elements_edit_structure_options($field) {
		$this->sql_structure_init();
		$cl=$this->sql_structure->list_tables_cols($this->table, $field);
		sort($cl['_value']);
		$a=array();
		foreach ($cl['_value'] as $k=>$v) if ($k) $a[$v]=array('value'=>$v, 'label'=>$v, );
		return $a;
		} #</list_form_elements_edit_structure_options()>
	function list_form_elements_edit_element_add() { #<Add new element>
		if (!$this->save) return;
		if (!$this->permission['add']) return $this->msg[]='No permission for Add element operation!';
		$cl=array();
		$this->cl['site_nr']=$this->site_nr;
		foreach (array('site_nr'=>true,'module'=>true,'form'=>true,'type'=>true,'fieldset'=>false,'field'=>true,'nr'=>false) as $k=>$v) {
			$cl[$k]=($this->$k) ? $this->$k:$this->cl[$k];
			if ($v) {	if (!$cl[$k]) $this->msg[]='"'.$k.'" not specified!';	}
			}
		if (
			axs_db_query("SELECT `id` FROM `".$this->table."` WHERE `site`='".$cl['site_nr']."' AND `module`='".$cl['module']."' AND `form`='".$cl['form']."' AND `level`=0 AND `field`='".$cl['field']."'", 'cell', $this->editor->db, __FILE__, __LINE__)
			) $this->msg[]='Field "'.$cl['field'].'" already exists!';
		if (!empty($this->msg)) return;
		if ($cl['type']=='fieldset') {
			$cl['fieldset']=$cl['field'];
			$cl['field']='';
			}
		$this->cl['id']=axs_db_query('INSERT INTO `'.$this->table.'` SET `site`=\''.$cl['site_nr'].'\', `module`=\''.$cl['module'].'\', `form`=\''.$cl['form'].'\', `fieldset`=\''.$cl['fieldset'].'\', `field`=\''.$cl['field'].'\', `type`=\''.$cl['type'].'\', `nr`=\''.$this->cl['nr'].'\'', 'insert_id', $this->editor->db, __FILE__, __LINE__);
		$this->url[$this->key_id]=$this->cl['id'];
		exit(axs_redir($this->url_root.axs_url($this->url, array($this->key_structure_edit=>'ee', ), false)));
		} #</list_form_elements_edit_element_add()>
	function list_form_elements_edit_option_add(&$cl) { #<Add option>
		if (!isset($_POST['option_add'])) return;
		$require=array('site'=>'site','module'=>'module','form'=>'form','type'=>'type','field'=>'field','parent'=>'id','level'=>'level',);
		$cl['level']=1;
		$levels='';
		if ($cl['type']=='category') {
			$require[]='level1';
			$cl['level']=intval($_GET['level']);
			for ($nr=1; isset($cl['level'.$nr]); $nr++) {
				$cl['level'.$nr]=intval($_GET['level'.$nr]);
				$levels.=', `level'.$nr.'`='.$cl['level'.$nr];
				}
			$cl['level1']=$cl['id'];
			}
		if ($cl['type']=='set-checkbox' or $cl['type']=='multi-checkbox') {
			$tmp=$this->header->structure[$cl['field']]['options'];
			for($nr=1; $nr<=$this->types['set-checkbox']['options_max']; $nr++) if (!isset($tmp[$nr])) {
				$cl['level']=$nr;
				break;
				}
			}
		foreach ($require as $v) if (!$cl[$v]) return $this->msg[]='"'.$v.'" not specified!';
		$this->cl['id']=axs_db_query('INSERT INTO `'.$this->table.'` SET'."\n".
		'	`site`=\''.$this->site_nr.'\', `module`=\''.$this->module.'\', `form`=\''.$this->form.'\','."\n".
		'	`fieldset`=\''.$cl['fieldset'].'\', `field`=\''.$cl['field'].'\', `parent`=\''.$cl['id'].'\','."\n".
		'	`type`=\''.$cl['type'].'\', `size`=\''.$this->types[$cl['type']]['size'].'\', `nr`=\''.$this->cl['nr'].'\','."\n".
		'	`level`=\''.$cl['level'].'\''.$levels, 'insert_id', $this->editor->db, __FILE__, __LINE__);
		$this->url[$this->key_id]=$this->cl['id'];
		exit(axs_redir($this->url_root.axs_url($this->url, array('action'=>'edit', ), false)));
		} #</list_form_elements_edit_option_add()>
	function list_form_elements_edit_element_del(&$cl) {
		//if (!$this->permission['add']) return $this->msg[]='No permission for Delete operation!';
		if (!$id=$cl['id']+0) return $this->msg[]='Bad ID, Could not delete entry!';
		#<Find table name>
		$this->tables=$this->sql_structure->list_tables('', $this->editor->db.$cl['module'].'%');
		$table=false;
		$find=array();
		if ($cl['type']=='table') $find+=array($cl['module'].'_'.$cl['form'].'_'.$cl['field']=>'', $cl['module'].'_'.$cl['field']=>'', );
		$find+=array($cl['module'].'_'.$cl['form'], $cl['module']=>'', );
		foreach ($find as $k=>$v) {
			if (isset($this->tables[$this->editor->db.$k])) {	$table=$this->editor->db.$k;	break;	}
			}
		#</Find table name>
		$del_where='';
		if ($table && $cl['type']=='set-checkbox') {
			axs_db_query('UPDATE `'.$table.'` SET `'.$cl['field'].'`=REPLACE(`'.$cl['field'].'`,\''.$cl['level'].'\',\'\')', '', $this->editor->db, __FILE__, __LINE__);
			}
		if ($cl['level']>0) { #<Delete option>
			if ($cl['type']=='category') {
				if (isset($cl['level'.($cl['level']+1)])) {
					$del_where=' AND (`id`='.$id.' OR `level'.($cl['level']+1).'`='.$id.')';
					}
				if ($table) {
					$result=axs_db_query($q='SELECT `id`, `count`.* FROM `'.$table.'`,'."\n".
					'	(SELECT COUNT(*) AS `total` FROM `'.$table.'` WHERE `'.$cl['field'].'_'.$cl['level'].'`=\''.$id.'\') AS `count`'."\n".
					'	WHERE `'.$cl['field'].'_'.$cl['level'].'`=\''.$id.'\' LIMIT 10', 1, $this->editor->db, __FILE__, __LINE__);
					if (!empty($result)) {
						$msg=$this->editor->tr('msg_del_option');
						foreach ($result as $v) $msg.=' '.$v['id'].',';
						return $this->msg['msg_del_option']=$msg.' ...('.$v['total'].')';
						}
					}
				}
			else {
				$del_where=' AND `id`=\''.$id.'\'';
				}
			} #</Delete option>
		axs_db_query('DELETE FROM `'.$this->table.'` WHERE `site`=\''.$this->site_nr.'\' AND `module`=\''.$cl['module'].'\' AND `form`=\''.$cl['form'].'\' AND `field`=\''.$cl['field'].'\''.$del_where, '', $this->editor->db, __FILE__, __LINE__);
		$this->sql_structure->rank_set($this->table, 'nr', $id, 0, $this->sql_rank_where);
		exit(axs_redir($this->b));
		} #</list_form_elements_edit_element_del()>
	function list_form_elements_edit_translations() {
		$this->url[$this->key_structure_edit]='tr';
		$th=array('id'=>array('t'=>''), 'module'=>array('t'=>''), 'form'=>array('t'=>''), 'field'=>array('t'=>'th'), );
		foreach ($th as $k=>$v) $th[$k]['lbl']=$this->editor->tr('tr.'.$k.'.lbl');
		$th=array(''=>$th, 'tr'=>$th, );
		$txt=array('label'=>'textarea','abbr'=>'text','comment'=>'textarea','txt'=>'textarea','msg'=>'textarea',);
		$sql_fields=array();
		foreach ($txt as $k=>$v) foreach ($this->content_langs as $kk=>$vv) $th[''][$k.'_'.$kk]=$sql_fields[$k.'_'.$kk]=array('t'=>$v, 'lbl'=>$this->editor->tr('tr.'.$k.'.lbl').' '.$kk, );
		foreach ($this->content_langs as $kk=>$vv) $th['tr']['label_'.$kk]=$th['']['label_'.$kk];//array('t'=>'textarea', 'lbl'=>$this->editor->tr('tr.label.lbl').' '.$kk, );
		$th['tr']['_del']=array('t'=>'checkbox', 'lbl'=>$this->editor->tr('tr.del.lbl'), );
		$files=array();
		foreach (glob(axs_dir('tmp').'*.tr.php') as $v) $files[basename($v)]=basename($v);
		#<Save>
		if (isset($_POST['save'])) {
			$input=$_POST;
			#<Delete _tr key />
			foreach ($input['id'] as $id=>$cl) if (!empty($cl['_del'])) {
				axs_db_query("DELETE FROM `".$this->table."` WHERE `site`='".$this->site_nr."' AND `module`='".$this->module."' AND `type`='_tr' AND `id`='".intval($id)."' LIMIT 1", '', $this->editor->db, __FILE__, __LINE__);
				unset($input['id'][$id]);
				}
			#<Add _tr key />
			if (!empty($input['id'][0]['field']['val'])) {
				$id=$this->list_form_elements_edit_translations_add($input['id'][0]['field']['val']);
				if (!isset($input['id'][$id])) $input['id'][$id]=$input['id'][0];
				unset($input['id'][0]);
				}
			#<Import .tr.php file />
			foreach ($this->content_langs as $k=>$v) if (isset($files[$_POST['import_php'][$k]])) {
				$a=include(axs_dir('tmp').$_POST['import_php'][$k]);
				foreach ($a as $kk=>$vv) {
					$id=$this->list_form_elements_edit_translations_add($kk);
					$input['id'][$id]['label_'.$k]['val']=$vv;
					}
				}
			#<Save all />
			foreach ($input['id'] as $id=>$cl) {
				$sql=array();
				foreach ($sql_fields as $k=>$v) if (isset($cl[$k]['val'])) {
					if (($cl[$k]['val']) || (!empty($cl[$k]['empty']))) $sql[$k]="`".$k."`='".addslashes($cl[$k]['val'])."'";
					}
				if ($sql) axs_db_query($q="UPDATE `".$this->table."` SET ".implode(', ', $sql)."\n".
				"	WHERE `site`='".$this->site_nr."' AND `module`='".$this->module."' AND (`form`='' OR `form`='".$this->form."')  AND `id`='".intval($id)."'", '', $this->editor->db, __FILE__, __LINE__);
				}
			axs_exit(axs_redir('?'.$_SERVER['QUERY_STRING']));
			} # </Save>
		#</Save>
		#<Read data />
		$r=axs_db_query("SELECT * FROM `".$this->table."`\n".
		"	WHERE `site`='".$this->site_nr."' AND `module`='".$this->module."' AND (`form`='' OR `form`='".$this->form."')\n".
		"	ORDER BY `module`, `form`, `fieldset`, `nr`, `field`", 'k', $this->db, __FILE__, __LINE__);
		$table=array('cfg'=>array(), 'elements'=>array(), 'tr'=>array(), );
		$opts=array();
		foreach ($r as $id=>$cl) if (($cl['type']) && ($cl['parent'])) {
			$opts[$cl['parent']][$id]=$cl;
			unset($r[$id]);
			}
		foreach ($r as $id=>$cl) {
			if (!$cl['type']) $table['cfg'][$id]=$cl;
			if (($cl['type']) && ($cl['type']!=='_tr')) {
				$table['elements'][$id]=$cl;
				if (isset($opts[$id])) foreach ($opts[$id] as $k=>$v) $table['elements'][$k]=array_merge($v, array('field'=>$cl['field'].'.'.$v['field']));
				}
			if ($cl['type']==='_tr') $table['tr'][$id]=$cl;
			}
		foreach ($cl as $k=>$v) if ($k!=='module') $cl[$k]='';
		$table['tr'][0]=array_merge($cl, array('form'=>$this->form));
		$this->editor->tpl_set($this->editor->name, $this->tpl['form']);
		$this->editor->tpl_set('input_required', $this->templates('required'));
		$this->editor->vars['action_url']=$this->url_root.axs_url($this->url, array('axs'=>[$this->key_form_id=>$this->form_id]));
		$this->editor->vars['back_url']=$this->url_root.axs_url($this->url, array($this->key_structure_edit=>''));
		$this->editor->vars['msg']=$this->msg_html();
		$this->editor->vars['elements']=
		'		<table id="edit-tr" class="table selectable">'."\n".
		'			<caption>'.$this->editor->tr('tr.lbl').'</caption>'."\n";
		foreach ($table as $k=>$v) {
			$key=(isset($th[$k])) ? $k:'';
			$thead='';
			foreach ($th[$key] as $kk=>$vv) $thead.='<th>'.$vv['lbl'].'</th>';
			$this->editor->vars['elements'].=
			'				<tr><th colspan="'.count($th[$key]).'">'.$this->editor->tr('tr.'.$k.'.lbl').'</th></tr>'."\n".
			'				<tr>'.$thead.'</tr>'."\n";
			foreach ($v as $id=>$cl) {
				$tr=array();
				foreach ($th[$key] as $kk=>$vv) switch ($vv['t']) {
					case 'checkbox':
						$tr[$kk]='<label><span class="visuallyhidden">'.$this->editor->tr('tr.del.lbl').'</span><input name="id['.$id.']['.$kk.'][del]" type="checkbox" value="1" title="'.$this->editor->tr('tr.empty.lbl').'" /></label>';
						break;
					case 'th':
						$tr[$kk]=axs_html_safe($cl[$kk]);
						break;
					case 'text':
					case 'textarea':
						$input=($vv['t']==='text') ? 
							'<input name="id['.$id.']['.$kk.'][val]" type="text" size="20" maxlength="255" value="'.axs_html_safe($cl[$kk]).'" title="'.$vv['lbl'].'" />':
							'<textarea name="id['.$id.']['.$kk.'][val]" cols="20" rows="1" title="'.$vv['lbl'].'">'.axs_html_safe($cl[$kk]).'</textarea>';
						$tr[$kk]=
						'<label><span class="visuallyhidden">'.$vv['lbl'].'</span>'.$input.'</label>'.
						'<label><span class="visuallyhidden">'.$this->editor->tr('tr.empty.lbl').'</span><input name="id['.$id.']['.$kk.'][empty]" type="checkbox" value="1" title="'.$this->editor->tr('tr.empty.lbl').'" class="empty" /></label>';
						break;
					default: $tr[$kk]=axs_html_safe($cl[$kk]);
					}
				if (!$id) {
					$tr['field']='<label><span class="visuallyhidden">'.$this->editor->tr('tr.field.lbl').'</span><input name="id[0][field][val]" type="text" size="25" maxlength="255" />';
					$opts='<option value="">-</option>';
					foreach ($files as $kk=>$vv) $opts.='<option value="'.htmlspecialchars($vv).'">'.htmlspecialchars($vv).'</option>';
					foreach ($this->content_langs as $kk=>$vv) $tr['label_'.$kk].=' <br /><label><span class="visuallyhidden">'.$this->editor->tr('tr.import.lbl').'</span><select name="import_php['.$kk.']" title="'.$this->editor->tr('tr.import.lbl').'" />'.$opts.'</select></label>';
					}
				foreach ($tr as $kk=>$vv) $tr[$kk]=($th[$key][$kk]['t']==='th') ? '<th class="'.$kk.'" scope="row">'.$vv.'</th>':'<td class="'.$kk.'">'.$vv.'</td>';
				$this->editor->vars['elements'].=
				'				<tr>'."\n".
				implode('					'."\n", $tr)."\n".
				'				</tr>'."\n";
				}
			}
		$this->editor->vars['elements'].=
		'		</table>'.
		'		<input name="save" type="submit" value="'.$this->editor->tr('save_lbl').'" />'."\n".
		'		<script>'."\n".
		'			$("#header_edit #edit-tr input.empty").on("change",function(){'."\n".
		'				var input=$(this).closest("td").find("input[type=\"text\"], textarea");'."\n".
		'				if(this.checked) {'."\n".
		'					this.setAttribute("data-val",$(input).val());'."\n".
		'					$(input).val("");'."\n".
		'					}'."\n".
		'				else $(input).val(this.getAttribute("data-val"));'."\n".
		'				});'."\n".
		'		</script>';
		$this->css_js();
		$this->editor->vars+=$this->editor->tr->tr;
		return $this->editor->finish(); #</Read editor>
		} #</list_form_elements_edit_translations()>
	function list_form_elements_edit_translations_add($key) {
		$id=axs_db_query("SELECT `id` FROM `".$this->table."` WHERE `site`='".$this->site_nr."' AND `module`='".$this->module."' AND `type`='_tr' AND `field`='".addslashes($key)."' LIMIT 1", 'cell', $this->editor->db, __FILE__, __LINE__);
		if (!$id) {
			$id=axs_db_query("INSERT INTO `".$this->table."` SET `site`='".$this->site_nr."', `module`='".$this->module."', `form`='', `field`='".addslashes($key)."', `type`='_tr'", 'insert_id', $this->editor->db, __FILE__, __LINE__);
			}
		return $id;
		} #</list_form_elements_edit_translations_add()>
	#</Editor functions ----------------------------------------------- >
	
	#<Misc. functions ----------------------------------------------- >
	function header($get=false) {
		$this->header=new axs_form_header(array('site_nr'=>$this->site_nr, 'url'=>$this->url, 'visibility'=>array()));
		if (!$get) $this->header->sql_header_get($this->module, $this->form, false, $_REQUEST);
		else $this->header->header=$this->header->sql_header_modules_list();
		} #</header()>
	# <Language functions ----------------------------------------------- >
	function lang_add($l) {
		global $axs;
		$this->sql_structure_init();
		if (!$this->sql_structure->table_col_exists($this->editor->px.'header')) return;
		# <Get languages from all sites />
		$langs=array();
		foreach ($axs['cfg']['site'] as $k=>$v) foreach ($v['langs'] as $kk=>$vv) $langs[$kk]=$vv;
		# <Alter DB header />
		foreach (array('label'=>'varchar(255)','comment'=>'varchar(255)','txt'=>'text','msg'=>'varchar(255)',) as $k=>$v) if (!$this->sql_structure->table_col_exists($this->editor->px.'header', $k.'_'.$l)) {
			$after='';
			foreach ($langs as $kk=>$vv) {
				if ($this->sql_structure->table_col_exists($this->editor->px.'header', $k.'_'.$kk)) $after=$k.'_'.$kk;
				}
			$this->sql_structure->table_alter_add($this->editor->px.'header', array($k.'_'.$l=>array('after'=>$after, 'Type'=>$v), ));
			# <Copy column content from default language />
			axs_db_query('UPDATE `'.$this->editor->px.'header` SET `'.$k.'_'.$l.'`=`'.$k.'_'.$this->l_content.'`', 0, $this->editor->db, __FILE__, __LINE__);
			}
		# <Alter tables that have multi-language fields />
		foreach ($this->lang_fields() as $k=>$v) {
			foreach ($this->content_langs as $kk=>$vv) if ($this->sql_structure->table_col_exists($v['table'], $v['field'].'_'.$kk)) $vv['after']=$v['field'].'_'.$kk;
			if (!$this->sql_structure->table_col_exists($v['table'], $v['field'].'_'.$l)) {
				$this->sql_structure->table_alter_add($v['table'], array($v['field'].'_'.$l=>array('after'=>$vv['after'], 'Type'=>$v['Type']), ));
				# <Copy column content from default language />
				axs_db_query('UPDATE `'.$v['table'].'` SET `'.$v['field'].'_'.$l.'`=`'.$v['field'].'_'.$this->l_content.'`', 0, $this->editor->db, __FILE__, __LINE__);
				}
			}
		} #</lang_add()>
	function lang_del($l) {
		$this->sql_structure_init();
		if (!$this->sql_structure->table_col_exists($this->editor->px.'header')) return;
		# <Alter tables that have multi-language fields />
		foreach ($this->lang_fields() as $k=>$v) if ($this->sql_structure->table_col_exists($v['table'], $v['field'].'_'.$l)) {
			$this->sql_structure->table_alter_del($v['table'], $v['field'].'_'.$l);
			}
		# <Alter DB header />
		foreach ($this->txt as $v) if ($this->sql_structure->table_col_exists($this->editor->px.'header', $v.'_'.$l)) {
			$this->sql_structure->table_alter_del($this->editor->px.'header', $v.'_'.$l);
			}
		} #</lang_del()>
	function lang_fields() {
		$this->sql_structure_init();
		$tables=array();
		foreach (axs_db_query("SELECT `module`, `form`, `field` FROM `".$this->editor->px."header` WHERE `site`='".$this->site_nr."' AND FIND_IN_SET('lang-multi', `attributes`)>0", 1, $this->editor->db, __FILE__, __LINE__) as $cl) {
			$table=false;
			if ($this->sql_structure->table_col_exists($tmp=$this->editor->px.$cl['module'].'_'.$cl['form'])) $table=$tmp;
			else {
				if ($this->sql_structure->table_col_exists($tmp=$this->editor->px.$cl['module'])) $table=$tmp;
				}
			if ($table) {
				$cl['Type']=$this->sql_structure->list_tables_cols($table, $cl['field'].'_'.$this->l);
				$tables[]=array('table'=>$table, 'field'=>$cl['field'], 'Type'=>$cl['Type']['Type'], );
				}
			}
		return $tables;
		} #</lang_fields()>
	# </Language functions ---------------------------------------------- >
	#<SQL structure --------------------------------------------------------- />
	function sql_structure_init() {
		if (!isset($this->sql_structure)) {
			$this->sql_structure=new axs_sql_structure($this->editor->db);
			return true;
			}
		else return false;
		} #</sql_structure_init()>
	} #</class::axs_db_header_edit>
#'1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31', '32', '33', '34', '35', '36', '37', '38', '39', '40', '41', '42', '43', '44', '45', '46', '47', '48', '49', '50', '51', '52', '53', '54', '55', '56', '57', '58', '59', '60', '61', '62', '63', '64'
#UPDATE axs_header AS h, axs_header_tmp AS t SET h.cfg=t.cfg WHERE h.id=t.id
#2010-03-10 ?>