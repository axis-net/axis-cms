<?php #2020-12-11
class axs_tab {
	static $index_meta=array('publ'=>0, 'hidden'=>1, 'user'=>2, 'updated'=>3, 'pref'=>4, );
	static function parse($data, $tpl=false, $options=array()) {
		# Parse rows with template(s). 
		# $data - a table in x-tab format (will be passed to proc()). 
		# $tpl - row template. Can be array of two templates, key 0 for odd rows and 1 for even rows. 
		# 	$tpl='<div>{$cell1}</div><div>{$cell2}</div>';
		# $options=array(
		# 	'start'=>2,
		#	'limit'=>10, - at which row to start processing data, how many rows to process. The first row is nr 1, not 0! 
		# 	'date_format'=>'d.m.y H:i:s' - date and time format for PHP date() function. Default is 'd.m.Y H:i' (eg. 30.04.2006 11:35) 
		# 	);
		# 	Options will also be passed to proc() if applicable. 
		if (is_string($tpl)) $tpl=array(0=>$tpl, );
		if (empty($options['start'])) $options['start']=1;
		if (empty($options['limit'])) $options['limit']=999999999;
		$stop=$options['start']+$options['limit']-1;
		if (!isset($options['time'])) $options['time']=time();
		if (!isset($options['header'])) $options['header']='cell';
		if (empty($options['date_format'])) $options['date_format']='d.m.Y H:i';
		$a=1;
		$a1=(!empty($tpl[1])) ? 1:0;
		$dataout='';
		$data=self::proc($data, $options);
		$rownr=0;
		foreach($data as $row) { #<cycle over all lines>
			$rownr++;
			if ($rownr>$stop) break;
			if ($rownr<$options['start']) continue;
			$cells=array('rownr'=>$rownr, );
			$cells['publ']=(!empty($row[0]['publ'])) ? date($options['date_format'], $row[0]['publ']):'';
			$cells['hidden']=(!empty($row[0]['hidden'])) ? date($options['date_format'], $row[0]['hidden']):'';
			unset($row[0]);
			$cells+=$row;
			$a=($a) ? 0:$a1;
			$dataout.=($tpl) ? axs_tpl_parse($tpl[$a], $cells):implode('', $cells);
			} #</cycle over all lines>
		return $dataout;
		} #</parse()>
	static function proc($data, $options=array()) {
		# Split an x-tab table and process row data. 
		# REQUIRES:
		# $data - a table in x-tab format (string or array) 
		# 	'<?php #|1234567890,,username,,|row1 cell1 content|row1 cell2 content|? >,
		# 	<?php #|1234567912,,username,,|row2 cell1 content|row2 cell2 content|? >',
		# or a fetched database result with associative keys
		# 	array(
		# 		1=>array('cell1'=>'row1 cell1 content', 'cell2'=>'row1 cell2 content', ),
		# 		2=>array('cell1'=>'row2 cell1 content', 'cell2'=>'row2 cell2 content', ),
		# 		);
		# $options=array(
		# 	'header'=>'cell', - column names will be "cell1", "cell2", ...
		# 	'header'=>0, - column names are taken from first row's content 
		# 	'header'=>array('col1'=>2, 'col2'=>3, ), - give columns directly 
		# 	'time'=>time(), - time in UNIX timestamp format. If given, unpublished table rows are removed.
		#	'merge'=>"\n" - string separator to implode result. If none given array is returned. 
		#	'rem_col'=>0 - remove col nr X. 
		# 	);
		global $axs;
		#if (!isset($options['time'])) $options['time']=$axs['time'];
		if (!is_array($data)) $data=self::proc_split_tab($data);//preg_split('/\r|\n/', $data, null, PREG_SPLIT_NO_EMPTY);
		if (empty($data)) return (isset($options['merge'])) ? '':array();
		reset($data);
		if (!isset($options['header'])) $th=false;
		else {
			if (is_int($options['header'])) {
				$key=key($data);
				if (!is_array($data[$key])) $data[$key]=self::proc_split_tab_row($data[$key]);
				$th=array();
				foreach ($data[$key] as $k=>$v) if ($k!==0) $th[$v]=$k;
				unset($data[$key]);
				}
			else $th=$options['header'];
			}
		$table=array();
		$rownr=1;
		foreach($data as $key=>$row) { #<cycle over all lines>
			if (!is_array($row)) $row=self::proc_split_tab_row($row, $rownr);
			if (isset($options['time'])) {
				$tmp=array(
					'publ'=>(isset($row[0]['publ'])) ? $row[0]['publ']:axs_get('publ', $row),
					'hidden'=>(isset($row[0]['hidden'])) ? $row[0]['hidden']:axs_get('hidden', $row),
					);
				if ($tmp['publ']>$options['time']) continue;
				if (($tmp['hidden']) && ($tmp['hidden']<$options['time'])) continue;
				}
			if (isset($options['rem_col'])) unset($row[$options['rem_col']]);
			if ($th!==false) {
				$tmp=array();
				if (is_array($th)) foreach ($th as $k=>$v) $tmp[$k]=$row[$v];
				else foreach ($row as $k=>$v) if ($k!==0) {	$tmp[$th.$k]=$v;	unset($row[$k]);	}
				$row=$tmp;
				}
			if (!empty($options['merge'])) {
				if ((isset($row[0])) && (is_array($row[0]))) $row[0]=implode(',', $row[0]);
				$row=implode($options['merge'], $row);
				}
			$table[$rownr]=$row;
			$rownr++;
			} #</cycle over all lines>
		if (!empty($options['merge'])) return implode("\n", $table);
		return $table;
		} #</proc()>
	static function proc_split_tab($table, $split_rows=0) { #<Make data an array of rows />
		if (!is_array($table)) {
			$table=preg_split('~'."\n".'~', '-'."\n".$table, -1, PREG_SPLIT_NO_EMPTY);
			unset($table[0]);
			foreach ($table as $k=>$v) $table[$k].="\n";
			}
		if ($split_rows) foreach ($table as $k=>$cl) if (!is_array($cl)) $table[$k]=self::proc_split_tab_row($cl, $split_rows);
		return $table;
		} #</proc_split_tab()>
	static function proc_split_tab_row($row, $rownr=1) { #<Split row from string />
		$row=explode('|', $row);
		array_pop($row); #<array_pop() to remove last (PHP end tag) />
		array_shift($row); #<remove  first cell (PHP start tag) />
		$row[0]=self::proc_split_tab_row_meta(axs_get(0, $row), $rownr);
		return $row;
		} #</proc_split_tab_row()>
	static function proc_split_tab_row_meta($meta, $rownr=1, $val=array()) { #<Split row metadata  from string />
		if (!is_array($meta)) {
			$tmp=explode(',', $meta);
			$meta=array();
			foreach (self::$index_meta as $k=>$v) $meta[$k]=axs_get($v, $tmp, '');
			}
		foreach (self::$index_meta as $k=>$v) {
			if (!isset($meta[$k])) $meta[$k]='';
			if (isset($val[$k])) $meta[$k]=$val[$k];
			}
		if ($rownr!=1) $meta['pref']='';
		return $meta;
		} #</proc_split_tab_row_meta()>
	static function sort($array, $sort_col, $descending) { #<Sort table array />
		$tmp_array=$index=array();
		if (strlen($sort_col)) {
			foreach ($array as $k=>$v) {
				if (!is_array($v)) $v=explode('|', $v);
				$tmp_array[$k]=$v;
				$index[$k]=$v[$sort_col];
				}
			$array=array();
			natcasesort($index);
			foreach ($index as $k=>$v) $array[]=$tmp_array[$k];
			unset($tmp_array, $index);
			}
		else {
			foreach ($array as $k=>$v) {
				$v=explode('|', $v);
				$tmp_array[$k]=$v;
				}
			$array=$tmp_array;
			unset($tmp_array);
			}
		if ($descending) $array=array_reverse($array);	
		return $array;
		} #</sort()>
	} #</class::axs_tab>

#<Legacy wrapper functions />
function axs_tab($data, $tpl='', $tpl1='', $options=array()) {
	return axs_tab::parse($data, $tpl, $options);
	} #</axs_tab()>
function axs_tab_proc($data, $time=false, $merge=false) {
	return axs_tab::proc($data, array('time'=>$time, 'merge'=>$merge));
	} #</axs_tab_proc()>
function axs_tab_sort($array, $sort_col, $descending) {
	return axs_tab::sort($array, $sort_col, $descending);
	} #</axs_tab_sort()>
#2005-05 ?>