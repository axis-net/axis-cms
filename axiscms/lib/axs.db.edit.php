<?php #2022-03-22
class axs_db_edit /*extends axs_admin*/ {
	public $vr=array('tools'=>array(), 'addons'=>'', 'js'=>'', );
	function __construct(&$e, $name, $module=false, $suffix='') {
		global $axs;
		$this->parent=&$e;
		$this->name=$name;
		$this->suffix=$suffix;
		$this->module=$module;
		$this->select=[];
		$this->url=$this->parent->url;
		if (isset($axs['get']['select'])) {
			$this->select=$this->url['axs']['select']=$axs['get']['select'];
			$this->select=json_decode($this->select, true);
			$axs['page']['class']['select']='select';
			}
		$this->_init();
		} #</construct()>
	function _init(){} #</_init()>
	function doc() {
		global $axs;
		return '';
		$o=new axs_doc(array($this->name=>array('dir'=>$this->f_path, )), $this->l);
		if (empty($o->data)) return '';
		$this->vr['doc_link']='<a class="doc" href="'.'?'.axs_url($this->url, array($this->form->key_doc=>$this->name, 'popup'=>'')).$this->form->doc_anchor.'" target="popup">'.$this->form->tr->s('doc_lbl').'</a>';
		if (axs_get($this->form->key_doc, $_GET)===$this->name) {
			$axs['page']['content_class'].=' doc';
			$axs['page']['head']['index.content.css']='<link href="'.AXS_PATH_CMS_HTTP.'index.content.css'.'" rel="stylesheet" type="text/css" media="screen" />'."\n";
			$axs['page']['head'][]='<script>axs.window_resize({"ow":1200});</script>'."\n";
			return $o->contents_html().$o->content_html();
			}
		} # </doc()>
	function header_edit($module=false, $form=false) {
		$this->vr['header_edit']='<a class="header_edit" href="'.'?'.axs_url($this->url, array($this->form->key_structure_edit=>'')).'">'.$this->form->tr->s('header_edit_lbl').'</a>';
		if (!isset($_GET[$this->form->key_structure_edit])) return;
		$o=new axs_form_header_edit($this->url, $this->parent->site_nr, $module, $form);
		return $o->editor();
		} #</header_edit()>
	
	function list_() {
		global $axs;
		$this->tpl=array();
		foreach ($this->templates_list() as $k=>$v) { #<Set custom templates />
			$tmp=($v) ? '.':'';
			if ($f=axs_file_choose(array($this->name.'.'.$this->f.$tmp.$v.'.edit.tpl', $this->name.$tmp.$v.'.edit.tpl', ))) $this->tpl[$k]=file_get_contents($f);
			}
		self::templates_prep($this);
		#<Forms tabs />
		foreach (array('forms'=>array(), '_id'=>0, '_rank'=>'', 'rank+'=>1, '_parent'=>0, ) as $k=>$v) $this->vr[$k]=$v;
		foreach ($this->form->header[$this->name] as $k=>$v) if ($k) {
			if (($this->select) && ($k!==$this->f)) continue;
			$this->vr['forms'][$k]=array('url'=>'?'.axs_url($this->url, array($this->form->key_f=>$k)), 'label'=>$v['']['label'], 'act'=>$this->f, );
			}
		$this->vr['form.title']=$this->vr['forms'][$this->f]['label'];
		$this->vr['forms']=$this->parent->menu_build($this->vr['forms'], '  ', array('class'=>'tabs'));
		$this->tabs=$this->form->cfg('tabs');
		if ($tmp=$this->list_item_tab()) return $tmp; # <Item tab content />
		# Table header
		//$this->thead=$this->form->structure_get_thead();
		//$this->vr['header']=implode('', $this->table_row($this->thead, array(), 'col'));
		# Search
		#$this->search=new axs_form_search($this->form->structure, $_GET, $this->url);
		$this->search->elements('t');
		$this->vr['search']=$this->search->form(null, $this->form->tr->get());
		# Perform database query
		$this->form->sql_result_set($this->list_query(), $this->search);
		#dbg($q);
		# Generate pages links
		$this->vr['total']=$this->form->found_rows;
		$this->search->pager($this->vr['total'], '?', $this->vr);
		
		# Display result rows
		$this->vr['list']='';
		$urls=array();
		foreach ($this->form->sql_result as $cl) {
			$vl=array('_class'=>array(), '_tools'=>array(), '_data'=>'', );
			$this->list_item($cl, $vl);
			$this->list_item_header($vl, $cl);
			foreach ($this->form->structure_get_thead() as $k=>$v) $vl['_data'].=axs_tpl_parse($this->tpl['item.cell'], array('class'=>$k, 'label'=>htmlspecialchars($v['label']), 'value'=>$vl[$k], ));
			$this->vr['list'].=axs_tpl_parse($this->tpl['item'], array_merge($cl, $vl));
			$urls[$cl[$this->form->key_id]]=$cl['back.url'];
			}
		unset($this->search->sql_result);
	$this->vr['edit.url']='?'.axs_url($this->url, array($this->form->key_edit=>'', 'id'=>'', 'axs'=>['form'=>'editform'], 'b'=>'?'.$_SERVER['QUERY_STRING']));
		$this->search->pager->back_url_set('db'.$this->name.'.'.$this->f, $urls);
		$this->vr['add']='<input name="_add_new" type="submit" value="+" />';//'<form action="'.$this->vr['add.url'].'" method="post" class="add"></form>';
		#$this->vr['js']='';
		$this->vr['add.url']='?'.axs_url($this->url, array('id'=>'', $this->form->key_edit=>'', 'b'=>'?'.$_SERVER['QUERY_STRING'].'#id'));
		return axs_tpl_parse($this->tpl[''], $this->vr+$this->form->tr->get());
		} #</list_()>
	function list_item(&$cl, &$vl=array()) {
		$vl['add.url']=array('id'=>'', $this->form->key_edit=>'', );
		$vl['edit.url']=array('id'=>$cl[$this->form->key_id], $this->form->key_edit=>$cl[$this->form->key_id], );
		$cl['back.url']='?'.$_SERVER['QUERY_STRING'].'#id'.$cl[$this->form->key_id];
		foreach ($this->form->structure_get_thead() as $k=>$v) {
			$vl[$k]=$this->form->value_display($k, $v, $cl);
			if (!empty($v['title_item'])) $vl[$k]='<a href="'.'?'.axs_url($this->url, $vl['edit.url']).'">'.$vl[$k].'</a>'; #Edit link
			#$vl['_data'].=axs_tpl_parse($this->tpl['item.cell'], array('class'=>$k, 'label'=>htmlspecialchars($v['label']), 'value'=>$vl[$k], ));
			}
		foreach (array('_id'=>$cl[$this->form->key_id], '_select'=>[], '_add'=>'', 'rank'=>'', 'rank+'=>'', '_parent'=>0, 'add.url'=>'?'.axs_url($this->url, $vl['add.url']), 'edit.url'=>'?'.axs_url($this->url, $vl['edit.url']), 'title.url'=>'?'.axs_url($this->url, $vl['edit.url']), ) as $k=>$v) $vl[$k]=$v;
		foreach ($this->select as $k=>$v) $vl['_select'][$k]=axs_get($v, $cl);
		$vl['_select']=htmlspecialchars(JSON_encode($vl['_select']));
		} #</list_item()>
	function list_item_header(&$vl, &$cl) {
		$vl['_title']=$this->form->structure_get_item_title($cl);
		foreach ($vl['_tools'] as $k=>$v) if (is_array($v)) {
			$v['attribs']=(!empty($v['attribs'])) ? $this->form->attributes($v['attribs']):'';
			$vl['_tools'][$k]='<a href="'.htmlspecialchars($v['url']).'"'.$v['attribs'].'>'.axs_html_safe($v['label']).'</a>';
			}
		$vl['tabs']=$this->list_item_tabs($cl);
		if (count($this->tabs)>1) $vl['_class']['tab']='tab';
		foreach (array('_class'=>'', '_tools'=>'', ) as $k=>$v) if (is_array($vl[$k])) $vl[$k]=implode(' ', $vl[$k]);
		} #</list_item_header()>
	function list_item_tab() {
		global $axs;
		$this->tab=axs_get($this->form->key_t, $_GET, '');
		if (!$this->tab) return;
		$this->vr['tab']=$this->url[$this->form->key_t]=$this->tab;
		$this->id=$this->url['_id']=($tmp=axs_get('_id', $_GET)) ? intval($tmp):'';
		$cl=axs_db_query("SELECT * FROM `".$this->form->sql_table."` WHERE `id`='".$this->id."'", 'row', $this->form->db, __FILE__, __LINE__);
		$this->id=$cl[$this->form->key_id];
		$this->vr['_title']=$this->form->structure_get_item_title($cl);
		$axs['page']['title'][]=strip_tags($this->vr['_title']);
		$this->tab_title=$this->tabs[$this->tab]['label'];
		$this->vr['title']=$axs['page']['title'][]=$this->tabs[$this->tab]['label.html'];
		//foreach ($this->tabs as $k=>$v) if ($k===$this->tab) $axs['page']['title'][]=$v['label.html'];
		$this->vr['tabs']=$this->list_item_tabs($cl);
		$this->vr['content']=$this->list_item_tab_content();
		return axs_tpl_parse($this->templates('item_tab'), $this->vr);
		} #</list_item_tab()>
	function list_item_tab_content() {
		require($this->name.'.'.$this->form.'.'.$this->tab.'.tab.php');
		} #</list_item_tab_content()>
	function list_item_tabs($cl) {
		if (empty($this->tabs)) return '';
		foreach ($tabs=$this->tabs as $k=>$v) {
			if (empty($v['url'])) {
				$v['url']=array();
				$v['url']['t']=$k;
				foreach (array('_id'=>$cl[$this->form->key_id], $this->form->key_t=>$k) as $kk=>$vv) $v['url'][$kk]=($k) ? $vv:false;
				$v['url_fragment']='#id'.$cl[$this->form->key_id];
				}
			if (is_array($v['url'])) {
				foreach ($v['url'] as $kk=>$vv) {
					if (strpos(' '.$vv, '{$')) $vv=axs_tpl_parse($vv, $cl);
					$v['url'][$kk]=$vv;
					}
				$v['url']='?'.axs_url($this->url, $v['url']);
				}
			$fr=($k) ? '':axs_get('url_fragment', $v);
			$tabs[$k]['url']=$v['url'].$fr;
			$tabs[$k]['act']=($k===$this->tab) ? $k:false;
			}
		return axs_admin::menu_build($tabs, ' ', array('class'=>'tabs'));
		} #</list_item_tabs()>
	function list_query() {
		$r=axs_db_query($q=array('SELECT '.$this->form->sql_select_found_rows().$this->form->sql_select('t')."\n".
		'	FROM "'.$this->form->sql_table.'" AS t'."\n".
		'	'.$this->form->sql_tables_join('t')."\n".
		'	'.$this->search->sql_where('WHERE ')."\n".
		'	'.$this->search->sql_order('ORDER BY ').' LIMIT '.$this->search->pager->psize.' OFFSET '.$this->search->pager->start, $this->search->sql_val), 1, $this->form->db, __FILE__, __LINE__);
		#dbg($q, $r);
		return $r;
		} #</list_query();
	function edit() {
		/*$f=$this->f_path.$this->f_px.$this->name;
		if ($this->tab) $f.='.'.$this->f.'.'.$this->tab;
		else {
			if (file_exists($f.'.'.$this->f.'.edit.php')) $f.='.'.$this->f;
			}			
		if (file_exists($f.'.edit.php')) return include($f.'.edit.php');*/
		
		//require_once(axs_dir('plugins').'axs_fn_editform.php');
		//$f=new axs_editform($this, 'et', $this->tr, 'editform', '_edit', $header);
		$this->post=$_POST;
		$this->edit_vars();
		$this->form->url=array_merge($this->url, $this->form->url, array($this->form->key_edit=>$this->form->vl[$this->form->key_id], ));
		$this->form->input_type_text_join_browse();
		$b=$this->search->pager->back_url_get('db'.$this->name.'.'.$this->f, intval($_GET[$this->form->key_edit]), '?'.axs_url($this->url, array($this->form->key_id=>false, )));
		#<Save article>
		$this->form->form_save_add($this->post);
		if ($this->form->save_rank) { #<Change article rank />
			$this->form->sql_rank_step($b, array_merge($this->form->sql_add, array('_parent'=>$_POST['_'][$this->form->save_rank]['_parent']+0)));
			}
		#if ($this->form->save_delete) $this->edit_article_del($this->form->user_input['_select']); #<Delete article />
		if (isset($_POST['_delete'])) $this->form->form_save_del($this->form->vl);
		if ($this->form->save) $this->form->validate($this->form->vl); #<Error check article />
		if (!empty($this->form->msg)) $this->form->save=false;
		if ($this->form->vl[$this->form->key_id]) $this->form->form_save_dir($this->form->vl);
		if ($this->form->save) {
			$this->form->sql_rank_set('', $this->form->rank, $this->form->sql_add, $this->form->vl[$this->form->key_id], axs_get($this->form->rank, $this->form->vl)); #<Change article rank />
			axs_db_query(array('UPDATE "'.$this->form->sql_table.'"'."\n".
			'	SET '.$this->form->sql_values_set($this->form->vl)."\n".
			'	WHERE "'.$this->form->key_id.'"=\''.intval($this->form->vl[$this->form->key_id]).'\''.$this->form->sql_add(' AND ', '', ' AND '), $this->form->sql_p), '', $this->form->db, __FILE__, __LINE__);
			$this->form->form_save_tables($this->form->vl);
			$this->form->form_save_files($this->form->vl);
			#$this->edit_article_files_qr($this->form->vl);
			if (empty($this->form->msg)) axs_exit(axs_redir($b));
			} #</Save article>
		#<Read />
		if (!$this->form->submit) {
			$this->form->cl=axs_db_query($q=array('SELECT '.$this->form->sql_select('t')."\n".
			'	FROM "'.$this->form->sql_table.'" AS t'."\n".
			'	'.$this->form->sql_tables_join('t')."\n".
			'	WHERE t."'.$this->form->key_id.'"=:'.$this->form->key_id,
			array($this->form->key_id=>$this->form->vl[$this->form->key_id], )),
			'row', $this->form->db, __FILE__, __LINE__);
			$this->form->vl=$this->form->sql_values_get($this->form->cl);
			#dbg($q,$this->form->cl,$this->form->vl);
			}
		#<Create elements and parse elements />
		if (!isset($this->form->structure['_submit'])) $this->form->structure['_submit']=array('type'=>'submit', 'value'=>$this->form->tr->s('_submit.lbl'), 'add_fn'=>true, );
		$this->edit_end();
		return $this->form->form_parse_html();
		} #</edit()>
	function edit_vars() {
		$this->form->vr['class']['edit']='edit';
		$this->form->vl=$this->form->form_input($_POST, $_FILES);
		#if ($this->form->vl[$this->form->key_id]) $this->form->vl=array_merge($this->form->vl, $cl);
		if ($this->form->rank) {
			$this->form->vl[$this->form->rank]=intval($this->form->vl[$this->form->rank]);
			if ($this->form->vl[$this->form->rank]<1) $this->form->vl[$this->form->rank]=1;
			}
		} #</edit_vars()>
	function edit_end() {} #</edit_end()>
	function structure($m=false, $f=false) {
		if (!$m) $m=$this->module;
		if (!$f) $f=axs_get($this->form->key_f, $_GET);
		if (!isset($this->form)) $this->form=new axs_form_edit(array('site_nr'=>$this->parent->site_nr, 'url'=>$this->url, 'user_input'=>$_POST, ));
		if ($m) {
			$this->form->sql_header_get($m, $f);
			$this->f=$this->url[$this->form->key_f]=$this->form->f;
			}
		} #</structure()>
	static function table(array $array, array $opts=array()) {
		foreach (array('id'=>'table', 'container'=>true, 'caption'=>false, 'thead'=>true, 'space'=>'', ) as $k=>$v) {	if (!isset($opts[$k])) $opts[$k]=$v;	}
		reset($array);
		$table=($opts['container']===true) ? $opts['space'].'<table class="table">'."\n":$opts['space'].$opts['container'];
		if ($opts['thead']) {
			if ($opts['thead']===true) $opts['thead']=array_combine(array_keys(current($array)), array_keys(current($array)));
			if (is_array($opts['thead'])) {
				$table.=$opts['space'].'	<thead><tr>';
				foreach ($opts['thead'] as $k=>$v) $table.='<th scope="col" class="'.$k.'">'.$v.'</th>';
				$table.='</tr></thead>'."\n";
				}
			else $table.=$opts['thead'];
			}
		foreach ($array as $id=>$cl) {
			$table.=$opts['space'].'	<tr id="'.$opts['id'].$id.'" class="top selectable">'."\n";
			foreach($cl as $k=>$v) $table.=$opts['space'].'		<td class="'.$k.'">'.$v.'</td>'."\n";
			$table.="\n".$opts['space'].'	</tr>'."\n";
			}
		if (strlen($opts['container'])) $table.=$opts['space'].'</table>'."\n";
		return $table;
		} #</array_table()>
	static function table_data(&$fields, &$data, $cols=array(), $colspan=array()) {
		$rows=array();
		$cols_nr=count($cols);
		$fields_nr=count($fields);
		$done=$nr=1;
		$row='';
		foreach ($fields as $k=>$v) {
			$attributes=array();
			foreach ((array)$cols[$nr] as $kk=>$vv) $attributes[$kk]=$kk.'="'.$vv.'"';
			if ($done==$fields_nr) $colspan[$k]=$cols_nr;
			if ($colspan[$k]>1) {
				if ($colspan[$k]>$cols_nr) $colspan[$k]=$cols_nr;
				$tmp=($colspan[$k]+($nr-1)>$cols_nr) ? $cols_nr-($nr-1):$colspan[$k];
				$attributes['colspan']='colspan="'.$tmp.'"';
				$nr+=$tmp;
				}
			else $nr++;
			$attributes=(!empty($attributes)) ? ' '.implode(' ', $attributes):'';
			$row.='<td'.$attributes.'><strong>'.$v['label'].'</strong> '.$data[$k].'</td>';
			if ($nr>$cols_nr) {
				$rows[]=$row;
				$row='';
				$nr=1;
				}
			$done++;
			}
		if ($row) $rows[]=$row;
		return implode("\n".' </tr>'."\n".' <tr>'."\n", $rows);
		} # </table_data()>
	static function table_row($cl, $attribs=array(), $th='') {
		//if ($th=='<cols>') foreach ($cl as $k=>$v) $cl[$k]='<col class="'.$k.'" />';
		foreach ($cl as $k=>$v) {
			if (is_array($v)) {
				if (empty($v['label.html'])) dbg($k,$v);
				$v=$v['label.html'];
				}
			$t=($th) ? 'th':'td';
			$cl[$k]='<'.$t;
			if ($th) $cl[$k].=' scope="'.$th.'"';
			$attribs[$k]['class']=(!empty($attribs[$k]['class'])) ? $k.' '.$attribs[$k]['class']:$k;
			foreach ((array)$attribs[$k] as $kk=>$vv) $cl[$k].=' '.$kk.'="'.htmlspecialchars($vv).'"';
			$cl[$k].='>'.$v.'</'.$t.'>';
			}
		return $cl;
		} # </table_row()>
	
	static function templates($get=false) {
		$tpl=array(
		''=>
			'		<p class="toolbar right">{$tools}</p>'."\n".
			'		{$forms}'."\n".
			'		<div class="tab">'."\n".
	  		'{$search}'."\n".
			'			'."\n".
			'			<div class="pager">'."\n".
			'				<p class="found">{$search.found.lbl} <strong>{$total}</strong>&nbsp;&nbsp;<span class="prev-next">{$prev}{$current}{$next}</span></p>'."\n".
			'				<p class="pages">{$pages}</p>'."\n".
			'			</div>'."\n".
			'{$addons}'.
			'			'."\n".
			'			<form id="list" class="list" action="{$edit.url}" method="post" enctype="multipart/form-data">'."\n".
			'{$data}'.
			'			{$js}'.
			'			</form>'."\n".
			'			<div class="pager">'."\n".
			'				<p class="found">{$search.found.lbl} <strong>{$total}</strong> <span class="prev-next">{$prev}{$current}{$next}</span></p>'."\n".
			'				<p class="pages">{$pages}</p>'."\n".
			'			</div>'."\n".
			'		</div>',
		'add'=>
			'			<fieldset class="add">'."\n".
			'				<legend><abbr title="{$add.lbl}">+</abbr></legend>'."\n".
			'				<button class="article" type="submit" name="_add[{$_id}][article]" title="{$add.article.lbl}">+<img src="'.AXS_PATH_CMS_HTTP.'gfx/icon.ftype.page.png" alt="{$add.article.lbl}" /></button>'."\n".
			'				<label class="batch{$_add_batch.class}">{$add.batch.lbl}<input type="file" name="_add[]" multiple="multiple"{$_add_batch.disabled} /></label>'."\n".
			'				<button class="folder" type="submit" name="_add[{$_id}][folder]" title="{$add.folder.lbl}"{$_add_folder.disabled}>+<img src="'.AXS_PATH_CMS_HTTP.'gfx/icon.ftype.dir.opn.png" alt="{$add.folder.lbl}" /></button>'."\n".
			'				<input type="hidden" name="_[{$_id}][{$_rank}]" value="{$rank+}" />'."\n".
			'				<input type="hidden" name="_[{$_id}][_parent]" value="{$_parent}" />'."\n".
			'{$_addform}'.
			'			</fieldset>'."\n",
		'rank'=>
			'          <span class="rank">'."\n".
			'           <button name="_rank[{$_id}][-]" type="submit" class="up" title="{$rank.up.lbl}">&uarr;</button>'."\n".
			'           {${$_rank}}'."\n".
			'			<input type="hidden" name="_[{$_id}][_rank]" value="{${$_rank}}" />'."\n".
			'           <button name="_rank[{$_id}][+]" type="submit" class="down" title="{$rank.down.lbl}">&darr;</button>'."\n".
			'          </span>'."\n",
		
		'data.list'=>
			'			<h1>{$form.title}</h1>'."\n".
			'{$add}'.
			'{$list}'."\n",
		'data.list.item'=>
			'			<article id="id{$_id}" class="item {$_class}">'."\n".
			'				<h2 class="visuallyhidden">{$_title}</h2>'."\n".
			'{$tabs}'.
			'				<div>'."\n".
			'					<table class="table_data vertical" width="100%" border="1">'."\n".
			'						<caption>'."\n".
			'							<span class="left">'."\n".
			'									<label><input class="select" name="_select[{$_id}]" type="checkbox" value="{$_id}" data-select="{$_select}" /><span class="visuallyhidden" lang="en">select</span></label>'."\n".
			'{$rank}'.
			'								<a class="title" href="{$title.url}" title="id{$_id}"><em><img src="'.AXS_PATH_CMS_HTTP.'gfx/icon.type.{$_type}.png" alt="{$_type}" />{$_title}</em></a>'."\n".
			'							</span>&nbsp;'."\n".
			'							<span class="tools right">'."\n".
			'								{$_tools} <a href="{$edit.url}">{$edit.lbl}</a>'."\n".
			'								<span class="del">'."\n".
			'									<input class="delete" name="_delete" type="submit" value="x" title="{$del_confirm.lbl}" />'."\n".
			'								</span>'."\n".
			'							</span>'."\n".
			'						</caption>'."\n".
			'{$_data}'."\n".
			'					</table>'."\n".
			'				</div>'."\n".
			'			</article>'."\n".
			'{$_add}',
		'data.list.item.cell'=>
			'						<tr class="{$class}">'."\n".
			'							<th scope="row">{$label}</th><td>{$value}</td>'."\n".
			'						</tr>'."\n",
			
		'data.table'=>
			'     <table class="table selectable">'."\n".
			'      <caption>{$form.title}</caption>'."\n".
			'	   <tr><th class="_select"><label><input class="select" name="_select_all" type="checkbox" value="" /><span class="visuallyhidden" lang="en">select</span></label></th>{$thead}<th class="_edit">{$add}</th></tr>'."\n".
			'{$list}'."\n".
			'     </table>',
		'data.table.item'=>
			'         <tr id="id{$_id}" class="{$_class} top">'."\n".
			'			<td class="_select"><label><input class="select" name="_select[{$_id}]" type="checkbox" value="{$_id}" data-select="{$_select}" /><span class="visuallyhidden" lang="en">select</span></label></td>'."\n".
			'{$_data}          <td class="_edit">{$_tools}{$_add}</td>'."\n".
			'         </tr>'."\n",
		'data.table.item.cell'=>
			'          <td class="{$class}">{$value}</td>'."\n",
			
		'item_tab'=>
			'    {$forms}'."\n".
			'	 <div class="tab">'."\n".
			'     <h1 class="visuallyhidden">{$_title}</h1>'."\n".
			'	  {$tabs}'."\n".
			'	  <div id="tab-{$tab}" class="tab">'."\n".
			'     <h2 class="title">{$_title}</h2>'."\n".
			'{$content}'."\n".
			'      </div>'."\n".
			'     </div>',
			);
		if ($get===false) return $tpl;
		if (!is_array($get)) return $tpl[$get];
		foreach ($get as $k=>$v) $get[$k]=$tpl[$v];
		return $get;
		} #</templates()>
	function templates_list() {
		$this->layout=axs_get('layout', $this->form->structure[''], 'list');
		return array(''=>'', 'add'=>'add', 'rank'=>'rank', 'data'=>'data.'.$this->layout, 'item'=>'data.'.$this->layout.'.item', 'item.cell'=>'data.'.$this->layout.'.item.cell', );
		} #</templates_list()>
	static function templates_prep(&$o, array $set=array()) {
		$layout=$o->templates_list();
		//dbg($o->tpl);
		if (empty($set)) $set=$layout;
		foreach ($set as $k=>$v) if (!isset($o->tpl[$k])) $o->tpl[$k]=self::templates((isset($layout[$k])) ? $layout[$k]:$v);
		if (isset($o->tpl['add'])) {
			$tmp=$o->form->structure_get_by_type('file');
			$vr=array('_add_batch.disabled'=>(!empty($tmp)) ? '':' disabled="disabled"', '_add_folder.disabled'=>(!empty($o->folders)) ? '':' disabled="disabled"', '_rank'=>$o->form->rank, );
			$vr['_add_batch.class']=($vr['_add_batch.disabled']) ? ' disabled':'';
			$o->tpl['add']=axs_tpl_parse($o->tpl['add'], $vr);
			}
		if (isset($o->tpl[''])) $o->tpl['']=axs_tpl_parse($o->tpl[''], array('data'=>$o->tpl['data'], 'add'=>$o->tpl['add'], ));
		if (isset($o->tpl['item'])) {
			$vr=array('_add'=>$o->tpl['add'], 'rank'=>(!empty($o->form->rank)) ? $o->tpl['rank']:'', '_rank'=>$o->form->rank, );
			$o->tpl['item']=axs_tpl_parse($o->tpl['item'], $vr);
			}
		$o->vr['tools']=implode(' - ', $o->vr['tools']);
		if (axs_tpl_has($o->tpl[''], 'thead')) $o->vr['thead']=implode('', self::table_row($o->form->structure_get_thead(), array(), 'th'));
		foreach (array('id'=>0, 'rank+'=>1, '_parent'=>(!empty($o->article_parent)) ? $o->article_parent['id']:0, 'list'=>'', '_addform'=>'', ) as $k=>$v) $o->vr[$k]=$v;
		} #</templates_prep()>
	function ui() {
		global $axs;
		#$axs['page']['head']['axs.db.edit.css']='<link type="text/css" rel="stylesheet" href="'.axs_dir('lib', 'h').'axs.db.edit.css'.'" />'."\n";
		if ($tmp=axs_file_choose($this->name.'.module.css', 'http')) $axs['page']['head'][$this->name.'.module.css']='<link type="text/css" rel="stylesheet" href="'.$tmp.'" />'."\n";
		$axs['page']['head']['axs.db.edit.js']='<script src="'.axs_dir('lib', 'h').'axs.db.edit.js'.'"></script>'."\n";
		if ($tmp=axs_file_choose($this->name.'.module.js', 'http')) {
			$axs['page']['head'][$this->name.'.module.js']='<script src="'.$tmp.'"></script>'."\n";
			$this->vr['js'].='<script>axs.'.$this->name.'.init();</script>'."\n";
			}
		#if ($tmp=$this->error()) return $tmp;
		$this->b=(!empty($_REQUEST['b'])) ? $_REQUEST['b']:'?'.axs_url($this->url, array(), '&');
		$this->form=new axs_form_edit(array('site_nr'=>$this->parent->site_nr, 'url'=>$this->url, 'user_input'=>$_POST, ));
		if ($tmp=$this->header_edit()) return $tmp; # <Structure edit />
		if ($tmp=$this->doc()) return $tmp; # <Documentation />
		#<Structure />
		$this->structure();
		$this->search=new axs_form_search($this->form->structure, $_GET, $this->url);
		#<Edit article />
		if (isset($_GET[$this->form->key_edit])) return $this->edit().$this->vr['js'];
		#<Open article />
		#if ($this->article) return $this->display_article_parse($this->article);
		# <Show articles list />
		$this->vr['js']='<script>axs.db=new axs.db("list");</script>'."\n".$this->vr['js'];
		return $this->list_();
		} #</ui()>
	function module_menu() {
		$this->parent->modules[$this->name]['menu']=axs_admin::menu_build($this->menu, '  ', array('class'=>'vertical'), $this->name, $this->act);
		}
	} # </class:axs_db_edit>
#2009-01-04 ?>