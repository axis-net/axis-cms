<?php #2022-03-29 utf-8
return array(
	'search.found.lbl'=>'found',
	'search.order.lbl'=>'order',
	'search.submit.lbl'=>'search',
	'_submit.lbl'=>'submit',
	'msg_value_required'=>'Input required!',
	'msg_value_invalid'=>'Invalid value!',
	'msg_value_unique'=>'Value must be unique!',
	);
#2022-03-29 ?>