<?php #2016-04-24
/* axs.lyoness.config.php
<?php
return array(
	'organization'=>'',
	'checksumCode'=>'',
	'event'=>'',
	'organizationPage'=>'',
	'langs'=>array('et'=>'?l=et', 'en'=>'?l=en', ),
	);
?>
axs.lyoness.php
<?php
require_once('axs.lyoness.class.php');
exit(axs_lyoness::redirect());
?>
*/
class axs_lyoness {
	static $organization='';
	static $checksumCode='';
	static $event='';
	static $organizationPage='';
	static $langs=array();
	static function _init($cfg=array()) {
		if ((!$organization) && (!$cfg) && (file_exists($f=preg_replace('#\.class\.php$#', '.cfg.php', __FILE__)))) $cfg=include($f);
		foreach ($cfg as $k=>$v) if ($v) self::${$k}=$v;
		} #</_init()>
	static function redirect() {
		// Default landing page
		$defaultUrl='http'.((!empty($_SERVER['HTTPS']))?'s':'').'://'.$_SERVER['SERVER_NAME'].'/';
		if ((!empty($_GET['l'])) && (isset(self::$langs[$_GET['l']]))) $defaultUrl.=self::$langs[$_GET['l']];
		// The domain under which this script is installed
		$domain=preg_replace('/^www\./', '', $_SERVER['SERVER_NAME']);
		if (!empty($_GET["tduid"])) {
			setcookie("TRADEDOUBLER", $_GET["tduid"], (time() + 3600 * 24 * 365), "/", '.'.$domain);
			if (!isset($_SESSION)) session_start();
			$_SESSION["TRADEDOUBLER"]=$_GET["tduid"];
			}
		$url=(empty($_GET["url"])) ? $defaultUrl:urldecode(substr(strstr($_SERVER["QUERY_STRING"], "url"), 4));
		header("Location: " . $url);
		} #</redirect()>
	static function tracking_pixel($orderValue, $orderNumber, $reportInfo="", $isSale=true, $currency="EUR") {
		self::_init();
		// Your organization ID; provided by Lyoness
		//
		#$organization = "xxxx";
		$organization=self::$organization;
		// Your checksum code; provided by Lyoness
		//
		#$checksumCode = "xxxx";
		$checksumCode=self::$checksumCode;
		// Value of the sale; sale specific value has to
		// be assigned dynamically out of the shop database
		//
		#$orderValue = "X.XX";
		// Currency of the sale.
		// For example "USD" for US $, or "EUR" for Euro
		//
		#$currency = "EUR";
		// Event ID; provided by Lyoness
		//
		#$event = "xxxx";
		$event=self::$event;
		// Event type:
		// in most cases $isSale should be left as "true" unless
		// you arranged a lead commission with Lyoness
		// true = Sale
		// false = Lead
		//
		$isSale = true;
		// Encrypted connection on this page:
		// true = Yes (https)
		// false = No (http)
		//
		$isSecure = (!empty($_SERVER['HTTPS'])) ? true:false;
		// Here you must specify a unique identifier for the transaction.
		// You should assign your internal shop order number dynamically
		// out of your shop database
		//
		//$orderNumber = "xxxxxxxx";
		// If you do not use the built-in session functionality in PHP, modify
		// the following expressions to work with your session handling routines.
		//
		$tduid = "";
		if (!empty($_SESSION["TRADEDOUBLER"])) $tduid = $_SESSION["TRADEDOUBLER"];
		// OPTIONAL: You may transmit a list of items ordered in the reportInfo
		// parameter. See the chapter reportInfo for details.
		//
		#$reportInfo = "";
		$reportInfo = urlencode($reportInfo);
		
		/***** IMPORTANT: *****/
		/***** In most cases, you should not edit anything below this line. *****/
		if (!empty($_COOKIE["TRADEDOUBLER"]))
		$tduid = $_COOKIE["TRADEDOUBLER"];
		if ($isSale)
		{
		$domain = "tbs.tradedoubler.com";
		$checkNumberName = "orderNumber";
		}
		else
		{
		$domain = "tbl.tradedoubler.com";
		$checkNumberName = "leadNumber";
		$orderValue = "1";
		}
		$checksum = "v04" . md5($checksumCode . $orderNumber . $orderValue);
		if ($isSecure) $scheme = "https";
		else $scheme = "http";
		$trackBackUrl = $scheme . "://" . $domain . "/report"
		. "?organization=" . $organization
		. "&amp;event=" . $event
		. "&amp;" . $checkNumberName . "=" . $orderNumber
		. "&amp;checksum=" . $checksum
		. "&amp;tduid=" . $tduid
		. "&amp;reportInfo=" . $reportInfo;
		if ($isSale)
		{
		$trackBackUrl
		.= "&amp;orderValue=" .$orderValue
		.= "&amp;currency=" .$currency;
		}
		return '<img src="' . $trackBackUrl . '" alt="" style="border:none" />';
		} #</tracking_pixel()>
	static function organization_page_link($lbl) {
		self::_init();
		return '<a href="'.htmlspecialchars(self::$organizationPage).'" class="lyoness" target="_blank"><img src="moodulid/lyoness/lyoness-partner_200.png" alt="'.htmlspecialchars($lbl).'" /></a>';
		} #</organization_page_link()>
	} #</class::axs_lyoness>
#2015-01 ?>