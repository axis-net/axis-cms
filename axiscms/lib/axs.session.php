<?php #2016-10-14
#<Emulate simple session bahavior if PHP's built-in session handling is disabled />
class axs_session {
	static function start($sessid='') {
		global $axs;
		$axs['session_name']='axs_sessid';
		if ($axs['session()']) {
			if (!isset($_SESSION)) session_start();
			$axs['SESSION']=&$_SESSION;
			return '';
			}
		$config=array('session_name'=>$axs['session_name'], 'dir'=>axs_dir('cfg').'tmp/', 'expire'=>1800, );
		if (!$axs['time']) $axs['time']=time();
		if (!$sessid) $sessid=$_REQUEST[$config['session_name']];
		$sessid=preg_replace("#\"|//|'|\..\|\\\|:#", '', $sessid);
		$too_old=$axs['time']-$config['expire'];
		if ($sessid+0<$too_old) $sessid='';
		if (!$sessid) $sessid=$axs['time'].uniqid('-'); # new session ID
		# remove expired sessions
		$handle=opendir($config['dir']);
		while (($file=readdir($handle))!==false) {
			if (strncmp($file, 'session-', 8)==0) {
				if ((substr($file, 8, 10)+0)<$too_old) unlink($config['dir'].$file);
				}
			}
		closedir($handle);
		# read current session
		$f=$config['dir'].'session-'.$sessid.'.php';
		$axs['SESSION']=false;
		if (file_exists($f)) $axs['SESSION']=unserialize(include($f));
		# save session
		else {
			$fp=fopen($f, 'w+');
			fwrite($fp, '<?php return \''.serialize(array()).'\'; ?>');
			fclose($fp);
			}
		if (!$axs['SESSION']) $axs['SESSION']=array();
		if (file_exists($f)) {
			$f=realpath($f);
			register_shutdown_function(create_function('', '	global $axs;
			$fp=fopen(\''.$f.'\', \'w+\');
			fwrite($fp, "<?php return \'".serialize($axs[\'session\'])."\'; ?>");
			fclose($fp);'));
			}
		$axs['session_id']=$sessid;
		return $axs['session_id'];
		}
	} #</class::axs_session>
#2007-12-10 ?>