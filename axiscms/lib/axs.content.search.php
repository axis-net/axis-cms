<?php #2019-03-07
class axs_content_search {
	var $tpl=array(
		'list'=>'     <div class="row{$nr}">
      <h3><a href="{$url}" target="_blank">{$title}</a></h3>
      <p>
       {$text}
       <a href="{$url}" target="_blank" class="path"><span>{$path}</span></a>
      </p>
     </div>
',
		'link_sepr'=>'</span> &gt; <span>',
		);
	var $vr=array();
	static function plugin_load($f, &$cl) {
		global $axs, $axs_content;
		return (array)include($f);
		} #<plugin_load()>
	static function result($index=false) {
		global $axs, $axs_content;
		if (!is_array($index)) $index=$axs_content['index'];
		foreach ($index as $k=>$cl) {
			if (!axs_content::permission_get($cl['path'])) continue;
			$cl['l']=$axs['l'];
			if (isset($cl['submenu'])) self::result($cl['submenu']);
			else self::result_item($cl);
			}
		} #</result()>
	static function result_item(&$cl) {
		global $axs, $axs_content;
		if ($cl['plugin']=='menu') $cl['text']='';
		else {
			$cl['text']=axs_content::text_get($cl['c'], $cl['dir']);
			#if (is_array($a=axs_tab_proc($cl['text'], $axs['time'], "\n"))) dbg($cl['c'], $cl['dir'],$a);
			$cl['text']=strip_tags(axs_tab::proc($cl['text'], array('time'=>$axs['time'], 'rem_col'=>0, 'merge'=>"\n")));
			#dbg($cl['c'],$cl['text']);
			}
		if (preg_match('/'.$axs_content['search']['str_regex'].'/i', $cl['title'].$cl['text'])) {
			if ($cl['text']) $cl['text']=self::text_chop($cl['text'], $axs_content['search']['str']);
			$axs_content['search']['result'][$cl['c']]=$cl;
			}
		if ($tmp=axs_file_choose($cl['plugin'].'.search.php')) foreach (self::plugin_load($tmp, $cl) as $k=>$v) {
			$tmp=$cl['path'];
			if (!isset($v['path'])) $v['path']=array();
			foreach ($v['path'] as $vv) $tmp[]=$vv;
			$v['path']=$tmp;
			$v=array_merge($cl, $v);
			$axs_content['search']['result'][]=$v;
			}
		} #</result_item()>
	static function result_hilight($html, $search) {
		return preg_replace('/(>[^<]*)('.str_replace('/', '\/', quotemeta($search)).')/i', '\\1<mark>\\2</mark>', $html);
		} #</result_hilight()>
	function result_html() {
		global $axs, $axs_content;
		$html='';
		$nr=1;
		foreach ($axs_content['search']['result'] as $k=>$cl) {
			$cl['nr']=$nr;
			$cl['url']=(isset($cl['url'])) ? htmlspecialchars($cl['url']):'';
			$cl['url']=$axs['http_root'].$axs['site_dir'].'?'.axs_url($axs['url'], array('c'=>$cl['c'])).$cl['url'];
			$cl['path']=axs_content::path_get($cl['path'], 'title');
			//foreach ($cl['path'] as $kk=>$vv) $cl['path'][$kk]=$vv['title'];
			$cl['path']=implode($this->tpl['link_sepr'], $cl['path']);
			$html.=axs_tpl_parse($this->tpl['list'], $cl);
			$nr++;
			}
		$html=$this->result_hilight($html, $axs_content['search']['str']);
		return $html;
		} #</result_html()>
	static function text_chop($data, $search, $show_max=5, $pad_len=30, $separator=' &nbsp;&nbsp;&hellip;&nbsp;&nbsp; ') {
		global $axs;
		$data=html_entity_decode(strip_tags($data), ENT_QUOTES|ENT_HTML5, $axs['cfg']['charset']);
		if ((strlen($data)) && (($pos=mb_stripos($data, $search))!==false)) {
			$pre=$end='';
			if ($pos>$pad_len) $pre=$separator;
			$txt=array();
			$start=$lmt=$i=0;
			$search_len=mb_strlen($search);
			while($pos) {
				$i++;
				if ($start>mb_strlen($data)) break;
				if (($pos=mb_stripos($data, $search, $start))===false) break;
				if ($i>$show_max) {
					$end=$separator;
					break;
					}
				$start=$pos-$pad_len-1;
				if ($start<0) $start=0;
				$lmt=$pad_len+$search_len+$pad_len;
				$txt[]='<span>'.mb_substr($data, $start, $lmt).'</span>';
				$start=$pos+$lmt;
				}
			if (($start+$lmt)<mb_strlen($data)) $end=$separator;
			$data=$pre.implode($separator, $txt).$end;
			}
		else {
			if (mb_strlen($data)>$pad_len) $data=mb_substr($data, 0, $pad_len).$separator;
			}
		return $data;
		} # </text_chop()>
	} #</class::axs_content>
#2011-12-15 ?>