<?php #2022-02-01
class axs_filesystem { # Filesystem operations
	var $err_codes=array(
		0=>'UPLOAD_ERR_OK (There is no error, the file uploaded with success.)',
		1=>'UPLOAD_ERR_INI_SIZE (The uploaded file exceeds the upload_max_filesize directive in php.ini.)',
		2=>'UPLOAD_ERR_FORM_SIZE (The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.)',
		3=>'UPLOAD_ERR_PARTIAL (The uploaded file was only partially uploaded.)',
		4=>'UPLOAD_ERR_NO_FILE (No file was uploaded.)',
		6=>'UPLOAD_ERR_NO_TMP_DIR (Missing a temporary folder.)',
		7=>'UPLOAD_ERR_CANT_WRITE (Failed to write file to disk.)',
		8=>'UPLOAD_ERR_EXTENSION (A PHP extension stopped the file upload.)',
		);
	static $ftypes=array(
		'image.*'=>array('gif'=>'gif', 'jpg'=>'jpg', 'jpeg'=>'jpeg', 'png'=>'png', 'wbmp'=>'wbmp', 'xbm'=>'xbm', ),
		'media.*'=>array('asf'=>'asf', 'avi'=>'avi', 'flv'=>'flv', 'mov'=>'mov', 'mp3'=>'mp3', 'mp4'=>'mp4', 'wmv'=>'wmv', 'wma'=>'wma', ),
		'blacklist'=>array(''=>'', 'asp'=>'asp', 'cfs'=>'cfs', 'htaccess'=>'htaccess', 'php'=>'php', 'php3'=>'php3', 'php4'=>'php4', 'php5'=>'php5', 'php6'=>'php6', 'php7'=>'php7', 'php8'=>'php8', 'php9'=>'php9', 'pht'=>'pht', 'py'=>'py', ),
		);
	var $f=''; # filename
	static $f_lenght=255; # filename max lenght
	var $msg=array();
	//var $post_file=array('name'=>'', 'type'=>'', 'tmp_name'=>'', 'error'=>'', 'size'=>'', );
	function __construct($d, $site_nr=1) {
		global $axs;
		$this->cfg=$axs['cfg'];
		$this->site_nr=$site_nr;
		$this->dir_set($d); # default directory
		if ($this->cfg['image_convert_util']) foreach (array('tif', 'tiff', ) as $v) self::$ftypes['image.*'][$v]=$v;
		$this->f_types=self::$ftypes;
		} #</axs_filesystem()>
	function log($function, $line, $msg='') { # log errors
		return axs_admin::log(__FILE__, $line, get_class($this), $function, $msg);
		} #</log()>
	function msg($key, $msg='', $vars=false) { #<Register message for user />
		return axs_admin::msg($this, $key, $msg, $vars);
		} #</msg()>
	static function max_upl_get($unit='B', $get='upload_max_filesize') { #<Get max upload size />
		global $axs;
		# $unit: in bytes (B), megabytes (M, MB, MiB) or gigabytes (G, GiB); 
		# $get: 'FILE', 'POST'
		//switch ($get) {
		//	case 'POST': $gt='post_max_size'; # POST total size
		//	case 'FILE': $gt='upload_max_filesize'; # Max size of one file
		//	case 'FILES': $gt='max_file_uploads'; # Max number of files
		//	}
		$val=trim(ini_get($get));
		$last=strtolower($val[strlen($val)-1]);
		if ((!$axs['file_uploads']) && ($get==='upload_max_filesize')) $val=trim(ini_get('post_max_size'))/100*60;
		$val=intval($val);
		switch ($last) {
			case 'g': $val*=1024;
			case 'm': $val*=1024;
			case 'k': $val*=1024;
			}
		$unit.='';
		switch ($unit[0]) {
			case 'G': $val=$val/(1024*1024*1024);
			case 'M': $val=$val/1024/1024;
			}
		return $val;
		} #</max_upl_get()>
		
	function chmod($file, $check_exists=false) { # $file is full pathname
		if (!file_exists($file)) {
			if ($check_exists) $this->log(__FUNCTION__, __LINE__, 'file not found "'.$file.'"');
			return;
			}
		$d=dirname($file);
		if (!is_writable($d)) @chmod($d, 0777);	if (!is_writable($file)) @chmod($file, 0777);
		if (!is_writable($file)) return $this->log(__FUNCTION__, __LINE__, 'Write permission denied on file "'.$file.'" (chmod 0777)');
		} #</chmod()>
	function copy($f, $fnew, $overwrite=true, $d=false) { # Copy file
		$d=($d===false) ? $this->d:$d;
		if (file_exists($d.$fnew)) {
			if (is_dir($d.$f)) return 'Kataloogi ei saa kopeerida "'.$d.$f.'"';
			if (is_dir($d.$fnew)) return 'Kataloogi ei saa üle kirjutada "'.$d.$fnew.'"';
			if ($overwrite) {
				if (!is_writable($d.$fnew)) {
					if ($error=$this->chmod($d.$fnew)) return $error;
					}
				}
			else return 'Fail on juba olemas "'.$d.$fnew.'"';
			}
		if (file_exists($d.$f)) {
			@copy($d.$f, $d.$fnew);
			if (!file_exists($d.$fnew)) return 'Faili kopeerimine ebaõnnestus "'.$d.$f.'"->"'.$d.$fnew.'"';
			}
		else {
			$this->log(__FUNCTION__, __LINE__, $msg='File not found "'.$d.$f.'"');
			return $this->msg('', $msg);
			}
		} #</copy()>
	function del($f, $check_exists=true) { #<Delete file or directory recursively>
		if (is_dir($this->d.$f)) return $this->dir_rm($f, $check_exists);
		else return $this->f_del($f, $check_exists);
		} #</del()>
	function dir_cd($dir) { #<Change base directory>
		if (is_dir($this->d)) $this->d=str_replace('\\', '/', realpath($this->d)).'/';
		if (is_dir($tmp=realpath($this->d.$dir))) $this->d=$tmp.'/';
		else {
			$this->log('dir_cd', __LINE__, $msg='Invalid directory "'.$this->d.$dir.'"');
			return $this->msg('', $msg);
			}
		} #</dir_cd()>
	function dir_mk($newd, $check_exist=false, $hide_list=false, $d=false) { #<Create directory>
		$site=$this->cfg['site'][$this->site_nr];
		$d=($d===false) ? $this->d:$d;
		if ($check_exist) { if (is_dir($d.$newd)) return $this->msg('dir_exists', 'The directory "{$dir}" already exists!', array('dir'=>$d.$newd)); }
		if (!is_writable($d)) $this->chmod($d);
		if (!is_writable($d)) {
			$this->log('dir_mk', __LINE__, $msg='Failed to create dir "'.$d.$newd.'". Check file permissions (chmod 777)');
			return $this->msg('', $msg);
			}
		$oldumask=umask(0);
		@mkdir($d.$newd, 0777); # or even 01777 so you get the sticky bit set
		umask($oldumask);
		@fclose(@fopen($tmp=$d.$newd.'/tmp.txt','w+')); # attaempt to create tmp file for testing writability
		if (!file_exists($tmp)) {
			@rmdir($d.$newd);
			$ftp=parse_url($this->cfg['ftp']); 
			if (axs_function_ok('ftp_connect')) {
				$connection=ftp_connect($ftp['host']);
				$login=ftp_login($connection, $ftp['user'], $ftp['pass']);
				# check if connection was made
				if ((!$connection) or (!$login)) return $this->msg('', 'FTP connection failed @ dir_mk()');
				# go to destination dir
				if (!ftp_chdir($connection, $ftp['path'].$site['dir'])) return $this->msg('', 'ftp_chdir() failed @ dir_mk() "'.$ftp['path'].$site['dir'].'"');
				if (ftp_mkdir($connection, $newd)) { # create directory
					if (!ftp_chmod($connection, 0777, $newd)) return $this->msg('', 'ftp_chmod() failed @ dir_mk()');
					}
				else return $this->msg('', 'ftp_mkdir() failed @ dir_mk() "'.$ftp['path'].$site['dir'].$newd.'"');
				ftp_close($connection);
				@fclose(@fopen($tmp,'w+')); # attaempt create tmp file for testing writability
				if (!file_exists($tmp)) {
					@rmdir($d.$newd);
					return $this->msg('', 'Failed to write in created dir @ dir_mk() "'.$d.$newd.'". Check file permissions (chmod 777)');
					}
				else unlink($tmp);
				}
			else return $this->msg('', 'Failed to create dir @ dir_mk() "'.$d.$newd.'". Check file permissions (chmod 777)');
			}
		else unlink($tmp);
		if ($hide_list) $this->f_save($newd.'/index.php', '<?php if (!defined(\'AXS_PATH_CMS\')) header(\'HTTP/1.0 404 Not Found\'); ?>');
		} #</dir_mk()>
	function dir_rm($d, $check_exists=true) { #<Delete directory recursively>
		$msg=array();
		if (empty($d)) return $this->msg('dir_name_empty', 'Directory name empty!');
		if ($check_exists) {
			if (!is_dir($this->d.$d)) return $this->msg('d_not_found', 'Directory "'.$this->d.$d.'" not found!', array('f'=>$this->d.$d));
			}
		if (file_exists($this->d.$d)) {
			$dir=dir($this->d.$d);
			while($file=$dir->read()) {
				if($file != '.' && $file != '..') {
					if(is_dir($this->d.$d.'/'.$file)) $this->dir_rm($d.'/'.$file);
					else @unlink($this->d.$d.'/'.$file) or $this->log('dir_rm', $msg[]='File '.$this->d.$d.'/'.$file.' couldn\'t be deleted');
					}
				}
			$dir->close();
			@rmdir($this->d.$d) or $this->log(__FUNCTION__, $msg[]='Folder '.$this->d.$d.' couldn\'t be deleted');
			}
		if (!empty($msg)) return $this->msg('', implode('<br />', $msg));
		} #</dir_rm()>
	function dir_set($d=true) { #<Base directory set>
		if ($d===true) {	$this->d=$this->d_bak;	return;	}
		else $this->d_bak=(isset($this->d)) ? $this->d:false;
		$this->d=$d;
		if (!$this->d) return;
		if (!is_dir($this->d)) {
			$this->log(__FUNCTION__, __LINE__, $msg='Invalid directory "'.$this->d.'"');
			return $this->msg('', $msg);
			}
		} #</dir_set()>
	function exists($f, $type=false) {
		$exists=false;
		if (file_exists($this->d.$f)) $exists=true;
		if ($exists) {
			if ($type=='file') {	if (!is_file($this->d.$f)) $exists=false;	}
			if ($type=='dir') {	if (!is_dir($this->d.$f)) $exists=false;	}
			}
		return $exists;
		} #</exists()>
	function f_del($f, $check_exists=true, $d=false) { #<Delete file>
		$d=($d===false) ? $this->d:$d;
		if ($check_exists) {
			if (!is_file($d.$f)) return $this->msg('f_not_found', 'File "'.$d.$f.'" not founf!', array('f'=>$d.$f));
			}
		if (is_file($d.$f)) {
			if (!is_writable($d.$f)) $this->chmod($d.$f);
			@unlink($d.$f);
			}
		if (is_file($d.$f)) return $this->msg('f_del_err', 'Could not delete file "'.$d.$f.'"!', array('f'=>$d.$f));
		} #</f_del()>
	static function f_ext($f, $preserve_case=false) { # function to get file extension
		$f=pathinfo($f, PATHINFO_EXTENSION);
		return ($preserve_case) ? $f:strtolower($f);
		} # </f_ext()>
	function f_get_contents($f, $check_exists=false, $d=false) {
		$d=($d===false) ? $this->d:$d;
		if (is_file($d.$f)) return file_get_contents($d.$f);
		else {	if ($check_exists) $this->log('f_get_contents', __LINE__, $this->msg('', 'File not found "'.$d.$f.'"!'));	}
		} #</f_get_contents()>
	function f_open($f, $mode, $d=false) {
		$d=($d===false) ? $this->d:$d;
		if (!is_file($d.$f)) {
			$this->msg('f_not_found', 'File not found "'.$d.$f.'"!', array('f'=>$d.$f));
			return false;
			}
		$fp=fopen($d.$f, $mode) or $this->msg('f_open_err', 'Could not open file "'.$d.$f.'"!', array('f'=>$d.$f));
		return $fp;
		} #</f_open()>
	function f_save($f, $data, $check=false, $d=false) { # Update file
		$d=($d===false) ? $this->d:$d;
		if ($check) {
			if ($check=='add') { if (file_exists($d.$f)) return 'fexist'; }
			else { if (!file_exists($d.$f)) return 'fnotexist'; }
			}
		if (is_file($d.$f)) {	if (!is_writable($d.$f)) {	if (!$err=$this->chmod($d.$f)) return $err;	}	}
		if (is_array($data)) $data=implode('', $data);
		@flock($fp=@fopen($d.$f, 'w+'), LOCK_EX);
		@fwrite($fp, $data);
		@fclose($fp);
		//exit();
		} #</f_save>
	function f_upl($n, $save=false, $d=false) { #<Save uploaded file />
		global $axs;
		$d=($d===false) ? $this->d:$d;
		//if (!is_writable($d)) $this->chmod($d);
		//if (!is_writable($d)) return $this->msg('dir_writeprotected', 'Dir "'.$d.'" not writable!', array('d'=>$d));
		
		$file_uploads=$axs['file_uploads'];
		if (is_array($n)) $files=$n;
		else {	$files=($file_uploads) ? axs_get($n, $_FILES, array()):axs_get($n, $_POST, array());	}
		//else {	$files=(is_array($n)) ? $n:axs_get($n, $_FILES, array());	}
		$files=$this->f_upl_array($files);
		
		if ($save) foreach ((array)$save as $k=>$v) {	if (isset($files[$k])) $files[$k]['name']=axs_valid::f_name($v, $this->f_ext($v), self::$f_lenght);	}
		else foreach ($files as $k=>$v) {
			$files[$k]['name']=$v['name']=axs_valid::f_name($v['name'], $this->f_ext($v['name']), self::$f_lenght);
			if (!file_exists($d.$v['name'])) continue;
			$tmp=$this->f_ext($v['name'], true);
			for ($nr=1; file_exists($d.$v['name']); $nr++) {
				$v['name']=preg_replace('/\('.($nr-1).'\)\.'.$tmp.'$/', '.'.$tmp, $v['name']);
				$v['name']=preg_replace('/\.'.$tmp.'$/', '('.$nr.').'.$tmp, $v['name']);
				}
			$files[$k]=$v;
			}
		
		$msg=$this->f=array();
		foreach ($files as $k=>$v) {
			if (!$v['name']) continue;
			$f_ext=$this->f_ext($v['name']);
			//$v['name']=axs_valid::f_name($v['name'], $f_ext, self::$f_lenght);
			$this->f[$k]=$v['name'];
			$tmp=array();
			if (!$v['name']) $this->msg('f_name_err', $tmp[]='Problem with file "'.$k.'" name!');
			if (in_array($f_ext, $this->f_types['blacklist'])) $this->msg('f_ext_forbidden', $tmp[]='Forbidden file extension "{$f_ext}" ("{$name}")', array('f_ext'=>$f_ext, 'name'=>htmlspecialchars($v['name'])));
			if (!empty($tmp)) {
				$msg[]=implode('<br />', $tmp);
				continue;
				}
			if ($file_uploads) {
				if (move_uploaded_file($v['tmp_name'], $d.$v['name'])) @chmod($d.$v['name'], 0644);
				else $this->msg('', $msg[]='Error at file "{$f}": {$error}', array('f'=>htmlspecialchars($v['name']), 'error'=>$v['error'].' '.htmlspecialchars($this->err_codes[$v['error']]), ));
				}
			else $this->f_save($v['name'], base64_decode($v['content']));
			}
		if (!$this->f_upl_multiple) $this->f=$this->f[0];
		if (!empty($msg)) return implode('<br />'."\n", $msg);
		} #</f_upl()>
	function f_upl_array(&$array, $nr=false) { #<Convert array of uploaded files />
		$files=array();
		if (is_array($array['name'])) { #<Multiple files />
			$this->f_upl_multiple=true;
			foreach ($array as $k=>$v) foreach ($v as $kk=>$vv) $files[$kk][$k]=$vv;
			}
		else { #<Single file />
			$this->f_upl_multiple=false;
			foreach ($array as $k=>$v) $files[0][$k]=$v;
			}
		if ($nr!==false) $files=array($nr=>$files[$nr]);
		return $files;
		} #</f_upl_array()>
	function f_tmp_name($file=false, $d=false) {
		$d=($d===false) ? $this->d:$d;
		if (is_array($file)) {
			foreach ($file as $k=>$v) $file[$k]=$this->f_tmp_name($v, $d);
			return $file;
			}
		else {
			$ext=($file) ? '.'.$this->f_ext($file):'';
			$f_tmp='_'.uniqid('');
			while (file_exists($d.$f_tmp)) $f_tmp=$this->f_tmp_name();
			return $f_tmp.$ext;
			}
		} #</f_tmp_name()>
	function rename($fold, $fnew, $overwrite=false, $check_exists=false, $d=false) { # rename file or dir
		if (!$fold) $fold=$this->f;
		$d=($d===false) ? $this->d:$d;
		$err=false;
		if (file_exists($d.$fold)) {
			if ($overwrite) $this->f_del($fnew, false);
			if (file_exists($d.$fnew)) $err=$this->msg('rename_exists', 'The file "'.$d.$fnew.'" already exists!', array('f'=>htmlspecialchars($d.$fnew)));
			else {
				if (!is_writable($d.$fold)) $this->chmod($d.$fold);
				if (!is_writable($d.$fold)) $err=$this->msg('rename_err', 'Could not rename "'.$d.$fold.'" -&gt; "'.$d.$fnew.'"!', array('fold'=>$d.$fold, 'fnew'=>$d.$fnew));
				else @rename($d.$fold, $d.$fnew);
				}
			}
		else { if ($check_exists) $err=$this->msg('f_not_found', 'File not found: "'.$d.$fold.'"', array('f'=>$d.$fold)); }
		if ($err) return $err;
		} #</rename()>
	static function size_format($bytes, $round=false, $unit='auto') {
		if ($unit[0]==='a') {
			$unit='B';
			if ($bytes>=1024) $unit='K';
			if ($bytes>=1024*1024) $unit='MiB';
			if ($bytes>=1024*1024*1024) $unit='GiB';
			if ($bytes>=1024*1024*1024*1024) $unit='TiB';
			}
		switch($unit[0]) {
			case 'T': $bytes=$bytes/(1024*1024*1024*1024);	break;
			case 'G': $bytes=$bytes/(1024*1024*1024);	break;
			case 'M': $bytes=$bytes/1024/1024;	break;
			case 'K': $bytes=$bytes/1024;	break;
			case 'B': $round=0;	break;
			}
		if ($round!==false) $bytes=number_format($bytes, $round, '.', ' ');
		return array('size'=>$bytes, 'unit'=>$unit);
		} #</size_format>
	} #</class::axs_filesystem>
#2008-11-28 ?>