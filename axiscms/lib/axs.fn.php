<?php #2016-05-21
class axs_fn {
	#<Array functions>
	static function array_code($array, $space='	', $n="\n	") {
		$code='array(';
		if ($space) {
			$br="\n".$space;
			$code.=$br;
			}
		else $br=' ';
		foreach ($array as $k=>$v) {
			$code.=(is_string($k)) ? "'".self::array_el_esc($k)."'".'=>':$k.'=>';
			if (is_string($v)) $code.="'".self::array_el_esc($v)."'".','.$br;
			elseif (is_bool($v)) {	$code.=($v) ? 'true'.','.$br:'false'.','.$br;	}
			elseif (is_null($v)) $code.='null'.','.$br;
			elseif (is_array($v)) $code.=self::array_code($v, '	'.$space, $n).', '.$br;
			else $code.=$v.','.$br;
			}
		return $code.')';
		} #</array_code()>
	static function array_el_add($array, $pos, $key, $element=null) {
		$pos=(($tmp=intval($pos))>=1) ? $tmp:1;
		$new=array();
		$nr=0;
		foreach ($array as $k=>$v) {
			$nr++;
			if ($nr===$pos) {
				if (is_array($key)) foreach ($key as $kk=>$vv) $new[$kk]=$vv;
				else $new[$key]=$element;
				}
			$new[$k]=$v;
			}
		if (!is_array($key)) {	if ($nr<$pos) $new[$key]=$element;	} #If nr>total nr of elements
		return $new;
		} #</array_el_add()>
	static function array_el_move($array, $key, $move) {
		$move=(($tmp=intval($move))>=1) ? $tmp:1;
		$index=array();
		$nr=0;
		foreach ($array as $k=>$v) {	$nr++;	$index[$k]=$nr;	}
		if ($index[$key]===$move) return $array;
		$new=array();
		$new_el=$array[$key];
		unset($array[$key]);
		$nr=0;
		foreach ($array as $k=>$v) {
			$nr++;
			if ($nr===$move) $new[$key]=$new_el;
			$new[$k]=$v;
			}
		if ($move>$nr) $new[$key]=$new_el; #If nr>total nr of elements
		return $new;
		} #</array_el_move()>
	static function array_el_pos($array, $key, $default='') {
		$nr=0;
		foreach ($array as $k=>$v) {
			$nr++;
			if ($k===$key) return $nr;
			}
		if ($default==='end') return count($array)+1;
		} #</array_el_pos()>
	static function array_el_esc($str) {	return str_replace(array('\\','\''),  array('\\\\','\\\''), $str);	}
	static function array_find($array, $col, $value) {
		$found=array();
		foreach ($array as $k=>$v) if ($v[$col]===$value) $found[$k]=$k;
		return $found;
		} #</element()>
	static function array_sort($array, $sort_key) {
		$sorted=array();
		foreach ($array as $k=>$v) $sorted[$k]=$v[$sort_key];
		asort($sorted);
		foreach ($sorted as $k=>$v) $sorted[$k]=$array[$k];
		return $sorted;
		} #</array_sort()>
	static function array_table($array, $container=true) {
		$table=($container===true) ? '<table class="table" border="1">'."\n":$container;
		$rownr=0;
		foreach ($array as $cl) {
			$rownr++;
			if ($rownr==1) {
				$table.='	<tr valign="top">'."\n";//.'  <th scope="col">&nbsp;</th>';
				$cols=count($cl);
				foreach ($cl as $k=>$v) $table.='<th scope="col">'.$k.'</th>';
				$table.="\n".'	</tr>'."\n";
				}
			$table.='	<tr valign="top">'."\n";#.'  <th scope="row">'.$rownr.'</th>';
			foreach($cl as $k=>$v) $table.='<td>'.nl2br($v).'</td>';
			$table.="\n".'	</tr>'."\n";
			}
		if (strlen($container)) $table.='</table>'."\n";
		return $table;
		} #</array_table()>
	#</Array functions>
	
	static function str2link($str, $protocol='http://', $separator=', ') {
		$str=explode($separator, $str);
		foreach ($str as $k=>$v) {
			$str[$k]=trim($str[$k]);
			if ($str[$k]) $str[$k]='<a href="'.((strpos($str[$k], '://')) ? '':$protocol).$str[$k].'" target="_blank">'.$str[$k].'</a>';
			else unset($str[$k]);
			}
		return implode(', ', $str);
		} # </str2link()>
	} #</class::axs_fn>
#2010-11-27 ?>