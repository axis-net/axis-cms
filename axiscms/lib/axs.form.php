<?php #2022-04-29
class axs_form {
	public $form_id='axs_form';
	public $key_form_id='form';
	public $key_id='id';
	public $key_add='_add';
	public $key_edit='_edit';
	public $key_order='_order';
	public $key_rank='_rank';
	public $key_search='search';
	public $key_m='m';
	public $key_f='f';
	public $key_t='t';
	public $key_structure_edit='_header_edit';
	public $key_doc='_doc';
	public $doc_anchor='';
	var $msg=array();
	var $msg_codes=array(1=>'required', 2=>'invalid', 3=>'unique', );
	var $http_server='';
	var $tpl=array();
	var $disabled='';
	var $visibility=array();
	var $category_levels=5;
	var $types=array( #<'key'=>true:auto; 'key'=>*:provide default value />
		'_captcha'=>array('type'=>'text', 'size'=>10, 'label.html'=>'Please skip this field<em class="input_required"><abbr title="Please skip this field!">*</abbr></em>', 'lang'=>'en', 'content-form'=>false, 'value_type'=>'s', 'class'=>array('visuallyhidden'), ),
		'button'=>array('type'=>'button', 'label'=>true, 'content-form'=>false, 'value_type'=>'s', ),
		'category'=>array('type'=>'category', 'size'=>10, 'content-form'=>false, 'options'=>array(), 'options_max'=>0, 'value_type'=>'i', 'sql'=>array(
			'_type'=>'int', '_value'=>10, '_attribute'=>'unsigned', 'Null'=>false, 
			), ),
		'checkbox'=>array('type'=>'checkbox', 'label'=>true, 'value'=>'1', 'value_type'=>'i', 'sql'=>array(
			'_type'=>'int', '_value'=>10, '_attribute'=>'unsigned', 'Null'=>false, 
			), ),
		'color'=>array('type'=>'color', 'size'=>7, 'label'=>true, 'value'=>'', 'maxlength'=>7, 'format'=>'#', 'options'=>array(), 'value_type'=>'s', 'sql'=>array(
			'_type'=>'mediumint', '_value'=>8, '_attribute'=>'unsigned', 'Null'=>false,
			), ),
		'content'=>array('type'=>'content', 'label'=>true, 'txt'=>'<p>Text Lorem ipsum dolor sit amet...</p>', ),
		//date
		//datetime
		//datetime-local
		'email'=>array('type'=>'email', 'label'=>true, 'value'=>'', 'size'=>50, 'maxlength'=>'', 'options'=>array(), 'value_type'=>'s', 'sql'=>array(
			'_type'=>'varchar', '_value'=>255, 'Null'=>false, '_charset'=>'ascii',
			), ),
		'fieldset'=>array('type'=>'fieldset', 'label'=>true, ),
		'fieldset_end'=>array('type'=>'fieldset_end', true, ),
		'file'=>array(
			'type'=>'file', 'label'=>true, 'size'=>0, 'content-form'=>false, 'value_type'=>'s',
			'accept'=>array('image.*'=>'','media.*'=>'','*'=>'*',), #<to specify custom ext, give comma separated list />
			'name_file'=>'{$name_valid}', #<if save original file />
			'name_save'=>'{$ext}', #<if save name to SQL />
			'link'=>'_t2', 'link_target'=>'popup',
			#<generate thumbs />
			't'=>array('ext'=>array('jpg'=>'jpg',), 'q'=>90, 'name_base'=>'{$id}', 'alt'=>'{$id}.{$ext}', 'title'=>'', 'w'=>'', 'h'=>'', 'crop_pos'=>'', 'crop_size'=>'', 'enlarge'=>0, 'dw'=>200, 'dh'=>200, ),
			),
		//month
		'number'=>array('type'=>'number', 'label'=>true, 'value'=>'', 'value_type'=>'f', 'min'=>'', 'max'=>'', 'step'=>'any', 'options'=>array(), 'dec'=>0, 'point'=>'.', 'sepr'=>'', 'sql'=>array(
			'_type'=>'int', '_value'=>10, '_attribute'=>'', 'Null'=>true, 'Default'=>'NULL',
			), ),
		'radio'=>array(
			'type'=>'radio', 'label'=>true, 'value_type'=>'i', 'options'=>array(
				''=>array('value'=>'', 'label'=>' - '), 1=>array('value'=>1, 'label'=>'lorem ipsum 1'), 2=>array('value'=>2, 'label'=>'lorem ipsum 2'),
				),
			'options_max'=>0, 'sql'=>array(
				'_type'=>'int', '_value'=>10, '_attribute'=>'unsigned', 'Null'=>false, 
				),
			),
		//range
		'hidden'=>array('type'=>'hidden', 'value'=>'', ),
		'multi-checkbox'=>array(
			'type'=>'multi-checkbox', 'label'=>true, 'size'=>10, 'content-form'=>false, 'value_type'=>'i', 'options'=>array(
				1=>array('value'=>1, 'label'=>'lorem ipsum 1'), 2=>array('value'=>2, 'label'=>'lorem ipsum 2'), 3=>array('value'=>3, 'label'=>'lorem ipsum 3'),
				),
			'options_max'=>0, 'sql'=>array(
				'_type'=>'int', '_value'=>10, '_attribute'=>'unsigned', 'Null'=>false, 
				),
			),
		'password'=>array('type'=>'password', 'label'=>true, 'value'=>'', 'value_type'=>'s', 'size'=>50, 'maxlength'=>'', 'sql'=>array(
			'_type'=>'varchar', '_value'=>255, 'Null'=>false, '_charset'=>'ascii',
			), ),
		'search'=>array('type'=>'text', 'label'=>true, 'value'=>'', 'value_type'=>'s', 'size'=>50, 'maxlength'=>'', 'options'=>array(), ),
		'set-checkbox'=>array(
			'type'=>'set-checkbox', 'label'=>true, 'size'=>2, 'value_type'=>'i', 'options'=>array(
				1=>array('value'=>1, 'label'=>'lorem ipsum 1'), 2=>array('value'=>2, 'label'=>'lorem ipsum 2'), 3=>array('value'=>3, 'label'=>'lorem ipsum 3'),
				),
			'options_max'=>64, 'sql'=>array(
				'_type'=>'set', '_value'=>"'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50','51','52','53','54','55','56','57','58','59','60','61','62','63','64'", 'Null'=>true, 'Default'=>'NULL',
				),
			),
		'select'=>array(
			'type'=>'select', 'label'=>true, 'value_type'=>'i', 'options'=>array(
				1=>array('value'=>'', 'label'=>''), 2=>array('value'=>2, 'label'=>'lorem ipsum 1'), 3=>array('value'=>3, 'label'=>'lorem ipsum 2'),
				),
			'options_max'=>0, 'sql'=>array(
				'_type'=>'int', '_value'=>10, '_attribute'=>'unsigned', 'Null'=>false, 
				),
			),
		'submit'=>array('type'=>'submit', 'label'=>true, ),
		'table'=>array(
			'type'=>'table', 'label'=>true, 'tpl'=>array(''=>'<table>','ul'=>'<ul>',), 'options'=>array(
				'col1'=>array('type'=>'text', 'size'=>2, 'label'=>'Col 1'), 'col2'=>array('type'=>'text', 'size'=>20, 'label'=>'Col 2'),
				), 'options_max'=>0,
			'value'=>'<tr><td><input type="text" /></td><td><input type="text" /></td></tr>',
			),
		'tel'=>array('type'=>'tel', 'label'=>true, 'value'=>'', 'value_type'=>'s', 'size'=>50, 'maxlength'=>'', 'options'=>array(), 'sql'=>array(
			'_type'=>'varchar', '_value'=>255, 'Null'=>false, '_charset'=>'utf8mb4',
			), ),
		//time
		'text'=>array('type'=>'text', 'label'=>true, 'value'=>'', 'value_type'=>'s', 'size'=>50, 'maxlength'=>'', 'options'=>array(), 'sql'=>array(
			'_type'=>'varchar', '_value'=>255, 'Null'=>false, '_charset'=>'utf8mb4', 'COLLATE'=>'utf8mb4_unicode_ci',
			), ),
		'text-between'=>array('type'=>'text-between', 'label'=>true, 'value'=>'', 'value_type'=>'i', 'size'=>10, 'maxlength'=>'', 'separator'=>' - ',  'sql'=>array(
			'_type'=>'int', '_value'=>10, '_attribute'=>'unsigned', 'Null'=>false,
			), ),
		'text-captcha'=>array('type'=>'text-captcha', 'label'=>true, 'value'=>'', 'value_type'=>'s', 'size'=>5, 'maxlength'=>5, 'required'=>1, ),
		'text-join'=>array(
			'type'=>'text-join', 'label'=>true, 'value'=>'', 'value_type'=>'i', 'size'=>50, 'maxlength'=>10, 'content-form'=>false, 'f'=>array(
				'fstname'=>array('module'=>'', 'form'=>'', 'table'=>'users', 'table_alias'=>'u', 'join-col'=>'(id)', ),
				'lstname'=>array('module'=>'', 'form'=>'', 'table'=>'users', 'table_alias'=>'u', 'join-col'=>'(id)', ),
				), 'concat'=>'u.fstname,\' \',u.lstname', 'limit'=>10, 'value'=>'user()', 'limit'=>10, 'sql'=>array(
				'_type'=>'int', '_value'=>10, '_attribute'=>'unsigned', 'Null'=>false, 
				),
			),
		'textarea'=>array('type'=>'textarea', 'label'=>true, 'value'=>'', 'value_type'=>'s', 'cols'=>50, 'rows'=>3, 'bbcode'=>'', 'sql'=>array(
			'_type'=>'text', '_value'=>'', 'Null'=>false, '_charset'=>'utf8mb4',
			), ),
		'timestamp'=>array('type'=>'timestamp', 'label'=>true, 'value'=>'', 'value_type'=>'i', 'size'=>10, 'format'=>'d.m.Y H:i', 'sql'=>array(
			'_type'=>'int', '_value'=>10, '_attribute'=>'unsigned', 'Null'=>false, 
			), ),
		'timestamp-between'=>array('type'=>'timestamp-between', 'label'=>true, 'value'=>'', 'value_type'=>'i', 'size'=>10, 'format'=>'d.m.Y H:i', 'separator'=>' - ', 'sql'=>array(
			'_type'=>'int', '_value'=>10, '_attribute'=>'unsigned', 'Null'=>false, 
			), ),
		'timestamp-updated'=>array('type'=>'timestamp-updated', 'label'=>true, 'value'=>'', 'value_type'=>'i', 'size'=>50, 'format'=>'d.m.Y H:i:s', 'content-form'=>false, 'add_fn'=>true, 'sql'=>array(
			'_type'=>'int', '_value'=>10, '_attribute'=>'unsigned', 'Null'=>false, 
			), ),
		'url'=>array('type'=>'url', 'label'=>true, 'value'=>'', 'value_type'=>'s', 'size'=>50, 'maxlength'=>'', 'options'=>array(), 'sql'=>array(
			'_type'=>'varchar', '_value'=>255, 'Null'=>false, '_charset'=>'ascii',
			), ),
		//week
		'wysiwyg'=>array('type'=>'wysiwyg', 'label'=>true, 'value'=>'', 'value_type'=>'s', 'size'=>50, 'rows'=>3, 'content-form'=>false, 'sql'=>array(
			'_type'=>'longtext', '_value'=>'', 'Null'=>false, '_charset'=>'utf8mb4',
			), ),
		);
	static $types_table_cols=array(
		'id'=>array('_type'=>'int', '_value'=>10, '_attribute'=>'unsigned', 'Key'=>'PRI', 'Extra'=>'AUTO_INCREMENT', ), 
		'_field'=>array('_type'=>'enum', '_value'=>array('', ), 'Default'=>'', ),
		'_parent_id'=>array('_type'=>'int', '_value'=>10, 'Default'=>'0', '_attribute'=>'unsigned', '_charset'=>'ascii', ),
		);
	public $cfg=array('class', 'link'=>'', 'link_target'=>'', 'link_fragment'=>'', 'sql_select'=>'', );
	public $txt=array('label'=>'lbl','abbr'=>'abbr','comment'=>'comment','txt'=>'txt','msg'=>'msg',);
	public $sql_table=false;
	public $sql_add=array();
	public $sql_found_rows=false;
	function __construct($site_nr=1, $url=array(), $structure=false, $dir='', $user_input=array(), $form_id=false, $sql_add=false) {
		global $axs, $axs_content;
		if (is_array($site_nr)) {
			$tmp=$site_nr;
			$site_nr=axs_get('site_nr', $tmp, 1);
			foreach ($tmp as $k=>$v) ${$k}=$v;
			}
		if ($form_id!==false) $this->form_id=$form_id;
		if (!empty($structure['']['form_id'])) $this->form_id=$structure['']['form_id'];
		if (isset($visibility)) $this->visibility=$visibility;
		else $this->visibility=(defined('AXS_SITE_NR')) ? array('site'):array('admin');
		if ($sql_add!==false) $this->sql_add=$sql_add;
		$this->site_nr=$site_nr;
		$this->dir_entry='';
		$this->structure_set_dir($dir);
		$this->url_root=rtrim(dirname($_SERVER['SCRIPT_NAME']), '/\\').'/'.'?';
		$this->url=&$url;
		$this->action_target=$this->form_id;
		$this->l=axs_get('l', $axs);
		$this->db=$axs['cfg']['site'][$this->site_nr]['db'];	
		$this->px=$axs['cfg']['db'][$this->db]['px'];
		foreach ($axs['cfg']['site'][$this->site_nr]['langs'] as $k=>$v) $this->langs[$k]=$k;
		reset($this->langs);
		$this->submit=((axs_get($this->key_form_id, axs_get('axs', $_REQUEST, array()))===$this->form_id) && (!empty($user_input)));
		$this->user_input=($this->submit) ? $user_input:array();
		$this->save=(($this->submit) && (isset($this->user_input['_submit']))) ? true:false;
		//dbg($this->form_id, $this->submit, $this->save);
		$this->save_delete=((isset($this->user_input['_select'])) && (isset($this->user_input['_delete']))) ? true:false;
		$this->save_rank=(isset($_POST[$this->key_rank])) ? key($_POST[$this->key_rank]):false;
		$this->tr=new axs_tr(__DIR__.'/', 'axs.form.tr', $this->l);
		if (isset($tr)) $this->tr->set($tr);
		if ($structure!==false) $this->structure_set($structure);
		} #</__construct()>
	static function _to_string(array $vr, $f='', $l='') {
		foreach ($vr as $k=>$v) if ((!is_null($v)) && (!is_scalar($v))) unset($vr[$k]);
		return $vr;
		} #</_to_string()>
	function _visible($el, $visibility=null) {
		$visibility=($visibility!==null) ? $visibility:$this->visibility;
		if ((empty($visibility)) || (empty($el['visible']))) return true;
		return array_intersect((array)$visibility, (array)$el['visible']);
		} #</_visible()>
	function cfg($get='') {
		if ($get==='') return $this->structure[''];
		if (is_array($get)) {
			foreach ($get as $k=>$v) $get[$k]=axs_get($k, $this->structure[''], $v);
			return $get;
			}
		$a=func_get_args();
		if (count($a)===1) return axs_get($get, $this->structure['']);
		$cfg=axs_get($get, $this->structure['']);
		//$cfg=$this->structure[''];
		foreach ($a as $v) {
			if (!is_array($v)) $cfg[$v]=axs_get($v, $cfg);
			else foreach ($v as $kk=>$vv) $cfg[$kk]=axs_get($kk, $cfg, $vv);
			}
		return $cfg;
		} #</cfg()>
	function checksum($structure, $vl) {
		foreach ($structure as $k=>$v) if (!$this->sql_writable($v)) foreach ($this->element_fields($k, $v) as $kk=>$vv) unset($vl[$kk]);
		return md5(serialize($vl));
		} #</checksum()>
	function dir_entry(array $vl) {
		if (!$this->dir_entry) return '';
		return ltrim(axs_tpl_parse($this->dir_entry, $this->_to_string($vl), __FILE__, __LINE__), '/');
		} #</dir_entry()>
	function fs_init($d=false) {
		if (!isset($this->fs)) {
			$this->fs=new axs_filesystem_img($this->dir_fs, $this->site_nr);
			$this->fs->msg=$this->msg;
			$this->msg=&$this->fs->msg;
			}
		if ($d!==false) $this->fs->dir_set($this->dir_fs.$d);
		} #</fs_init()>
	
	#<Form element level functions>
	function element_esc_html($el) {
		foreach (array('label'=>'', 'comment'=>'', 'abbr'=>'', ) as $k=>$v) if ((isset($el[$k])) && (!isset($el[$k.'.html']))) {
			$el[$k.'.html']=preg_replace("/(\r\n)+|(\n\r)+|\n+/", '', nl2br(axs_html_safe($el[$k])));
			}
		if (strlen(axs_get('abbr', $el))) $el['label.html']='<abbr title="'.axs_html_safe($el['label']).'">'.$el['abbr.html'].'</abbr>';
		if (isset($el['options'])) foreach ($el['options'] as $k=>$v) $el['options'][$k]=$this->element_esc_html($el['options'][$k]);
		return $el;
		} #</element_esc_html()>
	function element_fields($n, $el=false) { #<Get elemet's value structure />
		if ($el===false) $el=$this->structure[$n];
		$f=array();
		$type=explode('-', $el['type']);
		switch ($type[0]) {
			case 'category':
			case 'multi':
				foreach ($el['options'] as $k=>$v) $f[$n.'_'.$k]=$k;
				break;
			case 'set':
				foreach ($el['options'] as $k=>$v) $f[$n][$k]=$k;
				break;
			case 'content':
			case 'fieldset':
			case 'fieldset_end':
			case 'label':
			case 'table':
				$f=array(''=>'');
				break;
			case 'password':
				$f=array($n=>$n, $n.'_changed'=>$n.'_changed');
				break;
			case 'text':
			case 'textarea':
			case 'wysiwyg':
				switch (axs_get(1, $type)) {
					case 'between':
						foreach (array(1,2) as $k=>$v) $f[$n.'_'.$v]=$n.'_'.$v;
						break;
					case 'captcha':
						$f=array(''=>'');
						break;
					case 'join':
						foreach (array($n,$n.'_text') as $k=>$v) $f[$v]=$v;
						break;
					default:
						if (!empty($el['lang-multi'])) foreach ($el['lang-multi'] as $k=>$v) $f[$n.'_'.$k]=$n.'_'.$k;
						else $f[$n]=$n;
						break;
					}
				break;
			case 'timestamp':
				$format=array('date'=>'date', 'h'=>'h', 'i'=>'i', 's'=>'s');
				switch (axs_get(1, $type)) {
					case 'between':
						foreach (array(1,2) as $nr) $f[$n.'_'.$nr]=$format;
						break;
					case 'updated':
						$f=array($n=>$n, $n.'_text'=>$n.'_text');
						break;
					default: $f[$n]=$format;
					} #</switch ($type[1])>
				break;
			default:
				$f[$n]=$n;
				break;
			} #</switch ($type[0])>
		return $f;
		} # </element_fields()>
	function element_file_accept(&$el) {
		if (is_array($el['accept'])) return;
		$this->fs_init();
		$el['accept']=(isset($this->fs->f_types[$el['accept']])) ? $this->fs->f_types[$el['accept']]:preg_split('#,#', $el['accept'], 0, PREG_SPLIT_NO_EMPTY);
		} #</element_file_accept()>
	function element_file_data($key, $el, $vl) {
		if (is_string($el)) $el=$this->structure[$el];
		if (!isset($el['dir'])) $el['dir']=$this->dir_entry($vl);
		if ((isset($vl[$key]['name'])) && (!empty($vl[$key]['name']))) {
			$this->fs_init();
			$el['name']=axs_valid::f_secure($vl[$key]['name']);
			$el['name_valid']=axs_valid::f_name($vl[$key]['name'], false, axs_filesystem::$f_lenght);
			$el['ext']=axs_filesystem::f_ext($el['name_valid']);
			}
		$tmp=array_merge($el, $vl);
		foreach ($tmp as $k=>$v) if ((!is_scalar($v)) && (!is_null($v))) unset($tmp[$k]);
		foreach (array('t'=>array(), /*'name'=>'',*/ 'name_file'=>'', 'name_save'=>'', '_n'=>$key, '_field'=>$key, ) as $k=>$v) {
			if (!isset($el[$k])) $el[$k]=$v;
			if (is_string($v)) $el[$k]=axs_tpl_parse($el[$k], $tmp, __FILE__, __LINE__);
			}
		if (!empty($vl[$key]['name_file'])) $el['name_file']=axs_valid::f_secure($vl[$key]['name_file']);
		$el['f_fs']=$this->dir_fs.$el['dir'].$el['name_file'];
		$el['f_http']=$this->http_server.$this->dir_http.$el['dir'].$el['name_file'];
		foreach ($el['t'] as $k=>$v) {
			$v['type']=$el['type'];
			foreach ($this->types['file']['t'] as $kk=>$vv) {
				if (!isset($v[$kk])) $v[$kk]=(is_array($vv)) ? key($vv):$vv;
				if (is_string($v[$kk])) $v[$kk]=axs_tpl_parse($v[$kk], $tmp=self::_to_string(array_merge($el, $v, $vl)), __FILE__, __LINE__);
				}
			$v['dir']=(isset($vl['_dir'])) ? $vl['_dir']:$el['dir'];
			$v['name']=$v['name_base'].(($k)?'.'.$k:'').(($v['ext'])?'.'.$v['ext']:'');
			$v['f_fs']=$this->dir_fs.$v['dir'].$v['name'];
			$v['f_http']=$this->http_server.$this->dir_http.$v['dir'].$v['name'];
			if (isset($vl['updated'])) $v['f_http'].='?v='.urlencode($vl['updated']);
			else {	if (file_exists($v['f_fs'])) $v['f_http'].='?v='.date('ymdHis', filemtime($v['f_fs']));	}
			$v['src']=(!empty($el['src'])) ? $v[$el['src']]:$v['f_http'];
			if (!strlen($v['alt'])) $v['alt']=$v['name'];
			$v['alt']=preg_replace("/((\r\n)+|(\n\r)+|\n+)/", '', strip_tags($v['alt']));
			$el['t'][$k]=$v;
			}
		reset($el['t']);
		if (empty($el['pic'])) $el['pic']=key($el['t']);
		$el['pic']=(isset($el['t'][$el['pic']])) ? $el['t'][$el['pic']]:array();
		return $el;
		} #</element_file_data()>
	function element_value_get($fields, &$values) { #<Get elemet's value />
		$vl=array();
		if (count($fields)>1) foreach ($fields as $k=>$v) {
			if (is_array($v)) {
				$vl[$k]=axs_get($k, $values, array());
				if (!is_scalar($values[$k])) foreach ($v as $kk=>$vv) $vl[$k][$kk]=axs_get($kk, $values[$k], '');
				}
			else $vl[$k]=axs_get($k, $values, '');
			}
		else $vl=axs_get(key($fields), $values, '');
		return $vl;
		} #</element_value_get()>
	function element_writable($el) {
		#if (empty($el['type'])) exit(dbg($el));
		if ((!empty($el['disabled'])) or (in_array($el['type'], array('fieldset','fieldset_end','content','submit','text-captcha')))) return false;
		return true;
		} #</element_writable()>
	#</Form element level functions>
	
	#<Single input level functions>
	static function input_type_timestamp_value_int($val) { #<Make UNIX timestamp from value array. />
		if (!is_array($val)) return intval($val);
		foreach (array('d'=>'d','m'=>'m','y'=>'Y','h'=>'H','i'=>'i','s'=>'s') as $k=>$v) $val[$k]=intval(axs_get($k, $val));
		if (!array_sum($val)) return 0;
		return mktime($val['h'], $val['i'], $val['s'], $val['m'], $val['d'], $val['y']);
		} #</input_type_timestamp_value_int()>
	#</Single input level functions>
	
	#<msg>
	function msg($key, $msg='', $vars=false) { #<Register message for user />
		return axs_admin::msg($this, $key, $msg, $vars);
		} #</msg()>
	function msg_html($msg=false, $space='     ') { #<Parse messages HTML />
		if ($msg===false) $msg=$this->msg;
		$html='';
		foreach ($msg as $k=>$v) {
			if (is_array($v)) {
				if (isset($v['html'])) $v=$v['html'];
				else $v=nl2br(axs_html_safe($v['label']))."\n".$this->msg_html($v['msg'], $space.' ');
				}
			else $v=nl2br(axs_html_safe($v));
			$v=$space.' <li data-f="'.$k.'">'.$v.'</li>'."\n";
			$html.=$v;
			}
		$class=(strlen($space)<=5) ? ' class="msg"':'';
		if ($html) {
			$html=$space.'<ul'.$class.'>'."\n".$html.$space.'</ul>';
			if ((isset($this->vr['class'])) && (is_array($this->vr['class']))) $this->vr['class']['msg']='msg';
			}
		return $html;
		} #</msg_html()>
	function msg_text($code, $lbl, $html=false) { #<Get message text by error code />
		$txt='"'.$lbl.'": '.$this->tr->t('msg_value_'.$this->msg_codes[$code]);
		if ($html) $txt=nl2br(axs_html_safe($txt));
		return $txt;
		} #</msg_text()>
	#</msg>
	
	#<Structure>
	/*function structure_edit(&$e, $module=false, $form=false) {
		if (!isset($_GET[$this->key_structure_edit])) return;
		$this->structure_edit=new axs_form_header_edit($e->url, $e->site_nr, $module, $form);
		return $this->structure_edit->editor();
		}*/ #</structure_edit()>
	function structure_elements_enable($vl, $structure=false) {
		if (!$structure) $structure=$this->structure;
		foreach ($structure as $k=>$v) {
			if (!empty($v['attr']['data-elements-enable'])) {
				$disabled=true;
				if (!empty($vl[$k])) {
					#if (is_array($vl[$k])) {	if ()	}
					#else {	if ()	}
					}
				foreach ($v['attr']['data-elements-enable'] as $kk=>$vv) {
					
					}
				}
			foreach ($this->structure_get() as $kk=>$vv) {
				if (!isset($vv['fieldset'])) $vv['fieldset']=null;
				if ($vv['fieldset']===$k) {
					if ((!$v) && (($vv['type']==='fieldset') || ($vv['type']==='fieldset_end'))) continue;
					$list[$kk]=$vv;
					}
				else {	if ($k===$kk) $list[$k]=$vv;	}
				}
			}
		if (!empty($fields)) {
			$all=false;
			if (!is_array($fields)) $fields=array($fields=>'');
			foreach ($fields as $k=>$v) $list[$k]=$this->structure[$k];
			}
		if ($all) $list=$this->structure;
		unset($list['']);
		if ($this->visibility) foreach ($list as $k=>$v)  if (!$this->_visible($v)) unset($list[$k]);
		return $list;
		} #</structure_elements_enable()>
	function structure_get($fieldset=false, $fields=array()) {
		$all=true;
		$list=array();
		if ($fieldset!==false) {
			$all=false;
			if (!is_array($fieldset)) $fieldset=array($fieldset=>'');
			foreach ($fieldset as $k=>$v) {
				foreach ($this->structure_get() as $kk=>$vv) {
					if (!isset($vv['fieldset'])) $vv['fieldset']=null;
					if ($vv['fieldset']===$k) {
						if ((!$v) && (($vv['type']==='fieldset') || ($vv['type']==='fieldset_end'))) continue;
						$list[$kk]=$vv;
						}
					else {	if ($k===$kk) $list[$k]=$vv;	}
					}
				}
			}
		if (!empty($fields)) {
			$all=false;
			if (!is_array($fields)) $fields=array($fields=>'');
			foreach ($fields as $k=>$v) $list[$k]=$this->structure[$k];
			}
		if ($all) $list=$this->structure;
		unset($list['']);
		if ($this->visibility) foreach ($list as $k=>$v)  if (!$this->_visible($v)) unset($list[$k]);
		return $list;
		} #</structure_get()>
	function structure_get_by_attr($attr, $structure=false) {
		if ($structure===false) $structure=(isset($this->structure)) ? $this->structure:array();
		$list=array();
		foreach ($structure as $k=>$v) if ($k) foreach ((array)$attr as $kk=>$vv) if (!empty($v[$vv])) $list[$k]=$v;
		return $list;
		} #</structure_get()>
	function structure_get_by_type($type, $structure=false) {
		$type=(array)$type;
		if ($structure===false) $structure=(isset($this->structure)) ? $this->structure:array();
		$list=array();
		foreach ($structure as $k=>$v) if ($k) {	if (in_array($v['type'], $type, true)) $list[$k]=$v;	}
		return $list;
		} #</structure_get()>
	function structure_get_labels($html=false, $structure=false) {
		if ($structure===false) $structure=(isset($this->structure)) ? $this->structure:array();
		$list=array();
		foreach ($structure as $k=>$v) if ($k) {
			foreach ($this->txt as $key=>$suffix) $list[$k.'.'.$suffix]=($html) ? axs_html_safe(axs_get($key, $v)):axs_get($key, $v);
			if (!empty($v['options'])) foreach ($v['options'] as $kk=>$vv) {
				foreach ($this->txt as $key=>$suffix) $list[$k.'.'.$kk.'.'.$suffix]=($html) ? axs_html_safe(axs_get($key, $vv)):axs_get($key, $vv);
				}
			}
		return $list;
		} # </structure_get_labels()>
	function structure_get_thead($structure=false) {
		if ($structure===false) $structure=$this->structure;
		$th=array();
		foreach ($structure as $k=>$v) if (!empty($v['thead'])) $th[$k]=$v;
		if (!empty($th)) return $th;
		foreach ($structure as $k=>$v) if ($k) {
			//if (!isset($v['type'])) dbg($k,$v);
			if (!in_array($v['type'], array('button','content','fieldset','fieldset_end','submit','text-captcha',))) $th[$k]=$v;
			}
		return $th;
		} #</structure_get_thead()>
	function structure_get_item_title(&$vl) {
		if (!isset($this->item_title)) {
			$this->item_title=$this->structure_get_by_attr('title_item');
			if (!$this->item_title) {
				$this->item_title=$this->structure_get_by_type('text');
				if ($this->item_title) $this->item_title=array(key($this->item_title)=>current($this->item_title));
				}
			}
		$t=$tmp=array();
		foreach ($this->item_title as $k=>$v) $t[]=$this->value_display($k, $v, $vl, $tmp, false);
		return ($this->item_title) ? implode(' ', $t):'#'.$vl['id'];
		} #</structure_get_item_title()>
	function structure_set($structure=false) {
		if ($structure!==false) $this->structure=$structure;
		if (isset($this->structure[''])) {
			$this->structure_set_cfg($this->structure['']);
			}
		if (empty($this->structure['']['tabs'])) $this->structure['']['tabs']=array();
		$this->browse=$this->rank=false;
		$fieldset_open=0;
		$fieldset=array(1=>'', );
		foreach ($this->structure as $k=>$v) if ($k) {
			if (!$this->_visible($v)) continue;
			if (!empty($v['txt_strip'])) $v['txt']=strip_tags($v['txt']);
			#<Count open fieldsets />
			if ($v['type']==='fieldset') {
				$fieldset_open++;
				$fieldset[$fieldset_open]=$k;
				}
			if ($v['type']==='fieldset_end') {
				unset($fieldset[$fieldset_open]);
				if ($fieldset_open<1) unset($this->structure[$k]); #<Remove unnecessary fieldset closings />
				else $fieldset_open--;
				}
			$this->structure[$k]['fieldset']=implode('>', $fieldset);
			if ($v['type']==='submit') { #<Set the last [type="submit"] button as submit indicator />
				if ($this->submit) {
					if (isset($this->user_input[$k])) $this->save=$k;
					}
				}
			//if ((!isset($v['label'])) && ($this->tr->has($k.'.lbl'))) $v['label']=$this->tr->t($k.'.lbl');
			$this->structure[$k]=$this->structure_set_element($k, $v);
			$v['sql_table']=$this->sql_table;
			if ($v['type']==='table') {
				foreach ($v['options'] as $kk=>$vv) {
					$vv['sql_table']=$this->sql_table.'_table';
					if ((!isset($vv['label'])) && ($this->tr->has($k.'.'.$kk.'.lbl'))) $vv['label']=$this->tr->t($k.'.'.$kk.'.lbl');
					$this->structure[$k]['options'][$kk]=$vv=$this->structure_set_element($kk, $vv);
					if (!empty($vv['rank'])) $this->structure[$k]['rank_col']=$kk;
					}
				}
			if (!empty($v['rank'])) $this->rank=$k;
			}
		while ($fieldset_open>0) { #<Close open fieldsets />
			$this->structure['f_end'.$fieldset_open]=array('type'=>'fieldset_end', );
			$fieldset_open--;
			}
		if (!isset($this->structure[$this->key_id])) $this->key_id=key(array_slice($this->structure, 1, 1));
		#<Add simple spam protection element />
		//array_splice($this->structure, count($this->structure)-1, 0, array('_captcha'=>array('type'=>'text', 'size'=>0, 'label'=>'Please skip this field<em class="input_required"><abbr title="Please skip this field!">*</abbr></em>', 'lang'=>'en', )));
		} #</structure_set()>
	function structure_set_cfg($cl) {
		global $axs, $axs_content;
		foreach (array('px'=>$this->px, ) as $k=>$v) if (!isset($cl[$k])) $cl[$k]=$v;
		//foreach (array('dir_entry', 'dir', ) as $v) if (!empty($cl[$v])) $cl[$v]=rtrim($cl[$v], '/').'/';
		//$tmp=$this->_to_string($cl);
		//foreach (array('dir', 'sql_table', ) as $v) if (!empty($cl[$v])) {
		//	if (strpos($cl[$v], '{$')!==false) $cl[$v]=axs_tpl_parse($cl[$v], $tmp, __FILE__, __LINE__);
		//	}
		$cl=self::structure_set_cfg_parse($cl, $cl);
		foreach (array('dir', 'dir_entry', ) as $v) if (isset($cl[$v])) $this->{$v}=$cl[$v];
		if (!empty($cl['sql_table'])) $this->sql_table=$cl['sql_table'];
		foreach (array('table'=>'sql_table', /*'sql_fields',*/ ) as $k=>$v) if (isset($cl['sql'][$k])) $this->{$v}=$cl['sql'][$k];
		if (!empty($cl['dir'])) $this->structure_set_dir($cl['dir']);
		$this->structure['']=$this->cfg=$cl;
		if (!empty($this->cfg['tr'])) $this->tr->set($this->cfg['tr']);
		unset($this->cfg['tr']);
		if (empty($this->tr->tr))  $this->tr=axs_tr::load_class_tr(__CLASS__, $axs['cfg']['cms_lang']);
		} #</structure_set_cfg()>
	static function structure_set_cfg_parse($cfg, $cl) {
		foreach (array('dir_entry', 'dir', ) as $v) if (!empty($cfg[$v])) $cfg[$v]=rtrim($cfg[$v], '/').'/';
		//$cl=self::_to_string($cl);
		foreach (array('dir'=>'', 'sql_table'=>'', 'sql'=>array('table'=>'', ), ) as $k=>$v) if (!empty($cfg[$k])) {
			if (is_array($v)) foreach ($v as $kk=>$vv) {
				if ((isset($cfg[$k][$kk])) && (axs_tpl_has($cfg[$k][$kk]))) $cfg[$k][$kk]=axs_tpl_parse($cfg[$k][$kk], $cl, __FILE__, __LINE__);
				}
			else {	if (axs_tpl_has($cfg[$k])) $cfg[$k]=axs_tpl_parse($cfg[$k], $cl, __FILE__, __LINE__);	}
			}
		if (empty($cfg['layout'])) $cfg['layout']='list';
		return $cfg;
		} #</structure_set_cfg()>
	function structure_set_dir($d) {
		global $axs;
		$this->dir=$d;
		$this->dir_fs=axs_dir('content').$this->dir;
		$this->dir_http=axs_dir('content', 'http').$this->dir;
		} #</structure_set_dir()>
	function structure_set_element($n, $el) {
		global $axs;
		if (!isset($el['value'])) $el['value']='';
		if (!empty($el['options_get'])) {
			$a=array('px'=>$this->px, 'l'=>$this->l, 'module'=>$el['module'], 'form'=>$el['form'], );
			foreach($el['options_get'] as $k=>$v) foreach ($v as $kk=>$vv) {
				if (is_array($vv)) foreach ($vv as $kkk=>$vvv) {	if (axs_tpl_has($vvv)) $el['options_get'][$kk][$kkk]=axs_tpl_parse($vvv, $a);	}
				else {	if (axs_tpl_has($vv)) $el['options_get'][$k][$kk]=axs_tpl_parse($vv, $a);	}
				}
			if (!empty($el['options_get']['sql'])) {
				$select=array();
				if (!empty($el['options_get']['sql']['select'])) foreach ($el['options_get']['sql']['select'] as $k=>$v) {
					$select[]=(strlen($v)) ? '"'.$k.'" AS "'.$v.'"':$k;
					}
				if (empty($select)) $select=array('*');
				$order=(!empty($el['options_get']['sql']['order'])) ? $el['options_get']['sql']['order']:'"label"';
				$db=(!empty($el['options_get']['sql']['db'])) ? $el['options_get']['sql']['db']:$this->db;
				$r=axs_db_query($q='SELECT DISTINCT '.implode(', ', $select).' FROM "'.$el['options_get']['sql']['t'].'" ORDER BY '.$order, 'k', $db, __FILE__, __LINE__, array('error_handle'=>1));
				if ($r) foreach ($r as $id=>$cl) {
					foreach (array('value'=>'', 'label'=>'_'.$this->l, 'abbr'=>'_'.$this->l, 'comment'=>'_'.$this->l, ) as $k=>$v) {
						if (!isset($cl[$k])) $cl[$k]=axs_get($k.$v, $cl);
						}
					$el['options'][$id]=$cl;
					}
				#echo dbg($q,$r,$el['options']);
				}
			if (!empty($el['options_get']['class'])) {
				if (!class_exists($el['options_get']['class']['name'])) axs_class_load($el['options_get']['class']['name']);
				if (is_callable(array($el['options_get']['class']['name'], $el['options_get']['class']['fn']))) {
					$tmp=call_user_func(array($el['options_get']['class']['name'], $el['options_get']['class']['fn']), axs_get('a', $el['options_get']['class'], array()));
					foreach ((array)$tmp as $k=>$v) {	$el['options'][$k]=$v;	}
					}
				}
			}
		$el=$this->element_esc_html($el);
		if (isset($el['lang-multi'])) {
			$el['lang-multi']=array();
			foreach ($axs['cfg']['site'][$this->site_nr]['langs'] as $k=>$v) $el['lang-multi'][$k]=$k;
			}
		if ((isset($el['lang'])) && (!$el['lang'])) $el['lang']=$this->l;
		
		switch ($el['type']) {
			case 'number':
				if (empty($el['step'])) $el['step']='any';
				foreach (array('dec'=>'', 'point'=>'.', 'sepr'=>'', ) as $k=>$v) if (!isset($el[$k])) $el[$k]=$this->types[$el['type']][$k];
				break;
			case 'select':
				//if (axs_get('cfg', $el)==='options=country') $el['options']+=axs_country::form_select();
				//options[sql][t]={$px}oah_themes&options[sql][value]=id&options[sql][label]=title_{$l} 
				break;
			case 'text':
			case 'textarea':
			case 'wysiwyg':
				if ($el['value']==='user()') $el['value']=axs_user::get('name');
				#if ($el['type']=='wysiwyg') 	if (!isset($el['url'])) $this->structure[$n]['url']=array('dir'=>urlencode($this->dir)); #<Set URL />
				break;
			case 'text-join':
				foreach (array('f', 'concat', 'data-copy') as $k=>$v) if (!isset($el[$v])) $el[$v]=array();
				if (is_string($el['data-copy'])) parse_str($el['data-copy'], $el['data-copy']);
				foreach ($el['f'] as $k=>$v) {
					#$el['f'][$k]['table']=($v['table']==='users') ? $axs['cfg']['site'][1]['px'].'users':$this->px.$v['table'];
					if ($v['table']==='users') $el['f'][$k]['table']=$axs['cfg']['site'][1]['px'].'users';
					}
				if (!is_array($el['concat'])) $el['concat']=preg_split("/(,)(?=(?:[^']|'[^']*')*$)/", $el['concat'], -1, PREG_SPLIT_NO_EMPTY);
				foreach ($el['data-copy'] as $k=>$v) if (!is_array($v)) $el['data-copy'][$k]=preg_split("/(,)(?=(?:[^']|'[^']*')*$)/", $v, -1, PREG_SPLIT_NO_EMPTY);
				if (empty($el['limit'])) $el['limit']=$this->types[$el['type']]['limit'];
				if (isset($_GET[$n.'_browse'])) $this->browse=$n;
				if (!isset($el['url'])) $el['url']=array_merge($this->url, array($n.'_browse'=>'', ));
				if ($el['value']==='user()') $el['value']=array(''=>intval(axs_user::get('id')) ,'_text'=>axs_user::get('name'));
				break;
			case 'text-between':
				if (!strlen(axs_get('separator', $el))) $el['separator']=$this->types[$el['type']]['separator'];
				break;
			case 'timestamp':
				if ($el['value']==='time()') $el['value']=date('Y-m-d H:i:s');
				if ($el['value']) $el['value']=array(
					'date'=>substr($el['value'], 0, 10), 'h'=>substr($el['value'], 11, 2), 'i'=>substr($el['value'], 14, 2), 's'=>substr($el['value'], 17, 2),
					);
				break;
			case 'timestamp-between':
				if (!strlen(axs_get('separator', $el))) $el['separator']=$this->types[$el['type']]['separator'];
				break;
			}
		return $el;
		} #</structure_set_element()>
	function structure_set_labels($tr=false, $prefix='') {
		if ($tr===false) $tr=$this->tr->tr;
		foreach ($this->structure as $k=>$v) foreach ($this->txt as $kkk=>$vvv) {
			if (isset($tr[$prefix.$k.'.'.$vvv])) {
				$this->structure[$k][$kkk]=$tr[$prefix.$k.'.'.$vvv];
				$this->structure[$k][$kkk.'_html']=
				$this->structure[$k][$kkk.'.html']=axs_html_safe($tr[$prefix.$k.'.'.$vvv]);
				}
			if (!empty($v['options'])) foreach ($v['options'] as $kk=>$vv) foreach ($this->txt as $kkk=>$vvv) if (!isset($vv[$kkk])) {
				if (isset($tr[$prefix.$k.'.'.$kk.'.'.$vvv])) {
					$this->structure[$k]['options'][$kk][$kkk]=$tr[$prefix.$k.'.'.$kk.'.'.$vvv];
					$this->structure[$k]['options'][$kk][$kkk.'_html']=
					$this->structure[$k]['options'][$kk][$kkk.'.html']=axs_html_safe($tr[$prefix.$k.'.'.$kk.'.'.$vvv]);
					}
				}
			}
		} #</structure_set_labels()>
	#</Structure>
	
	#<SQL>
	function sql_add($spr='', $t='', $pre='', $add=array(), $sql=false) {
		if ($t) $t.='.';
		if ($sql===false) $sql=$this->sql_add;
		if (is_array($add)) foreach ($add as $k=>$v) $sql[$k]=$v;
		foreach ($sql as $k=>$v) {
			if (($v===null) or ($v===false)) {	unset($sql[$k]);	continue;	}
			if (is_string($k)) {
				if (is_array($v)) {
					foreach ($v as $kk=>$vv) $v[$kk]=$t."`".$k."`='".addslashes($vv)."'";
					$sql[$k]=($v) ? '('.implode(' OR ', $v).')':null;
					}
				else $sql[$k]=$t."`".$k."`='".addslashes($v)."'";
				}
			else $sql[$k]=$v;
			if (!strlen($sql[$k])) unset($sql[$k]);
			}
		return ((!empty($sql))?$pre:'').implode($spr, $sql);
		} #</sql_add()>
	function sql_order($pre='', $t='', $structure=false) {
		if ($structure===false) {
			$structure=$this->structure;
			}
		else $sort=array('col'=>'', 'desc'=>'');
		$tmp=explode('.', axs_get($this->key_order, $this->user_input));
		$sort=array('col'=>axs_get(0, $tmp), 'desc'=>axs_get(1, $tmp));
		
		$order=$sql=array();
		if ($sort['col']) {
			if (!empty($structure[$sort['col']]['sort'])) $order=array($sort['col']=>$structure[$sort['col']], );
			($sort['desc']==='dsc') ? $order[$sort['col']]['DESC']=true:$order[$sort['col']]['ASC']=true;
			}
		else foreach ($structure as $k=>$v) if ((!empty($v['ASC'])) || (!empty($v['DESC']))) $order[$k]=$v;
		if ($t) $t.='.';
		foreach ($order as $k=>$v) {
			$col=(empty($v['lang-multi'])) ? $k:$k.'_'.$this->l;
			if (!empty($v['sort'])) $sql[$k]=$t.'`'.$col.'`';
			foreach (array('ASC','DESC') as $vv) if (!empty($v[$vv])) {
				if (preg_match('/.*-between$/', $v['type'])) $sql[$k]=$t.'"'.$k.'_1" '.$vv.', '.$t.'"'.$k.'_2" '.$vv;
				//['type']==='text-join')) $this->sort_col.='_text';
				else $sql[$k]=$t.'"'.$col.'" '.$vv;
				}
			}
		//dbg((!empty($sql)) ? $pre.implode(', ', $sql):'');
		return (!empty($sql)) ? $pre.implode(', ', $sql):'';
		} #</sql_order()>
	function sql_select($t='', $before='', $after='', $structure=false, $prev_next=false) {
		global $axs;
		if (strlen($t)>1) $t='"'.$t.'"';
		if ($t) $t.='.';
		if ($structure===false) $structure=$this->structure;
		$sql=array();
		foreach ($structure as $k=>&$v) {
			if (!empty($v['size'])) switch($v['type']) {
				case 'category':
				case 'multi-checkbox':
					$tmp=array();
					foreach ((array)$v['options'] as $kk=>$vv) $tmp[$kk]=$t.'"'.$k.'_'.$kk.'"';
					if ($tmp) $sql[$k]=implode(', ', $tmp);
					break;
				#case 'file':
					#$sql[$k]=$t.'"'.$k.'"';
					#break;
				case 'password':
					$sql[$k]=$t.'"'.$k.'"';
					if (isset($structure[$k.'_confirm'])) $structure[$k.'_confirm']['size']=0;
					break;
				case 'text-join':
					$sql[$k]=$t.'"'.$k.'", '.$this->sql_select_concat($v['concat']).' AS "'.$k.'_text"';
					break;
				case 'table':
					if (!axs_get('select_count', $v)) break;
					$tmp=$this->sql_table.'_table';
					$sql[$k]='(SELECT COUNT(*) FROM "'.$tmp.'" WHERE "'.$tmp.'"."_parent_id"='.$t.'"id" AND "'.$tmp.'"."_field"=\''.$el['id'].'\') AS "'.$k.'"';
					break;
				case 'text-between':
				case 'timestamp-between':
					$sql[$k]=$t.'"'.$k.'_1", '.$t.'"'.$k.'_2"';
					break;
				case 'timestamp-updated':
					$sql[$k]=$t.'"'.$k.'"';
					if (!empty($v['add_fn'])) $sql[$k].=', "'.$axs['cfg']['db'][1]['px'].'users"."user" AS "'.$k.'_text"';
					break;
				default:
					$tmp=array();
					if (!empty($v['lang-multi'])) foreach ($v['lang-multi'] as $kk=>$vv) $tmp[$kk]=$k.'_'.$vv;
					else $tmp[$k]=$k;
					foreach ($tmp as $kk=>$vv) $tmp[$kk]=((axs_get('maxlength', $v)<0) && ($v['type']=='textarea' or $v['type']=='wysiwyg')) ? 	'LEFT('.$t.'"'.$vv.'",'.abs($v['maxlength']).') AS "'.$vv.'", LENGTH('.$t.'"'.$vv.'") AS "'.$vv.'.len"':$t.'"'.$vv.'"';
					$sql[$k]=implode(', ', $tmp);
					break;
				}
			if (!empty($v['sql_select'])) $sql[$k]=$v['sql_select'];
			}
		#if ($prev_next) $sql['_prev_next']=$this->sql_select_prev_next($this->sql_table, $prev_next, $t);
		return (!empty($sql)) ? $before.implode(', ', $sql).$after:'';
		} #</sql_select()>
	function sql_select_found_rows() {
		global $axs;
		return ($axs['cfg']['db'][$this->db]['type']==='mysql') ? 'SQL_CALC_FOUND_ROWS ':'count(*) OVER() AS "FOUND_ROWS()", ';
		} #</sql_select()>
	function sql_select_prev_next($t, $col, $pt, $where='', $select='id') {
		if (!$col) $col='id';
		if ($where) $where.=' AND ';//' WHERE '.$where;
		$where.='"'.$select.'" AND ';
		return '	(SELECT "'.$select.'" FROM "'.$t.'" WHERE '.$where.'"'.$col.'"<"'.$pt.'"."'.$col.'" ORDER BY "'.$col.'" DESC LIMIT 1) AS "_prev",'."\n".
			'	(SELECT "'.$select.'" FROM "'.$t.'" WHERE '.$where.'"'.$col.'">'.$pt.'."'.$col.'" ORDER BY "'.$col.'" ASC LIMIT 1) AS "_next",'."\n".
			'	(SELECT COUNT(*) FROM "'.$t.'" WHERE '.$where.'1) AS "_total"';
		} #</sql_select_rank()>
	function sql_select_concat($s) {
		foreach ($s as $k=>$v) {
			if (axs_tpl_has($v)) $v=axs_tpl_parse($v, array('l'=>$this->l));
			$tmp=$v;
			if (strncmp($v, '"', 1)!==0) $v='"'.str_replace('.', '"."', $v).'"';
			if (!preg_match('/^\'|^"/', $tmp)) $s[$k]='COALESCE('.$v.',\'\')';
			}
		return (count($s)>1) ? 'CONCAT('.implode(',', $s).')':implode('', $s);
		} #</sql_select_concat()>
	function sql_tables_join($t, $structure=false) {
		global $axs;
		if ($structure===false) $structure=$this->structure;
		if (strlen($t)>1) $t='"'.$t.'"';
		if ($t) $t.='.';
		$sql=array();
		foreach ($structure as $k=>$v) if ($k) {
			if ($v['type']==='text-join') foreach ($v['f'] as $kk=>$vv) {
				$vv=$this->sql_tables_join_element($k, $vv, $t);
				$sql[$vv['table_alias']]='LEFT JOIN "'.$vv['table'].'" AS "'.$vv['table_alias'].'" ON "'.$vv['table_alias'].'"."id"='.$vv['join_col'];
				}
			if ($v['type']==='timestamp-updated') {
				if (!empty($v['add_fn'])) $sql[$k]='LEFT JOIN "'.$axs['cfg']['db'][1]['px'].'users" ON "'.$axs['cfg']['db'][1]['px'].'users"."id"='.$t.'"'.$k.'_uid"';
				}
			}
		return (!empty($sql)) ? implode(' ', $sql):'';
		} #</sql_tables_join()>
	function sql_tables_join_element($key, $v, $t='') {
		if (empty($v['join_col'])) $v['join_col']=$t.'"'.$key.'"';
		return $v;
		} #</sql_tables_join_element()>
	function sql_values_get($values, $structure=false) { # make values from SQL suitable for form elements 
		if ($structure===false) $structure=$this->structure;
		foreach ($structure as $k=>$v) if ($k) {
			foreach ($this->element_fields($k, $v) as $kk=>$vv) if (!isset($values[$kk])) $values[$kk]='';
			switch ($v['type']) {
				case 'color':
					$values[$k]=(strlen(axs_get($k, $values, ''))) ? '#'.strtoupper(dechex($values[$k])):'';
					break;
				case 'file':
					$values[$k]=array('name_file'=>axs_get($k, $values), );
					break;
				case 'password':
					$values[$k]=$values[$k.'_changed']=(strlen($values[$k])) ? str_repeat('*', $v['size']):'';
					if (isset($structure[$k.'_confirm'])) $values[$k.'_confirm']=$values[$k.'_confirm_changed']=$values[$k];
					break;
				case 'set-checkbox':
					if ($values[$k]) {
						$values[$k]=explode(',', $values[$k]);
						$values[$k]=array_combine($values[$k], $values[$k]);
						}
					else $values[$k]=array();
					break;
				case 'table':
					if (!isset($values[$k])) {
						$tables=current($this->sql_query_element_table($values['id']));
						if ($tables) foreach ($tables as $kk=>$vv) $values[$kk]=$vv;
						}
					if (isset($values[$k])) foreach ($values[$k] as $id=>$cl) $values[$k][$id]=$this->sql_values_get($cl, $v['options']);
					break;
				case 'text-captcha':
					break;
				case 'text-join':
					if (!$values[$k]) $values[$k.'_text']='';
					break;
				case 'timestamp':
					$value=$values[$k];
					$values[$k]=array('date'=>'Y-m-d','d'=>'d','m'=>'m','y'=>'Y','h'=>'H','i'=>'i','s'=>'s');
					if ($value) foreach ($values[$k] as $kk=>$vv) $values[$k][$kk]=date($vv, $value);
					else foreach ($values[$k] as $kk=>$vv) $values[$k][$kk]='';
					break;
				case 'timestamp-between':
					foreach (array(1,2) as $nr) {
						$value=$values[$k.'_'.$nr];
						$values[$k.'_'.$nr]=array('date'=>'Y-m-d','d'=>'d','m'=>'m','y'=>'Y','h'=>'H','i'=>'i','s'=>'s');
						if ($value) foreach ($values[$k.'_'.$nr] as $kk=>$vv) $values[$k.'_'.$nr][$kk]=date($vv, $value);
						else foreach ($values[$k.'_'.$nr] as $kk=>$vv) $values[$k.'_'.$nr][$kk]='';
						}
					break;
				case 'timestamp-updated':
					if (empty($v['format'])) $v['format']='d.m.Y H:i:s';
					$values[$k]=(intval($values[$k])) ? date($v['format'], intval($values[$k])):'';
					break;
				default: 
					break;
				}
			}
		return $values;
		} #</sql_values_get()>
	function sql_query_element_table($_parent_id, &$data=false) {
		if (empty($_parent_id)) return array();
		$tables=$fields=$cols=$where=array();
		#<Fields and sort />
		foreach ($this->structure_get_by_type('table') as $k=>$v) {
			$fields[$k]=$k;
			$cols=array_merge($v['options'], $cols);
			foreach ((array)$_parent_id as $vv) if (!isset($tables[$vv][$k])) $tables[$vv][$k]=array();
			}
		if (empty($cols)) return array();
		foreach ((array)$_parent_id as $v) $where[]='"_parent_id"=\''.intval($v).'\'';
		$result=axs_db_query('SELECT t."id", t."_field", t."_parent_id"'.$this->sql_select('t', ', ', '', $cols).' FROM "'.$this->sql_table.'_table" AS t'."\n".
		'	'.$this->sql_tables_join('t', $cols)."\n".
		'	WHERE '.implode(' OR ', $where).' ORDER BY "_field" ASC'.$this->sql_order(', ', 't', $cols), 'k', $this->db, __FILE__, __LINE__);
		foreach ($result as $id=>$cl) {
			$cl['_key']=$id;
			$tables[$cl['_parent_id']][$cl['_field']][$id]=$cl;
			}
		if (is_array($data)) {
			reset($data);
			if (is_int(key($data))) {
				foreach ($data as $k=>$cl) foreach ($fields as $kk=>$vv) $data[$k][$kk]=(isset($tables[$cl['id']][$kk])) ? $tables[$cl['id']][$kk]:array();
				}
			else foreach ($fields as $kk=>$vv) $data[$kk]=(isset($tables[$data['id']][$kk])) ? $tables[$data['id']][$kk]:array();
			}
		return $tables;
		} #</sql_query_element_table()>
	function sql_result_set($data, &$search=false) {
		if ($search) $search->export($data);
		if ($tmp=axs_get('FOUND_ROWS()', current($data))) $this->found_rows=$this->sql_found_rows=$tmp;
		else $this->found_rows=$this->sql_found_rows=axs_db_query('', 'found_rows', $this->db);
		$_parent_id=array();
		foreach ($data as $cl) if (isset($cl['id'])) $_parent_id[$cl['id']]=$cl['id'];
		$tables=$this->sql_query_element_table($_parent_id);
		if ($tables) foreach ($data as $k=>$cl) if (isset($tables[$cl['id']])) foreach ($tables[$cl['id']] as $field=>$table) $data[$k][$field]=$table;
		return $this->sql_result=$data;
		} #</sql_result_set()>
	#</SQL>
	
	#<Validate>
	function validate(&$vl=false) { #<Validate form>
		if ($this->save_delete) return array();
		if ($vl===false) $vl=$this->vl;
		
		foreach ($this->structure as $k=>$v) if ($k) {
			if ($v['type']==='table') {
				$msg=array();
				foreach ($vl[$k] as $nr=>$cl) foreach ($v['options'] as $kk=>$vv) {
					if (!empty($cl['_del'])) $vv['required']=false;
					if ($tmp=$this->validate_element($kk, $vv, $cl)) $msg[$nr.'-'.$kk]=$tmp;
					}
				if (!empty($msg)) $this->msg($k, array('label'=>$v['label'], 'msg'=>$msg));
				}
			else {
				$msg=$this->validate_element($k, $v, $vl);
				if (!empty($msg)) $this->msg($k, $msg);
				}
			}
		#<Simple spam protection element />
		if (strlen(axs_get('_captcha', $this->user_input, ''))) {
			$this->msg('_captcha', '"'.strip_tags($this->types['_captcha']['label']).'": Invalid value!');
			axs_log(__FILE__, __LINE__, 'form-spam', 'Possible spam bot');
			}
		return $this->msg;
		} #</validate()>
	function validate_element($key, $el, &$vl) { #<Validate single element />
		if (!is_array($el)) $el=$this->structure[$el];
		$error=0;
		$filled=$this->validate_element_filled($key, $el, $vl);
		if (!$filled) {
			if (empty($el['required'])) return $error;
			else return $this->msg_text(1, $el['label']);
			}
		$error=$ok=false;
		switch ($el['type']) {
			case 'email':
				$ok=(!empty($el['multiple'])) ? axs_valid::emails($vl[$key]):axs_valid::email($vl[$key]);
				if (!$ok) $error=2;
				break;
			case 'file':
				if (!empty($vl[$key])) { #<Validate upload>
					//$el=$this->element_file_data($key, $el, $vl);
					$this->element_file_accept($el);
					if (!empty($el['accept'])) { #<Check file extension if type restricted />
						if (!in_array($this->fs->f_ext($vl[$key]['name']), $el['accept'], true)) {
							$error=2;
							if (empty($el['msg'])) $el['msg']=$this->msg_text($error, $el['label']).' ('.implode(', ', $el['accept']).')';
							}
						else $ok=true;
						}
					else $ok=true;
					}
				break;
			case 'category':
			case 'multi-checkbox':
				#foreach ($el['options'] as $k=>$v) if ($vl[$key.'_'.$k]) {	$ok=true;	break;	}
				#if (!$ok) $error=2;
				break;
			case 'password':
				#if (!isset($vl[$key.'_changed'])) $vl[$key.'_changed']=false;
				if ($vl[$key]!==$vl[$key.'_changed']) {
					if ($vl[$key]!=axs_valid::id($vl[$key])) $this->msg($key.'.invalid', $this->tr->t('msg.pass_invalid'));
					}
				if ((!empty($el['min'])) && (mb_strlen($vl[$key])<$el['min'])) $this->msg($key.'.short', $this->tr->t('msg.pass_short', array('min'=>$el['min'])));
				if ((isset($vl[$key.'_confirm'])) && ($vl[$key]!=$vl[$key.'_confirm'])) $this->msg($key.'.pass_repeat', $this->tr->t('msg.pass_repeat'));
				break;
			case 'radio':
			case 'select':
				if (strlen($vl[$key]) && strlen($this->value_option_find($el['options'], $vl[$key], 'label'))) $ok=true;
				if (!$ok) $error=1;
				break;
			case 'set-checkbox':
				#foreach ((array)$el['options'] as $k=>$v) if ($vl[$key][$k]) {	$ok=true;	break;	}
				#if (!$ok) $error=2;
				break;
			case 'table':
				#if (!empty($vl[$key])) $ok=true;
				#else $error=1;
				break;
			case 'text-between':
				#if ((!strlen($vl[$key.'_1'])) && (!strlen($vl[$key.'_2']))) $error=2;
				break;
			case 'text-captcha':
				if(!axs_form_captcha::verify($vl[$key])) $error=2;
				break;
			case 'timestamp':
				if (!array_sum($vl[$key])) $error=1;
				break;
			case 'timestamp-between':
				foreach (array(1,2) as $nr) if (array_sum($vl[$key.'_'.$nr])) $ok=true;
				if (!$ok) $error=1;
				break;
			default: 
				if (!empty($el['lang-multi'])) foreach ($el['lang-multi'] as $k=>$v) {
					if ((!empty($el['required'])) && (!strlen($vl[$key.'_'.$k]))) {	$error=1;	break;	}
					}
				else {
					if (!strlen($vl[$key])) $error=1;
					}
				break;
			}
		#<Check unique value />
		if ((!empty($el['unique'])) && (!$error)) {
			if (
				axs_db_query("SELECT `id` FROM `".$this->sql_table."` WHERE `id`!='".(axs_get('id', $vl)+0)."' AND `".addslashes($key)."`='".addslashes($vl[$key])."' LIMIT 1", 'cell', $this->db, __FILE__, __LINE__)
				) $error=3;
			}
		#<Set message if value is invalid />
		if ($error) {
			if (!empty($el['msg'])) $error=$el['msg'];
			else $error=$this->msg_text($error, $el['label']);
			}
		return $error;
		} #</validate_element()>
	function validate_element_filled($key, $el, &$vl) { #<Check if input is filled. />
		if (!is_array($el)) $el=$this->structure[$el];
		$filled=null;
		switch ($el['type']) {
			case 'category':
			case 'multi-checkbox':
				if ($this->submit) $filled=false;
				foreach ((array)$el['options'] as $k=>$v) if ($vl[$key.'_'.$k]) {	$filled=true;	break;	}
				break;
			case 'file':
				if ($this->submit) $filled=false;
				if (!empty($vl[$key]['name'])) $filled=true;
				break;
			case 'radio':
			case 'select':
				if (isset($this->user_input[$key])) $filled=false;
				if (strlen($vl[$key]) && strlen($this->value_option_find($el['options'], $vl[$key], 'label'))) $filled=true;
				break;
			case 'set-checkbox':
				foreach ($el['options'] as $k=>$v) if ($vl[$key][$k]) {	$filled=true;	break;	}
				break;
			case 'table':
				if ($this->submit) $filled=false;
				if (!empty($vl[$key])) $filled=true;
				break;
			case 'text-between':
				if ((isset($this->user_input[$key.'_1'])) or (isset($this->user_input[$key.'_2']))) $filled=false;
				if ((strlen($vl[$key.'_1'])) or (strlen($vl[$key.'_2']))) $filled=true;
				break;
			case 'timestamp':
				if (isset($this->user_input[$key])) $filled=false;
				if (array_sum($vl[$key])) $filled=true;
				break;
			case 'timestamp-between':
				foreach (array(1,2) as $nr) if (isset($this->user_input[$key.'_'.$nr])) $filled=false;
				foreach (array(1,2) as $nr) if (array_sum($vl[$key.'_'.$nr])) $filled=true;
				break;
			default: 
				if (!empty($el['lang-multi'])) foreach ($el['lang-multi'] as $k=>$v) {	
					if (isset($this->user_input[$key.'_'.$k])) $filled=false;
					if (strlen($vl[$key.'_'.$k])) {	$filled=true;	break;	}
					}
				else {
					if (isset($this->user_input[$key])) $filled=false;
					if (strlen($vl[$key])) $filled=true;
					}
			}
		return $filled;
		} #</validate_element_filled()>
	#</Validate>
	
	#<Value display>
	function value_display($key, &$el, array $values, &$vr=array(), $html=true) { #<Get single element's value as text />
		global $axs;
		#if ($values===null) echo dbg($key, $el);
		if (is_string($el)) $el=$this->structure[$el];
		if (!isset($el['dir'])) $el['dir']=$this->dir_entry($values);
		if (strlen($html)>1) {
			$st='<'.$html.'>';
			$et='</'.$html.'>';
			}
		else $st=$et='';
		$type=explode('-', $el['type']);
		$vl=$this->element_value_get($this->element_fields($key, $el), $values);
		$text='';
		switch ($type[0]) {
			case 'category':
				$text=array();
				$separator=(!empty($el['sepr'])) ? $el['sepr']:' > ';
				foreach ($el['options'] as $k=>$v) {
					if (isset($v[$vl[$key.'_'.$k]])) $text[$k]=$st.$v[$vl[$key.'_'.$k]]['label'].$et;
					}
				$text=implode($separator, $text);
				if ($html) $text=axs_html_safe($text);
				break;
			case 'multi':
				switch ($type[1]) {
					default:
						$text=array();
						if (empty($el['separator'])) $el['separator']=', ';
						foreach ((array)$el['options'] as $k=>$v) {
							if ($vl[$key.'_'.$k]) $text[$k]=$st.$v['label'].$et;
							unset($vl[$key.'_'.$k]);
							}
						$text=implode($el['separator'], $text);
					}
				break;
			case 'checkbox':
				$text=(!empty($vl)) ? '+':'-';
				if ($html) $vr[$key.'.text']=(!empty($vl)) ? $el['label.html']:'';
				else $vr[$key.'.text']=(!empty($vl)) ? $el['label']:'';
				if ((!empty($vl)) && (isset($vr['_class']))) $vr['_class'][$key]=$key;
				break;
			case 'color':
				$text=(strlen($vl)) ? '#'.strtoupper(dechex($vl)):'';
				break;
			case 'content':
				break;
			case 'file':
				$el=$this->element_file_data($key, $el, $values);
				if (file_exists($el['f_fs'])) {
					$vl=$el['name_file'];
					foreach (array('url'=>'f_http', ) as $k=>$v) $vr[$key.'.'.$k]=$el[$v];
					}
				else {
					$vl='';
					foreach (array('url'=>'f_http', ) as $k=>$v) $vr[$key.'.'.$k]='';
					}
				if ($html) {
					$text.=axs_html_safe($vl);
					if (!empty($el['pic'])) $text=$this->value_display_file_t($key, $el, $el['pic'], $values);
					}
				else $text=$vl;
				if (isset($el['t'])) foreach ($el['t'] as $k=>$v) {
					$vr[$key.'.'.$k]=$this->value_display_file_t($key.' '.$k, $el, $v, $values);
					$v['link']=false;
					$vr[$key.'.'.$k.'.img']=$this->value_display_file_t($key.' '.$k, $el, $v, $values);
					$vr[$key.'.'.$k.'.url']=$v['f_http'];
					}
				break;
			case 'number':
				$text=((strlen($vl)) && ($el['sepr'])) ? number_format($vl, $el['dec']+0, $el['point'], $el['sepr']):$vl;
				break;
			case 'password':
				$text=(strlen($vl[$key])) ? '*':'';
				break;
			case 'radio':
				foreach ($el['options'] as $k=>$v) {
					if ($v['value'].''==$vl.'') {
						$text=$st.$v['label'].$et;
						break;
						}
					}
				break;
			case 'set':
				$text=array();
				$separator=(!empty($el['sepr'])) ? $el['sepr']:', ';
				if (!is_array($vl)) {
					$vl=preg_split('#,#', $vl, 0, PREG_SPLIT_NO_EMPTY);
					if (!empty($vl)) $vl=array_combine($vl, $vl);
					}
				foreach ($el['options'] as $k=>$v) if (!empty($vl[$k])) $text[$k]=$st.$v['label'].$et;
				$text=implode($separator, $text);
				break;
			case 'select':
				$text=$this->value_option_find($el['options'], $vl);
				$vr[$key.'.abbr']=axs_get('abbr'.(($html) ? '.html':''), $el['options'][$text], '');
				$text=$el['options'][$text]['label'.(($html) ? '.html':'')];
				break;
			case 'table':
				if (!isset($values[$key])) {
					$values[$key]=array();
					$tables=current($this->sql_query_element_table($values['id']));
					if ($tables) foreach ($tables[$key] as $kk=>$vv) $values[$key][$kk]=$vv;
					}
				$thead=$this->structure_get_thead($el['options']);
				$tr=array();
				if (is_array($values[$key])) foreach ($values[$key] as $rownr=>$cl) {
					foreach ($thead as $k=>$v) {
						if (!isset($v['dir'])) $v['dir']=$el['dir'];
						$cl[$k]=$this->value_display($k, $v, array_merge($values, $cl), $cl, $html);
						}
					$tr[$rownr]=$cl;
					}
				if ($html) switch (axs_get('tpl', $el, '')) {
					case '': #<table>
					case 'table':
						$th='';
						foreach ($thead as $k=>$v) $th.='<th scope="col" class="'.$k.' '.$v['type'].'">'.axs_html_safe($v['label']).'</th>';
						if (is_array($values[$key])) {
							foreach ($tr as $rownr=>$cl) {
								$tr[$rownr]=$class=array();
								foreach ($thead as $k=>$v) {
									if (($v['type']==='checkbox') && ($cl[$k]==='+')) $class[$k]=$k;
									$tr[$rownr][$k]='<td class="'.$k.' '.$v['type'].'">'.$cl[$k].'</td>';
									}
								$tr[$rownr]='<tr class="'.implode(' ', $class).'">'.implode(' ', $tr[$rownr]).'</tr>'."\n";
								}
							$tr='       '.implode('       ', $tr);
							}
						else $tr=($values[$key]) ? $values[$key]."\n":'';
						$text=
						'      <table class="table"><caption>'.axs_html_safe($el['label']).'</caption>'."\n".
						'       <tr>'.$th.'</tr>'."\n".
						$tr.
						'      </table>';
						break;
					case 'ul':
						if (is_array($values[$key])) {
							foreach ($tr as $rownr=>$cl) {
								$tr[$rownr]=array();
								foreach ($thead as $k=>$v) $tr[$rownr][$k]=$cl[$k];
								//if ($rownr===1) $vr[$key.'.1st']=implode(' ', $tr[$rownr]);
								$tr[$rownr]='<li>'.implode(' ', $tr[$rownr]).'</li>';
								}
							$tr='       '.implode("\n".'       ', $tr)."\n";
							}
						else $tr=($values[$key]) ? $values[$key]."\n":'';
						$text=
						'      <ul>'."\n".
						$tr.
						'      </ul>';
						break;
					default:
						foreach ($tr as $rownr=>$cl) {
							$text.=axs_tpl_parse($el['tpl'], $cl, __FILE__, __LINE__);
							}
						break;
					}
				else {
					if (is_array($vl)) {
						foreach ($vl as $rownr=>$cl) {
							$tr[$i]=array(1=>$i.'.', );
							foreach ($thead as $k=>$v) $tr[$i][]=$v['label'].':'.axs_get($k, $cl);
							$tr[$i]=implode('|', $tr[$i]);
							$i++;
							}
						$tr='	'.implode("\n".'	', $tr);
						}
					else $tr=($vl) ? $vl."\n":'';
					$text="\n".$tr;
					}
				break;
			case 'text':
				switch (axs_get(1, $type)) {
					case 'between':
						$text=axs_html_safe($vl[$key.'_1']);
						if ($vl[$key.'_2']) $text.=axs_html_safe($el['separator']).axs_html_safe($vl[$key.'_2']);
						break 2;
					case 'join':
						$text=axs_html_safe($vl[$key.'_text']);
						break 2;
					default:
						$text=$this->value_display_text($key, $el, $values, $html);
						break;
					}
				break;
			case 'textarea':
				$text=$this->value_display_text($key, $el, $values, $html);
				break;
			case 'timestamp':
				switch (axs_get(1, $type)) {
					case 'between':
						#$text='<time class="'.$key.'" datetime="'.$vl[$key.'_1'].'">'.$this->value_display_timestamp($el, $vl[$key.'_1']).'</time>';
						#if ($tmp=$this->value_display_timestamp($el, $vl[$key.'_2'])) $text.='-'.'<time class="'.$key.'" datetime="'.$vl[$key.'_2'].'">'.$tmp.'</time>';
						$text=$this->value_display_timestamp($el, $vl[$key.'_1']);
						if ($tmp=$this->value_display_timestamp($el, $vl[$key.'_2'])) $text.=$el['separator'].$tmp;
						break;
					case 'updated':
						if (empty($el['format'])) $el['format']=$this->types[$el['type']]['format'];
						$text=array();
						if (intval($vl[$key])) {
							$text[$key]=date($el['format'], intval($vl[$key]));
							if ($html) $text[$key]='<time class="'.$key.'" datetime="'.$vl[$key].'">'.htmlspecialchars($text[$key]).'</time>';
							}
						if (!empty($vl[$key.'_text'])) $text[$key.'_text']=($html) ? axs_html_safe($vl[$key.'_text']):$vl[$key.'_text'];
						$text=implode(' ', $text);
						break;
					default:
						$text=$this->value_display_timestamp($el, $vl);
						#$text='<time class="'.$key.'" datetime="'.$vl.'">'.$this->value_display_timestamp($el, $vl).'</time>';
						if (is_array($vl)) $vr[$key.'.datetime']=(array_sum($vl)) ? $vl['y'].'-'.$vl['m'].'-'.$vl['d'].' '.$vl['h'].':'.$vl['i'].':'.$vl['s']:'';
						else $vr[$key.'.datetime']=($vl) ? date('Y-m-d H:i:s', $vl):'';
					}
				break;
			case 'url':
				$text=$this->value_display_link($key, $el, $vl, $values, $html);
				break;
			case 'wysiwyg':
				$text=$this->value_display_text($key, $el, $values, $html);
				break;
			default: $text=(($html) && (empty($el['html']))) ? axs_html_safe($vl):$vl;
				break;
			}
		if (!empty($el['link'])) {
			if ((empty($el['lang-multi'])) or (!empty($el['lang']))) $text=$this->value_display_link($key, $el, $text, $values, $html);
			}
		return $text;
		} #</value_display()>
	function value_display_file_t($key, $el, $t, $vl=array()) {
		if (!is_array($t)) $t=$el['t'][$t];
		foreach ($t as $k=>$v) if ((is_string($v)) && (axs_tpl_has($v))) $t[$k]=axs_tpl_parse($v, self::_to_string(array_merge($el, $vl)));
		$t['title']=(strlen(axs_get('title', $t))) ? ' title="'.axs_html_safe($t['title']).'"':'';
		if (!file_exists($t['f_fs'])) $text='';
		else {
			$class=array($key, );
			if (!empty($t['size_get'])) {
				$s=getimagesize($t['f_fs']);
				$class[]=($s[1]>$s[0]) ? 'portrait':'landscape';
				}
			$text='<img class="'.implode(' ', $class).'" src="'.htmlspecialchars($t['src']).'" alt="'.axs_html_safe($t['alt']).'"'.$t['title'].' />';
			}
		if (!$text) return '';
		if (!empty($t['link'])) $text=$this->value_display_link($key, array_merge($el, array('link'=>$t['link'], 'link_target'=>axs_get('link_target', $t))), $text, $vl);
		return $text;
		} #</value_display_file_t()>
	function value_display_link($key, $el, $txt, $cl=array(), $html=true) {
		if ($el['type']==='url') $el['link']=$cl[$key];
		if ((empty($el['link'])) or (empty($txt))) return $txt;
		if (is_array($el['link'])) {
			$tmp=array('_module'=>(isset($this->m))?$this->m:false, '_form'=>(isset($this->f))?$this->f:false, '_key'=>axs_get($key, $cl), );
			foreach ($el['link'] as $k=>$v) if (axs_tpl_has($v)) $el['link'][$k]=axs_tpl_parse($v, $this->_to_string($cl+$tmp), __FILE__, __LINE__);
			$el['link']=$this->url_root.axs_url($this->url, $el['link'], false);
			}
		else {
			$link=(isset($el['t'][$el['link']])) ? '_':$el['link'];
			switch ($link) {
				case '_':
				case 'i':
					if ($el['type']==='file') {
						$el['link']=$el['t'][$el['link']]['f_http'];
						if ((isset($el['link_target'])) && (($el['link_target']==='popup')||($el['link_target']==='overlay'))) $el['link']=$this->url_root.axs_url(array('axs'=>array('gw'=>$el['link'])), array(), false);
						}
					if ($el['type']==='url') $el['link']=$cl[$key];
					break;
				case 'f':
					$el['link']=$el['f_http'];
					break;
				}
			}
		$el['class']=(!empty($el['class'])) ? $key.' '.$el['class']:$key;
		if (!empty($el['link_fragment'])) $el['link'].=$el['link_fragment'];
		if (!isset($el['link_target'])) $el['link_target']='';
		if ($el['link_target']) {
			//$el['class'].=' '.$el['link_target'];
			$el['link_target']=' target="'.$el['link_target'].'"';
			}
		if (empty($el['link_text'])) $el['link_text']=$txt;
		else {
			$tmp=array('_label'=>$el['label.html'], );
			if (axs_tpl_has($el['link_text'])) $el['link_text']=axs_tpl_parse($el['link_text'], $this->_to_string($cl+$tmp), __FILE__, __LINE__);
			}
		if ($html) return '<a class="'.$el['class'].'" href="'.htmlspecialchars($el['link']).'"'.$el['link_target'].'>'.$el['link_text'].'</a>';
		return $el['link'];
		} #</value_display_link()>
	function value_display_text($key, $el, $vl, $html=true) {
		$text=array();
		if (!empty($el['lang-multi'])) {
			if (!empty($el['lang'])) $text=$this->value_display_text_esc($el, $vl[$key.'_'.$el['lang']], $html);
			else {
				foreach ($el['lang-multi'] as $k=>$v) $text[]=($html) ? 
				'<dt lang="'.$k.'">'.$k.'</dt><dd>'.$this->value_display_text_esc($el, $vl[$key.'_'.$k], $html).'</dd>':
				$k.': '.$this->value_display_text_esc($el, $vl[$key.'_'.$k], $html);
				$text=($html) ? '<dl>'.implode('', $text).'</dl>':implode('; ', $text);
				}
			}
		else $text=$this->value_display_text_esc($el, $vl[$key], $html);
		return $text;
		} #</value_display_text()>
	function value_display_text_esc($el, $vl, $html) { #<Escape text values />
		if ($html) {
			if (($el['type']!='wysiwyg') && (empty($el['html'])) && (empty($el['bbcode']))) $vl=axs_html_safe($vl);
			if (($el['type']=='textarea') && (empty($el['html'])) && (empty($el['bbcode']))) $vl=nl2br($vl);
			}
		else {	if (($el['type']==='wysiwyg') or (!empty($el['html']))or (!empty($el['bbcode']))) $vl=strip_tags($vl);	}
		if ((axs_get('maxlength', $el)<0) && ($len_orig)) {
			if (($el['type']==='wysiwyg') or (!empty($el['bbcode']))) $vl=strip_tags($vl);
			if (mb_strlen($vl)<$len_orig) $vl.=($html) ? '&hellip;':'...';
			}
		return $vl;
		} #</value_display_text_esc()>
	function value_display_timestamp(&$el, $val) {
		if (empty($el['format'])) $el['format']=$this->types[$el['type']]['format'];
		if (is_array($val)) $val=$this->input_type_timestamp_value_int($val);
		else $val=intval($val);
		return ($val) ? date($el['format'], $val):'';
		} #</value_display_timestamp()>
	function value_option_find($el, $value, $key=false) {
		if (is_string($el)) $el=$this->structure[$el]['options'];
		$found=false;
		foreach ($el as $k=>$v) if ($v['value'].''===$value.'') {
			$found=$k;
			if ($key!==false) $found=axs_get($key, $v);
			break;
			}
		return $found;
		} #</value_option_find()>
	#</Value display>
	} #</class::axs_form>
#2009-01-16 ?>