<?php #2022-03-01
class axs_form_edit extends axs_form_header {
	public $vr=array('class'=>array('axs'=>'axs', 'form'=>'form', ), 'enctype'=>'multipart/form-data', 'method'=>'post', 'data-cfg'=>array(), 'tools'=>array(), 'elements'=>'', 'js'=>'', );
	#<Form element level functions>
	function element($n, &$el) {
		foreach (array('class','disabled','lang','multiple','readonly','required','style','title') as $v) {
			if (isset($el[$v])) {	if (!isset($el['attr'][$v])) $el['attr'][$v]=$el[$v];	}
			if (isset($el['options'])) foreach ($el['options'] as $kk=>$vv) if (isset($vv[$v])) {
				if (!isset($vv['attr'][$v])) $el['options'][$kk]['attr'][$v]=$vv[$v];
				}
			}
		foreach ($tags=array('name'=>$n,'required'=>'','label'=>'','label.html'=>'','comment_html'=>'','comment'=>'','txt'=>'') as $k=>$v) if (!isset($el[$k])) $el[$k]=$v;
		$el['required']=($el['required']) ? $this->templates('required'):'';
		if ((!empty($el['accept'])) && (!$el['comment'])) {
			$this->element_file_accept($el);
			$el['comment']='('.implode(', ', $el['accept']).')';
			}
		} #</element()>
	function element_files_del($key, $el, $vl=false) {
		$files=array();
		if (is_array($vl)) $el=$this->element_file_data($key, $el, $vl);
		$this->fs_init();
		if (!empty($el['name_file'])) $files[]=$el['name_file'];
		foreach ((array)$el['t'] as $k=>$v) $files[]=$v['name'];
		foreach ($files as $k=>$v) $this->fs->f_del($v, false);
		} #</element_files_del()>
	function element_files_save($key, $el, $vl) {
		$this->fs_init();
		if (!is_array($el)) $el=$this->structure[$el];
		$el=$this->element_file_data($key, $el, $vl);
		if (!empty($el['add_fn']) && (!empty($vl[$key]['_delete']))) return $this->element_files_del($key, $el); #<Delete files />
		#<Upload file(s)>
		if (!empty($el['dir'])) foreach ((array)$el['t'] as $k=>$v) if ((!empty($v['ext'])) && (!empty($vl[$key.'.'.$k]))) {
			if (!$this->fs->exists($v['name'])) $this->fs->rename('../'.$vl[$key.'.'.$k], $v['name']);
			}
		if (empty($vl[$key]['name'])) return;
		#<Save main file as given name or else use temp name />
		$file_save=($el['name_file']) ? $el['name_file']:false;
		$el['f']=($file_save) ? $file_save:$this->fs->f_tmp_name($vl[$key]['name']);
		$this->fs->f_upl($vl[$key], $el['f']);
		#<Make thumb files />
		foreach ((array)$el['t'] as $k=>$v) if (!empty($v['ext'])) $this->fs->img_proc($el['f'], $v['name'], $v);
		if (!$file_save) $this->fs->f_del($el['f'], false); #<Delete main file if no filename given />
		#</Upload file>
		} #</files_save()>
	function element_html($n, &$el, array &$vl=array()) {
		$this->element($n, $el);
		$el['input']=$this->element_input_html($n, $el, $vl);
		//$el['attr']=$this->attributes(axs_get('attr', $el));
		//if ($n==='lei_address_choice') echo dbg($el['attr']);
		return axs_tpl_parse(self::templates($el), $this->_to_string($el), __FILE__, __LINE__);
		} #</element_html()>
	function element_input_html($key, $el, array $vl=array()) { #<get form element's input HTML code />
		global $axs;
		if (empty($el['attr'])) $el['attr']=array();
		$n=(!empty($el['name'])) ? $el['name']:$key;
		$id=$this->id($n, $el['attr']);
		$value=axs_get($key, $vl);
		if ((!is_array($value)) && (!strlen($value))) $value=axs_get('value', $el);
		if (isset($this->types[$el['type']]['maxlength'])) {
			if ((empty($el['maxlength'])) && ($this->types[$el['type']]['maxlength'])) $el['maxlength']=$this->types[$el['type']]['maxlength'];
			if (!empty($el['maxlength'])) $el['attr']['maxlength']=$el['maxlength'];
			}
		if (!empty($el['placeholder'])) $el['attr']['placeholder']=axs_html_safe($el['label']);
		if (!isset($el['dir'])) $el['dir']=$this->dir_entry($vl);
		$input='';
		switch ($el['type']) {
			case 'button':
			case 'submit':
				if (!strlen(axs_get('value', $el))) $el['value']=$el['label.html'];
				if ((!isset($el['attr']['title'])) && (strlen(axs_get('comment', $el)))) $el['attr']['title']=$el['comment'];
				$disabled=(!empty($el['disabled'])) ? ' disabled="disabled"':'';
				$input='<input '.$id.'name="'.$n.'" type="'.$el['type'].'" value="'.$el['value'].'"'.$this->attributes($el['attr']).' />';
				$tmp=axs_get($this->key_id, $vl, 0);
				if (!empty($el['add_fn'])) $input.='<span class="delete" lang="en"><label>'.$el['comment'].'<input name="_select['.$tmp.']" type="checkbox" value="'.$tmp.'"'.$disabled.' title="delete" /></label><input name="_delete" type="submit" value="x" class="_del"'.$disabled.' title="delete" /></span>';
				break;
			case 'category':
				foreach ($el['options'] as $k=>$v) {
					if (!empty($v)) {
						$input.='	<select '.$this->id($n.'_'.$k, $v).'" name="'.$n.'_'.$k.'">'."\n".'	 <option value=""></option>'."\n";
						foreach ($v as $kk=>$vv) {
							$selected=($vv['value'] && $vl[$key.'_'.$k]==$vv['value']) ? ' selected="selected"':'';
							$input.='	 <option value="'.$vv['value'].'"'.$selected.'>'.$vv['label.html'].'</option>'."\n";
							}
						$input.='	</select>'."\n";
						}
					}
				$input.='	<input name="category_choose" type="submit" value="&gt;" class="js_hide" />';
				break;
			case 'checkbox':
				$input='<input '.$id.'name="'.$n.'" type="checkbox" value="'.axs_html_safe($el['value']).'"'.((!empty($vl[$key])) ? ' checked="checked"':'').$this->attributes($el['attr']).' />';
				if (!empty($el['attr']['data-elements-enable'])) $input.='<button type="submit">&gt;</button>';
				break;
			case 'content':
				break;
			case 'fieldset':
				$input='<fieldset '.$id.''.$this->attributes($el['attr']).'>'."\n".' <legend>'.$el['label.html'].'</legend>';
				break;
			case 'fieldset_end':
				$input='</fieldset>';
				break;
			case 'file':
				$input='';
				if (!isset($this->upload_name)) { #Create these inputs only once!
					$this->upload_name=$axs['time'];
					foreach (array('post_max_size', 'upload_max_filesize', 'max_file_uploads') as $v) $this->vr['data-cfg'][$v]=axs_filesystem::max_upl_get('B', $v);
					$input.='<input type="hidden" name="MAX_FILE_SIZE" value="'.$this->vr['data-cfg']['upload_max_filesize'].'" />'."\n";
					if ((stripos(PHP_SAPI,'FastCGI')===false) && (stripos(PHP_SAPI,'FCGI')===false)) $input.='<input type="hidden" name="'.ini_get('session.upload_progress.name').'" class="upl_id" value="'.$this->upload_name.'" disabled="disabled" />'."\n"; #This must be enabled by JS because some servers (like IIS8) will cause 500 Internal Server Error if no file is selected for upload. 
					}
				if (!$axs['file_uploads']) {
					$el['disabled']=$el['attr']['disabled']='disabled';
					$el['attr']['class'][]='upload_fake';
					}
				$el=$this->element_file_data($key, $el, $vl);
				if (!empty($el['accept'])) $el['attr']['accept']=implode(', ', (array)$el['accept']);
				if (!empty($el['multiple'])) {
					$name=$n.'[]';
					$el['attr']['multiple']='multiple';
					}
				else $name=$n;
				$fexist=is_file($el['f_fs']);
				#if ($fexist) $el['attr']['required']=false;
				#foreach ($el['t'] as $k=>$v) if (file_exists($v['f_fs'])) $el['attr']['required']=false;
				$input.='<input '.$id.'name="'.$name.'" type="file"'.$this->attributes($el['attr']).' />';
				#<Preview />
				$del=$file=$img='';
				$disabled=(!empty($el['disabled'])) ? ' disabled="disabled"':'';
				if (!empty($el['add_fn'])) $del=' <label class="_del" lang="en" title="delete file">x<input name="'.$n.'[_delete]" type="checkbox" value="x" title="delete file" /></label><br />';
				if (($el['name_file']) or ($el['name_save'])) {
					$file=($el['name_file']) ? axs_html_safe($el['name_file']):'';
					$file=($fexist) ? '<a class="file_name" href="'.axs_html_safe($el['f_http']).'" target="_blank">'.$file.'</a>':'<span class="file_name">'.$file.'</span>';
					}
				if (!empty($el['t'])) {
					reset($el['t']);
					$t=current($el['t']);
					foreach ($el['t'] as $k=>$v) {
						$v['caption']=(!empty($v['w']) && !empty($v['h'])) ? '<figcaption>'.$v['w'].'&times;'.$v['h'].'px</figcaption>':'';
						$v['img']=axs_html_safe($v['name']);
						if (is_file($v['f_fs'])) {
							if (!empty($v['dw'])) {
								foreach (array('dw'=>'width','dh'=>'height') as $kk=>$vv) {
									if ($v[$kk]) $t[$kk]=$v[$kk];
									$v[$kk]=$t[$kk];
									}
								$v['s']=axs_admin::media_size($v['f_fs'], $t['dw'], $t['dh']);
								$v['s']=' width="'.$v['s']['w'].'" height="'.$v['s']['h'].'"';
								}
							else $v['s']='';
							$v['img']='<a href="'.htmlspecialchars($v['f_http']).'" target="_blank"><img src="'.htmlspecialchars($v['f_http']).'" alt="'.$v['img'].'" title="'.$v['img'].'"'.$v['s'].' /></a>';
							}
						$img.='<figure class="thumb">'.$v['caption'].$v['img'].'</figure>';
						break;
						}
					if ($img) $img='<div>'.$img.'</div>';
					}
				if (!$fexist && !$img) $del='';
				$input.=$del.$file.$img;
				break;
			case 'hidden':
				$input=$this->input_type_hidden_html($id, $n, $value, $el);
				break;
			case 'label':
				$input='<span class="label'.$el['attr']['class'].'">'.$this->input_type_hidden_html($id, $n, $value, $el).axs_html_safe($value).'</span>';
				break;
			case 'password':
				$input=$this->input_type_text_html($id, $n, $value, $el).$this->input_type_hidden_html('', $n.'_changed', $vl[$key.'_changed']);
				break;
			case 'multi-checkbox':
			case 'set-checkbox':
				$sbtn='';
				foreach ((array)$el['options'] as $k=>$v) {
					if (isset($v['class'])) $v['attr']['class']=$v['class'];
					if ($el['type']=='set-checkbox') {
						$v['name']=$n.'['.$k.']';
						$v['checked']=(!empty($vl[$key][$k])) ? ' checked="checked"':'';
						$v['value']=$k;
						}
					else {
						$v['name']=$n.'_'.$k;
						$v['checked']=(!empty($vl[$key.'_'.$k])) ? ' checked="checked"':'';
						}
					$v['attr']['class']['option']='option';
					$input.='<label class="'.$this->id($n.'_'.$k, $v, false).'">'.
						'<input '.$this->id($n.'_'.$k, $v).' name="'.$v['name'].'" type="checkbox" value="'.axs_html_safe($v['value']).'"'.$v['checked'].$this->attributes($el['attr']).' /><span>'.$v['label.html'].
						'</span></label>'.axs_get('txt', $v);
					if (!empty($v['attr']['data-elements-enable'])) $sbtn='<button type="submit">&gt;</button>';
					}
				$input.=$sbtn;
				break;
			case 'radio':
				$readonly=(empty($el['attr']['readonly'])) ? '':$this->input_type_hidden_html('', $n, $value);
				$sbtn='';
				foreach ($el['options'] as $k=>$v) {
					if (isset($v['class'])) $v['attr']['class']=$v['class'];
					if (!empty($el['required'])) $v['attr']['required']=$el['required'];
					if (($readonly) or (!empty($el['disabled']))) {	unset($v['attr']['readonly']);	$v['attr']['disabled']='disabled';	}
					$v['attr']['class']['option']='option';
					$input.='<label'.$this->_class($v, true).'>'.$this->input_type_radio_html($this->id($n.'_'.$k, $v), $n, $value, $v).'<span>'.$v['label.html'].' <span class="comment">'.$v['comment.html'].'</span></span></label>'.axs_get('txt', $v);
					if (!empty($v['attr']['data-elements-enable'])) $sbtn='<button type="submit">&gt;</button>';
					}
				$input.=$readonly.$sbtn;
				break;
			case 'select': 
				$input=$this->input_type_select_html($id, $n, $value, $el);
				if (!empty($el['attr']['data-elements-enable'])) $input.='<button type="submit">&gt;</button>';
				break;
			case 'table':
				#<thead>
				foreach (array(
					'_nr'=>array('name'=>$key.'[0][_nr]', 'type'=>'content', 'label'=>'#', 'label.html'=>'#', 'txt'=>'', ),
					'_add'=>array('name'=>$key.'[0][_add]', 'type'=>'submit', 'label'=>'+', 'label.html'=>'+', 'lang'=>'en', 'title'=>'add', ),
					'_del'=>array('name'=>$key.'[0][_del]', 'type'=>'checkbox', 'label'=>'x', 'label.html'=>'<abbr title="delete" lang="en">x</abbr>', 'lang'=>'en', 'title'=>'delete', 'value'=>1, ),
					) as $k=>$v) if (isset($el['options'][$k])) $el['options'][$k]=$v;
				$th=$el['options'];
				if (isset($th['_add'])) $th['_add']['label.html']=$this->element_input_html('_add', $th['_add']).'<input id="'.$key.'.tpl" type="hidden"  disabled="disabled" value="'.htmlspecialchars($this->input_type_table_html_tr('{$key}', $key, $this->form_input(array(), array(), $el['options']), $el, '{$nr}')).'" />';
				foreach ($th as $k=>$v) $th[$k]='<th scope="col" class="'.$k.'">'.$v['label.html'].((!empty($v['required']))?$this->templates('required'):'').'</th>';
				$input='      <thead>'."\n".'       <tr>'.implode('', $th).'</tr>'."\n".'      </thead>'."\n";
				#</thead>
				#<tfoot>
				if ((is_array(axs_get($key, $vl))) && (isset($vl[$key]['_tfoot']))) {
					if (!isset($el['tfoot'])) $el=$this->input_type_table_html_tfoot($key, $vl[$key]['_tfoot'], $el);
					unset($vl[$key]['_tfoot']);
					}
				if (isset($el['tfoot'])) $input.=$el['tfoot'];
				#</tfoot>
				#<tbody>
				# <AJAX get new row>
				/*if (axs_get('ajax', $axs['get'])==='form.'.$this->form_id.'.'.$key) {
					$tmp=explode(',', axs_get('id', $axs['get']));
					exit($this->input_type_table_html_tr($tmp[0], $key, $this->form_input(array(), array(), $el['options']), $el, $tmp[1]));
					}*/
				# <Blank row template for adding rows>
				if (strlen(axs_get('value', $el))) {	$input.=$el['value'];	break;	}
				# <Prevent empty tbody>
				if (empty($vl[$key])) {
					$vl[$key]=array();
					$count=(!empty($el['size'])) ? $el['size']:1;
					if (!empty($el['add_fn'])) for ($nr=-1; abs($nr)<=$count; $nr--) $vl[$key][$nr]=array();
					}
				# </Prevent empty tbody>
				if (!empty($vl[$key])) {
					$input.='      <tbody>'."\n";
					$nr=1;
					foreach ($vl[$key] as $id=>$cl) {
						$input.=$this->input_type_table_html_tr($id, $key, $cl, $el, $nr);
						$nr++;
						}
					$input.='      </tbody>'."\n";
					}
				#</tbody>
				break;
			case 'text':
			case 'email':
			case 'color':
			case 'date':
			case 'datetime':
			case 'datetime-local':
			case 'month':
			case 'number':
			case 'range':
			case 'search':
			case 'tel':
			case 'time':
			case 'url':
			case 'week':
				if (!empty($el['rank'])) $el['attr']['class']['rank']='rank';
				if ($el['type']==='color') {
					foreach (array('size','maxlength') as $v) $el[$v]=$this->types['color'][$v];
					$axs['page']['head']['axs.colorpicker.js']='<script src="'.axs_dir('lib.js', 'h').'axs.colorpicker.js"></script>'."\n";
					}
				if ($el['type']==='number') foreach (array('min', 'max', 'step') as $v) if (strlen(axs_get($v, $el))) $el['attr'][$v]=$el[$v];
				if (isset($el['lang-multi'])) {
					//if (!empty($el['lang'])) $el['lang-multi']=array($el['lang']=>$el['lang-multi'][$el['lang']]);
					foreach ($el['lang-multi'] as $k=>$v) {
						$v=$el;
						$v['attr']['lang']=$k;
						$v['value']=$vl[$key.'_'.$k];
						$tmp=$this->input_type_text_html($this->id($n.'_'.$k, $v), $this->_name($n, $k), $v['value'], $v);
						$input.=(count($el['lang-multi'])>1) ? '<label><span>'.$k.'</span>'.$tmp.'</label> ':$tmp;
						}
					}
				else $input=$this->input_type_text_html($id, $n, $value, $el);
				break;
			case 'text-between':
				foreach ($input=array(1=>1,2=>2) as $k=>$v) {
					$v=(isset($vl[$key.'_'.$k])) ? $vl[$key.'_'.$k]:axs_get('value_'.$k, $el, '');
					$input[$k]=$this->input_type_text_html($this->id($n.'_'.$k, $el), $this->_name($n, $k), $v, $el);
					}
				$input=$input[1].axs_html_safe($el['separator']).$input[2];
				break;
			case 'text-captcha':
				foreach (array('type'=>'text','size'=>5,'maxlength'=>5,'required'=>'required') as $k=>$v) $el[$k]=$v;
				$input=axs_form_captcha::get().$this->input_type_text_html($id, $n, '', $el);
				break;
			case 'text-join':
				$input='<label>'.
				'<a id="'.$this->id_valid($n).'.browse_link" href="'.$this->url_root.axs_url(array_merge($this->url, $el['url']), array('value'=>$value, 'text'=>axs_get($key.'_text', $vl), )).'" target="_blank" class="browse"><span>'.axs_html_safe(axs_get($key.'_text', $vl)).'</span>#</a>'.
				$this->input_type_text_html(
					$this->id($n.'.text', $el['attr']),
					$this->_name($n, 'text'),
					axs_get($key.'_text', $vl),
					array('size'=>$el['size'], 'attr'=>array('disabled'=>'disabled', 'autocomplete'=>'off', 'class'=>'browse', ))
					).
				'</label>';
				$el['size']=(!empty($el['size2'])) ? $el['size2']:5;
				$input.='<label for="'.$this->id_valid($n).'">#</label>'.$this->input_type_text_html($id, $n, $value, $el);
				break;
			case 'textarea':
				if (!empty($el['bbcode'])) $el['attr']['class']['bbcode']='bbcode';
				if (!empty($el['lang-multi'])) foreach ($el['lang-multi'] as $k=>$v) {
					$v=$el;
					$v['attr']['lang']=$k;
					$v['value']=(isset($vl[$key.'_'.$k])) ? $vl[$key.'_'.$k]:axs_get('value_'.$k, $el, '');
					$tmp=$this->input_type_textarea_html($this->id($n.'_'.$k, $v), $this->_name($n, $k), $v['value'], $v);
					$input.=(count($el['lang-multi'])>1) ? '<label><span>'.$k.'</span>'.$tmp.'</label> ':$tmp;
					}
				else $input=$this->input_type_textarea_html($id, $n, $value, $el);
				break;
			case 'timestamp':
				$input.=$this->input_type_timestamp_html($id, $n, $value, $el);
				break;
			case 'timestamp-between':
				$el['separator']=(strlen(axs_get('separator', $el))) ? axs_html_safe($el['separator']):$this->types[$el['type']]['separator'];
				foreach ($input=array(1=>1,2=>2) as $k=>$v) {
					$v=(isset($vl[$key.'_'.$k])) ? $vl[$key.'_'.$k]:(array)axs_get('value_'.$k, $el);
					$name=$this->_name($n, $k);
					$input[$k]=$this->input_type_timestamp_html($this->id($name, $el), $name, $v, $el);
					}
				$input=$input[1].$el['separator'].$input[2];
				break;
			case 'timestamp-updated':
				$el['type']='text';
				if (empty($el['size'])) $el['size']=50;
				$el['attr']['readonly']='readonly';
				$el['value']=(!empty($vl[$key.'_text'])) ? trim($vl[$key.'_text'].' '.$value):$value;
				$input=$this->input_type_text_html($id, $n, $el['value'], $el);
				break;
			case 'wysiwyg':
				if (!isset($el['url'])) $el['url']=array('dir'=>urlencode($this->dir.$el['dir'])); #<Set URL />
				if (isset($el['lang-multi'])) foreach ($el['lang-multi'] as $k=>$v) {
					$v=$el;
					$v['attr']['lang']=$k;
					$v['value']=(isset($vl[$key.'_'.$k])) ? $vl[$key.'_'.$k]:axs_get('value_'.$k, $el).'';
					$input.='<label>'.$k.''.axs_textedit::editor($this->_name($n, $k), $v['value'], 'html', $el['url'], $v['attr']).'</label>';
					}
				else $input=axs_textedit::editor($n, $value, 'html', $el['url'], $el['attr']);
				break;
			default:
				$input='*'.$el['type'].'*@'.__CLASS__.'::'.__FUNCTION__.'('.$n.')';
				break;
			}
		return $input;
		} #</element_input_html()>
	#</Form element level functions>
	
	#<Form level functions>
	function form_action_url(array $url, $method='', array $exclude=array()) {
		if (!$method) return $this->url_root.axs_url($url, array('axs'=>[$this->key_form_id=>$this->action_target])).'#'.$this->action_target;
		$html=array();
		foreach ($url as $k=>$v) {
			if (!in_array($k, $exclude)) $html[$k]='			<input type="hidden" name="'.htmlspecialchars($k).'" value="'.htmlspecialchars($v).'" />';
			}
		return implode("\n", $html);
		} #</form_action_url();
	function form_input($values=false, $files=false, $structure=false) { #<Get all input values from form />
		global $axs;
		if ($values===false) $values=$this->user_input;
		if ($files===false) $files=$_FILES;
		if (!$axs['file_uploads']) $files=$values;
		if ($structure===false) {	$structure=$this->structure_get();	$id_fix=true;	}
		else $id_fix=false;
		$vl=array();
		foreach ($structure as $k=>$v) {	$vl+=$this->form_input_element($k, $v, $values, $files);	}
		if (($id_fix) && (isset($structure[$this->key_id])) && ($structure[$this->key_id]['type']==='number')) {
			if (empty($vl[$this->key_id])) $vl[$this->key_id]=axs_get($this->key_edit, $_GET);
			if (strlen($vl[$this->key_id])) $vl[$this->key_id]=intval($vl[$this->key_id]);
			}
		if (($this->rank) && (isset($vl[$this->rank]))) {	if (!strlen($vl[$this->rank])) $vl[$this->rank]=axs_get($this->rank, $_GET);	}
		#$this->vl=$vl;//type=table puhul rikub kogu vl
		return $vl;
		} #</form_input()>
	function form_input_element($key, $el, $values, $files=array()) { #<Get input values from single element />
		if (!is_array($el)) $el=$this->structure[$key];
		if (!isset($el['value'])) $el['value']='';
		$type=explode('-', $el['type']);
		$vl=array();
		switch ($type[0]) {
			case 'category':
			case 'multi':
				foreach ($el['options'] as $k=>$v) $vl[$key.'_'.$k]=$this->input_value_sanitize(axs_get($key.'_'.$k, $values, ''), $el);
				if ((isset($values[$key])) && (is_string($values[$key]))) $vl[$key.'_'.$values[$key]]=$values[$key];
				break;
			case 'checkbox':
				$vl[$key]=$this->input_value_sanitize(axs_get($key, $values, ''), $el);
				break;
			case 'set':
				foreach ($el['options'] as $k=>$v) {
					$vl[$key][$k]=$this->input_value_sanitize((isset($values[$key][$k])) ? $values[$key][$k]:'', $el);
					}
				if ((isset($values[$key])) && (!is_array($values[$key]))) $vl[$key][$values[$key]]=$values[$key];
				break;
			case 'file':
				$vl[$key]=(empty($el['multiple'])) ? axs_get($key, $files, array()):array('name'=>'', );
				if (isset($values[$key])) $vl[$key]+=$values[$key];
				break;
			case 'password':
				foreach (array('', '_changed') as $v) $vl[$key.$v]=axs_get($key.$v, $values, '');
				break;
			case 'table':
				$vl[$key]=array();
				$value=axs_get($key, $values, array());
				$rank=axs_get('rank_col', $el);
				$input_f=$this->structure_get_by_type('file', $el['options']);
				$nr=$add=0;
				foreach (array(0=>array())+(array)$value as $id=>$cl) {
					$nr++;
					$f=array();
					foreach ($input_f as $k=>$v) if (!empty($files[$key]['name'][$id][$k])) foreach ($files[$key] as $kk=>$vv) {
						$f[$k][$kk]=$files[$key][$kk][$id][$k];
						}
					if (($rank) && ($add)) $cl[$rank]++;
					if (($id) && (isset($value[$id]))) $vl[$key][$id]=$this->form_input($cl, $f, $el['options'])+array('_field'=>$key, '_parent_id'=>axs_get('id', $values), '_key'=>$id, );
					if (!empty($this->user_input[$key][$id.'']['_add'])) for ($i=-1; $i>-500; $i--) if (!isset($value[$i])) {
						$add=$nr;
						$tmp=($rank) ? array($rank=>$nr):array();
						$vl[$key][$i]=$this->form_input($tmp, $f, $el['options'])+array('_field'=>$key, '_key'=>$i, );
						break 1;
						}
					}
				break;
			case 'text':
			case 'textarea':
			case 'wysiwyg':
				switch (axs_get(1, $type)) {
					case 'join':
						foreach (array('','_text') as $v) $vl[$key.$v]=axs_get($key.$v, $values, '');
						break;
					case 'between':
						foreach (array(1,2) as $v) $vl[$key.'_'.$v]=$this->input_value_sanitize(axs_get($key.'_'.$v, $values, ''), $el);
						break;
					default:
						if (!empty($el['lang-multi'])) foreach ($el['lang-multi'] as $k=>$v) $vl[$key.'_'.$k]=$this->input_value_sanitize(axs_get($key.'_'.$k, $values, ''), $el);
						else $vl[$key]=$this->input_value_sanitize(axs_get($key, $values, ''), $el);
						break;
					} #</switch ($type[1])>
				break;
			case 'timestamp':
				switch (axs_get(1, $type)) {
					case 'between':
						foreach (array(1,2) as $v) $vl[$key.'_'.$v]=$this->input_type_timestamp_value_array(axs_get($key.'_'.$v, $values, array()), $v);
						break;
					case 'updated':
						$vl[$key]=axs_get($key, $values, '');
						break;
					default:
						$vl[$key]=$this->input_type_timestamp_value_array(axs_get($key, $values, array()));
						break;
					} #</switch ($type[1])>
				break;
			default:
				$vl[$key]=$this->input_value_sanitize(axs_get($key, $values, ''), $el);
				break;
			} #</switch ($type[0])>
		#<Provide default value if form not submitted />
		//if (($this->validate_element_filled($key, $el, $vl)===null) && (strlen($el['value']))) {
		//	if (!isset($this->user_input[$key])) $vl[$key]=$el['value'];
		//	}
		if (/*(!$this->submit)*/ ($this->validate_element_filled($key, $el, $vl)===null) && (!empty($el['value']))) switch ($type[0]) {
			case 'checkbox':
			case 'radio':
			case 'select':
				break;
			case 'text':
			case 'textarea':
			case 'wysiwyg':
				/*switch (axs_get(1, $type)) {
					case 'join':
						if (isset($this->user_input[$key])) break;
						if ($el['value']==='user()') $tmp=array(''=>axs_user::get('id')+0 ,'_text'=>axs_user::get('name'));
						else $tmp=array(''=>preg_replace('/ .*$/', '' ,$el['value']) ,'_text'=>preg_replace('/^[0-9]* /', '' ,$el['value']));
						foreach (array('','_text') as $v) $vl[$key.$v]=$tmp[$v];
						break;
					case 'between':
						break;
					default:
						if (isset($this->user_input[$key])) break;
						if ($el['value']==='user()') $el['value']=axs_user::get('name');
						if (empty($el['lang-multi'])) $vl[$key]=$el['value'];
						break;
					}*/ #</switch ($type[1])>
				break;
			case 'timestamp':
				/*switch (axs_get(1, $type)) {
					case 'between':
						break;
					case 'updated':
						break;
					default:
						if (isset($this->user_input[$key])) break;
						if ($el['value']==='time()') $value=time();
						else $value=array(
							'date'=>substr($el['value'], 0, 10), 'h'=>substr($el['value'], 11, 2), 'i'=>substr($el['value'], 14, 2), 's'=>substr($el['value'], 17, 2),
							);
						$vl[$key]=$this->input_type_timestamp_value_array($value);
						break;
					}*/ #</switch ($type[1])>
				break;
			default:
				if (!isset($this->user_input[$key])) $vl[$key]=$el['value'];
				break;
			} #</switch ($type[1])>
		return $vl;
		} #</form_input_element()>
	function form_parse_html($css=true, $js=true, $tpl=false) {
		global $axs, $axs_content;
		if (!$tpl) $tpl=$this->templates('form');
		foreach (array('id'=>$this->form_id, 'action'=>$this->form_action_url($this->url), ) as $k=>$v) {
			if (empty($this->vr[$k])) $this->vr[$k]=$v;
			}
		foreach ($this->vr['tools'] as $k=>$v) {
			$class='';
			if (is_array($v)) {
				if (!empty($v['class'])) $class=' '.$v['class'];
				if (is_array($v['url'])) $v['url']=$this->url_root.axs_url($this->url, $v['url'], false);
				$v='<a href="'.htmlspecialchars($v['url']).'">'.axs_html_safe($v['label']).'</a>';
				}
			$this->vr['tools'][$k]='<li class="'.$k.$class.'">'.$v.'</li>';
			}
		$this->vr['tools']=($this->vr['tools']) ? '      <ul class="tools">'.implode('', $this->vr['tools']).'</ul>':'';
		$this->vr['msg']=$this->msg_html();
		foreach ($this->structure_get() as $k=>$v) {
			if ($this->disabled) $v['disabled']='disabled';
			$this->vr['elements'].=$this->element_html($k, $v, $this->vl);
			}
		$this->vr['class']=implode(' ', $this->vr['class']);
		$this->vr['data-cfg']=htmlspecialchars(json_encode((object)$this->vr['data-cfg']));
		$this->css_js($css,$js);
		return axs_tpl_parse($tpl, $this->vr);
		} #</form_parse_html()>
	function form_save($values, $q=false) {
		//$this->form_save_dir($values);
		//$this->form_save_files($values);
		//$this->form_save_tables($values);
		} #</form_save()>
	function form_save_del($cl) {
		$table=$this->structure_get_by_type('table');
		#<Delete files />
		if ($d=$this->dir_entry($cl)) {
			$this->fs_init();
			$this->fs->dir_rm($d, false);
			}
		else {
			foreach ($this->structure_get_by_type('file') as $kk=>$vv) if (empty($vv['disabled'])) {
				$vv=$this->element_file_data($kk, $vv, $cl);
				$this->element_files_del($kk, $vv);
				}
			foreach ($table as $k=>$v) {
				foreach ($v['options'] as $kk=>$vv) if (($vv['type']==='file') && (empty($vv['disabled']))) foreach ($cl[$k] as $r) {
					$vv=$this->element_file_data($kk, $vv, $r);
					$this->element_files_del($kk, $vv);
					}
				}
			}
		if (!empty($this->msg)) return $this->msg;
		#<Delete SQL element "table" />
		if ($table) axs_db_query('DELETE FROM "'.$this->sql_table.'_table" WHERE "_parent_id"=\''.intval($cl['id']).'\'', '', $this->db, __FILE__, __LINE__);
		#<Delete SQL main />
		axs_db_query('DELETE FROM "'.$this->sql_table.'" WHERE "id"=\''.intval($cl['id']).'\'', '', $this->db, __FILE__, __LINE__);
		$this->sql_rank_set('', $this->rank, $this->sql_add);
		} #</form_save_del()>
	function form_save_dir($cl) {
		//$this->fs_init();
		if ((!empty($this->structure['']['dir'])) && (!is_dir(axs_dir('content').$this->structure['']['dir']))) {
			$this->fs_init();
			$this->fs->dir_set(axs_dir('content'));
			$this->fs->dir_mk($this->structure['']['dir']);
			$this->fs->dir_set($this->dir_fs);
			}
		if (!$d=$this->dir_entry($cl)) return false;
		$this->fs_init();
		$this->fs->dir_set($this->dir_fs);
		if (!$this->fs->exists($d, 'dir')) $this->fs->dir_mk($d);
		$error=$this->fs->dir_set($this->dir_fs.$d);
		return ($error) ? false:true;
		} #</form_save_dir()>
	function form_save_files(&$vl) {
		$this->form_save_dir($vl);
		foreach ($this->structure_get_by_type('file') as $k=>$v) $this->element_files_save($k, $v, $vl);
		} #</form_save_files()>
	function form_save_add($post=false, $append='') {
		if ($post===false) $post=$_POST;
		if (empty($post[$this->key_add])) return;
		else $key=key($post[$this->key_add]);
		$vl=$this->form_input(axs_get($key, $post['_'], array()));
		$add=array();
		if (implode('', $_FILES[$this->key_add]['name'])) { #<If import from files>
			$this->fs_init();
			$f=$this->structure_get_by_type('file');
			#<Separate data tables and other files>
			$data=$files=array();
			foreach ($_FILES[$this->key_add]['name'] as $k=>$v) if (strlen($v)) {
				if (axs_admin::f_ext($v)==='csv') $data=array_merge($data, axs_form_importexport::data_csv_import_file($_FILES[$this->key_add]['tmp_name'][$k], '', '', ',', true));
				else $files[$v]=array('name'=>$v, 'tmp_name'=>$_FILES[$this->key_add]['tmp_name'][$k], );
				}
			#</Separate data tables and other files>
			if (!empty($data)) foreach ($data as $nr=>$cl) { #<Import table and save files that are present in the table>
				if (!empty($cl['id'])) unset($this->structure['id']['readonly']);
				else $cl['id']=null;
				foreach ($f as $k=>$v) {
					if ((!empty($cl[$k])) && (!empty($files[$cl[$k]]))) $cl[$k]=$files[$cl[$k]];
					foreach((array)$v['t'] as $kk=>$vv) if ((!empty($cl[$k.'.'.$kk])) && (!empty($files[$cl[$k.'.'.$kk]]))) {
						$cl[$k]='';
						$this->fs->f_upl($files[$cl[$k.'.'.$kk]]);
						}
					}
				$add[]=array_merge($vl, $cl);
				if ($this->rank) $vl[$this->rank]++;
				} #</Import table>
			#<Import each file as a new entry if no table submitted>
			$f=key($f);
			if ((empty($data)) && ($f)) foreach ($_FILES[$this->key_add]['name'] as $k=>$v) if (strlen($v)) {
				$add[$k][$f]['name']=$v;
				foreach ($_FILES[$this->key_add] as $k=>$v) foreach ($v as $kk=>$vv) if (isset($add[$kk])) $add[$kk][$f][$k]=$vv;
				foreach ($add as $k=>$v) {
					$add[$k]=array_merge($vl, $v);
					if (isset($this->structure['title'])) foreach ($this->element_fields('title') as $kk=>$vv) $add[$k][$kk]=$add[$k][$f]['name'];
					if ($this->rank) $vl[$this->rank]++;
					}
				} #</Import each file>
			} #</If import from files>
		else $add[]=$vl; #<Else add a single item />
		#<Perform insert />
		foreach (array('_type'=>false, '_parent'=>false, ) as $k=>$v) $bak[$k]=(isset($this->sql_add[$k])) ? $this->sql_add[$k]:false;
		//exit(dbg($add));
		foreach ($add as $k=>$v) {
			set_time_limit(ini_get('max_execution_time'));
			foreach ($bak as $kk=>$vv) if (($vv!==false) && (isset($v[$kk]))) $this->sql_add[$kk]=$v[$kk];
			$vl['id']=$this->form_save_add_item($v, $append);
			if ($this->rank) $this->sql_rank_set('', $this->rank, $this->sql_add, $vl['id'], $v[$this->rank]);
			foreach ($bak as $kk=>$vv) if (($vv!==false) && (isset($v[$kk]))) $this->sql_add[$kk]=$vv;
			}
		$redir='?'.axs_url($this->url, array($this->key_edit=>$vl['id'], $this->key_id=>$vl['id']), false).'#'.$this->form_id;
		if ((count($add)>1) && (!empty($this->url['b']))) $redir=urldecode($this->url['b']);
		axs_exit(axs_redir($redir));
		} #</form_save_add()>
	function form_save_add_item($vl, $append='') {
		$vl['id']=axs_db_query(array('INSERT INTO "'.$this->sql_table.'" SET '.$this->sql_values_set($vl, '').$this->sql_add(', ', '', ', ').$append, $this->sql_p), 'insert_id', $this->db, __FILE__, __LINE__);
		$this->form_save_dir($vl);
		$this->form_save_files($vl);
		foreach ($this->structure_get_by_type('table') as $k=>$v) if (!empty($v['required'])) {
			$this->vl[$k][-1]=array();
			$this->form_save_tables($vl);
			}
		return $vl['id'];
		} #</form_save_insert_item()>
	function form_save_tables(&$values) {
		global $axs;
		foreach ($this->structure_get_by_type('table') as $k=>$v) {
			foreach ($values[$k] as $id=>$vl) if (empty($vl['_parent_id'])) $values[$k][$id]['_parent_id']=$values['id'];
			#<Delete />
			foreach ($values[$k] as $id=>$vl) if (!empty($this->user_input[$k][$id]['_del'])) {
				foreach ($this->structure_get_by_type('file', $v['options']) as $kk=>$vv) {
					$vv['dir']=$this->dir_entry($values);
					$this->element_files_del($kk, $vv, $vl);
					}
				if ((empty($this->msg)) && ($id>0)) axs_db_query("DELETE FROM `".$this->sql_table."_table` WHERE `id`='".($id+0)."' AND `_field`='".$k."' AND `_parent_id`='".($values['id']+0)."'", '', $this->db, __FILE__, __LINE__);
				unset($values[$k][$id]);
				}
			#<Add />
			foreach ($values[$k] as $id=>$vl) if ($id<0) {
				$tmp=axs_db_query("INSERT INTO `".$this->sql_table."_table` SET `id`=NULL, `_field`='".$k."', `_parent_id`='".($values['id']+0)."'", 'insert_id', $this->db, __FILE__, __LINE__);
				unset($values[$k][$id]);
				$values[$k][$tmp]=array_merge($vl, array('id'=>$tmp, '_key'=>$tmp));
				}
			#<Update />
			foreach ($values[$k] as $id=>$vl) {
				axs_db_query(array("UPDATE `".$this->sql_table."_table` SET ".$this->sql_values_set($vl, $this->sql_table.'_table', $v['options'])." WHERE `id`='".intval($id)."' AND `_field`='".$k."' AND `_parent_id`='".intval($values['id'])."'", $this->sql_p), '', $this->db, __FILE__, __LINE__);
				#<Save files />
				foreach ($this->structure_get_by_type('file', $v['options']) as $kk=>$vv) {
					$vv['dir']=$this->dir_entry($values);
					$this->element_files_save($kk, $vv, $vl);
					}
				}
			if (!empty($v['rank_col'])) $this->sql_rank_set($this->sql_table.'_table', $v['rank_col'], array('_field'=>$k, '_parent_id'=>($values['id']+0), ));
			}
		} #</form_save_tables()>
	#</Form level functions>
	
	#<Single input level functions>
	function input_type_checkbox_html($id, $n, $value, $el=array()) {
		$checked=(strlen($value)) ? ' checked="checked"':'';
		$readonly=(empty($el['attr']['readonly'])) ? '':$input.=$this->input_type_hidden_html('', $n, $value);
		if ($readonly) {	unset($el['attr']['readonly']);	$el['attr']['disabled']='disabled';	}
		$input='<input '.$id.'name="'.$n.'" type="checkbox" value="'.axs_html_safe($el['value']).'"'.$checked.$this->attributes($el['attr']).' />'.$readonly;
		return $input;
		} #</input_type_checkbox_html()>
	function input_type_hidden_html($id, $n, $value, $el=array()) {
		return '<input '.$id.'name="'.$n.'" type="hidden" value="'.axs_html_safe($value).'"'.$this->attributes(axs_get('attr', $el)).' />';
		} #</input_type_hidden_html()>
	function input_type_radio_html($id, $n, $value, $el=array()) {
		$checked=($el['value'].''===$value.'') ? ' checked="checked"':'';			
		return '<input '.$id.'name="'.$n.'" type="radio" value="'.axs_html_safe($el['value']).'"'.$checked.$this->attributes(axs_get('attr', $el)).' />';
		} #</input_type_radio_html()>
	function input_type_select_html($id, $n, $value, $el) {
		$readonly=(empty($el['attr']['readonly'])) ? '':$this->input_type_hidden_html('', $n, $value);
		if ($readonly) {	unset($el['attr']['readonly']);	$el['attr']['disabled']='disabled';	}
		$input='<select '.$id.'name="'.$n.'"'.$this->attributes(axs_get('attr', $el)).'>'."\n";
		foreach ($el['options'] as $k=>$v) {
			if (!isset($v['attr'])) $v['attr']=array();
			if (isset($v['class'])) $v['attr']['class']=$v['class'];
			$v['selected']=($v['value'].''===$value.'') ? ' selected="selected"':'';
			$input.='	<option '.$this->id($n.'_'.$k, $v['attr']).' value="'.axs_html_safe($v['value']).'"'.$v['selected'].$this->attributes($v['attr']).'>'.$v['label.html'].'</option>'."\n";
			}
		$input.='</select>'.$readonly;
		return $input;
		} #</input_type_select_html()>
	function input_type_table_html_tfoot($n, $vl, $el=false) {
		if (!$el_orig=$el) $el=$this->structure[$n];
		if (!is_array($vl)) $el['tfoot']=$vl;
		else {
			if (empty($vl)) $vl=array();
			if (!is_int(key($vl))) $vl=array(''=>$vl);
			$tr='';
			foreach ($vl as $id=>$value) {
				$td='';
				foreach ($el['options'] as $k=>$v) $td.='<td id="'.$n.'_'.$id.'_'.$k.'_container" class="element '.$k.' '.$v['type'].'">'.axs_get($k, $value, '').'</td>';
				$tr.='			<tr id="'.$n.'_'.$id.'">'.$td.'</tr>'."\n";
				}
			$el['tfoot']='		<tfoot>'."\n".$tr.'</tfoot>'."\n";
			}
		if (!$el_orig) $this->structure[$n]=$el;
		return $el;
		} #</input_type_table_html_tfoot()>
	function input_type_table_html_tr($id, $n, $value, $el, $nr) {
		$td=array();
		if (isset($el['options']['_nr'])) $el['options']['_nr']['txt']=$nr;
		foreach ($el['options'] as $k=>$v) {
			$v['name']=$n.'['.$id.']['.$k.']';
			$v['required']=(!empty($v['required'])) ? $this->templates('required'):'';
			if (!empty($value['_del'])) $v['required']=$v['attr']['required']='';
			foreach (array('dir'=>axs_get('dir', $el),'_field'=>$n,'_key'=>($id>0)?$id:'') as $kk=>$vv) if (!isset($v[$kk])) $v[$kk]=$vv;
			$td[$k]='<td id="'.$n.'_'.$id.'_'.$k.'_container" class="element '.$k.' '.$v['type'].'"><label for="'.$n.'_'.$id.'_'.$k.'" class="visuallyhidden">'.$v['label'].$v['required'].'</label> '.$this->element_input_html($k, $v, $value).axs_get('txt', $v, '').'</td>';
			}
		return '       <tr id="'.$n.'_'.$id.'"'.((!empty($value['_del']))?' class="delete"':'').'>'.implode('', $td).'</tr>'."\n";
		} #</input_type_table_html_tr()>
	function input_type_text_html($id, $n, $value, $el=array()) {
		global $axs;
		$types=array('text','color','date','datetime','datetime-local','email','month','number','password','range','search','tel','time','url','week',);
		$type=(in_array($tmp=axs_get('type', $el),  $types)) ? $el['type']:current($types);
		if ((empty($el['size'])) or ($el['size']<1)) $el['size']=10;
		$el['size']=(in_array($type, array('text', 'search', 'url', 'tel', 'email', 'password'))) ? ' size="'.$el['size'].'"':'';
		$options='';
		if ((!empty($el['attr']['data-list-url'])) && (!isset($el['options']))) $el['options']=array();
		if (isset($el['options'])) {
			//$axs['page']['head']['jquery.datalist.js']='<script src="'.axs_dir('lib.js', 'http').'jquery.datalist.js'.'"></script>'."\n";
			$el['attr']['autocomplete']='off';
			$list_id=$this->id_valid('_'.$n.'_options');
			$el['attr']['list']=$list_id;
			foreach ($el['options'] as $k=>$v) {
				if (!isset($v['value'])) $v['value']=$v['label'];
				if (!isset($v['label.html'])) $v['label.html']=axs_html_safe($v['label']);
				$options.='<option value="'.axs_html_safe($v['value']).'"'.$this->attributes(axs_get('attr', $v, array())).'>'.$v['label.html'].'</option>';
				}
			$options='<datalist id="'.$list_id.'">'.$options.'</datalist>';
			}
		return '<input '.$id.'name="'.$n.'" type="'.$type.'" value="'.axs_html_safe($value).'"'.$el['size'].$this->attributes($el['attr']).' />'.$options;
		} #</input_type_text_html()>
	function input_type_text_join_browse() {
		foreach ($this->structure_get() as $n=>$e) {
			if ($n===$this->browse) $this->input_type_text_join_browse_html($n, $e);
			if ($e['type']==='table') foreach ($e['options'] as $k=>$v) {
				if ($k===$this->browse) $this->input_type_text_join_browse_html($k, $v);
				}
			}
		} #</input_type_text_join_browse()>
	function input_type_text_join_browse_html($n, $e) { #<"text-join" value browse ui />
		global $axs;
		$ajax=axs_get('ajax', $_GET);
		if ($ajax==='value') {
			$cl=$this->sql_browse_search_value($e, axs_get('value', $_GET));
			exit(axs_get('text', $cl));
			}
		$tbody=$this->sql_browse_search($e, axs_get('text', $_GET));
		if ($this->browse_search_found>$e['limit']) $tbody[]=array(0=>'&hellip;('.$this->browse_search_found.')');
		foreach ($tbody as $r=>$cl) {
			//$cl['url']=$this->url_root.axs_url($this->url, array('value'=>axs_valid::convert($cl['value'], 'html'), 'text'=>axs_valid::convert($cl['text'], 'html')));
			$cl['url']=$this->url_root.axs_url($this->url, array('value'=>$cl['value'], 'text'=>$cl['text']));
			$cl['data-copy']=array();
			foreach ($e['data-copy'] as $k=>$v) $cl['data-copy'][$k]=$cl['data-'.$k];
			$cl['data-copy']=json_encode($cl['data-copy']);
			if (key($cl)) $cl='<td class="text"><a class="update values" href="'.$cl['url'].'" data-value="'.htmlspecialchars($cl['value']).'" data-text="'.htmlspecialchars($cl['text']).'" data-copy="'.htmlspecialchars($cl['data-copy']).'">'.axs_html_safe($cl['text']).'</a></td><td class="value">'.axs_html_safe($cl['value']).'</td>';
			else $cl='<td colspan="2" class="pager">'.$cl[0].'</td>';
			$tbody[$r]='        <tr>'.$cl.'</tr>'."\n";
			}
		$tbody=implode('', $tbody);
		if ($ajax=='result_content') exit(axs_html_doc($tbody, $e['label.html']));
		$action=array_merge($this->url, array($n.'_browse'=>''));
		foreach ($action as $k=>$v) $action[$k]='      '.$this->input_type_hidden_html('', $k, urldecode($v))."\n";
		
		$th['text']='&nbsp;';
		$table=
			'      <table>'."\n".
			'       <thead><tr><th class="text">'.$th['text'].'</th><th class="value">'.$th['value'].'</th></tr></thead>'."\n".
			'       <tbody id="result_content">'.
			$tbody.
			'       </tbody>'."\n".
			'      </table>'."\n";
		if ($ajax==='') exit(axs_html_doc($table, $e['label.html']));
		exit(axs_html_doc(
			'     <form method="get" action="?">'."\n".
			implode('', $action).
			'      <label>'.$th['text'].'<input id="text" name="text" type="text" size="10" value="'.axs_html_safe(axs_get('text', $_GET)).'" /></label>'."\n".
			'      <input name="s" type="submit" value="&gt;"" />'."\n".
			$table.
			'     </form>'."\n", 
			axs_html_safe($e['label.html'])
			));
		} #</input_type_text_join_browse_html()>
	function input_type_textarea_html($id, $n, $value, $el=array()) {
		foreach (array('cols'=>'size','rows'=>'rows') as $k=>$v) $el['attr'][$k]=((empty($el[$v])) or ($el[$v]<1)) ? $this->types['textarea'][$k]:$el[$v];
		return '<textarea '.$id.'name="'.$n.'"'.$this->attributes($el['attr']).'>'.axs_html_safe($value).'</textarea>';
		} #</input_type_textarea_html()>
	function input_type_timestamp_html($id, $n, $value, $el=array()) {
		if (empty($el['format'])) $el['format']=$this->types[$el['type']]['format'];
		$input=array(
			'date'=>array('type'=>'date', 'size'=>10, 'maxlength'=>10, 'attr'=>array('title'=>'yyyy-mm-dd'), ),
			'h'=>array('type'=>'number', 'attr'=>array('title'=>'H', 'class'=>'h', 'min'=>0, 'max'=>24, 'step'=>1), ),
			'i'=>array('type'=>'number', 'attr'=>array('title'=>'i', 'class'=>'i', 'min'=>0, 'max'=>59, 'step'=>1), ),
			's'=>array('type'=>'number', 'attr'=>array('title'=>'s', 'class'=>'s', 'min'=>0, 'max'=>59, 'step'=>1), ),
			);
		foreach ($input as $k=>$v) $input[$k]['value']=axs_get($k, $value);
		$fields=$this->input_type_timestamp_fields($el['format']);
		foreach ($input as $k=>$v) {
			foreach (array('disabled','readonly') as $vv) if (!empty($el[$vv])) $v[$vv]=$el[$vv];
			if ($fields[$k]) $input[$k]='<label for="'.$this->id($n.'_'.$k, $v, false).'" class="visuallyhidden">'.$v['attr']['title'].'</label>'.$this->input_type_text_html($this->id($n.'_'.$k, $v, true), $n.'['.$k.']', $v['value'], $v);
			else unset($input[$k]);
			}
		$tmp=axs_get('date', $input, '');
		unset($input['date']);
		return $tmp.((!empty($input)) ? ' '.implode(':', $input):'');
		} #</input_type_timestamp_html()>
	function input_type_timestamp_fields($format) {
		$f=strtolower($format);
		$fields=array('date'=>true,'h'=>false,'i'=>false,'s'=>false);
		foreach ($fields as $k=>$v) if (!$v) {
			if (($pos=strpos($f, $k))===false) continue;
			if ($f[$pos]!=='\\') $fields[$k]=true;
			}
		if (preg_match('/^H[^a-zA-Z]i$|^H[^a-zA-Z]i[^a-zA-Z]s$/', $format)) $fields['date']=false;
		return $fields;
		} #</input_type_timestamp_fields()>
	static function input_type_timestamp_value_array($val, $nr=1) { #<Correct value array and fill empty values if at least one filled. />
		$d=array(/*'date'=>'Y-m-d',*/'d'=>'d','m'=>'m','y'=>'Y','h'=>'H','i'=>'i','s'=>'s');
		$d2=array('y'=>0,'m'=>1,'d'=>2);
		if (!is_array($val)) {
			$tmp=$val;
			$val=array();
			foreach ($d as $k=>$v) $val[$k]=($tmp) ? date($v, $tmp):'';
			}
		foreach ($d as $k=>$v) if (!isset($val[$k])) $val[$k]='';
		$date=(isset($val['date'])) ? explode('-', $val['date']):array();
		foreach ($d2 as $k=>$v) if (strlen($tmp=axs_get($v, $date, ''))) $val[$k]=$tmp;
		$filled=false;
		foreach ($d as $k=>$v) if (strlen($val[$k])) $filled=true;
		if ($filled) {
			if ($nr>1) $def=array('d'=>31,'m'=>12,'y'=>date('Y'),'h'=>23,'i'=>59,'s'=>59);
			else $def=array('d'=>'01','m'=>'01','y'=>date('Y'),'h'=>'00','i'=>'00','s'=>'00');
			foreach ($d as $k=>$v) if (!strlen($val[$k])) $val[$k]=$def[$k];
			$min=array('d'=>'01','m'=>'01','y'=>1970,'h'=>'00','i'=>'00','s'=>'00');
			$max=array('d'=>31,'m'=>12,'y'=>2037,'h'=>23,'i'=>59,'s'=>59);
			if ($val['d']>28) { #<Don't allow invalid day nr />
				if ($val['d']>($tmp=date('t', mktime(1, 1, 1, intval($val['m']), 1, intval($val['y'])))+0)) $val['d']=$tmp;
				}
			foreach ($d as $k=>$v) {	if ($val[$k]<$min[$k]) $val[$k]=$min[$k];	if ($val[$k]>$max[$k]) $val[$k]=$max[$k];	}
			}
		$val['date']=array();
		foreach ($d2 as $k=>$v) if (strlen($val[$k])) $val['date'][$k]=$val[$k];
		$val['date']=implode('-', $val['date']);
		return $val;
		} #</input_type_timestamp_value_array()>
	function input_value_sanitize($val, &$el, $empty='') {
		if (!empty($el['maxlength'])) $val=mb_substr($val, 0, $el['maxlength']).'';
		$t=axs_get('value_type', $el);
		if (($el['type']==='number') or ($t==='i') or ($t==='f')) $val=(strlen($val)) ? floatval(preg_replace('/[^-(0-9)\.]/', '', str_replace(array(' ',','), array('','.'), $val))):$empty;
		if ($t==='i') $val=(strlen($val)) ? intval($val):$empty;
		return $val;
		} #</input_value_sanitize()>
	#</Single input level functions>
	
	#<SQL functions>
	function sql_browse_search($el, $str) { #<Get values for "text-join" type fields />
		$f=$el['f'];
		$tables=$join=$where=array();
		$words=$str;
		foreach ($el['concat'] as $k=>$v) {
			if (preg_match('/^\'|^"/', $v)) $words=str_replace(trim($v, '\'"'), "\n", $words);
			else $where[$v]=array();
			}
		$words=array_unique(array_merge(array($str), explode("\n", $words)));
		foreach ($where as $k=>$v) {
			$col=(axs_tpl_has($k)) ? axs_tpl_parse($k, array('l'=>$this->l, )):$k;
			foreach ($words as $kk=>$vv) if ($vv) $where[$k][]=$col." LIKE '".addslashes($vv)."%'";
			if (!empty($where[$k])) $where[$k]='('.implode(' OR ', $where[$k]).')';
			else unset($where[$k]);
			}
		$where=(!empty($where)) ? implode(' OR ', $where):'';
		if (!empty($el['sql_where'])) {	$where.=(!empty($where)) ? " AND ".$el['sql_where']:$el['sql_where'];	}
		$key=key($f);
		$t=array_shift($f);
		$tables[$t['table_alias']]=$t['table_alias'];
		foreach ($f as $k=>$v) if (!isset($tables[$v['table_alias']])) {
			$v=$this->sql_tables_join_element($key, $v, $t['table_alias'].'.');
			$join[$v['table_alias']]='LEFT JOIN `'.$v['table'].'` AS `'.$v['table_alias'].'` ON `'.$v['table_alias'].'`.`id`='.$v['join_col'];
			$tables[$v['table_alias']]=$v['table_alias'];
			}
		$data_copy=array();
		foreach ($el['data-copy'] as $k=>$v) $data_copy[$k]=$this->sql_select_concat($v)." AS `data-".$k."`";
		$data_copy=($data_copy) ? ', '.implode(', ', $data_copy):'';
		$result=axs_db_query($q="SELECT SQL_CALC_FOUND_ROWS `".$t['table_alias']."`.`id` AS `value`, ".$this->sql_select_concat($el['concat'])." AS `text`".$data_copy."\n".
		"	FROM `".$t['table']."` AS `".$t['table_alias']."`\n".
		((!empty($join)) ? "	".implode(' ', $join)."\n":'').
		((!empty($where)) ? "	WHERE ".$where."\n":'').
		"	ORDER BY `text` ASC LIMIT ".$el['limit'],
		'k', $this->db, __FILE__, __LINE__);
		$this->browse_search_found=axs_db_query('', 'found_rows', $this->db, __FILE__, __LINE__);
		return $result;
		} #</sql_browse_search()>
	function sql_browse_search_value($e, $str) { #<Check value of a "text-join" type field />
		$select=$this->sql_select_concat($e['concat']);
		$result=axs_db_query('SELECT `'.addslashes($e['join_col']).'` AS `value`, '.$select.' AS `text`'."\n".
		'	FROM `'.addslashes($e['table']).'` AS `'.addslashes($e['table_alias']).'`'."\n".
		'	WHERE `'.addslashes($e['join_col']).'`=\''.addslashes($str).'\' LIMIT 1', 'row', $this->db, __FILE__, __LINE__);
		$this->browse_search_found=(!empty($result)) ? 1:0;
		return $result;
		} #</sql_browse_search_value()>
	/*function sql_qry($sql, $spr='', $t='', $pre='') {
		if ($sql===true) $sql=$this->sql_qry;
		if ($t) $t.='.';
		foreach ($sql as $k=>$v) $sql[$k]=$t."`".$k."`='".addslashes($v)."'";
		return ((!empty($sql))?$pre:'').implode($spr, $sql);
		}*/ #</sql()>
	#<Register a parameter for using in a prepared statement and return the corresponding placeholder. />
	function sql_p($key, $val) {
		$this->sql_p[':'.$key]=$val;
		return ':'.$key;
		} #</sql_set_param()>
	function sql_structure($structure=false) {
		if (!$structure) $structure=$this->structure;
		$t=$structure['']['sql']['table'];
		$sql=array($t=>array(''=>axs_get('attr', $structure['']['sql'], array())));
		foreach ($structure as $k=>$v) if ($k) {
			if ($v['type']==='table') {
				if (!isset($sql[$t.'_table'])) $sql[$t.'_table']=array(''=>axs_get('sql_attr', $structure[''], array()))+axs_form::$types_table_cols;
				$sql[$t.'_table']['_field']['_value'][$k]=$k;
				foreach ($v['options'] as $kk=>$vv) if ($kk) $sql[$t.'_table']+=$this->sql_structure_element($kk, $vv);
				}
			else $sql[$t]+=$this->sql_structure_element($k, $v);
			}
		$pos=axs_fn::array_el_pos($sql[$t], ((isset($sql[$t]['id'])) ? 'id':''))+1;
		if (!empty($structure['']['sql']['cols'])) $sql[$t]=axs_fn::array_el_add($sql[$t], $pos, $structure['']['sql']['cols']);
		return $sql;
		} #</sql_structure()>
	function sql_structure_element($n, $el=false) {
		global $axs;
		$sql=array();
		if (isset($this->types[$el['type']]['sql'])) foreach ($this->types[$el['type']]['sql'] as $k=>$v) if (!isset($el['sql'][$k])) $el['sql'][$k]=$v;
		if (empty($el['sql'])) return array();
		if (($el['type']==='checkbox') && ($n==='_del') && (empty($el['size']))) return array();
		if (!empty($el['lang-multi'])) foreach ($axs['cfg']['site'][$this->site_nr]['langs'] as $k=>$v) $sql[$n.'_'.$k]=$el['sql'];
		else switch ($el['type']) {
			case 'category':
				for ($i=1; $i<=$this->category_levels; $i++) $sql[$n.'_'.$i]=$el['sql'];
				break;
			case 'text-between':
			case 'timestamp-between':
				foreach (array(1=>1, 2=>2) as $k=>$v) $sql[$n.'_'.$k]=$el['sql'];
				break;
			case 'timestamp-updated':
				$sql[$n]=$el['sql'];
				if (!empty($el['add_fn'])) $sql[$n.'_uid']=$el['sql'];
				break;
			default: $sql[$n]=$el['sql'];
			} #</switch ($el['type'])>
		return $sql;
		} #</sql_structure_element()>
	function sql_rank_set($table, $col, $sql=array(), $id=0, $set=0) {
		if (!$col) return;
		$id+=0;
		$set+=0;
		if ($table==='') $table=$this->sql_table;
		if ($table===true) $table=$this->sql_table.'_table';
		if (($id<1) or ($set<1)) { #<Reorder all rows>
			axs_db_query("SET @rownum:=0", '', $this->db, __FILE__, __LINE__);
			axs_db_query("UPDATE `".$table."` AS t SET t.`".$col."`=(@rownum:=@rownum+1) ".$this->sql_add(' AND ', 't', '	WHERE ', $sql, array())." ORDER BY t.`".$col."` ASC", '', $this->db, __FILE__, __LINE__);
			return;
			} #</Reorder all rows>
		#<Get target row />
		$cl=axs_db_query($q[]="SELECT t.`id`, t.`".$col."` FROM  (SELECT @rownum:=0) AS `nr`, `".$table."` AS t WHERE t.`id`='".$id."'".$this->sql_add(' AND ', 't', ' AND ', $sql, array()), 'row', $this->db, __FILE__, __LINE__);
		if (empty($cl['id'])) return;
		#<Shift rows>
		if (empty($cl[$col])) $cl[$col]=$set;
		if ($set>$cl[$col]) { #<Rank down />
			$sign='-';	$op="'".$cl[$col]."' AND '".$set."'";
			}
		else {	$sign='+';	$op="'".$set."' AND '".$cl[$col]."'";	} #<Rank up />
		axs_db_query($q[]="UPDATE `".$table."` AS t SET t.`".$col."`=(t.`".$col."`".$sign."1) WHERE t.`".$col."` BETWEEN ".$op.$this->sql_add(' AND ', 't', ' AND ', $sql, array()), '', $this->db, __FILE__, __LINE__);
		#</Shift rows>
		#<Update target row />
		axs_db_query($q[]="UPDATE `".$table."` SET `".$col."`='".$set."' WHERE `id`='".$cl['id']."'", '', $this->db, __FILE__, __LINE__);
		#<Reorder all rows />
		axs_db_query($q[]="UPDATE `".$table."` AS t SET t.`".$col."`=(@rownum:=@rownum+1)".$this->sql_add(' AND ', 't', ' WHERE ', $sql, array()).' ORDER BY t.`'.$col."` ASC", '', $this->db, __FILE__, __LINE__);
		} #</sql_rank_set()>
	function sql_rank_step($redir=false, $sql=array()) { #<Change rank by 1 step />
		if ((!$this->rank) or (!$this->save_rank)) return;
		$set=(isset($_POST[$this->key_rank][$this->save_rank]['+'])) ? $_POST['_'][$this->save_rank][$this->key_rank]+1:$_POST['_'][$this->save_rank][$this->key_rank]-1;
		$this->sql_rank_set('', $this->rank, $sql, $this->save_rank, $set);
		if ($redir) axs_exit(axs_redir($redir));
		} #</sql_rank_step()>
	function sql_values_set(&$values, $table='', $structure=false) { # get SQL update string for form elements
		global $axs, $axs_user;
		$this->sql_p=array();
		if ($structure===false) $structure=$this->structure;
		if ($table===true) {
			if (!empty($this->sql_table)) $table=$this->sql_table;
			if (!empty($structure['']['sql']['table'])) $table=$structure['']['sql']['table'];
			}
		if ($table) $table='"'.$table.'".';
		$sql=array();
		foreach ($structure as $k=>&$v) {
			//if ((empty($v['size'])) or (!empty($v['readonly'])) or (!empty($v['disabled']))) continue;
			if (!$this->sql_writable($v, $k)) continue;
			$value=axs_get($k, $values);
			switch ($v['type']) {
				case '':
				//case 'button':
				case 'content':
				case 'fieldset':
				case 'fieldset_end':
				//case 'hidden':
				case 'label':
				case 'table':
				case 'text-captcha':
				//case 'submit':
					break;
				case 'file':
					if ((!empty($v['name_save'])) && (isset($values[$k]['name']))) {
						$v=$this->element_file_data($k, $v, $values);
						$sql[]=$table."`".$k."`='".addslashes($v['name_file'])."'";
						}
					break;
				case 'category':
				case 'multi-checkbox':
					foreach ((array)$v['options'] as $kk=>$vv) $sql[]=$table.'"'.$k.'_'.$kk.'"='.$this->sql_p($k.'_'.$kk, $values[$k.'_'.$kk]);
					break;
				case 'color':
					$sql[]=$table.'"'.$k.'"='.((strlen($value)) ? $this->sql_p($k, hexdec(str_replace('#', '', $value))):'NULL');
					break;
				case 'number':
					$sql[]=$table.'"'.$k.'"='.$this->sql_p($k, $this->sql_values_set_sanitize($v, $value));
					break;
				case 'password':
					if (($values[$k]!==$values[$k.'_changed'])) $sql[]=$table.'"'.$k.'"='.$this->sql_p($k, password_hash($values[$k], PASSWORD_DEFAULT));
					$structure[$k.'_confirm']['size']=0;
					break;
				case 'set-checkbox':
					$sql[$k]=array();
					foreach ((array)$v['options'] as $kk=>$vv) if ($values[$k][$kk]) $sql[$k][$kk]=$kk;
					$sql[$k]=$table.'"'.$k.'"='.$this->sql_p($k, implode(',', $sql[$k]));
					break;
				case 'text-between':
					$sql[$k]=array();
					foreach (array(1,2) as $nr) $sql[$k][]=$table.'"'.$k."_".$nr.'"='.$this->sql_p($k.'_'.$nr, $values[$k.'_'.$nr]);
					$sql[$k]=implode(', ', $sql[$k]);
					break;
				case 'timestamp':
					$sql[]=$table.'"'.$k.'"='.$this->sql_p($k, $this->input_type_timestamp_value_int($value));
					break;
				case 'timestamp-between':
					$tmp=array(1=>'',2=>'');
					foreach ($tmp as $nr=>$vv) $tmp[$nr]=$table.'"'.$k.'_'.$nr.'"='.$this->sql_p($k.'_'.$nr, $this->input_type_timestamp_value_int($values[$k.'_'.$nr]));
					$sql[$k]=implode(', ', $tmp);
					break;
				case 'timestamp-updated':
					$sql[$k]=$table.'"'.$k.'"=\''.time().'\'';
					if (!empty($v['add_fn'])) $sql[$k].=', "'.$k.'_uid"=\''.intval(axs_get('id', $axs_user)).'\'';
					break;
				default: 
					if (!empty($v['lang-multi'])) foreach ($v['lang-multi'] as $kk=>$vv) {
						$sql[$k.'_'.$kk]=$table.'"'.$k.'_'.$kk.'"='.$this->sql_p($k.'_'.$kk, $values[$k.'_'.$kk]);
						}
					else $sql[]=$table.'"'.$k.'"='.$this->sql_p($k, $value);
					#break;
				}
			}
		return implode(', ', $sql);
		} #</sql_values_set()>
	function sql_values_set_sanitize($el, $v) {
		$t=axs_get('value_type', $el);
		if (!$t) $t=$this->types[$el['type']]['value_type'];
		switch ($t) {
			case 'i':
				if ((!empty($el['Null'])) && (!strlen($v))) return 'NULL';
				return intval(trim($v));
			case 'f':
			case 'd':
			case 'n':
				if ((!empty($el['Null'])) && (!strlen($v))) return 'NULL';
				return floatval(str_replace(array(',', ' '), array('.', ''), trim($v)));
			case 's':
			default: return $v;
			} #</switch ($t)>
		} #</sql_values_set_sanitize()>
	static function sql_writable($el, $n=false) {
		return ((!empty($el['size'])) && (empty($el['readonly'])) && (empty($el['disabled'])) && ($n!=='_captcha'));
		} #</sql_writable()>
	#</SQL functions>
	
	#<Templates>
	function templates($v) {
		$class='{$class}';
		$comment=' <span class="comment">{$comment.html}</span>';
		if (is_string($v)) {
			$type=$v;
			$v=array('type'=>$v, 'comment'=>'', 'attr'=>array(), );
			}
		else {
			$type=((isset($v['lang-multi'])) && (is_array($v['lang-multi'])) && (count($v['lang-multi'])>1)) ? 'multi':$v['type'];
			#if ((isset($v['name'])) && ($v['name'][0]==='_')) $v['attr']['class']['visuallyhidden']='visuallyhidden';
			if (empty($v['comment'])) $comment='';
			$class=(!empty($v['attr']['class'])) ? $v['attr']['class']:'';
			if (is_array($class)) $class=implode(' ', $class);
			}
		switch ($type) {
			case 'content': return '     <div id="{$name}" class="element '.$v['type'].' '.$class.'">'."\n".
				'      {$txt}'."\n".
				'     </div>'."\n";
			case 'fieldset': return "\n".' '."\n".
				'     <fieldset id="{$name}" class="'.$v['type'].' '.$class.'">'."\n".
				(($v['label'].$v['comment']) ? '      <legend>{$label.html}'.$comment.'</legend>'."\n":'').
				'      {$txt}'."\n";
			case 'fieldset_end': return '     {$txt}</fieldset>'."\n".
				'     '."\n\n";
			case 'form': return 
				'		<form id="{$id}" class="{$class}" action="{$action}" method="{$method}" enctype="{$enctype}" data-cfg="{$data-cfg}">'."\n".
				'			<input type="hidden" name="axs['.$this->key_form_id.']" value="{$id}" />'."\n".
				'			<div class="tools">'."\n".
				'				{$tools}'."\n".
				'				<input type="submit" name="_submit" value="{$_submit.lbl}" />'."\n".
				'			</div>'."\n".
				'{$msg}{$elements}'.
				'			<script>axs.form.init("{$id}");</script>'."\n".
				'{$js}'.
				'		</form>';
			case 'hidden': return '     {$input}'."\n";
			case 'required':
				if (isset($this->input_required_html)) return $this->input_required_html;
				else {
					$tmp='<em class="input_required"><abbr title="{$msg_value_required}">*</abbr></em>';
					if ($this->tr->has('msg_value_required')) return $this->input_required_html=str_replace('{$msg_value_required}', $this->tr->s('msg_value_required'), $tmp);
					return $tmp;
					}
			case 'multi':
			case 'radio':
			case 'set-checkbox':
			case 'multi-checkbox':
			case 'text-between':
			case 'timestamp':
			case 'timestamp-between':
				return '     <fieldset id="{$name}" class="element '.$v['type'].' '.$class.'">'."\n".
				'      <legend>{$label.html}{$required}'.$comment.'</legend>'."\n".
				'      {$input}{$txt}'."\n".
				'     </fieldset>'."\n";
			case 'submit': return '     <div id="{$name}_container" class="element '.$v['type'].' '.$class.'">'."\n".
				'      {$input}{$txt}'."\n".
				'     </div>'."\n";
			case 'table': return '     <div id="{$name}_container" class="element '.$v['type'].' '.$class.'">'."\n".
				'      <table id="{$name}" class="table selectable"><caption>{$label.html}{$required}'.$comment.'</caption>'."\n".
				'{$input}'.
				'      </table>{$txt}'."\n".
				'     </div>'."\n";
			default: return
				'     <div id="{$name}_container" class="element '.$v['type'].' '.$class.'">'."\n".
				'      <label for="{$name}">{$label.html}{$required}'.$comment.'</label>'."\n".
				'      {$input}{$txt}'."\n".
				'     </div>'."\n";
			}
		} #</templates()>
	#</Templates>
	
	#<Misc.>
	function attributes($a) { #<Make element's attributes. Skip empty attributes or id />
		#<Fix boolean attributes />
		foreach (array('multiple','required','readonly','disabled') as $v) {	if (!empty($a[$v])) $a[$v]=$v;	else unset($a[$v]);	}
		if ((isset($a['class'])) && (is_array($a['class']))) $a['class']=implode(' ', $a['class']);
		foreach ((array)$a as $k=>$v) {
			if ((strncmp($k, 'data-', 5)===0) && (is_array($a[$k]))) $a[$k]=json_encode($v);
			if (is_array($a[$k])) $a[$k]=implode(' ', $a[$k]);
			if (($k==='id') or ($a[$k]===false) /*or (!strlen($a[$k]))*/) unset($a[$k]);
			}
		$code='';
		foreach ((array)$a as $k=>$v) $code.=' '.htmlspecialchars($k).'="'.htmlspecialchars($v).'"';
		return $code;
		} #</attributes()>
	function bbcode($s) {
		$this->bbcode_tags=array(
			'b' => '<b>%s</b>',
			'u' => '<u>%s</u>',
			'i' => '<i>%s</i>',
			'quote' => '<blockquote>%s</blockquote>',
			'url' => '<a href="%s" target="_blank" rel="nofollow">%s</a>',
			'mail' => '<a href="mailto:%s">%s</a>',
			//'img' => '<img src="%s" alt="%s">',
			'code' => '<font color="green">%s</font>',
			'color' => '<font color="%s">%s</font>',
			'size' => '<font size="%s">%s</font>'
			//'php' => '<blockquote>%s</blockquote>'
			);
		$this->bbcode_stag=implode('|', array_keys($this->bbcode_tags));
		return $this->bbcode_nk($s);
		} #</bbcode()>
	function bbcode_nk($s) { # Call first
		return preg_replace_callback('/\[('.$this->bbcode_stag.')(=(.*))?\](.*)\[\/\1\]/isU', array($this, 'bbcode_nc'), $s);
		} #</bbcode_nk()>
	function bbcode_nc($s) {
		/*if ($s[1]=='php') {
			ob_start();
			highlight_string($s[4]);
			$s[4]=ob_get_contents();
			ob_end_clean();
			$s[4]=str_replace ('&nbsp;', ' ', $s[4]);
			return sprintf($tagid[$s[1]], $s[4]);
			// peale PHP 4.2.0 v�ib eelneva kustutada ja kirjutada:
			// return sprintf($tagid[$s[1]], highlight_string($s[4], TRUE));
			}*/
		if ($s[1]) $s[4]=$this->bbcode_nk($s[4]);
		return ($s[1]) ? sprintf($this->bbcode_tags[$s[1]], (!empty($s[3]) ? $s[3]:$s[4]), $s[4]):$s[0];
		} #</bbcode_nc()>
	static function css_js($css=true, $js=true) {
		global $axs;
		if ($css) $axs['page']['head']['axs.form.css']='<link href="'.axs_dir('lib', 'h').'axs.form.css" rel="stylesheet" type="text/css" media="screen" />'."\n";
		if ($js) {
			$axs['page']['head']['axs.form.js']='<script src="'.axs_dir('lib', 'h').'axs.form.js"></script>'."\n";
			$axs['page']['head']['jquery.js']='<script src="'.axs_dir('lib.js').'jquery.js"></script>'."\n";
			$axs['page']['head']['jquery.ui.js']='<script src="'.axs_dir('lib.js').'jquery.ui.js"></script>'."\n";
			$axs['page']['head']['jquery.ui.css']='<link type="text/css" href="'.axs_dir('lib.js').'jquery.ui.css" rel="stylesheet" />'."\n";
			}
		} #</css_js()>
	function id($n, $attr=array(), $html=true) { #<Make id attribute. $attr['id']===false: no id attribute; empty($attr['id']): generated automatically from $n; />
		if ((isset($attr['id'])) && ($attr['id']===false)) return '';
		if (empty($attr['id'])) $attr['id']=$this->id_valid($n);
		return ($html) ? 'id="'.$attr['id'].'" ':$attr['id'];
		} #</id()>
	static function id_valid($n) { #<Make a valid id from input name />
		return str_replace(array('[',']'), array('_',''), $n);
		} #</id_valid()>
	static function progress_set($key, $current=0, $total=0, $txt='') {
		if (!isset($_SESSION)) session_start();
		if (!isset($_SESSION['axs']['progress'][$key])) $_SESSION['axs']['progress'][$key]=array('complete'=>0, 'txt'=>'', );
		foreach (array('complete'=>($total) ? round($current*100/$total):0, 'txt'=>$txt, ) as $k=>$v) { $_SESSION['axs']['progress'][$key][$k]=$v;	}
		session_write_close();
		session_start();
		# Every time you call session_start(), PHP adds another identical session cookie to the response header. Clear out the duplicate session cookies. 
		/*if (headers_sent()) return;
		$cookies=array();
		#Identify cookie headers
		foreach (headers_list() as $header) {	if (strpos($header, 'Set-Cookie:') === 0) $cookies[] = $header;	}
		#Removes all cookie headers, including duplicates
		header_remove('Set-Cookie');
		#Restore one copy of each cookie
		foreach(array_unique($cookies) as $cookie) {	header($cookie, false);	}*/
		} #</progress_set()>
	static function _class($el, $wrap=false) {
		$class=(!empty($el['attr']['class'])) ? implode(' ', (array)$el['attr']['class']):'';
		if (($wrap) && ($class)) $class=' class="'.$class.'"';
		return $class;
		} #</_class()>
	static function _name($n, $add) {
		return (!preg_match('/\]$/', $n)) ? $n.'_'.$add:preg_replace('/\]$/', '_'.$add.']', $n);
		} #</_name()>
	#</Misc.>
	} #</class::axs_form_edit>
#2012-11-11 ?>