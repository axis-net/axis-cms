<?php #2016-09-27
class axs_textedit {
	static function init() {
		global $axs;
		$f='axs.textedit.js';
		$axs['page']['head'][$f]='<script src="'.axs_dir('lib', 'h').$f.'"></script>'."\n";
		} #</init()>
	static function editor($name, $source, $format, $url_edit=array(), $attr=array()) {
		global $axs;
		$id=str_replace(array('[',']'), array('_',''), $name);
		foreach ($attr as $k=>$v) $attr[$k]=($v) ? ' '.htmlspecialchars($k).'="'.htmlspecialchars($v).'"':'';
		$textarea='<textarea id="'.$id.'" name="'.$name.'" cols="50" rows="5"'.implode('', $attr).'>'.htmlspecialchars($source).'</textarea>'."\n";
		if (($format!='html') || (!empty($attr['readonly'])) || (!empty($attr['disabled']))) return $textarea;
		$url=$axs['url'];
		foreach ((array)$url_edit as $k=>$v) $url[$k]=$v;
		$axs['page']['head']['ckeditor.js']='<script src="'.axs_dir('lib.js', 'http').'ckeditor/ckeditor.js"></script>'."\n";
		return '      '.$textarea."\n".
		'      <script>'."\n".
		'		<!--'."\n".
		'		CKEDITOR.replace("'.$id.'",{'."\n".
		'			language:"'.$axs['cfg']['cms_lang'].'",'."\n".
		'			skin:"moonocolor",'."\n".
		'			filebrowserBrowseUrl:"'.'?'.axs_url($url, array('e'=>'files', 'browse'=>'link', 'axs'=>array('section'=>'content')), false).'",'."\n".
		'			filebrowserImageBrowseUrl:"'.'?'.axs_url($url, array('e'=>'files', 'browse'=>'img', 'axs'=>array('section'=>'content')), false).'",'."\n".
		'			filebrowserFlashBrowseUrl:"'.'?'.axs_url($url, array('e'=>'files', 'browse'=>'swf', 'axs'=>array('section'=>'content')), false).'",'."\n".
		'			uploadUrl:"'.'?'.axs_url($url, array('e'=>'files', 'browse'=>'upload', 'axs'=>array('section'=>'content')), false).'",'."\n".
		'			startupOutlineBlocks:true,'."\n".
		'			language_list:["en:en (English)", "et:et (Eesti)", "ru:ru (На русском)"],'."\n".
		'			extraPlugins:"'.
						'uploadimage,'.
						'uploadwidget,'.
						'notificationaggregator,'.
						'notification,'.
						'toolbar,'.
						'button,'.
						'filetools,'.
						'widget,'.
						'lineutils,'.
						'autolink,'.
						'embed,'.
						'embedbase,'.
						#'autoembed,'.
						#'embedsemantic,'.
						#'locationmap,'.
						'undo"'.
		'			});'."\n".
		'		// -->'."\n".
		'      </script>'."\n";
		} # </editor()>
	static function url($url) {
		self::init();
		foreach (array('CKEditor','CKEditorFuncNum','langCode') as $v) $url[$v]=urlencode(axs_get($v, $_GET, ''));
		return $url;
		} #</url()>
	}#</class::axs_textedit>
#2005 ?>