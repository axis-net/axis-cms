<?php #2019-05-07
class axs_sql_structure {
	static $table_def=array('ENGINE'=>'MyISAM', 'DEFAULT CHARSET'=>'utf8mb4', 'COLLATE'=>'utf8mb4_unicode_ci', 'COMMENT'=>'', );
	static $table_def_id=array('_type'=>'int', '_value'=>10, '_attribute'=>'unsigned', 'Key'=>'PRI', 'Extra'=>'AUTO_INCREMENT', );
	static $types=array(
		'txt'=>array('CHAR'=>'', 'VARCHAR'=>'', 'TINYTEXT'=>'', 'TEXT'=>'', 'MEDIUMTEXT'=>'', 'LONGTEXT'=>'', ),
		'bin'=>array('BINARY'=>'', 'VARBINARY'=>'', 'TINYBLOB'=>'', 'MEDIUMBLOB'=>'', 'BLOB'=>'', 'LONGBLOB'=>'', ),
		'pre'=>array('ENUM'=>'', 'SET'=>'', ),
		);
	public $msg=array();
	public $tables=array();
	function __construct($db_nr) {
		$this->db=$db_nr;
		} #</__construct()>
	function msg($ln='') {
		global $axs;
		$this->msg=array_merge($this->msg, $axs['db'][$this->db]->axs_error);
		} #</msg()>
	function rank_set($table, $col, $id=false, $set=0, $where='1') {
		$id+=0;
		$set+=0;
		if (!$where) axs_log(__FILE__, __LINE__, 'sql_rank', 'No WHERE caluse @ '.get_class($this).'::rank_set()', true, '---');
		if ($set<1) {
			axs_db_query('SET @rownum:=0', '', $this->db, __FILE__, __LINE__);
			axs_db_query('UPDATE `'.$table.'` SET `'.$col.'`=(@rownum:=@rownum+1) WHERE '.$where.' ORDER BY `'.$col.'` ASC', '', $this->db, __FILE__, __LINE__);
			return;
			}
		$cl=axs_db_query('SELECT t.`id`, t.`'.$col.'` FROM  (SELECT @rownum:=0) AS nr, `'.$table.'` AS t WHERE t.`id`=\''.$id.'\'', 'row', $this->db, __FILE__, __LINE__);
		if (!$cl['id']) return;
		if (!$cl[$col]) $cl[$col]=$set;
		if ($set>$cl[$col]) { #<Rank down />
			$sign='-';	$op='\''.$cl[$col].'\' AND \''.$set.'\'';
			}
		else {	$sign='+';	$op='\''.$set.'\' AND \''.$cl[$col].'\'';	}
		axs_db_query('UPDATE `'.$table.'` SET `'.$col.'`=(`'.$col.'`'.$sign.'1) WHERE `'.$col.'` BETWEEN '.$op.' AND '.$where, '', $this->db, __FILE__, __LINE__);
		axs_db_query('UPDATE `'.$table.'` SET `'.$col.'`=\''.$set.'\' WHERE `id`=\''.$cl['id'].'\'', '', $this->db, __FILE__, __LINE__);
		axs_db_query('UPDATE `'.$table.'` SET `'.$col.'`=(@rownum:=@rownum+1) WHERE '.$where.' ORDER BY `'.$col.'` ASC', '', $this->db, __FILE__, __LINE__);
		return axs_db_query('SELECT `'.$col.'` FROM  `'.$table.'` WHERE `id`=\''.$id.'\'', 'cell', $this->db, __FILE__, __LINE__);
		} #</rank_set()>
	
	function convert($from='', $to='', $tables=array(), $cols=array()) {
		global $axs;
		if (!$from) $from='latin1';
		if (!$to) $to=$axs['cfg']['db'][$this->db]['collation'];
		if (empty($tables)) $tables=$this->list_tables();
		if (!is_array($tables)) $tables=array($tables=>array());
		foreach ($tables as $k=>$v) {
			if (empty($v)) foreach ($this->list_tables_cols($k) as $kk=>$vv) if (isset(self::$types['txt'][strtoupper($vv['_type'])])) $v[$kk]=$vv;
			foreach ($v as $kk=>$vv) $v[$kk]="`".$kk."`=CONVERT(CAST(CONVERT(`".$kk."` USING  '".$from."') AS BINARY) USING '".$to."')";
			if (empty($v)) continue;
			$this->msg[]=$k.' ('.implode(', ', array_keys($v)).'): '.axs_db_query("UPDATE `".$k."` SET ".implode(', ', $v), 'affected_rows', $this->db, __FILE__, __LINE__, array('error_handle'=>1));
			}
		} #</convert()>
	function list_db($like='') { #<Get table list>
		global $axs;
		if ($like) $like=" LIKE '".$like."'";
		$list=array();
		foreach (axs_db_query("SHOW DATABASES".$like, 1, $this->db, __FILE__, __LINE__) as $cl) $list[current($cl)]=current($cl);
		return $list;
		} #</list_db()>
	function list_tables($table='', $like='') { #<Get table list>
		global $axs;
		if (!isset($this->tables)) $this->tables=array();
		if ($like) $like=" LIKE '".$like."'";
		$result=axs_db_query('SHOW TABLES'.$like, 1, $this->db, __FILE__, __LINE__);
		foreach ($result as $cl) $this->tables[current($cl)]=array();
		if ($table) return axs_get($table, $this->tables);
		return $this->tables;
		} #</list_tables()>
	function list_tables_cols($table, $field=false, $like='') { #<Get column list>
		global $axs;
		if (!isset($this->tables[$table])) $this->tables[$table]=array();
		if (empty($this->tables[$table])) {
			if ($like) $like=" LIKE '".$like."'";
			$result=axs_db_query('SHOW FULL COLUMNS FROM `'.$table.'`'.$like, 1, $this->db, __FILE__, __LINE__);
			foreach ($result as $cl) {
				$cl['_type']=preg_replace('/\(.*$/', '', $cl['Type']);
				$cl['_value']=(preg_match('/^[a-z]+\(|\) *[a-z]*$/', $cl['Type'])) ? preg_replace('/^[a-z]+\(|\) *[a-z]*$/', '', $cl['Type']):'';
				if ($cl['_type']=='enum' or $cl['_type']=='set') {
					$cl['_value']=preg_split("/','/", "',".$cl['_value'].",'", 0);
					$tmp=$cl['_value'];
					$cl['_value']=array();
					foreach ($tmp as $v) $cl['_value'][$v]=$v;
					}
				$cl['_attribute']=(preg_match('/^.*\)/', $cl['Type'])) ? trim(preg_replace('/^.*\)/', '', $cl['Type'])):'';
				$this->tables[$table][$cl['Field']]=$cl;
				}
			}
		if ($field) return axs_get($field, $this->tables[$table]);
		return $this->tables[$table];
		} #</list_tables_cols/>
	function table_alter_add($table, $cols) {
		foreach ($cols as $k=>$v) {
			$v=$this->table_create_prepare($v);
			if (!empty($v['after'])) {	$after=($v['after']===1) ? " FIRST":" AFTER `".addslashes($v['after'])."`";	}
			else $after='';
			$result=axs_db_query("ALTER TABLE `".addslashes($table)."` ADD `".addslashes($k)."` ".$this->table_create_col($v).$after, 0, $this->db, __FILE__, __LINE__);
			if (!$result) return $this->msg(__LINE__);
			}
		return true;
		} #</table_alter_add()>
	function table_alter_change($table, $col, $modify, $do=null) {
		$params=$this->list_tables_cols($table, $col);
		$params=$this->table_create_prepare($params, $modify, $do);
		return axs_db_query("ALTER TABLE `".$table."` MODIFY `".$col."` ".$this->table_create_col($params), 0, $this->db, __FILE__, __LINE__);
		} #</table_alter_change()>
	function table_alter_del($table, $col) {
		return axs_db_query('ALTER TABLE `'.$table.'` DROP `'.$col.'`', 0, $this->db, __FILE__, __LINE__);
		} #</table_alter_del()>
	function table_col_exists($table, $col=false) {
		$exists=$this->list_tables($table);
		$exists=($exists!==NULL) ? true:false;
		if ($exists && $col) {
			$exists=$this->list_tables_cols($table, $col);
			if ($exists) return true;
			}
		if ($exists) return true;
		} #</table_col_exists()>
	function table_create_prepare($col, $modify=array(), $do='add') {
		foreach (array('Type'=>'', 'Null'=>'', 'Key'=>'', 'Default'=>false, 'Extra'=>'', 'Comment'=>'', 'Collation'=>'', '_type'=>'', '_value'=>'', '_attribute'=>'', '_charset'=>'', ) as $k=>$v) if (!isset($col[$k])) $col[$k]=$v; #Prevent PHP errors, not SQL errors
		foreach ($modify as $k=>$v) if ($k!=='_value') $col[$k]=$v;
		if ($col['_type']) {
			$col['Type']=$col['_type'];
			$tmp=$col['_value'];
			if ($col['_value']!=='') {
				if ((($col['_type']==='enum') or ($col['_type']==='set')) && (is_array($col['_value']))) {
					if (isset($modify['_value'])) switch ($do) {
						case 'all':
							$col['_value']=(array)$modify['_value'];
							break;
						case 'del':
							foreach ((array)$modify['_value'] as $v) unset($col['_value'][array_search($v, $col['value'])]);
							break;
						default: $col['_value']=array_merge($col['_value'], (array)$modify['_value']);
						} #</switch ($do)>
					$tmp=array_unique($col['_value']);
					foreach ($tmp as $k=>$v) $tmp[$k]="'".str_replace("'", "\\'", $v)."'";
					$tmp=implode(',', $tmp);
					}
				$col['Type'].='('.$tmp.')';
				}
			if (strlen($col['_attribute'])) $col['Type'].=' '.$col['_attribute'];
			}
		if (empty($col['Type'])) axs_log(__FILE__, __LINE__, __CLASS__, 'Empty column type @ '.__FUNCTION__.'()', true);
		$col['Null']=(($col['Null']==='YES') or ($col['Null']===true)) ? true:false;
		#<Prevent SQL errors />
		if ((!$col['Null']) && ($col['Default']==='NULL')) $col['Default']='';
		if (!in_array($col['_type'], array('char','varchar','tinytext','text','mediumtext','longtext','enum','set',))) $col['_charset']='';
		return $col;
		} #</table_create_prepare()>
	function table_create($structure) {
		foreach ($structure as $table=>$cols) {
			$sql=$pri=array();
			foreach ($cols as $k=>$v) if ($k) {
				$v=$this->table_create_prepare($v);
				if ($v['Key']==='PRI') $pri=$k;
				$sql[]="	`".$k."` ".$this->table_create_col($v);
				}
			if ($pri) $sql[]="	PRIMARY KEY (`".$pri."`)";
			foreach (self::$table_def as $k=>$v) if (empty($cols[''][$k])) $cols[''][$k]=$v;
			$cols['']['COMMENT']="'".addslashes($cols['']['COMMENT'])."'";
			foreach ($cols[''] as $k=>$v) $cols[''][$k]=$k.'='.$v;
			$result=axs_db_query("CREATE TABLE `".$table."` (\n".implode(",\n", $sql)."\n"."	) ".implode(' ', $cols['']).";", 0, $this->db, __FILE__, __LINE__, array('error_handle'=>2));
			if (!$result) return false;
			}
		return true;
		} #</table_create()>
	function table_create_col($col) {
		$col['Null']=($col['Null']) ? "NULL":"NOT NULL";
		if (($col['Default']!==null) && ($col['Default']!==false)) {	$col['Default']=(($col['Default']===null) or ($col['Default']==='NULL')) ? ' Default NULL':" Default '".addslashes($col['Default'])."'";	}
		$col['_charset']=(!empty($col['_charset'])) ? " CHARACTER SET ".$col['_charset']." ":'';
		if ($col['Collation']) $col['Collation']=" COLLATE ".$col['Collation']." ";
		$col['Comment']="Comment '".addslashes($col['Comment'])."'";
		return $col['Type'].rtrim(" ".$col['_charset'].$col['Collation'].$col['Null']."".$col['Default']." ".$col['Extra']." ".$col['Comment']);
		} #</table_create_col()>
	function table_install($structure, $check_types=false) {
		foreach ($structure as $table=>$cols) {
			$result=$after=1;
			if (!$this->table_col_exists($table)) {
				$result=$this->table_create(array($table=>$cols));
				if (!$result) return $this->msg(__LINE__);
				}
			else foreach ($cols as $k=>$v) if ($k) {
				$c=$this->list_tables_cols($table, $k);
				if ($c) {
					$v=$this->table_create_prepare($v, $c);
					$change=false;
					if (($check_types) && ($c['Type']!==$v['Type'])) $change=true;
					if (($v['_type']==='enum') or ($v['_type']==='set')) foreach ($v['_value'] as $kk=>$vv) if (!in_array($vv, $c['_value'], true)) $change=true;
					if ($change) $result=$this->table_alter_change($table, $k, $v);
					}
				else $result=$this->table_alter_add($table, array($k=>$v+array('after'=>$after)));
				if (!$result) return $this->msg(__LINE__);
				$after=$k;
				}
			}
		return true;
		} #</table_install()>
	function tables_site($data, $site) {
		global $axs;
		$tables=array();
		foreach ($data as $table=>$cols) {
			$tmp=$cols;
			foreach ($tmp as $k=>$v) if ($k) {
				if (!empty($v['_multilang'])) {
					if ($v['_multilang']===true) {
						$v['_multilang']=array();
						foreach ($axs['cfg']['site'] as $kk=>$vv) foreach ($vv['langs'] as $kkk=>$vvv) $v['_multilang'][$kkk]=true;
						$cols=$this->tables_col_multiply($cols, $k, $v['_multilang']);
						}
					else $cols=$this->tables_col_multiply($cols, $k, $axs['cfg']['site'][$site]['langs']);
					}
				if (($table==='users') && ($k==='g')) $cols=$this->tables_col_multiply($cols, $k, $axs['cfg']['groups']);
				}
			$tables[$axs['cfg']['site'][$site]['prefix'].$table]=$cols;
			}
		return $tables;
		} #</tables_site()>
	function tables_col_multiply($cols, $col, $multiply) {
		global $axs;
		$new=array();
		foreach ($cols as $k=>$v) {
			if ($k===$col) foreach ($multiply as $kk=>$vv) $new[$k.'_'.$kk]=$v;
			else $new[$k]=$v;
			}
		return $new;
		} #</tables_col_multiply()>
	} #</class::axs_sql_structure>
#2010-01-02 ?>