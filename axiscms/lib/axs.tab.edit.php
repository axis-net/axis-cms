<?php #2020-03-18
class axs_tab_edit extends axs_admin {
	var $structure=array(
		'publ'=>array('type'=>'timestamp', 'format'=>'d.m.Y H:i', ),
		'hidden'=>array('type'=>'timestamp', 'format'=>'d.m.Y H:i', ),
		'fup'=>array('type'=>'file', ),
		);
	static $media_pref=array(
		'media'=>'', 'img__proc'=>'', 'img__w'=>'', 'img__h'=>'', 'img_s_proc'=>'', 'img_s_w'=>'', 'img_s_h'=>'', 'img_s_q'=>'', 'mp_w'=>'', 'mp_h'=>'', 'mp_play'=>'',
		);
	function __construct($id, $site_nr, $d='', $l, $pref=array(), $css=true, $tr=array()) {
		global $axs, $axs_user;
		$this->id=$id;
		$this->site_nr=$site_nr;
		$this->d=$d;
		$this->dir=axs_dir('content').$axs_user['home']['dir'].$this->d;
		$this->pref=$pref;
		$this->url_root='?';
		$this->b_url='';
		if ($css) {
			if ($css===true) $css=axs_dir('lib', 'h').'axs.tab.edit.css';
			$axs['page']['head']['edit-tab']='<link href="'.$css.'" rel="stylesheet" type="text/css" media="screen" />'."\n";
			}
		$this->tr=axs_tr::load_class_tr(__CLASS__, $l);
		if ($tr) $this->tr->set($tr);
		$this->form=new axs_form_edit($this->site_nr, $axs['url'], $this->structure, $this->d, $_POST);
		$this->msg=&$this->form->msg;
		} #</__construct()>
	static function b_redir() {	if ($this->b_url) exit(axs_redir($this->b_url));	} #</b_redir()>
	static function esc($str, $nlrepl='<br />') {
		$str=str_replace(array('|', '<?', '?>', '<%', '%>'), array('&#124;', '&lt;?', '&gt;?', '&lt;%', '&gt;?'), $str);
		if ($nlrepl===false) $nlrepl='<br />';
		return preg_replace("/(\r\n)+|(\n\r)+|\n+/", $nlrepl, $str);
		} #</esc()>
	function content_edit($table, $cmd, $input) {
		global $axs, $axs_user;
		$id=($this->id) ? $this->id.'_':'';
		$vl=$this->form->form_input($input);
		$cells=array();
		foreach (array('publ','hidden') as $v) {
			if (isset($input[$v])) $cells[0][$v]=$this->form->input_type_timestamp_value_int($vl[$v]);
			}
		if (is_array($input['cells'])) foreach($input['cells'] as $k=>$v) $cells[$k]=$v;
		#<Options>
		$this->b_url=$input['b'];
		$format=$cmd[$id.'format'];
		$tmp=explode(',', $cmd[$id.'edit']);
		$opts=array(
			'editrow'=>$tmp[0]+0, 'editcell'=>$tmp[1]+0, 'moverow'=>$input['moverow']+0,
			'pref_save'=>axs_get('pref_save', $input),  'pref_clear'=>axs_get('pref_clear', $input),
			);
		if (!empty($input['add'])) {
			$opts['addrow']=$opts['editrow'];
			$cells[0]['publ']=$axs['time'];
			}
		if (!empty($input['move_up'])) $opts['moverow']=$opts['editrow']-1;
		if (!empty($input['move_down'])) $opts['moverow']=$opts['editrow']+1;
		if ($opts['moverow'] && $opts['moverow']!=$opts['editrow']) $this->b_url=preg_replace('/#row'.$opts['editrow'].'$/', '#row'.$opts['moverow'], $this->b_url);
		if (!empty($input['del_confirm'])) $opts['delrow']=$input['del'];
		#</Options>
		#<If file&media editor>
		if ($format=='file') {
			$fs=new axs_filesystem_img($this->dir, $this->site_nr);
			$fs->msg=&$this->form->msg;
			$editcell=array_pop(array_keys($cells));
			$http=$axs['http_root'].$axs['site_dir'].$axs['dir_c'].$axs_user['home']['dir'].$this->d;
			if ((!empty($_FILES['fup']['name'])) or (!empty($_POST['fup']['name']))) { #<Handle file upload>
				$fs->f_upl('fup');
				if (empty($fs->msg)) {
					$fupl=$fs->f;
					$f=$http.$fs->f;
					}
				} #</Handle file upload>
			else {
				$fupl=false;
				$f=$input['f'];
				}
			$f2=$input['f2'];
			
			if ($f) { #<set media if file was added>
				$fname=basename($f); # only file name of $f
				$title_url=urlencode($input['title']);
				$title=htmlspecialchars($input['title']);
				$t=(strlen($title)) ? $title:htmlspecialchars($fname);
				#<detect media type>
				$media=($input['media']) ? $input['media']:'media';
				if ($media==='media') {
					$f_ext=$fs->f_ext($f);
					$media='hlink';
					if (in_array($f_ext, $fs->f_types['image.*'])) $media='img';
					if (in_array($f_ext, $fs->f_types['media.*'])) $media='mp';
					if ($f_ext=='swf') $media='flash';
					if ($f_ext=='js') $media='js';
					} #</detect media type>
				
				if ($media=='img') {
					if ($fupl) { #<If file uploaded>
						$settings=axs_admin::media_settings($this->site_nr, $input, $axs['c']);
						if (($settings['img']['0']['proc']) or ($settings['img']['s']['proc'])) {
							$tmp=$fs->img_proc_ui_save($fupl, $settings['img'], axs_admin::$media_defaults);
							if (!empty($tmp['t'])) {
								$f=$http.$tmp['t'];
								if (!empty($tmp['s'])) $f2=$http.$tmp['s'];
								}
							else {
								if (!empty($tmp['s'])) $f=$http.$tmp['s'];
								}
							}
						} #</If file uploaded>
					$alt=(strlen($t)) ? $t:basename($f);
					if ($f2) {
						$cells[$opts['editcell']]='<!--file--><figure class="image"><a class="axs file-media img" href="?'.urlencode('axs[gw]').'='.urlencode($f2).'&amp;alt='.$title_url.'" target="'.htmlspecialchars($input['img']['s']['proc']).'"><img src="'.$f.'" alt="'.$alt.'" /></a><figcaption>'.$t.'</figcaption></figure><!--/file-->'.$cells[$editcell];
						}
					else $cells[$opts['editcell']]='<!--file--><figure class="image"><img src="'.$f.'" alt="'.$alt.'" /><figcaption>'.$t.'</figcaption></figure><!--/file-->'.$cells[$opts['editcell']];
					} #</$media=='img'>
				elseif ($media=='mp') $cells[$opts['editcell']]=axs_admin::media_html($f, $t).'	'.$cells[$opts['editcell']];
				elseif ($media=='flash') {
					list($w, $h)=@getimagesize(file_exists($this->dir.$fname) ? $this->dir.$fname:$f);
					if (!$w) $w=$media_defaults['mp_w'];
					if (!$h) $h=$media_defaults['mp_h'];
					$cells[$opts['editcell']]=axs_admin::media_html($f, $t, $w, $h).'	'.$cells[$opts['editcell']];
					} #</$media=='flash'>
				elseif ($media=='hlink') $cells[$opts['editcell']]=axs_admin::media_html($f, $t).'	'.$cells[$opts['editcell']];
				elseif ($media=='js') $cells[$opts['editcell']]=axs_admin::media_html($f, $t).'	'.$cells[$opts['editcell']];
				else $cells[$opts['editcell']]=$f.$cells[$opts['editcell']];
				} #</set media if file was added>
			else { #<if no file added, attempt to modify existing media>
				$title_url=urlencode($input['title']);
				$title_old_url=urlencode($input['title_old']);
				$title=htmlspecialchars($input['title']);
				$title_old=htmlspecialchars($input['title_old']);
				if ($title!==$title_old) $cells[$opts['editcell']]=str_replace(
					array('<!--file-->'.$title_old.'<!--/file-->','alt="'.$title_old.'"','&amp;alt='.$title_old_url.'"'),
					array('<!--file-->'.$title.'<!--/file-->','alt="'.$title.'"','&amp;alt='.$title_url.'"'),
					$cells[$opts['editcell']]
					);
				$f2=urlencode($input['f2']);
				$f2_old=urlencode($input['f2_old']);
				if ($f2!==$f2_old) $cells[$opts['editcell']]=str_replace('href="?axs%5Bgw%5D='.$f2_old, 'href="?axs%5Bgw%5D='.$f2, $cells[$opts['editcell']]);
				} #</if no file added, attempt to modify existing media>
			#<if save settings requested />
			if ($opts['pref_save']) $opts['pref']=self::tab_edit_meta_pref($input);
			} #</If file&media editor>
		#<Update content only if no error occured />
		if (empty($this->form->msg)) $table=$this->tab_edit($table, $cells, $format, $opts);
		else $this->b_url='';
		//$axs['msg_error']+=$this->msg;
		return $table;
		} #</edit_content()>
	static function pref_get($data, $get=true) {
		if (!is_array($data)) $data=axs_tab::proc_split_tab($data);
		$pref=null;
		if (isset($data[1])) {
			$cl=self::tab_split_row($data[1]);
			$pref=axs_get('pref', $cl[0], '');
			}
		if ($get) {
			parse_str($pref, $pref);
			if ($get!==true) $pref=$pref[$get];
			}
		return $pref;
		} #</pref_get()>
	static function pref_set($data, $pref) {
		if (!is_array($data)) $data=axs_tab::proc_split_tab($data);
		if (!isset($data[1])) return $data;
		$is_array=is_array($data[1]);
		//if ($is_array) $data[1]=self::tab_split_row($data[1]);
		if (is_array($pref)) $pref=self::tab_edit_meta_pref($pref);
		$cl=self::tab_split_row($data[1]);
		$cl[0]=implode(',', self::tab_edit_meta($cl[0], 1, array('pref'=>$pref)));
		$data[1]=($is_array) ? $cl:self::tab_merge_row($cl);
		return $data;
		} #</pref_set()>
	static function tab_edit($data, $cells, $format, $cmd) {
		global $axs, $axs_user;
		$tmp1=$data;
		foreach (array('addrow','editrow','moverow','delrow','pref','pref_clear', ) as $v) ${$v}=(isset($cmd[$v])) ? $cmd[$v]:false;
		if (!is_array($data)) $data=axs_tab::proc_split_tab($data); #<Make data an array of rows />
		$cols=1;
		$pref=false;
		if (isset($data[1])) { #<Count columns, store meta pref and clear it from table. />
			$cl=self::tab_split_row($data[1]);
			$cols=count($cl)-1;
			$pref=self::pref_get($data, false);
			$data=self::pref_set($data, '');
			}
		if (!empty($cmd['pref_save'])) $pref=$cmd['pref'];
		if (!empty($cmd['pref_clear'])) $pref='';
		#<secure new cells>
		foreach ($cells as $k=>$v) {
			if (is_array($v)) $v=implode(',', $v);
			$cells[$k]=self::esc($v, ($format=='html') ? '	':false);
			}
		#</secure new cells>
		#<editrow>
		if ($editrow && !$addrow) {
			$cl=self::tab_split_row($data[$editrow]);
			foreach ($cells as $k=>$v) $cl[$k]=$v;
			$cl[0]=implode(',', self::tab_edit_meta($cl[0], 0, array('user'=>$axs_user['user'], 'updated'=>$axs['time'])));
			$data[$editrow]=self::tab_merge_row($cl);
			} #</editrow>
		#<delrow>
		if ($delrow) {
			unset($data[$delrow]);
			$data=array_values(array('')+$data);
			unset($data[0]);
			if (empty($data)) {
				$cl=array_pad(array(), $cols+1, '');
				$cl[0]=implode(',', self::tab_edit_meta($cl[0], 0, array('publ'=>$axs['time'], 'user'=>$axs_user['user'], 'updated'=>$axs['time'])));
				$data[1]=self::tab_merge_row($cl);
				}
			} #</delrow>
		#<moverow>
		if ($moverow && $moverow!==$editrow && isset($data[$editrow])) {
			if (isset($data[$moverow])) {
				if ($moverow>$editrow) $data[($moverow-1).'.1']=$data[$moverow];
				else $data[$moverow.'.1']=$data[$moverow];
				}
			$data[$moverow]=$data[$editrow];
			unset($data[$editrow]);
			ksort($data);
			$data=array_values(array('')+$data);
			unset($data[0]);
			} #</moverow>
		#<addrow>
		if ($addrow) {
			if (isset($data[$addrow])) $data[$addrow.'.1']=$data[$addrow];
			$cl=array_pad(array(), $cols+1, '');
			foreach ($cells as $k=>$v) $cl[$k]=$v;
			$cl[0]=implode(',', self::tab_edit_meta($cl[0], 0, array('publ'=>$axs['time'], 'user'=>$axs_user['user'], 'updated'=>$axs['time'])));
			$data[$addrow]=self::tab_merge_row($cl);
			ksort($data);
			$data=array_values(array('')+$data);
			unset($data[0]);
			} #</addrow>
		
		#<restore pref />
		if ($pref) $data=self::pref_set($data, $pref);
		return $data;
		} #</tab_edit()>
	static function tab_edit_meta($meta, $rownr=1, $val=array()) { #<Get and set row metadata />
		if (!is_array($meta)) {
			$tmp=explode(',', $meta);
			$meta=array();
			foreach (axs_tab::$index_meta as $k=>$v) $meta[$k]=axs_get($v, $tmp, '');
			}
		foreach (axs_tab::$index_meta as $k=>$v) {
			if (!isset($meta[$k])) $meta[$k]='';
			if (isset($val[$k])) $meta[$k]=str_replace(',', '.', $val[$k]);
			}
		foreach (array('publ','hidden') as $v) if (!$meta[$v]) $meta[$v]='';
		if ($rownr!=1) $meta['pref']='';
		return $meta;
		} #</tab_edit_meta()>
	static function tab_edit_meta_pref($input) {
		$out=array();
		if (!empty($input['pref_clear'])) return '';
		foreach (array('media'=>'')+axs_admin::$media_defaults as $k=>$v) {
			if (!is_array($v)) $out[$k]=(isset($input[$k])) ? $input[$k]:$v;
			else foreach ($v as $kk=>$vv) {
				if (is_array($vv)) $vv=key($vv);
				$out[$k][$kk]=(isset($input[$k][$kk])) ? $input[$k][$kk]:$vv;
				}
			}
		foreach (axs_admin::$media_defaults['img'] as $k=>$v) if (!strlen($out['img'][$k]['proc'])) $out['img'][$k]['proc']='0';
		foreach (array('mp_play'=>'') as $k=>$v)  if (!strlen($out[$k])) $out[$k]='0';
		return self::esc(http_build_query($out, '', '&'));
		} #</tab_edit_meta_pref()>
	static function tab_merge($table) {
		$str='';
		foreach ($table as $k=>$v) {
			if (is_array($v)) $v=self::tab_merge_row($v);
			$str.=$v;
			}
		return $str;
		} #</tab_merge()>
	static function tab_merge_row($cl) {
		if (is_string($cl)) return $cl;
		if ((isset($cl[0])) && (is_array($cl[0]))) $cl[0]=implode(',', $cl[0]);
		$cl='<?php #|'.implode('|', $cl).'|?>'."\n";
		return $cl;
		} #</tab_merge_row()>
	/*static function tab_split($table, $split_rows=0) { #<Make data an array of rows />
		if (!is_array($table)) {
			$table=preg_split('~'."\n".'~', '-'."\n".$table, -1, PREG_SPLIT_NO_EMPTY);
			unset($table[0]);
			foreach ($table as $k=>$v) $table[$k].="\n";
			}
		if ($split_rows) foreach ($table as $k=>$cl) if (!is_array($cl)) $table[$k]=self::tab_split_row($cl, $split_rows);
		return $table;
		}*/ #</tab_split()>
	static function tab_split_row($cl, $split_meta=2) {
		if (!is_array($cl)) {
			$cl=explode('|', $cl);
			array_pop($cl); # remove first and last cell (PHP start and end tags)
			array_shift($cl);
			}
		if ($split_meta>=2) $cl[0]=self::tab_edit_meta($cl[0]);
		return $cl;
		} #</tab_split_row()>
	function ui($table, $edit, $input, $url=array(), $test_url=false) {
		global $axs;
		$this->form->css_js(false, true);
		$id=($this->id) ? $this->id.'_':'';
		$tpl=array(
			'toolbar'=>
			'       <tr id="row{$rownr}" class="tools row {$status}">'."\n".
			'        <th id="row{$rownr}_tools" scope="colgroup" colspan="{$cols}">'."\n".
			'         <form action="{$formact_url}" method="post">'."\n".
			'          <fieldset>'."\n".
			'{$msg}          <a href="#" class="top">&nbsp;&nbsp;&nbsp;&uarr;</a>'."\n".
			'           {$edit} -'."\n".
			'           <label>'.$this->tr('move_lbl').'<input name="move_up" type="submit" value="&uarr;" title="'.$this->tr('move_up_lbl').'"{$disabled} /></label><label><span class="visuallyhidden">'.$this->tr('move_lbl').'</span><input name="move_down" type="submit" value="&darr;" title="'.$this->tr('move_down_lbl').'"{$disabled} /></label> -'."\n".
			'           <input name="add" type="submit" value="'.$this->tr('add_lbl').'" /> -'."\n".
			'           <label class="js_hide"><input name="del" type="checkbox" value="{$rownr}"{$disabled} />{$del_lbl}</label><input name="del_confirm" type="submit" value="'.$this->tr('del_lbl').'"{$disabled} /> -'."\n".
			'           {$times}'."\n".
			'           <input name="b" type="hidden" value="{$b}{$rownr}" />'."\n".
			'           <input name="axs_save" type="hidden" value="" />'."\n".
			'          </fieldset>'."\n".
			'         </form>'."\n".
			'        </th>'."\n".
			'       </tr>'."\n",
			);
		
		$edit_target=axs_get($id.'edit', $edit, '');
		if ($edit_target) { #<If edit requested>
			$editcell=explode(',', $edit[$id.'edit']);
			$editrow=(!empty($editcell[0])) ? intval($editcell[0]):'';
			if (!empty($input['add']) or !empty($input['move_up']) or !empty($input['move_down']) or !empty($input['del'])) $editrow='';
			$editcell=(!empty($editcell[1])) ? intval($editcell[1]):'';
			} #</If edit requested>
		else $editcell=$editrow='';
		$format=axs_get($id.'format', $edit);
		
		if (!is_array($table)) $table=axs_tab::proc_split_tab($table); #<Make data an array of rows />
		
		if (!empty($table[1])) { #<Find nr of columns and preferences config>
			$cells=self::tab_split_row($table[1]);
			$cols=count($cells)-1;
			if (!$this->pref) $this->pref=axs_get('pref', $cells[0], '');
			}
		else $cols=1;
		#</Find nr of columns and preferences config>
		
		$b=$this->url_root.axs_url($url, array(), false).'#row';
		$data='';
		$toolbar=array();
		$rownr=0;
		foreach($table as $rownr=>$cells) { #<Cycle over all rows>
			$cells=self::tab_split_row($cells); #<Make row an array>
			#<Row metadata and status>
			$r=$cells[0];
			unset($cells[0]);
			$r['updated']=($r['updated']) ? date('d.m.Y H:i:s', $r['updated']):'';
			$cl=array('publ'=>$r['publ'], 'hidden'=>$r['hidden'], );
			if ($cl['publ'] or $cl['hidden']) { #<Row status>
				$cl['times']=($cl['publ']+0) ? date('d.m.Y H:i', $cl['publ']+0):'';
				if ($cl['publ'] && $cl['hidden']) $cl['times'].='-';
				if ($cl['hidden']) $cl['times'].=date('d.m.Y H:i', $cl['hidden']);
				if (!$cl['publ'] or ($cl['publ'] && $cl['publ']>$axs['time']) or ($cl['hidden'] && $cl['hidden']<$axs['time'])) $cl['status']='hidden';
				else $cl['status']='publ';
				}
			else {
				$cl['times']='';
				$cl['status']='locked';
				} #</Row status>
			$cl['times']=$this->tr($cl['status'].'_lbl').($cl['times'] ? ' ':'').$cl['times'];
			$cl['updated']=$this->tr('modified_lbl').': '.$r['user'].' '.$r['updated'];
			#</Row metadata and status>
			
			$cl['msg']=(intval($edit_target)===$rownr) ? $this->form->msg_html():'';
			
			#<Edit> ------------------------------
			if ($rownr===$editrow) {
				$this->form->vl=$this->form->form_input($input);
				if ($format!==null) {
					if ($editcell) { #<edit single cell>
						$content=$this->ui_edit_cell($cells, $editcell, $format, $input, $url);
						} #</edit single cell>
					else { #<edit entire row>
						$content=$this->ui_edit_row($cells, $cols, $input);
						} #</edit entire row>
					$cl['msg']=$this->form->msg_html();
					$url_edit=$rownr;
					$title=$rownr.'. '.$this->tr('row_lbl');
					if ($editcell) {
						$url_edit.=','.$editcell;
						$title.=' '.$editcell.'. '.$this->tr('cell_lbl');
						}
					$tmp=$r;
					$data.=
					'       <tr>'."\n".
					'        <td colspan="'.$cols.'" class="editor">'."\n".
					'         <form id="edit_tab_form" action="'.$this->url_root.axs_url($url, array($id.'edit'=>$url_edit, $id.'format'=>$format, )).'#edit_tab_form" method="post" enctype="multipart/form-data" class="clear-content">'."\n".
					'          <div id="form_head">'."\n".
					$cl['msg'].
					'           <a id="row_close" href="'.$this->url_root.axs_url($url, array('edit'=>false, )).'#row'.$rownr.'">'.$title.' ('.$this->tr('close_lbl').')</a>'."\n".
					'           <a id="lock_lbl" href="#publ_date">'.$this->tr('lock_lbl').'</a> |'."\n". 
					'           <a id="publ_lbl" href="#publ_date">'.$this->tr('publish_lbl').'</a>'."\n".
					'           '.$this->form->element_input_html('publ', $this->structure['publ'], array('publ'=>$this->form->input_type_timestamp_value_array($tmp['publ'])))."\n".
					'           <a id="hidden_lbl" href="#hidden_date">'.$this->tr('hide_lbl').'</a>'."\n".
					'           '.$this->form->element_input_html('hidden', $this->structure['hidden'], array('hidden'=>$this->form->input_type_timestamp_value_array($tmp['hidden'])))."\n".
					'          </div>'."\n".
					'          <div class="content">'."\n".
					$content."\n".
					'          </div>'."\n".
					'          <div id="save_container">'."\n".
					'           <input type="submit" name="edit-tab_save" value="'.$this->tr('save_lbl').'" />'."\n".
					'           <input name="axs_save" type="hidden" value="" />'."\n".
					'           <input name="pref" type="hidden" value="'.htmlspecialchars($this->pref).'" />'."\n".
					'           <input name="b" type="hidden" value="'.htmlspecialchars($b.$rownr).'" />'."\n".
					'           <label for="moverow">'.$this->tr('ranknr_lbl').'</label>'."\n".
					'           <input id="moverow" name="moverow" type="number" min="1" step="1" value="'.$rownr.'" /> '."\n".
					'           '.$this->tr('modified_lbl').': <b>'.$r['user'].'</b> '.$r['updated']."\n".
					'          </div>'."\n".
					'         </form>'."\n".
					'         <script>axs.form.init("edit_tab_form");</script>'."\n".
					'        </td>'."\n".
					'       </tr>'."\n";
					}
				else $cl['msg']=$this->form->msg_html();
				} #</Edit>
			else { #<If don't edit>
				$cl['cells_tools']=$cl['cells_content']='';
				foreach($cells as $cellnr=>$cell) { #<cycle over all cells>
					$cl['cells_tools'].='<th id="row'.$rownr.'_cell'.$cellnr.'_tools" scope="col" headers="row'.$rownr.'_tools" class="cell_tools">'.$cellnr.') '.
					'<a href="'.$this->url_root.axs_url($url, array($id.'edit'=>$rownr.','.$cellnr, $id.'format'=>'html')).'#edit_tab_form">'.$this->tr('edit_txt').'&nbsp;html&gt;&gt;</a> '.
					'<a href="'.$this->url_root.axs_url($url, array($id.'edit'=>$rownr.','.$cellnr, $id.'format'=>'file')).'#edit_tab_form">'.$this->tr('file&media_txt').'&gt;&gt;</a> '.
					'<a href="'.$this->url_root.axs_url($url, array($id.'edit'=>$rownr.','.$cellnr, $id.'format'=>'txt')).'#edit_tab_form">'.$this->tr('txt_txt').'&gt;&gt;</a>'.
					'</th>';
					if (!strlen($cell)) $cell.='&nbsp;';
					$cl['cells_content'].='<td headers="row'.$rownr.'_tools row'.$rownr.'_cell'.$cellnr.'_tools">'.$cell.'</td>';
					} #</cycle over all cells>
				$toolbar['cols']=$cols;
				$toolbar['rownr']=$rownr;
				$toolbar['status']=$cl['status'];
				$toolbar['formact_url']=$this->url_root.axs_url($url, array($id.'edit'=>$rownr, $id.'format'=>$format, ));
				$toolbar['msg']=$cl['msg'];
				$toolbar['edit']='<a href="'.$this->url_root.axs_url($url, array($id.'edit'=>$rownr, $id.'format'=>'txt')).'#edit_tab_form'.'">'.$rownr.'. '.$this->tr('row_lbl').'&gt;&gt;</a>';
				$toolbar['del_lbl']=$this->tr('del_confirm_lbl');
				$toolbar['updated']=$cl['updated'];
				$toolbar['times']='('.$cl['times'].')';
				$toolbar['b']=htmlspecialchars($b);
				$toolbar['disabled']='';
				if ($cl['status']=='locked') {
					$toolbar['disabled']=' disabled="disabled"';
					$toolbar['del_lbl']=$this->tr('del_cannot_lbl');
					}
				$toolbar['times']=($test_url) ? '<a href="'.htmlspecialchars($test_url).'" class="status" title="'.$toolbar['updated'].'" target="_blank">'.$toolbar['times'].'</a>':'<span class="status" title="'.$toolbar['updated'].'">'.$toolbar['times'].'</span>';
				$data.=axs_tpl_parse($tpl['toolbar'], $toolbar).
				'       <tr class="tools cell '.$cl['status'].'">'.$cl['cells_tools'].'</tr>'."\n".
				'       <tr class="'.$cl['status'].'">'.$cl['cells_content'].'</tr>'."\n";
				} #</If don't edit>
			} #</cycle over all rows>
		$rownr++;
		$toolbar['cols']=$cols;
		$toolbar['rownr']=$rownr;
		$toolbar['status']='locked';
		$toolbar['formact_url']=$this->url_root.axs_url($url, array($id.'edit'=>$rownr, $id.'format'=>$format, ));
		$toolbar['msg']=(intval($edit_target)===$rownr) ? $this->form->msg_html():'';
		$toolbar['edit']=$rownr.'. '.$this->tr('row_lbl').'&gt;&gt;';
		$toolbar['del_lbl']=$this->tr('del_confirm_lbl');
		$toolbar['times']='';
		$toolbar['b']=htmlspecialchars($b);
		$toolbar['disabled']=' disabled="disabled"';
		$data.=axs_tpl_parse($tpl['toolbar'], $toolbar);
		
		//$axs['msg_error']+=$this->msg;
		/*if (!$editrow) {
			$msg=(is_array($this->msg)) ? '       <caption class="msg">'.implode('<br />       '."\n", $this->msg).'</caption>'."\n":'';
			}*/
		return '      <table'.(($this->id)?' id="'.$this->id.'"':'').' class="edit-tab" border="1">'."\n".
			//$msg.
			'       <tbody>'."\n".
			$data.
			'       </tbody>'."\n".
			'      </table>'."\n".
			'      <script src="'.axs_dir('lib', 'h').'axs.tab.edit.js"></script>';
		} #</ui()>
	function ui_edit_row(&$cells, $cols, $input) {
		if ($cols<1) $cols=1;
		$cl=array_pad(array(0=>''), $cols+1, '');
		unset($cl[0]);
		foreach ($cl as $k=>$v) $cl[$k]=(isset($input[$k])) ? $input[$k]:$cells[$k];
		$th=$fields='';
		foreach ($cl as $k=>$v) {
			$th.='<th id="cell_'.$k.'_h" scope="col"><label for="cells_'.$k.'">'.$k.')</label></th>';
			$v=preg_replace('#<br />|<br/>|<br>#i', "\n", $v);
			$fields.='              <td headers="cell_'.$k.'_h"><textarea id="cells_'.$k.'" name="cells['.$k.']" cols="25" rows="10">'.htmlspecialchars($v).'</textarea></td>';
			}
		return '           <table class="row_edit">'."\n".
		'            <thead><tr>'.$th.'</tr></thead>'."\n".
		'            <tbody>'."\n".
		'             <tr>'."\n".
		$fields.
		'             </tr>'."\n".
		'            </tbody>'."\n".
		'           </table>'."\n";
		} #</ui_edit_row()>
	function ui_edit_cell(&$cells, $editcell, $format, $input, $url) {
		global $axs;
		if ($format=='file') {
			$cl=array();
			/*if ($axs['file_uploads']) $cl['fup_disabled']='';
			else {
				$cl['fup_disabled']=' disabled="disabled"';
				$this->form->msg('fupl_disabled', $this->tr('msg_fupl_disabled'));
				}*/
			$cl['cell']=htmlspecialchars($cells[$editcell]);
			if ($cl['cell']) $this->form->msg('use_empty_cell', $this->tr('msg_use_empty_cell'));
			
			//$cl+=$this->media_settings($this->site_nr, (!empty($input)) ? $input:$this->pref, $axs['c']);
			#$cl+=$this->media_settings($this->site_nr, $input, $axs['c']);
			$pref=array();
			$tmp=array('media'=>'media', 'hlink'=>'hlink', 'none'=>'none', );
			$pref['media']=(!empty($tmp[axs_get('media', $pref)])) ? $pref['media']:'media';
			$cl['media_media']=$cl['media_hlink']=$cl['media_none']='';
			$cl['media_'.$pref['media']]=' checked="checked"';
			
			/*if (!$cl['img_proc_disabled']) $cl['img_proc_enabled']='';
			else {
				$cl['img_proc_enabled']=' readonly="readonly"';
				$this->form->msg('img_proc_disabled', $this->tr('msg_img_proc_disabled'));
				}*/
			
			//$cl['browse']=$this->url_root.axs_url($url, array('e'=>'files', 'dir'=>$this->d, 'popup'=>'', 'browse'=>'', ));
			$cl['browse']=array_merge($url, array('e'=>'files', 'dir'=>$this->d, 'browse'=>'', 'axs'=>array('section'=>'content')));
			$tmp=array('title','f2','pref_save','pref_clear');
			foreach ($tmp as $v) $cl[$v]='';
			if (!empty($input)) foreach ($tmp as $v) $cl[$v]=htmlspecialchars($input[$v]);
			else {
				if (($start=strpos(' '.$cells[$editcell], '<!--file-->')) && ($end=strpos($cells[$editcell], '<!--/file-->'))) {
					$cl['title']=substr($cells[$editcell], $start+10, $end-$start-10);
					if ($start=strpos($cl['title'], 'alt="')) {					
						if (strpos(' '.$cl['title'], '<a href="'.$this->url_root.urlencode('axs[gw]='))) $cl['f2']=substr($cl['title'], 16, strpos($cl['title'], '&amp;txt=')-16);
						$cl['title']=substr($cl['title'], $start+5);
						$cl['title']=substr($cl['title'], 0, strpos($cl['title'], '"'));
						}
					}
				}
			$cl['f2']=urldecode($cl['f2']);
			foreach (array('pref_save','pref_clear') as $v) if ($cl[$v]) $cl[$v]=' checked="checked"';
			#dbg($this->media_settings($this->site_nr, $input, $axs['c']));
			return '           <fieldset id="file"><legend>'.$this->tr('file_add_lbl').'</legend>'."\n".
			'            <label id="fup_1_lbl" for="fup_1" class="clickable">'.$this->tr('file_upload_lbl').' (max '.axs_filesystem::max_upl_get('MiB').'MiB)</label>'."\n".
			'            <input type="hidden" name="MAX_FILE_SIZE" value="'.axs_filesystem::max_upl_get().'" />'."\n".
			//'            <input name="fup" type="file" id="fup_1" size="15"'.$cl['fup_disabled'].' />'."\n".
			'            '.$this->form->element_input_html('fup', $this->form->structure['fup'], $this->form->vl)."\n".
			'            <input name="f" type="text" id="f" size="50" />'."\n".
			'            <label for="f">'.$this->tr('file_or_txt').' <a id="f_lbl" href="'.$this->url_root.axs_url($cl['browse'], ['browse'=>'f'], true).'" target="popup">'.$this->tr('file_browse_server_lbl').'</a></label>'."\n".
			'            <a id="ftp" href="'.$this->url_root.axs_url($url, array('e'=>'ftp', 'dir'=>$this->dir, 'popup'=>'', )).'" target="_blank">FTP</a>'."\n".
			'           </fieldset>'."\n".
			'           <fieldset id="media_choose"><legend>'.$this->tr('file_media_choose_lbl').'</legend>'."\n".
			'            <label><input name="media" type="radio" id="media_media" value="media"'.$cl['media_media'].' />'.$this->tr('file_media_media_lbl').'</label><br />'."\n".
			'            <label><input type="radio" name="media" value="hlink" id="media_hlink"'.$cl['media_hlink'].' />'.$this->tr('file_media_hlink_lbl').'</label><br />'."\n".
			'            <label><input type="radio" name="media" value="none" id="media_none"'.$cl['media_none'].' />'.$this->tr('file_media_none_lbl').'</label><br />'."\n".
			'           </fieldset>'."\n".
			'           <fieldset id="media_settings"><legend>'.$this->tr('file_media_pref_lbl').'</legend>'."\n".
			'            <fieldset class="row no-border">'."\n".
			'             <label for="title">'.$this->tr('file_meida_title_lbl').'</label><input name="title" type="text" id="title" size="40" maxlength="255" value="'.$cl['title'].'" />'."\n".
			'             <input name="title_old" type="hidden" id="title_old" value="'.$cl['title'].'" />'."\n".
			'            </fieldset>'."\n".
			axs_filesystem_img::img_proc_ui($this->media_settings($this->site_nr, $input, $axs['c'])).
			'            <fieldset class="row no-border">'."\n".
			'             <label for="f2"><a href="'.$this->url_root.axs_url($cl['browse'], ['browse'=>'f2'], true).'" target="popup">'.$this->tr('file_link_server_lbl').'</a></label><input name="f2" type="text" id="f2" value="'.$cl['f2'].'" size="10" />'."\n".
			'             <input name="f2_old" type="hidden" id="f2_old" value="'.$cl['f2'].'" />'."\n".
			'            </fieldset>'."\n".
			'            <fieldset class="no-border">'."\n".
			'             <input name="pref_save" type="checkbox" id="pref_save" value="1"'.$cl['pref_save'].' />'."\n".
			'             <label for="pref_save">'.$this->tr('file_media_pref_save_lbl').'</label>'."\n".
			'             <input name="pref_clear" type="checkbox" id="pref_clear" value="1"'.$cl['pref_clear'].' />'."\n".
			'             <label for="pref_clear">'.$this->tr('file_media_pref_clear_lbl').'</label>'."\n".
			'             <input name="cells['.$editcell.']" type="hidden" id="cells_'.$editcell.'" value="'.$cl['cell'].'" />'."\n".
			'             <input name="httproot" type="hidden" id="httproot" value="'.htmlspecialchars($axs['http_root']).'" />'."\n".
			'            </fieldset>'."\n".
			'           </fieldset>';
			}
		else {
			if ($format=='txt') $cells[$editcell]=preg_replace('#<br />|<br/>|<br>#i', "\n", $cells[$editcell]);
			if (!isset($url['dir'])) $url['dir']=$this->d;
			return '           <label for="cells_'.$editcell.'">'.$editcell.')</label>'."\n".
			'           '.axs_textedit::editor('cells['.$editcell.']', $cells[$editcell], $format, $url);
			}
		} #</ui_edit_cell()>
	} #</class:axs_tab_edit>
#2005-06 ?>