<?php #2021-08-11
class axs_user {
	static $px='';
	static $table='';
	static $user_length=255;
	static $pass_length=255;
	static $pass_min=4;
	static $login_update=600; # If interval not defined, set to 600 seconds
	static $pass_tmp_expire=86400;
	static $fields=array(
		'id'=>'', 'lock'=>false, 'user'=>true, 'pass'=>true, 'fstname'=>'', 'lstname'=>'', 'email'=>'', 'phone'=>'', 'online'=>'', 'g'=>array(), 'homedirs'=>array(), 
		);
	static $fields_tmp=array('pass_tmp'=>'', 'pass_tmp_expire'=>'', 'online'=>'', );
	static function init($u=array()) {
		global $axs, $axs_user;
		if (defined('AXS_LOGINCHK')) return AXS_LOGINCHK;
		if ((isset($axs['page']['head'])) && (is_array($axs['page']['head']))) $axs['page']['head']['axs.user.js']='<script src="'.axs_dir('lib', 'http').'axs.user.js"></script>'."\n";
		if (!strlen($axs['cfg']['login_update'])) $axs['cfg']['login_update']=self::$login_update; # If interval not defined, set to 600 seconds
		self::$px=$axs['cfg']['db'][$axs['cfg']['users_db']]['px'];
		self::$table=self::$px.'users';
		$axs_user=array();
		if ($u) { #<Login process>
			if ($u=self::verify($u)) { # Set user session
				if ($axs['session()']) { # use PHP session if available
					if (!isset($_SESSION)) session_start();
					if (axs_function_ok('session_regenerate_id')) session_regenerate_id();
					}
				self::session_set($u);
				self::update_online($axs['time']); # update users table to show user as online
				}
			} #</Login process>
		else { #<Verify user session>
			#<Get user session data>
			if ($axs['session()']) { #<use session if available />
				if (!isset($_SESSION)) session_start();
				$axs_user=(!empty($_SESSION['axs_'.$axs['http_root']]['user'])) ? $_SESSION['axs_'.$axs['http_root']]['user']:array();
				} 
			else { #<else use coockies />
				foreach (array('id','user','pass','online') as $v) $axs_user[$v]=axs_get('axs_user_'.$v, $_COOKIE);
				}
			#</Get user session data>
			#<Validate login if session is old or no PHP session available />
			if (((!empty($axs_user['user'])) && (!empty($axs_user['pass']))) && (!defined('AXS_LOGOUT'))) {
				# Revalidate session if last login validation was enaough time ago. 
				# If PHP session support not available, always revalidate login. 
				$offline=$axs['time']-$axs['cfg']['login_update']; #<The time when user session is old enaugh to be revalidated />
				if (($axs_user['online']<=$offline) or (!$axs['session()'])) {
					$axs_user=self::verify($axs_user);
					if ($axs_user) { #<Renew session if valid />
						self::session_set($axs_user);
						self::update_online($axs['time']);
						}
					}
				}
			setcookie('axs_lastcheck', $axs['time'], $axs['time']+2592000, $axs['http_root']);
			} #</Verify user session>
		define('AXS_LOGINCHK', ((!empty($axs_user['user'])) && (!empty($axs_user['pass']))) ? 1:0);
		return AXS_LOGINCHK;
		} #</init()>
	static function get($key=false) {
		global $axs_user;
		if ($key==='@') return (strpos($axs_user['user'],'@')) ? $axs_user['user']:$axs_user['email'];
		if ($key==='name') return trim($axs_user['fstname'].' '.$axs_user['lstname']);
		if ($key!==false) return axs_get($key, $axs_user, '');
		return $axs_user;
		} #</get()>
	static function set($key, $value) {
		global $axs, $axs_user;
		# use session if available
		if ($axs['session()']) $axs_user[$key]=$_SESSION['axs_'.$axs['http_root']]['user'][$key]=$value;
		# else use coockies
		else $axs_user[$key]=$_COOKIE['axs_user_'.$key]=$value;
		} #</set()>
	static function keepalive() {
		global $axs;
		if (!strlen($axs['cfg']['login_update'])) $axs['cfg']['login_update']=self::$login_update; # If interval not defined, set to 600 seconds
		if ($axs['cfg']['login_update']<60) $axs['cfg']['login_update']=60;
		$nr=intval($axs['get']['login']['keepalive']);
		if ($nr>0) exit('keepalive:'.round($axs['cfg']['login_update']*$nr/60, 2).'min (interval'.$axs['cfg']['login_update'].'s)');
		else {
			header('Content-type: text/javascript');
			axs_exit('axs.user.init('.$axs['cfg']['login_update'].');');
			}
		} #</keepalive()>
	static function update_online($time) { # function for defining online user
		global $axs, $axs_user;
		if ((!empty($axs_user['id'])) && ($axs['cfg']['db'][$axs['cfg']['users_db']]['type'])) axs_db_query(array(
			'UPDATE "'.self::$table.'" SET online=:online WHERE "id"=:id  AND "user"=:user',
			array(':online'=>$time, ':id'=>intval($axs_user['id']), ':user'=>$axs_user['user'], )
			), '', 1, __FILE__, __LINE__);
		else axs_user_login::tmp_update($axs_user, array('online'=>$time));
		return $time;
		} #</update_online()>
	static function permission_compact($path, $key=false) { #<Compact recursive permissions />
		$compact=array();
		foreach ($path as $k=>$v) {
			if ($key) $v=$v[$key]; 
			foreach ($v as $kk=>$vv) foreach ($vv as $kkk=>$vvv) $compact[$kk][$kkk]=$vvv;
			}
		return $compact;
		} #</permission_compact()>
	#<Check if current user belongs to listed group(s) or require a specific username. Member of the dev group is always allowed except when a specific username is required. />
	static function permission_get($groups=array(), $user=false) {
		global $axs, $axs_user;
		if ((!defined('AXS_LOGINCHK')) || (AXS_LOGINCHK!==1)) return false;
		if (!is_array($groups)) $groups=array($groups=>array(), );
		$permission=false;
		if (!empty($groups)) foreach ($groups as $k=>$v) { #<Validate groups requirements>
			if ($k==='-') { #"-" doesn't require any groups but requires at least login />
				$permission=true;	break;
				}
			if ($k==='*') { #"*" matches any group but requires at least one group
				if (!empty($axs_user['g'])) {	$permission=true;	break;	}
				}
			if (!isset($axs['cfg']['groups'][$k])) continue; #<Check if required group exists />
			if (!empty($axs_user['g'][$k])) {	$permission=true;	break;	}
			if (!empty($axs_user['g']['dev'])) {	$permission=true;	break;	} #<Bypass member of the dev group />
			}
		else $permission=true; #</Validate groups requirements>
		#<If specific user required />
		if ($user) {	$permission=($axs_user['user']===$user) ? true:false;	}
		return $permission;
		} #</permission_get()>
	#<Get permission by group and role />
	static function permission_get_role($restrictions=array(), $role=false) {
		global $axs;
		$permission=true;
		#<Find if there are any restrictions />
		$groups=array();
		foreach ($restrictions as $k=>$v) {
			if (!is_array($v)) $v=array();
			if ((!$role) or (empty($v)) or (in_array($role, (array)$v, true))) $groups[$k]=$k;
			}
		#<Check if user in required group />
		if (!empty($groups)) $permission=self::permission_get($groups);
		return $permission;
		} #</permission_get_role()>
	static function session_set($u, $time=false) {
		global $axs, $axs_user;
		$axs_user=$u;
		$axs_user['online']=$axs['time'];
		if ($axs['session()']) { # use PHP session if available
			if (!isset($_SESSION)) session_start();
			$_SESSION['axs_'.$axs['http_root']]['user']=$axs_user;
			} 
		else { # else use coockies
			if (!$time) $time=$axs['time']+900;
			$_COOKIE['axs_user_user']=$u['user'];
			$_COOKIE['axs_user_pass']=$u['pass'];
			setcookie('axs_user_id', axs_get('id', $axs_user), $time, $axs['http_root']);
			setcookie('axs_user_user', axs_get('user', $axs_user), $time, $axs['http_root']);
			setcookie('axs_user_pass', axs_get('pass', $axs_user), $time, $axs['http_root']);
			setcookie('axs_user_online', axs_get('online', $axs_user), $axs['time']+2592000, $axs['http_root']);
			}
		} #</session_set()>
	static function tmp_get(array &$user) {
		$fs=new axs_filesystem(AXS_PATH_CMS.'data/');
		$f=(!empty($user['id'])) ? intval($user['id']):strtolower(axs_valid::f_secure($user['user']));
		$f='user.'.$f.'..php';
		$u=($fs->exists($f)) ? (array)include(AXS_PATH_CMS.'data/'.$f):array();
		foreach (self::$fields_tmp as $k=>$v) {	if (!isset($user[$k])) $user[$k]=axs_get($k, $u, $v);	}
		return $f;
		} #</tmp_get()>
	static function verify($usr) {
		global $axs;
		#<Sanitize user session data>
		$usr['id']=(isset($usr['id'])) ? intval($usr['id']):0;
		$usr['user']=(isset($usr['user'])) ? strtolower(preg_replace('#"|//|\'|\..\|\\\|:#', '', substr($usr['user'], 0, self::$user_length))):'';
		$usr['pass']=(isset($usr['pass'])) ? substr($usr['pass'], 0, self::$pass_length):'';
		if (strlen(axs_get('pass_plain', $usr, ''))) $usr['pass']=password_hash($usr['pass_plain'], PASSWORD_DEFAULT);
		#</Sanitize user session data />
		if ((!strlen($usr['user'])) or (!strlen($usr['pass']))) return array();
		#<Reload user data>
		$user=array('user'=>'', 'pass'=>'', 'lock'=>false, );
		if (file_exists($f=AXS_PATH_CMS.'data/user.'.$usr['user'].'.php')) { # Get user data from files if exists
			$user=array_merge(array('id'=>false, ), (array)include($f));
			}
		else { # Else get user data from SQL
			if ($axs['cfg']['db'][$axs['cfg']['users_db']]['type']) {
				$qry=array();
				foreach ($axs['cfg']['groups'] as $k=>$v) $qry[]='"g_'.$k.'"';
				if (!empty($usr['id'])) {	$where='u."id"=:id';	$params=array(':id'=>$usr['id']);	}
				else {	$where='u."user"=:user';	$params=array(':user'=>$usr['user']);	}
				$cl=axs_db_query(array('SELECT u."id", u."user", u."pass", u."lock", u."fstname", u."lstname", u."email", u."phone", GROUP_CONCAT(CONCAT(h."id",\':\',h."site",\':\',h."dir") SEPARATOR \',\') AS "homedirs",'."\n".
				'	"online"'.((!empty($qry))?', '.implode(', ', $qry):'')."\n".
				'	FROM "'.self::$table.'" AS u LEFT JOIN "'.self::$table.'_home" AS h ON h."uid"=u."id"'."\n".
				'	WHERE '.$where.' AND !"lock" GROUP BY u."id" LIMIT 1', $params), 'row', $axs['cfg']['users_db'], __FILE__, __LINE__, array('error_handle'=>1));
				foreach (self::$fields as $k=>$v) $user[$k]=($v) ? trim(axs_get($k, $cl, '')):axs_get($k, $cl, $v);
				$user['g']=array();
				foreach ($axs['cfg']['groups'] as $k=>$v) if ($cl['g_'.$k]) $user['g'][$k]=true;
				$cl['homedirs']=preg_split('/,/', $cl['homedirs'], -1, PREG_SPLIT_NO_EMPTY);
				$user['homedirs']=array();
				foreach ($cl['homedirs'] as $k=>$v) {
					$v=explode(':', $v);
					$user['homedirs'][intval($v[0])]=array('site'=>intval($v[1]), 'dir'=>$v[2]);
					}
				}
			}
		self::tmp_get($user);
		#</Reload user data>
		#<Validate user and pass>
		if (($user['lock']) || (!strlen($user['pass']))) return array();
		if (isset($usr['pass_plain'])) {
			if (password_verify($usr['pass_plain'], $user['pass'])) return $user;
			if (($user['pass_tmp']) && ($user['pass_tmp_expire']>=$axs['time'])) {
				if (password_verify($usr['pass_plain'], $user['pass_tmp'])) return $user;
				}
			}
		else {	if (($usr['user']===strtolower($user['user'])) && ($usr['pass']===$user['pass'])) return $user;	}
		return array();
		} #</verify()>
	} #</class::axs_user>
#2005 ?>