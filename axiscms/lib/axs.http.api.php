<?php #2022-03-29
class axs_http_api {
	public $params=array(
		'url'=>[CURLOPT_URL],
		'POST'=>[CURLOPT_POSTFIELDS],
		'PUT'=>[CURLOPT_POSTFIELDS],
		);
	public $params_add=array(
		'auth'=>[CURLOPT_HTTPAUTH, CURLAUTH_ANY],
		'POST'=>[CURLOPT_POST, true],
		'PUT'=>[CURLOPT_PUT, true],
		);
	static $content_types=array('json'=>'application/json', 'soap'=>'application/soap+xml', 'xml'=>'application/xml', );
	public $p=array();
	public $r=array('Content-Type'=>'', 'Accept'=>'', 'status'=>'', 'content'=>'', );
	function __construct($p=array()) {
		$this->ch=curl_init();
		#curl_setopt($this->ch, CURLOPT_HEADER, true);
		curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
		$this->_params($p);
		} #</__construct()>
	function _params($p) {
		$params=array();
		if (isset($p['header'])) foreach ($p['header'] as $k=>$v) {	$p['header'][$k]=$k.': '.$v;	}
		if (isset($p['auth'])) {
			$p['header']['auth']='Authorization: '.$p['auth'];
			unset($p['auth']);
			}
		foreach (array('Content-Type'=>'', 'Accept'=>'', ) as $k=>$v) if (isset($p[$k])) {
			if (isset(self::$content_types[$p[$k]])) $p[$k]=$this->r[$k]=self::$content_types[$p[$k]];
			$p['header'][$k]=$k.': '.$p[$k];
			unset($p[$k]);
			}
		if (!empty($p['header'])) $p['header']=array(CURLOPT_HTTPHEADER, $p['header']);
		foreach ($p as $k=>$v) {
			$params[$k]=(isset($this->params[$k])) ? $this->params[$k]:array();
			if (!is_array($v)) $v=array(1=>$v);
			foreach ($v as $kk=>$vv) {	$params[$k][$kk]=$vv;	}
			}
		foreach ($params as $k=>$v) {
			$this->p[$k]=$v;
			curl_setopt($this->ch, $v[0], $v[1]);
			}
		foreach ($this->params_add as $k=>$v) {
			if (isset($params[$k])) {	$this->p[]=$v;	curl_setopt($this->ch, $v[0], $v[1]);	}
			}
		} #</_params()>
	function request($p=array()) {
		if ($p) $this->_params($p);
		$this->r['content']=curl_exec($this->ch);
		$this->r['status']=curl_getinfo($this->ch, CURLINFO_RESPONSE_CODE);
		$this->r['Content-Type']=curl_getinfo($this->ch, CURLINFO_CONTENT_TYPE);
		$type=($this->r['Accept']) ? $this->r['Accept']:$this->r['Content-Type'];
		$type=preg_replace('/;.+$/i', '', $type);
		if ($type) switch ($type) {
			case 'application/json':
			case 'text/json':
				$r=json_decode($this->r['content'], true);
				if ($r===false) axs_log(__FILE__, __LINE__, __CLASS__, __FUNCTION__.'():invalid response'); 
				$r=(array)$r;
				break;
			default: $r=$this->r['content'];
			}
		return $r;
		} #</request()>
	function stat($key=null) {	return ($key) ? curl_getinfo($this->ch, $key):curl_getinfo($this->ch);	}
	} #</class::axs_http_api>
#2021-08-28 ?>