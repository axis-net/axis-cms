//2021-03-29
axs.db=function(id){
	this.id=id;
	this.select=function(){
		$('#'+axs.db.id+' input.select').each(function(){
			$('#id'+this.value+' input.delete').prop('disabled',($(this).is(':checked')) ? false:true);
			});
		var selected=$('#'+axs.db.id+' input.select:checked').length;
		if (!selected) $('#'+axs.db.id+' input.delete').prop('disabled',false);
		if((axs.class_has(document.querySelector('#content'),'select'))&&(window.parent)){
			var data=JSON.parse(this.dataset.select);
			for(k in data) {	window.parent.document.querySelector('#'+k).value=data[k];	}
			window.parent.axs.overlay.close();
			}
		}//</select()>
	this.delete=function(){
		var selected=$('#'+axs.db.id+' input.select:checked').length;
		var current=$(this).closest('.item').find('input.select');
		var currentSelected=$(current).is(':checked');
		if (!selected){
			if (confirm(this.getAttribute('title'))){
				$(current).prop('checked',true);
				return true;
				}
			}
		if ((selected)&&(!currentSelected)) {
			console.log('Current not selected (selected:'+selected,'currentSelected:'+currentSelected+')');
			return false;
			}
		}//</delete()>
	$('#'+this.id+' input.select').click(this.select);
	//$('#'+this.id+' input.delete').prop('disabled',true);
	$('#'+this.id+' input.delete').click(this.delete);
	if(axs.form.forms.search_form) axs.form.forms.search_form.formClose();
	}//</class::axs.db>
//2012-01-25