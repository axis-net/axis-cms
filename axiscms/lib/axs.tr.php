<?php #2020-04-30
#<Translation functions>
class axs_tr {
	static $lang='en';
	static $charset='utf-8';
	public $tr=array();
	function __construct($d=false, $f=false, $l=false, $l_default='en', $section=false) {
		global $axs;
		if (is_array($f)) {
			$tmp=$f;
			$f=axs_get('f', $tmp, false);
			foreach ($tmp as $k=>$v) ${$k}=$v;
			}
		$this->l_default=($l_default!==false) ? $l_default:self::$lang;
		$this->l=($l!==false) ? $l:$axs['cfg']['cms_lang'];
		$this->f=($f!==false) ? $f:'index.tr';
		$this->d=AXS_PATH_CMS;
		if ($d!==false) {
			if (is_string($d)) $this->d=$d;
			if ((is_array($d)) or (is_object($d))) $this->set($d);
			}
		if (($f) or ($section)) {
			if ($tmp=$this->load($this->d.$this->f, array($this->l, $this->l_default), $section)) $this->l=$tmp;
			}
		} #</__construct()>
	function filter($prefix, $tr=false) { #<Get all elements HTML-escaped or plain-text. />
		if ($tr===false) $tr=$this->tr;
		$prefix=str_replace('.', '\.', $prefix);
		foreach ($tr as $k=>$v) foreach ((array)$prefix as $vv) {
			if (preg_match('/^'.$vv.'/', $k)) {
				$tr[preg_replace('/^'.$vv.'/', '', $k)]=$v;
				unset($tr[$k]);
				}
			}
		return $tr;
		} #</filter()>
	function get($html=true) { #<Get all elements HTML-escaped or plain-text. />
		if (!$html) return $this->tr;
		$tr=$this->tr;
		foreach ($tr as $k=>$v) if (substr($k, -1, 5)==='.html') $tr[$k]=nl2br(htmlspecialchars($v));
		return $tr;
		} #</get()>
	function has($key, $section=false) {
		return ($section===false) ? isset($this->tr[$key]):isset($this->tr[$key][$section]);
		} #</has()>
	function load($f, $l, $section=false, $check_exists=true) { #<Load translations array />
		global $axs;
		if ($f===false) $f=$this->d.$this->f;# $f='module.class.tr.??.php'
		$lang='';
		if ($f) {
			$l=array_unique(array_merge((array)$l, array($this->l, $this->l_default)));
			foreach ($l as $v) if (file_exists($file=$f.'.'.$v[0].$v[1].'.php')) {
				$lang=$v[0].$v[1];
				$tmp=(array)include($file);
				if ($axs['cfg']['charset']!==self::$charset) foreach ($tmp as $k=>$v) {
					if (!is_array($v)) $tmp[$k]=mb_convert_encoding($v, $axs['cfg']['charset'], self::$charset);
					}
				$this->tr=array_merge($this->tr, $tmp);
				break;
				}
			if ((!$lang) && ($check_exists)) axs_log(__FILE__, __LINE__, __CLASS__, 'No translation file found for "'.$file.'"!');
			}
		if ($section!==false) {
			$this->tr=(array)$this->tr[$section];
			if ($axs['cfg']['charset']!==self::$charset) foreach ($this->tr as $k=>$v) $this->tr[$k]=mb_convert_encoding($v, $axs['cfg']['charset'], self::$charset);
			}
		return $lang;
		} # </load()>
	static function load_class_tr($class, $l=false) {
		return new self(AXS_PATH_CMS, 'index.tr', $l, false, 'class::'.$class);
		} #</load_fn()>
	function s($key, $vars=false) { #<Get translation for key HTML escaped />
		if (substr($key, -5)==='.html') return $this->t($key, $vars);
		return nl2br(htmlspecialchars($this->t($key, $vars)));
		} #</s()>
	function t($key, $vars=false) { #<Get translation for key in plaintext />
		$s=(isset($this->tr[$key])) ? $this->tr[$key]:'$'.$key;
		if ($vars) foreach ($vars as $k=>$v) $s=str_replace('{$'.$k.'}', $v, $s);
		if (substr($key, -5)==='.html') return strip_tags(html_entity_decode(preg_replace('#<br />|<br/>|<br>#i', "\n", $s)));
		return $s;
		} #</t()>
	function set($key, $val=false) {
		if (is_array($key)) foreach ($key as $k=>$v) {
			(is_array($v)) ? $this->tr[$k]=array_merge($this->tr[$k], $v):$this->tr[$k]=$v;
			}
		elseif (is_object($key)) foreach ($key->tr as $k=>$v) {
			(is_array($v)) ? $this->tr[$k]=array_merge($this->tr[$k], $v):$this->tr[$k]=$v;
			}
		elseif (is_array($val)) $this->tr[$key]=array_merge($this->tr[$key], $val);
		else $this->tr[$key]=$val;
		} #</set()>
	} #</class::axs_tr>
#2014-06-06 ?>