<?php #2021-08-11
class axs_admin {
	public $l_default='en', $tmp='', $f_px='', $msg=array();
	static $user_log=array();
	static $character_sets=array('iso-8859-1'=>'', 'windows-1252'=>'', 'utf-8'=>'', 'html'=>'', );
	/*static $content_types=array(
		'css'=>'text/css', 'csv'=>'application/csv', 'doc'=>'application/msword', 'gif'=>'image/gif', 'html'=>'text/html', 'jpg'=>'image/jpeg', 'js'=>'text/javascript',  'ods'=>'application/vnd.oasis.opendocument.spreadsheet', 'odt'=>'application/vnd.oasis.opendocument.text', 'pdf'=>'application/pdf',
		'png'=>'image/png', 'txt'=>'text/plain', 'xlsx'=>'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'zip'=>'application/zip',
		);*/
	static $ftypes_blacklist=array('', 'asp', 'cfs', 'htaccess', 'php', 'php3', 'php4', 'php5', 'php6', 'php7', 'php8', 'php9', 'pht', 'py', );
	static $ftypes_img=array('gif', 'jpg', 'jpeg', 'png', 'wbmp', 'xbm', );
	static $ftypes_media=array('asf', 'avi', 'flv', 'mov', 'mp3', 'wmv', 'wma', );
	static $ftypes_audio=array('m4a', 'mp2', 'mp3', 'mpa', 'ogg', 'wav', 'wma', );
	static $ftypes_video=array('asf', 'avi', 'flv', 'mov', 'mp4', 'mpg', 'mpeg', 'ts', 'webm', 'wmv', );
	static $media_defaults=array(
		'img'=>array(
			'0'=>array('proc'=>1, 'w'=>450, 'h'=>300, 'crop_pos'=>'', 'crop_size'=>'', 'q'=>90, ),
			's'=>array('proc'=>array(0=>'','_blank'=>'_blank','popup'=>'popup','overlay'=>'overlay','_parent'=>'_parent',), 'w'=>2000, 'h'=>2000, 'crop_pos'=>'', 'crop_size'=>'', 'q'=>90, ),
			'format'=>'',
			),
		'mp_w'=>320, 'mp_h'=>240, 'mp_play'=>0,
		);
	var $tr_file='.tr';
	//var $msg=array();
	function __construct($f_path='', $editor_name='', $f_px='', $default_lang='', $site_nr=1) {
		global $axs;
		$this->f_path=$f_path;
		$this->f_px=$f_px;
		$this->url=$axs['url'];
		$this->site_set($axs['site_nr']);
		if ($default_lang==='') $default_lang=$this->l_default;
		$this->l=$axs['cfg']['cms_lang'];
		$this->dir_admin=preg_replace('/^.*\//', '', dirname($_SERVER['SCRIPT_NAME'])).'/';
		$this->initialize($editor_name, $f_path, $default_lang, $f_px);
		} #</__construct()>
	
	#<Editor builder functions>
	function initialize($editor_name, $f_path=false, $default_lang='', $f_px=false, $site_nr=false) { #<Start editor object />
		global $axs, $axs_content;
		$this->msg=$this->templates=$this->vars=array();
		$this->name=$editor_name;
		if ($f_path!==false) $this->f_path=$f_path;
		if ($f_px!==false) $this->f_px=$f_px;
		if ($default_lang==='') $default_lang=$this->l;
		if ($site_nr!==false) axs_admin::site_set($site_nr);
		if ($default_lang) axs_admin::tr_load($default_lang);
		if (!empty($_POST)) register_shutdown_function(array(__CLASS__, 'user_log'), 'post');
		} #</initialize()>
	function load_editor($editor) {
		global $axs, $axs_user, $axs_content;
		if (!$editor) return '';
		if (is_object($editor)) return $editor->ui($this);
		return include($editor);
		} #</load_editor()>
	static function content_lang_set($site_nr, $l) {
		global $axs;
		$axs['l']=(isset($axs['cfg']['site'][$site_nr]['langs'][$l])) ? $l:key($axs['cfg']['site'][$site_nr]['langs']);
		}
	function site_set($site_nr) {
		global $axs;
		if (!$site_nr=intval($site_nr)) return;
		$axs['site_nr']=$this->site_nr=$site_nr;
		$this->db=$axs['cfg']['site'][$this->site_nr]['db'];
		$this->px=$axs['cfg']['site'][$this->site_nr]['prefix'];	
		} # </site_set()>
	function variables_import(&$obj_to) {
		foreach (array('name','f_path','f_px','url','site_nr','db','px','l',) as $k=>$v) $obj_to->$v=$this->$v;
		} #</variables_import()>
	#</Editor builder functions>
	
	#<Documentation related functions>
	/*function doc_init($anchor=false, $url_key='doc') {
		$this->doc=new axs_doc(array($this->name=>array('dir'=>$this->f_path, 'px'=>$this->f_px)), $this->l);
		if (empty($this->doc->data)) return '';
		return $this->doc_link($anchor, $url_key);
		} # </doc_init()>
	function doc_link($anchor='', $url_key=false) {
		if ($url_key!==false) $this->doc_url_key=$url_key;
		$this->doc_url=array_merge($this->url, array($this->doc_url_key=>$this->name, 'popup'=>''));
		$anchor=($anchor) ? '#'.$anchor:'';
		$lbl=(isset($this->tr['doc_lbl'])) ? $this->tr('doc_lbl'):'( ? )';
		return '<a class="doc" href="'.'?'.axs_url($this->doc_url).$anchor.'" target="popup">'.$lbl.'</a>';
		} # </doc_link()>
	function doc_get($output=false) {
		global $axs;
		$this->vars['doc']=$this->doc_init();
		if (isset($_GET[$this->doc_url_key])) {	if ($_GET[$this->doc_url_key]==$this->name) $output=true;	}
		if ($output) {
			$axs['page']['content_class'].=' doc';
			$axs['page']['head']['index.content.css']='<link href="'.AXS_PATH_CMS_HTTP.'index.content.css'.'" rel="stylesheet" type="text/css" media="screen" />'."\n";
			$axs['page']['head'][]='<script>axs.window_resize({"ow":1200});</script>'."\n";
			$output=$this->doc->contents_html().$this->doc->content_html();
			return ($output) ? $output:true;
			}
		}*/ # </doc_output()>
	#</Documentation related functions>
	
	#<Message reporting functions>
	function log($file, $line, $class, $function, $msg='') { #<Report errors />
		return axs_log($file, $line, 'class', $class.'::'.$function.'(): '.$msg.' (line '.$line.')');
		#return axs_admin::msg($this, false, $msg);
		} # </log()>
	static function msg(&$obj, $key, $msg='', $vars=false) { #<Register message for user />
		global $axs;
		if (is_array($key)) {
			foreach ($key as $k=>$v) {	(isset($obj->msg[$k])) ? $obj->msg[]=$v:$obj->msg[$k]=$v;	}
			return;
			}
		if ($key) {
			#<Load translations />
			if (!isset($obj->tr)) $obj->tr=axs_tr::load_class_tr(get_class($obj), $axs['cfg']['cms_lang']);
			$msg=($obj->tr->has($key)) ? $obj->tr->t($key):$msg;
			if (!$msg) $msg='$'.$key;
			}
		if (is_array($vars)) foreach ($vars as $k=>$v) $msg=str_replace('{$'.$k.'}', $v, $msg);
		if ($key) {
			if (isset($obj->msg[$key])) {
				((is_string($obj->msg[$key])) && (is_string($msg))) ? $obj->msg[$key].='<br />'.$msg:$obj->msg[]=$msg;
				}
			else $obj->msg[$key]=$msg;
			}
		else $obj->msg[]=$msg;
		return $axs['msg'][]=$msg;
		} # </msg()>
	#</Message reporting functions>
	
	#<File related functions>
	static function f_choose($path, $names=array(), $f_end='') {
		foreach ($names as $k=>$v) if (file_exists($path.$v.$f_end)) return $v.$f_end;
		} # </f_choose()>
	static function f_ext($f, $get='el') { # function to get file extension
		$f=pathinfo($f, axs_get($get[0], array('d'=>PATHINFO_DIRNAME, 'f'=>PATHINFO_BASENAME, 'e'=>PATHINFO_EXTENSION, 'n'=>PATHINFO_FILENAME, )));
		return ($get==='el') ? strtolower($f):$f;
		} # </f_ext()>
	static function f_list($path, $pattern=false, $get='file') { # list files in directory
		$files=array();
		$handle=false;
		if (!is_dir($path)) axs_log(__FILE__, __LINE__, 'f_list', 'Path not found "'.$path.'"');
		else $handle=opendir($path); //rtrim($path, '\\/'));
		if (!preg_match('/\/$|\\$/', $path)) $path.='/';
		if ($handle) while (($file=readdir($handle))!==false) { # <Read directory cycle>
			if ($file=='.' or $file=='..') continue;
			if ($get) {
				if ($get[0]==='f') {	if (!is_file($path.$file)) continue;	}
				if ($get[0]==='d') {	if (!is_dir($path.$file)) continue;	}
				}
			if (!$pattern) $files[$file]=$file;
			else {	if (preg_match($pattern, $file)) $files[$file]=$file;	}
			} # </Read directory cycle>
		if ($handle) closedir($handle);
		return $files;
		} # </f_list()>
	static function f_secure($f) { # function to secure file or directory name
		//return preg_replace('#"|//|\'|\..\|\\\|:#', '', $f);
		return str_replace(array('"','//','\'','/../','\..\\','\\\\',':'), '', $f);
		} # </f_secure()>
	static function f_send($f_read=false, $f_save='', $content=false, $headers=array()) { # function to secure file or directory name
		global $axs;
		$header=array(
			'Cache-Control'=>'must-revalidate, post-check=0, pre-check=0',
			'Content-Description'=>'File Transfer',
			'Content-Transfer-Encoding'=>'binary',
			'Content-Length'=>($f_read) ? filesize($f_read):strlen($content),
			);
		$f=($f_save) ? $f_save:$f_read;
		if (!isset($header['Content-type'])) {
			if ($f_read) {	if (function_exists('mime_content_type')) $header['Content-type']=mime_content_type($f);	}
			if (!isset($header['Content-type'])) {
				if ($tmp=axs_file_array_get(axs_dir('lib').'axs.dict.mimetypes.php', self::f_ext($f))) $header['Content-type']=$tmp;
				}
			}
		if ($f_save) $header['Content-Disposition']='attachment; filename="'.$f_save.'"';
		foreach ($headers as $k=>$v) {	if ($v) $header[$k]=$v;	else unset($header[$k]);	}
		foreach ($header as $k=>$v) header($k.': '.$v);
		if (isset($axs['ob'])) $axs['ob']=axs_ob_catch($axs['ob']);
		if ($f_read) readfile($f_read);
		else echo $content;
		} # </f_send()>
	#</File related functions>
	
	#<Media functions>
	static function media_html($f, $alt=false, $w='', $h='', $f2=false) {
		# <detect media type />
		$html='';
		$f_ext=self::f_ext($f);
		$media='hlink';
		if (in_array($f_ext, self::$ftypes_img)) $media='img';
		if (in_array($f_ext, self::$ftypes_audio)) $media='audio';
		if (in_array($f_ext, self::$ftypes_video)) $media='video';
		if ($f_ext==='swf') $media='flash';
		if ($f_ext==='js') $media='js';
		if ($alt===false) {	$alt=($media==='flash') ? '<a href="'.htmlspecialchars($f).'">'.basename($f).'</a>':basename($f);	}
		
		$width=($w) ? ' width="'.intval($w).'"':'';
		$height=($h) ? ' height="'.intval($h).'"':'';
		
		if ($media==='img') {
			if (!$f2) $html='<!--file--><img src="'.$f.'"'.$width.$height.' alt="'.axs_html_safe($alt).'" /><!--/file-->';
			else $html='<!--file--><a href="?'.urlencode('axs[gw]').'='.urlencode($f2).'&amp;txt='.urlencode($alt).'" target="popup"><img src="'.htmlspecialchars($f).'"'.$width.$height.' alt="'.axs_html_safe($alt).'" /></a><!--/file-->';
			} # </$media==='img'>
		elseif ($media==='audio') $html='<audio src="'.htmlspecialchars($f).'" controls="controls"><a href="'.htmlspecialchars($f).'"><!--file-->'.$alt.'<--/file--></a></audio>';
		elseif ($media==='video') $html='<video src="'.htmlspecialchars($f).'"'.$width.$height.' controls="controls"><a href="'.htmlspecialchars($f).'"><!--file-->'.$alt.'<--/file--></a></video>';
		elseif ($media==='flash') {
			$html='<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="'.$w.'" height="'.$h.'">'.
			' <param name="movie" value="'.htmlspecialchars($f).'" />'.
			' <!--[if !IE]>-->'.
			' <object type="application/x-shockwave-flash" data="'.htmlspecialchars($f).'" width="'.$w.'" height="'.$h.'">'.
			' <!--<![endif]-->'.
			'  <!--file-->'.$alt.'<!--/file-->'.
			' <!--[if !IE]>-->'.
			' </object>'.
			' <!--<![endif]-->'.
			'</object>';
			} # </$media==='flash'>
		elseif ($media==='hlink') $html='<a href="'.htmlspecialchars($f).'"><!--file-->'.$alt.'<!--/file--></a>';
		elseif ($media==='js') $html='<script src="'.htmlspecialchars($f).'"></script><noscript>(<span lang="en">JavaScript unavailable.</span>) <a href="'.htmlspecialchars($f).'"><!--file-->'.$alt.'<!--/file--></a></noscript>';
		return $html;
		} #</media_html()>
	static function media_settings($site_nr, array $vl=array(), $c=false) {
		global $axs, $axs_content;
		if (!empty($vl)) {
			foreach (self::$media_defaults['img'] as $k=>$v) if ((is_array($v)) && (!isset($vl['img'][$k]['proc']))) $vl['img'][$k]['proc']='0';
			if (!isset($vl['mp_play'])) $vl['mp_play']=0;
			}
		$pref=array('site'=>axs_content::site_cfg_get('media'), );
		if ($c) $pref['page']=axs_tab_edit::pref_get($axs_content['content'][$c]['text']);
		$pref['input']=$vl;
		$out=self::$media_defaults;
		foreach ($out['img'] as $k=>$v) if ((isset($v['proc'])) && (is_array($v['proc']))) $out['img'][$k]['proc']=key($v['proc']);
		foreach ($pref as $k=>$v) {
			if (!is_array($v)) $v=array();
			if ((!isset($v['img'])) or (!is_array($v['img']))) $v['img']=array();
			foreach (self::$media_defaults['img'] as $kk=>$vv) {
				if ((!isset($v['img'][$kk])) or (!is_array($v['img'][$kk]))) $v['img'][$kk]=array();
				foreach (array('proc'=>'', 'w'=>0, 'h'=>0, 'q'=>0, ) as $kkk=>$vvv) {
					if (strlen(axs_get($kkk, $v['img'][$kk]))) $out['img'][$kk][$kkk]=(is_int($vvv)) ? intval($v['img'][$kk][$kkk]):$v['img'][$kk][$kkk];
					foreach (array('crop_pos'=>'', 'crop_size'=>'', ) as $kkk=>$vvv) if (isset($v['img'][$kk][$kkk])) $out['img'][$kk][$kkk]=$v['img'][$kk][$kkk];
					}
				}
			foreach (self::$media_defaults as $kk=>$vv) if (!is_array($vv)) {	if (strlen(axs_get($kk, $v))) $out[$kk]=intval($v[$kk]);	}
			}
		$out['img']['format']=axs_get('format', axs_get('img', $vl, array()), '');
		return $out;
		} #</media_settings()>
	static function media_size($f, $w=200, $h=200) { # get proportional dimensions
		$width_orig=$height_orig='';
		if (axs_function_ok('getimagesize')) {
			if (file_exists($f)) {	if (filesize($f)) list($width_orig, $height_orig)=getimagesize($f);	}
			if (($width_orig>$w) or ($height_orig>$h)) { # if image not small enough already
				$ratio_orig=$width_orig/$height_orig;
				if ($w/$h > $ratio_orig) $w=$h*$ratio_orig;
				else $h=$w/$ratio_orig;
				}
			else {
				$w=$width_orig;
				$h=$height_orig;
				}
			}
		return array('w'=>ceil($w), 'h'=>ceil($h), 'w_orig'=>$width_orig, 'h_orig'=>$height_orig, 'f'=>$f, );
		} #</media_size()>
	#</Media functions>
	
	#<UI building functions>
	static function menu_build($menu, $spc=' ', $attribs=array(), $id='', $act='') {
		if (is_string($menu)) return $menu;
		if (empty($menu)) return '';
		$html=$spc.'<ul';
		if (($id) or (!empty($attribs['id']))) $attribs['id']=$id.axs_get('id', $attribs);
		//if (!empty($attribs['class'])) $attribs['class']='menu '.$attribs['class'];
		if ($act && $act==$id) $attribs['class'].=($attribs['class']) ? ' current':'current';
		if (is_array($attribs)) foreach ($attribs as $k=>$v) $html.=' '.$k.'="'.htmlspecialchars($v).'"';
		$html.='>'."\n";
		$nr=$a=1;
		foreach ($menu as $k=>$v) {
			$tmp=(is_numeric($k)) ? '_'.$k:$k;
			$v['attribs']['class']=(!empty($v['attribs']['class'])) ? $tmp.' '.$v['attribs']['class']:$tmp;
			if (isset($v['act']) && $v['act']===$k) {
				$v['attribs']['class'].=' current';
				$actst='<em>';
				$actet='</em>';
				}
			else $actst=$actet='';
			$v['target']=(!empty($v['target'])) ? ' target="'.$v['target'].'"':'';
			if (isset($v['accesskey'])) {
				if (!$v['accesskey']) $v['accesskey']=$a;
				$v['accesskey']=' accesskey="'.$v['accesskey'].'"';
				}
			else $v['accesskey']='';
			if (!empty($v['submenu'])) {
				$v['attribs']['class'].=' submenu';
				$submenu="\n".self::menu_build($v['submenu'], $spc.'  ', '').$spc.' ';
				}
			else $submenu='';
			$attribs='';
			foreach ((array)$v['attribs'] as $kk=>$vv) $attribs.=' '.$kk.'="'.htmlspecialchars($vv).'"';
			$ast=$aet='';
			if (isset($v['url'])) {
				if ($v['url']===false) {	$ast='<span class="a">';	$aet='</span>';	}
				else {	$ast='<a class="a" href="'.$v['url'].'"'.$v['target'].$v['accesskey'].'>';	$aet='</a>';	}
				}
			$html.=$spc.' <li'.$attribs.'>'.$ast.$actst.$v['label'].$actet.$aet.$submenu.'</li>'."\n";
			$nr++;
			($a<9) ? $a++:$a=0;
			}
		$html.=$spc.'</ul>'."\n";
		return $html;
		} #</menu_build()>
	#<!Deprecated />
	static function no_permission($restrictions=array(), $role='', $file=false, $line=false) {
		global $axs;
		$permission=axs_user::permission_get_role($restrictions, $role);
		if (!$permission) {
			if (axs_user::permission_get('dev')) return;
			if (AXS_LOGINCHK!=1) {	if (($file) or ($line)) axs_log($file, $line, 'loginfalse');	}
			else $axs['msg']['axs_login']='denied';
			return axs_user_login::form();
			}
		} #</no_permission()>
	#</UI building functions>
	
	#<Templates functions>
	function tpl_set($name, $tpl=false) { # function to set default template or get it from a skin if it exists
		global $axs;
		if (!is_array($name)) {
			$tpl=array($name=>$tpl);
			$name=array($name=>$name);
			}
		foreach ($name as $k=>$v) {
			if (file_exists($f=$this->f_path.'skins/'.$axs['cfg']['cms_skin'].$this->f_px.$v.'.tpl')) $tpl=implode('', file($f));
			if (empty($tpl[$k])) {	if (file_exists($f=$this->f_path.$this->f_px.$v.'.tpl')) $tpl[$k]=implode('', file($f));	}
			if (isset($_GET['templates_dump'])) $axs['templates_dump'][$k]=$tpl[$k];
			$this->templates[$k]=$tpl[$k];
			}
		return $tpl;
		} #</tpl_set()>
	#</Templates functions>
	
	#<Translation functions>
	function tr($key, $vars=false) { #<Wrapper function for axs_tr />
		return $this->tr->s($key, $vars);
		}
	function tr_load($default_lang=false, $section=false) { #<Load translations array />
		if (!isset($this->tr)) $this->tr=axs_tr::load_class_tr('axs_db_edit', $this->l);//new axs_tr();
		if ($default_lang) $this->tr->load($this->f_path.$this->f_px.$this->name.$this->tr_file, array($this->l, $default_lang, ), $section);
		} # </tr_load()>
	#</Translation functions>
	
	#<System config and user related functions>
	function user_home_get($home_nr, $l) { # Homedirs and content menu for current user
		global $axs, $axs_user;
		$axs_user['home']=array();
		$axs['content_menu']=$axs['site_select']=array();
		if (AXS_LOGINCHK!==1) return array();
		$home_nr=($home_nr) ? intval($home_nr):@key($axs_user['homedirs']);
		if (isset($axs_user['homedirs'][$home_nr])) {
			foreach($axs_user['homedirs'] as $k=>$v) {
				if (!isset($axs['cfg']['site'][$axs_user['homedirs'][$k]['site']])) continue;
				$tmp=$axs['cfg']['site'][$axs_user['homedirs'][$k]['site']];
				if ($tmp['domain']) {
					if (!preg_match('/^http|^\/\//', $tmp['domain'])) $tmp['domain']='http://'.$tmp['domain'];
					if (!preg_match('/\/$/', $tmp['domain'])) $tmp['domain'].='/';
					$v['url']=$tmp['domain'].$this->dir_admin;
					}
				else $v['url']='';
				$tmp['dir']=($v['dir']) ? ' /'.$v['dir']:'';
				$axs['content_menu'][$k]=array(
					'url'=>$v['url'].'?'.axs_url($axs['url'], array('e'=>'content', 'h'=>$k, 'c'=>false, )),
					'label'=>$tmp['title'].$tmp['dir'],
					'act'=>(axs_get('e', $_GET)==='content') ? $home_nr:'',
					);
				$axs['site_select'][$k]=$tmp['title'].$tmp['dir'];
				}
			$axs_user['home']=$axs_user['homedirs'][$home_nr];
			$axs_user['home_nr']=$home_nr;
			$axs_user['site_nr']=intval($axs_user['homedirs'][$home_nr]['site']);
			}
		$axs['site_nr']=(!empty($axs_user['site_nr'])) ? $axs_user['site_nr']:1;
		$this->site_set($axs['site_nr']);
		$axs['site_dir']=$axs['cfg']['site'][$axs['site_nr']]['dir'];
		$axs_user['home_dir']=(!empty($axs_user['home']['dir'])) ? axs_dir('content').$axs_user['home']['dir']:false;
		
		$this->content_lang_set($axs['site_nr'], $l);
		if (!$axs['cfg']['ftp']) $axs['cfg']['ftp']='ftp://'.$axs_user['user'].':'.$axs_user['pass'].'@'.$_SERVER['SERVER_NAME'].'/';
		$axs['cfg']['ftp'].=$axs['site_dir'].$axs['dir_c'].$axs_user['home_dir'];
		return $axs_user['home'];
		} # </user_home_get()>
	static function user_log($type) {
		global $axs;
		if ((self::$user_log===false) or (!$axs['cfg']['db'][1]['type']) or (!$axs['cfg']['db'][1]['db'])) return;
		if (($type==='post') && (isset(self::$user_log['post']))) return;
		$post=$_POST;
		foreach (array('pass'=>'', 'pass_changed'=>'', ) as $k=>$v) if ((isset($post['axs'][$k])) && (strlen($post['axs'][$k]))) $post['axs'][$k]='*';
		$error=axs_db_query("INSERT INTO `".axs_user::$table."_log` SET `time`='".$axs['time']."', `uid`='".axs_user::get('id')."', `user`='".axs_user::get('user')."', `type`='".addslashes($type)."', `request`='".addslashes($axs['http'].'://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'])."', `post`='".addslashes(self::array_dump($post))."', `files`='".addslashes(self::array_dump($_FILES))."', `ip`='".addslashes($_SERVER['REMOTE_ADDR'])."'", 'error', 1, __FILE__, __LINE__, array('error_handle'=>1));
		if ($error) return;
		$limit=(!empty($axs['cfg']['loguser'])) ? $axs['cfg']['loguser']:1000;
		$grace=intval($limit/5);
		$where=(axs_user::get('id')) ? "`uid`='".axs_user::get('id')."'":"`user`='".axs_user::get('user')."'";
		$count=axs_db_query("SELECT COUNT(*) FROM `".axs_user::$table."_log` WHERE  ".$where, 'cell', 1, __FILE__, __LINE__);
		if ($count>$limit+$grace) axs_db_query("DELETE FROM `".axs_user::$table."_log` WHERE ".$where." ORDER BY `time` ASC LIMIT ".($count-$limit), '', 1, __FILE__, __LINE__);
		self::$user_log[$type]=$type;
		} #</user_log()>
	#</System config and user related functions>
	
	#<Misc>
	static function array_dump($array, $space='') { # <recursive function to output array as a string />
		$space.='	';
		$txt='';
		#if (is_string($array)) $array=self::array_decode($array);
		foreach((array)$array as $k=>$v) {
			$txt.=$space.'['.$k.']=';
			if (is_string($v)) $txt.='"'.$v.'"'."\n";
			elseif (is_bool($v)) {	$txt.=($v) ? 'TRUE'."\n":'FALSE'."\n";	}
			elseif (is_null($v)) $txt.='NULL'."\n";
			elseif (is_array($v)) $txt.='array('."\n".self::array_dump($v, $space).$space.')'."\n";
			else $txt.=$v."\n";
			}
		return $txt;
		} #</array_dump()>
	#</Misc>
	
	function finish($tpl_name=false) {
		global $axs;
		$axs['msg']+=$this->msg;
		if ($tpl_name===false) $tpl_name=$this->name;
		foreach ($this->vars as $k=>$v) if ((!is_null($v)) && (!is_scalar($v))) unset($this->vars[$k]);
		$out=axs_tpl_parse($this->templates[$tpl_name], $this->vars+$this->tr->get());
		$this->f_path=AXS_PATH_CMS;
		$this->f_px='';
		return $out;
		} #</finish()>
	} #</class::axs_admin>
#2008-12-29 ?>