<?php
//ini_set('display_errors', 0);
//set_error_handler('axs_log_php_errors');
define('AXS_CMS_VER', '2.0.0-dev49 (build 2022-05-10)');
define('AXS_DOMAIN', 'veebid.eu');
define('AXS_LOCALHOST', (($_SERVER['SERVER_NAME']===AXS_DOMAIN) or (($_SERVER['SERVER_NAME']==='localhost') && (strncmp(dirname($_SERVER['SCRIPT_NAME']), '/axiscms/', 9)===0))) ? true:false);
if (!defined('AXS_PATH_CMS')) define('AXS_PATH_CMS', dirname(__DIR__).'/'); # <Path to CMS />
#if (!defined('AXS_PATH_SYS')) define('AXS_PATH_SYS', rtrim(dirname(AXS_PATH_CMS), '/\\').'/'); # <Path to SYSTEM root />
#if (!defined('AXS_SITE_ROOT')) define('AXS_SITE_ROOT', './'); # <Path to SITE root />

#<Initialize global variables>
$axs=$axs_user=array();
$axs['cfg']=require(AXS_PATH_CMS.'data/index.php'); #<Get configuration />
$axs['cfg']['db'][1]['px']=$axs['cfg']['site'][1]['prefix'];
$axs['cfg']['users_db']=1;
#</Initialize global variables>
#<Available features>
$axs['file_uploads']=ini_get('file_uploads');
$axs['ini_set()']=axs_function_ok('ini_set');
$axs['mail()']=(axs_function_ok('mail') && !ini_get('safeex.disable_mail')) ? true:false; #<check if mail function is available />
$axs['session()']=(extension_loaded('session') && axs_function_ok('session_start') && !strpos(' '.ini_get('safeex.disable_extensions'), 'session')) ? true:false;
#</Available features>
#<Set environment>
if (!empty($axs['cfg']['timezone'])) date_default_timezone_set($axs['cfg']['timezone']);
if (!empty($axs['cfg']['charset'])) mb_internal_encoding($axs['cfg']['charset']);
if ($axs['ini_set()']) ini_set('arg_separator.output', '&amp;');
#</Set environment>
$axs['time']=time();
$axs['http']=axs_get('HTTPS', $_SERVER);
$axs['http']='http'.((($axs['http']) && ($axs['http']!=='off')) ? 's':'');
$axs['http_root']=rtrim(dirname($_SERVER['SCRIPT_NAME']), '/\\').'/';
if ((!defined('AXS_SITE_NR')) or ((defined('AXS_SITE_NR')) && ($axs['cfg']['site'][AXS_SITE_NR]['dir']))) $axs['http_root']=rtrim(dirname($axs['http_root']), '/\\').'/';
//dbg(AXS_PATH_SYS,$_SERVER['SCRIPT_NAME'],$axs['http_root']);
$axs['dir_cfg']='data/';
$axs['dir_cms']='axiscms/';
$axs['dir_lib']='lib/';
$axs['dir_lib_js']='lib.js/';
$axs['dir_modules']='modules/';
$axs['dir_plugins']='plugins/';
$axs['dir_site_base']='site/';
$axs['dir_site']='axs_site/';
$axs['dir_c']=$axs['dir_site'].'content/';
$axs['f_px']='axs_';
$axs['debug']=$axs['dbg']=$axs['msg']=$axs['db']=$axs['user']=$axs['plugins']=array();
if (file_exists(AXS_PATH_CMS.'data/index.customize.php')) include(AXS_PATH_CMS.'data/index.customize.php');

if ((defined('AXS_SITE_NR')) && (!$axs['cfg']['cms_http'])) $axs['cfg']['cms_http']=$axs['cfg']['site'][1]['domain'].$axs['cfg']['site'][1]['dir'].$axs['dir_cms'];
foreach ($axs['cfg']['site'] as $k=>$v) {
	if (!$v['dir_fs_root']) $v['dir_fs_root']=$v['dir'];
	if (!preg_match('#(^/)|(^[a-zA-Z]\:)|(^\\\)#', $v['dir_fs_root'])) {
		$v['dir_fs_root']=realpath(AXS_PATH_CMS.'../'.$v['dir_fs_root']).'/';
		if ($v['dir']) $v['dir_fs_root']=dirname($v['dir_fs_root']).'/';
		}
	$axs['cfg']['site'][$k]['dir_fs_root']=$v['dir_fs_root'];
	}
$axs['site_nr']=(defined('AXS_SITE_NR')) ? AXS_SITE_NR:1;
$axs['site_dir']=$axs['cfg']['site'][$axs['site_nr']]['dir'];
$axs['get']=axs_get('axs', $_GET, array());
if (!defined('AXS_PATH_CMS_HTTP')) define('AXS_PATH_CMS_HTTP', $axs['cfg']['cms_http']); # <HTTP path to CMS />

function axs_class_load($class) {
	global $axs;
	if (strncmp($class, 'axs_', 4)===0) {
		$f=strtolower(str_replace('_', '.', substr($class, 4)));
		if (file_exists($tmp=AXS_PATH_CMS.'/'.$axs['dir_lib'].'axs.'.$f.'.php')) return include_once($tmp);
		#if (file_exists($tmp=AXS_PATH_CMS.'/'.'axs.'.$f.'.php')) return include_once($tmp);
		if (file_exists($tmp=AXS_PATH_CMS.'/'.$axs['dir_plugins'].$f.'.class.php')) return include_once($tmp);
		if (file_exists($tmp=AXS_PATH_CMS.'/'.$axs['dir_modules'].$f.'.class.php')) return include_once($tmp);
		}
	if (file_exists($tmp=AXS_PATH_CMS.'/'.$axs['dir_lib'].strtolower($class).'.php')) return include_once($tmp);
	if (file_exists($tmp=AXS_PATH_CMS.'/'.$axs['dir_plugins'].strtolower($class).'.php')) return include_once($tmp);
	//echo(AXS_PATH_CMS.$axs['dir_lib'].'axs.'.$f.'.php');
	} #</axs_class_load()>
spl_autoload_register('axs_class_load');
if (axs_function_ok('__autoload')) spl_autoload_register('__autoload'); #If autoload already exists, it has to be added to the stack.

#<Function for connecting databases and performing querys. Can set up connections to multiple databases and supports different SQL servers. 
# $query (str): SQL query string. With empty query this function can be used to set up the connection to the database server. 
# $fetch (str)
# 	'0': return result without fetching it or DB object if there was no query. 
# 	'1':
#	'table': fetch result as 2D array (table) with keys as column names. 
# 	'k': fetch result as 2D array (table) with keys as column names and use the first column as the row key. 
# 	'l':
# 	'list': fetch list from the first column. 
#	'kl':
#	'klist': fetch list from the first column and use the value as the array key. 
# 	'row': return only one row. 
# 	'cell': return only the first cell from the first row. 
# 	'found_rows': return the number of rows calculated by the SQL_CALC_FOUND_ROWS keyword in the SQL query. 
# 	'insert_id': return the ID nr generated by the last INSERT operation on the current database connection. 
# 	'affected_rows': return the number of affected rows by the last UPDATE operation on the current database connection. 
# 	'error': return the last error message on the current database connection. 
# $dbnr (int): which database profile to use 
# $f (str), $ln (int): Filename and line nr where function was called from. Useful for logging. 
# $options (array): set additional options 
# 	'error_handle' (int): 
# 		0: set up error message but don't log or interrupt 
# 		1: log error silently and continue script execution 
# 		2: log error and stop script execution 
# 	All error messages are added to the variable $axs['db'][$dbnr]->axs_error
# 	'log_query' (bool): log SQL query to CMS log 
# />
function axs_db_query($query=false, $fetch=0, $dbnr=1, $f='', $ln='', array $options=array()) {
	global $axs;
	if (($query) && (!is_array($query))) $query=array($query);
	if (!is_array($fetch)) $fetch=array($fetch=>array());
	foreach (array('error_handle'=>2, 'loq_query'=>false, ) as $k=>$v) if (!isset($options[$k])) $options[$k]=$v;
	$error=$result=$id_col=null;
	$log_exit=$log_output='';
	if ($options['error_handle']===2) {	$log_exit=true;	$log_output='---';	}
	if (empty($axs['db'][$dbnr])) {
		$dsn_cmd=array();
		switch($axs['cfg']['db'][$dbnr]['type']) {
			case 'sqlsrv': $dsn='sqlsrv:Server='.$axs['cfg']['db'][$dbnr]['host'].';Database='.$axs['cfg']['db'][$dbnr]['db'];	break;//';charset='.$axs['cfg']['db'][$dbnr]['collation']; #MSSQL with PDO_SQLSRV
			case 'mssql': $dsn='mssql:host='.$axs['cfg']['db'][$dbnr]['host'].';dbname='.$axs['cfg']['db'][$dbnr]['db'].';charset='.$axs['cfg']['db'][$dbnr]['collation'];	break; #MSSQL with PDO_DBLIB
			case 'sybase': $dsn='sybase:host='.$axs['cfg']['db'][$dbnr]['host'].';dbname='.$axs['cfg']['db'][$dbnr]['db'].';charset='.$axs['cfg']['db'][$dbnr]['collation'];	break; #Sybase with PDO_DBLIB
			case 'pgsql': $dsn='pgsql:host='.$axs['cfg']['db'][$dbnr]['host'].';dbname='.$axs['cfg']['db'][$dbnr]['db'];	break; #PostgreSQL with PDO_PGSQL
			default:
				$dsn='mysql:host='.$axs['cfg']['db'][$dbnr]['host'].';dbname='.$axs['cfg']['db'][$dbnr]['db'].';charset='.$axs['cfg']['db'][$dbnr]['collation'];
				$dsn_cmd=array(PDO::MYSQL_ATTR_INIT_COMMAND=>"SET NAMES '".$axs['cfg']['db'][$dbnr]['collation']."', SESSION sql_mode='ANSI_QUOTES'", ); 
				break; #MySQL with PDO_MYSQL
			}
		try {	$axs['db'][$dbnr]=new PDO($dsn, $axs['cfg']['db'][$dbnr]['user'], $axs['cfg']['db'][$dbnr]['pass'], $dsn_cmd);	}
		catch(PDOException $e) {
			$error='Failed to connect to database: '.$e->getMessage().' (DSN:"'.$dsn.') (on profile '.$dbnr.')';
			$axs['db'][$dbnr]=new StdClass;
			}
		$axs['db'][$dbnr]->axs_error=array();
		if ($error) {
			$axs['db'][$dbnr]->axs_error[]=$error;
			if ($options['error_handle']) axs_log($f, $ln, 'database', $error, $log_exit, '---');
			return false;
			}
		}
	if (!method_exists($axs['db'][$dbnr], 'query')) { #<Prevent query and fetch if connection failed>
		$query=false;
		if (key($fetch)!=='error') return;
		}
	if ($query) {
		if ($options['loq_query']) axs_log($f, $ln, 'database.query', $query);
		if (!empty($query[1])) {
			$result=$axs['db'][$dbnr]->prepare($query[0]);
			$result->execute($query[1]) or $error=$result->errorInfo();
			}
		else $result=$axs['db'][$dbnr]->query($query[0]) or $error=$axs['db'][$dbnr]->errorInfo();
		if ($error) {
			$error=$error[2].' ('.$error[0].','.$error[1].') on profile '.$dbnr."\n".$query[0];
			if (isset($query[1])) $error.="\nParams:\n".axs_log::array_dump($query[1]);
			$axs['db'][$dbnr]->axs_error[]=$error;
			if ($options['error_handle']) axs_log($f, $ln, 'database', $error, $log_exit, $log_output);
			}
		}
	switch (key($fetch).'') {
		case '1':
		case 'table': #<fetch table with rows as associative arrays />
			$table=$result->fetchAll(PDO::FETCH_ASSOC);
			$result=null;
			return $table;
		case 'k': $table=array(); #<fetch table with rows as associative arrays and use the first column as the row key />
			if ($result) while ($cl=$result->fetch(PDO::FETCH_ASSOC)) $table[current($cl)]=$cl;
			$result=null;
			return $table;
		case 'l':
		case 'list': $table=array(); #<fetch list from the first column />
			while ($cl=$result->fetchColumn()) $table[]=$cl;
			$result=null;
			return $table;
		case 'kl':
		case 'klist': $table=array(); #<fetch list from the first column and use the value as the array key />
			while ($cl=$result->fetchColumn()) $table[$cl]=$cl;
			$result=null;
			return $table;
		case 'row': #<return only one row />
			$row=(array)$result->fetch(PDO::FETCH_ASSOC);
			$result=null;
			return $row;
		case 'cell': #<return only the first cell from the first row />
			$cell=$result->fetchColumn();
			$result=null;
			return $cell;
		case 'found_rows':
			$result=$axs['db'][$dbnr]->query("SELECT FOUND_ROWS()");
			$cell=($result) ? $result->fetchColumn():0;
			$result=null;
			return $cell;
		case 'insert_id':
			return $axs['db'][$dbnr]->lastInsertId($id_col);
		case 'affected_rows': return ($result) ? $result->rowCount():0;
		case 'error': return end($axs['db'][$dbnr]->axs_error);
		case '0':
		default: return ($query) ? $result:$axs['db'][$dbnr]; #<return result without fetching it or DB object if there was no query />
		}
	} #</axs_db_query()>
function axs_dir($key, $r='f', $site=false) {
	global $axs;
	switch ($key) {
		case 'cfg': return ($r[0]==='h') ? AXS_PATH_CMS_HTTP.$axs['dir_cfg']:AXS_PATH_CMS.$axs['dir_cfg'];
		case 'cms': return ($r[0]==='h') ? AXS_PATH_CMS_HTTP:AXS_PATH_CMS;
		case 'content':
			$s=$axs['cfg']['site'][($site) ? $site:$axs['site_nr']];
			return ($r[0]==='h') ? $axs['http_root'].$s['dir'].$axs['dir_c']:$s['dir_fs_root'].$s['dir'].$axs['dir_c'];
		case 'editors': return ($r[0]==='h') ? AXS_PATH_CMS_HTTP.'editors/':AXS_PATH_CMS.'editors/';
		case 'lib': return ($r[0]==='h') ? AXS_PATH_CMS_HTTP.$axs['dir_lib']:AXS_PATH_CMS.$axs['dir_lib'];
		case 'lib.js': return ($r[0]==='h') ? AXS_PATH_CMS_HTTP.$axs['dir_lib_js']:AXS_PATH_CMS.$axs['dir_lib_js'];
		case 'modules': return ($r[0]==='h') ? AXS_PATH_CMS_HTTP.$axs['dir_modules']:AXS_PATH_CMS.$axs['dir_modules'];
		case 'plugins': return ($r[0]==='h') ? AXS_PATH_CMS_HTTP.$axs['dir_plugins']:AXS_PATH_CMS.$axs['dir_plugins'];
		case 'site.base': 
		case 'site_base': return ($r[0]==='h') ? AXS_PATH_CMS_HTTP.$axs['dir_site_base']:AXS_PATH_CMS.$axs['dir_site_base'];
		case 'site':
			$s=$axs['cfg']['site'][($site) ? $site:$axs['site_nr']];
			return ($r[0]==='h') ? $axs['http_root'].$s['dir']:$s['dir_fs_root'].$s['dir'];
		case 'tmp': return ($r[0]==='h') ? AXS_PATH_CMS_HTTP.$axs['dir_cfg'].'tmp/':AXS_PATH_CMS.$axs['dir_cfg'].'tmp/';
		default:
			axs_log(__FILE__, __LINE__, 'dir', __FUNCTION__.'(): Could not get directory path for "'.$key.'"');
			return false;
		}
	} #</axs_dir()>
#<Function to stop the application in a clean way. Do not use plain exit() because it's necessary 
# to handle unexpected output detection mechanism before exiting. />
function axs_exit($output='', $opts=array()) {
	global $axs;
	if (!empty($opts['headers'])) foreach ((array)$opts['headers'] as $v) header($v);
	if (isset($axs['ob'])) $axs['ob']=axs_ob_catch($axs['ob']);
	exit($output);
	} #</axs_exit()>
function axs_file_array_get($f, $key=false) { #<Load array from file and return element if a key provided />
	$a=(array)include($f);
	if ($key!==false) return axs_get($key, $a, null);
	return $a;
	} #</axs_file_array_get()>
function axs_file_choose($name=array(), $return=false, $path=false) {
	global $axs;
	if (!is_array($name)) $name=array($name=>$name);
	if (!$return) $return='f';
	if ($path===false) {
		$path=array();
		if (!empty($axs['page']['theme'])) $path[AXS_SITE_ROOT.$axs['site_dir'].$axs['dir_site'].$axs['page']['theme']]=$axs['http_root'].$axs['site_dir'].$axs['dir_site'].$axs['page']['theme']; # theme
		$path[axs_dir('site','f').$axs['dir_site']]=axs_dir('site','h').$axs['dir_site']; # current site
		$path[AXS_PATH_CMS.$axs['dir_plugins']]=AXS_PATH_CMS_HTTP.$axs['dir_plugins']; # plugins
		$path[AXS_PATH_CMS.$axs['dir_modules']]=AXS_PATH_CMS_HTTP.$axs['dir_modules']; # modules
		$path[AXS_PATH_CMS.$axs['dir_site_base']]=AXS_PATH_CMS_HTTP.$axs['dir_site_base']; # site base
		$path[AXS_PATH_CMS.$axs['dir_lib']]=AXS_PATH_CMS_HTTP.$axs['dir_lib']; # lib
		}
	if (is_string($path)) $path=array($path=>$path, );
	foreach ($name as $k=>$v) {
		foreach ($path as $kk=>$vv) if (file_exists($kk.$v)) switch ($return[0]) {
			case 'a': return array('f'=>$kk.$v, 'http'=>$vv.$v, 'time'=>date('ymdHis', filemtime($kk.$v)), );
			case 'h': return $vv.$v;
			default: return $kk.$v;
			}
		}
	} #</axs_file_choose()>
#<Check if a function is available. $f can be a string function name or array of names. In case of 
# multiple names, this function returns false if at least one of the names is not a callable function. />
function axs_function_ok($f) {
	foreach ((array)$f as $v) if (!function_exists($v) or (preg_match('/\b'.$v.'\b/i', ini_get('disable_functions')))) return false;
	return true;
	} #</axs_function_ok()>
function axs_get($key, $array=false, $default=null) { #<Get value from array. />
	if ($array===false) $array=$_REQUEST;
	return (isset($array[$key])) ? $array[$key]:$default;
	} #</axs_get()>
function axs_getm($key, $array=false, $default=null) { #<Get values from array. />
	foreach ($key as $k=>$v) {
		$key[$k]=((is_array($v)) && (!empty($v))) ? axs_getm($v, axs_get($k, $array, array(), $default)):axs_get($k, $array, $v);
		}
	return $key;
	} #</axs_getm()>
function axs_html_doc($body='', $title='', $head='', $attr=array()) {
	global $axs;
	header('Content-Type: text/html; charset='.$axs['cfg']['charset']);
	return '<!DOCTYPE html>'."\n".
	'<html lang="'.$axs['l'].'">'."\n".
	'<head>'."\n".
	'<meta charset="'.$axs['cfg']['charset'].'" />'."\n".
	'<title>'.$title.'</title>'."\n".
	$head.
	'</head>'."\n".
	'<body'.axs_get('body', $attr, '').'>'."\n".$body.'</body>'."\n".
	'</html>';
	} #</axs_html_doc()>
#<Function to use instead of htmlspecialchars(). Accepts both string and array as input. In case of single-byte charset it 
# will not replace & with &amp;, because browser may post cyrillic in form of html entities. />
function axs_html_safe($input) {
	global $axs;
	if (is_array($input)) {
		foreach ($input as $k=>$v) $input[$k]=axs_html_safe($v);
		return $input;
		}
	else return (($axs['cfg']['charset']=='utf-8') ? htmlspecialchars($input):str_replace(array('"', '<', '>'), array('&quot;', '&lt;', '&gt;'), $input));
	} #</axs_html_safe()>
function axs_localhost() {
	global $axs;
	if (($_SERVER['SERVER_NAME']!=='localhost') && ($_SERVER['SERVER_NAME']!=='a02') && ($_SERVER['SERVER_NAME']!=='a02laptop')) return false;
	$d=str_replace('.', '_', preg_replace('#/.*$#', '', trim($_SERVER['PHP_SELF'], '/')));
	if ($d==='axiscms') {
		$axs['cfg']['db'][1]['type']='MySQL';
		$axs['cfg']['site'][1]['db']=1;
		}
	if (($axs['cfg']['db'][1]['type']) or (!empty($_POST['db_1_type']))) foreach (array('host'=>'localhost', 'user'=>'root', 'pass'=>'', 'db'=>$d, 'local'=>true, ) as $k=>$v) $axs['cfg']['db'][1][$k]=$v;
	$axs['mail()']=false;
	return true;
	} #</axs_localhost()>
#<Function for handling system messages. />
function axs_log($file_name='', $line='', $msgtype='', $msg='', $exit=false, $notify='', $print='', $header='') {
	global $axs;
	if (!isset($axs['log'])) $axs['log']=new axs_log();
	$axs['log']->msg($file_name, $line, $msgtype, $msg);
	if ($header) header($header);
	if ($notify) {	if ((isset($axs['page']['head'])) && (is_array($axs['page']['head']))) $axs['page']['head'][]=($notify===true) ? $msg:$notify;	}
	if ($print) {	if ($print===true) echo $msg; else echo $print;	}
	if ($exit) exit();
	if ($msg) return $msg;
	} #</axs_log()>
function axs_log_php_errors($errno, $errstr, $errfile, $errline, $errcontext=array()) {
	axs_log($errfile, $errline, 'PHP', $errstr.' ('.__FUNCTION__.'())', false, '---');
	return false;
	} #</axs_log_php_errors()>
function axs_log_shutdown() {
	global $axs;
	#<Attempt to catch fatal errors />
	$e=(array)error_get_last();
	$tmp=axs_get('type', $e);
	if (in_array($tmp, array(E_ERROR, E_CORE_ERROR, E_PARSE, E_COMPILE_ERROR))) {
		#echo $e['message'];
		if (isset($axs['ob'])) axs_ob_catch($axs['ob']);
		axs_log($e['file'], $e['line'], 'PHP', 'Fatal error: '.$e['message'].' ('.__FUNCTION__.'())', false, '---');
		}
	#<Mail log messages />
	if (isset($axs['log'])) $axs['log']->mail();
	} #</axs_log_shutdown()>
register_shutdown_function('axs_log_shutdown');

#<Mail sending function with custom headers. />
function axs_mail($toemail, $subject, $txt, $html, $fromname, $fromemail, $attach=array(), $headers_modify=array()) {
	global $axs;
	if (!$axs['mail()']) return false;
	if (!empty($axs['cfg']['smtp_host'])) return axs_mail::send($toemail, $subject, $txt, $html, $fromname, $fromemail, $attach, $headers_modify);
	$n="\r\n";
	$headers_array=array(
		'From'=>mb_encode_mimeheader($fromname).' <'.$fromemail.'>',
		'X-Priority'=>'3',
		'X-MSMail-Priority'=>'Normal',
		'X-Mailer'=>'php/'.phpversion(),
		'MIME-Version'=>'1.0',
		'Content-type'=>'text/plain; charset='.$axs['cfg']['charset'],
		);
	$subject=preg_replace('/(MIME-Version:)+/i', 'MIME&ndash;Version:', $subject);
	$txt=preg_replace('/(Content-Type:)+/i', 'Content&ndash;Type:', $txt);
	$html=preg_replace('/(Content-Type:)+/i', 'Content&ndash;Type:', $html);
	$msg=$txt;
	if (($html) or (!empty($attach))) {
		$mime_boundary=md5(time());
		$headers_modify['Content-type']='multipart/mixed; boundary="'.$mime_boundary.'"';
		# Text Version
		if (!$txt) $txt='(HTML e-mail)';
		$tmp='--'.$mime_boundary.$n;
		$tmp.='Content-Type: text/plain; charset='.$axs['cfg']['charset'].$n;
		$tmp.='Content-Transfer-Encoding: 8bit'.$n.$n;
		$tmp.=$txt.$n.$n;
		if ($html) { # HTML version
			$tmp='--'.$mime_boundary.$n;
			$tmp.='Content-Type: text/html; charset='.$axs['cfg']['charset'].$n;
			$tmp.='Content-Transfer-Encoding: 8bit'.$n.$n;
			$tmp.=$html.$n.$n;
			}
		if (!empty($attach)) foreach ($attach as $k=>$v) { # Attachments
			$tmp.='--'.$mime_boundary.$n;
			$tmp.='Content-Type: '.$v['content_type'].'; name="'.$k.'"'.$n;
			$tmp.='Content-Transfer-Encoding: base64'.$n;
			$tmp.='Content-Description: '.$k.$n;
			$tmp.='Content-Disposition: attachment; filename="'.$k.'"'.$n.$n; # !! This line needs TWO end of lines !! IMPORTANT !!
			$tmp.=chunk_split(base64_encode($v['content'])).$n.$n;
			}
		# Finished
		$msg=$tmp.'--'.$mime_boundary.'--'.$n.$n; # finish with two eol's for better security. see Injection.
		}
	foreach ($headers_modify as $k=>$v) $headers_array[$k]=$v;
	$headers='';
	foreach ($headers_array as $k=>$v) $headers.=$k.': '.$v.$n;
	$subject=mb_encode_mimeheader($subject);
	//exit(dbg($toemail, $subject, $msg, $html, $headers, '-f '.$fromemail));
	//return mb_send_mail($toemail, $subject, $msg, $headers, '-f '.$fromemail);
	return mail($toemail, $subject, $msg, $headers, '-f '.$fromemail);
	} #</axs_mail()>
#<Function for logging output buffer. />
function axs_ob_catch($ob=false) {
	global $axs;
	if (!$ob) {
		$ob=(function_exists('ob_start') && !strpos(' '.ini_get('ob_start'), 'ob_start')) ? ob_start():false;
		}
	else { # END output buffering for error logging
		$ob=ob_get_contents();
		ob_end_clean();
		if ($ob) axs_log(__FILE__, __LINE__, 'output', $ob.' ('.__FUNCTION__.'())', false, '---');
		$ob=false;
		}
	return $ob;
	} #<axs_ob_catch()>
function axs_redir($url=false) { #<URL redirection function. />
	global $axs;
	if ($url===false) $url=$axs['http'].'://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
	if ($url[0]==='?') {
		$http_path=$_SERVER['SCRIPT_NAME'];
		if (basename($http_path)!='axiscms.php') $http_path=rtrim(dirname($_SERVER['SCRIPT_NAME']), '/\\').'/';
		$url=$axs['http'].'://'.$_SERVER['SERVER_NAME'].$http_path.$url;
		}
	if ($url[0]==='#') $url=$axs['http'].'://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'].$url;
	if (!headers_sent()) header('Location: '.$url);
	$url=htmlspecialchars($url);
	if (headers_sent()) return '<script>if(axs.throbber_set) axs.throbber_set(document.querySelector("body"));window.location="'.$url.'";</script><noscript><a href="'.$url.'">'.$url.'</a></noscript>';
	return axs_html_doc(
		'<a href="'.$url.'">'.$url.'</a>'."\n",
		$url,
		'<meta http-equiv="Refresh" content="0; url='.$url.'" />'."\n".
		'<script>window.location="'.$url.'";</script>'."\n"
		);
	} #</axs_redir()>
#<Determine if custom theme/template exists or use a default one. Then read the requested template file into a string. />
function axs_tpl($path, $filename, $file=__FILE__, $line=__LINE__) {
	global $axs;
	//dbg($path, $filename);
	$f=axs_file_choose($filename, 'fs', $path);
	if (is_file($f)) $tpl=file_get_contents($f);
	else {
		$tpl='---';
		#dbg($path, $filename);
		axs_log($file, $line, 'tpl', 'Template file "'.$f.'" ("'.$path.'": "'.implode(', ', (array)$filename).'") not found');
		}
	return $tpl;
	} #</axs_tpl()>
#<Check if the template contains a tag. $tag=true matches any tag. />
function axs_tpl_has($tpl, $tag=true) {
	if ($tag===true) return (strpos($tpl, '{$')===false) ? null:true;
	if ((strpos($tpl, '{$'.$tag.'}')!==false) || (strpos($tpl, '{$'.$tag.'=')!==false)) return true;
	} #</axs_tpl_has()>
#<Extract tags with JSON encoded parameters and return template with parameters removed. $tag=true & $partial=true returns all tags. />
#Example {$menu.menu1="type":"dropdown","dropout":true,"fixed":true}
function axs_tpl_tags($tpl, $tag=true, &$found=array(), $partial=false) {
	$t=($tag===true) ? '{$':'{$'.$tag;
	$t_len=strlen($t);
	$find=$replace=array();
	for ($i=$start=0; ($start=strpos($tpl, $t, $start))!==false && $i<10000; $i++) {
		$end=strpos($tpl, '}', $start+$t_len);
		$key=substr($tpl, $start+2, $end-$start-2);
		$start=$end;
		$p=array();
		if (count($tmp=explode('=', $key, 2))>1) {	$key=$tmp[0];	$p=$tmp[1];	}
		if ((!$partial) && ($key!==$tag)) continue;
		if ($p) {
			$find[]='{$'.$key.'='.$p.'}';	$replace[]='{$'.$key.'}';
			$p=(array)json_decode('{'.$p.'}', true);
			}
		if (empty($found[$key])) $found[$key]=$p;
		}
	return str_replace($find, $replace, $tpl);
	} #</axs_tpl_tags()>
#<Replaces {$*} tags in a template with array elements whitch have corresponding key names (eg. $vars['title']='My Page Title'; 
# would replace {$title} in a template with "My Page Title"). />
function axs_tpl_parse($tpl, $vars, $f='', $l='') {
	if (strpos($tpl, '{$')===false) return $tpl;
	$tags=array();
	if (is_array($vars)) foreach($vars as $k=>$v) {
		$tags[]='{$'.$k.'}';
		if ((!is_scalar($v)) && (!is_null($v))) {
			$vars[$k]='{$!'.gettype($v).'}';
			#axs_log(__FILE__, __LINE__, __FUNCTION__.'()', 'Data type of "'.$k.'"="'.gettype($v).'" '.$f.' ('.$l.')');
			}
		}
	return str_replace($tags, $vars, $tpl);
	} #</axs_tpl_parse()>
#<Function to make URL from an array. />
function axs_url(array $url, array $customize=array(), $html=true) {
	foreach ($customize as $k=>$v) {	if ($v===false) unset($url[$k]);	else $url[$k]=$v;	}
	$url=http_build_query($url, null, '&', PHP_QUERY_RFC3986); #Params with null value do not present in result string.
	return ($html) ? htmlspecialchars($url):$url;
	} #</axs_url()>

#<Debugging function. $log: log message silently, no direct output. Output info starting from 4th parameter. />
function axs_dbg($file='', $line='', $log=false) {
	global $axs;
	$data=func_get_args();
	if (!$data) exit((isset($axs['page'])) ? implode("\n", $axs['debug']):'-');
	foreach (array(0, 1, 2) as $v) {	unset($data[$v]);	}
	$out=array();
	ob_start();
	foreach ($data as $v) {	var_dump($v);	}
	$out[]=ob_get_clean();
	if ($log) return axs_log($file, $line, __FUNCTION__, implode("\n", $out));
	$out='<hr />'.$file.' ('.$line.'):'."\n".'<pre>'."\n".implode("\n".'</pre>'."\n".'<pre>'."\n", $out).'</pre>'."\n";
	if (!isset($axs['page'])) echo $out;
	$axs['debug'][]=$axs['dbg'][]=$out;
	return $out;
	} #</axs_dbg()>
function dbg() { #<Output info about input. />
	global $axs;
	$data=func_get_args();
	if (!$data) exit((isset($axs['page'])) ? implode("\n", $axs['debug']):'-');
	ob_start();
	echo '<hr />';
	foreach ($data as $v) {
		echo '<pre>'."\n";
		var_dump($v);
		echo '</pre>'."\n";
		}
	$out=ob_get_clean();
	if (!isset($axs['page'])) print $out;
	$axs['debug'][]=$axs['dbg'][]=$out;
	return $out;
	} #</dbg()>
?>