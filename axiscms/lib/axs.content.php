<?php #2022-04-05
class axs_content {
	static $c_length=255;
	static $favicon=array('ico', 'gif', 'png', 'jpg', 'jpeg', 'svg');
	static $menu_pic=array('cls'=>'.cls', 'act'=>'.act', ''=>'', 'this.cls'=>'.cls', 'this.act'=>'.act', 'this'=>'', );
	static $menu_pic_files=array('cls'=>'', 'act'=>'_act', );
	static $menu_pic_ext=array('jpg', 'jpeg', 'png', 'gif', 'svg', 'swf');
	static $socialmedia_og_image=array('png', 'jpg', 'jpeg', );
	static $index_fields=array(
		'publ'=>'publ', 'hidden'=>'hidden', 'user'=>'user', 'updated'=>'updated', 
		'c'=>1, 'title'=>2, 'plugin'=>3, 'description'=>4, 'pw'=>5, 'restrict'=>6, 'url'=>7, 'target'=>8, 'title_bar'=>9, 'style'=>10,
		);
	static $index_fields_meta=array('publ'=>0, 'hidden'=>1, 'user'=>2, 'updated'=>3, );
	static $url_fake=false;
	static $site_cfg=array(
		'media'=>array('media'=>'', 'img_proc_t'=>1, 'img_w_t'=>200, 'img_h_t'=>2000, 'img_proc'=>0, 'img_w'=>800, 'img_h'=>600, 'img_q'=>80, 'mp_w'=>'', 'mp_h'=>'', 'mp_play'=>0, ),
		'theme'=>'',
		'code_head'=>'',
		'code_bottom'=>'',
		);
	static function init($site_nr) { #<Initialize content />
		global $axs, $axs_content, $axs_user;
		ini_set("auto_detect_line_endings", true);
		$axs['site_nr']=(isset($axs['cfg']['site'][$site_nr])) ? $site_nr:1;
		if (defined('AXS_URL_FAKE')) self::$url_fake=true;
		$axs_content=array('index'=>array(), 'content'=>array(), 'frontpage'=>'', 'search'=>array('str'=>'', 'result'=>array()), 'title_bar'=>'', 'title_site'=>'', 'l'=>$axs['l'], );
		$axs_content['index']=self::data_get(0, 0, $axs['page']['content'], $axs['c']);
		if (($axs['c']) && (!isset($axs_content['content'][$axs['c']]))) {
			header('HTTP/1.1 301 Moved Permanently');
			axs_redir('?l='.$axs['l']);
			if (axs_valid::id($axs['c'])!=$axs['c']) {	$msgtype='xssalert'; $msg='Possible XSS or PHP injection';	}
			else {	$msgtype='url'; $msg='Page ID "'.$axs['c'].'" not found @ '.$axs['http'].'://'.$_SERVER['SERVER_NAME'].$axs['http_root'].$axs['site_dir'];	}
			axs_log(__FILE__, __LINE__, $msgtype, $msg, true);
			}
		$tmp=current($axs_content['index']);
		while (axs_get('c', $tmp)) {
			if (axs_get('plugin', $tmp)==='menu') $tmp=current($tmp['submenu']);
			else {	$axs_content['frontpage']=$tmp['c'];	break;	}
			}
		$axs['url']['c']=$axs['c'];
		
		if (isset($_GET['axs']['content-search'])) { #<Search>
			if (strlen($tmp=trim($_GET['axs']['content-search']))>=2) {
				$axs_content['search']['str']=substr($tmp, 0, 100);
				$axs_content['search']['str_html']=axs_html_safe($axs_content['search']['str']);
				$axs_content['search']['str_regex']=preg_quote($axs_content['search']['str'], '/');
				$axs_content['search']['array']=explode(' ', preg_replace('/ +/', ' ', str_replace(',', ' ', $axs_content['search']['str'])));
				axs_content_search::result();
				}
			} #</Search>
		
		$dir_content=axs_dir('content');
		#<Favicon />
		foreach (self::$favicon as $v) if (file_exists($dir_content.'favicon.'.$v)) {
			$axs['page']['head']['favicon']='<link type="image/x-icon" href="'.axs_dir('content', 'http').'favicon.'.$v.'?v='.filemtime($dir_content.'favicon.'.$v).'" rel="shortcut icon" />'."\n";
			break;
			}
		#<Menu pics>
		$axs_content['menu.pic']='';
		if (isset($axs['page']['menu'])) {
			foreach ($axs['page']['menu'] as $k=>$v) { # Define default pic for each menu. 
				foreach (self::menu_pic_get(array(1=>array('c'=>$k, 'dir'=>''))) as $kk=>$vv) $axs_content[$k.'.pic'.(($kk)?'.'.$kk:'')]=$vv;
				if ($axs_content['content'][$axs['c']]['path'][1]['c']===$k) {
					if ($axs_content[$k.'.pic.act']) $axs_content[$k.'.pic']=$axs_content[$k.'.pic.act'];
					$axs_content['menu.pic']=$axs_content[$k.'.pic'];
					}
				$axs_content[$k.'.pic.ext']=preg_replace('/^.+\./', '', $axs_content[$k.'.pic']); # Necessary for menu_pic_get() later to operate recursively. 
				}
			if (!$axs_content['menu.pic']) foreach ($axs['page']['menu'] as $k=>$v) if (!empty($axs_content[$k.'.pic'])) {
				$axs_content['menu.pic']=$axs_content[$k.'.pic'];
				break;
				}
			$tmp=self::menu_pic_get($axs_content['content'][$axs['c']]['path']);
			if ($tmp['']) foreach ($tmp as $k=>$v) $axs_content['menu.pic'.(($k)?'.'.$k:'')]=$v;
			}
		#</Menu pics>
		#<Frame translations />
		$tr=self::tr_get(array(), __FILE__, __LINE__, 'index-tr', '', $axs['l']) or $tr=new axs_tr();
		$axs_content['title_site']=$tr->t('title_site');
		foreach ($tr->get() as $k=>$v) {
			if (!isset($axs['page'][$k])) $axs['page']=array_merge(array($k=>$v), $axs['page']);
			else $axs['page'][$k]=$v;
			}
		} #</init()>
	static function data_get($menu_level=0, $maxlevel=0, $content=array(), $c='', $dir='', $path=array(), $c_ok=false) { #<Get content index, data and form content structure />
		global $axs, $axs_content;
		$menu_level++;
		if ($maxlevel<2) $maxlevel=100;
		#<Load index />
		$menu=self::menu_get($dir, $path);
		#<Find additional content areas>
		if ($menu_level==1) foreach ($menu as $cl) {
			if ((isset($axs['page']['content'][$cl['c']])) or (isset($axs['page']['menu'][$cl['c']]))) continue;
			$axs['page']['content.'.$cl['c'].'.title']=axs_html_safe($cl['title']);
			if (!axs_tpl_has($axs['template'], 'content.'.$cl['c'])) continue;
			$content[$cl['c']]=$axs['page']['content'][$cl['c']]=$cl;
			}
		#</Find additional content areas>
		
		$index=array();
		foreach($menu as $k=>$cl) { 
			if ((!$cl['c']) or (!self::published($cl, $axs['time']))) continue;
			if (($dir==='') && ($cl['c']==='recycler')) continue;
					
			if (!empty($content)) { #<Build content>
				if ((($cl['c']===$c) or (!$c)) && (!$c_ok)) {
					if ($cl['plugin']==='menu') /*or ($cl['plugin']==='menu-page')*/ $c='';
					else {
						if (($axs['c']) && (!$c)) {
							header('HTTP/1.1 301 Moved Permanently');
							if (self::$url_fake) exit(axs_redir(
								$axs['http'].'://'.$_SERVER['SERVER_NAME'].axs_url::fake($axs['url'], array('c'=>$cl['c'])))
								);
							else exit(axs_redir('?'.axs_url($axs['url'], array('c'=>$cl['c']), false)));
							}
						if ($cl['url']) axs_exit(axs_redir($cl['url']));
						
						if (!$axs['c']) $axs['c']=$c=$cl['c'];
						//$content[$axs['c']]=array();
						$axs_content['title_bar']=$cl['title_bar'];
						$c_ok=true;
						}
					}
				if (($cl['c']===$axs['c']) or (isset($content[$cl['c']]))) {
					if (!isset($axs_content['content'][$cl['c']])) $axs_content['content'][$cl['c']]=$cl+array(
						'text'=>($cl['plugin']!=='menu-page') ? self::text_get($cl['c'], $dir):array(),
						);
					}
				} #</Build content>
			
			# <Permission check />
			$denied=!self::permission_get($cl['path']);
			
			#<Recurse into submenu>
			if (($cl['plugin']==='menu') or ($cl['plugin']==='menu-page')) {
				$cl['submenu']=self::data_get($menu_level, $maxlevel, $content, $c, $dir.$cl['c'].'/', $cl['path'], $c_ok);
				if ($denied) $cl['submenu']=array();
				} #</Recurse into submenu>
			if (!empty($content)) {
				if (isset($axs_content['content'][$axs['c']])) $c_ok=true; #<Enable empty $_GET['c']
				}
			unset($cl['pw'], $cl['restrict'], $cl['text']);
			$index[$cl['c']]=$cl;
			}
		return $index;
		} #</data_get()>
	static function index_c_find($c=false, $plugin=false, $index=false, $path=array()) {
		global $axs, $axs_content;
		$submenus=$found=array();
		if (!is_array($index)) $index=$axs_content['index'];
		foreach ($index as $k=>$v) {
			$v['l']=$axs['l'];
			$v['path']=array_merge(array(0=>''), $path, array($v /*array('c'=>$v['c'], 'dir'=>$v['dir'], 'title'=>$v['title'], )*/));
			unset($v['path'][0]);
			if (($c) && ($v['c']===$c)) {				
				$found[$v['c']]=$v;
				if ($plugin===false) return $found;
				}
			if (($plugin!==false) && ($v['plugin']===$plugin)) {	$found[$v['c']]=$v;	}
			if (isset($v['submenu'])) $submenus[$v['c']]=$v;
			}
		foreach ($submenus as $k=>$v) $found+=self::index_c_find($c, $plugin, $v['submenu'], $v['path']);
		return $found;
		} #</index_c_find()>
	static function index_row($cl) {
		$r=explode('|', $cl);
		array_shift($r);
		array_pop($r);
		$meta=explode(',', $r[0]);
		$cl=array();
		foreach (self::$index_fields_meta as $k=>$v) $r[$k]=axs_get($v, $meta);
		foreach (self::$index_fields as $k=>$v) $cl[$k]=axs_get($v, $r);
		return $cl;
		} #</index_row()>
	static function path_get($path=false, $get=false) {
		global $axs, $axs_content;
		if ($path===false) $path=$axs_content['content'][$axs['c']]['path'];
		if (($path[1]['plugin']==='menu') or (($path[1]['plugin']==='menu-page') && (count($path)>1))) unset($path[1]);
		if ($get) foreach ($path as $k=>$v) $path[$k]=$v[$get];
		return $path;
		} #</path_get()>
	static function menu_get($dir, $path=false, $l=false) {
		global $axs;
		if (!$l) $l=$axs['l'];
		if ($path===false) {
			$path=preg_split('#/#', '-/'.$dir, -1, PREG_SPLIT_NO_EMPTY);
			unset($path[0]);
			$d='';
			foreach ($path as $k=>$v) {
				$v=array('c'=>$v, 'dir'=>$d, );
				foreach (self::$index_fields as $kk=>$vv) $v[$kk]=axs_get($kk, $v);
				$path[$k]=$v;
				$d.=$v['c'].'/';
				}
			}
		$menu_level=count($path)+1;
		$tmp=axs_dir('content').$dir.'index_'.$l.'.php';
		$menu=(file_exists($tmp)) ? file($tmp, FILE_SKIP_EMPTY_LINES):array(/*0=>'',*/ );
		foreach($menu as $k=>$cl) { #<Build index row>
			$cl=self::index_row($cl);
			$cl['l']=$l;
			$cl['dir']=$dir;
			$cl['path']=$path+array(
				$menu_level=>array(
					'c'=>$cl['c'], 'title'=>$cl['title'], 'dir'=>$dir, 'plugin'=>$cl['plugin'], 'style'=>$cl['style'], 'pw'=>$cl['pw'], 'restrict'=>self::permission_parse($cl['restrict']),
					),
				);
			$menu[$k]=$cl;
			} #</Build index row>
		unset($menu[0]);
		return $menu;
		} #</menu_get()>
	static function menu_pic_get($path, $key=false) {
		global $axs, $axs_content;
		foreach (self::$menu_pic as $k=>$v) $pic[$k]='';
		$ext=axs_get($path[1]['c'].'.pic.ext', $axs_content, '');
		$dir_base=axs_dir('content');
		if (!$ext) foreach (self::$menu_pic_ext as $v) if (file_exists($dir_base.$path[1]['c'].'_'.$axs['l'].'.'.$v)) {
			$ext=$v;
			break;
			}
		if (!$ext) return $pic; #If no default pic, file ext is unspecified and return empty filenames. 
		#<Current menu item's pics>
		$last=end($path);
		foreach (self::$menu_pic_files as $k=>$v) {
			if (file_exists($dir_base.($f=$last['dir'].$last['c'].'_'.$axs['l'].$v.'.'.$ext))) $pic['this.'.$k]=$pic[$k]=$f;
			}
		$pic['this']=$pic['']=$pic['cls'];
		if (($last['c']===$axs['c']) && ($pic['act'])) $pic['this']=$pic['']=$pic['act'];
		#</Current menu item's pics>
		if (!$pic['']) { #If current menu item has no pic, get the default pic from the parent levels. 
			$c=$dir=$f='';
			foreach (array_reverse($path) as $k=>$v) {
				foreach (self::$menu_pic_files as $kk=>$vv) {
					if (file_exists($dir_base.($f=$v['dir'].$v['c'].'_'.$axs['l'].$vv.'.'.$ext))) $pic[$kk]=$f;
					}
				$pic['']=(($path[1]['c']===$axs_content['content'][$axs['c']]['path'][1]['c']) && ($pic['act'])) ? $pic['act']:$pic['cls'];
				if ($pic['']) break;
				}
			}
		if ($key!==false) $pic=$pic[$key];
		return $pic;
		} #</menu_pic_get()>
	
	static function text_get($c=false, $dir=false, $l=false, $split=false) {
		global $axs, $axs_content;
		if ($c===false) $c=$axs_content['c'];
		if ($dir===false) $dir=$axs_content['dir'];
		if ($l===false) $l=$axs_content['l'];
		if (!isset($axs_content['content'][$c]['text'])) {
			$f=axs_dir('content').$dir.$c.'_'.$l.'.php';
			$axs_content['content'][$c]['text']=(file_exists($f)) ? file($f, FILE_SKIP_EMPTY_LINES):array();
			}
		return ($split) ? axs_tab_proc($axs_content['content'][$c]['text']):$axs_content['content'][$c]['text'];
		} #</text_get()>
	static function form_get($plugin_def=array(), $file='', $line='', $c=false, $dir=false, $l=false) {
		global $axs, $axs_content;
		if ($c===false) $c=$axs_content['c'];
		if ($dir===false) $dir=$axs_content['dir'];
		if ($l===false) $l=$axs_content['l'];
		if (isset($axs_content['content'][$c]['form'])) return $axs_content['content'][$c]['form'];
		if (file_exists($f=axs_dir('content').$dir.$c.'_'.$l.'.form.php')) {
			$form=(array)include($f);
			$axs_content['content'][$c]['cfg']=$form[''];
			return $axs_content['content'][$c]['form']=$form;
			}
		return array();
		} #</form_get()>
	static function form_parse($axs_local, $form, $unset=false) {
		global $axs_content;
		if (is_array($form)) { #From can be pre-made
			if (count($form)>1) {
				$form=new axs_form_site($axs_local, $form, $_REQUEST);
				$form->post();
				$form=$form->build();
				}
			else $form='';
			}
		if ($unset) unset($axs_content['content'][$unset]['form']);
		return $form;
		} #</form_parse()>
	static function tr_get($plugin_def=array(), $file='', $line='', $c=false, $dir=false, $l=false) {
		global $axs, $axs_content;
		if ($c===false) $c=$axs_content['c'];
		if ($dir===false) $dir=$axs_content['dir'];
		if ($l===false) $l=$axs_content['l'];
		if (isset($axs_content['content'][$c]['tr'])) return $axs_content['content'][$c]['tr'];
		if (file_exists($f=axs_dir('content').$dir.$c.'_'.$l.'.tr.php')) {
			$tr=new axs_tr((array)include($f));
			if (isset($axs_content['content'][$c])) $axs_content['content'][$c]['tr']=$tr;
			return $tr;
			}
		if (isset($plugin_def['tr'])) {
			if (!is_array($plugin_def['tr'])) $plugin_def['tr']=array(array());
			$data=(!empty($plugin_def['tr'][$l])) ? $plugin_def['tr'][$l]:current($plugin_def['tr']);
			axs_log($file, $line, 'content', 'Missing translation array for "'.$f.'"');
			$tr=new axs_tr($data);
			if (isset($axs_content['content'][$c])) $axs_content['content'][$c]['tr']=$tr;
			return $tr;
			}
		return new axs_tr();
		} #</tr_get()>
	
	static function posts_get($p, $psize, $desc=false, $time=false, $c=false, $l=false) {
		global $axs, $axs_content;
		$p=intval($p);
		if ($c==false) $c=$axs_content['c'];
		$dir=$axs_content['content'][$c]['dir'];
		if ($l===false) $l=$axs['l'];
		//$pager=new axs_pager($p, array($psize_default+0,$psize+0), $psize, );
		$files=array();
		$strlen=strlen($c.'_'.$l.'.post');
		$handle=opendir(axs_dir('content').$dir);
		while (($file=readdir($handle))!==false) {
			if (strncmp($file, $c.'_'.$l.'.post', $strlen)==0) $files[]=$file;
			}
		closedir($handle);
		($desc) ? rsort($files):sort($files);
		# Get posts from current page
		if (!isset($axs_content['content'][$c]['post'])) {
			$data=(is_file($f=axs_dir('content').$dir.$files[$p-1])) ? file($f, FILE_SKIP_EMPTY_LINES):array();
			$data=axs_tab::proc($data, array('time'=>$time));
			foreach ($data as $k=>$v) $data[$k]=array(
				'time'=>date('d.m.Y H:i',$v[0]['publ']), 'name'=>$v[1], 'email'=>$v[2], 'message'=>$v[3], 'user_ip'=>$v[4], 'hostname'=>$v[5], 
				);
			if ($desc) $data=array_reverse($data);
			$axs_content['content'][$c]['post']=$data;
			}
		# Get total nr of posts
		return array('total'=>count($files)*$psize, 'p'=>$p, 'psize'=>$psize);
		} #</posts_get()>
	static function permission_get($path) { #<Permission check />
		global $axs, $axs_user, $axs_content;
		$permission=true;
		$pw=false;
		$restrict=array();
		foreach ($path as $k=>$v) {
			if (!empty($v['restrict'])) $restrict[]=$v['restrict'];
			if (strlen(axs_get('pw', $v))) $pw=$v['pw'];
			}
		if (!empty($restrict)) {
			if (!defined('AXS_LOGINCHK')) axs_user::init();
			$restrict=axs_user::permission_compact($restrict);
			if (AXS_LOGINCHK==1) $permission=(!empty($axs_user['g']['admin'])) ? true:axs_user::permission_get_role($restrict, 'r');
			else $permission=false;
			}
		if (isset($axs_content['content'][$v['c']])) {
			$axs_content['content'][$v['c']]['pw']=$pw;
			$axs_content['content'][$v['c']]['restrict']=$restrict;
			}
		if (($permission) && ($pw)) {
			$tmp='';
			if (isset($_COOKIE['pw'])) $tmp=$_COOKIE['pw'];
			if (isset($_GET['pw'])) $tmp=$_GET['pw'];
			if (isset($_POST['pw'])) $tmp=$_POST['pw'];
			if (trim($tmp)==$pw) setcookie('pw', trim($tmp), $axs['time']+3600, dirname($_SERVER['PHP_SELF']));
			else $permission=false;
			}
		return $permission;
		} #</permission_get()>
	static function permission_parse($str) { #<Parse string of privileges data />
		parse_str($str, $str);
		$parsed=array();
		foreach ($str as $k=>$v) foreach (explode(',', $v) as $vv) $parsed[$k][$vv]=$vv;
		return $parsed;
		} #</permission_parse()>
	static function plugin_dirs($r='f') {
		global $axs;
		return array('site'=>axs_dir('site', $r).$axs['dir_site'], 'plugins'=>axs_dir('plugins', $r), 'site_base'=>axs_dir('site_base', $r), );
		} #</plugin_dirs()>
	#<Function for loading plugins in a private scope />
	static function plugin_load($c, $section='', $axs_local=array(), &$section_vr=array(), $include=false) {
		global $axs, $axs_user, $axs_content;
		$axs_local['section']=$section;
		if (!isset($axs_local['c'])) $axs_local['c']=$c;
		if (!isset($axs_content['content'][$axs_local['c']]['path'])) {
			$tmp=self::index_c_find($c);
			if ($tmp) {	foreach ((array)current($tmp) as $k=>$v) $axs_content['content'][$axs_local['c']][$k]=$v;	}
			else axs_log(__FILE__, __LINE__, 'content', 'Content item "'.$c.'" not found!');
			}
		//foreach ($content_customize as $k=>$v) $axs_content['content'][$axs_local['c']][$k]=$v;
		#if (!isset($axs_content['content'][$axs_local['c']]['plugin'])) dbg($axs_local['c'],$axs_content['content'][$axs_local['c']]);
		if (!$axs_content['content'][$axs_local['c']]['plugin']) $axs_content['content'][$axs_local['c']]['plugin']='deflt';
		if (!isset($axs_local['url'])) $axs_local['url']=$axs['url'];
		foreach ($axs_content['content'][$axs_local['c']] as $k=>$v) if (!isset($axs_local[$k])) $axs_local[$k]=$v;	
		if (!isset($axs_local['l'])) $axs_local['l']=$axs['l'];
		$axs_local['vr']=$section_vr;
		#<Permission check>
		$permission=self::permission_get($axs_content['content'][$axs_local['c']]['path']);
		if (!$permission) {
			if ((defined('AXS_LOGINCHK')) && (AXS_LOGINCHK==1)) $axs['msg']['axs_login']='denied';
			$axs_content['content'][$axs_local['c']]['text']='';
			unset($axs_content['content'][$axs_local['c']]['form']);
			foreach (array('c'=>'index-login', 'plugin'=>'authform', 'dir'=>'', 'pic'=>'') as $k=>$v) $axs_local[$k]=$v;
			}
		#</Permission check>
		foreach (array('c','dir','l') as $v) $axs_content[$v]=$axs_local[$v];
		self::text_get();
		self::tr_get();
		self::form_get();
		//foreach ($content_customize as $k=>$v) $axs_content['content'][$axs_local['c']][$k]=$v;
		#<Load page stylesheets and scripts if it's the main content section />
		if ($section==='content') {
			if ($tmp=axs_file_choose($f=$axs_local['plugin'].'.plugin.css', 'http')) $axs['page']['head'][$f]='<link type="text/css" href="'.$tmp.'" rel="stylesheet" media="all" />'."\n";
			if ($tmp=axs_file_choose($f=$axs_local['plugin'].'.plugin.js', 'http')) $axs['page']['head'][$f]='<script src="'.$tmp.'"></script>'."\n";
			foreach ($axs_local['path'] as $k=>$v) if (!empty($v['style'])) {
				$tmp=$v['style'].'.plugin.css';
				$axs['page']['head'][$tmp]='<link type="text/css" href="'.axs_dir('site', 'http').$axs['dir_site'].$tmp.'" rel="stylesheet" media="all" />'."\n";
				}
			}
		#<Load content plugin />
		if (!$include) foreach (self::plugin_dirs() as $v) if (file_exists($tmp=$v.$axs_local['plugin'].'.plugin.php')) {
			$include=$tmp;
			break;
			}
		$out=(file_exists($include)) ? include($include):'';
		#<Build content form if set />
		if (isset($axs_content['content'][$axs_local['c']]['form'])) $out.=self::form_parse($axs_local, $axs_content['content'][$axs_local['c']]['form']);
		if (isset($axs_local['vr'])) foreach ($axs_local['vr'] as $k=>$v) $section_vr[$k]=$v;
		return $out;
		} #</plugin_load()>
	static function published(&$cl, $time) {
		return ($cl['publ']<$time && (!$cl['hidden'] or $cl['hidden']>$time));
		} #</published()>
	static function site_cfg_get($key=false) {
		global $axs;
		$data=axs_file_array_get(axs_dir('content').'index.php');
		foreach (self::$site_cfg as $k=>$v) if (!isset($data[$k])) $data[$k]=$v;
		return ($key!==false) ? $data[$key]:$data;
		} #</site_cfg_get()>
	static function url(array $url, array $customize=array(), $html=true) {
		global $axs;
		if (self::$url_fake) return axs_url::fake($url, $customize);
		$url=axs_url($url, $customize, $html);
		return ($url) ? '?'.$url:'';
		} #</url()>
	} #</class::axs_content>
#2005-11 ?>