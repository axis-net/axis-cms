//2021-03-23
//axs.include('jquery');
axs.form={
	forms:{},
	browse_timer:false,
	browse_timeout:500,
	browse_values:{},
	customize:{},
	ftypes:{img:{'jpg':true,'jpeg':true,'png':true,'webp':true,'tif':true,'tiff':true,'bmp':true,'gif':true}},
	fileFunctions:((window.btoa)&&(window.File)&&(window.FileReader)&&(window.FileList)&&(window.Blob))?true:false,
	init:function(form_id){
		var o=this;
		var el;
		o.form_id=form_id;
		//<form being updated />
		o.saving=false;
		o.el=document.getElementById(o.form_id);
		o.el.addEventListener('submit',this.formSubmit);
		window.onbeforeunload=this.formChangesLeave;//<form not saved warning />
		jQuery('form span.delete input[type="submit"]').click(function(){
			jQuery(this).closest('form').find('input, select, textarea').removeAttr('required');
			});
		this.init_elements(o.el);
		window.axs.form.forms[o.form_id]=o;
		if (this.customize['']) this.customize['']();
		if (this.customize[form_id]) this.customize[form_id]();
		},//</init()>
	init_elements:function(node){	
		//<Type color>
		//jQuery(node).find('.element input[type="color"]').each(function(){	axs.colorpicker.add(this);	});
		//</Type color>
		//<Type file>
		jQuery(node).find('input[type="file"]').change(axs.form.element_file_select);
		if (axs.form.fileFunctions){
			jQuery(node).find('.element input[type="file"].upload_fake').removeAttr('disabled').change(axs.form.element_file_upload_fake);
			}
		//</Type file>
		//<Type table>
		jQuery(node).find('.element.table').each(function(){	axs.form.element_table_init(this);	});
		//</Type table>
		//<Type text-join>
		jQuery(node).find('.text-join input[type="text"]').each(function(){
			jQuery(this).removeAttr('disabled').focus(axs.form.browse_open).keydown(axs.form.browse_keyboard_nav).keyup(function(){
				if (axs.form.browse_timer) clearTimeout(axs.form.browse_timer);
				axs.form.browse_timer=window.setTimeout('axs.form.browse_search_text("'+this.id+'");',axs.form.browse_timeout);
				});
			});
		jQuery(node).find('.text-join a.browse').hide().focus(axs.form.browse_open);
		//console.log(jQuery(node).find('a.browse-window'));
		jQuery(node).find('a.browse-window').attr('data-overlay-content-iframe','1');/*.on('click',function(){
			var id=jQuery(this).closest('.element').attr('id')+'_browse-window';
			if (!jQuery('#'+id).length) jQuery(this).after('<iframe class="browse-window" id="'+id+'" src="'+this.getAttribute('href')+'"></iframe>');
			jQuery(this).attr('data-overlay-content-iframe','1');
			});*/
		//</Type text-join>
		//<Type timestamp>
		var el=jQuery(node).find('input[type="date"]');
		if (el.length) {
			var datefield=document.createElement("input");
			datefield.setAttribute("type", "date");
			if (datefield.type!="date") jQuery(el).datepicker({	dateFormat:'yy-mm-dd',	firstDay:1	});
			}
		//</Type timestamp>
		//<Type submit>
		jQuery(node).find('.element.submit span.delete input[type="submit"]').click(function(){
			var form_id=jQuery(this).closest('form').attr('id');
			var name='_select';//this.getAttribute('name').replace(/_commitjQuery/,'');
			var checked=jQuery('#'+form_id+' .element.submit span.delete input[type="checkbox"]').is(':checked');
			if (checked) jQuery('#'+form_id+' input, #'+form_id+' select, #'+form_id+' textarea').removeAttr('required');
			return checked;
			}); //</Type submit>
		//<Type clear>
		var el=jQuery(node).find('input[type="button"].clear');
		if (el.length) {
			el.removeAttr('disabled');
			el.on('click',this.formClear);
			}
		//</Type clear>
		//<datalist polyfill />
		//jQuery(node).find('input[list]').datalist();
		},//</init_elements()>
	//<element type file functions>
	element_file_select:function(evt){
		var form_id=jQuery(this).closest('form').attr('id');
		var container='#'+form_id+' #'+this.id+'_container';
		jQuery('#'+form_id+' span[data-fieldname="'+this.name+'"].progress').remove();
		if(axs.form.fileFunctions) {
			jQuery(container).find('figure.thumb img').remove();
			jQuery(container).find('figure.thumb figcaption').html('&nbsp;');
			}
		var html='';
		var r=[];
		var f=evt.target.files;
		for(var i=0; i<f.length; i++) {
			html+='<span class="progress" data-fieldname="'+this.name+'" data-filename="'+this.name+'.'+f[i].name+'" data-filesize="'+f[i].size+'"><span>'+f[i].name+'</span> <progress value="0" max="100">0%</progress></span>';
			if((axs.form.fileFunctions)&&(f[i].type.match('image.*'))){
				var id=this.id+'-'+i+'thumb';
				jQuery(container).find('figure.thumb figcaption').html(f[i].name);
				jQuery(container).find('figure.thumb').append('<img id="'+id+'" src="#" alt="'+f[i].name+'" />');
				r[i]=new FileReader();
				r[i].axs={ id:id, container:container };
				r[i].onload=function(e){
					jQuery('#'+this.axs.id).attr('src',e.target.result);
					};
				r[i].readAsDataURL(f[i]);
				}
			}
		jQuery(this).after(html);
		},//</element_file()>
	element_file_upload_fake:function(evt){
		var form_id=jQuery(this).closest('form').attr('id');
		var container_id=form_id+'.'+this.name.replace(/\[|\]/g,'')+'.upload_fake';
		jQuery('#'+container_id.replace(/\./g,'\\.')).remove();
		var container=document.createElement('span');
		container.id=container_id;
		jQuery(container).insertAfter(this);
		var r=[];
		var f = evt.target.files;
		if (f.length<1) return alert("Failed to load file");
		for(var i=0; i<f.length; i++){
			r[i]=new FileReader();
			r[i].axs={ name:this.name.replace('[]',''), multi:(this.name.search('[]')>0)?'[]':'', file:f[i], container:container };
			r[i].addEventListener('loadstart',function(evt) {
				this.axs.progress=document.createElement('progress');
				if (evt.lengthComputable) this.axs.progress.max=evt.total;
				this.axs.container.appendChild(this.axs.progress);
				}, false);
			r[i].addEventListener('progress',function(evt) {
				if (evt.lengthComputable) this.axs.progress.value=evt.loaded;
				}, false);
			r[i].onload=function(e){
				var el=document.createElement('input');
				el.name=e.target.axs.name+'[name]'+e.target.axs.multi;
				el.value=e.target.axs.file.name;
				container.appendChild(el);
				var el=document.createElement('textarea');
				el.name=e.target.axs.name+'[content]'+e.target.axs.multi;
				el.innerHTML=window.btoa(e.target.result);
				e.target.axs.container.appendChild(el);
				}
			r[i].readAsBinaryString(f[i]);
			}
		},//</element_file_upload_fake()>
	element_file_upload_progress:function(form_id){
		axs.ajax(
			axs.url(document.location.href,{'axs%5Bgw%5D':'fup_progress','axs%5Bupl_id%5D':jQuery('#'+form_id+' input[type="hidden"].upl_id').val()}),
			function(status,data){
				var done=false;
				if (data.responseText==='-') return;
				if (!data.responseText) done=false;
				else {
					data=JSON.parse(data.responseText);
					for(var k in data){
						var el=jQuery('#'+form_id+' span[data-filename="'+k+'"].progress');
						var current=data[k];
						var total=jQuery(el).data('filesize');
						var val=(current<total) ? Math.ceil(current/total*100):100;
						jQuery(el).find('progress').attr('value',val).html(val+'%');
						if (val<100) done=false;
						}
					}
				if (!done) window.setTimeout('axs.form.element_file_upload_progress("'+form_id+'");',1500);
				}
			);
		},//</element_file_upload_progress()>
	//</element type file functions>
	//<element type table functions>
	element_table_init:function(el){
		jQuery(el).find('th input[type="checkbox"]._check_all').click(function(){
			var chk=jQuery(this).is(':checked');
			var col=jQuery(this).closest('th').attr('class').replace(/ .*$/g,'');
			jQuery(this).closest('table').find('td.'+col+' input[type="checkbox"]').attr('checked',chk).trigger('change');
			});
		jQuery(el).find('table>*>tr').each(function(){
			if (jQuery(this).hasClass('_init')) return;
			jQuery(this).addClass('_init');
			axs.form.init_elements(this);
			jQuery(this).find('._add input[type="submit"]').click(function(){
				e.preventDefault();
				var form=jQuery(this).closest('form');
				var table=jQuery(this).closest('table');
				var tr=jQuery(this).closest('tr');
				//jQuery('#'+jQuery(table).attr('id')+' tr._loading').remove();
				//var tmp='<tr class="_loading"><td colspan="'+jQuery(tr).find('>*').length+'"></td></tr>';
				for (var i=-1; i>-500; i--) if (!document.getElementById(jQuery(table).attr('id')+'_'+i)) break;
				var tmp=document.querySelector('#'+jQuery(table).attr('id')+'\\.tpl').value;
				//console.log('#'+jQuery(table).attr('id')+'\\.tpl',tmp);
				tmp=tmp.replace(/\{\$key\}/g,i);
				tmp=tmp.replace(/\{\$nr\}/g,'');
				var tbody=jQuery(table).find('tbody');
				if (!tbody.length) jQuery(table).append('<tbody></tbody>');
				if (this.parentNode.tagName!=='TD') jQuery(table).find('tbody').prepend(tmp);
				else jQuery(tr).after(tmp);
				//axs.throbber_set(document.querySelector('#'+jQuery(table).attr('id')+' tr._loading td'));
				//for (var i=-1; i>-500; i--) if (!document.getElementById(jQuery(table).attr('id')+'_'+i)) break;
				//jQuery(this).closest('table').find('tr._loading').replaceWith(data);
				axs.form.element_table_init(jQuery(this).closest('.element.table'));
				axs.form.element_table_rank_reorder(this);
				//var url=axs.url(jQuery(form).attr('action'),{
				//	'axs%5Bajax%5D':'form.'+jQuery(form).attr('id')+'.'+jQuery(table).attr('id'),'axs%5Bid%5D':i+','
				//	});
				//jQuery.ajax({
				//	url:url,context:tr,
				//	success:function(data){
				//		jQuery(this).closest('table').find('tr._loading').replaceWith(data);
				//		axs.form.element_table_init(jQuery(this).closest('.element.table'));
				//		axs.form.element_table_rank_reorder(this);
				//		},
				//	error:function(o){	jQuery(this).closest('form').find('tr._loading td').html(axs.ajax_error(o));	}
				//	});
				return false;
				});
			jQuery(this).find('.element._del input[type="checkbox"]').click(function(){
				(this.checked) ? jQuery(this).closest('tr').addClass('delete'):jQuery(this).closest('tr').removeClass('delete');
				jQuery(this).closest('tr').find('*[required], *[data-required="1"]').each(function(){
					jQuery(this).attr('data-required','1');
					(jQuery(this).closest('tr').hasClass('delete')) ? jQuery(this).removeAttr('required'):jQuery(this).attr('required','required');
					});
				});
			jQuery(this).find('input[type="number"].rank').each(function(i){
				if (!i) jQuery(this).closest('table').addClass('rank');
				jQuery(this).closest('tr').addClass('rank');
				});
			});
		jQuery(el).find('.rank>tbody').sortable({
			opacity:.75,revert:true,axis:"y",cursor:"move",scroll:true,delay:100,stop:function(){
				axs.form.element_table_rank_reorder(this);
				}
			});
		},//</element_table_init()>
	element_table_rank_reorder:function(el){
		jQuery(el).closest('table').find('input[type="number"].rank').each(function(i){this.value=i+1;});
		},//</element_table_rank_reorder()>
	//</element type table functions>
	
	
	
	//<element type text-join functions>
	browse_ajax:function(url,selector){
		axs.form.browse_selector=selector;
		axs.form.browse_result='';
		jQuery(axs.form.browse_selector).html(axs.throbberHTML);
		jQuery.ajax({	url:url,	async:false,	dataType:'html',	success:function(data){
				data=axs.ajax_body(data);
				//alert(data);
				axs.form.browse_result=data;
				if(axs.form.browse_selector){
					jQuery(axs.form.browse_selector).html(data);
					jQuery(axs.form.browse_selector+' a.update').click(axs.form.browse_update).keydown(axs.form.browse_keyboard_nav);
					jQuery(axs.form.browse_selector+' tr td a.update').each(function(){
						jQuery(this).focus(function(){
							jQuery(this.parentNode.parentNode.parentNode).find('tr').removeClass('focus');
							jQuery(this.parentNode.parentNode).addClass('focus');
							});
						jQuery(this.parentNode.parentNode).click(function(){	jQuery(this).find('a.update').trigger('click');	});
						}
						);
					//jQuery(axs.form.browse_selector+' .set-focus').focus();
					}
				},
			error:function(o,status,thrown){	alert(status);	}
			});
		},//</browse_ajax()>
	browse_close:function(){//<Close browse window />
		axs.form.browse_close_all(this);
		return false;
		},//</browse_close_end()>
	browse_close_all:function(el){
		var form_id=jQuery(el).closest('form').attr('id');
		jQuery('#'+form_id+' .element div.browse').remove();
		return form_id;
		},//</browse_close_all()>
	browse_close_end:function(){//<Close browse window and move to next element />
		var opener_id=axs.form.browse_id_field(this);
		var form_id=axs.form.browse_close_all(this);
		var el=jQuery('#'+form_id+' :input, #'+form_id+' a').filter(':visible');
		var foc=false;
		for (var i=0, l=el.length; i<l; i++){
			if(!foc){
				if(el[i].getAttribute('id')==opener_id)foc=el[i];
				continue;
				}
			else{
				jQuery(el[i]).focus();
				break;
				}
			}
		return false;
		},//</browse_close_end()>
	browse_id_element:function(el){
		return jQuery(el).closest('.element').attr('id').replace(/.container$/,'');
		//return jQuery(el).attr('id').replace(/\..*$/,'');
		},//</browse_id_element()>
	browse_id_field:function(el){
		return jQuery(el).closest('div.browse').attr('id').replace(/.browse$/,'');
		//return jQuery(el).attr('id').replace(/\..*$/,'');
		},//</browse_id_field()>
	browse_keyboard_nav:function(event){;
		if (event.keyCode==axs.keycodes.up||event.keyCode==axs.keycodes.down) event.preventDefault();
		switch(this.tagName){
			case 'INPUT':
				if (event.keyCode==axs.keycodes.down) jQuery('#result_content a:first').focus();
				if (event.keyCode==axs.keycodes.up) jQuery('#result_content a:last').focus();
				if (event.keyCode==axs.keycodes.esc) axs.form.browse_close_all(this);
				break;
			case 'A':
				if (event.keyCode==axs.keycodes.up){
					if (jQuery(this.parentNode.parentNode).is('tr:first-child')) jQuery(this.parentNode.parentNode.parentNode).find('tr:last a:first').focus();
					else jQuery(this.parentNode.parentNode).prev().find('a').focus();
					}
				if (event.keyCode==axs.keycodes.down){
					if (jQuery(this.parentNode.parentNode).is('tr:last-child')) jQuery(this.parentNode.parentNode.parentNode).find('tr:first a:first').focus();
					else jQuery(this.parentNode.parentNode).next().find('a').focus();
					}
				if (event.keyCode==axs.keycodes.left) 
				jQuery('#'+axs.form.browse_id_field(this)).focus();
				if (event.keyCode==axs.keycodes.right) jQuery(this).trigger('click');
				break;
			}
		},//</browse_keyboard_nav()>
	browse_open:function(event){
		if (this.tagName=='A') event.preventDefault();
		if (this.tagName=='INPUT') this.select();
		var id=axs.form.browse_id_element(this);
		var browse_id=this.id+'.browse';
		if (document.getElementById(browse_id)) return true;
		axs.form.browse_close_all(this);//<Remove all browse windows />
		//<Create new browse window />
		jQuery(this).after('<div id="'+browse_id+'" class="browse"><a class="close" href="#">x</a><div class="result"></div><a class="close visuallyhidden" href="#">x</a></div>');
		browse_id=browse_id.replace(/\./g,'\\.');
		jQuery('#'+browse_id+' a.close').focus(axs.form.browse_close_end).click(axs.form.browse_close);
		axs.form.browse_ajax(document.getElementById(id+'.browse_link').href+'&ajax=','#'+browse_id+'>.result');
		},//</browse_open()>
	browse_search_text:function(id){
		el=document.getElementById(id);
		if (typeof(axs.form.browse_values[id])=='undefined') axs.form.browse_values[id]=false;
		if ((axs.form.browse_values[id])&&(axs.form.browse_values[id]===el.value)) return;
		var browse_link=document.getElementById(axs.form.browse_id_element(el)+'.browse_link');
		browse_link.href=axs.url(browse_link.href,{	'text':el.value	});
		axs.form.browse_ajax(browse_link.href+'&ajax=result_content','#result_content');
		axs.form.browse_values[id]=el.value;
		},//</browse_search_text()>
	browse_search_value:function(){
		var browse_link=axs.form.browse_values_set(this.id,this.value,false);
		axs.form.browse_ajax(browse_link.href+'&ajax=value','');
		axs.form.browse_values_set(this.id,this.value,axs.form.browse_result);
		},//</browse_search_text()>
	browse_update:function(event){
		if (this.tagName==='A') event.preventDefault();
		var id=axs.form.browse_id_element(this);
		var value=this.getAttribute('data-value');//axs.url_get('value',this.href);
		var text=this.getAttribute('data-text');//axs.url_get('text',this.href);
		var initial=jQuery('#'+id).val(), copy=this.getAttribute('data-copy');
		axs.form.browse_values_set(id,unescape(value),unescape(text));
		if ((value!==initial)&&(copy)){
			copy=jQuery.parseJSON(copy);
			var container=document.getElementById(id+'_container');
			for (var k in copy) {
				var s=(container.tagName==='TD') ? container.parentNode.getAttribute('id')+'_'+k:k;
				jQuery('#'+s).val(copy[k]);
				}
			}
		var selector_to=(jQuery('#result_content').length) ? '#result_content':'.element.text-join .result';
		if (/\bbrowser\b/.exec(this.className)) axs.form.browse_ajax(this.href+'&ajax=result_content',selector_to);
		else jQuery('#'+id+'_container div.browse a.close:first').trigger('focus');
		return false;
		},//</browse_update()>
	browse_values_set:function(id,value,text){
		var browse_link=document.getElementById(id+'.browse_link');
		if (value!==false){
			browse_link.href=axs.url(browse_link.href,{	'value':escape(value)	});
			jQuery('#'+id).val(value);
			}
		if (text!==false){
			browse_link.href=axs.url(browse_link.href,{	'text':escape(text)	});
			jQuery('#'+id+'\\.text').val(text);
			jQuery('#'+id+'\\.browse_link span').html(text);
			}
		return browse_link;
		},//</browse_values_set()>
	//</element type text-join functions>
	//<form functions>
	formClose:function(){
		jQuery("#"+this.form_id+' #s_container').append('<button type="button" class="close">[-]</button>');
		jQuery("#"+this.form_id+' #s_container button.close').click(function(e){
			var form=axs.elementParent(this,'form');
			var c='axs[form]['+form.getAttribute('id')+'][closed]';
			//var c='aaa';
			if(axs.class_has(this,'closed')){
				axs.class_rem(form.querySelectorAll('.element'),'js_hide');
				axs.class_rem(this,'closed');
				this.innerHTML='[-]';
				axs.cookie_set(c,'0');
				}
			else{
				axs.class_add(form.querySelectorAll('.element'),'js_hide');
				axs.class_add(this,'closed');
				this.innerHTML='[+]';
				axs.cookie_set(c,'1');
				}
			});
		if(axs.cookie_get('axs[form]['+this.form_id+'][closed]')!=='0') jQuery("#"+this.form_id+' #s_container button.close').trigger('click');
		},//</formClose()>
	formSend:function(){
		jQuery("#"+this.form_id).on("submit",function(e){
			e.preventDefault();
			jQuery.post(this.action,jQuery(this).serialize());
			});
		},//</submit()>
	formChanges:function() {
		/* 
		* FormChanges(string FormID | DOMelement FormNode)
		* Returns an array of changed form elements.
		* An empty array indicates no changes have been made.
		* NULL indicates that the form does not exist.
		*
		* By Craig Buckler,		http://twitter.com/craigbuckler
		* of OptimalWorks.net		http://optimalworks.net/
		* for SitePoint.com		http://sitepoint.com/
		* 
		* Refer to http://blogs.sitepoint.com/javascript-form-change-checker/
		*
		* This code can be used without restriction. 
		*/
		// get form
		var form = document.getElementById(this.form_id);
		if (!form || !form.nodeName || form.nodeName.toLowerCase() != "form") return null;
		
		// find changed elements
		var changed = [], n, c, def, o, ol, opt;
		for (var e = 0, el = form.elements.length; e < el; e++) {
			n = form.elements[e];
			if (!n.getAttribute('name')) continue;
			if (n.getAttribute('name').charAt(0)==='_') continue;
			c = false;
			
			switch (n.nodeName.toLowerCase()) {
				// select boxes
				case "select":
					def = 0;
					for (o = 0, ol = n.options.length; o < ol; o++) {
						opt = n.options[o];
						c = c || (opt.selected != opt.defaultSelected);
						if (opt.defaultSelected) def = o;
					}
					if (c && !n.multiple) c = (def != n.selectedIndex);
					break;
				// input / textarea
				case "textarea":
				case "input":
					switch (n.type.toLowerCase()) {
						case "checkbox":
						case "radio":
							// checkbox / radio
							c = (n.checked != n.defaultChecked);
							break;
						default:
							// standard values
							c = (n.value != n.defaultValue);
							break;				
						}//</switch>
					break;
				}//</switch>
			if (c) changed.push(n);
			}//</for>
		return changed;
		},//</formChanges()>
	formChangesLeave:function(){
		for (id in window.axs.form.forms){
			if (axs.class_has(document.getElementById(id),'search'))  continue;//<Do not check search forms />
			if (window.axs.form.forms[id].saving) continue;
			var msg=[];
			var changes=axs.form.forms[id].formChanges();
			for(var i=0; i<changes.length; i++) if (changes[i].name) msg.push(changes[i].name);
			if (changes.length>0) {
				var l=document.getElementsByTagName('html')[0].getAttribute('lang');
				var txt=(l.charAt(0)+l.charAt(1)=='et') ? "Vormi sisestatud andmed ei ole salvestatud. Kas oled kindel, et soovid lahkuda?":"Your form updates have not be saved. Do You really want to leave?";
				if (!confirm(txt+'\n('+msg.join(', ')+')')) return false;
				}
			}//</for>
		},//</window.onbeforeunload>
	formChangesSaving:function(form_id){	axs.form.forms[form_id].saving=true;	},//</formChangesSaving()>
	formClear:function(){
		var form=document.getElementById('person_find');//jQuery(this).closest('form');
		for (i=0, l=form.elements.length; i<l ;i++){
			var field_type=form.elements[i].type.toLowerCase();
			switch (field_type){
				case "fieldset":
				case "submit":
				case "button":
				case "reset":
				case "hidden":
				case "object":
				case "output":
					break;
				case "radio":
				case "checkbox":
					if (form.elements[i].checked) form.elements[i].checked=false;
					break;
				case "select-one":
				case "select-multi":
					form.elements[i].selectedIndex=-1;
					break;
				default:
					form.elements[i].value="";
					break;
				}
			}
		},//</formClear()>
	formSubmit:function(event){
		var id=jQuery(this).attr('id');
		var fupl=jQuery('#'+id).find('input[type="file"]');
		if (fupl.length){
			//<Enable file upload progress monitoring element only if a file is selected for upload />
			var fselected=false;
			for(var i=0, len=fupl.length; i<len; i++) if(fupl[i].value) fselected=true;
			if (fselected) jQuery('#'+id).find('input[type="hidden"].upl_id').removeAttr('disabled');
			//<Check if selected files fit in the size limits>
			var msg=[], fsizes=0, limit=jQuery('#'+id).data('cfg'), f=jQuery('#'+id+' span.progress');
			if (limit) {
				for(var i=0, l=f.length; i<l; i++){
					fsizes+=jQuery(f[i]).data('filesize');
					if (jQuery(f[i]).data('filesize')>limit.upload_max_filesize) msg.push('- File too large: "'+jQuery(f[i]).data('filename')+'"! ('+axs.form._unit(jQuery(f[i]).data('filesize'))+'MiB/'+axs.form._unit(limit.upload_max_filesize)+'MiB)');
					}
				if (fsizes>limit.post_max_size) msg.push('- Total size of files too large! ('+axs.form._unit(fsizes)+'MiB/'+axs.form._unit(limit.post_max_size)+'MiB)');
				if (f.length>limit.max_file_uploads) msg.push('- Too many files selected! ('+f.length+'/'+limit.max_file_uploads+')');
				if (msg.length>0) {	if (!confirm(msg.join('\n')+'\n	Continue?')) return false;	}
				}//</Check if selected files fit in the size limits>
			axs.form.element_file_upload_progress(id);
			}
		axs.form.formChangesSaving(id);
		if (jQuery(this).hasClass('ajax-post')){
			event.preventDefault();
			var url=this.action.replace(/\#/,' #');
			//console.log(url);
			axs.ajax(url,this,{post:jQuery(this).serialize()+'&_submit=1'});
			}
		},//</formSubmit()>
	//</form functions>
	//<Misc. />
	progressGet(key,target){
		if(typeof(target)==='string') target=document.querySelectorAll(target);
		var a=axs.fn_args(arguments,{key:'',target:'',refresh:3});
		var r=JSON.parse(axs.ajax(window.location.protocol+'//'+window.location.hostname+window.location.pathname+'?axs%5Bgw%5D%5Bprogress%5D='+key,null,{async:false}));
		if (r) {
			for(var i=0; i<target.length; i++){
				target[i].innerHTML=r.txt;
				if (target[i].tagName.toLowerCase()==='progress') target[i].setAttribute('value',r.complete);
				}
			if (r.complete>=100) return;
			}
		window.setTimeout(axs.form.progressGet,a.refresh*1000,key,/*token,*/target,a.refresh);
		},//</progressGet()>
	_unit(i){
		var a=axs.fn_args(arguments,{i:'',u:'MiB',prc:2});
		return (i/(1024*1024)).toFixed(a.prc);
		}//</_unit()>
	}//</class::axs.form>
//2009-01-26