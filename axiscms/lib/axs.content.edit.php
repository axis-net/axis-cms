<?php #2021-05-31
class axs_content_edit extends axs_admin {
	var $key_ajax='edit-content-m';
	var $key_save='axs_save';
	var $menu_depth=0;
	var $save=false;
	function __construct($site_nr, $l, $dir_user) {
		global $axs, $axs_user, $axs_content;
		$this->site_nr=$site_nr;
		$this->site=$axs['cfg']['site'][$this->site_nr];
		$this->db=$this->site['db'];
		$this->px=$this->site['prefix'];
		$this->l=(isset($this->site['langs'][$l])) ? $l:key($this->site['langs']);
		$this->d=$this->f_secure(axs_get('d', $_GET));
		$this->path=preg_split('~/~', '-/'.$this->d, -1, PREG_SPLIT_NO_EMPTY);
		unset($this->path[0]);
		$this->dir_plugins=axs_dir('site').$axs['dir_site'];
		$this->dir=$this->f_secure(axs_get('dir', $_GET));
		$this->dir_content=axs_dir('content');
		$this->dir_content_http=$axs['http_root'].$this->site['dir'].$axs['dir_c'];
		$this->dir_user=$this->f_secure($dir_user);
		
		$this->content_init(axs_get('c', $_GET));
		
		$this->dir_page=$this->dir_content.$this->dir_user.$this->d;
		$this->d_page=$this->dir_user.$this->d;
		$this->dir_page_files=$this->dir_page;
		$this->d_page_files=$this->d;
		if (is_dir($this->dir_page_files.$axs['c'].'_files/')) {
			$this->dir_page_files.=$axs['c'].'_files/';
			$this->d_page_files.=$axs['c'].'_files/';
			}
		$this->fs=new axs_filesystem($this->dir_page, $this->site_nr);
		$this->fs->msg=&$this->msg;
		$this->fs->d=&$this->dir_page;
		$this->site_cfg=@include($this->dir_content.'index.php');
		} #</axs_content_edit()>
	function lg($function, $line, $msg='', $tmp='') { # log errors
		return axs_admin::log(__FILE__, $line, get_class($this), $function, $msg);
		} #</log()>
	
	function dir_set($dir=false) {
		if ($dir===false) $this->d=$this->d_bak;
		else {
			$this->d_bak=$this->d;
			$this->d=$dir;
			}
		$this->dir_page=$this->dir_content.$this->dir_user.$this->d;
		} #</dir_set()>
	function load_editor($file) {
		global $axs, $axs_content;
		$e=include($file);
		if (axs_file_choose($tmp=$axs_content['plugin'].'.edit.css')) $axs['page']['head'][$tmp]='<link type="text/css" rel="stylesheet" href="'.axs_file_choose($tmp, 'http').'" />'."\n";
		if (axs_file_choose($tmp=$axs_content['plugin'].'.edit.js')) $axs['page']['head'][$tmp]='<script src="'.axs_file_choose($tmp, 'http').'"></script>'."\n";
		if (is_string($e)) return $e;
		if (is_callable(array($e, 'installer'))) $e->installer($this);
		return $e->ui();
		} #</load_editor()>
	
	# ------------ <Installer functions> ------------
	function installer_sql($tables, $check_types=false) {
		if (!isset($this->sql_structure)) $this->sql_structure=new axs_sql_structure($this->db);
		//exit(dbg($tables));
		$this->sql_structure->table_install($tables, $check_types);
		$this->form->msg+=$this->sql_structure->msg;
		} #</installer_sql()>
	# ------------ </Installer functions> -----------
	
	# ------------ <Content functions> --------------
	function c_get($d, $c, $l, $recursive=false) { # function to find if content ID exists and return index entry if found
		$menu=$this->menu_get($d, $l, false);
		foreach ($menu as $k=>$v) if (isset($v['c'])) {
			//if ($d==='menu1/' && strpos(' '.$c, 'almm')) dbg($d,$c,$l,$v['c'],$v['c']===$c);
			$v['nr']=$k;
			if ($v['c']===$c) return $v;//+array('nr'=>$k, );
			if (($recursive) && (($v['plugin']==='menu') or ($v['plugin']==='menu-page'))) $v=$this->c_get($d.$v['c'].'/', $c, $l, $recursive);
			if ((isset($v['c'])) && ($v['c']===$c)) return $v;
			}
		return array();
		} #</c_get()>
	function content_init($c) {
		global $axs, $axs_user, $axs_content;
		$axs['c']=$this->f_secure($c);
		$axs['l']=$this->l;
		$axs_content=array('section'=>'content', 'c'=>$axs['c'], 'dir'=>$this->dir, 'l'=>$axs['l'], 'url'=>$axs['url'], );
		$axs_content['content'][$axs['c']]['path']=array();
		$axs_content['index']=$this->menu_get_index(0, '');
		$axs_content['plugin']=(isset($axs_content['content'][$axs['c']]['plugin'])) ? $axs_content['content'][$axs['c']]['plugin']:axs_get('plugin', $_GET);
		$axs_content['content'][$axs['c']]['c']=$axs['c'];
		$axs_content['content'][$axs['c']]['l']=$axs['l'];
		$axs_content['content'][$axs['c']]['section']=$axs_content['section'];
		$this->restrict=axs_user::permission_compact($axs_content['content'][$axs['c']]['path'], 'restrict');
		if (!empty($this->restrict)) $this->restrict['admin']=array();
		$axs_content['title_site']=$this->site_title_get($this->l);
		$axs_content['menu-pic_ext'][$axs['c']]='';
		foreach (axs_content::$menu_pic_ext as $v) { # menu default backgroud picture properties
			$tmp=(isset($axs_content['content'][$axs['c']]['path'][1]['c'])) ? $axs_content['content'][$axs['c']]['path'][1]['c']:false;
			if (($tmp) && (file_exists($f=$this->dir_content.$tmp.'_'.$axs['l'].'.'.$v))) {
				$axs_content['menu-pic_ext'][$axs['c']]=$v;
				break;
				}
			}
		} #</content_init()>
	#<Get content status>
	function content_status_deletable($d) {
		global $axs, $axs_user;
		$perm=(($this->dir_user.$d) or (axs_user::permission_get('admin'))) ? true:false;
		return $perm;
		} #</content_status_deletable()>
	function content_status_recycled($d) {
		$n='recycler/';
		return ($d===$n) ? true:false;
		} #</content_status_deletable()>
	#</Get content status>
	#<Get or unset data>
	function content_get($c, $array=false, $d=false, $l=false) { # Get content as a string or an array of rows
		global $axs, $axs_content;
		if (!$c) axs_log(__FILE__, __LINE__, 'script', $error['c']='Missing $c @ content_get()');
		if ($d===false) $d=$this->dir_user.$this->d;
		if (!$l) $l=$this->l;
		if (isset($axs_content['content'][$c]['text']) && $l==$this->l) $data=$axs_content['content'][$c]['text'];
		else {
			$data=@file($this->dir_content.$d.$c.'_'.$l.'.php') or $data=array();
			$data=array_merge(array("\n"), $data);
			unset($data[0]);
			}
		if (!$array) {	if (is_array($data)) $data=implode('', $data);	}
		return $data;
		} #</content_get()>
	function content_tr_get($c, $d=false, $l=false) {
		if (!$c) axs_log(__FILE__, __LINE__, 'script', $error['c']='Missing $c @ content_tr_get()');
		if ($d===false) $d=$this->dir_user.$this->d;
		if (!$l) $l=$this->l;
		if (file_exists($f=$this->dir_content.$d.$c.'_'.$l.'.tr.php')) $data=include($f);
		else $data=array();
		return $data;
		} #</content_tr_get()>
	function content_form_get($c, $d=false, $l=false) {
		if (!$c) axs_log(__FILE__, __LINE__, 'script', $error['c']='Missing $c @ content_form_get()');
		if ($d===false) $d=$this->dir_user.$this->d;
		if (!$l) $l=$this->l;
		if (file_exists($f=$this->dir_content.$d.$c.'_'.$l.'.form.php')) $data=include($f);
		else $data=array();
		return $data;
		} #</content_form_get()>
	function content_unset($key=array('text','form')) {
		global $axs, $axs_content;
		foreach ((array)$key as $v) $axs['editor']->vr[$v]=false;
		} #</content_unset()>
	#</Get or unset data>
	
	function menu_get($d=false, $l=false, $check_error=true) { # Get content as rows array or string
		global $axs_content;
		if (!$l) $l=$this->l;
		$data=array();
		if (is_file($f=$this->dir_content.$d.'index_'.$l.'.php')) $data=file($f);
		else {
			if ($check_error) $this->lg(__FUNCTION__, __LINE__, $this->msg($this, '', 'Menu file "'.$f.'" not found!'));
			}
		$menu=array();
		foreach ($data as $cl) {
			$cl=axs_content::index_row($cl);
			$cl['dir']=$this->dir_user.$d;
			$menu[]=$cl;
			}
		//if ($d==='') $axs_content['title_site']=$this->site_title_get($l);
		unset($menu[0]);
		return $menu;
		} #</menu_get()>
	function menu_get_index($level, $dir) {
		global $axs, $axs_content;
		$level++;
		if ($level>$this->menu_depth) $this->menu_depth=$level;
		$menu=$this->menu_get($dir);
		$index=array();
		foreach($menu as $nr=>$cl) {
			$cl['nr']=$nr;
			if (($cl['c']) && ($cl['c']===$axs['c'])) {
				$cl['restrict']=axs_content::permission_parse($cl['restrict']);
				$axs_content['content'][$cl['c']]+=$cl;
				$axs_content['content'][$cl['c']]['path'][$level]=$cl;
				if (($cl['plugin']!=='menu') && ($cl['plugin']!=='menu-page')) {
					$axs_content['content'][$cl['c']]['text']=$this->content_get($cl['c'], true);
					if ($tmp=$this->content_tr_get($cl['c'])) $axs_content['content'][$cl['c']]['tr']=$tmp;
					if ($tmp=$this->content_form_get($cl['c'])) $axs_content['content'][$cl['c']]['form']=$tmp;
					}
				}
			if (($cl['plugin']==='menu') or ($cl['plugin']==='menu-page')) {
				if (($cl['c']===axs_get($level, $this->path)) or ($cl['c']===$axs['c'])) {
					if (!is_array($cl['restrict'])) $cl['restrict']=axs_content::permission_parse($cl['restrict']);
					$axs_content['content'][$cl['c']]['path'][$level]=$cl;
					if ($cl['c']!=$axs['c']) $axs_content['content'][$axs['c']]['path'][$level]=$cl;
					$cl['submenu']=$this->menu_get_index($level, $dir.$cl['c'].'/');
					}
				}
			$index[$cl['c']]=$cl;
			}
		return $index;
		} #</menu_get_index()>
	function plugin_form_get($plugin, $l=false) { # Get form elements from plugin
		global $axs, $axs_user;
		if ($plugin==='') return false;
		if (!$l) $l=$this->l;
		if ($f=axs_file_choose($plugin.'.plugin.php')) include($f);
		else axs_log(__FILE__, __LINE__, 'content', 'Missing plugin file "'.$f.'"');
		if (!isset($plugin_def['form'])) return false;
		return $this->plugin_form_get_parse($plugin_def['form'], $l);
		} #</lugin_form_get()>
	function plugin_form_get_parse($array, $lang) { # Get form elements from plugin
		$form=array();
		foreach ($array[''] as $k=>$v) if (is_array($v)) foreach (array('label','comment','txt','value',/*'msg'*/) as $vv) if (isset($v[$vv])) {
			if (!is_array($v[$vv])) $v[$vv]=array(''=>$v[$vv]);
			reset($v[$vv]);
			$array[''][$k][$vv]=(isset($v[$vv][$lang]))	? $v[$vv][$lang]:current($v[$vv]);
			if (isset($v['options'])) foreach ($v['options'] as $kk=>$vv) {
				#if (!isset($vv['value'])) $vv['value']=$kk;
				if (!is_array($vv['label'])) $vv['label']=array(''=>$vv['label']);
				$array[''][$k]['options'][$kk]['label']=(isset($vv['label'][$lang])) ? $vv['label'][$lang]:current($vv['label']);
				//$array[''][$k]['options'][$kk]=$vv;
				}
			}
		foreach ($array as $k=>$v) {
			foreach (array('label','comment','txt','value','msg') as $vv) if (isset($v[$vv])) {
				if (!is_array($v[$vv])) $v[$vv]=array(''=>$v[$vv]);
				$v[$vv]=(isset($v[$vv][$lang]))	? $v[$vv][$lang]:current($v[$vv]);
				}
			if (isset($v['options'])) foreach ($v['options'] as $kk=>$vv) {
				if (!isset($vv['value'])) $vv['value']=$kk;
				if (!is_array($vv['label'])) $vv['label']=array(''=>$vv['label']);
				$vv['label']=(isset($vv['label'][$lang])) ? $vv['label'][$lang]:current($vv['label']);
				$v['options'][$kk]=$vv;
				}
			$form[$k]=$v;
			}
		//foreach ($form[''] as $k=>$v) if ((is_array($v)) && (isset($v['value']))) $v=$v['value'];
		if (isset($form['']['tr'])) foreach ($form['']['tr'] as $k=>$v) if (is_array($v)) {
			$form['']['tr'][$k]=(isset($v[$lang])) ? $v[$lang]:current($v);
			}
		return $form;
		} #</lugin_form_get()>
	function plugin_tr_get($plugin, $l=false, $auto=true) { # Get translation keys from plugin
		global $axs, $axs_user, $axs_content;
		$axs_local=$axs_content['content'][$axs['c']];
		if (!$l) $l=$this->l;
		if (!$plugin) return false;
		if ($f=axs_file_choose($plugin.'.plugin.php')) include($f);
		else {
			axs_log(__FILE__, __LINE__, 'content', 'Missing plugin file "'.$f.'"');
			}
		if (!isset($plugin_def['tr'])) return false;
		if ($auto) {	$form=(isset($plugin_def['tr'][$l])) ? $plugin_def['tr'][$l]:@current($plugin_def['tr']);	}
		else {	$form=(isset($plugin_def['tr'][$l])) ? $plugin_def['tr'][$l]:false;	}
		return $form;
		} #</lugin_tr_get()>
	function plugins_get($plugin=false) {
		global $axs, $axs_user, $axs_content;
		$axs_local=($axs['c']) ? $axs_content['content'][$axs['c']]:array();
		if (!isset($this->plugins)) {
			$this->plugins=$disabled=array();
			foreach (axs_content::plugin_dirs() as $dir) foreach ($this->f_list($dir, '/\.plugin\.php$/') as $k=>$v) {
				unset($plugin_def);
				if ($v==='deflt.plugin.php') continue;
				$p=preg_replace('/\.plugin\.php$/', '', $v);
				if (isset($this->plugins[$p])) continue;
				$axs_local['plugin']=$p;
				$plugin_def=$this->plugins_get_scoped($axs_local, $dir.$v);
				if (empty($plugin_def)) {	$disabled[$p]=true;	continue;	}
				foreach (array('name','help') as $vv) {
					if (!isset($plugin_def[$vv])) $plugin_def[$vv]='';
					if (is_array($plugin_def[$vv])) $plugin_def[$vv]=(!empty($plugin_def[$vv][$axs['cfg']['cms_lang']])) ? $plugin_def[$vv][$axs['cfg']['cms_lang']]:current($plugin_def[$vv]);
					}
				$this->plugins[$p]=$plugin_def;
				}
			foreach ($disabled as $k=>$v) unset($this->plugins[$k]);
			$this->plugins=axs_fn::array_sort($this->plugins, 'name');
			}
		if ($plugin!==false) return $this->plugins[$plugin];
		} #</plugins_get()>
	function plugins_get_scoped(&$axs_local, $f) {
		global $axs, $axs_user, $axs_content;
		include($f);
		if (isset($plugin_def)) return $plugin_def;
		} #</plugins_get_scoped()>
	function posts_get($c, $p=1, $limit=0, $desc=false, $d=false, $l=false) {
		global $axs, $axs_content;
		if ($d===false) $d=$this->dir_user.$this->d;
		if (!$l) $l=$this->l;
		$p=new axs_pager($p, $limit, $limit);
		$axs_content['content'][$c]['post_files']=array(0=>'');
		foreach ($this->f_list($this->dir_content.$d, '/^'.$c.'_'.$l.'\.post[0-9]{4}\.php$'.'/') as $file) $axs_content['content'][$c]['post_files'][]=$file;
		sort($axs_content['content'][$c]['post_files']);
		unset($axs_content['content'][$c]['post_files'][0]);
		# Get posts from current page
		if (!empty($axs_content['content'][$c]['post_files'])) {
			$axs_content['content'][$c]['post']=(is_file($f=$this->dir_content.$d.$axs_content['content'][$c]['post_files'][$p->p])) ? file($f):array();
			$axs_content['content'][$c]['post']=array_merge(array(0), $axs_content['content'][$c]['post']);
			unset($axs_content['content'][$c]['post'][0]);
			}
		else $axs_content['content'][$c]['post']=array();
		# Get total nr of posts
		$total=count($axs_content['content'][$c]['post_files'])*$p->psize;
		return array($total, $p->p, $p->start, $p->psize, $desc);
		} #</posts_get()>
	function site_title_get($l=false) {
		if (!$l) $l=$this->l;
		return axs_file_array_get($this->dir_content.'index-tr_'.$l.'.tr.php', 'title_site');
		} #</site_title_get()>
	function styles_get() {
		global $axs;
		if (!isset($this->styles)) {
			$this->styles=array();
			foreach ($this->f_list($this->dir_plugins, '/\.plugin\.css$/') as $k=>$v) {
				$v=preg_replace('/\.plugin\.css$/', '', $v);
				$this->styles[$v]=array('name'=>$v, );
				}
			$this->styles=axs_fn::array_sort($this->styles, 'name');
			}
		} #</styles_get()>
	# ------------ </Content functions> --------------
	# ------------ <UI functions> --------------------
	function ui_ajax() {
		global $axs, $axs_content, $axs_user;
		$this->url['s2']='m';
		$this->url['browse']=urlencode($_GET['browse']);
		$this->url['value']=$this->url['text']=$this->url[$this->key_ajax]='';
		$this->ajax_txt=array();
		exit(axs_html_doc($this->ui_ajax_menu($axs_content['index']),'browse'));
		} #</ui_ajax()>
	function ui_ajax_menu($index, $dir='', $level=1) {
		global $axs, $axs_content;
		//$this->current_dir=(isset($_GET['cd'])) ? $_GET['cd']:$axs['c'];
		$this->current_dir=axs_get('cd', $_GET);
		$this->url['cd']=urlencode($this->current_dir);
		$space=str_repeat(' ', $level-1);
		$html=$space.'<ul>'."\n";
		foreach($index as $cl) {
			if (($cl['plugin']!=='menu') && ($cl['plugin']!=='menu-page')) continue;
			$this->ajax_txt[$level]=$cl['title'];
			$cl['text']=implode('&gt;', array_merge($this->ajax_txt, array($cl['title'])));
			$cl['text']=array();
			foreach ($axs_content['content'][$axs['c']]['path'] as $k=>$v) if ($k<$level) $cl['text'][]=$v['title'];
			$cl['text_close']=implode('&gt;', $cl['text']);
			$cl['text'][]=$cl['title'];
			$cl['text']=implode('&gt;', $cl['text']);
			foreach (array('','_close') as $v) {
				if (!$cl['text'.$v]) $cl['text'.$v]='&nbsp;/&nbsp;';
				$cl['text'.$v.'_url']=rawurlencode(axs_valid::convert($cl['text'.$v], 'html'));
				}
			$cl['url']=$cl['url_close']='?'.axs_url($this->url, array('c'=>$cl['c'], 'd'=>$dir.$cl['c'].'/', 'value'=>$dir.$cl['c'].'/', 'text'=>$cl['text_url'], ));
			$cl['value']=($dir) ? $dir:'/';
			if ($cl['c']==$this->path[$level] && $cl['c']!=$this->current_dir) {
				$this->ajax_txt[$level+1]=$cl['title'];
				$cl['url_close']='?'.axs_url($this->url, array('c'=>$cl['c'], 'd'=>$dir, 'value'=>$cl['value'], 'text'=>$cl['text_close_url'], ));
				$cl['submenu']=$this->ui_ajax_menu($cl['submenu'], $dir.$cl['c'].'/', $level+1);
				$cl['ico']='opn';
				$cl['ico_txt']='[-]';
				}
			else {
				$cl['submenu']='';
				$cl['ico']='cls';
				$cl['ico_txt']='[+]';
				}
			if ($cl['c']==$this->current_dir) {
				$cl['url']='?'.axs_url($this->url, array('c'=>$cl['c'], 'd'=>$dir, 'value'=>$cl['value'], 'text'=>$cl['text_close_url'], ));
				}
			$html.=$space.' <li><a href="'.$cl['url_close'].'" data-value="'.htmlspecialchars($cl['value']).'" data-text="'.htmlspecialchars($cl['text_close_url']).'" class="update browser"><img src="'.AXS_PATH_CMS_HTTP.'gfx/icon.ftype.dir.'.$cl['ico'].'.png" alt="'.$cl['ico_txt'].'" /></a><a href="'.$cl['url'].'" data-value="'.htmlspecialchars($cl['value']).'" data-text="'.htmlspecialchars($cl['text_url']).'" class="update browser">'.$cl['title'].'</a>'.$cl['submenu'].'</li>'."\n";
			}
		$html.=$space.'</ul>'."\n";
		return $html;
		} #</ui_ajax_menu()>
	function ui_menu_build($index, $level, $dir, $url=array(), $tr=array(), $space=' ') {
		global $axs, $axs_content;
		if (!isset($this->url)) $this->url=&$url;
		if (!isset($this->tr)) $this->tr=&$tr;
		$this->tpl=array();
		foreach (array(
			//'menu'=>'      {$space}<ul>'."\n".'{$rows}      {$space}</ul>'."\n",
			'menu_toolbar'=>
				'       {$space}<li class="toolbar{$toolbar_act}"><span class="a"><abbr class="ico" title="'.$this->tr('content-menu_add_new_lbl').'">+</abbr> <a href="{$url_add_page}">{$act_add_page}'.$this->tr('content-menu_page_lbl').'{$/act_add_page}</a> <span></span> <a href="{$url_add_menu}">{$act_add_menu}'.$this->tr('content-menu_submenu_lbl').'{$/act_add_menu}</a></span></li>'."\n",
			'menu_cls'=>
				'       {$space}<li class="{$class}"><span class="a"><a class="ico" href="{$url}"{$atitle}><img src="'.AXS_PATH_CMS_HTTP.'gfx/icon.ftype.page.png" alt="&bull;" /></a><a class="title" href="{$url}"{$atitle}><span class="nr">{$nr}.</span> {$title}</a></span></li>'."\n",
			'menu_act'=>
				'       {$space}<li class="{$class}"><span class="a"><a class="ico" href="{$url}"{$atitle}><img src="'.AXS_PATH_CMS_HTTP.'gfx/icon.ftype.page.png" alt="&bull;" /></a><a class="title" href="{$url}"{$atitle}><span class="nr">{$nr}.</span> <strong>{$title}</strong></a></span></li>'."\n",
			'menu_sub'=>
				'       {$space}<li class="submenu {$class}"><span class="a"><a class="ico" href="{$url}"{$atitle}><img src="'.AXS_PATH_CMS_HTTP.'gfx/icon.ftype.dir.cls.png" alt="+" /></a><a class="title" href="{$url}"{$atitle}><span class="nr">{$nr}.</span> {$title}</a></span></li>'."\n",
			'menu_opn'=>
				'       {$space}<li class="submenu open {$class}"><span class="a"><a class="ico" href="{$url_close}"{$atitle}><img src="'.AXS_PATH_CMS_HTTP.'gfx/icon.ftype.dir.opn.png" alt="-" /></a><a class="title" href="{$url}"{$atitle}><span class="nr">{$nr}.</span> {$title}</a></span>'."\n".
				'        {$space}<ul class="menu current">'."\n".
				'{$submenu}        {$space}</ul>'."\n".
				'       {$space}</li>'."\n",
			'menu-page_cls'=>
				'       {$space}<li class="submenu {$class}"><span class="a"><a class="ico" href="{$url}"{$atitle}><img src="'.AXS_PATH_CMS_HTTP.'gfx/icon.ftype.menu-page.cls.png" alt="+" /></a><a class="title" href="{$url}"{$atitle}><span class="nr">{$nr}.</span> {$title}</a></span></li>'."\n",
			'menu-page_opn'=>
				'       {$space}<li class="submenu open {$class}"><span class="a"><a class="ico" href="{$url_close}"{$atitle}><img src="'.AXS_PATH_CMS_HTTP.'gfx/icon.ftype.menu-page.opn.png" alt="-" /></a><a class="title" href="{$url}"{$atitle}><span class="nr">{$nr}.</span> {$title}</a></span>'."\n".
				'        {$space}<ul>'."\n".
				'{$submenu}        {$space}</ul>'."\n".
				'       {$space}</li>'."\n",
			'rcyc_cls'=>
				'       {$space}<li class="submenu {$class}"><span class="a"><a class="ico" href="{$url}"{$atitle}><img src="'.AXS_PATH_CMS_HTTP.'gfx/icon.recycler.cls.png" alt="+" /></a><a class="title" href="{$url}"{$atitle}><span class="nr">{$nr}.</span> {$title}</a></span></li>'."\n",
			'rcyc_opn'=>
				'       {$space}<li class="opn {$class}"><span class="a"><a class="ico" href="{$url_close}"{$atitle}><img src="'.AXS_PATH_CMS_HTTP.'gfx/icon.recycler.opn.png" alt="-" /></a><a class="title" href="{$url}"{$atitle}><span class="nr">{$nr}.</span> {$title}{$size}</a></span>'."\n".
				'        {$space}<ul>'."\n".
				'{$submenu}        {$space}</ul>'."\n".
				'       {$space}</li>'."\n",
				) as $k=>$v) if (!isset($this->tpl[$k])) $this->tpl[$k]=$v;
		$space=str_repeat($space, $level+(($level>1) ? 1:0));
		$nr=1;
		$html='';
		$deflt_plugin=array();//axs_get('plugin', current($index), '');
		foreach($index as $cl) $deflt_plugin[$cl['plugin']]=$cl['plugin'];
		$deflt_plugin=((count($index)>1) && (count($deflt_plugin)===1)) ? current($deflt_plugin):'';
		if ($deflt_plugin==='menu') $deflt_plugin='';
		foreach($index as $cl) {
			$cl['class']=array();
			$cl['atitle']='';
			if ($cl['publ']>$axs['time'] or ($cl['hidden'] && $cl['hidden']<$axs['time'])) {
				$cl['atitle']=($cl['publ']) ? date('d.m.Y H:i', $cl['publ']):'';
				if ($cl['publ'] && $cl['hidden']) $cl['atitle'].='-';
				if ($cl['hidden']) $cl['atitle'].=date('d.m.Y H:i', $cl['hidden']);
				if (!$cl['publ'] or ($cl['publ'] && $cl['publ']>$axs['time']) or ($cl['hidden'] && $cl['hidden']<$axs['time'])) {
					$cl['class'][]='hidden';
					$cl['atitle']=$this->tr('content-menu_hidden_lbl').': '.$cl['atitle'];
					$cl['title']='<del>'.$cl['title'].'</del>';
					}
				else $cl['atitle']=$this->tr('content-menu_publ_lbl').': '.$cl['atitle'];
				}
			if (!$cl['publ'] && !$cl['hidden'])  $cl['atitle']=$this->tr('content-menu_locked_lbl');
			if ($cl['atitle']) $cl['atitle']=' title="'.$cl['atitle'].'"';
			if (($cl['plugin']==='menu') or ($cl['plugin']==='menu-page')) {
				$cl['url']=$cl['url_close']='?'.axs_url($this->url, array('c'=>$cl['c'], 'd'=>$dir, 's2'=>'m', ));
				if (($cl['c']==axs_get($level, $this->path)) or ($cl['c']==$axs['c'])) {
					$opn=true;
					$tpl=($cl['plugin']==='menu-page') ? $this->tpl['menu-page_opn']:$this->tpl['menu_opn'];
					$cl['url_close']='?'.axs_url($this->url, array('s2'=>'', 'c'=>false, 'd'=>$dir, ));#.'#'.axs_get($level-1, $this->path);
					$cl['submenu']=$this->ui_menu_build($cl['submenu'], $level+1, $dir.$cl['c'].'/');
					}
				else {
					$opn=false;
					$tpl=($cl['plugin']==='menu-page') ? $this->tpl['menu-page_cls']:$this->tpl['menu_sub'];
					}
				if ($cl['c']==='recycler') {
					$tpl=($opn) ? $this->tpl['rcyc_opn']:$this->tpl['rcyc_cls'];
					$cl['size']=($opn) ? '('.count($index[$cl['c']]['submenu']).'/'.$this->site['recycle'].')':'';
					}
				}
			else {
				$cl['url']='?'.axs_url($this->url, array('c'=>$cl['c'], 'd'=>$dir, 's2'=>'', ));
				$tpl=($cl['c']==$axs['c']) ? $this->tpl['menu_act']:$tpl=$this->tpl['menu_cls'];
				}
			if ($cl['c']==$axs['c']) $cl['class'][]='current';
			$cl['class']=implode(' ', $cl['class']);
			$cl['nr']=$nr;
			unset($cl['restrict']);
			//if (($cl['plugin']!=='menu') && ($cl['plugin']!=='menu-page')) $deflt_plugin=$cl['plugin'];
			$html.=$this->ui_menu_build_toolbar($level, $dir, $nr, $deflt_plugin).axs_tpl_parse($tpl, $cl);
			$nr++;
			}
		$html.=$this->ui_menu_build_toolbar($level, $dir, $nr, $deflt_plugin);
		return axs_tpl_parse($html, array('space'=>$space, ));
		} #</ui_menu_build()>
	function ui_menu_build_toolbar($level, $dir, $nr, $plugin) {
		if ($level<$this->menu_depth) return '';
		if ($level==1) { if (!$this->content_status_deletable($dir)) return '';	}
		$tpl=$this->tpl['menu_toolbar'];
		$cl=array();
		$cl['toolbar_act']=$cl['act_add_page']=$cl['/act_add_page']=$cl['act_add_menu']=$cl['/act_add_menu']='';
			if ($nr==axs_get('add', $_GET)) {
				$cl['toolbar_act']=' current';
				if (axs_get('plugin', $_GET)=='menu') {	$cl['act_add_menu']='<strong>';	$cl['/act_add_menu']='</strong>';	}
				else {	$cl['act_add_page']='<strong>';	$cl['/act_add_page']='</strong>';	}
				}
		$cl['url_add_page']=array('c'=>'', 'd'=>$dir, 'add'=>$nr, 's2'=>'m', );
		if (($plugin!=='menu') && ($plugin!=='menu-page')) $cl['url_add_page']['plugin']=$plugin;
		$cl['url_add_page']='?'.axs_url($this->url, $cl['url_add_page']);
		$cl['url_add_menu']='?'.axs_url($this->url, array('c'=>'', 'd'=>$dir, 'add'=>$nr, 's2'=>'m', 'plugin'=>'menu'));
		return axs_tpl_parse($tpl, $cl);
		} #</ui_menu_build_toolbar()>
	# ------------ </UI functions> -------------------
	} #</class::axs_content_edit>
#2011-05-30 ?>