<?php #2021-02-22
class axs_users_edit {
	static $table='users';
	var $structure=array(
		'id_'=>array(
			'id_'=>array('type'=>'fieldset', ),
			'id'=>array('type'=>'text', 'size'=>10, 'maxlength'=>10, 'required'=>0, 'readonly'=>'readonly', ),
			'lock'=>array('type'=>'checkbox', 'size'=>1, 'maxlength'=>1, 'required'=>0, 'value'=>1, ),
			'id_/'=>array('type'=>'fieldset_end', ),
			),
		'data'=>array(
			'data'=>array('type'=>'fieldset', ),
			'user'=>array('type'=>'text', 'size'=>20, 'maxlength'=>1, 'required'=>1, ),
			'pass'=>array('type'=>'password', 'size'=>20, 'maxlength'=>20, 'required'=>1, 'min'=>4, ),
			'fstname'=>array('type'=>'text', 'size'=>20, 'maxlength'=>20, 'required'=>1, ),
			'lstname'=>array('type'=>'text', 'size'=>20, 'maxlength'=>20, 'required'=>0, ),
			'email'=>array('type'=>'email', 'size'=>20, 'maxlength'=>99, 'required'=>0, 'multiple'=>true, ),
			'phone'=>array('type'=>'tel', 'size'=>20, 'maxlength'=>99, 'required'=>0, ),
			'data/'=>array('type'=>'fieldset_end', ),
			),
		'g'=>array(
			'g'=>array('type'=>'multi-checkbox', 'options'=>array(), ),
			),
		'homedirs'=>array(
			'homedirs'=>array('type'=>'table', 'options'=>array(
				'id'=>array('type'=>'number', 'size'=>2, 'readonly'=>'readonly'),
				'site'=>array('type'=>'select', 'options'=>array(), ),
				'dir'=>array('type'=>'text-join', 'size'=>70, 'size2'=>70, 'maxlength'=>255, ),
				'_add'=>array('type'=>'submit', 'label'=>'+', ),
				'_del'=>array('type'=>'checkbox', 'value'=>1, ),
				)),
			),
		'login'=>array(
			'online'=>array('type'=>'hidden', 'size'=>10, ),
			),
		'save'=>array(
			'save'=>array('type'=>'submit', 'add_fn'=>false, ),
			),
		);
	var $msg=array();
	//var $msg_error=array();
	function __construct($db=0, $tr=false, $usr_format='', $fieldset=array(), $fields=array(), $psize=50) {
		global $axs, $axs_user;
		$this->db=(($db) && ($axs['cfg']['db'][$axs['cfg']['users_db']]['type'])) ? 1:0;
		$this->px=$axs['cfg']['db'][$axs['cfg']['users_db']]['px'];
		$this->cfg['usr_format']=$usr_format;
		$this->cfg['pwd_len']=2;
		$this->tr=new axs_tr($tr);
		//$this->tr->set('msg_value_required', axs_get('msg_value_required', $this->tr->tr, '$user_msg_value_required'));
		$this->tr->set($this->tr->filter('user.'));
		$this->psize=$psize;
		$this->structure['data']['user']['maxlength']=axs_user::$user_length;
		$this->fields=array(''=>array('tr'=>$this->tr));
		foreach ($this->structure as $k=>$v) foreach ($v as $kk=>$vv) {
			$vv['fieldset']=$k;
			$vv['label']=($this->tr->has($kk.'.lbl')) ? $this->tr->t($kk.'.lbl'):axs_get($kk.'.lbl', $this->tr->tr, $kk.'.lbl');
			$this->structure[$k][$kk]=$vv;
			}
		if (!empty($fieldset)) {
			foreach ($fieldset as $k=>$v) $this->fields+=$this->structure[$k];
			$this->fields+=$this->structure['save'];
			}
		else foreach ($this->structure as $k=>$v) foreach ($v as $kk=>$vv) $this->fields[$kk]=$vv;
		$this->url=axs_get('url', $axs, array());
		$this->user=&$axs_user;
		$this->form=new axs_form_edit(1, $this->url, $this->fields, '', $_POST, 'user-edit');
		$this->form->url=&$this->url;
		} # </axs_users_edit()>
	function _esc($str) {	return str_replace(array('\\','\''),  array('\\\\','\\\''), $str);	}
	function _customize($plugin_def=array()) {
		foreach ((array)$plugin_def['config'] as $k=>$v) $this->cfg[$k]=$v;
		}
	static function _table() {
		global $axs;
		return $axs['cfg']['db'][$axs['cfg']['users_db']]['px'].self::$table;
		}
	function editor($user, $id, $values, $back_url='', $css_js=true) {
		global $axs;
		if ($css_js) $this->form->css_js();
		#<Values>
		foreach (array('pass','pass_changed') as $v) if (isset($_POST[$v])) $_POST[$v]='*';
		$this->permission_level();
		if (!$this->form->submit) {
			$values=$this->user_get($user, $id, $values);
			$values['pass']=$values['pass_changed']=(strlen($values['pass'])) ? '*':'';
			if (isset($this->form->structure['g'])) foreach ($axs['cfg']['groups'] as $k=>$v) {
				$values['g_'.$k]=axs_get($k, $values['g']);
				}
			}
		# <Customize values>
		if (!$this->privilege_add) $this->form->structure['user']['readonly']='readonly';
		unset($this->form->structure['login']);
		if (empty($values['homedirs'])) $values['homedirs']=array();
		# </Customize values>
		#</Values>
		# <Customize elements>
		if (isset($this->form->structure['g'])) { # <Group membership>
			foreach ($axs['cfg']['groups'] as $k=>$v) {
				$v=array('value'=>1, 'label'=>$k.' ('.$v.')');
				if (!isset($values['g'])) $values['g']=array();
				if (isset($values['g_'.$k])) $values['g'][$k]=$values['g_'.$k];
				$values['g_'.$k]=axs_get($k, $values['g']);
				# <If current user not admin and with no permission, disable this group's checkbox>
				if (!$this->privilege_admin) {
					if (!axs_user::permission_get(array($k=>'', ))) $v['readonly']='readonly';
					} # </If current user not admin>
				$this->form->structure['g']['options'][$k]=$v;
				}
			} # </Group membersip>
		#<Sites and home directorys>
		if (isset($this->form->structure['homedirs'])) {
			foreach ($this->form->structure['homedirs']['options'] as $k=>$v) $this->form->structure['homedirs']['options'][$k]['label']=$this->tr->s('homedirs.'.$k.'.lbl');//($this->tr->has('m.homedirs_'.$k.'_lbl')) ? $this->tr->s('user.homedirs_'.$k.'_lbl'):'';
			foreach ($axs['cfg']['site'] as $k=>$v) {
				$v=array('value'=>$k, 'label'=>$v['title'], );
				if (!$this->permission_homedir($k)) $v['disabled']='disabled';
				$options[$k]=$v;
				}
			$this->form->structure['homedirs']['options']['site']['options']=array(''=>array('value'=>'', 'label'=>''))+$options;
			if (is_array($values['homedirs'])) foreach ($values['homedirs'] as $k=>$v) {
				$values['homedirs'][$k]['id']=$k;
				}
			$this->form->structure['homedirs']['txt']="\n".'      <script src="'.axs_dir('lib', 'http').'axs.users.edit.js"></script>';
			} #</Sites and home directorys>
		if ($user!=axs_user::get('user')) $this->form->structure['save']['add_fn']=true;
		$permission_write=$this->permission_write(axs_get('g', $values, array()));
		if (!$permission_write) $this->form->structure['']['save']['disabled']='disabled'; # <Disable changeing users from another group if not admin />
		$this->form->structure_set($this->form->structure);
		# </Customize elements>
		$values=$this->form->form_input($values);
		
		# <Save>
		if ($this->form->save) {
			if (isset($this->form->structure['g'])) foreach ($axs['cfg']['groups'] as $k=>$v) {
				$values['g'][$k]=axs_get('g_'.$k, $values);
				unset($values['g_'.$k]);
				}
			$tmp=$this->write($user, $id, $values);
			if (empty($this->form->msg)) {
				//$values=$tmp;
				$user=$values['user'];
				}
			if (empty($this->form->msg)) {	if ($back_url) exit(axs_redir($back_url));	}
			} # </Save>
		# <If delete>
		if ($this->form->save_delete) {
			if ($permission_write) $this->delete($user, $id);
			else $this->form->msg('', $this->tr->s('msg.permission'));
			} # </If delete>
		# <Build form />
		$el='';
		foreach ($this->form->structure_get() as $k=>$v) $el.=$this->form->element_html($k, $v, $values);
		$formact_url='?'.axs_url($this->url, array('axs'=>array($this->form->key_form_id=>$this->form->form_id))).'#user-edit';
		$back=($back_url) ? '      <p class="back"><a href="'.htmlspecialchars($back_url).'">&lt;'.$this->tr->s('back.lbl').'</a></p>'."\n":'';
		$msg=$this->form->msg_html();
		return '     <form id="user-edit" action="'.$formact_url.'" method="post" class="form">'."\n".
		$back.$msg.
		$el.'     '.
		'</form>'."\n".
		'     <script>axs.form.init("user-edit");</script>';
		} # </editor()>
	function list_get($p, $search=false) {
		global $axs;
		$this->p=$p;
		$list=array();
		if ($this->db) {
			if (strlen($search->vl['status'])) {
				if ($search->vl['status']==1) $search->sql['status']="u.`online`>('".$axs['time']."'-'".$axs['cfg']['login_update']."')";
				if ($search->vl['status']==0) $search->sql['status']="u.`online`<('".$axs['time']."'-'".$axs['cfg']['login_update']."')";
				}
			$p=new axs_pager($this->p, array($this->psize), $this->psize);
			foreach (axs_db_query(
			"SELECT SQL_CALC_FOUND_ROWS u.*, GROUP_CONCAT(CONCAT(h.`id`,':',h.`site`,':',h.`dir`) SEPARATOR ',') AS `homedirs`\n".
			"	FROM `".self::_table()."` AS u LEFT JOIN `".self::_table()."_home` AS h ON h.`uid`=u.`id`\n".
			(($search) ? $search->sql_where("	WHERE ")."\n":'').
			"	GROUP BY u.`id` ORDER BY `user` ASC LIMIT ".$p->start.','.$p->psize,
			1, $this->db, __FILE__, __LINE__) as $cl) {
				foreach (array('user','pass') as $v) $cl[$v]=trim($cl[$v]);
				$cl['g']=array();
				foreach ($axs['cfg']['groups'] as $kk=>$vv) {
					if ($cl['g_'.$kk]) $cl['g'][$kk]=$kk;
					unset($cl['g_'.$kk]);
					}
				$tmp=preg_split('/,/', $cl['homedirs'], -1, PREG_SPLIT_NO_EMPTY);
				$cl['homedirs']=array();
				foreach ($tmp as $k=>$v) {
					$v=explode(':', $v);
					$cl['homedirs'][$v[0]+0]=array('site'=>$v[1]+0, 'dir'=>$v[2]);
					}
				$list[$cl['id']]=$cl;
				}
			# Generate pages links
			$total=axs_db_query('', 'found_rows', 1, __FILE__, __LINE__);
			}
		else {
			$total=0;
			foreach (axs_admin::f_list(AXS_PATH_CMS.'data/', '/^user\..+\.php$/') as $cl) {
				if (substr($cl, -5)==='..php') continue;
				$cl=include(AXS_PATH_CMS.'data/'.$cl);
				axs_user::tmp_get($cl);
				if ($search) {
					if (strlen($tmp=axs_get('lock', $search->vl))) {	if (intval($cl['lock'])!=$tmp) continue;	}
					if ($search->validate_element_filled('g', 'g', $search->vl)) {
						$tmp=false;
						foreach ($axs['cfg']['groups'] as $k=>$v) if ((!empty($search->vl['g_'.$k])) && (!empty($cl['g'][$k]))) $tmp=true;
						if (!$tmp) continue;
						}
					}
				$list[$cl['user']]=$cl;
				$total++;
				}
			ksort($list);
			}
		foreach ($list as $user=>$cl) {
			$cl=$this->user_format($cl);
			$cl['pass']=(strlen($cl['pass'])) ? '*':'';
			$cl['g']=implode(', ', array_keys($cl['g']));
			foreach ($cl['homedirs'] as $k=>$v) $cl['homedirs'][$k]=$v['site'].':'.$axs['cfg']['site'][$v['site']]['title'].'/'.$v['dir'];
			$cl['homedirs']=implode(', '."\n", $cl['homedirs']);
			$list[$user]=$cl;
			}
		return array('list'=>$list, 'total'=>$total, );
		} # </list_get()>
	function permission_level() {
		if (!isset($this->privilege_admin)) $this->privilege_admin=axs_user::permission_get(array('admin'=>'', ));
		if (!isset($this->privilege_add)) $this->privilege_add=axs_user::permission_get(array('admin'=>'', 'cms'=>'', ));
		} # </permission_level()>
	function permission_homedir($site_nr) {
		global $axs;
		$this->permission_level();
		if ($this->privilege_admin) return true;
		foreach ($this->user['homedirs'] as $v) if ($v['site']==$site_nr) return true;
		return false;
		} # </permission_homedir()>
	function permission_write($groups) {
		$this->permission_level();
		if ($this->privilege_admin) return true;
		$permission=false;
		foreach ((array)$groups as $k=>$v) {
			if ($v) {	if (axs_user::permission_get(array($k=>''))) {	$permission=true;	break;	}	}
			}
		return $permission;
		} # </permission_write()>
	static function user_format($data=array()) {
		global $axs;
		foreach (axs_user::$fields as $k=>$v) if (!isset($data[$k])) $data[$k]=(is_array($v)) ? $v:'';
		foreach ($axs['cfg']['groups'] as $k=>$v) if (empty($data['g'][$k])) unset($data['g'][$k]);
		foreach ($data['g'] as $k=>$v) if (!isset($axs['cfg']['groups'][$k])) unset($data['g'][$k]);
		$data['status']=self::user_online($data);
		$data['online']=($data['online']) ? date('d.m.Y H:i:s', $data['online']):'';
		$data['lock']=intval($data['lock']);
		return $data;
		} # </user_format()>
	static function user_get($user='', $id='', $cl=array()) {
		global $axs;
		$user=axs_valid::f_secure($user);
		if (file_exists($tmp=AXS_PATH_CMS.'data/user.'.$user.'.php')) $cl=include($tmp);
		else {
			if (($axs['cfg']['db'][$axs['cfg']['users_db']]['db']) && ($axs['cfg']['db'][$axs['cfg']['users_db']]['type'])) {
				$where=($id) ? "u.`id`='".($id+0)."'":"u.`user`='".addslashes($user)."'";
				$cl=axs_db_query("SELECT u.*, GROUP_CONCAT(CONCAT(h.`id`,':',h.`site`,':',h.`dir`) SEPARATOR ',') AS `homedirs`\n".
				"	FROM `".self::_table()."` AS u LEFT JOIN `".self::_table()."_home` AS h ON h.`uid`=u.`id`\n".
				"	WHERE ".$where." GROUP BY u.`id` LIMIT 1", 'row', $axs['cfg']['users_db'], __FILE__, __LINE__);
				foreach (array('user','pass') as $v) $cl[$v]=trim(axs_get($v, $cl));
				$cl['g']=array();
				foreach ($axs['cfg']['groups'] as $k=>$v) {
					if (!empty($cl['g_'.$k])) $cl['g'][$k]=true;
					unset($cl['g_'.$k]);
					}
				$tmp=preg_split('/,/', axs_get('homedirs', $cl), -1, PREG_SPLIT_NO_EMPTY);
				$cl['homedirs']=array();
				foreach ($tmp as $k=>$v) {
					$v=explode(':', $v);
					$cl['homedirs'][$v[0]+0]=array('site'=>$v[1]+0, 'dir'=>$v[2]);
					}
				}
			}
		axs_user::tmp_get($cl);
		return self::user_format($cl);
		} # </user_get()>
	static function user_online($data) {
		global $axs;
		$online='-';
		if (strlen($data['online'])) { #<The time when user session is old enaugh to be considered offline />
			$online=($data['online']<$axs['time']-$axs['cfg']['login_update']) ? 0:1;
			}
		return $online;
		} # </user_online()>
	function validate($user, $values) { # <Validate user data />
		global $axs;
		$this->permission_level();
		//if (!$this->privilege_add) $values['user']=$user;
		foreach ($this->structure as $k=>$v) foreach ($v as $kk=>$vv) if (!empty($vv['required'])) {
			if (!$values[$kk]) {
				$vv=$this->tr->s('msg_value_required').' "'.$vv['label'].'"!';
				if ($this->tr->has('msg_'.$kk)) $vv=$this->tr->s('msg_'.$kk);
				$this->form->msg($kk, $vv);
				}
			}
		if (!$values['user']) $this->form->msg('user', true);
		else {
			if (!axs_valid::user($values['user'], $this->cfg['usr_format'])) $this->form->msg('msg.user_invalid', $this->tr->s('msg.user_invalid'));
			else {
				if ($user!=$values['user']) {
					$tmp=$this->user_get($values['user']);
					if ($tmp['user']) $this->form->msg('msg.user_exists', $this->tr->s('msg.user_exists'));
					if ($values['user']=='adm') {	if (!$this->privilege_admin) $this->form->msg('msg.user_exists', $this->tr->s('msg.user_exists'));	}
					}
				}
			}
		if ((strlen($values['pass'])) && (isset($values['pass_changed'])) && ($values['pass']!==$values['pass_changed'])) {
			if (strlen($values['pass'])<$this->cfg['pwd_len']) $this->form->msg('msg.pass_short', $this->tr->s('msg.pass_short'));
			else {
				if ($values['pass']!=$values['pass_changed']) {	if ($values['pass']!=axs_valid::id($values['pass'])) $this->form->msg('msg.pass_invalid', $this->tr->s('msg.pass_invalid'));	}
				}
			}
		if ($values['email']) {	if (!axs_valid::emails($values['email'])) $this->form->msg('msg.emails', $this->tr->s('msg.email'));	}
		if (is_array($values['homedirs'])) {
			$sites=&$axs['cfg']['site'];
			foreach ($values['homedirs'] as $k=>$v) {
				if ($k=='add') {	if (!$v['site'] && !$v['dir']) continue;	}
				foreach ($v as $kk=>$vv) $v[$kk]=htmlspecialchars($vv);
				if (!isset($sites[$v['site']])) $this->form->msg('msg.homedirs', $this->tr->s('msg.homedirs').' "'.$v['site'].'"!');
				else {
					if (!is_dir($tmp=$sites[$v['site']]['dir_fs_root'].$v['dir'])) $this->form->msg('msg.homedirs_dir', $this->tr->s('msg.homedirs_dir').' '.$v['site'].': "'.$tmp.'"!');
					}
				}
			}
		return $this->form->msg;
		} # </validate()>
	static function validate_user(&$form, $user='', $privilege_admin=false, $cfg=array()) { # <Validate user data />
		global $axs;
		//exit(dbg($user,$form->vl['user']));
		foreach (array(
			'user_format'=>$form->structure['user']['type']==='email' ? '@':'auto', 'user_length'=>axs_user::$user_length,
			) as $k=>$v) if (!isset($cfg[$k])) $cfg[$k]=$v;
		if (!strlen($form->vl['user'])) $form->msg('user', true);
		else {
			if (!axs_valid::user($form->vl['user'], $cfg['user_format'])) $form->msg('msg.user_invalid', $form->tr->t('msg.user_invalid'));
			else {
				if ($user!=$form->vl['user']) {
					$tmp=self::user_get($form->vl['user']);
					if (!empty($tmp['user'])) $form->msg('msg.user_exists', $form->tr->t('msg.user_exists'));
					if ($form->vl['user']==='adm') {	if (!$privilege_admin) $form->msg('msg.user_exists', $form->tr->t('msg.user_exists'));	}
					}
				}
			}
		
		return $form->msg;
		} # </validate_user()>
	static function register_user(&$form, $uid, $redir=false) {
		global $axs;
		$form->validate(); # <Error check />
		if (empty($form->msg)) self::validate_user($form, ($uid) ? axs_user::get('user'):'');
		if (!empty($form->msg)) return false;
		$q=($uid) ? 
			array('UPDATE "'.self::_table().'" SET '.$form->sql_values_set($form->vl).' WHERE "id"=\''.intval($uid).'\'', $form->sql_p):
			array('INSERT INTO "'.self::_table().'" SET '.$form->sql_values_set($form->vl), $form->sql_p);
		axs_db_query($q, '', $axs['cfg']['users_db'], __FILE__, __LINE__);
		if ($uid) foreach (array('user'=>'', 'fstname'=>'', 'lstname'=>'', ) as $k=>$v) axs_user::set($k, $form->vl[$k]); #<If change current user />
		if ($redir) axs_exit(axs_redir($redir));
		} #</register_user()>
	static function register_user_structure($url, $tr) {
		global $axs;
		$form=new axs_form_edit(array('form_id'=>$url['c'].'-form', 'site_nr'=>AXS_SITE_NR, 'url'=>$url, 'user_input'=>$_POST, 'tr'=>$tr));
		$form->sql_header_get('users', 'users');
		foreach (array('user'=>'', 'pass'=>'', 'pass_confirm'=>'', ) as $k=>$v) {	if (isset($form->structure[$k])) $form->structure[$k]['required']=1;	}$form->vl=$form->form_input();
		return $form;
		} #</register_user()>
	function write($user, $id, $change) {
		global $axs, $axs_user;
		$id=intval($id);
		$this->permission_level();
		$fields=array();
		foreach ($this->structure as $k=>$v) foreach ($v as $kk=>$vv) $fields[$kk]=$vv;
		# <Change values>
		$values=$this->user_get($user, $id);
		//$values['pass_changed']=$values['pass'];
		if ((!$change['pass']) or ($change['pass']==='*')) unset($fields['pass'], $change['pass'], $change['pass_changed']); # <Update pass only if changed />
		$change['email']=preg_replace('/ +/', ' ', $change['email']);
		foreach ($change as $k=>$v) {	if (!is_array($v)) $values[$k]=$v;	}
		if (!empty($change['g'])) foreach ($change['g'] as $k=>$v) {
			if (!isset($axs['cfg']['groups'][$k])) continue;
			if (!$this->privilege_admin) {	if (!axs_user::permission_get($k)) continue;	}
			$values['g'][$k]=($v) ? 1:0;
			}
		foreach ($values['g'] as $k=>$v) if (!isset($axs['cfg']['groups'][$k])) unset($values['g'][$k]);
		if (!empty($change['homedirs'])) foreach ($change['homedirs'] as $k=>$v) {
			if (!$this->permission_homedir($v['site'])) continue;
			if (!isset($axs['cfg']['site'][$v['site']])) continue;
			if (!isset($v['_del'])) $v['_del']=false;
			if ($v['_del']) {
				unset($values['homedirs'][$k]);
				$change['homedirs']['del'][$k]=$k;
				continue;
				}
			$v=array('site'=>$v['site']+0, 'dir'=>($v['dir']) ? rtrim($v['dir'], '/').'/':'', '_del'=>$v['_del']);
			$values['homedirs'][$k]=$v;
			}
		//if (!$this->privilege_add) $values['user']=$user; # <Don't allow adding or renaming user if no permission>
		# </Change values>
		# <Error check>
		$this->validate($user, $values);
		if (!empty($this->form->msg)) return $values;
		# </Error check>
		# <If change current user>
		if (strlen(axs_get('pass', $change, ''))) $change['pass']=$values['pass']=password_hash($change['pass'], PASSWORD_DEFAULT);
		if (AXS_LOGINCHK===1) {
			$tmp=axs_user::get();
			if ($user==$tmp['user']) foreach ($tmp as $k=>$v) if (isset($values[$k])) axs_user::set($k, $values[$k]);
			} # </If change current user>
		if ($this->db) { # <Save to SQL>
			# <If add new user />
			if (!$id) $id=$values['id']=axs_db_query("INSERT INTO `".self::_table()."` SET `user`='".addslashes($values['user'])."'", 'insert_id', $this->db, __FILE__, __LINE__);
			# <Update user />
			$tmp=$fields;
			unset($tmp['id'], $tmp['g'], $tmp['homedirs']);
			$qry=array();
			foreach ($tmp as $k=>$v) if ((!empty($v['size'])) && ($v['type']!='hidden') && (empty($v['disabled'])) && (empty($v['readonly']))) $qry[$k]='`'.$k.'`=\''.addslashes($values[$k]).'\'';
			foreach ($axs['cfg']['groups'] as $k=>$v) if (isset($values['g'][$k])) $qry['g_'.$k]='`g_'.$k.'`='.intval($values['g'][$k]);
			$where=($id) ? '`id`=\''.($id+0).'\'':'`user`=\''.addslashes($user).'\'';
			axs_db_query("UPDATE `".self::_table()."` SET ".implode(', ', $qry).", `updated`='".$axs['time']."', `updated_uid`='".$this->user['id']."' WHERE ".$where, '', $this->db, __FILE__, __LINE__);
			#<Edit homedirs />
			//$add=false;
			foreach ($values['homedirs'] as $k=>$v) {
				if ($k<1) { #<Add homedir />
					$k=axs_db_query("INSERT INTO `".self::_table()."_home` SET `uid`='".$id."'", 'insert_id', $this->db, __FILE__, __LINE__);
					$values['homedirs'][$k]=$v;
					//$add=$k;
					}
				#<Update homedir />
				axs_db_query("UPDATE `".self::_table()."_home` SET `site`='".$v['site']."', `dir`='".addslashes($v['dir'])."'\n".
				"	WHERE `id`='".($k+0)."' AND `uid`='".$id."'", '', $this->db, __FILE__, __LINE__);
				}
			if (isset($change['homedirs']['del'])) foreach ($change['homedirs']['del'] as $k=>$v) { #<Del homedirs />
				axs_db_query("DELETE FROM `".self::_table()."_home` WHERE `id`='".($k+0)."' AND `uid`='".$id."'", '', $this->db, __FILE__, __LINE__);
				}
			/*if ($add) { #<Set "add" to the end />
				unset($values['homedirs']['add']);
				$values['homedirs']['add']=array('site'=>'', 'dir'=>'', );
				}*/
			} # </Save to SQL>
		else { # <Save to filesystem>
			$fs=new axs_filesystem(AXS_PATH_CMS.'data/');
			# <If rename user>
			if (($user) && ($values['user']!==$user)) foreach (array(''=>'', 'tmp'=>'.', ) as $k=>$v) {
				if (file_exists(AXS_PATH_CMS.'data/user.'.$user.$v.$k.'.php')) {
					if ($tmp=$fs->rename('user.'.$user.$v.$k.'.php', 'user.'.$values['user'].$v.$k.'.php')) $this->form->msg('', $tmp);
					if (!empty($this->form->msg)) return $values;
					}
				} # </If rename user>
			# <Remove empty groups />
			if (!empty($values['g'])) foreach ($values['g'] as $k=>$v) if (!$v) unset($values['g'][$k]);
			# <Build file contents>
			$g=$homedirs='';
			if (!empty($values['g'])) foreach ($values['g'] as $k=>$v) $g.='\''.self::_esc($k).'\'=>true, ';
			if (!empty($values['homedirs'])) {
				foreach ($values['homedirs'] as $k=>$v) {
					if (isset($change['homedirs']['del'][$k])) {	unset($values['homedirs'][$k]);	continue;	}
					if ($k<1) {
						$values['homedirs'][]=$v;
						unset($values['homedirs'][$k]);
						}
					}
				$values['homedirs']=array_merge(array(0=>0), $values['homedirs']);
				unset($values['homedirs'][0]);
				foreach ($values['homedirs'] as $k=>$v) $homedirs.='		'.intval($k).'=>array(\'site\'=>'.intval($v['site']).', \'dir\'=>\''.self::_esc($v['dir']).'\', ),'."\n";
				}
			$data=
			'<?php # AXIS CMS user account file (modified by '.self::_esc($this->user['user']).' @ '.date('d.m.Y H:i:s').')'."\n".
			'return array('."\n".
			'	\'user\'=>\''.self::_esc($values['user']).'\', # username'."\n".
			'	\'pass\'=>\''.self::_esc($values['pass']).'\', # password'."\n".
			'	\'lock\'=>'.intval($values['lock']).', # account disabled'."\n".
			'	\'fstname\'=>\''.self::_esc($values['fstname']).'\', # user\'s first name'."\n".
			'	\'lstname\'=>\''.self::_esc($values['lstname']).'\', # user\'s last name'."\n".
			'	\'email\'=>\''.self::_esc($values['email']).'\', # e-mail address(es)'."\n".
			'	\'phone\'=>\''.self::_esc($values['phone']).'\', # user phone(es)'."\n".
			'	\'g\'=>array('.$g.'), # group membership'."\n".
			'	\'homedirs\'=>array( # home directorys'."\n".
			$homedirs.'		),'."\n".
			'	);'."\n".
			'?>';
			# </Build file contents>
			# <Write file />
			if ($tmp=$fs->f_save('user.'.strtolower($values['user']).'.php', $data)) $this->form->msg('', $tmp);
			} # </Save to filesystem>
		return $values;
		} # </write()>
	function delete($user, $id='') {
		global $axs;
		if (AXS_LOGINCHK==1) {
			if ($user==axs_user::get('user')) {
				$this->form->msg('', $this->tr->s('msg.del_error'));
				return false;
				}
			}
		$user=axs_valid::f_secure($user);
		$fs=new axs_filesystem(AXS_PATH_CMS.'data/');
		foreach (array($user=>'', $user.'.tmp', $id=>'', $id.'.tmp'=>'', ) as $k=>$v) if ($fs->exists($f='user.'.$k.'.php')) {
			if ($tmp=$fs->f_del($f, false)) $this->form->msg('', $f);
			if ($fs->exists($f)) {
				$this->form->msg('', $this->tr->s('msg.del_error'));
				return false;
				}
			}
		if ($axs['cfg']['db'][1]['db']) {
			$where=($id) ? "`id`='".($id+0)."'":"`user`='".addslashes($user)."'";
			axs_db_query("DELETE u, h\n".
			"	FROM `".self::_table()."` AS u LEFT JOIN `".self::_table()."_home` AS h ON h.`uid`=u.`id`\n".
			"	WHERE u.".$where, '', 1, __FILE__, __LINE__);
			}
		$this->form->msg('', $this->tr->s('user.msg.deleted'));
		return true;
		} # </delete()>
	} #</class::axs_users_edit>
#2009-10-02 ?>