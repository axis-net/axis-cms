<?php #2020-04-30
class axs_content_guests {
	static $name='content_guests';
	static $tr=array(
		'en'=>array(
			'guest.text.html'=>'Page text',
			'guest.email.lbl'=>'Please leave Your e-mail address to enter the page',
			'guest.enter.lbl'=>'Enter',
			'guest.msg.email'=>'Invalid e-mail!',
			),
		'et'=>array(
			'guest.text.html'=>'Tekst',
			'guest.email.lbl'=>'Sisenemeiseks palun sisestaga oma e-posti aadress',
			'guest.enter.lbl'=>'Edasi',
			'guest.msg.email'=>'E-posti aadress ei ole korrektne!',
			),
		);
	function login_form($tr) {
		global $axs;
		if (!isset($_SESSION)) session_start();
		if (isset($_GET['guest_leave'])) unset($_SESSION['guest_email']);
		if (!empty($_SESSION['guest_email'])) return;
		$msg='';
		$email=axs_get('guest_email', $_POST);
		if (!empty($email)) {
			if (strlen(axs_get('_captcha', $_POST))) $msg.='			<p class="msg" lang="en">Invalid input!</p>';
			if (!axs_valid::email($email)) $msg.='			<p class="msg">'.$tr->s('guest.msg.email').'</p>';
			if (empty($msg)) {
				setcookie('guest_email', $email, $axs['time']+86400*30, $axs['http_root']);
				axs_db_query($q="INSERT INTO `".$axs['cfg']['site'][AXS_SITE_NR]['prefix'].self::$name."` SET `email`='".addslashes($email)."'", '', $axs['cfg']['site'][AXS_SITE_NR]['db'], __FILE__, __LINE__);
				$_SESSION['guest_email']=$email;
				exit(axs_redir());
				}
			}
		if (!$email) $email=axs_get('guest_email', $_COOKIE);
		return 
		'		<form class="login guest" action="'.htmlspecialchars($_SERVER['REQUEST_URI']).'" method="post">'."\n".
		'			<div class="element content">'."\n".
		$tr->s('guest.text.html')."\n".
		'			</div>'."\n".
		$msg.
		'			<div class="element text" style="position:absolute;left:-9999em;top:-9999em;">'."\n".
		'				<label for="_captha" lang="en">Leave empty</label>'."\n".
		'				<input id="_captcha" name="_captcha" type="text" size="5" value="" />'."\n".
		'			</div>'."\n".
		'			<div class="element text">'."\n".
		'				<label>'.$tr->s('guest.email.lbl').'</label>'."\n".
		'				<input id="guest_email" name="guest_email" type="email" size="25" value="'.htmlspecialchars($email).'" />'."\n".
		'			</div>'."\n".
		'			<input name="s" type="submit" value="'.$tr->s('guest.enter.lbl').'" />'."\n".
		'		</form>';
		} #</login_form()>
	} #</class::axs_content_guests>
#2020-04-25 ?>