/*16.02.2012*/
axs.calendar={
	inst:{},
	init:function(cal_id){
		var el=document.getElementById(cal_id+'_today');
		if (!el) return false;
		this.inst[cal_id]={};
		this.inst[cal_id].id=cal_id;
		this.inst[cal_id].date=this.inst[cal_id].time={};
		el=el.getElementsByTagName('span');
		for (var i=0; i<el.length; i++) {
			if (el[i].className=='date') this.inst[cal_id].date=el[i];
			if (el[i].className=='time') this.inst[cal_id].time=el[i];
			}
		this.inst[cal_id].today=new Date();
		var tmp=this.inst[cal_id].date.innerHTML.split('.');
		this.inst[cal_id].today.setFullYear(tmp[2],tmp[1],tmp[0]);
		var tmp=this.inst[cal_id].time.innerHTML.split(':');
		this.inst[cal_id].today.setHours(parseInt(tmp[0]));
		this.inst[cal_id].today.setMinutes(parseInt(tmp[1]));
		this.inst[cal_id].today.setSeconds(parseInt(tmp[2]));
		this.startTime(cal_id);
		},//</init()>
	startTime:function(cal_id){
		this.inst[cal_id].today.setSeconds(this.inst[cal_id].today.getSeconds()+1);
		var d=this.inst[cal_id].today.getDate();
		var m=this.inst[cal_id].today.getMonth()+1;
		var Y=this.inst[cal_id].today.getFullYear();
		var H=this.inst[cal_id].today.getHours();
		var i=this.inst[cal_id].today.getMinutes();
		var s=this.inst[cal_id].today.getSeconds();
		if (H==0) this.inst[cal_id].date.innerHTML=this.checkTime(d)+'.'+this.checkTime(m)+'.'+this.checkTime(Y);
		this.inst[cal_id].time.innerHTML=this.checkTime(H)+':'+this.checkTime(i)+':'+this.checkTime(s);
		var t=setTimeout('axs.calendar.startTime("'+cal_id+'");',1000);
		},//</startTime()>
	checkTime:function(i){
		if (i<10) i="0"+i;
		return i;
		}//</checkTime()>
	}//</class::axs_calendar>
/*21.02.2010*/