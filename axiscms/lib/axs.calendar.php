<?php #2016-04-24
class axs_calendar {
	var $name='calendar';
	var $sql_result=null;
	static $tr=array(
		'en'=>array(
			'day1'=>'Monday',
			'day1_abbr'=>'Mon',
			'day2'=>'Tuesday',
			'day2_abbr'=>'Tue',
			'day3'=>'Wednesday',
			'day3_abbr'=>'Wed',
			'day4'=>'Thursday',
			'day4_abbr'=>'Thu',
			'day5'=>'Friday',
			'day5_abbr'=>'Fri',
			'day6'=>'Saturday',
			'day6_abbr'=>'Sat',
			'day0'=>'Sunday',
			'day0_abbr'=>'Sun',
			
			'mon1'=>'January',
			'mon2'=>'February',
			'mon3'=>'March',
			'mon4'=>'April',
			'mon5'=>'May',
			'mon6'=>'June',
			'mon7'=>'July',
			'mon8'=>'August',
			'mon9'=>'September',
			'mon10'=>'October',
			'mon11'=>'November',
			'mon12'=>'December',
			
			'month_select_lbl'=>'month',
			'year_select_lbl'=>'year',
			'prev_month_lbl'=>'previous month',
			'next_month_lbl'=>'next month',
			
			'calendar_lbl'=>'calendar',
			'today_is_lbl'=>'Today is',
			'show_month_lbl'=>'go',
			'week_nr_lbl'=>'Week noumber',
			'week_nr_abbr_lbl'=>'W',
			'legend_lbl'=>'Due dates',
			
			'back_lbl'=>'Tagasi',
			),
		'et'=>array(
			'day1'=>'Esmaspäev',
			'day1_abbr'=>'E',
			'day2'=>'Teisipäev',
			'day2_abbr'=>'T',
			'day3'=>'Kolmapäev',
			'day3_abbr'=>'K',
			'day4'=>'Neljapäev',
			'day4_abbr'=>'N',
			'day5'=>'Reede',
			'day5_abbr'=>'R',
			'day6'=>'Laupäev',
			'day6_abbr'=>'L',
			'day0'=>'Pähapäev',
			'day0_abbr'=>'P',
			
			'mon1'=>'Jaanuar',
			'mon2'=>'Veebruar',
			'mon3'=>'Märts',
			'mon4'=>'Aprill',
			'mon5'=>'Mai',
			'mon6'=>'Juuni',
			'mon7'=>'Juuli',
			'mon8'=>'August',
			'mon9'=>'September',
			'mon10'=>'Oktoober',
			'mon11'=>'November',
			'mon12'=>'Detsember',
			
			'month_select_lbl'=>'kuu',
			'year_select_lbl'=>'aasta',
			'prev_month_lbl'=>'Eelmine kuu',
			'next_month_lbl'=>'Järgmine kuu',
			
			'calendar_lbl'=>'kalender',
			'today_is_lbl'=>'Täna on',
			'show_month_lbl'=>'Näita',
			'week_nr_lbl'=>'Nädala number',
			'week_nr_abbr_lbl'=>'Ndl',
			'legend_lbl'=>'Tähtpäevad',
			
			'back_lbl'=>'Tagasi',
			),
		);
	var $vr=array();
	function __construct($l='', $date=array(), $tr=false, $cal_id='axs_calendar') {
		global $axs;
		$this->db=$axs['cfg']['site'][$axs['site_nr']]['db'];
		$this->px=$axs['cfg']['site'][$axs['site_nr']]['prefix'];
		$this->cal_id=$cal_id;
		$this->f_px=$axs['f_px'];
		$this->url=$axs['url'];
		//$this->langs=array_combine(array_keys(self::$tr), array_keys(self::$tr));
		//$this->l=(isset($this->langs[$l])) ? $l:current($this->langs);
		$this->tr=(isset(self::$tr[$l])) ? self::$tr[$l]:current(self::$tr);
		if (is_array($tr)) foreach ($tr as $k=>$v) $this->tr[$k]=$v;
		$this->tr['title']=$_SERVER['SERVER_NAME'].' '.$this->tr['calendar_lbl'];
		$this->js=axs_dir('lib', 'http').'axs.calendar.js';
		foreach ($this->selected=array('year'=>'','month'=>'','week'=>'','day'=>'') as $k=>$v) $this->selected[$k]=intval(axs_get($k, $date, 0));
		# Current date
		$tmp=explode('-', date('Y-m-W-d-H:i:s-w'));
		foreach (array(0=>'year',1=>'month',2=>'week',3=>'day',4=>'time',5=>'week_day') as $k=>$v) $this->{'this_'.$v}=$tmp[$k];
		$this->this_month_txt=$this->tr('mon'.$this->this_month);
		$this->this_week_day_txt=$this->tr('day'.$this->this_week_day);
		# Selected date
		foreach (array('month'=>'n','year'=>'Y','week'=>'W','day'=>'d',) as $k=>$v) {
			$this->$k=(!empty($date[$k])) ? intval($date[$k]):intval($this->{'this_'.$k});
			}
		$this->year_min=1970;
		if ($this->this_year<$this->year_min) $this->this_year=$this->year_min;
		$this->year_max=2037;
		if ($this->this_year>$this->year_max) $this->this_year=$this->year_max;
		if ($this->month<1) $this->month=1;
		if ($this->month>12) $this->month=12;
		if ($this->day>360) $this->day=360;
		$this->month_txt=$this->tr('mon'.$this->month);
		# Calculate dates
		$this->month_time=mktime(0, 0, 0, $this->month, 1, $this->year); # First day of the month Unix timestamp
		$tmp=explode('-', date('w-W-t', $this->month_time));
		foreach (array(
			0=>'week_day', # Weekday of the first day of the month
			1=>'week_year', # Week nr of the first day of the month
			2=>'date_max', # Number of days in month
			) as $k=>$v) $this->{'month_'.$v}=intval($tmp[$k]);
		$this->week=($this->selected['week']>=$this->month_week_year) ? intval($this->selected_week):'';
		# Calculate start and end timestamp
		$this->month_days_before=($this->month_week_day==0) ? 6:$this->month_week_day-1;
		$this->cal_start_timestamp=$this->start_timestamp=$this->month_time-($this->month_days_before*86400);
		if (!$this->week) {
			$nr_max=$this->month_days_before+$this->month_date_max;
			$nr=0;
			while ($nr<$nr_max) {
				$nr+=7;
				}
			if ($nr==$this->month_days_before+$this->month_date_max) {
				$end_date=$this->month_date_max;
				$end_month=$this->month;
				}
			else {
				$end_date=$nr-$nr_max;
				$end_month=$this->month+1;
				}
			}
		else {
			$nr=($this->week-$this->month_week_year)*7; # Cal days to the selected week
			$this->start_timestamp+=$nr*86400;
			$end_date=$nr+7-$this->month_days_before;
			$end_month=$this->month;
			}
		$this->cal_end_timestamp=$this->end_timestamp=mktime(23, 59, 59, $end_month, $end_date, $this->year); # Last day of the last week in calendar timestamp
		#<Start and end timestamp of selected date range>
		if ($this->selected['week'] or $this->selected['day']) {
			if ($this->selected['day']) {
				$this->start_timestamp=$this->month_time+($this->selected['day']-1)*86400;
				$this->end_timestamp=$this->start_timestamp+86400-1;
				}
			else {
				$this->start_timestamp+=($this->selected['week']-$this->month_week_year)*7*86400;
				$this->end_timestamp=$this->start_timestamp+7*86400-1;
				}
			} #</Start and end timestamp of selected date range>
		
		$this->vr['/']=$axs['http_root'];
		$this->vr['server']=$_SERVER['SERVER_NAME'];
		
		$this->classes=array();
		$this->dates=array();
		$this->templates=array(
			'cal'=>'<form id="{$cal_id}" action="" method="get" class="axs_calendar {$class}">'."\n".
				' <div>{$formact}</div>'."\n".
				' <h1 class="title">{$title}</h1>'."\n".
				' <p id="{$cal_id}_today" class="today"><a href="{$today_url}" class="today"><span class="today">{$today_is_lbl}</span> <span class="week_day">{$this_week_day_txt},</span> <span class="date">{$this_day}.{$this_month}.{$this_year}</span> <span class="time">{$this_time}</span></a></p>'."\n".
				' <fieldset class="select">'."\n".
				'  <label for="{$cal_id}_month">{$month_select_lbl}</label>'."\n".
				'  <select id="{$cal_id}_month" name="month">'."\n".
				'{$month_options}'."\n".
				'  </select>'."\n".
				'  <label for="{$cal_id}_year">{$year_select_lbl}</label>'."\n".
				'  <select id="{$cal_id}_year" name="year">'."\n".
				'{$year_options}'."\n".
				'  </select>'."\n".
				'  <input id="{$cal_id}_show" name="s" type="submit" class="submit" value="{$show_month_lbl}" />'."\n".
				' </fieldset>'."\n".
				' <table>'."\n".
				'  <caption>'."\n".
				'    <a href="{$prev_month_url}" title="{$prev_month_lbl}">&lt;</a> '."\n".
				'    <a href="{$month_url}">{$month_txt} {$year}</a>'."\n".
				'    <a href="{$next_month_url}" title="{$next_month_lbl}">&gt;</a>'."\n".
				'  </caption>'."\n".
				'  <thead>'."\n".
				'   <tr>'."\n".
				'    <th scope="col" class="week_nr"><abbr title="{$week_nr_lbl}">{$week_nr_abbr_lbl}</abbr></th>'."\n".
				'    <th scope="col" class="day1"><abbr title="{$day1_lbl}">{$day1_abbr_lbl}</abbr></th>'."\n".
				'    <th scope="col" class="day2"><abbr title="{$day2_lbl}">{$day2_abbr_lbl}</abbr></th>'."\n".
				'    <th scope="col" class="day3"><abbr title="{$day3_lbl}">{$day3_abbr_lbl}</abbr></th>'."\n".
				'    <th scope="col" class="day4"><abbr title="{$day4_lbl}">{$day4_abbr_lbl}</abbr></th>'."\n".
				'    <th scope="col" class="day5"><abbr title="{$day5_lbl}">{$day5_abbr_lbl}</abbr></th>'."\n".
				'    <th scope="col" class="day6"><abbr title="{$day6_lbl}">{$day6_abbr_lbl}</abbr></th>'."\n".
				'    <th scope="col" class="day0"><abbr title="{$day0_lbl}">{$day0_abbr_lbl}</abbr></th>'."\n".
				'   </tr>'."\n".
				'  </thead>'."\n".
				'  <tbody>'."\n".
				'{$weeks}'."\n".
				'  </tbody>'."\n".
				' </table>'."\n".
				'{$legend}'.
				'</form>{$js}',
			'week'=>'    <tr id="{$cal_id}_week{$nr_year}" class="{$class}">'."\n".
				'     <th scope="row"><a href="{$week_url}">{$nr_month}/{$nr_year}</a></th>'."\n".
				'{$days}    </tr>'."\n",
			'day'=>'     <td{$rowspan} class="{$class}">{$date}</td>'."\n",
			'legend'=>' <ul class="legend" title="{$legend_lbl}">'."\n".
				'{$list}'."\n".
				' </ul>'."\n",
			'legend.item'=>'     <li class="this_month {$name}">{$title}</li>'."\n",
			'space'=>'			',
			);
		} #</axs_calendar()>
	function tr($key) {
		if (isset($this->tr[$key])) return $this->tr[$key];
		else return '{$'.$key.'}';
		} #</tr()>
	function time_start_get($format=false) {
		return ($format) ? date($format, $this->start_timestamp):$this->start_timestamp;
		} #</time_start_get()>
	function time_end_get($format=false) {
		return ($format) ? date($format, $this->end_timestamp):$this->end_timestamp;
		} #</time_end_get()>
	function time_between_get($format=false, $separator=false) {
		$r=array(1=>$this->time_start_get($format), );
		$tmp=$this->time_end_get($format);
		if ($tmp!=$r[1]) $r[2]=$tmp;
		if ($separator) $r=implode($separator, $r);
		return $r;
		} #</time_between_get()>
	function url_set_date($root, $args=array()) { # Add link to every date
		if ($root===false) $root='?';
		$this->url_date=$root;
		$this->url_date_args=$args;
		} #</url_set_date()>
	function url_set_date_events($root, $args=array()) { # Add link only if date has events
		if ($root===false) $root='?';
		$this->url_date_events=$root;
		$this->url_date_args=$args;
		} #</url_set_date_events()>
	
	var $week_as_num=array(1=>'1',2=>'2',3=>'3',4=>'4',5=>'5',6=>'6',7=>'0');
	var $months_as_num=array(1=>'01',2=>'02',3=>'03',4=>'04',5=>'05',6=>'06',7=>'07',8=>'08',9=>'09',10=>'10',11=>'11',12=>'12');
	
	function season_get() { # Get season
		$seasons=array(
			'spring'=>array('m'=>3, 'd'=>20),
			'summer'=>array('m'=>6, 'd'=>20),
			'autumn'=>array('m'=>9, 'd'=>22),
			'winter'=>array('m'=>12, 'd'=>21),
			);
		foreach ($seasons as $k=>$v) $season=$k;
		foreach ($seasons as $k=>$v) {
			if ($this->month<$v['m']) break;
			$season=$k;
			}
		return $season;
		} #</season_get()>
	function dates_parse($class, $dates) {
		ksort($dates);
		foreach ($dates as $k=>$v) {
			$k.='';
			$dates[intval($k[0].$k[1])][intval($k[2].$k[3])][]=array('class'=>$class, 'text'=>$v, );
			unset($dates[$k]);
			}
		return $dates;
		} #</dates_parse()>
	function ylestousmispyha() {
		// easter_date()?
		# Arvutame välja 1. ülestõusmispüha. Selle järgi arvutame hiljem teised liikuvad pühad.
		# 1. ülestõusmispüha - esimesel täiskuujärgsel pühapäeval pärast kevadist pööripäeva 
		# (22. märts - 26. aprill). Üks kuutsükkel=29,5303 ööpäeva=2551418 s (andmed Eesti Entsüklopeediast)
		$this->ylestpyha=1106649120; # Esimene täiskuu 2005. a. 25.01
		$Mar22=mktime(0, 0, 0, 03, 22, $this->year); //kevadine pööripäev määratud aastal
		# Arvutame esimese täiskuu peale 22. märtsi kui aasta on alates 2005
		if ($this->year>=2005) while ($this->ylestpyha<=$Mar22) {
			$this->ylestpyha+=2551418;
			}
		# Arvutame esimese täiskuu peale 22. märtsi kui aasta on väiksem kui 2005
		else {
			while ($this->ylestpyha>=$Mar22) {
				$this->ylestpyha-=2551418;
				}
			$this->ylestpyha+=2551418;
			}
		# Liidame esimesele täiskuule peale 22. märtsi vajaliku arvu nädalapäevi et saada pühapäev
		$this->ylestpyha+=(7-date('w', $this->ylestpyha))*86400;
		return $this->ylestpyha;
		} #</ylestousmispyha()>
	function dates_get_holiday($set=true) { # määratleme riigipühad
		if (!isset($this->ylestpyha)) $this->ylestousmispyha();
		$dates=array(
			'0101'=>'Uusaasta.',
			'0224'=>'Iseseisvuspäev, Eesti Vabariigi aastapäev.',
			date('md', ($this->ylestpyha-172800))=>'Suur reede.', # - reede enne ülestõusmispüha
			date('md', $this->ylestpyha)=>'Ulestousmispuhade 1. puha.',
			'0501'=>'Kevadpuha.',
			date('md', ($this->ylestpyha+4233600))=>'Nelipuhade 1. puha.', # - 7 nädalat peale 1. ülestõusmispüha
			'0623'=>'Voidupuha.',
			'0624'=>'Jaanipäev.',
			'0820'=>'Taasiseseisvumispäev.',
			'1224'=>'Joululaupäev.',
			'1225'=>'Esimene joulupuha.',
			'1226'=>'Teine joulupuha.',
			);
		$dates=$this->dates_parse('holiday', $dates);
		if ($set) {
			$this->class_set('holiday', 'Riigipuha', 'background:#D9ECFF;'."\n".'		color:#ff0000;');
			$this->dates_set($dates);
			}
		return $dates;
		} #</dates_get_holiday()>
	function dates_get_national($set=true) { # määratleme rahvuslikud tähtpäevad
		if (!isset($this->ylestpyha)) $this->ylestousmispyha();
		$dates=array(
			'0106'=>'Kolmekuningapäev.',
			'0202'=>'Tartu rahulepingu aastapäev',
			'0314'=>'Emakeelepäev.',
			'0604'=>'Eesti lipu päev',
			'0614'=>'Leinapäev',
			'0922'=>'Vastupanuvoitluse päev',
			'1102'=>'Hingedepäev.',
			'1116'=>'Taassunni päev',
			(date('md', $tmp=mktime(0, 0, 0, 05, 07, $this->year)+(7-date('w', $tmp))*86400))=>'Emadepäev.',
			);
		$dates=$this->dates_parse('national', $dates);
		if ($set) {
			$this->class_set('national', 'Riiklik tähtapäev', 'background:#CCFFCC;'."\n".'		/*color:#000;*/');
			$this->dates_set($dates);
			}
		return $dates;
		} #</dates_get_national()>
	function dates_get_folklore($set=true) { # määratleme rahvakalendri tähtpäevad
		if (!isset($this->ylestpyha)) $this->ylestousmispyha();
		$dates=array(
			'0214'=>'Valentinipäev e. sobrapäev.',
			'0308'=>'Naistepäev.',
			'0401'=>'Aprillipäev e. naljapäev.',
			'0423'=>'Juripäev.',
			'0430'=>'Volbriöö.',
			'0501'=>'Volbripäev, maipäha.',
			'0901'=>'Esimene koolipäev e. tarkusepäev.',
			'1005'=>'Õpetajate päev.',
			'1031'=>'Halloween. Usupuhastuspäev.',
			'1110'=>'Mardipäev.',
			'1125'=>'Kadripäev.',
			# Liikuvad rahvakalendri tähtpäevad
			# Vastlapäeva leidmiseks tuleb ülestõusmispühadest lugeda tagasi seitse pühapäeva, millele järgnev 
			# teisipäev on vastlapäev (seda põhimõtet kasutab ka üks rahvakalendri arvestusmeetodeid). 
			date('md', $this->ylestpyha-4233600+2*86400)=>'Vastlapäev.', # - 7 nädalat enne 1.ülestõusmispüha + 2 päeva
			);
		$dates=$this->dates_parse('folklore', $dates);
		if ($set) {
			$this->class_set('folklore', 'Rahvalik tähtpäev', 'background:#61E496;'."\n".'		/*color:#000;*/');
			$this->dates_set($dates);
			}
		return $dates;
		} #</dates_get_folklore()>
	function class_set($class, $title, $css='', $options=array()) {
		$this->classes[$class]['title']=$title;
		$this->classes[$class]['css']=$css;
		foreach ($options as $k=>$v) $this->classes[$class][$k]=$v;
		} #</class_set()>
	function dates_set($dates) {
		if (is_array($dates)) foreach ($dates as $month=>$days) {
			foreach ($days as $day=>$events) foreach ($events as $k=>$v) $this->dates[$month][$day][]=$v;
			}
		} #</dates_set()>
	function dates_get($m=0, $d=0) {
		$dates=array();
		foreach ($this->dates as $month=>$days) foreach ($days as $day=>$evets) foreach ($evets as $k=>$v) {
			if ($m) {
				if ($m!=$month) continue;
				if ($d) {	if ($d!=$day) continue;	}
				}
			$dates[]=array('m'=>$month, 'd'=>$day, 'y'=>$this->year, )+$v;
			}
		return $dates;
		} #</dates_get()>
	function calendar_code_get($mode='') {
		global $axs;
		$cal=$this->vr+$this->tr;
		$cal['class']=$this->season_get().(($mode) ? ' '.$mode:'');
		if ($this->js) {
			$axs['page']['head'][$this->name.'.js']='<script src="'.$this->js.'"></script>'."\n";
			$cal['js']="\n".'<script>axs.calendar.init("'.$this->cal_id.'");</script>';
			}
		else $cal['js']='';
		$cal['month_url']='?'.axs_url($this->url, array('year'=>$this->year, 'month'=>$this->month, 'week'=>false, ));
		$cal['month_txt']=$this->month_txt;
		$cal['today_url']='?'.axs_url($this->url, array('year'=>'', 'month'=>'', ));
		$cal['this_week_day_txt']=$this->this_week_day_txt;
		# Scroll month/week URLs
		if ($this->month==1) {	$prev_month=12;	$prev_year=$this->year-1;	} # Previos month link
		else {	$prev_month=$this->month-1;	$prev_year=$this->year;	}
		$cal['prev_month_url']='?'.axs_url($this->url, array('year'=>$prev_year, 'month'=>$prev_month, ));
		if ($this->month==12) {	$next_month=1;	$next_year=$this->year+1;	} # Next month link
		else {	$next_month=$this->month+1;	$next_year=$this->year;	}
		$cal['next_month_url']='?'.axs_url($this->url, array('year'=>$next_year, 'month'=>$next_month, ));
		
		$cal['formact']='';
		foreach ($this->url as $k=>$v) $cal['formact'].='<input name="'.$k.'" type="hidden" value="'.htmlspecialchars($v).'" />'."\n";
		$cal['formact']=$cal['month_options']=$cal['year_options']=$cal['legend']='';
		$cal['rowspan']='';
		
		foreach ($this->url as $k=>$v) $cal['formact'].='<input name="'.$k.'" type="hidden" value="'.$v.'" />';
		foreach (array('cal_id','this_year','this_month','this_day','this_time','year',) as $v) $cal[$v]=$this->$v;
		foreach ($this->week_as_num as $k=>$v) {
			$cal['day'.$v.'_lbl']=$this->tr['day'.$v];
			$cal['day'.$v.'_abbr_lbl']=$this->tr['day'.$v.'_abbr'];
			}
		for ($y=$this->year_min; $y<=$this->year_max; $y++) {
			$selected=($this->year==$y) ? ' selected="selected"':'';
			$class=($this->this_year==$y) ? ' class="current"':'';
			$cal['year_options'].=$this->templates['space'].'<option value="'.$y.'"'.$selected.$class.'>'.$y.'</option>'."\n";
			}
		foreach ($this->months_as_num as $k=>$v) {
			$selected=($this->month==$k) ? ' selected="selected"':'';
			$class=($this->this_month==$k) ? ' class="current"':'';
			$cal['month_options'].=$this->templates['space'].'<option value="'.$k.'"'.$selected.$class.'>'.$this->tr['mon'.$k].'</option>'."\n";
			}
		# Construct calendar array
		$cal['weeks']=$calendar=array();
		$week_nr_year=$this->month_week_year;
		$week_day=1;
		if ($this->month_days_before) {
			$cal_year=($this->month==1) ? $this->year-1:$this->year;
			$cal_month=($this->month==1) ? 12:$this->month-1;
			$date_max=date('t', $this->cal_start_timestamp);
			$date_min=$date_max-$this->month_days_before+1;
			for ($date=$date_min; $date<=$date_max; $date++) {
				$calendar[$week_nr_year][$week_day]=array('y'=>$cal_year, 'm'=>$cal_month, 'date'=>$date, 'class'=>'other_month', );
				if ($week_day>6) {
					$week_day=1;
					$week_nr_year++;
					}
				else $week_day++;
				}
			}
		for ($date=1; $date<=$this->month_date_max; $date++) {
			$cal_year=$this->year;
			$cal_month=$this->month;
			$calendar[$week_nr_year][$week_day]=array('y'=>$cal_year, 'm'=>$cal_month, 'date'=>$date, 'class'=>'this_month', );
			if ($week_day>6) {
				$week_day=1;
				$week_nr_year++;
				}
			else $week_day++;
			}
		if ($week_day>1) {
			$cal_year=($this->month==12) ? $this->year+1:$this->year;
			$cal_month=($this->month==12) ? 1:$this->month+1;
			$date_max=7-$week_day+1;
			for ($date=1; $date<=$date_max; $date++) {
				$calendar[$week_nr_year][$week_day]=array('y'=>$cal_year, 'm'=>$cal_month, 'date'=>$date, 'class'=>'other_month', );
				$week_day++;
				}
			}
		# Add events and Parse calendar HTML
		if ($this->selected['week']) {	if (isset($this->url_date_args)) $this->url_date_args['week']=$this->selected['week'];	}
		$week_nr=1;
		foreach ($calendar as $k=>$v) {
			$week=array(
				'nr_month'=>$week_nr,
				'nr_year'=>$k,
				'week_url'=>'?'.axs_url($this->url, array('year'=>$this->year, 'month'=>$this->month, 'week'=>$k, )),
				'class'=>($k==$this->this_week) ? 'this_week':'',
				'days'=>'',
				'rowspan'=>'',
				'cal_id'=>$this->cal_id,
				);
			if ($k==$this->selected['week']) $week['class'].=($week['class']) ? ' selected':'selected';
			foreach ($v as $kk=>$vv) {
				if ($this->selected['week']) {	if ($k!=$this->selected['week']) continue;	}
				$date=$vv['date'];
				$class=$tags=array();
				$url=$events='';
				if ($vv['date']==$this->selected['day']) {
					$tags[]='em';
					$class[]='selected';
					}
				if ($vv['y']==$this->this_year && $vv['m']==$this->this_month && $vv['date']==$this->this_day) {
					$tags[]='strong';
					$class[]='today';
					}
				foreach ($tags as $vvv) $vv['date']='<'.$vvv.'>'.$vv['date'].'</'.$vvv.'>';
				if (isset($this->url_date)) { # Add link to every date
					if (!isset($this->url_date_events)) $url=$this->url_date.axs_url($this->url_date_args, array(
						'year'=>$vv['y'], 'month'=>$vv['m'], 'day'=>$date,
						));
					}
				# Add events to date
				$event_class=array();
				if (isset($this->dates[$vv['m']][$date])) {
					foreach ($this->dates[$vv['m']][$date] as $kkk=>$vvv) {
						$event_class[$vvv['class']]=$vvv['class'];
						$events.='<div class="'.$vvv['class'].'">'.$vvv['text'].'</div>	';
						}
					}
				foreach ($this->classes as $kkk=>$vvv) if (isset($event_class[$kkk])) $class[]=$kkk;
				
				if (isset($this->url_date_events)) { # Add link only if date has events
					if ($events) $url=$this->url_date_events.axs_url($this->url_date_args, array(
						'year'=>$vv['y'], 'month'=>$vv['m'], 'day'=>$date,
						));
					}
				if ($kk==6 or $kk==7) $class[]='weekend';
				$vv['class']=implode(' ', array('day'=>$vv['class'])+array_reverse($class));
				if ($url) $vv['date']='<a href="'.$url.'" class="'.$vv['class'].'">'.$vv['date'].'</a>'."\n";
				if ($mode!='compact') $vv['date'].=$events;
				$week['days'].=axs_tpl_parse($this->templates['day'], $vv);
				}
			$cal['weeks'][$k]=$week;
			if ($this->selected['week']) {
				$cal['weeks'][$k]['rowspan']=' rowspan="'.count($calendar).'"';
				if ($k==$this->selected['week']) {	$cal['weeks'][$this->month_week_year]['days']=$week['days'];	}
				if ($k!=$this->month_week_year) $cal['weeks'][$k]['days']='';
				}
			$week_nr++;
			}
		foreach ($cal['weeks'] as $k=>$v) $cal['weeks'][$k]=axs_tpl_parse($this->templates['week'], $v); # Parse week
		$cal['weeks']=implode('', $cal['weeks']);
		# Days legend
		foreach ($this->classes as $k=>$v) if (!empty($v['title'])) $cal['legend'].=axs_tpl_parse($this->templates['legend.item'], array('name'=>$k)+$v);
		if ($cal['legend']) $cal['legend']=axs_tpl_parse($this->templates['legend'], array('list'=>$cal['legend']));
		return axs_tpl_parse($this->templates['cal'], $cal);
		} # </calendar_code()>
	function css_get() {
		$css='<link href="'.axs_dir('lib', 'http').'axs.calendar.css" rel="stylesheet" type="text/css" />'."\n".
		'<style type="text/css">'."\n".
		'	<!--'."\n";
		foreach ($this->classes as $k=>$v) $css.='	.axs_calendar .'.$k.', .axs_calendar .this_month.'.$k.' {'."\n".
			'		'.$v['css']."\n".
			'		}'."\n";
		$css.='	-->'."\n".'</style>'."\n";
		return $css;
		} # </css_get()>
	# ------------------------------------------------------------
	function dates_get_sql($c='', $query='') {
		global $axs;
		$qurey_c=($c) ? ' AND `c`=\''.addslashes($c).'\'':'';
		if (!$query) $query='SELECT t.`class`, t.`color`, t.`label_'.addslashes($axs['l']).'` AS `label`, c.*'."\n".
		'	FROM `'.$this->px.$this->name.'_header` AS t LEFT JOIN ('."\n".
		'		SELECT c.*, CONCAT(u.`fstname`,\' \',u.`lstname`) AS `owner_name`'."\n".
		'		FROM `'.$this->px.$this->name.'` AS c LEFT JOIN `'.axs_user::$table.'` AS u ON u.`id`=c.`owner`'."\n".
		'		WHERE `site`=\''.intval($axs['site_nr']).'\''.$query_c.' AND c.`time` BETWEEN \''.$this->time_start_get().'\' AND \''.$this->time_end_get().'\''."\n".
		'		) AS c ON c.`class_id`=t.`id`'."\n".
		'	ORDER BY c.`time` ASC';
		$data=axs_db_query($query, 1, $this->db, __FILE__, __LINE__);
		return $this->dates_get_sql_parse($data);
		} #</dates_get_sql()>
	function dates_get_sql_parse($data=false) {
		if (!is_array($data)) $data=(array)$this->sql_result;
		$classes=$dates=array();
		foreach ($data as $cl) {
			if (!empty($cl['class'])) {
				if (!isset($classes[$cl['class']])) $classes[$cl['class']]=array(
					'label'=>$cl['label'], 'color'=>base_convert($cl['color'], 10, 16),
					);
				}
			if ($cl['id']) {
				$time=explode('-', date('n-d', $cl['time']));
				foreach(array(0=>'n',1=>'d') as $k=>$v) $cl[$v]=intval($time[$k]);
				$dates[$cl['n']][$cl['d']][$cl['time']]=$cl;
				}
			}
		foreach ($classes as $k=>$v) $this->class_set($k, $v['label'], 'background:#'.$v['color'].';');
		return $dates;
		} #</dates_get_sql_parse()>
	} # </class>
#2005-12 ?>