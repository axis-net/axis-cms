<?php #2019-04-09
class axs_socialmedia {
	static $name='socialmedia';
	static $buttons=array(
		'fb_like'=>array(),
		'fb_likebox'=>array('url'=>'', 'w'=>300, 'h'=>300, ),
		'g_plus1'=>array(),
		'linkedin_share'=>array(),
		'tw_share'=>array(),
		'mail'=>array('subject'=>false, ),
		'print'=>array(),
		);
	public $vr=array();
	public $tpl=array();
	function __construct() {
		//$this->og_meta();
		foreach (axs_content::plugin_dirs() as $k=>$v) if (file_exists($tmp=$v.__CLASS__.'.config.php')) include($tmp);
		} #</__construct()>
	static function btn($buttons=array()) {
		if ($buttons===array()) $buttons=self::$buttons;
		foreach ($buttons as $k=>$v) {
			if (empty($v)) $v=self::$buttons[$k];
			$v=call_user_func_array(array(__CLASS__, 'btn_'.$k), array($v));
			if ($v!==null) $buttons[$k]=$v;
			else unset($buttons[$k]);
			}
		return $buttons;
		} #</btn()>
	static function btn_fb_like() {
		#https://developers.facebook.com/docs/plugins/like-button
		return '<div id="fb-root"></div> <script>(function(d, s, id) {   var js, fjs = d.getElementsByTagName(s)[0];   if (d.getElementById(id)) return;   js = d.createElement(s); js.id = id;   js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";   fjs.parentNode.insertBefore(js, fjs); }(document, "script", "facebook-jssdk"));</script> <div class="fb-like" data-layout="button_count" data-send="false" data-show-faces="false" data-width="150">&nbsp;</div>';
		} #</fb_like()>
	static function btn_fb_likebox($a) {
		if (!$a['url']) return;
		return '<iframe width="'.$a['w'].'" height="'.$a['h'].'" src="//www.facebook.com/plugins/likebox.php?href='.urlencode($a['url']).'&amp;width='.$a['w'].'&amp;height='.$a['h'].'&amp;colorscheme=light&amp;show_faces=true&amp;border_color=white&amp;stream=false&amp;header=false"></iframe>';
		} #</fb_likebox()>
	/*static function btn_fb_share() {
		global $axs;
		#return '<a href="https://www.facebook.com/sharer/sharer.php?u='.urlencode($axs['http'].'://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']).'" class="share" lang="en"></a>';
		}*/ #</fb_share()>
	static function btn_g_plus1() {
		return '<!-- Place this tag where you want the +1 button to render. --> <div class="g-plusone" data-annotation="inline" data-size="medium" data-width="50">&nbsp;</div> <!-- Place this tag after the last +1 button tag. --><script>   (function() {     var po=document.createElement("script"); po.type="text/javascript"; po.async=true;     po.src="https://apis.google.com/js/plusone.js";     var s=document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(po, s);   })(); </script>';
		} #</g_plus1()>
	/*static function btn_g_share() {
		global $axs;
		#return '<a href="https://plus.google.com/share?'.urlencode($axs['http'].'://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']).'" class="share" lang="en"></a>';
		}*/ #</btn_g_share()>
	static function btn_linkedin_share() {
		global $axs;
		return '<script src="//platform.linkedin.com/in.js" type="text/javascript">lang: en_US</script><script type="IN/Share" data-counter="right"></script>';
		
		# mini 	: must be set to ‘true’
		# url 	: the page URL
		# source 	: the company/named source (200 characters maximum)
		# title 	: article title (200 characters maximum)
		# summary 	: a short description (256 characters maximum)
		#foreach (axs_content::path_get(false, 'title') as $k=>$v) $title=$v;
		#return '<a href="http://www.linkedin.com/shareArticle?mini=true&url='.urlencode($axs['http'].'://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']).'&amp;source='.urlencode($axs_content['title_site']).'&amp;title='.urlencode($title).'" class="share" lang="en"></a>';
		} #</btn_linkedin_share()>
	static function btn_tw_share() {
		return '<a class="twitter-share-button" href="https://twitter.com/share">Tweet</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>';
		
		# url 	: the page URL
		# text 	: optional text
		# hashtags 	: a comma-delimited set of hashtags
		#foreach (axs_content::path_get(false, 'title') as $k=>$v) $title=$v;
		#return '<a href="https://twitter.com/intent/tweet?url='.urlencode($axs['http'].'://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']).'&amp;text='.urlencode($title).'" class="share" lang="en"></a>';
		} #</btn_tw_share()>
	
	static function btn_mail($a=array()) {
		global $axs, $axs_content;
		if (empty($a['subject'])) $a['subject']=$axs_content['title_site'];
		return '     <script>'."\n".
		'	document.write(\'<a class="mail" href="mailto:?subject='.urlencode($a['subject']).'&amp;body=\'+escape(document.location.href)+\'" lang="en"><img src="'.axs_file_choose(
			self::$name.'.mail.png',
			'http',
			array(
				axs_dir('site').$axs['dir_site'].'gfx/'=>axs_dir('site', 'http').$axs['dir_site'].'gfx/',
				axs_dir('site_base')=>axs_dir('site_base', 'http'),
				)
			).'" alt="mail" /></a>\');'."\n".
		'     </script>';
		} #</btn_mail()>
	static function btn_print() {
		global $axs, $axs_content;
		return '     <script>'."\n".
		'	document.write(\'<div><a id="btn_print" href="#" lang="en"><img src="'.axs_file_choose(
			self::$name.'.print.png',
			'http',
			array(
				axs_dir('site').$axs['dir_site'].'gfx/'=>axs_dir('site', 'http').$axs['dir_site'].'gfx/',
				axs_dir('site_base')=>axs_dir('site_base', 'http'),
				)
			).'" alt="print" /></a></div>\');'."\n".
		'	document.getElementById("btn_print").onclick=function(){	window.print();	return false;	}'."\n".
		'     </script>';
		} #</btn_print()>
	
	static function og_meta($tags=array()) {
		global $axs, $axs_content;
		if (isset($tags['image'])) {
			if (strpos($tags['image'], '//')===false) $tags['image']=$axs['http'].'://'.$_SERVER['SERVER_NAME'].$tags['image'];
			}
		if (isset($tags['image.file'])) {
			$s=(axs_function_ok('getimagesize')) ? getimagesize($tags['image.file']):array(0, 0);
			foreach (array('width'=>0, 'height'=>1) as $k=>$v) if ($s[$v]) $tags['image:'.$k]=$s[$v];
			unset($tags['image.file']);
			}
		foreach ($tags as $k=>$v) $axs['page']['head']['og:'.$k]='<meta property="og:'.htmlspecialchars($k).'" content="'.htmlspecialchars($v).'" />'."\n";
		} #</og_meta()>
	} #</class::axs_socialmedia>
#http://www.sitepoint.com/social-media-button-links/?utm_medium=email&utm_campaign=SitePoint+Newsletter+September+5+2013&utm_content=SitePoint+Newsletter+September+5+2013+CID_6c94f66cb02d4578aa825f4ed8fee8b1&utm_source=Newsletter&utm_term=Read%20more
#2014-05-08 ?>