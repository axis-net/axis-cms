<?php #2021-03-04
class axs_config_edit {
	var $fields=array(
		'emails'=>array('type'=>'text', 'size'=>75, ),
		'dontmail'=>array('type'=>'text', 'size'=>75, ),
		'admin_contact'=>array('type'=>'textarea', 'size'=>75, 'rows'=>2, ),
		'charset'=>array('type'=>'text', 'size'=>10, ),
		'login_update'=>array('type'=>'number', 'step'=>1, ),
		'logsize'=>array('type'=>'number', 'step'=>1, ),
		'loguser'=>array('type'=>'number', 'step'=>1, ),
		'filemonitor'=>array('type'=>'textarea', 'size'=>75, 'rows'=>2, ),
		'timezone'=>array('type'=>'text', 'size'=>40, ),
		'cms_lang'=>array('type'=>'select', 'size'=>2, 'maxlength'=>2, 'options'=>array(), ),
		'cms_skin'=>array('type'=>'select', 'maxlength'=>25, 'options'=>array(''=>array('value'=>'', 'label'=>' - ')), ),
		'cms_http'=>array('type'=>'text', 'size'=>40, 'maxlength'=>255, ),
		'ftp'=>array('type'=>'text', 'size'=>75, ),
		'image_convert_util'=>array('type'=>'text', 'size'=>75, ),
		'smtp'=>array('type'=>'fieldset', ),
		'smtp_host'=>array('type'=>'text', 'size'=>40, ),
		'smtp_port'=>array('type'=>'number', 'step'=>1, ),
		'smtp_user'=>array('type'=>'text', 'size'=>40, ),
		'smtp_pass'=>array('type'=>'text', 'size'=>40, ),
		'smtp_encryption'=>array('type'=>'select', 'options'=>array(
			''=>array('value'=>'', 'label'=>'-'), 'tls'=>array('value'=>'tls', 'label'=>'TLS'), 'ssl'=>array('value'=>'ssl', 'label'=>'SSL'),
			)),
		'smtp_end'=>array('type'=>'fieldset_end', ),
		);
	var $fields_db=array(
		'type'=>array('type'=>'select', 'options'=>array(
				'mysql'=>array('value'=>'mysql', 'label'=>'MySQL (PDO_MYSQL)'),
				'sqlsrv'=>array('value'=>'sqlsrv', 'label'=>'MSSQL, SQL Azure (PDO_SQLSRV)'),
				'mssql'=>array('value'=>'mssql', 'label'=>'MSSQL (PDO_DBLIB)'),
				'sybase'=>array('value'=>'sybase', 'label'=>'Sybase (PDO_DBLIB)'),
				'pgsql'=>array('value'=>'pgsql', 'label'=>'PostgreSQL (PDO_PGSQL)'),
				''=>array('value'=>'', 'label'=>'disabled', 'lang'=>'en', ),
				),
			),
		'host'=>array('type'=>'text', 'size'=>40, ),
		'user'=>array('type'=>'text', 'size'=>40, ),
		'pass'=>array('type'=>'text', 'size'=>40, ),
		'db'=>array('type'=>'text', 'size'=>40, ),
		'collation'=>array('type'=>'text', 'size'=>10, ),
		);
	/*var $fields_smtp=array(
		'host'=>array('type'=>'text', 'size'=>40, ),
		'port'=>array('type'=>'number', 'step'=>1, ),
		'user'=>array('type'=>'text', 'size'=>40, ),
		'pass'=>array('type'=>'text', 'size'=>40, ),
		'encryption'=>array('type'=>'select', 'options'=>array(
				''=>array('value'=>'', 'label'=>'-'),
				'tls'=>array('value'=>'tls', 'label'=>'TLS'),
				'ssl'=>array('value'=>'ssl', 'label'=>'SSL'),
				),
			),
		);*/
	var $fields_site=array(
		'title'=>array('type'=>'text', 'size'=>40, ),
		'timezone'=>array('type'=>'text', 'size'=>40, ),
		'recycle'=>array('type'=>'text', 'size'=>3, ),
		'db'=>array('type'=>'select', 'size'=>3, 'options'=>array(0=>array('value'=>0, 'label'=>' - ')), ),
		'prefix'=>array('type'=>'text', 'size'=>20, ),
		'url_fake'=>array('type'=>'select', 'size'=>1, 'options'=>array(
				''=>array('value'=>'', 'label'=>'/?c=page-id&l=et'),
				'1'=>array('value'=>true, 'label'=>'/et/page-id'),
				),
			),
		'domain'=>array('type'=>'text', 'size'=>40, ),
		'dir'=>array('type'=>'text', 'size'=>40, ),
		'dir_fs_root'=>array('type'=>'text', 'size'=>40, ),
		);
	var $protected=array(
		'groups'=>array('cms'=>1, 'admin'=>2, 'dev'=>3, ), 'roles'=>array('r'=>1, 'w'=>2, 'e'=>3, ),
		);
	var $timezone_url='http://php.net/manual/en/timezones.php';
	function array_edit($array, $editnr, $do, $nk, $nv) {
		$newarray=array();
		$nr=1;
		foreach ($array as $k=>$v) {
			if ($nr==$editnr) {
				if ($do=='add') {
					$newarray[$nk]=$nv;
					$newarray[$k]=$v;
					}
				if ($do=='modif') $newarray[$nk]=$nv;
				}
			else $newarray[$k]=$v;
			$nr++;
			}
		if ($do=='add' && $nr<=$editnr) $newarray[$nk]=$nv;
		return $newarray;
		} #</array_edit()>
	static function array_from_list($str, $spr=',') {
		if ($spr==='n') $spr="\n|\r|\r\n";
		if (!is_array($str)) $str=preg_split('/'.$spr.'/', $str);
		$array=array();
		foreach ($str as $k=>$v) if ($v=trim($v)) $array[$v]='\''.axs_fn::array_el_esc($v).'\'';
		return implode(',', $array);
		} # </array_from_list()>
	static function groups_add($key, $label, $after, $f='groups') {
		global $axs;
		$pos=9999;
		$nr=1;
		foreach ($axs['cfg'][$f] as $k=>$v) {
			$nr++;
			if ($k==$after) $pos=$nr;
			}
		if (($f==='groups') && ($axs['cfg']['db'][1]['type'])) {
			$sql=new axs_sql_structure(1);
			if (!$sql->table_col_exists(axs_user::$table, 'g_'.$key)) $sql->table_alter_add(axs_user::$table, array(
				'g_'.$key=>array('Type'=>'TINYINT(1)', 'Null'=>false, 'after'=>'g_'.$after)
				));
			}
		$axs['cfg'][$f]=axs_fn::array_el_add($axs['cfg'][$f], $pos, $key, $label);
		return axs_config_edit::write(array($f=>$axs['cfg'][$f]));
		} #</groups_add()>
	function groups_edit($key, $label, $f='groups') {
		global $axs;
		if (array_search($label, $axs['cfg'][$f])) return $f.'_exists';
		if (isset($this->protected[$f][$key])) return;
		$axs['cfg'][$f][$key]=$label;
		return axs_config_edit::write(array($f=>$axs['cfg'][$f]));
		} #</groups_edit()>
	function groups_order($key, $pos, $f='groups') {
		global $axs;
		if (isset($this->protected[$f][$key])) return;
		if ($pos<($tmp=count($this->protected[$f])+1)) $pos=$tmp;
		$axs['cfg'][$f]=axs_fn::array_el_move($axs['cfg'][$f], $key, $pos);
		return axs_config_edit::write(array($f=>$axs['cfg'][$f]));
		} #</groups_edit()>
	static function groups_del($key, $f='groups') {
		global $axs;
		if (($f==='groups') && ($axs['cfg']['db'][1]['type'])) {
			if ($key=='admin' or $key=='cms') return; #Don't allow deleting system groups. 
			$sql=new axs_sql_structure(1);
			if ($sql->table_col_exists(axs_user::$table, 'g_'.$key)) {
				$users=axs_db_query("SELECT COUNT(*) FROM `".axs_user::$table."` WHERE `g_".$key."`", 'cell', 1, __FILE__, __LINE__);
				if ($users) return 'Could not delete group containing users! '.$key.'('.$users.')';
				$sql->table_alter_del(axs_user::$table, 'g_'.$key);
				}
			}
		unset($axs['cfg'][$f][$key]);
		return axs_config_edit::write(array($f=>$axs['cfg'][$f]));
		} #</groups_del()>
	static function site_themes_get(/*$site_nr*/) {
		global $axs;
		$themes=array();
		foreach (axs_admin::f_list(axs_dir('site').$axs['dir_site'], '/^theme\\.*+/', 'dir') as $k=>$v) {
			$v=preg_replace('/^theme\\./', '', $v);
			$themes[$k]=array('value'=>$v, 'label'=>$v);
			}
		ksort($themes);
		return $themes;
		} # </site_themes_get()>
	static function validate($values, $tr) { # Validate system configuration data
		global $axs;
		$error=array();
		if ($values['emails']) {	if (!axs_valid::emails($values['emails'])) $error['emails']=$tr->s('cfg.msg.emails');	}
		if (!trim($values['charset'])) $error['charset']=$tr->s('cfg.msg.charset');
		foreach ($values['site'] as $k=>$v) { #<Validate site profiles>
			if (!trim($v['title'])) $error['site_'.$k.'_title']=$tr->s('cfg.msg.site.title', array('nr'=>$k));
			} #</Validate site profiles>
		foreach ($values['db'] as $k=>$v) if ($v['type']) { #<Validate SQL database profiles>
			if (empty($axs['cfg']['db'][$k]['local'])) {
				$tmp=$axs['cfg']['db'][$k];
				foreach (array('type','host','user','pass','db') as $vv) $axs['cfg']['db'][$k][$vv]=$v[$vv];
				}
			$err=axs_db_query('', 'error', $k, __FILE__, __LINE__, array('error_handle'=>0));
			if (empty($axs['cfg']['db'][$k]['local'])) $axs['cfg']['db'][$k]=$tmp;
			if ($err) $error[]=$tr->s('cfg.db.msg').' '.$k.': '.$err;
			}
		#</Validate SQL database profiles>
		return $error;
		} # </validate()>
	
	static function write($change, $tr=array()) { # Write system main configuration file
		global $axs;
		# <Change values>
		$values=(file_exists($tmp=AXS_PATH_CMS.'data/index.php')) ? (array)include($tmp):array();
		foreach ($change as $k=>$v) {
			if (is_array($v)) foreach ($v as $kk=>$vv) {
				if (is_array($vv)) foreach ($vv as $kkk=>$vvv) $values[$k][$kk][$kkk]=$vvv;
				else $values[$k][$kk]=$vv;
				}
			else $values[$k]=$v;
			}
		foreach (array('groups','roles',) as $v) if (isset($change[$v])) $values[$v]=$change[$v];
		if (isset($change['roles'])) $values['roles']=$change['roles'];
		# </Change values>
		# <Error check>
		$error=axs_config_edit::validate($values, $tr);
		if (!empty($error)) return $error;
		# </Error check>
		# <Build file contents>
		$groups=$roles=$databases=$smtp=$sites='';
		foreach ($values['groups'] as $k=>$v) $groups.="		'".axs_fn::array_el_esc($k)."'=>'".axs_fn::array_el_esc($v)."',\n";
		foreach ($values['roles'] as $k=>$v) $roles.="		'".axs_fn::array_el_esc($k)."'=>'".axs_fn::array_el_esc($v)."',\n";
		foreach ($values['db'] as $k=>$v) $databases.="		".intval($k)."=>array('type'=>'".axs_fn::array_el_esc($v['type'])."', 'host'=>'".axs_fn::array_el_esc($v['host'])."', 'user'=>'".axs_fn::array_el_esc($v['user'])."', 'pass'=>'".axs_fn::array_el_esc($v['pass'])."', 'db'=>'".axs_fn::array_el_esc($v['db'])."', 'collation'=>'".axs_fn::array_el_esc($v['collation'])."', ),\n";
		foreach ($values['site'] as $k=>$v) {
			$v['url_fake']=($v['url_fake']) ? 'true':'false';
			foreach ($v['langs'] as $kk=>$vv) $v['langs'][$kk]="'".axs_fn::array_el_esc($kk)."'=>array('l'=>'".axs_fn::array_el_esc($vv['l'])."','abbr'=>'".axs_fn::array_el_esc($vv['abbr'])."','h'=>".(($vv['h'])?'true':'false')."), ";
			//\'max_level\'=>'.intval($v['max_level']).', 
			$sites.="		".intval($k)."=>array('title'=>'".axs_fn::array_el_esc(trim($v['title']))."', 'timezone'=>'".axs_fn::array_el_esc($v['timezone'])."', 'recycle'=>".intval($v['recycle']).", 'db'=>".intval($v['db']).", 'prefix'=>'".axs_fn::array_el_esc($v['prefix'])."', 'url_fake'=>".$v['url_fake'].", 'domain'=>'".axs_fn::array_el_esc(trim($v['domain']))."', 'dir'=>'".axs_fn::array_el_esc(trim($v['dir']))."', 'dir_fs_root'=>'".axs_fn::array_el_esc(trim($v['dir_fs_root']))."', 'langs'=>array(".implode('', $v['langs'])."), ),\n";
			}
		$data="<?php # AXIS CMS system config file (modified by ".axs_user::get('user').' @ '.date('d.m.Y H:i:s').")\n".
		"return array( #<system config />\n".
		"	'cfg_ver'=>'".axs_fn::array_el_esc(AXS_CMS_VER)."',\n".
		"	'emails'=>array(".axs_config_edit::array_from_list($values['emails'])."), #<system e-mail addresses />\n".
		"	'dontmail'=>array(".axs_config_edit::array_from_list($values['dontmail'])."), #<message types, that should not be notified by e-mail />\n".
		"	'admin_contact'=>'".axs_fn::array_el_esc($values['admin_contact'])."', #<on feedback form />\n".
		"	'login_update'=>".intval($values['login_update']).", #<Update login session after n seconds />\n".
		"	'logsize'=>".intval($values['logsize']).", #<number of logfiles to keep (+1MB buffer space needed) />\n".
		"	'loguser'=>".intval($values['loguser']).", #<number of log records to keep about user actions />\n".
		"	'filemonitor'=>array(".axs_config_edit::array_from_list($values['filemonitor'], 'n')."), #<monitor changes in these files/folders />\n".
		"	'charset'=>'".axs_fn::array_el_esc(trim($values['charset']))."', #<system character set />\n".
		"	'timezone'=>'".axs_fn::array_el_esc(trim($values['timezone']))."', #<system default timezone />\n".
		"	'cms_lang'=>'".axs_fn::array_el_esc($values['cms_lang'])."', #<CMS default language />\n".
		"	'cms_skin'=>'".axs_fn::array_el_esc($values['cms_skin'])."', #<CMS template set />\n".
		"	'cms_http'=>'".axs_fn::array_el_esc($values['cms_http'])."', #<CMS HTTP path />\n".
		"	'ftp'=>'".axs_fn::array_el_esc($values['ftp'])."', #<custom FTP URL />\n".
		"	'image_convert_util'=>'".axs_fn::array_el_esc($values['image_convert_util'])."', #<ImageMagic/GraphicsMagic path />\n".
		"	'smtp_host'=>'".axs_fn::array_el_esc($values['smtp_host'])."', #<SMTP host />\n".
		"	'smtp_port'=>'".axs_fn::array_el_esc($values['smtp_port'])."', #<SMTP port />\n".
		"	'smtp_encryption'=>'".axs_fn::array_el_esc($values['smtp_encryption'])."', #<SMTP encryption />\n".
		"	'smtp_user'=>'".axs_fn::array_el_esc($values['smtp_user'])."', #<SMTP user />\n".
		"	'smtp_pass'=>'".axs_fn::array_el_esc($values['smtp_pass'])."', #<SMTP password />\n".
		"	'groups'=>array( #<user groups />\n".
		$groups."		),\n".
		"	'roles'=>array( #<user roles />\n".
		$roles."		),\n".
		"	'db'=>array( #<databases config />\n".
		$databases."		),\n".
		"	'site'=>array( #<sites config />\n".
		$sites."		),\n".
		"	);\n".
		"?>";
		# </Build file contents>
		# Write file
		$fn=new axs_filesystem(AXS_PATH_CMS.'data/');
		if ($tmp=$fn->f_save('index.php', $data)) $error[]=$tmp;
		return $error;
		} # </write()>
	} # </class::axs_config_edit>
#2010-02-15 ?>