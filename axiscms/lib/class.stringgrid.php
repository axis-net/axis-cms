<?php

class stringGrid {
/*
	The first PHP-Class by Jochen Preusche, written in june / july 2007
	Licensed under the GPL
	http://www.gnu.org/licenses/gpl-3.0.html
	
	
*/



/*
Line feeds --------------------------------------------------------------------------
                                     Zeichen  Regex   ASCII hexadez.   ASCII dezimal
MS-DOS, Windows .................... CR LF    \r\n    0D0A             13 10
Apple Mac OS bis Version 9 ......... CR 	  \r      0D               13
alle Unix-Derivate, Mac OS X ....... LF 	  \n      0A               10
-------------------------------------------------------------------------------------
*/
	
	
	function __construct() {
	//nothing @ the moment..
	}
	
	
	var $cellStrings = array();


	function GetCollumned($textRows, $tabArray, $colSep='', $rowSep='', $lf="\n", $delJunkLines=true){
	if (strlen($rowSep)<1) $rowSep = false;
	
	# to make seperating lines between rows we need the full length of a row
	for ($i=0; $i<count($tabArray['colWidth']); $i++) {
		$fullRowLen += $tabArray['colWidth'][$i];
	}
	$fullRowLen = strlen($colSep)*(count($tabArray['colWidth']))+$fullRowLen;
	$rowSep = str_pad($rowSep, $fullRowLen, $rowSep);
	

		# error handling
		if(count($textRows[0]) > count($tabArray['colWidth'])){
			echo "Please make sure to define the width of each row!";
			exit();
		}
		# handle each Row
		$rowHeight = 1;
		for ($i=0; $i<count($textRows); $i++) {
			for ($j=0; $j<count($textRows[$i]); $j++) {
				# first, all cells are designed unattendet to their height
				$cells[$i][$j] = $this->prepCell(utf8_decode($textRows[$i][$j]), $tabArray['colWidth'][$j], $tabArray['hAlign'][$j]);
				if ($rowHeight<count($cells[$i][$j])) {
					$rowHeight = count($cells[$i][$j]);
				}
			}
			# $rowHeight is the height of the highest cell in this row
			if ($rowHeight!=1) {
				# adjust all heights
				$cells[$i] = $this->adjustCellHeights($cells[$i], $rowHeight, $tabArray['colWidth'], $tabArray['vAlign']);
			}
			# new row, new height: starting at 1 line height
			$rowHeight =1;
		}

		# make string
		$str = '';
		for ($i=0; $i<count($cells); $i++) {
			$linesThisRow = count($cells[$i][0]);
				for ($k=0; $k<$linesThisRow; $k++) {
					for ($j=0; $j<count($tabArray['colWidth']); $j++) {
						$str .= $cells[$i][$j][$k] .$colSep;
						if ($j == count($tabArray['colWidth'])-1) {
							$str .=$lf;
						}
					}
				}
			if (($i<count($cells)-1) && ($rowSep)) {
				$str .=$rowSep .$lf;
			}
		}
		
		if ( $delJunkLines) {
		# delete unnecessary spaces?
			$regex = '/\s*?(\r|\r\n|\n)/';
			$str =  preg_replace($regex, "\n", $str);
		}
		return ($str);
	}
	

	# ------------->    String,   CollumW, hAlign
	function prepCell($cellStr, $cellW, $hAlign=0){
	$proceeded =0;
	# to avoid double-line-feeds when a line ends with a line feed:
	$cellStr = ereg_replace("(\r|\r\n|\n)", ' \1', $cellStr);
	#$cellStr = ereg_replace("^/\r|\r\n|\n/", '-', $cellStr);
	
	for  ($i =0; $i<count(preg_split('/ |\n/', $cellStr)); $i++) {
		
		$strpart = $this->strtrim(substr($cellStr, $proceeded), $cellW);
		$cellStrings[$i]=trim($strpart[0]);
		
		$proc = strlen($strpart[0]);
		
		$proceeded = $proceeded + $proc;
		
				# indicates if to proceed
		if ($strpart[1]) {
			$strpart = $this->strtrim(substr($cellStr, $proceeded), $cellW);
				} else {
					$i = count(preg_split('/ |\n/', $cellStr));
				}
	} 
	# if there is an empty line
	$regex = '/^\s*?$/';
	for ($i=0; $i<count($cellStrings); $i++) {
		if (preg_match($regex, $cellStrings[$i])) {
   			## delete this line, its empty
   			array_splice($cellStrings, $i, 1);
		}	
	}
	
	# end if delete empty line

			
			# $cellStrings is now an array with fitting lines.
			if ($hAlign==0) {
				// left align
				for($i=0; $i<count($cellStrings); $i++){
					$cellStrings[$i]=utf8_encode(str_pad($cellStrings[$i], $cellW)); 
				}
			}
			else if ($hAlign==1) {
				// center
				for($i=0; $i<count($cellStrings); $i++){
					$cellStrings[$i]=utf8_encode(str_pad($cellStrings[$i], $cellW, ' ' ,STR_PAD_BOTH)); 
				}
			}
			else if ($hAlign==2) {
				// center
				for($i=0; $i<count($cellStrings); $i++){
					$cellStrings[$i]=utf8_encode(str_pad($cellStrings[$i], $cellW, ' ' ,STR_PAD_LEFT)); 
				}
			}
			return($cellStrings);
	}
	
	function adjustCellHeights($rowArray, $cH, $cW, $vAlign) {
		for ($i=0; $i<count($rowArray); $i++) {
		
		$fill = str_pad("", $cW[$i]);
		$tempArray = array();
		
			if (count($rowArray[$i])!=$cH){
				# check V-Alignment
				if (
				$vAlign[$i]==0 ||
				(($cH-count($rowArray[$i]))<2 && ($vAlign[$i]==1))
				
				) {
					# top
					$tmp = count($rowArray[$i]);
					for ($j=$tmp; $j<$cH; $j++){
						$rowArray[$i][$j] = $fill;
					}
				}

			if ($vAlign[$i]==1) {
					# center
					$tmp = $cH-count($rowArray[$i]);
					$toplines = floor ($tmp/2);
					$bottomlines = ceil ($tmp/2);
					if ($toplines>0){
						# ad lines on top
						for ($j=0; $j<$toplines; $j++){
							$tempArray[] = $fill;
						}
						$rowArray[$i] = array_merge($tempArray, $rowArray[$i]);
					}
					
					
					if ($bottomlines>0){
						# ad lines below
						$tempArray = array();
						for ($j=0; $j<$bottomlines; $j++){
							$tempArray[] = $fill;
						}
						$rowArray[$i] = array_merge($rowArray[$i], $tempArray);
						
					}
				}

			if ($vAlign[$i]==2) {
					# bottom
					$tmp = $cH-count($rowArray[$i]);
					for ($j=0; $j<$tmp; $j++){
						$tempArray[] = $fill;
					}
					$rowArray[$i] = array_merge($tempArray, $rowArray[$i]);				
				}			
			}
		}
		return ($rowArray);
	} // end of Height adjust 

	function strtrim($str, $maxlen=100, $elli=NULL, $maxoverflow=0) {
	# credits for this function:
	# feedback at realitymedias dot com
	# posted at http://de2.php.net/manual/de/function.substr.php
	# modified by jochen preusche
    global $CONF;
     
	if (preg_match("/\r\n|\r|\n/", $str)){
			# if there is  a line feed we process only until the first lf and throw the rest back
			preg_match("/\r\n|\r|\n/", $str, $treffer, PREG_OFFSET_CAPTURE);
			$str1 = substr($str, 0, $treffer[0][1]);
			$str2 = substr($str, $treffer[0][1]);
			$str = $str1;
	} else {
		$str2 = FALSE;
	}
	
    if ((strlen($str) > $maxlen) || $str2) {
           
        if ($CONF["BODY_TRIM_METHOD_STRLEN"]) {
            return substr($str, 0, $maxlen);
        }
           
        $output = NULL;
        $body = explode(" ", $str);
        $body_count = count($body);
       
        $i=0;
   
        do {
            $output .= $body[$i]." ";
            $thisLen = strlen($output);
            $cycle = ($thisLen < $maxlen && $i < $body_count-1 && ($thisLen+strlen($body[$i+1])) < $maxlen+$maxoverflow?true:false);
            $i++;
        } while ($cycle);
        
        return array($output, true, $str2);
    }
    else return array($str, false);
	}
	
}
?>
