<?php #2016-09-21
class axs_fn2 {
	function ip_to_number($ip) { # Convert IP to integer
		$ip=ip2long($ip);
		if ($ip==-1 or $ip===FALSE) return 0;
		return sprintf("%010u", $ip);
		} # </ip_to_number()>
	function ip_from_number($number) {
		$oct4=($number-fmod($number, 16777216))/16777216;
		$oct3=(fmod($number, 16777216)-(fmod($number, 16777216)%65536))/65536;
		$oct2=(fmod($number, 16777216)%65536-(fmod($number, 16777216)%65536%256))/256;
		$oct1=fmod($number, 16777216)%65536%256;
		$ip=$oct1.'.'.$oct2.'.'.$oct3.'.'.$oct4;
		return $ip;
		} # </ip_from_number()>
	} #</class::axs_fn2>
#2016-09-21 ?>