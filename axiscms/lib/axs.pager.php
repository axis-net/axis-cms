<?php #2016-09-18
# <Pager functions. />
class axs_pager {
	public $key_p='p';
	public $pager_size=20;
	public $templates=array(
		'page'=>'<a href="{$href}">{$p}</a> ',
		'page.current'=>'<a href="{$href}" class="current"><strong>{$p}</strong></a> ',
		'page.hellip'=>'<span class="hellip">&hellip;</span> ',
		);
	function __construct($p, $psizes=array(25,50,100), $psize=false, $templates=array()/*$desc=false, $sort=false*/) {
		$this->p=($p) ? abs(intval($p)):1;
		$this->psizes=(array)$psizes;
		$this->psize=intval((in_array($psize, $this->psizes)) ? $psize:current($this->psizes));
		$this->start=abs($this->psize*$this->p-$this->psize);
		foreach ($templates as $k=>$v) {	$this->templates[$k]=$v;	}
		} #</axs_pager()>
	function back_url_get($key, $id, $fallback_url) { # <Get back URL from session />
		if (!isset($_SESSION)) session_start();
		if (isset($_SESSION['axs_back_url'][$key][$id])) return $_SESSION['axs_back_url'][$key][$id];
		else return $fallback_url;
		} # </back_url_get()>
	function back_url_set($key, $urls, $urls_limit=false, $keys_limit=10) { # <Set back URLs into session />
		if (!isset($_SESSION)) session_start();
		if (empty($_SESSION['axs_back_url'])) $_SESSION['axs_back_url']=array();
		$nr=count($_SESSION['axs_back_url']);
		if ($nr>$keys_limit) foreach ($_SESSION['axs_back_url'] as $k=>$v) {
			unset($_SESSION['axs_back_url'][$k]);
			$nr--;
			if ($nr<=$keys_limit) break;
			}
		if (empty($_SESSION['axs_back_url'][$key])) $_SESSION['axs_back_url'][$key]=array();
		if (!$urls_limit) $urls_limit=$this->psize*2;
		$nr=count($_SESSION['axs_back_url'][$key]);
		if ($nr>$urls_limit) foreach ($_SESSION['axs_back_url'][$key] as $k=>$v) {
			unset($_SESSION['axs_back_url'][$key][$k]);
			$nr--;
			if ($nr<=$urls_limit) break;
			}
		foreach ($urls as $k=>$v) $_SESSION['axs_back_url'][$key][$k]=$v;
		} # </back_url_set()>
	function pages($total, $href) {
		$p=$start=$pages_show=0;
		$prev=$current=$next='';
		$pages=$a_before=$a=$a_after=array();
		$pages_show_half=floor($this->pager_size/2);
		while($start<$total) {
			$p++;
			$end=$start+$this->psize;
			if ($end>$total) $end=$total;
			if (($p===1) || (($p>$this->p-$pages_show_half) && ($pages_show<=$this->pager_size)) || ($end===$total)) {
				$tmp=array('href'=>$href.(($p>1) ? $p:''), 'p'=>$p, 'start'=>$start+1, 'end'=>$end, );
				if ($p===1) $a_before[$p]=$tmp;
				if (($p>$this->p-$pages_show_half) && ($pages_show<=$this->pager_size)) $a[$p]=$tmp;
				if ($end===$total) $a_after[$p]=$tmp;
				$pages_show++;
				}
			$start=$end;
			}
		reset($a);
		$first=current($a)['p'];
		if ($first>1) $pages=$a_before;
		if ($first>2) $pages['before']=$this->templates['page.hellip'];
		$pages=array_slice(array_merge($pages, $a), 0, $this->pager_size, true);
		end($pages);
		if ($tmp=current($a_after)['p']-current($pages)['p']>0) {
			array_pop($pages);
			array_pop($pages);
			$pages['after']=$this->templates['page.hellip'];
			$pages=array_merge($pages, $a_after);
			}
		foreach ($pages as $k=>$v) {
			if (is_array($v)) $pages[$k]=axs_tpl_parse(($v['p']===$this->p) ? $this->templates['page.current']:$this->templates['page'], $v);
			}
		if ($total) {
			if ($this->p>1) $prev=$this->p-1;
			if (($this->p*$this->psize)<$total) $next=$this->p+1;
			if ((!empty($this->templates['prev'])) && (!empty($this->templates['next']))) {
				$prev=($prev) ? str_replace('{$url}', $href.(($prev>1)?$prev:'') ,$this->templates['prev']):'';
				$next=($next) ? str_replace('{$url}', $href.$next ,$this->templates['next']):'';
				}
			else {
				$prev=($prev) ? '<a href="'.$href.(($prev>1)?$prev:'').'" rel="prev" title="&lt;">&lt;</a>':'';
				$next=($next) ? '<a href="'.$href.$next.'" rel="next" title="&gt;">&gt;</a>':'';
				}
			$current='('.$this->p.'/'.(($this->psize && $this->psize<$total) ? ceil($total/$this->psize):1).')';
			}
		$tmp=array('pages'=>($total>$this->psize) ? implode('', $pages):'', 'prev'=>$prev, 'next'=>$next, 'total'=>$total, 'current'=>$current);
		if (!empty($this->templates[''])) return ($tmp['pages']) ? axs_tpl_parse($this->templates[''], $tmp):'';
		return (!empty($this->templates[''])) ? axs_tpl_parse($this->templates[''], $tmp):$tmp;
		} #</pages()>
	function sql($desc=false, $sort=false) {
		$this->sort_col=$sort;
		$this->desc=($desc) ? ' DESC':'';
		if (!$this->sort_col) $this->desc='';
		return array('p'=>$this->p, 'start'=>$this->start, 'psize'=>$this->psize, 'desc'=>$this->desc);
		} #</sql()>
	} #</class::axs_pager>
#2010-09-09
?>