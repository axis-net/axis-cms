<?php #2019-06-19
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class axs_mail {
	static $phpmailer=false;
	static function send($toemail, $subject, $txt, $html, $fromname, $fromemail, $attach=array(), $headers_modify=array()) {
		global $axs;
		#exit(dbg($toemail, $subject, $txt, $html, $fromname, $fromemail));
		if (!self::$phpmailer) {
			require_once('phpmailer.php');
			require_once('phpmailer.smtp.php');
			require_once('phpmailer.exception.php');
			self::$phpmailer=new PHPMailer();                              // Passing `true` enables exceptions
			self::$phpmailer->CharSet='UTF-8';
			#self::$phpmailer->SMTPDebug=3;                                 // Enable verbose debug output
			self::$phpmailer->Debugoutput='html';
			self::$phpmailer->isSMTP();                                      // Set mailer to use SMTP
			self::$phpmailer->Host=$axs['cfg']['smtp_host'];  // Specify main and backup SMTP servers
			#self::$phpmailer->Host=gethostbyname($axs['cfg_smtp']['host']); // if your network does not support SMTP over IPv6
			self::$phpmailer->Port=$axs['cfg']['smtp_port'];                                   // TCP port to connect to
			self::$phpmailer->SMTPKeepAlive=true; // SMTP connection will not close after each email sent, reduces SMTP overhead
			if (!empty($axs['cfg']['smtp_user'])) {
				self::$phpmailer->SMTPAuth=true;                               // Enable SMTP authentication
				self::$phpmailer->Username=$axs['cfg']['smtp_user'];                 // SMTP username
				self::$phpmailer->Password=$axs['cfg']['smtp_pass'];                           // SMTP password
				self::$phpmailer->SMTPSecure=$axs['cfg']['smtp_encryption'];                            // Enable TLS encryption ('tls', 'ssl')
				}
			}
		//Recipients
		self::$phpmailer->setFrom($fromemail, $fromname);
		$toemail=explode(',', $toemail);
		foreach ($toemail as $v) self::$phpmailer->addAddress(trim($v));     // Add a recipient, Name is optional
		if (!empty($headers_modify['Reply-To'])) self::$phpmailer->addReplyTo($headers_modify['Reply-To']);
		foreach (array('Cc'=>'addCc', 'Bcc'=>'addBcc') as $k=>$v) if (!empty($headers_modify[$k])) {
			$tmp=explode(',', $headers_modify[$k]);
			foreach ($tmp as $kk=>$vv) self::$phpmailer->{$v}(trim($vv));
			}
		//Attachments
		#self::$phpmailer->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
		#self::$phpmailer->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
		if (!empty($attach)) foreach ($attach as $k=>$v) self::$phpmailer->AddStringAttachment($v['content'], $k, 'base64', $v['content_type']);
		//Content
		self::$phpmailer->Subject=$subject;
		if ($html) {
			self::$phpmailer->isHTML(true);                                  // Set email format to HTML
			self::$phpmailer->Body=$html;
			self::$phpmailer->AltBody=$txt;
			}
		else {
			self::$phpmailer->isHTML(false);                                  // Set email format to HTML
			self::$phpmailer->Body=$txt;
			}
		$result=self::$phpmailer->send();
		if (!$result) axs_log(__FILE__, __LINE__, __CLASS__, self::$phpmailer->ErrorInfo);
		self::$phpmailer->ClearAllRecipients();
		self::$phpmailer->clearAttachments();
		return $result;
		} #</send()>
	} # </class::axs_mail>
#2018-04-07 ?>