<?php #2022-01-30
class axs_form_header extends axs_form {
	static $fields_cfg=array(
		''=>array(
			'id'=>array('type'=>'text', 'size'=>10, 'maxlength'=>10, 'readonly'=>'readonly', ),
			'nr'=>array('type'=>'number', 'size'=>2, 'min'=>0, 'max'=>255, 'step'=>'1', 'required'=>1, ),
			'module'=>array('type'=>'text', 'size'=>25, 'maxlength'=>255, 'options'=>array(''=>array('value'=>'', 'label'=>'')), 'required'=>1, ),
			'form'=>array('type'=>'text', 'size'=>25, 'maxlength'=>255, 'options'=>array(''=>array('value'=>'', 'label'=>'')), ),
			'label'=>array('type'=>'text', 'size'=>50, 'maxlength'=>255, 'lang-multi'=>true, 'required'=>1, ),
			//'abbr'=>array('type'=>'text', 'size'=>25, 'maxlength'=>255, 'lang-multi'=>true, 'required'=>0, ),
			'comment'=>array('type'=>'textarea', 'size'=>20, 'rows'=>2, 'lang-multi'=>true, ),
			'txt'=>array('type'=>'textarea', 'size'=>20, 'rows'=>2, 'lang-multi'=>true, ),
			'disable'=>array('type'=>'checkbox', 'size'=>1, 'value'=>1, ),
			),
		'cfg'=>array(
			'layout'=>array('type'=>'text', 'size'=>50, 'maxlength'=>50, 'class'=>'form', ),
			'restrict'=>array('type'=>'text', 'size'=>50, 'maxlength'=>50, 'class'=>'form', ),
			'dir'=>array('type'=>'text', 'size'=>50, 'maxlength'=>50, 'class'=>'', ),
			'dir_entry'=>array('type'=>'text', 'size'=>50, 'maxlength'=>50, 'class'=>'form', ),
			'sql_table'=>array('type'=>'text', 'size'=>50, 'maxlength'=>50, 'class'=>'form', ),
			'mailto'=>array('type'=>'email', 'size'=>50, 'maxlength'=>255, 'class'=>'form', ),
			),
		);
	var $header=array();
	var $langs=array();
	var $module=null;
	var $form=null;
	var $value_types_auto=array('checkbox'=>'id', 'set-checkbox'=>'level', );//'button'=>'','number'=>'value','password'=>'','submit'=>'','table'=>'','text'=>'value','textarea'=>'value','timestamp'=>'value','wysiwyg'=>'value',
	function log($line, $function, $msg='') { #<Log errors />
		return axs_admin::log($file, $line, get_class($this), $function, $msg);
		} # </log()>
	# ------------------------
	
	/*function sql_header_cfg($module, $cfg_form) {
		$cfg=$this->sql_header_cfg_get($module); #<Module config />
		//$cfg_form=$this->sql_header_cfg_get($this->header, $module, $form); #<Form config />
		foreach ($cfg_form as $k=>$v) {
			if (is_array($v)) {	if (!empty($v)) $cfg[$k]=$v;	}
			else {	if (strlen($v)) $cfg[$k]=$v;	}
			}
		foreach (array('module'=>$this->module, 'form'=>$this->form, ) as $k=>$v) if (!isset($cfg[$k])) $cfg[$k]=$v;
		$cfg['sql']['table']=($cfg_form['sql_table']) ? $cfg_form['sql_table']:$this->px.$this->module.'_'.$this->form;
		return $cfg;
		}*/ #</sql_header_cfg()>
	function sql_header_cfg_get($module, $form=false, $field=false) {
		if (!$form) $cfg=$this->header[$module][''];
		else $cfg=$this->header[$module][$form][''];
		if ($field!==false) return axs_get($field, $cfg);
		return $cfg;
		} # </sql_header_cfg_get()>
	
	
	function sql_header_modules_list(&$sql_result=false, &$header=array(), $get_disabled=true) {
		if ($sql_result===false) $sql_result=axs_db_query("SELECT `id`, `nr`, `disable`, `module`, `form`, `field`, `type`, `cfg`, `attributes`, `label_".$this->l."` AS `label`, `comment_".$this->l."` AS `comment`, `msg_".$this->l."` AS `msg`, `txt_".$this->l."` AS `txt`\n".
		"	FROM `".$this->px."header` WHERE `site`='".$this->site_nr."' AND `type`='' AND `field`='cfg'".(($get_disabled)?'':' AND `disable`=0')." ORDER BY `nr`, `module`, `form`, `label`", 1, $this->db, __FILE__, __LINE__);
		#<Modules config>
		foreach ($sql_result as $rownr=>$cl) if ((!$cl['type']) && ($cl['field']==='cfg') && (!$cl['form'])) {
			if (!isset($header[$cl['module']][''])) $header[$cl['module']]['']=$this->sql_header_modules_list_cfg_entry($cl);
			unset($sql_result[$rownr]);
			} #</Modules config>
		#<Forms config>
		foreach ($sql_result as $rownr=>$cl) if ((!$cl['type']) && ($cl['field']==='cfg') && ($cl['form'])) {
			if ((isset($header[$cl['module']])) && (!isset($header[$cl['module']][$cl['form']]['']))) $header[$cl['module']][$cl['form']]['']=$this->sql_header_modules_list_cfg_entry($cl);
			unset($sql_result[$rownr]);
			}
		foreach ($header as $m=>$module) foreach ($module as $f=>$form) if ($f) {
			$parent=$module[''];
			if (!$form['']['sql_table']) {	$form['']['sql_table']=($parent['sql_table']) ? $parent['sql_table']:$this->px.$m.'_'.$f;	}
			if ($parent['restrict']) $form['']['restrict']=array_merge_recursive($parent['restrict'], $form['']['restrict']);
			foreach (array('layout'=>'', ) as $k=>$v) if (!$form[''][$k]) $form[''][$k]=$parent[$k];
			$header[$m][$f]['']=$form[''];
			}
		#</Forms config>
		return $header;
		} #</sql_header_modules_list()>
	function sql_header_modules_list_cfg_entry($cl) {
		$cl=$this->element_esc_html($cl);
		//$cfg=self::$fields_cfg[''];
		//foreach ($cfg as $k=>$v) $cfg[$k]=$cl[$k];
		$cl['cfg']=unserialize($cl['cfg']);
		foreach (self::$fields_cfg['cfg'] as $k=>$v) $cl[$k]=axs_get($k, $cl['cfg']);
		$cl['restrict']=axs_content::permission_parse($cl['restrict']);
		foreach ($cl as $k=>$v) if (strncmp($k, 'sql_', 4)===0) $cl['sql'][substr($k, 4)]=$v;
		return $cl;
		} # </sql_header_modules_list_cfg_entry()>
	function sql_header_set_module($module=false, $form=false) {
		if ($module) $this->module=$module;
		if ($form) $this->form=$form;
		if ($this->module && $this->form) return true;
		} # </set_module()>
	function sql_header_get($module='', $form='', $act_only=true, $values=array(), $cat_key=false/*, $cat_open=true*/) {
		$this->values=$values;
		$qry_levels='';
		for ($nr=1; $nr<=$this->category_levels; $nr++) $qry_levels.='`level'.$nr.'`, ';
		$act=($act_only) ? "`disable`='0' AND ":'';
		$visibility=array();
		if ($this->visibility) foreach ((array)$this->visibility as $k=>$v) $visibility[]="FIND_IN_SET('".addslashes($v)."',`visible`)";
		$visibility=($visibility) ? "(`visible`='' OR ".implode(" OR ", $visibility).") AND ":'';
		$this->sql_where_module=$act.$visibility;
		if ($module) {
			$tmp=($form) ? "'".$form."'":"(SELECT `form` FROM `".$this->px."header` WHERE `site`='".$this->site_nr."' AND `type`='' AND `field`='cfg' AND `module`='".$module."' AND `form`!='' ORDER BY `nr` LIMIT 1)";
			$this->sql_where_module.="`module`='".$module."' AND `form`=".$tmp;
			}
		if ($this->sql_where_module) $this->sql_where_module="AND ".$this->sql_where_module;
		$this->sql_header_get_element_category($cat_key);
		$this->sql_header_get_result=axs_db_query($tmp=
		"SELECT `id`, `disable`, `visible`, `module`, `form`, `fieldset`, `field`, `parent`, `type`, `size`, `size2`, `maxlength`,\n".
		"	`level`, ".$qry_levels."`nr`, `value_type`, `value`, `cfg`, `attributes`,\n".
		"	`required`, `search`,\n". "	`export`,\n".
		"	`label_".$this->l."` AS `label`, `abbr_".$this->l."` AS `abbr`, `comment_".$this->l."` AS `comment`, `msg_".$this->l."` AS `msg`, `txt_".$this->l."` AS `txt`\n".
		"	FROM `".$this->px."header`\n".
		"	WHERE \n".
		"		(`site`='".$this->site_nr."' AND `type`='' AND `field`='cfg') #cfg\n".
		"		OR\n".
		"		(`site`='".$this->site_nr."' AND `type`='_tr' AND `module`='".$module."') #tr\n".
		"		OR\n".
		"		(`site`='".$this->site_nr."' AND `type`!='' ".$this->sql_where_module.$this->sql_where_category.") #fields\n".
		"	ORDER BY `level`, `nr`, `module`, `form`, `field`, `label`", 1, $this->db, __FILE__, __LINE__);
		#exit(dbg($tmp)); exit(dbg($tmp,axs_fn::array_table($this->sql_header_get_result)));
		$f='';
		$tr=array();
		foreach ($this->sql_header_get_result as $rownr=>$cl) { #<Universal stuff for all types>
			if ($cl['type']==='_tr') { #<Extract tr />
				$tr[$cl['field']]=$cl['label'];
				unset($this->sql_header_get_result[$rownr]);
				continue;
				}
			$cl['visible']=preg_split('/,/', $cl['visible'], NULL, PREG_SPLIT_NO_EMPTY);
			if ($cl['visible']) $cl['visible']=array_combine($cl['visible'], $cl['visible']);
			if (($cl['visible']) && (!$this->_visible($cl))) continue;
			if ($cl['attributes']) {
				$cl['attributes']=explode(',', $cl['attributes']);
				foreach ($cl['attributes'] as $v) $cl[$v]=$v;
				}
			unset($cl['attributes']);
			if ($cl['cfg']) {
				if ($cl['cfg'][0]==='{') $tmp=(array)json_decode($cl['cfg'], true);
				else parse_str(str_replace(array("\n","\r"), '', $cl['cfg']), $tmp);
				foreach ($tmp as $k=>$v) {	if (!isset($cl[$k])) $cl[$k]=$v;	}
				#echo dbg($cl['cfg'], $tmp);
				}
			foreach (array('class'=>'', 'data-elements-enable'=>'', ) as $k=>$v) {	if (!empty($cl[$k])) $cl['attr'][$k]=$cl[$k];	}
			foreach (array('label','abbr','comment','msg') as $k=>$v) $cl[$v.'.html']=$cl[$v.'_html']=nl2br(axs_html_safe($cl[$v]));
			$this->sql_header_get_result[$rownr]=$cl;
			if ((!$f) && ($cl['module']===$module) && ($cl['form'])) $f=$cl['form'];
			} #</Universal stuff for all types>
		$this->sql_header_modules_list($this->sql_header_get_result, $this->header); #<Modules and forms config>
		$fields=array();
		foreach ($this->sql_header_get_result as $cl) if ($cl['level']==0) { #<Root elements>
			$fields[$cl['field']]=$this->sql_header_get_element($cl);
			} #</Root elements>
		unset($this->sql_header_get_result);
		#<Arrange fieldsets>
		$index=$fieldset=$header=array();
		foreach ($fields as $rownr=>$cl) {
			if (!$cl['fieldset'] or $cl['type']==='fieldset') $index[$cl['field']]=$cl['nr'];
			else $fieldset[$cl['fieldset']][$cl['field']]=$cl['nr'];
			}
		foreach ($index as $k=>$v) {
			$header[$k]=$fields[$k];
			if (isset($fieldset[$k])) {
				foreach ($fieldset[$k] as $kk=>$vv) $header[$kk]=$fields[$kk];
				$header['/'.$k]=array('module'=>$module, 'form'=>$form, 'fieldset'=>$k, 'type'=>'fieldset_end', );
				}
			}
		#</Arrange fieldsets>
		$this->m=$module;
		$this->f=($form) ? $form:$f;
		if (!empty($this->f)) {
			if (!isset($this->header[$module][$this->f])) $this->header[$module][$this->f]=array();
			$this->header[$module][$this->f]=array_merge($this->header[$module][$this->f], $header);
			#exit(dbg($this->header[$module][$this->f]['']));
			if ($tr) $this->header[$module][$this->f]['']['tr']=$tr;
			$this->structure_set($this->header[$module][$this->f]);
			#dbg($this->structure);
			return $this->structure_get();
			}
		} # </sql_header_get()>
	function sql_header_get_element($cl) {
		global $axs;
		$fill=false;
		if (!empty($cl['link'])) {	if (strpos($cl['link'], '=')) parse_str($cl['link'], $cl['link']);	}
		$cl['value']=$this->sql_header_get_defaultvalue($cl, $cl);
		if (isset($this->types[$cl['type']]['options'])) $cl['options']=array();
		if (!empty($cl['lang-multi'])) {
			$cl['lang-multi']=$this->langs;
			if (!empty($cl['lang'])) $cl['lang']=$this->l;
			}
		if ($cl['type']==='category') {
			if (!isset($this->category)) $this->sql_header_get_element_category($cl['field']);
			$cl['level_open']=$cl['cfg'];
			$this->category_sql_where=array();
			for ($nr=1; $nr<=$this->category_levels; $nr++) {
				$this->category_sql_where[$nr]='`'.$this->category_key.'_'.$nr.'`='.($values[$this->category_key.'_'.$nr]+0);
				if ($cl['level_open']) {	if ($nr>=$cl['level_open']) break 1;	}
				}
			$this->category_sql_where=implode(' AND ', $this->category_sql_where);
			#<Options />
			if ($cl['level1']) { # multi-level fields (like category)
				if ($cl['level']==1 or $this->values[$cl['field'].'_'.($cl['level']-1)]==$cl['level'.$cl['level']]) {
					$cl['level0']=$header[$cl['field']]['id'];
					$cl['value']=$cl['id'];
					$header[$cl['field']]['options'][$cl['level']][$cl['id']]=$cl;
					}
				$fill=array('field'=>$cl['field'], 'type'=>$cl['type']);
				}
			$level=2;
			if ($fill) while ($level<=$this->category_levels) {
				if (!isset($header[$fill['field']]['options'][$level])) $header[$fill['field']]['options'][$level]=array();
				$level++;
				}
			}
		if ($cl['type']==='file') {}
		if ($cl['type']==='number') {
			if (strlen($cl['size2'])) {	$cl['attr']['min']=$cl['size2'];	unset($cl['size2']);	}
			if (!empty($cl['maxlength'])) {	$cl['max']=$cl['maxlength'];	unset($cl['maxlength']);	}
			}
		if ($cl['type']==='password') {
			if (!empty($cl['size2'])) {	$cl['min']=$cl['size2'];	unset($cl['size2']);	}
			}
		if (($cl['type']==='radio') or ($cl['type']==='select')) {
			foreach ($this->sql_header_get_element_options($cl['id']) as $k=>$v) {
				if (!$v['size']) $v['value']='';
				else {	if (!strlen($v['value'])) $v['value']=$k;	}
				$cl['options'][$k]=$v;
				}
			}
		if ((strncmp($cl['type'], 'set-', 4)===0) or (strncmp($cl['type'], 'multi-', 6)===0)) {
			foreach ($this->sql_header_get_element_options($cl['id']) as $k=>$v) $cl['options'][$v['level']]=$v;
			if ($cl['type']==='set-checkbox') $cl['options']=$this->sql_header_element_options_sort($cl['options']);
			}
		if ($cl['type']==='table') {
			foreach ($this->sql_header_get_element_options($cl['id']) as $k=>$v) {
				//$v['scope']='col';
				$cl['options'][$v['field']]=$this->sql_header_get_element($v);
				}
			}
		if (($cl['type']==='text') or ($cl['type']==='text-between')) {
			if (!$cl['maxlength']) $cl['maxlength']=$cl['size'];
			}
		if ($cl['type']==='text-join') {
			//foreach (array('f'=>array(),'data-copy'=>'') as $kk=>$vv) if (!isset($cl[$kk])) $cl[$kk]=$vv;
			//parse_str($cl['data-copy'], $cl['data-copy']);
			#see on siin, et vältida topelt structure_set()
			if (!isset($cl['f'])) $cl['f']=array();
			foreach ($cl['f'] as $k=>$v) {
				$cl['f'][$k]['table']=($v['table']==='users') ? $axs['cfg']['site'][1]['prefix'].'users':$v['table'];
				}
			}
		if ($cl['type']==='textarea') {
			if (!empty($cl['size2'])) {	$cl['rows']=$cl['size2'];	unset($cl['size2']);	}
			}
		if (strncmp($cl['type'], 'timestamp', 9)===0) {
			//$header[$cl['field']]['format']=$cl['cfg'];
			}
		return $cl;
		} #</sql_header_get_element()>
	function sql_header_get_element_category($cat_key=false) {
		if (!$cat_key) return $this->sql_where_category='';
		
		$this->category_key=$cat_key;
		$this->category=array();
		for ($nr=1; $nr<=$this->category_levels; $nr++) {
			if (isset($this->values[$this->category_key.'_'.$nr])) $this->category[$nr]=$this->values[$this->category_key.'_'.$nr]+0;
			}
		if (!isset($this->category[1])) $this->category=array(1=>0);
		$this->category[]=0;
		if (isset($this->sql_where_category)) return;
		
		$this->sql_where_category=array();
		foreach ($this->category as $level=>$v) {
			if ($level>$this->category_levels) break;
			$tmp=array('`level`=\''.$level.'\'');
			$lvl=2;
			while ($lvl<=$level) {
				$tmp[]='`level'.$lvl.'`=\''.$this->category[$lvl-1].'\'';
				$lvl++;
				}
			$this->sql_where_category[$level]='	('.$this->sql_where_module.' AND '.implode(' AND ', $tmp).')';
			}
		$this->sql_where_category="\n".'	AND `level1`=\'0\' OR '."\n".implode(' OR '."\n", $this->sql_where_category)."\n";
		} # </sql_header_get_element_category()>
	function sql_header_get_element_options($parent) {
		$a=array();
		foreach ($this->sql_header_get_result as $rownr=>$cl) if ($cl['parent']===$parent) $a[$rownr]=$cl;
		return $a;
		} #</sql_header_get_element_options()>
	function sql_header_get_defaultvalue($el, &$cl) {
		if ((strlen($cl['value'])) or (!$cl['size'])) return $cl['value'];
		$tmp=(isset($this->value_types_auto[$el['type']])) ? $this->value_types_auto[$el['type']]:'value';
		//if ($el['type']=='select') dbg($tmp);
		return axs_get($tmp, $cl, '');
		} # </sql_header_get_defaultvalue()>
	function sql_header_category_tree_get($level=1) {
		$tree=$this->header[$this->module][$this->form][$this->category_key]['category'][$level];
		$node=array();
		foreach ($tree as $k=>$v) {
			$v['url']=array();
			for ($n=1; isset($v['level'.$n]); $n++) if ($v['level'.$n]) $v['url'][$this->category_key.'_'.$n]=$v['level'.($n+1)];
			$v['url'][$this->category_key.'_'.$v['level']]=$v['id'];
			$v['act']=axs_get($this->category_key.'_'.$v['level'], $_REQUEST)+0;
			if (is_array($tmp=$this->header[$this->module][$this->form][$this->category_key]['category'][$level+1])) {
				$tmp=current($tmp);
				if ($tmp['level'.($level+1)]==$v['id']) $v['submenu']=$this->sql_header_category_tree_get($level+1);
				}
			$node[$k]=$v;
			}
		return $node;
		} # </sql_header_category_tree_get()>
	function sql_header_element_options_sort($array, $descending=false) { #<Sort options by nr />
		$new_array=$index=array();
		foreach ($array as $k=>$v) $index[$k]=$v['nr'];
		natcasesort($index);
		foreach ($index as $k=>$v) {
			$new_array[$k]=$array[$k];
			}
		if ($descending) $new_array=array_reverse($new_array);	
		return $new_array;
		} # </sql_header_element_options_sort()>
	} #</class::axs_form_header>
#2008-04 ?>