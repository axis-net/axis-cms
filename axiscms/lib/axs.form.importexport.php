<?php #2022-02-21
class axs_form_importexport extends axs_form_edit { #<Import/export data />
	static $separator=array(
		','=>array('value'=>',', 'label'=>','), ';'=>array('value'=>';', 'label'=>';'), '	'=>array('value'=>'	', 'label'=>'(TAB)'), ' '=>array('value'=>' ', 'label'=>'(SPACE)'), '|'=>array('value'=>'|', 'label'=>'|'),
		);
	#<Export functions>
	static function export_table_csv($data, $separator=',', $header=true) {//function toCSV(array $data, array $colHeaders = array(), $asString = false) {
		reset($data);
		$stream=fopen("php://temp/maxmemory", "w+");
		if ($header) {
			if ($header===true) $header=array_keys(current($data));
			fputcsv($stream, $header, $separator);
			}
		foreach ($data as $record) fputcsv($stream, $record, $separator);
		rewind($stream);
		$returnVal=stream_get_contents($stream);
		fclose($stream);
		return $returnVal;
		/*If you want to export some utf-8 data into csv/tsv that will be readable by excel with correct encoding you must add special non vissable characters at the begining of file
		$data = "Some utf-8 characters ������"
		//these characters will make correct encoding to excel
		echo chr(255).chr(254).iconv("UTF-8", "UTF-16LE//IGNORE", $data); */
		} #</export_table_csv()>
	static function export_table_txt(&$data, $separator) {//function toCSV(array $data, array $colHeaders = array(), $asString = false) {
		$stream=fopen("php://temp/maxmemory", "w+");
		foreach ($data as $cl) fwrite($stream, implode($separator, $cl)."\n");
		rewind($stream);
		$returnVal=stream_get_contents($stream);
		fclose($stream);
		return $returnVal;
		} #</export_table_txt()>
	static function export_table_xlsx(&$data, $separator=null) {
		include_once(axs_dir('lib').'xlsxwriter.class.php');
		$writer=new XLSXWriter();
		$writer->writeSheet($data);
		return $writer->writeToString();
		} #</export_table_xlsx()>
	#</Export functions>
	
	
	#<Import functions>
	static function data_csv_import_file($f, $charset_from='', $charset_to='', $separator=',', $header='') {
		$data=array();
		if (!$handle=fopen($f, 'r')) axs_log(__FILE__, __LINE__, 'php', 'File "'.$f.'" not found!', true);
		while (($cl=fgetcsv($handle, 1000, $separator))!==false) {
			if (($charset_to) && ($charset_from!=$charset_to)) $cl=axs_valid::convert($cl, $charset_to, $charset_from);
			$data[]=$cl;
			}
		fclose($handle);
		if (empty($data)) return array();
		if (strlen($header)) {
			reset($data);
			$k=($header===true) ? key($data):$header;
			$th=$data[$k];
			unset($data[$k]);
			foreach ($data as $rownr=>$cl) {
				$row=array();
				foreach ($th as $k=>$v) $row[$v]=axs_get($k, $cl, '');
				$data[$rownr]=$row;
				}
			}
		return $data;
		} #</data_csv_import_file()>
	function import_table($data) {
		$this->sql_structure=new axs_sql_structure($this->db);
		$this->sql_cols=$this->sql_structure->table_cols($this->sql_table);
		$this->import_data=$data;
		$nr=2;
		foreach ($this->import_data as $cl) {
			if ($abort=$this->import_table_validate($nr, $cl)) break;
			$nr++;
			}
		$imported=0;
		if (empty($this->msg)) foreach ($this->import_data as $cl) {
			foreach ($cl as $k=>$v) $cl[$k]="`".addslashes($k)."`='".addslashes($v)."'";
			axs_db_query("INSERT INTO `".$this->sql_table."` SET ".implode(', ', $cl), '', $this->db, __FILE__, __LINE__);
			$imported++;
			}
		$this->msg('', 'Imported '.$imported.' row(s).');
		} #</import_table()>
	function import_table_validate(&$nr, &$cl) {
		$msg=array();
		$abort=false;
		foreach ($cl as $k=>$v) if (!isset($this->sql_cols[$k])) {
			$msg[]='Invalid column "'.axs_html_safe($k).'".';
			$abort=true;
			}
		foreach ($this->structure as $k=>$v) if ((!empty($v['unique'])) && (strlen(axs_get($k, $cl)))) {
			$found=count(axs_fn::array_find($this->import_data, $k, $cl[$k]));
			if ($found>1) $msg[]='Duplicate value "'.axs_html_safe($cl[$k]).'" ('.$found.') in UNIQUE column "'.axs_html_safe($k).'".';
			}
		$msg+=$this->validate($cl, false);
		if (!empty($msg)) $this->msg('', array('label'=>'Error(s) @ row '.$nr.' ("'.axs_html_safe(implode('|', $cl)).'")', 'msg'=>$msg));
		return $abort;
		} #</data_import_validate()>
	
	static function import_table_xml($data, $charset, $th, $opts=array()) {
		$data=self::xml2array($data, 1, 'tag');
		if (!empty($opts['path'])) {
			if (!is_array($opts['path'])) $opts['path']=explode('/', $opts['path']);
			foreach ($opts['path'] as $k=>$v) $data=axs_get($v, $data, array());
			}
		foreach ($th as $kk=>$vv) {
			if (!is_array($vv)) $th[$kk]=[explode('/', $vv)];
			else foreach ($vv as $kkk=>$vvv) $th[$kk][$kkk]=explode('/', $vvv);
			}
		foreach ($data as $k=>$v) {
			$cl=array();
			foreach ($th as $kk=>$vv) {
				$cl[$kk]=null;
				foreach ($vv as $vvv) {
					$td=$v;
					foreach ($vvv as $vvvv) {
						if (is_array($td)) {
							if (isset($td[$vvvv])) $td=$td[$vvvv];
							else {	$td=null;	break;	}
							}
						}
					if ($td!==null) {	$cl[$kk]=$td;	break;	}
					}
				}
			$data[$k]=$cl;
			}
		return $data;
		} #</import_table_xml()>
	#</Import functions>
	
	
	function pdf_init() {
		//require_once(axs_dir('plugins').'dompdf/dompdf_config.inc.php');
		//$this->pdf=new DOMPDF();
		//$this->pdf->set_protocol('file://');
		$this->pdf=require_once(axs_dir('plugins').'dompdf/axs.dompdf.php');
		} #</pdf_init()>
	/*function pdf_init() {
		require_once(axs_dir('plugins').'dompdf/autoload.inc.php');
		//use Dompdf\Dompdf;
		$this->pdf=new Dompdf();
		//$this->pdf->set_protocol('file://');
		}*/ #</pdf_init()>
	
	
	
	
	
	function axs_form_export_editform() {
		# <Initialize export>
		$this->export=axs_get('export', $_REQUEST);
		if ($this->export) {
			$this->export_tpl=array(
				'doc'=>false, 'element'=>false, 'element_content'=>false, 'element_fieldset'=>false, 'element_fieldset_end'=>false,
				'element_table'=>false,
				);
			$this->export_vr=array(
				'charset'=>$m->cfg_system_get('charset'), 'l'=>$this->l, 'path'=>$this->f_path,
				'title'=>$this->tr('form_'.$this->form.'_edit_title_lbl'), 'elements'=>'', 'time'=>date('d.m.Y H:i:s'),
				);
			$this->export_fname=axs_valid::f_name($this->export_vr['title'].'-'.$this->cl[$this->id_key].'.'.$this->export);
			if ($this->export=='pdf') {
				require_once(AXS_PATH_EXT.'dompdf/dompdf_config.inc.php');
				$this->pdf=new DOMPDF();
				$this->pdf->set_protocol('file://');
				}
			} # </Initialize export>
		} #</axs_form_export()>
	function export($formats=array('pdf'=>'', 'html'=>'', )) { # <Export>
		global $axs;
		if (!$this->export) {
			if (!is_dir(axs_dir('plugins').'dompdf')) unset($formats['pdf']);
			foreach ($formats as $k=>$v) $formats[$k]=array(
				'url'=>'?'.axs_url($this->url, array('export'=>$k), '&'),
				'label'=>$this->tr('export_'.$k.'_lbl'),
				);
			$this->tools=$formats+$this->tools;
			return;
			}
		foreach ($this->export_tpl as $k=>$v) {
			if ($v===false) {
				$v=(file_exists($this->f_path.($tmp=$this->f_px.$this->module.'_'.$this->form.'_export_'.$k.'.html'))) ? 
				$tmp:$this->f_px.'fn_export_'.$k.'.html';
				}
			$this->export_tpl[$k]=axs_tpl($this->f_path, $v);
			}
		$html='';
		foreach ($this->structure as $k=>$v) {
			$v=array_merge($v, (array)$this->header->get_element($k));
			$v['name']=$k;
			$v['input']=$this->header->get_value_text($k, $this->cl);
			$tpl=(isset($this->export_tpl['element_'.$v['type']])) ? 'element_'.$v['type']:'element';
			$this->export_vr['elements'].=axs_tpl_parse($this->export_tpl[$tpl], $v);
			}
		$html=axs_tpl_parse($this->export_tpl['doc'], $this->export_vr);
		//$html=str_replace(array('<caption','</caption>'), array('<!-- caption','</caption -->'), $html); # dompdf doesn't support <caption>
		if ($this->export=='pdf') {
			$this->pdf->load_html($html);
			$this->pdf->render();
			exit($this->pdf->stream($this->export_fname));
			}
		else exit($this->m->f_send('', $this->export_fname, $html));
		} # </export()>
	function export_file($html, $file_name) { # <Export>
		global $axs;
		$html=axs_tpl_parse($html, array('charset'=>$axs['cfg']['charset'], 'l'=>$axs['l'], 'title'=>$file_name, ));
		$format=axs_admin::f_ext($file_name);
		if ($format==='pdf') {
			$this->pdf->load_html($html);
			$this->pdf->render();
			exit($this->pdf->stream($file_name));
			}
		else exit(axs_admin::f_send('', $file_name, $html));
		} #</export_file()>
	
	/**
	 * xml2array() will convert the given XML text to an array in the XML structure.
	 * Link: http://www.bin-co.com/php/scripts/xml2array/
	 * Arguments : $contents - The XML text
	 *                $get_attributes - 1 or 0. If this is 1 the function will get the attributes as well as the tag values - this results in a different array structure in the return value.
	 *                $priority - Can be 'tag' or 'attribute'. This will change the way the resulting array sturcture. For 'tag', the tags are given more importance.
	 * Return: The parsed XML in an array form. Use print_r() to see the resulting array structure.
	 * Examples: $array =  xml2array(file_get_contents('feed.xml'));
	 *              $array =  xml2array(file_get_contents('feed.xml', 1, 'attribute'));
	 */
	static function xml2array($contents, $get_attributes=1, $priority='tag') {
		if(!$contents) return array();

		if(!function_exists('xml_parser_create')) {
			//print "'xml_parser_create()' function not found!";
			return array();
		}

		//Get the XML parser of PHP - PHP must have this module for the parser to work
		$parser = xml_parser_create('');
		xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, "UTF-8"); # http://minutillo.com/steve/weblog/2004/6/17/php-xml-and-character-encodings-a-tale-of-sadness-rage-and-data-loss
		xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
		xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
		xml_parse_into_struct($parser, trim($contents), $xml_values);
		xml_parser_free($parser);

		if(!$xml_values) return;//Hmm...

		//Initializations
		$xml_array = array();
		$parents = array();
		$opened_tags = array();
		$arr = array();

		$current = &$xml_array; //Refference

		//Go through the tags.
		$repeated_tag_index = array();//Multiple tags with same name will be turned into an array
		foreach($xml_values as $data) {
			unset($attributes,$value);//Remove existing values, or there will be trouble

			//This command will extract these variables into the foreach scope
			// tag(string), type(string), level(int), attributes(array).
			extract($data);//We could use the array by itself, but this cooler.

			$result = array();
			$attributes_data = array();
			
			if(isset($value)) {
				if($priority == 'tag') $result = $value;
				else $result['value'] = $value; //Put the value in a assoc array if we are in the 'Attribute' mode
			}

			//Set the attributes too.
			if(isset($attributes) and $get_attributes) {
				foreach($attributes as $attr => $val) {
					if($priority == 'tag') $attributes_data[$attr] = $val;
					else $result['attr'][$attr] = $val; //Set all the attributes in a array called 'attr'
				}
			}

			//See tag status and do the needed.
			if($type == "open") {//The starting of the tag '<tag>'
				$parent[$level-1] = &$current;
				if(!is_array($current) or (!in_array($tag, array_keys($current)))) { //Insert New tag
					$current[$tag] = $result;
					if($attributes_data) $current[$tag. '_attr'] = $attributes_data;
					$repeated_tag_index[$tag.'_'.$level] = 1;

					$current = &$current[$tag];

				} else { //There was another element with the same tag name

					if(isset($current[$tag][0])) {//If there is a 0th element it is already an array
						$current[$tag][$repeated_tag_index[$tag.'_'.$level]] = $result;
						$repeated_tag_index[$tag.'_'.$level]++;
					} else {//This section will make the value an array if multiple tags with the same name appear together
						$current[$tag] = array($current[$tag],$result);//This will combine the existing item and the new item together to make an array
						$repeated_tag_index[$tag.'_'.$level] = 2;
						
						if(isset($current[$tag.'_attr'])) { //The attribute of the last(0th) tag must be moved as well
							$current[$tag]['0_attr'] = $current[$tag.'_attr'];
							unset($current[$tag.'_attr']);
						}

					}
					$last_item_index = $repeated_tag_index[$tag.'_'.$level]-1;
					$current = &$current[$tag][$last_item_index];
				}

			} elseif($type == "complete") { //Tags that ends in 1 line '<tag />'
				//See if the key is already taken.
				if(!isset($current[$tag])) { //New Key
					$current[$tag] = $result;
					$repeated_tag_index[$tag.'_'.$level] = 1;
					if($priority == 'tag' and $attributes_data) $current[$tag. '_attr'] = $attributes_data;

				} else { //If taken, put all things inside a list(array)
					if(isset($current[$tag][0]) and is_array($current[$tag])) {//If it is already an array...

						// ...push the new element into that array.
						$current[$tag][$repeated_tag_index[$tag.'_'.$level]] = $result;
						
						if($priority == 'tag' and $get_attributes and $attributes_data) {
							$current[$tag][$repeated_tag_index[$tag.'_'.$level] . '_attr'] = $attributes_data;
						}
						$repeated_tag_index[$tag.'_'.$level]++;

					} else { //If it is not an array...
						$current[$tag] = array($current[$tag],$result); //...Make it an array using using the existing value and the new value
						$repeated_tag_index[$tag.'_'.$level] = 1;
						if($priority == 'tag' and $get_attributes) {
							if(isset($current[$tag.'_attr'])) { //The attribute of the last(0th) tag must be moved as well
								
								$current[$tag]['0_attr'] = $current[$tag.'_attr'];
								unset($current[$tag.'_attr']);
							}
							
							if($attributes_data) {
								$current[$tag][$repeated_tag_index[$tag.'_'.$level] . '_attr'] = $attributes_data;
							}
						}
						$repeated_tag_index[$tag.'_'.$level]++; //0 and 1 index is already taken
					}
				}

			} elseif($type == 'close') { //End of tag '</tag>'
				$current = &$parent[$level-1];
			}
		}
		
		return($xml_array);
		} #</xml2array()>
	} #</axs_form_export()>
#2012-12-01 ?>