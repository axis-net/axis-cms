//2017-11-08
axs.auction={
	interval:60000,
	selector:'',
	init:function(selector){
		this.selector=selector;
		window.setTimeout(axs.auction.refresh,this.interval);
		},//</init()>
	refresh:function(){
		axs.ajax(axs.url(window.location.href,{"axs%5Baxs_auction%5D":'ajax'}),function(status,data){
			if(status==200){
				data=JSON.parse(data.responseText);
				for(k in data){
					var el=document.querySelector(axs.auction.selector+k);
					if (!el) continue;
					axs.class_rem(el,['leading','losing','ending']);
					if (data[k]['class']) axs.class_add(el,Object.values(data[k]['class']));
					var node=el.querySelector('.msg_placeholder');
					if (node) node.innerHTML=data[k]['msg'];
					for(kk in data[k].form){
						var node=el.querySelector('input[name="'+kk+'"]');
						if (node) for(kkk in data[k].form[kk]) node.setAttribute(kkk,data[k].form[kk][kkk]);
						}
					if (data[k]['disabled']) {
						var node=el.querySelectorAll('input[name="bid_next"], input[name="bid_make"]');
						if (node) for(var i=0, l=node.length; i<l; i++) node[i].setAttribute('disabled','disabled');
						}
					for(kk in data[k].html){
						var node=el.querySelector('.'+kk+'.axs_auction');
						if (node) {
							node.innerHTML=data[k].html[kk];
							if (data[k].datetime[kk]) node.setAttribute('datetime',data[k].datetime[kk]);
							}
						}
					}
				//console.log(data);
				}
			window.setTimeout(axs.auction.refresh,axs.auction.interval);
			});
		}//</refresh()>
	}//</class::axs.auction>
//2010-09-28