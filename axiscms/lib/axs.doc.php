<?php #2016-06-29
class axs_doc {
	var $templates='';
	function __construct($f, $l, $code_indent=' ', $close_level=0, $url=array(), $s=array()) {
		$this->data=array();
		$this->code_indent=$code_indent;
		$this->close_level=$close_level;
		$this->url_root='?';
		$this->url=$url;
		$this->url_key='s';
		$this->link_top=''; //'<p class="top dontprint"><a href="#">&nbsp;&uarr;</a></p>';
		$this->file_suffix='.doc.';
		$this->section=array();
		for ($nr=1; $this->close_level>=$nr; $nr++) if (isset($s[$this->url_key.$nr])) $this->section[$nr]=$s[$this->url_key.$nr];
		$this->l=$l;
		if ($f) foreach ($f as $k=>$v) {
			$tmp=$this->data_list($v['dir'], axs_get('px', $v, ''), $k);
			foreach ($tmp as $kk=>$vv) $this->data_set($vv['f'], $vv['l'], axs_get('section', $v));
			}
		}#</axs_doc()>
	function data_list($dir, $prefix, $editor_name=false) {
		$end=str_replace(array('.','$'), array('\.','\$'), $this->file_suffix);
		$list=array();
		foreach (axs_admin::f_list($dir, '/^'.$prefix.'.+'.$end.'..\.php$/') as $k=>$v) { # <Read directory cycle>
			$k=preg_replace('/^'.$prefix.'|'.$end.'..\.php$/', '', $v);
			if ($editor_name!==false) {	if ($k!=$editor_name) continue;	}
			$list[$k]['f']=$dir.$prefix.$k.$this->file_suffix;
			$l=preg_replace('/^'.$prefix.$k.$end.'|\.php$/', '', $v);
			$list[$k]['l'][$l]=$l;
			}#</Read directory cycle>
		return $list;
		}#</data_list()>
	function data_load($f, $l=array(), $section=false) { #<Load a single file />
		if (empty($l)) $l=$this->l;
		$l=array_unique(array_merge((array)$this->l, (array)$l));
		$data=array();
		foreach ((array)$l as $lang) {
			$lang=$lang[0].$lang[1];
			if (file_exists($tmp=$f.$lang.'.php')) {
				$data=(array)include($tmp);
				if ($section!==false && $section!==null) {
					foreach ((array)$section as $v) {
						$data=(is_array($data[$v]['content'])) ? $data[$v]['content']:$data[$v];
						}
					$data=array($v=>$data);
					}
				foreach ($data as $k=>$v) $data[$k]['lang']=$lang;
				break;
				}
			}
		return $data;
		}#</data_load()>
	function data_set($f, $l=array(), $section=false, $clear=false) {
		$l=array_unique(array_merge((array)$this->l, (array)$l));
		if ($clear) $this->data=array();
		$data=$this->data_load($f, $l, $section);
		foreach ($data as $k=>$v) if (!isset($this->data[$k])) {
			if (!isset($v['lang'])) $v['lang']=$lang;
			$this->data[$k]=$v;
			}
		reset($this->data);
		if (!$this->close_level) $this->section_current=false;
		else {
			$this->section_current=(isset($this->data[axs_get($this->close_level, $this->section)])) ? $this->section[$this->close_level]:key($this->data);
			}
		}#</data_set()>
	function contents_html($space=' ', $chapter='', $data=false, $level=1, $parent='') {
		$tmp=$this->code_indent.str_repeat($space, $level-1);
		$class=($level==1) ? $class=' class="contents"':'';
		$txt=$tmp.'<ul'.$class.'>'."\n";
		$nr=$a=1;
		if (!is_array($data)) $data=&$this->data;
		foreach ($data as $k=>$v) {
			$ch='';
			if ($level==3) $ch=$nr;
			if ($level>3) $ch=$chapter.'.'.$nr;
			$chtrail=($level==3) ? '.':'';
			if ($ch) $chtrail.='&nbsp;';
			$url=($this->close_level==$level) ? $this->url_root.axs_url($this->url, array($this->url_key.$level=>$k)):'#'.$parent.$k;
			$v['title']=$ch.$chtrail.$v['title'];
			if ($k==$this->section_current) {
				$class=' class="act"';
				$v['title']='<em>'.$v['title'].'</em>';
				}
			else $class='';
			$v['lang']=(!empty($v['lang'])) ? ' lang="'.$v['lang'].'"':'';
			$v['title']=$tmp.$space.'<li'.$v['lang'].$class.'><a href="'.$url.'" accesskey="'.$a.'">'.$v['title'].'</a>';
			if (is_array($v['content'])) {
				$open=true;
				if ($this->close_level==$level) {
					$open=((!empty($this->section)) && ($this->section[$this->close_level]==$k)) ? true:false;
					}
				if ($open) $v['title'].="\n".$this->contents_html($space, $ch, $v['content'], $level+2, $parent.$k.'_').$tmp.$space;
				}
			$txt.=$v['title'].'</li>'."\n";
			$nr++;
			($a<9) ? $a++:$a=0;
			}
		return $txt.$tmp.'</ul>'."\n";
		} # </contents_html()>
	function content_html($chapter='', $data=false, $level=1, $parent='') {
		$txt=$ch='';
		$nr=1;
		if (!is_array($data)) $data=&$this->data;
		if ($this->section_current!==false) {
			if ($level==$this->close_level) $data=array($this->section_current=>$data[$this->section_current]);
			}
		foreach ($data as $k=>$v) {
			if ($level==2) $ch=$nr;
			if ($level>2) $ch=$chapter.'.'.$nr;
			$chtrail=($level==2) ? '.':'';
			if ($ch) $chtrail.='&nbsp;';
			$v['lang']=(!empty($v['lang'])) ? ' lang="'.$v['lang'].'"':'';
			$v['title']=$this->code_indent.'<h'.$level.$v['lang'].' id="'.$parent.$k.'">'.$ch.$chtrail.$v['title'].'</h'.$level.'>'."\n";
			$v['ver']=(isset($v['ver'])) ? $this->code_indent.'<p class="ver">'.$v['ver'].'</p>'."\n":'';
			if (is_array($v['content'])) $v['content']=$this->content_html($ch, $v['content'], $level+1, $parent.$k.'_');
			else $v['content'].="\n".$this->code_indent.$this->link_top."\n";
			$txt.=$v['title'].$v['ver'].$v['content'];
			$nr++;
			}
		return $txt;
		}#</content_html()>
	}#</class::axs_doc>

#2008-05 ?>