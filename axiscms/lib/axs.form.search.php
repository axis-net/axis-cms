<?php #2020-12-11
class axs_form_search extends axs_form_edit {
	var $search_types=array(
		1=>array('fn'=>'', 'type'=>'', 'description'=>'Auto'),
		2=>array('fn'=>'q_like', 'type'=>'text', 'description'=>"col LIKE 'str'"),
		3=>array('fn'=>'q_like_begin', 'type'=>'text', 'description'=>"col LIKE 'str%'"),
		4=>array('fn'=>'q_equals', 'type'=>'text', 'description'=>"col='str'"),
		5=>array('fn'=>'q_multi_bool_or', 'type'=>'multi-checkbox', 'description'=>"col1 OR col2 OR col3"),
		6=>array('fn'=>'q_bool', 'type'=>'select', 'description'=>"col | !col"),
		#7=>array('fn'=>'group_equals', 'type'=>'multi-checkbox', 'description'=>"col='x' OR col='y'"),
		8=>array('fn'=>'q_text_join_like_begin', 'type'=>'text-join', 'description'=>"t2.col LIKE 'str%'"),
		#9=>array('fn'=>'text_join_like', 'type'=>'text', 'description'=>"t2.col LIKE '%str%'"),
		10=>array('fn'=>'q_equals', 'type'=>'select', 'description'=>"col='x'"),
		11=>array('fn'=>'q_multi_to_single', 'type'=>'select', 'description'=>"col1 | col2 ; FIND_IN_SET('b',col)>0"),
		12=>array('fn'=>'q_timestamp', 'type'=>'timestamp-between', 'description'=>"col BETWEEN '2631536' AND '4738746387'"),
		13=>array('fn'=>'q_timestamp_between', 'type'=>'timestamp-between', 'description'=>"col_1 BETWEEN '2' AND '4' AND col_2 BETWEEN '2' AND '4'"),
		14=>array('fn'=>'q_between', 'type'=>'text-between', 'description'=>"col BETWEEN '1' AND '5'"),
		15=>array('fn'=>'q_between2', 'type'=>'text-between', 'description'=>"col_1 BETWEEN '1' AND '5' AND col_2 BETWEEN '1' AND '5'"),
		16=>array('fn'=>'q_multi_bool_or', 'type'=>'set-checkbox', 'description'=>"FIND_IN_SET('b',col)>0"),
		17=>array('fn'=>'q_equals_multi_and', 'type'=>'category', 'description'=>"col_1='x' AND col_2='y' AND col_3='z'"),
		18=>array('fn'=>'q_single_to_multi', 'type'=>'set-checkbox', 'description'=>"col='x' OR col='y' OR col='z'"),
		19=>array('fn'=>'q_like_mid', 'type'=>'text', 'description'=>"col LIKE '%str%'"),
		);
	var $auto_types=array(
		'button'=>6, 'category'=>17, 'checkbox'=>6, 'multi-checkbox'=>5, /*'multi-level'=>,*/ 'number'=>14, 'radio'=>10, 'select'=>10, 
		'set-checkbox'=>16, 'submit'=>6, 'text-between'=>15, 'text-join'=>8, 'timestamp'=>12, 'timestamp-between'=>13, 'timestamp-updated'=>12, 
		);
	var $form_id='form-search';
	var $export_table=false;
	var $css_path=true;
	var $search_count=0;
	function __construct($structure=array(), $values=array(), $url=array(), $export=true, $psizes=array(25,50,100,250,500,1000,1), $visibility=array()) {
		global $axs;
		$this->p=new stdClass;
		$this->p->structure=$structure;
		if (isset($values['axs']['search'])) $axs['get'][$this->key_form_id]=$this->form_id;
		axs_form::__construct(array('site_nr'=>1, 'url'=>$url, 'structure'=>false, 'dir'=>'', 'user_input'=>$values, 'form_id'=>$this->form_id, ));
		$this->visibility=$visibility;
		//$this->submit=(isset($values['s']['search'])) ? true:false;
		$this->name='search_form';
		$this->action='';
		$this->f_path=dirname(__FILE__).'/';
		$this->url=$url;
		$this->export=$export;
		$this->elements=$this->sort_cols=$this->sql=$this->sql_val=$this->templates=array();
		foreach ($structure as $k=>$v) {
			if (!$this->_visible($v)) continue;
			if (!empty($v['sort'])) $this->sort_cols[$k]=$v;
			if (empty($v['search'])) continue;
			unset($v['disabled'], $v['readonly'], $v['required'], $v['value']);
			//if (!empty($v['lang'])) unset($v['lang-multi']);
			if ((!empty($v['lang'])) && (!empty($v['lang-multi']))) $v['lang-multi']=array($v['lang']=>$v['lang-multi'][$v['lang']]);
			# Determine search function and form element type
			if (!isset($this->search_types[$v['search']])) $v['search']=1;
			if ($v['search']==1) { # auto
				$v['search']=(isset($this->auto_types[$v['type']])) ? $this->auto_types[$v['type']]:2;
				}
			$v['type_orig']=$v['type'];
			$v['type']=(isset($this->search_types[$v['search']]['type'])) ? $this->search_types[$v['search']]['type']:'text';
			#<Element type conversion>
			//if ($v['type_orig']!=$v['type']) $v['options']=$v[$v['type_orig']];
			switch ($v['type']) {
				case 'multi-checkbox':
				case 'set-checkbox':
					foreach ($v['options'] as $kk=>$vv) {
						unset($v['options'][$kk]['id']);
						if ((empty($vv['size'])) && (($vv['label']==='-') || ($vv['label']===''))) unset($v['options'][$kk]);
						}
					$this->structure[$k]['options']['-0']=$v['options']['-0']=array('value'=>'-0', 'label'=>'-', 'label.html'=>'-');
					break;
				case 'select':
					if ($v['type_orig']==='checkbox') $v['value_type']='s';
					if (!isset($v['options'])) $v['options']=array();
					if (!isset($v['options'][''])) $v['options']=array(''=>array('value'=>'', 'label'=>'-', 'label.html'=>'-'))+$v['options'];
					break;
				case 'text-between':
					$v['separator']=$this->types[$v['type']]['separator'];
					break;
				} #</switch>
			$this->elements[$k]=$v;
			}
		$this->structure_set($this->elements);
		$this->vl=$this->form_input($values);
		foreach ($this->structure as $k=>$v) if ($k) {
			switch ($v['search']) {
				case 6: # q_bool()
					//if ($this->vl[$k] && $this->vl[$k]!=='-') $this->vl[$k]='+';
					$v['options']=array(
						'-'=>array('value'=>'-', 'label'=>'', 'label.html'=>'', ), '1'=>array('value'=>'1', 'label'=>'+', 'label.html'=>'+', ), '0'=>array('value'=>'0', 'label'=>'-', 'label.html'=>'-', ),
						);
					break;
				case 11: # q_multi_to_single()
					$this->vl[$k]='';
					if (strlen($values[$k])) $this->vl[$k]=$values[$k];
					$tmp=array(''=>array('value'=>'', 'label'=>'', ));
					foreach ($v['options'] as $kk=>$vv) {
						$tmp[$kk]=array('value'=>$kk, 'label'=>$vv['label'], );
						}
					$v['options']=$tmp;
					break;
				case 12: # timestamp()
				case 13: # timestamp-between()
				case 14: # text-between()
				//case 15: # text-between2()
					//foreach (array(1,2) as $nr) $this->vl[$k.'_'.$nr]=$values[$k.'_'.$nr];
					break;
				} #</switch>
			#</Element type conversion>
			unset($v['value']);
			$this->structure[$k]=$v;
			}
		//$this->structure_set($this->elements);
		foreach(array('_order'=>'', 'p'=>'', 'ps'=>'', ) as $k=>$v) if (empty($values[$k])) $values[$k]='';
		$this->url['_order']=$values['_order'];
		$this->psizes=$psizes;
		$this->pager=new axs_pager($values['p'], $this->psizes, $values['ps']);
		$this->input_type_text_join_browse();
		} #</__construct()>
	function log($function, $line, $msg='') { # log errors
		axs_log(__FILE__, __LINE__, 'class', $msg=$function.'()@'.get_class($this).': '.$msg.' (line '.$line.')');
		return $msg;
		}
	function esc($str) {	return addslashes($str);	}
	static function f_ext($f) { # function to get file extension
		$tmp=explode('.', $f);
		return (strpos(' '.$f, '.')) ? strtolower(array_pop($tmp)):'';
		} # </f_ext()>
	
	function elements($default_table='', $tables=array()) {
		$t=($default_table) ? '"'.$default_table.'".':'';
		foreach ($this->structure as $k=>$v) if ($k) {
			#if (!$this->_visible($v)) {	unset($this->structure[$k], $this->sort_cols[$k]);	continue;	}
			$this->structure[$k]['table']=$t;
			if (!empty($v['options'])) foreach ($v['options'] as $kk=>$vv) if (is_array($vv)) unset($this->structure[$k]['options'][$kk]['id']); #<Remove id from options />
			}
		foreach ($tables as $k=>$v) foreach ($v as $vv) $this->structure[$vv]['table']='`'.$k.'`.';
		#<Make querys />
		if ($this->submit) foreach ($this->structure as $k=>$v) if (($k) && (!empty($v['size']))) {
			$filled=$this->validate_element_filled($k, $v, $this->vl);
			if (!empty($this->vl[$k.'_text'])) $filled=true;
			if ($filled) {
				$fn=$this->search_types[$v['search']]['fn'];
				# Validate function choice
				if (!is_callable(array($this, $fn))) $fn='q_like';
				$sql=array();
				if (empty($v['lang-multi'])) $sql[$k]=axs_get($k, $this->vl);
				else foreach ($v['lang-multi'] as $kk=>$vv) if (strlen(axs_get($k.'_'.$kk, $this->vl))) $sql[$k.'_'.$kk]=$this->vl[$k.'_'.$kk];
				foreach ($sql as $kk=>$vv) call_user_func(array($this, $fn), $v['table'], $kk, $vv);
				$this->search_count++;
				}
			}
		//$this->url['desc']=axs_get('desc', $_GET);//($this->sort_desc) ? 1:'';
		$this->url['ps']=$this->pager->psize;
		$this->url[urlencode('axs[search]')]='';
		} #</elements()>
	
	#<Export functions>
	function export($data) {
		if (!empty($this->export_table)) $this->export_table($data, $this->export_settings);
		} #</export()>
	function export_data_proc($cols, &$data, $format, $thead=false) {
		$table=array();
		if ($thead) {
			$table[0]=$cols;
			foreach ($table[0] as $k=>$v) {
				switch ($format) {
					case '0':	$table[0][$k]=$v;	break;
					case 'html':
					case 'txt':
					default:	$table[0][$k]=$this->p->structure[$v]['label'];
					}
				if ($format==='html') $table[0][$k]=axs_html_safe($table[0][$k]);
				}
			}
		$nr=1;
		foreach ($data as $cl) {
			$table[$nr]=$vr=array();
			foreach ($cols as $k=>$v) {
				$table[$nr][$v]=axs_get($v, $cl);
				if ($format) $table[$nr][$v]=$this->value_display($v, $this->p->structure[$v], $cl, $vr, $tmp=($format==='html'));
				}
			$nr++;
			}
		return $table;
		} #</export_data_proc()>
	function export_table(&$data, $cfg) {
		$data=$this->export_data_proc($cfg['cols'], $data, $cfg['data_proc'], $cfg['header']); #<Prepare data />
		$format=explode('-', $cfg['file_format']); #<Convert data>
		switch ($format[0]) {
			case 'xlsx':
				$type='application/xlsx';
				if (empty($cfg['separator'])) $cfg['separator']='	';
				$data=axs_form_importexport::export_table_xlsx($data, $cfg['separator']);
				break;
			case 'txt':
				$type='text/plain';
				if (empty($cfg['separator'])) $cfg['separator']='	';
				$data=axs_form_importexport::export_table_txt($data, $cfg['separator']);
				break;
			case 'csv':
			default:
				$type='application/csv';
				if (empty($cfg['separator'])) $cfg['separator']=(axs_get(1, $format)==='excel') ? ';':',';
				$data=axs_form_importexport::export_table_csv($data, $cfg['separator']);
				$format[0]='csv';
				break;
			} #</Convert data>
		#<Send file />
		exit(axs_admin::f_send(false, 'export-'.date('YmdHis').'.'.$format[0], $data, array('Content-type'=>$type)));
		} #</export_table()>
	function export_ui() {
		if (empty($this->export)) return '';
		if (empty($this->user_input['axs'][$this->key_search]['export'])) return '<input name="axs['.$this->key_search.'][export][open]" type="submit" value="export &gt;" lang="en" />';
		return $this->export_ui_el($this->key_search, $this->user_input);
		} #</export_ui()>
	function export_ui_el($key, $user_input) {
		$this->export_table=(isset($user_input['axs'][$key]['export']['table']));
		$this->export_settings=array('cols'=>array(), );
		$f=array(
			'export_container'=>array('type'=>'fieldset', 'label'=>'export', 'lang'=>'en', 'class'=>'export', ),
			'export_columns'=>array('type'=>'fieldset', 'label'=>'columns', 'class'=>'columns', ),
			);
		$count=0;
		$cols=array(''=>array('value'=>'', 'label'=>'', 'export'=>'', ), );
		foreach ($this->p->structure as $k=>$v) if ($k) {
			$cols[$k]=array('value'=>$k, 'label'=>$v['label'], 'export'=>axs_get('export', $v), );
			$count++;
			}
		for ($nr=1; $nr<=$count; $nr++) {
			if (!isset($user_input['col-'.$nr])) foreach ($cols as $k=>$v) if ($v['export'].''===$nr.'') $user_input['col-'.$nr]=$k;
			$f['col-'.$nr]=array('type'=>'select', 'label'=>$nr, 'options'=>$cols, );
			if (($this->export_table) && (!empty($user_input['col-'.$nr]))) $this->export_settings['cols'][$nr]=$user_input['col-'.$nr];
			}
		$f+=array(
			'export_columns/'=>array('type'=>'fieldset_end', ),
			'file_format'=>array('type'=>'select', 'label'=>'file format', 'options'=>array(
				'xlsx'=>array('value'=>'xlsx', 'label'=>'XLSX (Excel)'),
				'csv-standard'=>array('value'=>'csv-standard', 'label'=>'CSV (standard)'),
				'csv-excel'=>array('value'=>'csv-excel', 'label'=>'CSV (Excel)'),
				'txt'=>array('value'=>'txt', 'label'=>'TXT'),
				)),
			'separator'=>array('type'=>'select', 'label'=>'separator', 'options'=>array(
				''=>array('value'=>'', 'label'=>''),
				)+axs_form_importexport::$separator),
			'header'=>array('type'=>'checkbox', 'label'=>'add table header', 'value'=>1, ),
			'data_proc'=>array('type'=>'select', 'label'=>'data format', 'options'=>array(
				'txt'=>array('value'=>'txt', 'label'=>'text/plain'),
				'html'=>array('value'=>'html', 'label'=>'text/HTML'),
				'0'=>array('value'=>'0', 'label'=>'raw data unprocessed', 'disabled'=>'disabled', ),
				)),
			'limit'=>array('type'=>'text-between', 'label'=>'rows (from to)', 'size'=>5, 'separator'=>$this->types['text-between']['separator'], ),
			'export'=>array('type'=>'submit', 'label'=>'export', 'name'=>'axs['.$key.'][export][table]', ),
			'export_container/'=>array('type'=>'fieldset_end', ),
			);
		$user_input['header']=1;
		foreach ($f as $k=>$v) {
			$v=$this->element_esc_html($v);
			$f[$k]=$this->element_html($k, $v, $user_input);
			$this->export_settings[$k]=axs_get($k, $user_input);
			}
		foreach (array(1,2) as $v) $this->export_settings['limit_'.$v]=axs_get('limit_'.$v, $this->user_input, '');
		if ($this->export_table) {
			$this->pager->start=(intval($this->export_settings['limit_1']>=1)) ? intval($this->export_settings['limit_1'])-1:0;
			$this->pager->psize=($this->export_settings['limit_2']>=1) ? $this->export_settings['limit_2']-$this->start:'1000000';
			}
		return implode('', $f);
		} #</export_ui_el()>
	#</Export functions>
	
	function sql_result_set($data, &$search=false) { #DEPRECATED!
		$this->found_rows=
		$this->sql_found_rows=axs_db_query('', 'found_rows', $this->db);
		if (!empty($this->export_table)) $this->export_table($data, $this->export_settings);
		return $this->sql_result=$data;
		} #</sql_result_set()>
	function sql_where($add='WHERE ') {
		$sql=($this->sql) ? $add:'';
		return $sql.implode(' AND ', $this->sql);
		}
	function sql_order($pre='', $t='', $structure=false) {
		return axs_form::sql_order($pre, $t, $this->p->structure);
		} #</sql_order()>
	function url($url=array()) {
		return array_merge($url, $this->url);
		}
	
	function pager($total, $url, &$vr) {
		if ($total) {
			$vr['found']=' id="found"';
			$vr['pages']=$this->pager->pages($total, $url.axs_url($this->url).'&amp;p=');
			$vr['prev']=$vr['pages']['prev'];
			$vr['current']=$vr['pages']['current'];
			$vr['next']=$vr['pages']['next'];
			$vr['pages']=$vr['pages']['pages'];
			}
		else $vr['found']=$vr['prev']=$vr['current']=$vr['next']=$vr['pages']='';
		} #</pager()>
	
	#<Form>
	function form($css_path=null, $tr=array()) {
		global $axs;
		foreach (array(
			'form'=>'axs.form.search', 'row'=>'axs.form.search.row',
			) as $k=>$v) if (!isset($this->templates[$k])) $this->templates[$k]=axs_tpl($this->f_path, $v.'.tpl');
		$vr=array('id'=>$this->name, 'action'=>$this->action, 'elements1'=>array(), 'elements'=>'', '_order'=>'', );
		$skip=array_merge(array_keys($this->vl), array('_order',/*'desc',*/'ps',));
		foreach ($this->url as $k=>$v) {
			//$k=urldecode($k);
			if (!in_array($k, $skip)) $vr['elements1'][$k]=$v;
			}
		$vr['elements1']=self::form_action_method_get($vr['elements1']);
		//dbg($this->url,$vr['elements1']);
		//$row=array();
		foreach ($this->structure as $k=>$v) if ($k) {
			unset($v['comment'], $v['txt']);
			if ($v['type']==='text') $v['size']=20;
			unset($v['maxlength']);
			if ((!empty($v['lang'])) && (!empty($v['lang-multi']))) $v['lang-multi']=array($v['lang']=>$v['lang-multi'][$v['lang']]);
			$vr['elements'].=$this->element_html($k, $v, $this->vl);
			//$v['input']=$this->element_input_html($k, $v, $this->vl);
			/*$row[]=$this->element_html($k, $v, $this->vl);
			if (count($row)==2) {
				$vr['elements'].=axs_tpl_parse($this->templates['row'], array('elements'=>implode('', $row)));
				$row=array();
				}*/
			unset($vr['elements1'][$k]);
			if ($v['type']==='multi-checkbox') foreach ($v['options'] as $kk=>$vv) unset($vr['elements1'][$k.'_'.$kk]);
			if ($v['type']==='set-checkbox') foreach ($v['options'] as $kk=>$vv) unset($vr['elements1'][$k.'['.$kk.']']);
			}
		//if (!empty($row)) $vr['elements'].=axs_tpl_parse($this->templates['row'], array('elements'=>implode('', $row)));
		$vr['elements1']=implode('', $vr['elements1']);
		if (!empty($this->sort_cols)) {
			$tmp=array('type'=>'select', 'options'=>array(''=>array('value'=>'', 'label'=>'', )));
			foreach ($this->sort_cols as $k=>$v) {
				if (!isset($v['label'])) dbg($k, $v);
				$tmp['options'][$k.'.asc']=array('value'=>$k.'.asc', 'label'=>$v['label'].' <', );
				$tmp['options'][$k.'.dsc']=array('value'=>$k.'.dsc', 'label'=>$v['label'].' >', );
				}
			$tmp=$this->element_esc_html($tmp);
			$vr['_order'].=
			'<label for="_order">{$search.order.lbl}</label>'.
			$this->input_type_select_html(' id="_order"', '_order', axs_get($this->key_order, $this->user_input), $tmp);
			//'<label for="desc" lang="en" class="visuallyhidden">order direction</label>'.
			//$this->input_type_select_html(' id="desc"', 'desc', $this->url['desc'], array('type'=>'select', 'options'=>array(
			//		''=>array('value'=>'', 'label'=>'', 'label.html'=>'', ),
			//		0=>array('value'=>'0', 'label'=>'<', 'label.html'=>'&lt;', ),
			//		1=>array('value'=>'1', 'label'=>'>', 'label.html'=>'&gt;', ),
			//		)));
			if (count($this->psizes)>1) {
				$tmp=array('type'=>'select', 'options'=>array(), );
				foreach ($this->psizes as $k=>$v) $tmp['options'][$v]=array('value'=>$v, 'label'=>$v, 'label.html'=>$v, );
				$vr['_order'].=
				'<label for="ps" lang="en" class="visuallyhidden">page size</label>'.
				$this->input_type_select_html(' id="ps"', 'ps', $this->pager->psize, $tmp);
				}
			}
		$vr['export']=$this->export_ui('axs[search]');
		$this->css_js(true, true);
		if ($css_path!==null) $this->css_path=$css_path;
		if ($this->css_path===true) $this->css_path=axs_dir('lib', 'h');
		if ($this->css_path!==false) $axs['page']['head']['axs.form.search.css']='<link href="'.$this->css_path.'axs.form.search.css" rel="stylesheet" type="text/css" media="screen" />'."\n";
		return axs_tpl_parse($this->templates['form'], $vr+$tr);
		} #</form()>
	static function form_action_method_get($url, $n=false, &$el=array(), $lvl=1) {
		foreach ($url as $k=>$v) {
			$key=($n) ? $n.'['.$k.']':$k;
			if (is_array($v)) self::form_action_method_get($v, $key, $el, $lvl+1);
			else $el[$key]='<input type="hidden" name="'.htmlspecialchars($key).'" value="'.htmlspecialchars($v).'" />';
			}
		if ($lvl===1) return $el;
		} #</form_action_method_get()>
	
	#<Query functions>
	function q($key, $val) { #Register a parameter for using in a prepared statement and return the corresponding placeholder.
		$this->sql_val[':'.$key]=$val;
		return ':'.$key;
		} #</q()>
	function q_empty_val($t, $colname, $string) { # function to make empty string query
		$string=str_replace(array(',','""',"''",'\"\"',"\'\'") , array(' ','','','',''), $string);
		$string=preg_replace('/ +/', ' ', $string);
		if (!strlen($string)) $this->sql[$colname]='('.$t.'"'.$colname.'"=\'\' OR '.$t.'"'.$colname.'" IS NULL)';
		return $string;
		} #</q_empty_val()
	function q_group_elements($qry_group, $or) {
		foreach ($qry_group as $k=>$v) {
			$qry_group[$k]=$this->sql[$v];
			unset($this->sql[$v]);
			}
		$this->sql[$v]='('.implode((($or) ? ' OR ':' AND '), $qry_group).')';
		} #</q_group_elements()>
	
	function q_between($t, $colname, $value) {
		$v=array();
		foreach (array(1,2) as $nr) {
			$v[$nr]=$this->vl[$colname.'_'.$nr];
			$this->url[$colname.'_'.$nr]=urlencode($v[$nr]);
			}
		if (strlen($v[1]) && strlen($v[2])) $this->sql[$colname]=$t.'"'.$colname.'" BETWEEN '.$this->q($colname.'_1',$v[1]).' AND '.$this->q($colname.'_2',$v[2]);
		else {
			if (strlen($v[1])) $this->sql[$colname]=$t.'"'.$colname.'"='.$this->q($colname.'_1',$v[1]);
			if (strlen($v[2])) $this->sql[$colname]=$t.'"'.$colname.'"<='.$this->q($colname.'_2',$v[2]);
			}
		} #</q_between()>
	function q_between2($t, $colname, $value) {
		$v=array();
		foreach (array(1,2) as $nr) {
			$v[$nr]=$this->vl[$colname.'_'.$nr];
			$this->url[$colname.'_'.$nr]=urlencode($v[$nr]);
			}
		if (strlen($v[1]) && strlen($v[2])) $this->sql[$colname]='('.$t.'"'.$colname.'_1" BETWEEN '.$this->q($colname.'_1',$v[1]).' AND '.$this->q($colname.'_2',$v[2]).' OR '.$t.'"'.$colname.'_2" BETWEEN '.$this->q($colname.'_1',$v[1]).' AND '.$this->q($colname.'_2',$v[2]).')';
		else {
			if (strlen($v[1])) $this->sql[$colname]=$t.'"'.$colname.'_1"='.$this->q($colname.'_1',$v[1]);
			if (strlen($v[2])) $this->sql[$colname]=$t.'"'.$colname.'_1"<='.$this->q($colname.'_2',$v[2]).' AND '.$t.'"'.$colname.'_2"<='.$this->q($colname.'_2',$v[2]);
			}
		} #</q_between2()>
	function q_bool($t, $colname, $value) {
		$this->sql[$colname]=($value) ? $t.'"'.$colname.'"':'(!'.$t.'"'.$colname.'" OR '.$t.'"'.$colname.'" IS NULL)';
		$this->url[$colname]=urlencode($value);
		} #</q_bool()>
	function q_bool_str($t, $colname, $value) {
		$this->sql[$colname]=($value) ? $t.'"'.$colname.'"!=\'\'':'('.$t.'"'.$colname.'"=\'\' OR '.$t.'"'.$colname.'" IS NULL)';
		$this->url[$colname]=urlencode($value);
		} #</q_bool_str()>
	function q_date($colname, $date) { // $date=array('d'=>01, 'm'=>01, 'Y'=>2005, 'd2'=>31, 'm2'=>12, 'Y2'=>2006, );
		reset($date);
		$d1=($d1=current($date)) ? $d1:'00';
		$m1=($m1=next($date)) ? $m1:'00';
		$Y1=($Y1=next($date)) ? $Y1:'0000';
		$d2=($d2=next($date)) ? $d2:'00';
		$m2=($m2=next($date)) ? $m2:'00';
		$Y2=($Y2=next($date)) ? $Y2:'0000';
		if ($d2>0 or $m2>0 or $Y2>0) $this->sql[$colname]='"'.$colname.'" BETWEEN \''.$Y1.'-'.$m1.'-'.$d1.'\' AND \''.$Y2.'-'.$m2.'-'.$d2.'\'';
		else {
			array_splice($date, 3);
			$funcs=array(1=>'DAY("'.$colname.'")', 2=>'MONTH("'.$colname.'")', 3=>'YEAR("'.$colname.'")', );
			foreach ($date as $k=>$v) {
				$nr++;
				if ($v) {
					$this->sql[$k]=$funcs[$nr].'=\''.$v.'\'';
					$this->url[$k]=urlencode($v);
					}
				}
			}
		} #</q_date()>
	function q_equals($t, $colname, $string) {
		$urlname=($urlname=$this->f_ext($colname)) ? $urlname:$colname;
		$this->url[$colname]=urlencode($string);
		$string=$this->q_empty_val($t, $colname, $string);
		if (strlen($string)) $this->sql[$colname]=$t.'"'.$colname.'"='.$this->q($colname, $string);
		} #</q_equals()>
	function q_equals_multi_and($t, $colname, $string) {
		$sql=array();
		$e=$this->structure[$colname];
		foreach ($e['options'] as $k=>$v) {
			$sql[]='"'.$colname.'_'.$k.'"='.$this->q($colname.'_'.$k,$this->vl[$colname.'_'.$k]);
			$this->url[$colname.'_'.$k]=urlencode($this->vl[$colname.'_'.$k]);
			if ($e['level_open']) {	if ($k>=$e['level_open']) break 1;	}
			}
		if (!empty($sql)) $this->sql[$colname]='('.implode(' AND ', $sql).')';
		} #</q_equals_multi_and()>
	function q_like($t, $colname, $string) {
		$this->url[$colname]=urlencode($string);
		$string=$this->q_empty_val($t, $colname, $string);
		if ($string) $this->sql[$colname]=$t.'"'.$colname.'" LIKE '.$this->q($colname,$string);
		} #</q_like()>
	function q_like_begin($t, $colname, $string) {
		$this->url[$colname]=urlencode($string);
		$string=$this->q_empty_val($t, $colname, $string);
		if ($string) $this->sql[$colname]=$t.'"'.$colname.'" LIKE '.$this->q($colname,$string.'%');
		} #</q_like_begin()>
	function q_like_mid($t, $colname, $string) {
		$this->url[$colname]=urlencode($string);
		$string=$this->q_empty_val($t, $colname, $string);
		if ($string) $this->sql[$colname]=$t.'"'.$colname.'" LIKE '.$this->q($colname,'%'.$string.'%');
		} #</q_like()>
	function q_multi_bool_or($t, $colname, $string) {
		$e=$this->structure[$colname];
		$sql=array();
		if ($e['type']==='multi-checkbox') foreach ($e['options'] as $k=>$v) {
			if ($this->vl[$colname.'_'.$k]) {
				if ($k==='-0') {
					$sql[$k]=array();
					foreach ($e['options'] as $kk=>$vv) if ($kk!=='-0') $sql[$k][]='!'.$t.'"'.$colname.'_'.$kk.'"';
					$sql[$k]='('.implode(' AND ', $sql[$k]).')';
					}
				else $sql[$k]=$t.'"'.$colname.'_'.$k.'"';
				$this->url[$colname.'_'.$k]=urlencode($v['value']);
				}
			}
		else foreach ($e['options'] as $k=>$v) { #<type="set-*" />
			if ($this->vl[$colname][$k]) {
				if ($k==='-0') {
					$sql[$k]=array();
					foreach ($e['options'] as $kk=>$vv) if ($kk!=='-0') $sql[$k][]='FIND_IN_SET('.$this->q($colname.$kk,$kk).','.$t.'"'.$colname.'")=0';
					$sql[$k]='('.implode(' AND ', $sql[$k]).')';
					}
				else $sql[$k]='FIND_IN_SET('.$this->q($colname.$k,$k).','.$t.'"'.$colname.'")>0';
				$this->url[$colname.$k]=urlencode($v['value']);
				}
			}
		if (!empty($sql)) $this->sql[$colname]='('.implode(' OR ', $sql).')';
		} #</q_multi_bool_or()>
	function q_multi_to_single($t, $n, $value) {
		if (!isset($this->vl[$n])) return;
		$this->url[$n]=urlencode($value);
		$this->sql[$n]=($this->structure[$colname]['type']==='multi-checkbox') ? '"'.addslashes($n.'_'.$value).'`':'FIND_IN_SET('.$this->q($n,$value).',"'.$n.'")>0';
		} #</q_multi_to_single()>
	function q_single_to_multi($t, $n, $value) {
		$sql=array();
		foreach ($this->structure[$n]['options'] as $k=>$v) if (strlen($value[$k])) {
			$this->url[urlencode($n.'['.$k.']')]=urlencode($k);
			if ($k==='-0') {
				$sql['0']=$t.'"'.$n.'"=\'0\'';
				$sql['']=$t.'"'.$n.'"=\'\'';
				$sql['null']=$t.'"'.$n.'" IS NULL';
				}
			else $sql[$k]=$t.'"'.$n.'"='.$this->q($n.$k,$k);
			}
		if (!empty($sql)) $this->sql[$n]='('.implode(' OR ', $sql).')';
		} #</q_single_to_multi()>
	function q_str_or($t, $colname, $string) {
		global $axs;
		$urlname=($urlname=$this->f_ext($colname)) ? $urlname:$colname;
		$this->url[$colname]=urlencode($string);
		$string=$this->q_empty_val($t, $colname, $string);
		if (strlen($string)===1) $this->sql[$colname]='"'.$colname.'" LIKE \'%'.$string.'%\'';
		elseif (strlen($string)>1) {
			$this->sql[$colname]=preg_split('~ ~', $string, -1, PREG_SPLIT_NO_EMPTY);
			$this->sql[$colname]='"'.$colname.'" LIKE \'%'.implode('%\' OR "'.$colname.'" LIKE \'%', $this->sql[$colname]).'%\'';
			}
		} #</q_str_or()>
	function q_str_and($t, $colname, $string) {
		global $axs;
		$urlname=($urlname=$this->f_ext($colname)) ? $urlname:$colname;
		$this->url[$colname]=urlencode($string);
		$string=$this->q_empty_val($t, $colname, $string);
		if (strlen($string)==1) $this->sql[$colname]='"'.$colname.'" LIKE \'%'.$string.'%\'';
		elseif (strlen($string)>1) {
			$this->sql[$colname]=preg_split('~ ~', $string, -1, PREG_SPLIT_NO_EMPTY);
			$this->sql[$colname]='"'.$colname.'" LIKE \'%'.implode('%\' AND "'.$colname.'" LIKE \'%', $this->sql[$colname]).'%\'';
			}
		} #</q_str_and()>
	function q_text_join_like_begin($t, $colname, $value) {
		if (strlen($value)) return $this->q_equals($t, $colname, $value);
		$el=$this->structure[$colname];
		$s=array();
		$value_text=$this->vl[$colname.'_text'];
		$this->url[$colname.'_text']=urlencode($value_text);
		$value_text=$this->q_empty_val($t, $colname, $value_text);
		if (strlen($value_text)) {
			$this->sql[$colname.'_text']=$this->sql_select_concat($el['concat']).' LIKE '.$this->q($colname.'_text',$value_text.'%');
			}
		else {
			$this->sql[$colname]='('.$t.'"'.$colname.'"=\'0\' OR '.$t.'"'.$colname.'"=\'\' OR '.$t.'"'.$colname.'" IS NULL)';
			}
		} #</q_text_join_like_begin()>
	function q_timestamp($t, $colname, $value) {
		global $axs;
		$vl=array(1=>$this->vl[$colname.'_1'], 2=>$this->vl[$colname.'_2']);
		if (!empty($vl[1]['date']) && empty($vl[2]['date'])) {
			$vl[2]=$vl[1];
			foreach (array('date'=>$vl[1]['date'],'h'=>23,'i'=>59,'s'=>59) as $k=>$v) $vl[2][$k]=$this->vl[$colname.'_2'][$k]=$v;
			}
		$time=array(1=>0,2=>0);
		foreach ($time as $k=>$v) $time[$k]=$this->input_type_timestamp_value_int($vl[$k]);
		
		$this->vl[$colname]=$time;
		$this->sql[$colname]=($time[1]) ? $t.'"'.$colname.'" BETWEEN \''.$time[1].'\' AND \''.$time[2].'\'':$t.'"'.$colname.'"<\''.$time[2].'\'';
		$url=array();
		foreach (array(1,2) as $nr) foreach (array('date'=>'','h'=>'','i'=>'','s'=>'') as $k=>$v) {
			if ($vl[$nr][$k]) $url[]=urlencode($colname.'_'.$nr.'['.$k.']').'='.urlencode($vl[$nr][$k]);
			}
		$this->url[$colname]=implode('&amp;', $url);
		} #</q_timestamp()>
	#</Query functions>
	} #</class:axs_form_search>
#2007 ?>