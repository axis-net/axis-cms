<?php #2022-04-19
class axs_array {
	#<Modify />
	static function el_add($array, $pos, $key, $element=null) {
		$pos=(($tmp=intval($pos))>=1) ? $tmp:1;
		$new=array();
		$nr=0;
		foreach ($array as $k=>$v) {
			$nr++;
			if ($nr===$pos) {
				if (is_array($key)) foreach ($key as $kk=>$vv) $new[$kk]=$vv;
				else $new[$key]=$element;
				}
			$new[$k]=$v;
			}
		if (!is_array($key)) {	if ($nr<$pos) $new[$key]=$element;	} #If nr>total nr of elements
		return $new;
		} #</el_add()>
	static function el_move($array, $key, $move) {
		$move=(($tmp=intval($move))>=1) ? $tmp:1;
		$index=array();
		$nr=0;
		foreach ($array as $k=>$v) {	$nr++;	$index[$k]=$nr;	}
		if ($index[$key]===$move) return $array;
		$new=array();
		$new_el=$array[$key];
		unset($array[$key]);
		$nr=0;
		foreach ($array as $k=>$v) {
			$nr++;
			if ($nr===$move) $new[$key]=$new_el;
			$new[$k]=$v;
			}
		if ($move>$nr) $new[$key]=$new_el; #If nr>total nr of elements
		return $new;
		} #</el_move()>
	static function sort($array, $sort_key) {
		$sorted=array();
		foreach ($array as $k=>$v) $sorted[$k]=$v[$sort_key];
		asort($sorted);
		foreach ($sorted as $k=>$v) $sorted[$k]=$array[$k];
		return $sorted;
		} #</sort()>
	#<Search />
	static function pos($array, $key, $default='') {
		$nr=0;
		foreach ($array as $k=>$v) {
			$nr++;
			if ($k===$key) return $nr;
			}
		if ($default==='end') return count($array)+1;
		} #</pos()>
	static function search($array, $col, $value) {
		$found=array();
		foreach ($array as $k=>$v) if ($v[$col]===$value) $found[$k]=$k;
		return $found;
		} #</search()>
	#<Output />
	static function output_code($array, $space='	', $n="\n	") {
		$code='array(';
		if ($space) {
			$br="\n".$space;
			$code.=$br;
			}
		else $br=' ';
		foreach ($array as $k=>$v) {
			$code.=(is_string($k)) ? "'".self::esc($k)."'".'=>':$k.'=>';
			if (is_string($v)) $code.="'".self::esc($v)."'".','.$br;
			elseif (is_bool($v)) {	$code.=($v) ? 'true'.','.$br:'false'.','.$br;	}
			elseif (is_null($v)) $code.='null'.','.$br;
			elseif (is_array($v)) $code.=self::output_code($v, '	'.$space, $n).', '.$br;
			else $code.=$v.','.$br;
			}
		return $code.')';
		} #</output_code()>
	static function output_table($array, $opts=array()) {
		$cfg=array('attr'=>'', 'caption'=>false, 'container'=>true, );
		$cfg=array_merge($cfg, $opts);
		$table='';
		$rownr=$cols=0;
		foreach ($array as $cl) {
			$rownr++;
			if ($rownr==1) {
				$table.='	<tr style="vertical-align:top;">'."\n";//.'  <th scope="col">&nbsp;</th>';
				$cols=count($cl);
				foreach ($cl as $k=>$v) $table.='<th scope="col">'.$k.'</th>';
				$table.="\n".'	</tr>'."\n";
				}
			$table.='	<tr style="vertical-align:top;">'."\n";#.'  <th scope="row">'.$rownr.'</th>';
			$count=count($cl);
			$nr=0;
			foreach($cl as $k=>$v) {
				$nr++;
				$colspan=(($count<$cols) && ($nr===$count)) ? ' colspan="'.($cols-$count+1).'"':'';
				$table.='<td'.$colspan.'>'.nl2br($v).'</td>';
				}
			$table.="\n".'	</tr>'."\n";
			}
		if (!$cfg['container']) return $table;
		if (strlen($cfg['caption'])) $cfg['caption']='	<caption>'.$cfg['caption'].'</caption>'."\n";
		return '<table'.$cfg['attr'].'>'."\n".$cfg['caption'].$table.'</table>'."\n";
		} #</output_table()>
	#<Misc. />
	static function esc($str) {	return str_replace(array('\\','\''),  array('\\\\','\\\''), $str);	}
	} #</class::axs_array>
#2021-05-17 ?>