<?php #2022-03-07
class axs_content_write extends axs_content_edit {
	var $save=true;
	var $xtab_blank_row=array(1=>"<?php #|||||||||?>\n");
	function __construct($site_nr, $l, $dir_user) {
		axs_content_edit::__construct($site_nr, $l, $dir_user);
		if (isset($_POST[$this->key_save])) $this->save='content';
		} #</axs_content_edit()>
	function plugin_installer() {
		global $axs_content;
		#<plugin installer>
		if (!isset($this->plugin_installer)) {
			$this->plugin_installer=false;
			foreach (axs_content::plugin_dirs() as $k=>$v) if (file_exists($tmp=$v.$axs_content['plugin'].'.edit.php')) {
				$e=include_once($tmp);
				if (is_callable(array($e, 'installer'))) $this->plugin_installer=$e;
				break;
				}
			}
		return (!empty($this->plugin_installer));
		} #</plugin_installer()>
	# ------------ <Content functions> --------------
	function c_new($c) { #<Find available content ID c />
		global $axs;
		$new_c=$c;
		$nr=0;
		$exists=$this->c_get('', $new_c, $axs['l'], true);
		while (!empty($exists['c'])) {
			$nr++;
			$c=substr($c, 0, axs_content::$c_length-strlen($nr));
			$exists=$this->c_get('', $new_c=$c.$nr, $axs['l'], true);
			}
		return $new_c;
		} #</c_new()>
	function content_del($c, $d=false, $l=false) { # <Delete content item />
		global $axs, $axs_content;
		if (!$c) return $this->lg(__FUNCTION__, __LINE__, 'No $c');
		if ($d===false) $d=$this->dir_user.$this->d;
		if (!$l) $l=$this->l;
		$d_bak=$this->fs->d;
		$this->fs->dir_set($this->dir_content);
		foreach (axs_content::$menu_pic_ext as $v) { # delete menu pic
			foreach (array('','_act') as $vv) {
				$this->fs->f_del($f=$d.$c.'_'.$l.$vv.'.'.$v, false);
				if ($this->fs->exists($f)) return $this->msg($this, 'f_del_err', 'Could not delete file "'.$f.'"!', array('f'=>$f));
				}
			if ($this->fs->msg) return $this->fs->msg;
			}
		$exists=array();
		$cl=$this->c_get($d, $c, $l, false);
		if (($cl['plugin']==='menu') or ($cl['plugin']==='menu-page')) { #<Del submenu>
			$exists=false;
			foreach ($this->site['langs'] as $k=>$v) { # find out if the same $c exists in some other language
				if ($k!=$l) $exists=$this->c_get($d, $c, $k, false);
				if (!empty($exists)) break;
				}
			if (empty($exists)) {	if ($error=$this->fs->dir_rm($d.$c, false)) return $error;	}
			else {
				foreach ($this->menu_get($d.$c.'/', $l) as $v) {
					if ($error=$this->content_del($v['c'], $d.$c.'/', $l)) return $error;
					}
				if ($error=$this->fs->f_del($d.$c.'/'.'index_'.$l.'.php', false)) return $error;
				}
			//$d=rtrim(dirname($d), '/').'/';
			} #</Del submenu>
		else { #<Del page>
			if ($this->plugin_installer()) {
				if ($error=$this->plugin_installer->installer_delete($this, $c)) return $error;
				}
			if ($this->fs->exists($d.$c.'_files', 'dir')) {
				foreach ($this->site['langs'] as $k=>$v) {
					if ($k!=$l) $exists=$this->c_get($d, $c, $k, false);
					if (!empty($exists)) break;
					}
				if (empty($exists)) {
					if ($error=$this->fs->dir_rm($d.$c.'_files')) return $error;
					if ($this->fs->exists($d.$c.'_files', 'dir')) return $this->msg($this, 'd_del_err', 'Could not delete directory "'.$d.$c.'"!', array('f'=>$f));
					}
				}
			$this->posts_get($c);
			foreach (array_merge(
				(array)$axs_content['content'][$c]['post_files'],
				array($c.'_'.$l.'.form.php', $c.'_'.$l.'.tr.php', $c.'_'.$l.'.php')
				) as $v) {
				if ($error=$this->fs->f_del($d.$v, false)) return $error;
				}
			} #</Del page>
		if (empty($this->msg)) $this->structure_update_index($cl, array('del'=>$c), $d, $l);
		$this->fs->dir_set($d_bak);
		} #</content_del()>
	function content_del_recycle($c, $l=false) { # <Move content item to recycler />
		global $axs, $axs_content;
		if (!$c) return 'Missing $c @ content_del()';
		if (!$l) $l=$this->l;
		$this->structure_update_rename($c, $axs['time'].'-'.$c, $l);
		if (!empty($this->msg)) return;
		$this->content_move($axs['time'].'-'.$c, 'recycler/');
		#<Limit the size of recycler />
		$limit=(strlen($this->site['recycle'].'')) ? $this->site['recycle']:10;
		$index=$this->menu_get('recycler/', $l);
		foreach ($index as $nr=>$cl) {
			if ($nr>$limit) $this->content_del($cl['c'], 'recycler/', $l);
			}
		} #</content_del_recycle()>
	function content_move($c, $to) {
		global $axs, $axs_content;
		$cl=$this->c_get('', $c, $this->l, true);
		//$cl=$this->c_get($this->d, $c, $this->l, false);
		if (empty($cl)) return $this->lg(__FUNCTION__, __LINE__, 'Content item ID not found! ("'.$c.'")');
		#<Validate>
		$d=$this->f_secure($to);
		if ($d!=$to) return $this->msg($this, 'move_dirname_err', 'The destination directory name caontains forbidden characters! ("'.axs_html_safe($to).'")', array('d'=>axs_html_safe($to)));
		if (strlen($d)>0) $d=rtrim($this->f_secure($to), '/').'/';
		$dir=$this->dir_content.$d;
		if ($dir===$this->dir_page.$c.'/') return $this->msg($this, 'move_inself', 'A driectory cannot be moved into itself!');
		if (!is_dir($dir)) return $this->msg($this, 'move_dest_not_found', 'The destination directory "'.axs_html_safe($dir).'" could not be found!', array('d'=>axs_html_safe($dir)));
		if (!is_writable($dir)) return $this->msg($this, 'move_dest_write', 'The destination directory is not writable! chmod("'.axs_html_safe($dir).'",0777)', array('d'=>axs_html_safe($dir)));
		#</Validate>
		#<Move files>
		$files=array();
		foreach (axs_content::$menu_pic_ext as $v) foreach (array('','_act') as $vv) { #<Menu-pic files />
			if (file_exists($this->dir_content.$this->dir_page.($f=$c.'_'.$axs['l'].$vv.'.'.$v))) $files[$f]=$f;
			}
		if (($cl['plugin']==='menu') or ($cl['plugin']==='menu-page')) {
			foreach ($this->site['langs'] as $k=>$v) {
				if ($k!=$axs['l']) $exists=$this->c_get($this->dir_page, $c, $k, false);
				if (!empty($exists)) return $this->msg($this, 'move_dir_lang', 'Unable to move directory, because the same ID is also used in another language ("'.$k.'")', array('l'=>$k));
				}
			$files[$c]=$c;
			}
		else {
			if ($this->fs->exists($d.$c.'_files', 'dir')) foreach ($this->site['langs'] as $k=>$v) {
				if ($k!=$l) $exists=$this->c_get($this->dir_page.$v, $c, $k, false);
				if (!empty($exists)) return $this->msg($this, 'move_dir_files_lang', 'Unable to move files directory, because the same ID is also used in another language ("'.$k.'")', array('l'=>$k));
				}
			foreach (array('_'.$axs['l'].'.php', '_'.$axs['l'].'.tr.php', '_'.$axs['l'].'.form.php', '_files', ) as $v) $files[$c.$v]=$c.$v;
			$this->posts_get($c);
			foreach ($axs_content['content'][$c]['post_files'] as $v) $files[$v]=$v;
			}
		foreach ($files as $k=>$v) { #<Check if target exists and abort />
			if (file_exists($dir.$v)) {
				$msg=$this->msg($this, 'move_file_err', 'File already exists "'.$dir.$v.'"!', array('f'=>$dir.$v));
				return $this->lg('content_move', __LINE__, $msg);
				}
			}
		foreach ($files as $k=>$v) { #<Move files if no error found previously />
			if (file_exists($this->dir_page.$v)) rename($this->dir_page.$v, $dir.$v);
			if (file_exists($this->dir_page.$v)) {
				$msg=$this->msg($this, 'move_file_err', 'Could not move file "'.$this->dir_page.$v.'"!', array('f'=>$this->dir_page.$v));
				return $this->lg('content_move', __LINE__, $msg);
				}
			}
		#</Move files>
		if ($this->plugin_installer()) {
			if ($error=$this->plugin_installer->installer_move($this, $cl['dir'], $to)) return $error;
			}
		#<Update menu>
		if (empty($this->msg)) $this->menu_edit($to, $c, $cl, array('add'=>($d==='recycler/')?1:$cl['nr'], ), $this->l);//$this->structure_update_index($cl, $opts, $d, $this->l);
		if (empty($this->msg)) {
			//$this->structure_update_index($cl, array('edit'=>$cl['c'], 'delrow'=>$cl['nr'], ), $this->dir_page, $this->l);
			$this->menu_edit($cl['dir'], $c, $cl, array('del'=>true), $this->l);
			if (($cl['plugin']==='menu') or ($cl['plugin']==='menu-page')) {
				$find='/'.$this->d.$cl['c'].'/';
				$replace='/'.$d.$cl['c'].'/';
				}
			else {
				$find='/'.$this->d.$cl['c'].'_files/';
				$replace='/'.$d.$cl['c'].'_files/';
				}
			$this->content_replace(array($find, urlencode($find)), array($replace, urlencode($replace)), $cl['c']);
			}
		#</Update menu>
		} #</content_move()>
	function content_replace($find, $replace, $c='', $l=false, $d=false) { # Replace string in content recursive
		if (!$l) $l=$this->l;
		if ($d===false) {
			if ($c) {
				$d=$this->c_get('', $c, $l, true);
				$d=$d['dir'];
				}
			else $d=$this->dir_user.$this->d;
			}
		$index=($c) ? array(1=>$this->c_get('', $c, $l, true)):$this->menu_get($d, $l);
		foreach ($index as $v) {
			if (!$v['c']) continue;
			if (($v['plugin']==='menu') or ($v['plugin']==='menu-page')) $this->content_replace($find, $replace, '', $l, $d.$v['c'].'/');
			else {
				$data=$this->content_get($v['c'], false, $v['dir'], $l);
				foreach ($find as $kk=>$vv) if (strpos($data, $vv)) {
					$this->content_upd(str_replace($find, $replace, $data), $v['c'], $l, $v['dir']);
					break;
					}
				}
			}
		} #</content_replace()>
	function content_upd($data, $c, $l=false, $d=false) { # Save content
		if ($d===false) $d=$this->dir_page;
		else $d=$this->dir_content.$d;
		if ($l===false) $l=$this->l;
		if (!$c) return $this->lg(__FUNCTION__, __LINE__, 'Missing $c @ content_upd()');
		if (!$l) return $this->lg(__FUNCTION__, __LINE__, 'Missing $l @ content_upd()');
		if (is_array($data)) $data=implode('', $data);
		$this->fs->f_save($c.'_'.$l.'.php', $data, false, $d);
		} #</content_upd()>
	function menu_edit($d, $c, $cl, $cmd, $l=false) {
		if ($l===false) $l=$this->l;
		if (empty($c)) return $this->lg(__FUNCTION__, __LINE__, $this->msg($this, '', 'No $c'));
		$opts=array();
		if (empty($cmd['add'])) {
			$r=$this->c_get($d, $c, $l, false);
			if (empty($r['c'])) return $this->lg(__FUNCTION__, __LINE__, $this->msg($this, '', 'Content item "'.$d.$c.'" not found!'));
			$opts['editrow']=$r['nr']+1;
			}
		#if (!empty($cmd['edit'])) $opts['editrow']=$r['nr']+1;
		if (!empty($cmd['move'])) $opts['moverow']=$cl['nr']+1;
		if (!empty($cmd['del'])) $opts=array('delrow'=>$r['nr']+1, ); #This must clear all previous commands!
		if (!empty($cmd['add']))  $opts['addrow']=$cl['nr']+1;
		#<Conver $cells />
		$cells=array();
		foreach (axs_content::$index_fields_meta as $k=>$v) if (isset($cl[$k])) $cells[0][$k]=$cl[$k];
		foreach (axs_content::$index_fields as $k=>$v) if (isset($cl[$k])) $cells[$v]=$cl[$k];
		foreach (axs_content::$index_fields_meta as $k=>$v) unset($cells[$k]);
		#<Update menu file />
		$menu_data=array();
		if (file_exists($f=$this->dir_content.$d.'index_'.$l.'.php')) $menu_data=file($f);
		$menu_data=(!empty($menu_data)) ? array_merge(array(''), $menu_data):$this->xtab_blank_row;
		unset($menu_data[0]);
		$menu_data=axs_tab_edit::tab_edit($menu_data, $cells, '', $opts);
		$this->fs->f_save('index_'.$l.'.php', $menu_data, false, $this->dir_content.$d);
		} #</structure_update_index()>
	function posts_upd($c, $data, $p) {
		global $axs, $axs_content;
		$f=$c.'_'.$axs['l'].'.post'.substr($p+10000, 1).'.php';
		if (!empty($data)) return $this->fs->f_save($f, implode('', $data));
		else {
			$this->fs->f_del($f);
			unset($axs_content['content'][$c]['post_files'][$p]);
			foreach ($axs_content['content'][$c]['post_files'] as $k=>$v) {
				if ($k>$p) {
					$fnew=substr_replace($v, substr($k+10000-1, 1), -8, -4);
					$this->fs->rename($v, $fnew); 
					}
				}
			}
		} #</posts_upd()>
	function site_cfg_update($change) {
		global $axs_user;
		foreach ($change as $k=>$v) $this->site_cfg[$k]=$v;
		$data='<?php #updated '.$axs_user['user'].'@'.date('d.m.Y H:i:s')."\n".
		'return '.axs_fn::array_code($this->site_cfg, '	', "\n	").";\n".'?>';
		$this->fs->f_save('index.php', $data, false);
		} #</site_cfg_update()>
	function site_title_update($title, $l=false) {
		if ($l===false) $l=$this->l;
		$data=$this->content_tr_get('index-tr', '', $l);
		$data['title_site']=$title;
		$this->save_array(array_merge(array('title'=>$title), $data), 'index-tr_'.$l.'.tr.php');
		} #</site_title_update()>
	function structure_update($cl, $opts, $l=false, $d=false) { # Edit menu entry
		global $axs, $axs_content, $axs_user;
		if ($d===false) $d=$this->dir_user.$this->d;
		if (!$l) $l=$this->l;
		if (empty($opts)) return $this->lg('structure_update', __LINE__, 'No $opts @ structure_update()');
		# <Get plugin>
		$cl['plugin']=$this->f_secure($cl['plugin']);
		$this->plugin=($cl['plugin']) ? $cl['plugin']:'deflt';
		#<$plugin_def>
		if (($this->plugin!=='menu') && ($this->plugin!=='menu-page')) include(axs_file_choose($this->plugin.'.plugin.php'));
		else $plugin_def=array();
		#</$plugin_def>
		# </Get plugin>
		if (!empty($opts['add'])) { #<add>
			$this->structure_update_validate($cl, $opts);
			if (!empty($this->msg)) return;
			$this->structure_update_index($cl, $opts, $d, $l);
			if (!empty($this->msg)) return;
			# add content files
			if (($cl['plugin']==='menu') or ($cl['plugin']==='menu-page')) {
				if ($err=$this->fs->dir_mk($cl['c'], false)) return $err;
				if ($err=$this->fs->f_save($cl['c'].'/'.'index_'.$l.'.php', implode('', $this->xtab_blank_row), false)) return $err;
				if ($err=$this->fs->f_save($cl['c'].'/'.'index.php', '<?php header(\'HTTP/1.0 404 Not Found\'); ?>', false)) return $err;
				}
			else { 
				$data=(isset($plugin_def['content'][$l])) ? $plugin_def['content'][$l]:@current($plugin_def['content']);
				$this->content_upd($data, $cl['c'], $l);
				if (!empty($plugin_def['mkdir'])) {
					if ($plugin_def['mkdir']==='module') $this->fs->dir_mk('module.'.$cl['plugin'], axs_get('hide_dir', $plugin_def), '');
					else $this->fs->dir_mk($cl['c'].'_files', axs_get('hide_dir', $plugin_def));
					}
				if (!empty($plugin_def['form'])) {
					$plugin_def['form']=$this->plugin_form_get($this->plugin);
					$this->save_form($plugin_def['form'], $cl['c'], $l);
					/*aaa$plugin_def['form']='<?php #updated '.$axs_user['user'].'@'.date('d.m.Y H:i:s')."\n".'return '.axs_fn::array_code($plugin_def['form'], '	', "\n	").";\n".'?>';
					$this->fs->f_save($cl['c'].'_'.$l.'.form.php', $plugin_def['form'], false);*/
					}
				if (!empty($plugin_def['tr'])) {
					$plugin_def['tr']=(!empty($plugin_def['tr'][$l])) ? $plugin_def['tr'][$l]:@current($plugin_def['tr']);
					$plugin_def['tr']='<?php #updated '.$axs_user['user'].'@'.date('d.m.Y H:i:s')."\n".'return '.axs_fn::array_code($plugin_def['tr'], '	', "\n	").";\n".'?>';
					$this->fs->f_save($cl['c'].'_'.$l.'.tr.php', $plugin_def['tr'], false);
					}
				if ($this->plugin_installer()) {
					if ($error=$this->plugin_installer->installer_add($this, $cl['c']))	return $error;
					}
				}
			if (!empty($this->msg)) return;
			} #</add>
		if (!empty($opts['edit'])) { #<edit>
			$this->structure_update_validate($cl, $opts);
			if (!empty($this->msg)) return;
			$r=$this->c_get('', $opts['edit'], $l, true);
			$d=$r['dir'];
			if ($cl['c']==$opts['edit']) $cl['c']=$r['c'];
			$opts['nr']=$r['nr'];
			$menu_add=false;
			if (($r['plugin']==='menu') || ($r['plugin']==='menu-page')) {
				if (($cl['plugin']!=='menu') && ($cl['plugin']!=='menu-page')) $cl['plugin']=$r['plugin'];
				}
			else {
				if ($cl['menu_add']) $menu_add=true;
				if (!axs_user::permission_get('dev')) $cl['plugin']=$r['plugin'];
				}
			if ($menu_add) { #<Rename content item before creating new submenu />
				$bak=$cl;
				$cl['c']=$this->c_new($cl['c']);
				}
			else {
				if ($cl['mkdir']) { if ($err=$this->fs->dir_mk($cl['c'].'_files')) return $err; }
				}
			if (empty($this->msg)) {
				if ($menu_add) $this->structure_update_rename($bak['c'], $cl['c'], $axs['l']);
				else $this->structure_update_index($cl, $opts, $d, $l);
				}
			if ($menu_add) {
				# <Create new submenu>
				$bak['c']=$cl['c_old'];
				$bak['plugin']='menu';
				$this->structure_update($bak, array('add'=>$bak['nr']));
				# </Create new submenu>
				# <Move original page into the newly created submenu>
				$cl['menu_add']=false;
				$cl['dir']=$this->d.$cl['c_old'].'/';
				$opts=array('move_content'=>$this->d.$cl['c_old'].'/');
				}
			} #</edit>
		if (!empty($opts['move_content'])) { #<move>
			$this->content_move($cl['c'], $this->dir_user.$cl['dir']);
			} #</move>
		if (!empty($opts['del'])) { #<del>
			if ($opts['del']==='p') $this->content_del($cl['c']); #<del permanently />
			else $this->content_del_recycle($cl['c']); #<move to recycler>
			} #</del>
		} #</structure_update()>
	function structure_update_fields($cl) {
		$cells=array();
		foreach (axs_content::$index_fields as $k=>$v) $cells[$v]=$cl[$v];
		return $cells;
		} #</structure_update_fields()>
	function structure_update_index($cl, $opts, $d, $l) {
		$c=false;
		foreach (array('del'=>'','edit'=>'') as $k=>$v) if (!empty($opts[$k])) $c=$opts[$k];
		if (!$c && !$opts['add']) return $this->lg(__FUNCTION__, __LINE__, 'No $c');
		$r=array('nr'=>'', );
		if ($c) $r=$this->c_get($d, $c, $l, false);
		if (!isset($cl['nr'])) $cl['nr']=false;
		#<Convert $opts>
		if (!empty($opts['add'])) $opts+=array('addrow'=>$opts['add'], 'moverow'=>$cl['nr'], );
		if (!empty($opts['edit'])) $opts+=array('editrow'=>$r['nr'], 'moverow'=>$cl['nr'], );
		if (!empty($opts['del']) && empty($opts['delrow'])) $opts['delrow']=$r['nr'];
		if (!empty($opts['addrow'])) $opts['addrow']++;
		if (strlen(axs_get('editrow', $opts))) $opts['editrow']++;
		if (!empty($opts['moverow'])) $opts['moverow']++;
		if (!empty($opts['delrow'])) $opts['delrow']++;
		if (!empty($opts['addrow'])) unset($opts['editrow']);
		if (!empty($opts['edit'])) {
			if (($opts['edit']==='title') && ($d==='')) $opts=array('editrow'=>1, );
			}
		#</Convert $opts>
		#<Conver $cells />
		$cells=array();
		foreach (axs_content::$index_fields_meta as $k=>$v) if (isset($cl[$k])) $cells[0][$k]=$cl[$k];
		foreach (axs_content::$index_fields as $k=>$v) if (isset($cl[$k])) $cells[$v]=$cl[$k];
		foreach (axs_content::$index_fields_meta as $k=>$v) unset($cells[$k]);
		$menu_data=array();
		if (file_exists($f=$this->dir_content.$d.'index_'.$l.'.php')) $menu_data=file($f);
		$menu_data=(!empty($menu_data)) ? array_merge(array(''), $menu_data):$this->xtab_blank_row;
		unset($menu_data[0]);
		$menu_data=axs_tab_edit::tab_edit($menu_data, $cells, $format='', $opts);
		$this->fs->f_save('index_'.$l.'.php', $menu_data, false, $this->dir_content.$d);
		} #</structure_update_index()>
	function structure_update_rename($c_old, $c_new, $l, $d=false) { #<rename c />
		global $axs, $axs_content;
		if ($d===false) $d=$this->dir_user.$this->d;
		$cl=$this->c_get('', $c_old, $this->l, true);
		if (empty($cl)) return $this->lg(__FUNCTION__, __LINE__, 'Content item ID "'.$c_old.'" not found!');
		foreach (axs_content::$menu_pic_ext as $v) foreach (array(
			$c_old.'_'.$l.'.'.$v=>$c_new.'_'.$l.'.'.$v, 
			$c_old.'_'.$l.'_act.'.$v=>$c_new.'_'.$l.'_act.'.$v,
			) as $kk=>$vv) {
				if ($err=$this->fs->rename($kk, $vv, false)) return $this->msg($this, '', $err);
				}
		if (($cl['plugin']==='menu') or ($cl['plugin']==='menu-page')) {	if ($err=$this->fs->rename($c_old, $c_new, false)) return $this->msg($this, '', $err);	}
		else {
			$tmp=array();
			$this->posts_get($c_old);
			foreach ($axs_content['content'][$axs['c']]['post_files'] as $v) $tmp[$v]=preg_replace('/^'.$c_old.'/', $c_new, $v);
			foreach ($tmp+array(
				$c_old.'_files'=>$c_new.'_files',
				$c_old.'_'.$l.'.tr.php'=>$c_new.'_'.$l.'.tr.php',
				$c_old.'_'.$l.'.form.php'=>$c_new.'_'.$l.'.form.php',
				$c_old.'_'.$l.'.php'=>$c_new.'_'.$l.'.php',
				) as $k=>$v) {
					if ($err=$this->fs->rename($k, $v, false)) return $err;
					}
			if ($this->plugin_installer()) {
				if ($error=$this->plugin_installer->installer_rename($this, $c_old, $c_new)) return $error;
				}
			}
		if (empty($this->msg)) $this->menu_edit($d, $c_old, array('c'=>$c_new), array('edit'=>true),  $l);
		if (empty($this->msg)) {
			if (($cl['plugin']==='menu') or ($cl['plugin']==='menu-page')) {
				$find='/'.$this->d.$c_old.'/';
				$replace='/'.$this->d.$c_new.'/';
				}
			else {
				$find='/'.$this->d.$c_old.'_files/';
				$replace='/'.$this->d.$c_new.'_files/';
				}
			$this->content_replace(array($find, urlencode($find)), array($replace, urlencode($replace)), '', $l);
			}
		$this->c=$c_new;
		} #</structure_update_rename()>
	function structure_update_validate(&$cl, &$opts) { # <validate menu entry ($cl) />
		global $axs, $axs_content;
		if (!$cl['c']) return $this->msg($this, 'input_c_empty', 'Please fill in the "ID" field!');
		if (!$cl['title']) return $this->msg($this, 'input_title_empty', 'Please fill in the title field!');
		# pw valid?
		if ($cl['pw']) {if ($cl['pw']!==axs_valid::id($cl['pw'])) $this->msg($this, 'input_pass_ivalid', 'Password contains forbidden characters!'); }
		# validate plugin
		if (!empty($opts['add']) && ($this->plugin!=='menu') && ($this->plugin!=='menu-page')) {
			if (!axs_file_choose($this->plugin.'.plugin.php')) $this->lg(__FUNCTION__, __LINE__, 'Plugin "'.$tmp.'" not found!');
			}
		# validate groups restrict
		if (!is_array($cl['restrict'])) $cl['restrict']=axs_content::permission_parse($cl['restrict']);
		$tmp=$axs['cfg']['groups']+array('-'=>'login');
		foreach ($cl['restrict'] as $k=>$v) {
			if (!isset($tmp[$k])) unset($cl['restrict'][$k]); # remove any non-existing groups
			}
		foreach ($cl['restrict'] as $k=>$v) $cl['restrict'][$k]=$k.'='.implode(',', $v);
		$cl['restrict']=implode('&', $cl['restrict']);
		} #</structure_update_validate()>
	# ------------ </Content functions> --------------
	# ------------ <Editor backend functions> --------
	function save_() {
		global $axs;
		$data=$this->tab->content_edit($this->content_get($axs['c']), $_GET, $_POST);
		//$axs['msg']+=$this->tab->msg;
		# Update content only if no error occured
		if (empty($this->tab->msg)) {
			$this->content_upd($data, $axs['c']);
			if (empty($this->msg)) exit(axs_redir($this->tab->b_url));
			}
		} #</save_()>
	function save_array($data, $f) {
		global $axs_user;
		$data='<?php #updated '.$axs_user['user'].'@'.date('d.m.Y H:i:s')."\n".
		//'if (!defined(\'AXS_PATH_CMS\')) header(\'HTTP/1.0 404 Not Found\');'."\n".
		'return '.axs_fn::array_code($data, '	', "\n	").";\n".'?>';
		$this->fs->f_save($f, $data, false);
		} #</save_array()>
	function save_menu() {
		global $axs, $axs_content;
		$b=strip_tags($_POST['b']);
		$menu_pic_ext=$axs_content['menu-pic_ext'][$axs['c']];
		$cl=array(
			'c'=>strtolower(substr(axs_valid::id($_POST['c']), 0, axs_content::$c_length)), # force valid
			'c_old'=>strtolower(substr(axs_valid::id($_GET['edit']), 0, axs_content::$c_length)), # force valid
			'title'=>substr($_POST['title'], 0, 250),
			'title_bar'=>substr($_POST['title_bar'], 0, 200),
			'description'=>substr($_POST['description'], 0, 250),
			'plugin'=>(isset($_POST['plugin'])) ? $_POST['plugin']:axs_get('plugin', $_GET),
			'style'=>(isset($_POST['style'])) ? $_POST['style']:'',
			'nr'=>intval(trim($_POST['nr'])),
			'mkdir'=>(isset($_POST['mkdir'])) ? $_POST['mkdir']:false,
			'dir'=>$_POST['dir'],
			'url'=>substr($_POST['url'], 0, 250),
			'target'=>axs_get('target', $_POST, ''),
			'menu_add'=>(isset($_POST['menu_add'])) ? $_POST['menu_add']:false,
			'publ'=>axs_get('publ', $_POST, array()),
			'hidden'=>axs_get('hidden', $_POST, array()),
			'pw'=>substr($_POST['pw'], 0, 10),
			'restrict'=>(isset($_POST['restrict'])) ? $_POST['restrict']:array(),
			);
		if (strlen($cl['title'])>1) $cl['title']=trim($cl['title']);
		if (strlen($cl['dir'])>1) {	if (substr($cl['dir'], -1)!='/') $cl['dir'].='/';	}
		foreach (array('publ'=>'','hidden'=>'') as $k=>$v) $cl[$k]=axs_form::input_type_timestamp_value_int(axs_form_edit::input_type_timestamp_value_array($cl[$k]));
		
		$cl['del']=((!empty($_POST['del'])) && (isset($_POST['del_confirm']))) ? $_POST['del'][0]:false;
		if (!$this->content_status_deletable($this->dir_user.$this->d)) { #<If menu edit not allowed>
			$cl['c']=$cl['c_old'];
			$cl['dir']=$this->d;
			if ($axs['c']) foreach (array('publ'=>'','hidden'=>'') as $k=>$v) $cl[$k]=$axs_content['content'][$cl['c']][$k]+0;
			$cl['del']=false;
			} #</If menu edit not allowed>
		if ($add=intval(axs_get('add', $_GET))) {
			if (!$cl['title']) $this->msg['title']='Pealkiri puudub!';
			# check if c already exist and then generate new c
			if (!$cl['c']) $cl['c']=strtolower(substr(axs_valid::id($cl['title']), 0, axs_content::$c_length));
			$cl['c']=$this->c_new($cl['c']);
			$exists=$this->c_get('', $cl['c'], $axs['l'], true);
			if ($exists['c']) $this->msg[]='ID "'.$cl['c'].'" on juba kasutusel!';
			else {
				if (!$cl['publ']) $cl['publ']=$axs['time'];
				$opts=array('add'=>$add, 'nr'=>$cl['nr'], );
				$b=str_replace('c=&', 'c='.$cl['c'].'&', $b);
				if (($cl['plugin']==='menu') or ($cl['plugin']==='menu-page')) $b=str_replace('d='.$this->d, 'd='.$this->d.$cl['c'].'/', $b);
				}
			}
		if (axs_get('edit', $_GET)) {
			if (!$cl['title']) $this->msg['title']='Pealkiri puudub!';
			# if change c requested notify if new c already exists
			if ($cl['c']!==$cl['c_old'] && $cl['c'] && $cl['c_old']) {
				$exists=$this->c_get('', $cl['c'], $axs['l'], true);
				if (!empty($exists)) $this->msg[]='ID "'.$cl['c'].'" on juba kasutusel! ('.rtrim($exists['dir'], '/').'/'.'"'.$exists['title'].'")'; #error message if new cid already exists
				else {
					foreach ($this->site['langs'] as $k=>$v) { # rename c in all languages
						$exists=$this->c_get($this->d, $cl['c_old'], $k, false);
						if (!empty($exists)) {
							# update content and abort cycle if error occures
							$this->structure_update_rename($cl['c_old'], $cl['c'], $k, $exists['dir']);
							if (!empty($this->msg)) break;
							}
						}
					if (empty($this->msg)) {
						$b=str_replace(array('&c='.$cl['c_old'], $cl['c_old'].'%2F&'), array('&c='.$cl['c'], $cl['c'].'%2F&'), $b);
						if (($cl['plugin']==='menu') or ($cl['plugin']==='menu-page')) $b=str_replace('d='.urlencode($this->d.$cl['c_old'].'/'), 'd='.urlencode($this->d.$cl['c'].'/'), $b);
						}
					}
				}
			$opts=array('edit'=>$cl['c'], 'moverow'=>axs_get('nr', $_REQUEST), );
			if ($cl['menu_add']) $b=str_replace('d='.$this->d, 'd='.$this->d.$cl['c'].'/', $b);
			}
		#<Move cotent item>
		if ($cl['dir'] && $this->d!=$cl['dir']) {
			if (strlen($cl['dir'])>0) $cl['dir']=rtrim($this->f_secure($cl['dir']), '/').'/';
			$opts['move_content']=$cl['dir'];
			$b=str_replace('d='.urlencode($this->d), 'd='.urlencode($cl['dir']), $b);
			} #</Move cotent item>
		if ($cl['del']) {
			if (!($cl['publ']+$cl['hidden'])) $this->msg[]='Lehek&uuml;lje v&otilde;i alammen&uuml;&uuml; kustutamiseks peab see eelnevalt olema avaldatud v&otilde;i peidetud!';
			if ($this->content_status_recycled($this->d_page)) $cl['del']='p';
			$opts=array('del'=>$cl['del'], );
			if ($_REQUEST['b_del']) $b=$_REQUEST['b_del']; else $this->msg['b']='Missing $b';
			}
		if (empty($this->msg)) $this->structure_update($cl, $opts);
		
		#<Delete menu pic files>
		if (isset($_POST['menu-pic_del'])) foreach (axs_content::$menu_pic_ext as $v) foreach (array('','_act') as $vv) {
			if (file_exists($this->dir_page.($tmp=$cl['c'].'_'.$axs['l'].$vv.'.'.$v))) $this->fs->f_del($tmp);
			} #</Delete menu pic files>
		
		# upload menu pic files
		foreach (array(1=>'', 2=>'_act') as $k=>$v) {
			if ($_FILES['f'.$k]['name']) {
				$ext=$this->f_ext($_FILES['f'.$k]['name']);
				if ($ext=='jpeg') $ext='jpg';
				if ($menu_pic_ext && $ext!=$menu_pic_ext or !in_array($ext, axs_content::$menu_pic_ext)) $this->msg['m_pic_ext_'.$k]='Pildifaili laiend ei sobi! Antud men&uuml;&uuml; vaikimisi pildi formaat on "'.$menu_pic_ext.'"';
				if (!$cl['c']) $cl['c']=$cl['c_old'];
				if (!$cl['c']) $this->msg['m_pic_cid']='ID puudub!';
				if (empty($this->msg)) {
					$this->fs->f_upl('f'.$k, $cl['c'].'_'.$axs['l'].$v.'.'.$ext);
					}
				}
			}
		if (empty($this->msg)) exit(axs_redir($b));
		} #</save_menu()>
	function save_posts() {
		global $axs, $axs_content;
		//list($total, $p, $start, $limit, $desc)=posts_get(0, $cid, $l, $d, $_GET['p'], 1);
		list($total, $p, $start, $limit, $desc)=$this->posts_get($axs['c'], axs_get('p', $_GET), $limit=0, $desc=false, $d=false, $l=false);
		if ($reorder=intval(trim($_REQUEST['reorder']))) { # reorder posts
			# rename original files to tmp files
			foreach ($axs_content['content'][$axs['c']]['post_files'] as $k=>$v) {
				$this->fs->rename($v, $v.'.tmp.php', true);
				if ($this->fs->msg) break;
				}
			# rewrite posts
			$pnr=$nr=0;
			$buffer=array();
			if (empty($axs['msg'])) foreach ($axs_content['content'][$axs['c']]['post_files'] as $k=>$v) {
				$tmp=file($this->dir_page.$v.'.tmp.php') or $tmp=array();
				$this->fs->f_del($v.'.tmp.php');
				foreach ($tmp as $kk=>$vv) {
					$nr++;
					$buffer[]=$vv;
					if (!isset($axs_content['content'][$axs['c']]['post_files'][$k+1])) {
						if (!isset($tmp[$kk+1])) {
							$reorder=$nr;
							}
						}
					if ($nr>=$reorder) {
						$pnr++;
						$this->posts_upd($axs['c'], $buffer, $pnr);
						if (!empty($axs['msg'])) break 2;
						$nr=0;
						$buffer=array();
						}
					}
				}
			# attemt to rename tmp files back in case something went wrong
			foreach ($axs_content['content'][$axs['c']]['post_files'] as $k=>$v) {
				$this->fs->rename($v.'.tmp.php', $v, false);
				}
			}
		else { # edit post
			if ($_GET['edit']==1 && isset($_POST['del_confirm']) && count($axs_content['content'][$axs['c']]['post'])<=1) {
				//$axs_content['content'][$axs['c']]['post']=array();
				$this->fs->f_del($axs_content['content'][$axs['c']]['post_files'][$p]);
				$b=str_replace('&p='.$p, '&p='.($p-1), $_POST['b']);
				exit(axs_redir($b));
				}
			else {
				$tmp=$axs_user['home']['dir'].$this->d;
				if (is_dir(axs_dir('content').$tmp.$axs['c'].'_files/')) $tmp.=$axs['c'].'_files/';
				$tab=new axs_tab_edit('', $this->site_nr, $tmp, $axs['cfg']['cms_lang']);
				$axs_content['content'][$axs['c']]['post']=$tab->content_edit($axs_content['content'][$axs['c']]['post'], $_GET, $_POST);
				$axs['msg']+=$tab->msg;
				}
			# Update content only if no error occured
			if (empty($axs['msg'])) {
				$this->posts_upd($axs['c'], $axs_content['content'][$axs['c']]['post'], $p);
				if ($tab->b_url) exit(axs_redir($tab->b_url));
				}
			}
		} #</save_posts()>
	function save_form($data, $c=false, $l=false) {
		global $axs, $axs_user, $axs_content;
		if ($c===false) $c=$axs['c'];
		if ($l===false) $l=$axs['l'];
		foreach ($data[''] as $k=>$v) if ((is_array($v)) && (isset($v['type']))) $data[''][$k]=axs_get('value', $v, '');
		$data='<?php #updated '.$axs_user['user'].'@'.date('d.m.Y H:i:s')."\n".'return '.axs_fn::array_code($data, '	', "\n	").";\n".'?>';
		$this->fs->f_save($c.'_'.$l.'.form.php', $data, false);
		} # </save_form()>
	function save_form_del() {
		global $axs, $axs_user, $axs_content;
		$this->fs->f_del($axs['c'].'_'.$axs['l'].'.form.php', false);
		} # </save_form_del()>
	function save_tr($structure, $data) {
		global $axs, $axs_user, $axs_content;
		$array=array();
		if ($structure===false) $structure=$this->plugin_tr_get(axs_valid::id($axs_content['content'][$axs['c']]['plugin']));
		foreach ($structure as $k=>$v) {
			$array[$k]=axs_get($k, $data);
			//$array[$k]=preg_replace("/(\r\n)+|(\n\r)+|\n+/", '<br />', $array[$k]);
			}
		$array='<?php #updated '.$axs_user['user'].'@'.date('d.m.Y H:i:s')."\n".'return '.axs_fn::array_code($array, '	', "\n	").";\n".'?>';
		$this->fs->f_save($axs['c'].'_'.$axs['l'].'.tr.php', $array, $check=false);
		if (!empty($this->fs->msg)) $axs['msg']+=$this->fs->msg;
		else axs_redir($_POST['b']);
		} # </save_tr()>
	# ------------ </Editor backend functions> --------
	# ------------ <Import-export functions> ----------
	function import_export($c) {
		global $axs, $axs_content;
		if ($this->form->vl['action']==='import') {
			if (($axs_content['content'][$axs['c']]['plugin']==='menu') || ($axs_content['content'][$axs['c']]['plugin']==='menu-page')) $this->import_menu_csv($c);
			else $this->import_page_csv($c);
			}
		else {
			if (($axs_content['content'][$axs['c']]['plugin']==='menu') or ($axs_content['content'][$axs['c']]['plugin']==='menu-page')) {
				$d_bak=$this->d;
				$this->d.=$axs['c'].'/';
				$data=array(0=>array());
				foreach (axs_content::$index_fields as $k=>$v) $data[0][$k]=$k;
				$data=array_merge($data, $this->menu_get($this->dir.$this->d));
				$this->d=$d_bak;
				}
			else {
				$data=axs_tab::proc_split_tab($this->content_get($c), 1);
				foreach ($data as $k=>$v) {	$data[$k][0]=implode(',', $v[0]);	}
				}
			$data=axs_form_importexport::export_table_csv($data, $this->form->vl['separator']);
			exit(axs_admin::f_send(false, $c.'-'.date('YdmHis').'.csv', $data));
			}
		} #</import_export()>
	function import_menu_csv($c) { # Import CSV
		global $axs, $axs_user;
		if ($this->f_ext($_FILES['f']['name'])!='csv') return $this->form->msg['f']='Palun laadige CSV fail!';
		$this->fs->f_upl('f');
		if (!empty($this->fs->msg)) {
			$this->form->msg+=$this->fs->msg;
			return;
			}
		$separator=$_POST['separator'][0];
		if (!$separator) $separator=',';
		# get CSV data, check if number of colums match and convert to x-tab
		$th=$data=array();
		$rownr=0;
		$handle=fopen($this->dir_page.$this->fs->f, 'r');
		while (($cl=fgetcsv($handle, 1000, $separator))!==false) {
   			if ($axs['cfg']['charset']!==$_POST['charset']) foreach ($cl as $k=>$v) $cl[$k]=mb_convert_encoding($v, $axs['cfg']['charset'], $_POST['charset']);
			if ($rownr<1) foreach ($cl as $k=>$v) {	$th[$v]=$k;	}
			else {
				$row=array();
				foreach ($th as $k=>$v) $row[$k]=$cl[$v];
				foreach (axs_content::$index_fields+array('content'=>'', ) as $k=>$v) $data[$rownr][$k]=axs_get($k, $row, '');
				}
			$rownr++;
		    }
		fclose($handle);
		unlink($this->dir_page.$this->fs->f);
		#<Validate>
		foreach ($data as $nr=>$cl) {
			if (!strlen(axs_get('title',$cl))) $this->form->msg('', $nr.': Empty column "title"!');
			}
		#</Validate>
		$d_bak=$this->d;
		$this->d.=$axs['c'].'/';
		$this->fs->dir_set($this->dir_content.$this->d);
		if (empty($this->form->msg)) foreach ($data as $nr=>$cl) {
			if (!$cl['c']) $cl['c']=$cl['title'];
			$cl['c']=strtolower(substr(axs_valid::id($cl['c']), 0, axs_content::$c_length));
			$cl['c']=$this->c_new($cl['c']);
			if (!$cl['publ']) $cl['publ']=$axs['time'];
			$cl['nr']=($_POST['position']=='append') ? $nr+1000:$nr;
			$this->structure_update($cl, array('add'=>$cl['nr'], 'nr'=>$cl['nr'], ));
			if (($cl['plugin']!=='menu') && ($cl['plugin']!=='menu-page') && (strlen($cl['content']))) {
				if (strncmp('<?php #|', $cl['content'], 8)!==0) $cl['content']='<?php #|'.$cl['publ'].'|'.axs_tab_edit::esc($cl['content'], '	').'|?>'."\n";
				$this->content_upd($cl['content'], $cl['c']);//, false, $axs['c'].'/');
				}
			}
		if (empty($this->form->msg)) axs_exit(axs_redir());
		$this->d=$d_bak;
		$this->fs->dir_set();
		} #</import_menu_csv()>
	function import_page_csv($c) { # Import CSV
		global $axs, $axs_user;
		# check how many columns content has
		$data=$this->content_get($c, true);
		$cols=count(explode('|', $data[1]))-2;
		if ($cols<2) $cols=2;
		# file upload
		if ($this->f_ext($_FILES['f']['name'])!='csv') return $this->form->msg['fext']='Palun laadige CSV fail!';
		$this->fs->f_upl('f');
		if (!empty($this->fs->msg)) {
			$this->form->msg+=$this->fs->msg;
			return;
			}
		$separator=$_POST['separator'][0];
		if (!$separator) $separator=',';
		# get CSV data, check if number of colums match and convert to x-tab
		$csv=array();
		$rownr=1;
		$handle=fopen($this->dir_page.$this->fs->f, 'r');
		while (($cl=fgetcsv($handle, 1000, $separator))!==false) {
   			if (count($cl)<$cols) array_unshift($cl, $axs['time'].',,'.$axs_user['user'].','.$axs['time']);
			$cl=array_pad($cl, $cols, '');
			if ($tmp=count($cl)>$cols) {
				$this->form->msg[]='Veergude arv ei klapi real '.$rownr.' ('.$tmp.')';
				break;
				}
			$cl=str_replace(array('|', '<?', '?>', '<%', '%>'), array('&brvbar;', '&lt;?', '&gt;?', '&lt;%', '&gt;?'), $cl);
		    $cl='<?php #|'.implode('|', $cl).'|?>'."\n";
			$csv[$rownr]=$cl;
			$rownr++;
		    }
		fclose($handle);
		unlink($this->dir_page.$this->fs->f);
		if ($axs['cfg']['charset']!==$_POST['charset']) foreach ($csv as $k=>$v) $csv[$k]=mb_convert_encoding($v, $axs['cfg']['charset'], $_POST['charset']);
		# Update content only if no error occured
		if (empty($this->form->msg)) {
			# prepend, append or replace
			if ($_POST['position']=='append') $data=array_merge($data, $csv);
			elseif ($_POST['position']=='clear') $data=$csv;
			else $data=array_merge($csv, $data);
			$this->content_upd($data, $c);
			}
		} #</import_page_csv()>
	# ------------ </Import-export functions> ----------
	} #</class::axs_content_write>
#2011-05-30 ?>