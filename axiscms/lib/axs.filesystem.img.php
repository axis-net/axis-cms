<?php #2022-05-10
class axs_filesystem_img extends axs_filesystem { # Filesystem operations with image processing support
	static $ftypes=array(
		'image.save'=>array('jpg'=>'jpg', 'png'=>'png', 'gd'=>'gd', 'gd2'=>'gd2', 'gif'=>'gif', 'wbmp'=>'wbmp', 'xbm'=>'xbm', ),
		);
	#<Save or output image. />
	function img_output($f, $save=false, $q=false, $d=false) {
		if (!$f=$this->img_read($f)) return false;
		$d=($d===false) ? $this->d:$d;
		if (!strlen($q)) $q=90;
		if (!$save) { #<Direct output />
			header('Content-Type: image/jpeg');
			exit(imagejpeg($f, null, $q));
			}
		foreach ($ext=array('save'=>$this->f_ext($save)) as $k=>$v) $ext[$k]=($v==='jpeg') ? 'jpg':$v;
		@ini_set('max_execution_time', (ini_get('max_execution_time')+180));
		if ($this->cfg['image_convert_util']) {
			$dir=dirname(realpath($d.$f)).'/';
			$f_tmp=$this->f_tmp_name($f, $dir_tmp=AXS_PATH_CMS.'data/tmp/');
			imagepng($img, $f_tmp, 5);
			$cmd=$this->cfg['image_convert_util'].' -quality '.$q.' "'.$dir_tmp.$f_tmp.'" "'.$dir.$save.'" 2>&1';
			#$cmd=$this->cfg['image_convert_util'].' -resize '.$width.'x'.$height.' -quality '.$q.' "'.$dir.$f.'" "'.$dir.$save.'" 2>&1';
			@exec($cmd, $output, $retval); //$cmd='/usr/bin/convert identify -list format 2>&1';
			$this->f_del($f_tmp, true, $dir_tmp);
			if (!file_exists($d.$save)) {
				$msg=$this->msg('img.convert_fail', 'Failed to convert file "'.$dir_tmp.$f_tmp.'"->"'.$dir.$save.'"', array('f'=>$dir_tmp.$f_tmp, 'fsave'=>$dir.$save));
				$this->log(__FUNCTION__, __LINE__, $msg."\n".'Command: '.$cmd."\n".'Output: '.implode("	\n", $output)."\n".'Return: '.$retval);
				return $msg;
				}
			}
		else {
			switch ($ext['save']) {
				case 'gd':
					imagegd($f, $d.$save);
					break;
				case 'gd2':
					imagegd2($f, $d.$save);
					break;
				case 'gif':
					imagegif($f, $d.$save);
					break;
				case 'png':
					imagepng($f, $d.$save, 9);
					break;
				case 'wbmp':
					imagewbmp($f, $d.$save);
					break;
				case 'xbm':
					imagexbm($f, $d.$save);
					break;
				case 'jpg':
				default:
					imagejpeg($f, $d.$save, $q);
					break;
				}
			imagedestroy($f);
			if (is_file($d.$save)) chmod($d.$save, 0644);
			}
		} #</img_output()>
	function img_proc($f, $save, $opts) { #Process a single file
		if (!$img=$this->img_read($f, axs_get('dir_read', $opts, false))) return false;
		$crop_pos=axs_get('crop_pos', $opts);
		$crop_size=axs_get('crop_size', $opts);
		$w=axs_get('w', $opts);
		$h=axs_get('h', $opts);
		if (($crop_pos) or ($crop_size)) $img=$this->img_proc_crop($img, $crop_pos, $crop_size, $w, $h);
		if (($w) && ($h)) $img=$this->img_proc_resize($img, $w, $h, axs_get('enlarge', $opts));
		if (($f!==$save) && ($this->f_ext($f)===$this->f_ext($save))) { #<Don't convert file if image didn't change. />
			list($w_orig, $h_orig)=getimagesize($this->d.$f);
			if (($w_orig) && ($h_orig) && ($w_orig===imagesx($img))  && ($h_orig===imagesy($img))) {
				copy($this->d.$f, $this->d.$save);
				return;
				}
			}
		$this->img_output($img, $save, axs_get('q', $opts));
		} #</img_proc()>
	function img_proc_crop($f, $pos, $size, $min_w=0, $min_h=0) {
		/* Crop an image. 
			$f: Filename or image resource. 
			$pos: Box position. 
				Can be absolute position as an array('x'=>int(px), 'y'=>int(px)) or a string like "100,200". 
				Realtive position can be used with a string "30% 50%".
			$size: Box size. 
				Can be absolute size as an array('w'=>int(px), 'h'=>int(px)) or a string like "100x200". 
				Can also specify just the aspect ratio as a string like "1:1" using ":" as a separator.
		*/
		if (!$f=$this->img_read($f)) return false;
		$orig=array('w'=>imagesx($f), 'h'=>imagesy($f));
		$orig['ratio']=$orig['w']/$orig['h'];
		$min=array('w'=>($min_w>0) ? $min_w:$orig['w'], 'h'=>($min_h>0) ? $min_h:$orig['h']);
		#<Parse crop start position and size />
		$opts=array('pos'=>$pos, 'size'=>$size);
		foreach (array('pos'=>array('x','y'), 'size'=>array('w','h')) as $k=>$v) if (!is_array($opts[$k])) {
			$tmp=preg_split('/[^0-9]+/', $opts[$k], -1, PREG_SPLIT_NO_EMPTY);
			$opts[$k]=array();
			foreach ($v as $kk=>$vv) $opts[$k][$vv]=abs(intval(axs_get($kk, $tmp)));
			}
		#<size />
		foreach ($min as $k=>$v) if ($opts['size'][$k]<=0) $opts['size'][$k]=$v;
		$crop_ratio=$opts['size']['w']/$opts['size']['h'];
		# <calculate relative units to px />
		if ((is_string($size)) && (strpos($size, ':')!==false)) {
			if ($opts['size']['w']>=$opts['size']['h']) { #landscape
				$opts['size']['w']=$orig['w'];
				$opts['size']['h']=$opts['size']['w']/$crop_ratio;
				}
			else { #portrait
				$opts['size']['h']=$orig['h'];
				$opts['size']['w']=$opts['size']['h']*$crop_ratio;
				}
			# <prevent underflow />
			if (($opts['size']['w']<$min['w']) && ($opts['size']['h']<$min['h'])) {
				if ($opts['size']['w']>$opts['size']['h']) {
					$opts['size']['w']=$min['w'];
					$opts['size']['h']=$opts['size']['w']/$crop_ratio;
					}
				else {
					$opts['size']['h']=$min['h'];
					$opts['size']['w']=$opts['size']['h']*$crop_ratio;
					}
				}
			}
		# <prevent overflow />
		if ($opts['size']['w']>$orig['w']) {
			$opts['size']['w']=$orig['w'];
			$opts['size']['h']=$opts['size']['w']/$crop_ratio;
			}
		if ($opts['size']['h']>$orig['h']) {
			$opts['size']['h']=$orig['h'];
			$opts['size']['w']=$opts['size']['h']*$crop_ratio;
			}
		#<pos />
		if (($opts['pos']['x']) && (!strlen($opts['pos']['y']))) $opts['pos']['y']=$opts['pos']['x'];
		if ((is_string($pos)) && (strpos($pos, '%')!==false)) foreach (array('w'=>'x','h'=>'y') as $k=>$v) {
			$opts['pos'][$v]=round(($orig[$k]-$opts['size'][$k])/100*$opts['pos'][$v]);
			#<set limits to prevent overflow />
			if (($opts['pos'][$v]+$opts['size'][$k])>$orig[$k]) $opts['pos'][$v]=$orig[$k]-$opts['size'][$k];
			}
		foreach ($opts as $k=>$v) foreach ($v as $kk=>$vv) $opts[$k][$kk]=ceil($vv);
		#<Do crop />
		#dbg($opts);
		$new=imagecreatetruecolor($opts['size']['w'], $opts['size']['h']);
		imagecopy($new, $f, 0, 0, $opts['pos']['x'], $opts['pos']['y'], $opts['size']['w'], $opts['size']['h']);
		//dbg($pos,$size,$opts['pos']['x'], $opts['pos']['y'], $opts['size']['w'], $opts['size']['h']);
		//imagedestroy($f);
		return $new;
		} #</img_proc_crop()>
	function img_proc_resize($f, $w, $h, $enlarge=false) {
		if (!$f=$this->img_read($f)) return false;
		# Get new dimensions
		$w_orig=imagesx($f);
		$h_orig=imagesy($f);
		if (!$enlarge) { # if image already small enough and jpg, copy file instead of resample
			if (($w_orig<=$w) && ($h_orig<=$h)) return $f;
			}
		$ratio=$w_orig/$h_orig;
		if (($w/$h)>$ratio) $w=intval(ceil($h*$ratio));
		else $h=intval(ceil($w/$ratio));	
		# Resample
		$new=imagecreatetruecolor($w, $h);
		imagecopyresampled($new, $f, 0, 0, 0, 0, $w, $h, $w_orig, $h_orig);
		imagedestroy($f);
		return $new;
		} #</img_proc_resize()>
	static $img_proc_ui_fields=array(
		'proc'=>array('type'=>'checkbox', 'value'=>'1', ),
		'w'=>array('type'=>'number', 'value'=>'', 'min'=>1, 'max'=>20000, 'step'=>1),
		'h'=>array('type'=>'number', 'value'=>'', 'min'=>1, 'max'=>20000, 'step'=>1),
		'crop_pos'=>array('type'=>'text', 'value'=>'', 'size'=>5, 'maxlength'=>11, ),
		'crop_size'=>array('type'=>'text', 'value'=>'', 'size'=>5, 'maxlength'=>11, ),
		#'ext'=>array(), 
		'q'=>array('type'=>'number', 'value'=>'', 'min'=>1, 'max'=>100, 'step'=>1),
		);
	static function img_proc_ui(array $vl=array(), $tr=false, $opts=false, $n='img', $disabled='') {
		global $axs;
		if (!$opts) $opts=axs_admin::$media_defaults['img'];
		if (!$tr) $tr=axs_tr::load_class_tr(__CLASS__, $axs['cfg']['cms_lang']);
		$f='';
		$readonly=array();
		#$cl['img_proc_disabled']
		/*if (!axs_function_ok('getimagesize') or !axs_function_ok('imagecreatetruecolor') or !axs_function_ok('imagecopyresampled') or !axs_function_ok('imagejpeg')) {
			$cl['img_proc_disabled']=' disabled="disabled"';
			$cl['img_proc_msg']='SERVERI VIGA! Pildit&ouml;&ouml;tlusfunktsioonid ei t&ouml;&ouml;ta.';
			}
		else $cl['img_proc_disabled']=$cl['img_proc_title']='';*/
		foreach ($opts as $k=>$v) if (is_array($v)) {
			$fields=self::$img_proc_ui_fields;
			foreach ($fields as $kk=>$vv) {
				$readonly[$kk]='';
				if (isset($v[$kk])) {
					if (is_array($v[$kk])) {
						$fields[$kk]['type']='select';
						$fields[$kk]['options']=$v[$kk];//array_merge($fields[$kk], $v[$kk]);
						}
					if ($v[$kk]===false) {
						$readonly[$kk]=' disabled';
						$fields[$kk]['readonly']=' readonly="readonly"';
						}
					}
				}
			foreach ($fields as $kk=>$vv) {	$fields[$kk]['value']=$vl[$n][$k][$kk];	}
			$name=$n.'['.$k.']';
			if ($fields['proc']['type']==='select') {
				foreach ($fields['proc']['options'] as $kk=>$vv) $fields['proc']['options'][$kk]='<option value="'.$kk.'"'.(($fields['proc']['value'].''===$kk.'')?' selected="selected"':'').$disabled.'>'.axs_html_safe($vv).'</option>';
				$fields['proc']='<select name="'.$name.'[proc]" type="select"'.$disabled.'>'.implode('', $fields['proc']['options']).'</select>';
				}
			elseif ($fields['proc']['type']==='checkbox') $fields['proc']='<input name="'.$name.'[proc]" type="checkbox" value="1"'.(($fields['proc']['value'])?' checked="checked"':'').' />';
			foreach ($fields as $kk=>$vv) if (is_array($vv)) {
				if (($vv['type']==='number') || ($vv['type']==='text')) {
					foreach ($vv as $kkk=>$vvv) $vv[$kkk]=$kkk.'="'.htmlspecialchars($vvv).'"';
					$fields[$kk]='<input name="'.$name.'['.$kk.']" '.implode(' ', $vv).' />';
					}
				}
			$f.=
			'			<fieldset class="img_proc" id="'.$n.'_proc_'.$k.'">'."\n".
			'				<legend><label>'.$fields['proc'].' <span class="lbl">'.$tr->s('img.proc_'.$k.'.lbl').'</span></label></legend>'."\n".
			'				<div class="size'.$readonly['w'].$readonly['h'].'">'."\n".
			'					<label><span class="visuallyhidden">x</span>'.$fields['w'].'</label>&times<label><span class="visuallyhidden">y</span>'.$fields['h'].'</label><abbr class="hint" title="pixel" lang="en">px</abbr>'."\n".
			'				</div>'."\n".
			'				<label class="crop_pos'.$readonly['crop_pos'].'"><span class="lbl">'.$tr->s('img.crop_pos.lbl').'</span> '.$fields['crop_pos'].'<span class="hint">(50px/%50px/%)</span></label>'."\n".
			'				<label class="crop_size'.$readonly['crop_size'].'"><span class="lbl">'.$tr->s('img.crop_size.lbl').'</span> '.$fields['crop_size'].'<span class="hint">(50px/%50px/%, 1:1)</span></label>'."\n".
			'				<label class="q'.$readonly['q'].'"><span class="lbl">'.$tr->s('img.q.lbl').'</span> '.$fields['q'].'<span class="hint">%</span></label>'."\n".
			'			</fieldset>'."\n";
			}
		$fields['format']['value']=axs_get('format', $vl[$n], '');
		$format=array_merge(array(''=>'-'), self::$ftypes['image.save']);
		foreach ($format as $k=>$v) {
			$tmp=($k===$fields['format']['value']) ? ' selected="selected"':'';
			$format[$k]='<option value="'.$k.'"'.$tmp.'>'.$v.'</option>';
			}
		$f.='			<label class=format"><span class="lbl">'.$tr->s('img.format.lbl').'</span> <select name="'.$n.'[format]">'.implode('', $format).'</select></label>'."\n";
		return $f;
		} #</img_proc_ui()>
	function img_proc_ui_save($f, $vl, $media_defaults=array()) {
		$d=$this->d;
		$f_ext=$this->f_ext($f);
		$fnoext=preg_replace('/\.'.$f_ext.'$/i', '', $f);
		$ftmp=$this->f_tmp_name($f);
		list($width_orig, $height_orig)=getimagesize($d.$f);
		$orig=array('w'=>$width_orig, 'h'=>$height_orig);
		$files_saved=array('t'=>$f, );
		$proc=false;
		foreach($media_defaults['img'] as $k=>$v) if (!empty($vl[$k]['proc'])) {
			foreach ($v as $kk=>$vv) $vl[$k][$kk]=(strlen($tmp=axs_get($kk, $vl[$k]))) ? $tmp:$vv;
			foreach (array('w','h') as $kk=>$vv) if ($orig[$vv]>$vl[$k][$vv]) $proc=true;
			}
		if (!isset(self::$ftypes['image.save'][$vl['format']])) $vl['format']='';
		if (!empty($vl['format'])) $proc=true;
		if (empty($proc)) return $files_saved;
		if (empty($vl['format'])) $vl['format']='jpg';
		$files_saved=array();
		$this->rename($f, $ftmp);
		#<attempt to delete tmp file if it exists for some reason after script exit>
		$ftmp_path=realpath($d.$ftmp);
		register_shutdown_function(create_function('', 'if (file_exists(\''.$ftmp_path.'\')) unlink(\''.$ftmp_path.'\');'));
		#</attempt to delete tmp file>
		if ($vl['0']['proc']) {
			$files_saved['t']=$fnoext.($vl['s']['proc'] ? '.t':'').'.'.$vl['format'];
			$this->img_proc($ftmp, $files_saved['t'], $vl['0']);
			}
		if ($vl['s']['proc']) {
			$files_saved['s']=$fnoext.'.'.$vl['format'];
			$this->img_proc($ftmp, $files_saved['s'], $vl['s']);
			}
		if ((!$vl['0']['proc']) && (!$vl['s']['proc']) && ($vl['format']!==$f_ext)) {
			$files_saved['s']=$fnoext.'.'.$vl['format'];
			foreach ($orig as $k=>$v) {	$vl['0'][$k]=$v;	}
			$this->img_proc($ftmp, $files_saved['s'], $vl['0']);
			}
		$this->f_del($ftmp, false);
		return $files_saved;
		} #</img_ui_save()>
	function img_read($f, $d=false) {
		if (!is_string($f)) return $f;
		$d=($d===false) ? $this->d:$d;
		if (!file_exists($d.$f)) {
			$this->msg('f_not_found', 'File not found: "'.$d.$f.'"!', array('f'=>$d.$f));
			return false;
			}
		$f_ext=$this->f_ext($f);
		$img=false;
		switch ($f_ext) { # open image file
			case 'jpg':
			case 'jpeg':
				$img=imagecreatefromjpeg($d.$f);
				break;
			case 'gif':
				$img=imagecreatefromgif($d.$f);
				break;
			case 'png':
				$img=imagecreatefrompng($d.$f);
				break;
			case 'wbmp':
				$img=imagecreatefromwbmp($d.$f);
				break;
			case 'xbm':
				$img=imagecreatefromxbm($d.$f);
				break;
			case 'tif':
			case 'tiff':
				if ($this->cfg['image_convert_util']) { #<Convert to PNG and read into memory />
					$dir=dirname(realpath($d.$f)).'/';
					$f_tmp=$this->f_tmp_name('tmp.png', $dir_tmp=AXS_PATH_CMS.'data/tmp/');
					$cmd=$this->cfg['image_convert_util'].' "'.$dir.$f.'" "'.$dir_tmp.$f_tmp.'" 2>&1';
					@exec($cmd, $output, $retval); //$cmd='/usr/bin/convert identify -list format 2>&1';
					$img=imagecreatefrompng($dir_tmp.$f_tmp);
					$this->f_del($f_tmp, true, $dir_tmp);
					break;
					}
			default: #<File format not supported />
				$this->msg('img.unsupported', 'Image processing function does not support file format "'.$f_ext.'"!', array('f'=>$f_ext));
				return false;
			}
		return $img;
		} #</img_read()>
	function img_watermark($f, $mark, $left='50%', $top='50%', $size=0, $opts=array()) {
		if (!$f=$this->img_read($f)) return false;
		if (!$mark=$this->img_read($mark)) return false;
		$sizes=array(
			'img'=>array('x'=>imagesx($f), 'y'=>imagesy($f)),
			'mark'=>array('x'=>imagesx($mark), 'y'=>imagesy($mark)),
			);
		foreach ($pos=array('x'=>$left, 'y'=>$top) as $k=>$v) if (strpos($v, '%')) $pos[$k]=round(($sizes['img'][$k]-$sizes['mark'][$k])/100*$v);
		imagecopy($f, $mark, $pos['x'], $pos['y'], 0, 0, $sizes['mark']['x'], $sizes['mark']['y']);
		return $f;
		} #</img_watermark()>
	function img_watermark_generate($text, $size, $opts=array()) {
		foreach (array(
			'font'=>'arial.ttf',
			'color'=>array(255,255,255),
			'opacity'=>64, #0-127
			) as $k=>$v) if (!isset($opts[$k])) $opts[$k]=$v;
		if (strpos($opts['font'], '/')===false) $opts['font']=axs_dir('plugins').$opts['font'];
		$s=imagettfbbox($size, 0, $opts['font'], $text);
		$w=($w=abs($s[0])+abs($s[2]))+($w/100*5);
		$h=abs($s[5])+abs($s[1]);
		$mark=imagecreatetruecolor($w, $h);
		imagesavealpha($mark, true);
		$bg=imagecolorallocatealpha($mark, 0, 0, 0, 127);
		imagefill($mark, 0, 0, $bg);
		$color=imagecolorallocatealpha($mark, $opts['color'][0], $opts['color'][1], $opts['color'][2], $opts['opacity']);
		imagettftext($mark, $size, 0, abs($s[0]), abs($s[5]), $color, $opts['font'], $text);
		return $mark;
		} #</img_watermark_generate()>
	} #</class::axs_filesystem>
#2016-05-17 ?>