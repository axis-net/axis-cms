<?php #2017-11-14
class axs_form_site extends axs_form_edit {
	var $page_key='p';
	var $sql_parent_id=0;
	var $line_size=70;
	static $ipaddr=false;
	static $hostname=false;
	function __construct($axs_local, &$structure, &$values) {
		global $axs, $axs_content;
		$form_id=$this->target_id=($axs_local['section']==='content') ? $axs_local['c'].'_form':$axs_local['c'].'_banner_form';
		axs_form::__construct($axs['site_nr'], $axs_local['url'], $structure, $axs_local['dir'], $values, $form_id);
		
		$this->axs_local=$axs_local;
		$this->c=$axs_local['c'];
		if (!empty($this->cfg['url'])) $this->url=$this->cfg['url'];
		$this->url_root=$axs['http_root'].$axs['site_dir'].'?';
		$this->from_name=((!empty($this->cfg['mail_name_field'])) && (!empty($this->user_input[$this->cfg['mail_name_field']]))) ? $this->user_input[$this->cfg['mail_name_field']]:false;
		$this->from_email=false;
		foreach ($this->structure_get_by_type('email') as $k=>$v) {
			$this->from_email=axs_get($k, $this->user_input);
			$this->cfg['mail_addr_field']=$k;
			break;
			}
		$this->mailto=$this->value_split(axs_get('mailto', $this->cfg));
		if ($this->from_email) $this->mailto[]=$this->from_email;
		foreach ($this->mailto as $k=>$v) $this->mailto[$k]=trim($v);
		$this->mailto=implode(', ', array_unique($this->mailto));
		$this->subject='Post: '.implode('<', array_reverse(axs_content::path_get(false, 'title')));
		$this->mail_send=(!empty($this->mailto) && $axs['mail()']) ? true:false;
		
		$this->posts_save=intval(axs_get('posts_save', $this->cfg));
		$this->posts_show=axs_get('posts_show', $this->cfg);
		$this->cfg['posts_order']=(axs_get('posts_show', $this->cfg)==='desc') ? true:false;
		if ($this->mail_send && !$axs['mail()']) {
			$this->mail_send=false;
			$this->disabled=' disabled="disabled"';
			$this->msg['mail']='SERVER ERROR! mail() function disabled on '.$_SERVER['SERVER_NAME'];
			}
		if (!$this->mail_send && !$this->posts_save) $this->disabled=' disabled="disabled"';
		} #</axs_form_site()>
	static function ip_host_get($get='') { # get IP and DNS name of the sender
		if (self::$ipaddr===false) {
			self::$ipaddr=$_SERVER['REMOTE_ADDR'];
			self::$hostname=@gethostbyaddr(self::$ipaddr);
			}
		switch ($get) {
			case 'ip':
				return self::$ipaddr;
				break;
			/*case 'ipnr':
				return axs_form_site::ip_to_number(self::$ipaddr);
				break;*/
			case 'hostname':
				return self::$hostname;
				break;
			case 'hostname-ip':
				return self::$hostname.' ('.self::$ipaddr.')';
				break;
			default: return array('ip'=>self::$ipaddr, 'hostname'=>self::$hostname, );
			}
		} # </ip_host_get()>
	function mail($txt, $html='', $attach=array()) {
		global $axs, $axs_content;
		if (!$this->mail_send) return false;
		$message=$this->tr->t('msg')."\n".str_repeat('=', $this->line_size)."\n".$txt."\n\n".
		$axs['http'].'://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']."\n".
		'Link from: '.$_SERVER['HTTP_REFERER']."\n".
		'Sender: '.$this->ip_host_get('hostname-ip').' at '.date('d.m.Y H:i:s')."\n".
		'AXIS CMS http://'.AXS_DOMAIN.'/axiscms/?r='.urlencode(ltrim($_SERVER['SERVER_NAME'].$axs['http_root'], '/'));
		$message=wordwrap($message, $this->line_size);
		$from_email=($this->from_email) ? $this->from_email:$this->mailto;
		return axs_mail($this->mailto, $this->subject, $message, '', $this->from_name, $from_email, $attach);
		} #</mail()>
	function message_compact($format='txt', $exclude=array()) {
		$txt=array();
		foreach ($this->structure as $k=>$v) if ($k) {
			if ((isset($exclude[$k])) or ((!$this->element_writable($v)) && ($v['type']!='fieldset') && ($v['type']!='fieldset_end'))) continue;
			$txt[$k]=$v;
			}
		if ($format=='html') {
			if (isset($this->cfg['mail_name_field'])) unset($txt[$this->cfg['mail_name_field']]);
			if (isset($this->cfg['mail_addr_field'])) unset($txt[$this->cfg['mail_addr_field']]);
			foreach ($txt as $k=>$v) {
				if ($this->structure[$k]['type']=='fieldset') unset($txt[$k]);
				else $txt[$k]=$this->value_display($k, $this->structure[$k], $this->values);
				}
			if (count($txt)>1) {
				foreach ($txt as $k=>$v) $txt[$k]='<dt>'.axs_html_safe($this->structure[$k]['label']).'</dt><dd>'.$v.'</dd>';
				$txt='     <dl>'.implode(' ', $txt).'</dl>'."\n";
				}
			else $txt=implode('', $txt);
			}
		else {
			$sepr=str_repeat('-', $this->line_size);
			$sepr_fieldset=str_repeat('=', $this->line_size);
			//if (count($txt)>1) 
			foreach ($txt as $k=>$v) switch ($v['type']) {
				case 'fieldset':
					$txt[$k]="\n".$v['label']."\n";
					break;
				case 'fieldset_end':
					$txt[$k]=$sepr_fieldset."\n";
					break;
				default:
					$txt[$k]=$sepr."\n".$v['label'].': '.$this->value_display($k, $this->structure[$k], $this->values, $tmp=array(), false)."\n";
					break;
				}
			$txt=implode('', $txt).$sepr;
			}
		return $txt;
		} #</message_compact()>
	function save($message) { # save post
		global $axs, $axs_content;
		if (!empty($this->sql_table)) { #<If save to SQL>
			if ($this->sql_parent_id<1) axs_log(__FILE__, __LINE__, 'comments', 'Invalid $sql_parent_id', true);
			$this->values['time']=time();
			$this->values['user_ip']=$this->ip_host_get('ipnr');
			$this->values['hostname']=$this->ip_host_get('hostname');
			axs_db_query("INSERT INTO `".$this->sql_table."` SET `parent_id`='".$this->sql_parent_id."', ".$this->sql_values_set($this->values), '', $this->db, __FILE__, __LINE__);
			$this->values['user_ip']=$this->ip_host_get('hostname-ip');
			return;
			} #</If save to SQL>
		# prepare entry
		if (strlen($message)>5000) $message=substr($message, 0, 5000).' . . .';
		$entry=array(
			'time'=>$axs['time'], 'from_name'=>$this->from_name, 'from_email'=>$this->from_email, 'message'=>'error', 'ipaddr'=>$this->ip_host_get('ip'), 'hostname'=>substr($this->ip_host_get('hostname'), 0, 1000),
			);
		# Sanitize and bbcode entry
		foreach ($entry as $k=>$v) $entry[$k]=htmlspecialchars($v, ENT_QUOTES);
		$entry['message']=preg_replace("/((\r\n)+|(\n\r)+|\n+)/", '', $message); #<Enable HTML />
		$entry['message']=$this->bbcode($entry['message']); # <Enable BB code />
		$entry=str_replace('|', '&brvbar;', $entry);
		$entry=preg_replace("/((\r\n)+|(\n\r)+|\n+)/", '', nl2br(implode('|', $entry)));
		# find the last file
		$files=array();
		$strlen=strlen($this->c.'_'.$axs['l'].'.post');
		$dir=axs_dir('content').$axs_content['content'][$this->c]['dir'];
		$handle=opendir($dir);
		while (($file=readdir($handle))!==false) {
			if (strncmp($file, $this->c.'_'.$axs['l'].'.post', $strlen)==0) $files[]=$file;
			}
		closedir($handle);
		if (!empty($files)) {
			rsort($files);
			$f=$dir.$files[0];
			if (count(file($f))>=$this->posts_save) $f=$dir.$this->c.'_'.$axs['l'].'.post'.substr(
				substr($files[0], $strlen, 4) # find current nr
				+10001, 1) # add new nr + zerofill
				.'.php';
			}
		else $f=$dir.$this->c.'_'.$axs['l'].'.post0001.php';
		if (!file_exists($f)) fclose(fopen($f, 'a+'));
		# check file permissions
		if (!is_writable($f)) {
			@chmod(axs_dir('content'), 0644);
			@chmod($dir, 0644);
			@chmod($f, 0644);
			}
		# open file end, lock, write, close file
		flock($fp=fopen($f, 'a+'), LOCK_EX);
		fwrite($fp, '<?php #|'.$entry.'|?>'."\n");
		fclose($fp);
		if (!is_writable($f)) { # report error if file not writable
			$this->msg[]='Error: media write-protected.';
			axs_log(__FILE__, __LINE__, 'chmod', 'Error writing file "'.$f.'"');
			}	 
		} # </save()>
	function value_split($value, $name='', $type='') {
		$value=preg_split('~,~', '-,'.str_replace(';', ',', trim(strip_tags($value))), -1, PREG_SPLIT_NO_EMPTY);
		unset($value[0]);
		if ($name && $type) {
			if (count($value)>1) {
				$this->structure[$name]['type']=$type;
				foreach ($value as $k=>$v) $this->structure[$name][$type][$k]=array('value'=>$k, 'label'=>str_replace('@', '(&auml;t)', $v), );
				}
			else {
				$this->structure[$name]['type']='hidden';
				$this->user_input[$name]=current($value);
				}
			}
		return $value;
		} # </value_split()>
	function post($sql=array(), $redir=true) {
		global $axs;
		if (!$this->save) return;
		$this->values=$this->form_input();
		$this->validate($this->values);
		if (!empty($this->msg)) return;
		axs_log(__FILE__, __LINE__, 'site_post');
		if ($this->mail_send) $this->mail($this->message_compact('txt'));
		if ($this->posts_save) $this->save($this->message_compact('html'));
		$id=false;
		if (!empty($this->structure['']['sql'])) {
			if (!empty($this->structure['']['sql']['values'])) foreach ($this->structure['']['sql']['values'] as $k=>$v) {
				$this->structure[$k]=array('type'=>'text', 'size'=>-1, );
				$this->values[$k]=$v;
				}
			$id=axs_db_query("INSERT INTO `".$this->structure['']['sql']['table']."` SET ".$this->sql_values_set($this->values), 'insert_id', $this->db, __FILE__, __LINE__);
			}
		if ($redir) axs_exit(axs_redir($this->url_root.axs_url($this->url, array($this->form_id.'_msg'=>'ok'), false).'#'.$this->form_id));
		return $id;
		} #</post()>
	function build($templates=array()) {
		global $axs, $axs_content;
		if (empty($this->structure)) return '';
		# <Templates />
		foreach ($templates as $k=>$v) $this->tpl[$k]=$v;
		foreach (array('form',) as $v) if (!isset($this->tpl[$v])) $this->tpl[$v]=axs_tpl(false, 'index.'.$v.'.tpl');
		if (!isset($this->tpl['required'])) $this->tpl['required']=axs_tpl_parse($this->templates('required'), $this->tr->tr);
		foreach (array(
			'id'=>$this->form_id,
			'formact_url'=>$this->url_root.axs_url($this->url, array('axs'=>array($this->key_form_id=>$this->target_id))).'#'.(($this->posts_save)?preg_replace('/_form$/','_list',$this->target_id):$this->target_id),
			'posts'=>'',
			) as $k=>$v) $this->vr[$k]=$v;
		if (isset($_GET[$this->form_id.'_msg'])) {
			$this->msg[]=$this->tr->s('msg');
			$this->vr['class']['post_ok']='post_ok';
			}
		# <Show posts />
		$this->vr['posts']=($this->posts_show) ? $this->build_comments():'';
		# <Form />
		$this->vr['msg']=$this->msg_html();
		#<Simple spam protection element />
		$this->structure=array_merge(array('_captcha'=>$this->types['_captcha']), $this->structure);
		foreach ($this->structure_get() as $k=>$v) {
			//$this->element($k, $v, $this->tpl['required']);
			if ($this->disabled) $v['disabled']='disabled';
			unset($v['add_fn']);
			//$v['input']=$this->element_input_html($k, $v, $this->user_input);
			//$this->vr['elements'].=axs_tpl_parse($this->templates($v), $v);
			$this->vr['elements'].=$this->element_html($k, $v, $this->user_input);
			}
		$this->vr['class']=implode(' ', $this->vr['class']);
		$this->css_js();
		return axs_tpl_parse($this->tpl['form'], $this->vr);
		} #</build()>
	function build_comments() {
		global $axs, $axs_content;
		$vr=array('posts'=>'', 'pager'=>'', 'total'=>'', );
		foreach (array('form.posts','form.posts.item','pager') as $v) if (!isset($this->tpl[$v])) $this->tpl[$v]=axs_tpl(false, 'index.'.$v.'.tpl');
		$pager=new axs_pager(axs_get($this->page_key, $_GET), $this->posts_save, $this->posts_save, array(''=>$this->tpl['pager']));
		$posts=array();
		if ($this->sql_table) {
			$pager->sql($this->cfg['posts_order'], 'time');
			$result=axs_db_query("SELECT SQL_CALC_FOUND_ROWS * FROM `".$this->sql_table."`\n".
			"	WHERE `parent_id`='".$this->sql_parent_id."' ORDER BY `time`".$pager->desc.' LIMIT '.$pager->start.','.$pager->psize,
			1, $this->db, __FILE__, __LINE__);
			$this->vr['total']=$vr['total']=axs_db_query('', 'found_rows', $this->db, __FILE__, __LINE__);
			foreach ($result as $cl) {
				$vl=array();
				foreach ($this->structure_get_thead() as $k=>$v) $vl[$k]=$this->value_display($k, $v, $cl, $vl);
				$posts[]=$vl;
				}
			unset($result);
			}
		else {
			$tmp=axs_content::posts_get($pager->p, $this->posts_save, $this->cfg['posts_order'], $axs['time'], $this->c);
			$vr['total']=$tmp['total'];
			$posts=$axs_content['content'][$this->c]['post'];
			}
		foreach ($posts as $k=>$v) $vr['posts'].=axs_tpl_parse($this->tpl['form.posts.item'], $v);
		$vr['pager']=$pager->pages($vr['total'], $this->url_root.axs_url($this->url, array($this->page_key=>'')));
		unset($this->structure['time'], $this->structure['user_ip'], $this->structure['hostname']);
		return axs_tpl_parse($this->tpl['form.posts'], $vr);
		} #</build_comments()>
	} #</class::axs_form_site>
#2005 ?>