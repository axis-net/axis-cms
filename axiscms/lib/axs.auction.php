<?php #2021-11-25
	public $currency='EUR';
	public $bid_step=array(0=>25, 500=>50, 1000=>100, );
	public $bid_precision=array(0=>50, 1000=>100, );
	public $bid_end_time=22;
	public $bid_end_warning_mail=3600; #seconds
	public $bid_extend=300; #seconds
	public $ok_act=86400;
	public $ajax_update=array();
	static $data_fields=array(
		'class'=>'_class', 'msg'=>'msg', 'disabled'=>'disabled',
		'form'=>array('bid_next'=>array('value'=>'bid.next', 'step'=>'bid.step', )),
		'html'=>array(
			'price_bid'=>'price_bid.fmt', 'bid_count'=>'bid_count', 'price_bid_time'=>'price_bid_time.fmt',
			'bid_you'=>'bid_you.fmt', 'bid_you_time'=>'bid_you_time.fmt', 'time_end'=>'time_end.fmt',
			'bid_step'=>'bid.step', 'bid_next'=>'bid.next', 
			),
		'datetime'=>array('price_bid_time'=>'price_bid_time', 'bid_you_time'=>'bid_you_time', 'time_end'=>'time_end'),
		);
	static $tr=array(
		'en'=>array(
			'mail_addr'=>'info@onlinearthaus.com',
			'msg_reg'=>'To make a bid You must be registered and logged in.',
			'msg_bid_start'=>'Bidding has not started yet.',
			'msg_terms'=>'To make a bid You must agree to the terms and conditions!',
			'msg_bid_reserve'=>'Your bid is under the reserve price, rise your offer.',
			'msg_bid_low'=>'This bid has already been made!',
			'msg_bid_step'=>'The bid step must be',
			'msg_bid_ok'=>'Your bid has been received.',
			'msg_bid_mail'=>'The item received a new bid',
			'msg_end'=>'The bidding is closed.',
			'watchlist_add.lbl'=>'Add to watchlist',
			'watchlist_rem.lbl'=>'Remove from watchlist',
			'watchlist_add.msg'=>'Added to watchlist.',
			'watchlist_rem.msg'=>'Removed from watchlist.',
			),
		'et'=>array(
			'mail_addr'=>'info@onlinearthaus.com',
			'msg_reg'=>'NB! Oksjonil pakkumiste tegemiseks peate olema kasutajaks registreerunud ja sisse loginud. ',
			'msg_bid_start'=>'Pakkumine ei ole veel alanud.',
			'msg_terms'=>'Pakkumise tegemiseks peate n&otilde;ustuma oksjonil osalemise tingimustega!',
			'msg_bid_reserve'=>'Pakkumine jäi alla reservhinna, palun tehke suurem pakkumine!',
			'msg_bid_low'=>'Selline pakkumine on juba tehtud!',
			'msg_bid_step'=>'Pakkumise samm peab olema',
			'msg_bid_ok'=>'Teie pakkumine on edastatud.',
			'msg_bid_mail'=>'Teosele tehti uus pakkumine',
			'msg_end'=>'Pakkumine on lõppenud.',
			'watchlist_add.lbl'=>'Lisa tähelepanulisti',
			'watchlist_rem.lbl'=>'Eemalda tähelepanulistist',
			'watchlist_add.msg'=>'Lisatud tähelepanulisti.',
			'watchlist_rem.msg'=>'Tähelepanulistist eemaldatud.',
			),
		);
	function __construct($table, $site_nr=1, $tr=false) {
		global $axs, $axs_user;
		$this->site_nr=$site_nr;
		$this->db=$axs['cfg']['site'][$this->site_nr]['db'];
		$this->table=$table;
		$this->uid=intval(axs_get('id', $axs_user));
		if ($tr) $this->tro=$tr;
		else {
			$this->tro=(isset(self::$tr[$axs['l']])) ? self::$tr[$axs['l']]:current(self::$tr);
			$this->tro=new axs_tr($this->tro);
			}
		$this->tpl=array('msg'=>'<ul class="msg">{$msg}</ul>', );
		$this->msg=array();
		$axs['page']['head']['axs.auction.js']='<script src="'.axs_dir('lib', 'http').'axs.auction.js'.'"></script>'."\n";
		} #</__construct()>
	function ajax_update() { #<Update data via AJAX>
		global $axs;
		if (!isset($axs['get'][__CLASS__])) return;
		header('Content-Type: application/json');
		axs_exit(json_encode($this->ajax_update));
		} #</ajax_update()>
	function msg_display($msg) {
		foreach ($msg as $k=>$v) $msg[$k]='<li class="'.$k.'">'.$v.'</li>';
		$msg=(!empty($msg)) ? axs_tpl_parse($this->tpl['msg'], array('msg'=>implode('', $msg), )):'';
		return $msg;
		} #</msg_display()>
	function bid_precision($bid) {
		reset($this->bid_precision);
		$bid_precision=current($this->bid_precision);
		foreach ($this->bid_precision as $k=>$v) {
			if ($k>$bid) break; 
			$bid_precision=$v;
			}
		return $bid_precision;
		} #</bid_precision()>
	/*function bid_step($bid) {
		reset($this->bid_step);
		$bid_step=current($this->bid_step);
		foreach ($this->bid_step as $k=>$v) {
			if ($k>$bid) break; 
			$bid_step=$v;
			}
		return $bid_step;
		} #</bid_step()>*/
	function bid_next($bid) {
		$bid_step=$this->bid_step($bid);
		$bid_precision=$this->bid_precision($bid);
		$bid_next=$bid+$bid_step;
		if ($bid_next%$bid_precision!=0) for ($i=1; $i<1000; $i++) {
			if ($i*$bid_precision>$bid_next) {	$bid_next=$i*$bid_precision;	break;	}
			}
		return $bid_next;
		} #</bid_next()>
	function bid_open(&$cl, &$vr) {
		global $axs;
		$cl['uid']=$this->uid;
		$this->bid_step($cl);
		if (!isset($cl['msg'])) $cl['msg']=array();
		$cl['bid.open']=false;
		#<Check time>
		if ($cl['time_start']>$axs['time']) $cl['msg']['msg_bid_start']=$this->tro->s('msg_bid_start');
		else {
			if ($cl['time_end']) {
				if ($cl['time_end']>$axs['time']) $cl['bid.open']=true;
				else {
					$vr['_class']['bid_end']='bid_end';
					$cl['msg']['msg_end']=$this->tro->s('msg_end');
					}
				}
			else $cl['bid.open']=true;
			}
		#</Check time>
		if (!$this->uid) { #<Check user>
			$cl['bid.open']=false;
			$cl['msg']['msg_reg']=$this->tro->s('msg_reg');
			} #</Check user>
		$cl['bid.next']='';
		foreach (array('price_start', 'price_bid') as $v) if ($cl[$v]) $cl['bid.next']=$cl[$v];
		if ($cl['bid_count']) {
			if (!$cl['bid.next']) $cl['bid.next']=0;
			$cl['bid.next']+=$cl['bid.step'];
			}
		} #</bid_open()>
	function bid_make(&$cl, &$vr, $item_name='') { #<Submit bid>
		global $axs;
		#<Validate>
		if ((!$cl['bid.open']) || (!isset($_POST['bid_make']))) return;
		if (!$cl['bid_you'] && !isset($_POST['terms_agree'])) $cl['msg']['msg_terms']=$this->tro->s('msg_terms');
		
		$cl['bid.next']=intval(str_replace(' ', '', trim($_POST['bid_next'])));
		if (!empty($cl['price_reserve'])) {
			if ($cl['bid.next']<$cl['price_reserve']) $cl['msg']['msg_bid_reserve']=$this->tro->s('msg_bid_reserve');
			}
		if (empty($cl['msg'])) {
			if (!$cl['bid_count']) {
				if ($cl['bid.next']<$cl['price_bid']) $cl['msg']['msg_bid_low']=$this->tro->s('msg_bid_low');
				}
			else {
				if ($cl['bid.next']<=$cl['price_bid']) $cl['msg']['msg_bid_low']=$this->tro->s('msg_bid_low');
				}
			}
		if ($cl['bid.next']<=0) $cl['msg']['msg_bid_low']=$this->tro->s('msg_bid_low');
		#Kui pakkumine on üle alghinna, peab klappima pakkumise sammuga
		//$tmp=($cl['bid.next']-$cl['price_bid'])/$cl['bid.step'];
		//if (intval($tmp)!==$tmp) $cl['msg']['msg_bid_step']=$this->tro->s('msg_bid_step').' '.$cl['bid.step'];
		//dbg($cl['bid.next'],$cl['price_bid'],$cl['price_bid']+$cl['bid.step']);
		if (empty($this->msg)) { #Kui pakkumine on üle alghinna, peab klappima pakkumise sammuga
			if (($cl['bid.next']>$cl['price_bid']+$cl['bid.step']) && ($cl['bid.next']%$this->bid_precision($cl['bid.next'])!=0)) $cl['msg']['msg_bid_step']=$this->tro->s('msg_bid_step').' '.$cl['bid.step'];
			}
		#</Validate>
		if (empty($cl['msg'])) { #<Accept new bid>
			axs_db_query("INSERT INTO `".$this->table."_bid` SET `pid`='".$cl['id']."', `time`='".$axs['time']."', `uid`='".$this->uid."', `bid`='".$cl['bid.next']."'", '', $this->db, __FILE__, __LINE__);
			if ((!$cl['time_end']) && ($cl['time_lasts'])) $cl['time_end']=mktime($this->bid_end_time, 0, 0, date('n'), date('j')+intval($cl['time_lasts']), date('Y'));
			#<Extend time>
			if ($cl['time_end']) {
				if ($axs['time']>$cl['time_end']-$this->bid_extend) $cl['time_end']=$axs['time']+$this->bid_extend;
				}
			#</Extend time>
			axs_db_query("UPDATE `".$this->table."` AS p, `".$this->table."_bid` AS b SET p.`price_bid`=b.`bid`, p.`price_bid_uid`=b.`uid`, p.`price_bid_time`=b.`time`,  p.`time_end`='".$cl['time_end']."'\n".
			"	WHERE p.`id`='".$cl['id']."' AND b.`id`=(SELECT `id` FROM `".$this->table."_bid` WHERE `pid`='".$cl['id']."' ORDER BY `time` DESC LIMIT 1)", '', $this->db, __FILE__, __LINE__);
			$cl['bid_watchlist']=$this->watchlist_add($cl['id'], $this->uid);
			#<Update data />
			$cl['msg']['msg_bid_ok']=$this->tro->s('msg_bid_ok');
			$cl['price_bid_time']=$cl['bid_you_time']=$axs['time'];
			$cl['price_bid_uid']=$this->uid;
			$cl['price_bid']=$cl['bid_you']=$cl['bid.next'];
			$this->bid_step($cl);
			$cl['bid.next']+=$cl['bid.step'];
			$cl['bid_count']++;
			#<Notify previous bidder />
			$r=axs_db_query("SELECT CONCAT(u.`fstname`,' ',u.`lstname`) AS `name`, CONCAT(u.`user`,',',u.`email`) AS `mail`, b.`bid`\n".
			"	FROM `".$this->table."_bid` AS b LEFT JOIN `axs_users` AS u ON u.`id`=b.`uid`\n".
			"	WHERE b.`pid`='".$cl['id']."' AND u.`id`!='".$this->uid."' ORDER BY b.`bid` DESC LIMIT 1", 'row', $this->db, __FILE__, __LINE__);
			$r['mail']=explode(',', $r['mail']);
			foreach ($r['mail'] as $k=>$v) {	if (strpos($v, '@')) $r['mail'][$k]=trim($v);	else unset($r['mail'][$k]);	}
			if (!empty($r['mail'])) axs_mail(
				implode(', ', $r['mail']),
				$_SERVER['SERVER_NAME'].$axs['http_root'].$axs['site_dir'],
				$this->tro->t('msg_bid_mail')."\n".
					$item_name.': '.number_format($cl['price_bid'], 0, '.', ' ')."\n".
					'('.date('d.m.Y H:i:s', $axs['time']).')'."\n".
					$axs['http'].'://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'],
				'',
				$_SERVER['SERVER_NAME'],
				$this->tro->t('mail_addr')
				);
			} #</Accept new bid>
		} #</bid_make()>
	function bid_step(&$cl) {
		reset($this->bid_step);
		$cl['bid.step']=current($this->bid_step);
		$tmp=($cl['price_bid']) ? $cl['price_bid']:$cl['price_start'];
		foreach ($this->bid_step as $k=>$v) if ($tmp>=$k) $cl['bid.step']=$v;
		} #</bid_step()>
	function data(&$cl, &$vr) {
		global $axs;
		if (!isset($vr['_class'])) $vr['_class']=array();
		if ($cl['price_bid']>0) $vr['_class']['price_bid']='price_bid';
		if (($cl['time_end']) && ($cl['time_end']<$axs['time']+$this->ok_act)) $vr['_class']['ending']='ending';
		if ($cl['bid_you']>0) $vr['_class']['bid_you']='bid_you';
		if (($cl['bid_you']>0) && ($cl['bid_you']>=$cl['price_bid'])) $vr['_class']['leading']='leading';
		if (($cl['bid_you']>0) && ($cl['bid_you']<$cl['price_bid'])) $vr['_class']['losing']='losing';
		$vr['msg']=$this->msg_display($cl['msg']);
		foreach(array('bid.next'=>'bid.next', 'bid.step'=>'bid.step', 'bid_count'=>'bid_count', ) as $k=>$v) $vr[$k]=$cl[$v];
		foreach (array('price_start'=>'0', 'bid.step'=>'', 'price_bid'=>'', 'bid_you'=>'', ) as $k=>$v) $vr[$k.'.fmt']=($cl[$k]>0) ? number_format($cl[$k], 0, '.', ' ').' '.$this->currency:$v;
		$vr['price_estimate.fmt']=($vr['price_estimate']) ? $vr['price_estimate'].' '.$this->currency:'';
		foreach (array('price_bid_time'=>'price_bid_time', 'bid_you_time'=>'bid_you_time', 'time_end'=>'time_end', ) as $k=>$v) {
			if ($cl[$k]) {
				$vr[$k]=$cl[$k];
				$vr[$k.'.fmt']=date('d.m.y H:i', $cl[$k]);
				}
			else $vr[$k]=$vr[$k.'.fmt']='';
			}
		if ($cl['bid_you']) $vr['terms_agree_chk']=' checked="checked"';
		else {
			$vr['terms_agree_chk']=(isset($_POST['terms_agree'])) ? ' checked="checked"':'';
			}
		$vr['bid_watchlist.disabled']=((!$this->uid) or (!empty($cl['bid_watchlist']))) ? ' disabled="disabled"':'';
		$vr['watchlist.btn']=$this->tro->s(($cl['bid_watchlist']) ? 'watchlist_rem.lbl':'watchlist_add.lbl');
		$vr['disabled']=($cl['bid.open']) ? '':' disabled="disabled"';
		if (isset($axs['get'][__CLASS__])) { #<Update data via AJAX>
			$a=self::$data_fields;
			foreach ($a as $k=>$v) {
				if (!is_array($v)) $a[$k]=$vr[$v];
				else foreach ($v as $kk=>$vv) {
					if (!is_array($vv)) $a[$k][$kk]=$vr[$vv];
					else foreach($vv as $kkk=>$vvv) $a[$k][$kk][$kkk]=$vr[$vvv];
					}
				}
			$this->ajax_update[$cl['id']]=$a;
			} #</Update data via AJAX>
		$vr['currency']=$this->currency;
		} #</data()>
	function sql_bid($table) {
		return
		"	(SELECT MAX(`bid`) FROM `".$table."_bid` WHERE `pid`=t.`id` AND `uid`='".$this->uid."') AS `bid_you`,\n".
		"	(SELECT MAX(`time`) FROM `".$table."_bid` WHERE `pid`=t.`id` AND `uid`='".$this->uid."') AS `bid_you_time`,\n".
		"	(SELECT COUNT(*) FROM `".$table."_bid` WHERE `pid`=t.`id`) AS `bid_count`,\n".
		"	(SELECT `id` FROM `".$table."_bid_watchlist` WHERE `pid`=t.`id` AND `uid`='".$this->uid."' LIMIT 1) AS `bid_watchlist`\n";
		} #</sql_bid()>
	function cron($url) {
		global $axs;
		#<Send warning about bidding end />
		$r=axs_db_query(
		"SELECT DISTINCT t.*, u.user AS bid_user, u.email AS bid_user_email, CONCAT(u.fstname, ' ', u.lstname) AS bid_user_name, u.l AS bid_user_l\n".
		"	FROM `".$this->table."` AS t\n".
		"	LEFT JOIN `".$this->table."_bid` AS b ON b.`pid`=t.`id`\n".
		"	LEFT JOIN `axs_users` AS u ON u.id=b.uid\n".
		"	WHERE t.`time_end` BETWEEN ".$axs['time']." AND ".($axs['time']+$this->bid_end_warning_mail)."\n".
		"	ORDER BY u.id", 1, $this->db, __FILE__, __LINE__);
		$name=preg_replace('/^www\./', '', $_SERVER['SERVER_NAME']);
		foreach ($r as $cl) {
			$cl['email']=(strpos($cl['bid_user'], '@')) ? $cl['bid_user']:$cl['bid_user_email'];
			if (strpos($cl['email'], '@')) axs_mail(
				$cl['email'],
				'Auction ends at '.date('Y-m-d H:i', $cl['time_end']).' - '.$cl['auttext'].' "'.$cl['title'].'"',
				'Auction ends at '.date('Y-m-d H:i', $cl['time_end']).' - '.$cl['auttext'].' "'.$cl['title'].'"'."\n".
				axs_tpl_parse($url, $cl),
				'', $name, 'no-reply@'.$name);
			}
		return dbg($cl['email'],
				'Auction ends at '.date('Y-m-d H:i', $cl['time_end']).' - '.$cl['auttext'].' "'.$cl['title'].'"',
				'Auction ends at '.date('Y-m-d H:i', $cl['time_end']).' - '.$cl['auttext'].' "'.$cl['title'].'"'."\n".
				axs_tpl_parse($url, $cl),
				'', $name, 'no-reply@'.$name);
		} #</cron()>
	function watchlist_add($pid, $uid) {
		$pid=intval($pid);
		$uid=intval($uid);
		$id=axs_db_query("SELECT `id` FROM `".$this->table."_bid_watchlist` WHERE `pid`='".$pid."' AND `uid`='".$uid."' LIMIT 1", 'cell', $this->db, __FILE__, __LINE__);
		if (!$id) $id=axs_db_query("INSERT INTO `".$this->table."_bid_watchlist` SET `pid`='".$pid."', `uid`='".$uid."'", 'insert_id', $this->db, __FILE__, __LINE__);
		#<Remove inactive or orphan records />
		axs_db_query("DELETE l, t FROM `".$this->table."_bid_watchlist` AS l LEFT JOIN `".$this->table."` AS t ON t.`id`=l.`pid` WHERE t.`enable`='0' OR t.`id` IS NULL", '', $this->db, __FILE__, __LINE__);
		return $id;
		} #</watchlist_add()>
	function watchlist_del($pid, $uid) {
		if (!is_array($pid)) $pid=array($pid=>$pid);
		foreach ($pid as $k=>$v) $pid[$k]="`pid`='".intval($k)."'";
		if ($pid) axs_db_query("DELETE FROM `".$this->table."_bid_watchlist` WHERE (".implode(" OR ", $pid).") AND `uid`='".intval($uid)."'", '', $this->db, __FILE__, __LINE__);
		} #</watchlist_del()>
	} #</class::axs_auction>
/*
CREATE TABLE `table` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `enable` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `price_estimate_1` int(10) DEFAULT NULL,
  `price_estimate_2` int(10) unsigned DEFAULT NULL,
  `price_reserve` int(10) NOT NULL,
  `price_start` int(10) DEFAULT NULL,
  `price_bid` int(10) NOT NULL,
  `price_bid_uid` int(10) unsigned NOT NULL,
  `price_bid_time` int(10) unsigned NOT NULL,
  `time_start` int(10) unsigned NOT NULL,
  `time_lasts` tinyint(3) unsigned DEFAULT NULL,
  `time_end` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `title` (`title`),
  KEY `enable` (`enable`),
  KEY `price_bid_time` (`price_bid_time`),
  KEY `price_bid_uid` (`price_bid_uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_estonian_ci;

CREATE TABLE `table_bid` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '=table.id',
  `time` int(10) unsigned NOT NULL DEFAULT '0',
  `uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '=users.id',
  `bid` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`,`time`,`bid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_estonian_ci;

CREATE TABLE `table_bid_watchlist` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL COMMENT '=table.id',
  `uid` int(10) unsigned NOT NULL COMMENT '=users.id',
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`,`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_estonian_ci;
*/
#2010-09-27 ?>