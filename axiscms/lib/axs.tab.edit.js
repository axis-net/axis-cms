//2020-03-18
axs_tab_edit={
	elements: ['date','h','i','s'],
	date_time: function () {
		var today=new Date();
		var time={
			'date':this.zero_fill(today.getFullYear())+'-'+this.zero_fill(today.getMonth()+1)+'-'+this.zero_fill(today.getDate()),
			'h':this.zero_fill(today.getHours()),
			'i':this.zero_fill(today.getMinutes()),
			's':this.zero_fill(today.getSeconds())
			}
		return time;
		}, // </date_time()>
	file_set:function(){
		opener.document.getElementById(axs.url_get('browse')).value=this.getAttribute("href");
		window.close();
		},//</file_set()>
	zero_fill: function(i) {
		if (i<10) i="0"+i;
		return i;
		}, // </zerofill()>
	edit_row: function(table) { // <Row toolbar>
		var forms=table.getElementsByTagName('form');
		for (var i=0; i<forms.length; i++) {
			var form=forms[i];
			var del_button=false;
			var del=false
			var input=form.getElementsByTagName('input');
			if (input) {
				for (var ii=0; ii<input.length; ii++) {
					if (input[ii].name=='del_confirm') del_button=input[ii];
					if (input[ii].name=='del') del=input[ii];
					}
				}
			if (del_button) {
				del_button.axs_del_node=del;
				del_button.axs_msg=(del.parentNode.innerText) ? del.parentNode.innerText:del.parentNode.textContent;
				del_button.title=del_button.axs_msg;
				del_button.onclick=function() {
					if (confirm(this.axs_msg)) {
						this.axs_del_node.checked='checked';
						return true;
						}
					return false;
					}
				}
			}
		//axs.throbber_set();
		var td=table.getElementsByTagName('td');
		for (var i=0; i<td.length; i++){
			if (/\beditor\b/.exec(td[i].className)) continue;
			td[i].onclick=function(e){
				var url=document.getElementById(this.getAttribute('headers').replace(/^.* /g,'')).getElementsByTagName('a')[0].href;
				axs.throbber_set(this);
				document.location.href=url;
				}
			var el=td[i].getElementsByTagName('a');
			for (var ii=0; ii<el.length; ii++) el[ii].onclick=function(e){	e.stopPropagation();	};
			var el=td[i].getElementsByTagName('form');
			for (var ii=0; ii<el.length; ii++) el[ii].onsubmit=function(e){	e.stopPropagation();	};
			}
		}, // </edit_row()>
	editform: function() {
		// <Editform header>
		// <Lock button>
		var tmp=document.getElementById('lock_lbl');
		if (tmp) {
			tmp.onclick=function() {
				var elements=['date','h','i','s'];
				for (var i=0; i<elements.length; i++) {
					document.getElementById('publ_'+elements[i]).value='';
					document.getElementById('hidden_'+elements[i]).value='';
					}
				//this.onclick.preventDefault();
				return false;
				}
			} // </Lock button>
		// <Publish button>
		var tmp=document.getElementById('publ_lbl');
		if (tmp) {
			tmp.onclick=function() {
				var elements=['date','h','i','s'];
				var time=axs_tab_edit.date_time();
				for (var i=0; i<elements.length; i++) {
					document.getElementById('publ_'+elements[i]).value=time[elements[i]];
					document.getElementById('hidden_'+elements[i]).value='';
					}
				return false;
				}
			} // </Publish button>
		// <Hide button>
		var tmp=document.getElementById('hidden_lbl');
		if (tmp) {
			tmp.onclick=function() {
				var elements=['date','h','i','s'];
				var time=axs_tab_edit.date_time();
				for (var i=0; i<elements.length; i++) {
					document.getElementById('hidden_'+elements[i]).value=time[elements[i]];
					}
				return false;
				}
			} // </Hide button>
		// </Editform header>
		
		// <Editform file&media>
		var tmp=document.getElementById('fup_1');
		if (tmp) {
			if (tmp.disabled) {
				var msg=document.getElementById('fupl_disabled_msg');
				if (msg) tmp.onclick=function() {
					//document.getElementById('f').value='';
					msg.style.display='none';
					alert(msg.innerHTML);
					}
				}
			}
		var tmp=document.getElementById('fup_1_lbl');
		if (tmp) {
			tmp.onclick=function() {
				document.getElementById('fup_1').style.display='inline';
				document.getElementById('f').style.display='none';
				return false;
				}
			}
		var tmp=document.getElementById('f_lbl');
		if (tmp) {
			document.getElementById('f').style.display='none';
			tmp.addEventListener('click',function(e){
				e.preventDefault();
				var el=document.getElementById('fup');
				el.style.display='none';
				el.value='';
				document.getElementById('f').style.display='inline';
				//var newWindow=window.open(this.getAttribute('href'), '', 'scrollbars=1,location=0,resizable=1,toolbar=0,status=1,width=750,height=500,top=50,left=40');
	    		//newWindow.focus();
				return false;
				});
			}
		// </Editform file&media>
		} // </editform()>
	} // </class:axs_tab_edit>

axs_tab_edit.tmp=document.getElementsByTagName('table');
if (axs_tab_edit.tmp) {
	for (var i=0; i<axs_tab_edit.tmp.length; i++) if (axs_tab_edit.tmp[i].className=='edit-tab') {
		axs_tab_edit.edit_row(axs_tab_edit.tmp[i]);
		}
	}
axs_tab_edit.tmp=document.getElementById('edit_tab_form');
if (axs_tab_edit.tmp) axs_tab_edit.editform();
//2009-12-04