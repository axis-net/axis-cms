//2014-01-19
// JS powered throbber funtion. Requires AXIS CMS JS API "axs.js" included first. 
axs.throbber={
	speed:400,
	run:5,
	finish:false,
	init:function(id){
		var a=axs.fn_args(arguments,{id:'',conf:{}});
		var o=this;
		o.id=id;
		for(key in a.conf) o[key]=a.conf[key];
		o.el=document.getElementById(o.id);
		if (!o.el) return;
		o.size=o.el.childNodes.length;
		o.opacity=[];
		for(var i=0; i<=o.size; i++) o.opacity[i]=$(o.el.childNodes[i]).css('opacity');
		$(o.el.childNodes).css('opacity',0.0);
		$(o.el).addClass('started');
		window.axs.throbber[o.id]=o;
		window.axs.throbber[o.id].fade_animate();
		},//</init()>
	finsh:function(){
		$(this.el).removeClass('started');
		for(var i=0; i<=this.size; i++){
			setTimeout('$(window.axs.throbber["'+this.id+'"].el.childNodes['+i+']).animate({opacity:'+this.opacity[i]+'},'+this.speed+',function(){});',i*(this.speed/2));
			}
		},//</finish()>
	fade:function(nr){
		var el=this.el.childNodes[nr];
		var smooth_in=(nr==this.size) ? 0.5:1;
		var smooth_out=(nr==this.size) ? 2:3;
		$(el).animate({opacity:1.0},this.speed*smooth_in,function(){});
		$(el).animate({opacity:0.0},this.speed*smooth_out,function(){});
		},//</fade()>
	fade_animate:function(){
		if (this.run>1 || window.axs.throbber.finish) this.run--;
		if (this.run<=0) return setTimeout('window.axs.throbber["'+this.id+'"].finsh();',this.speed);
		for(var i=0; i<=this.size; i++) setTimeout('window.axs.throbber["'+this.id+'"].fade('+i+');',i*this.speed);
		if (this.run>0) setTimeout('window.axs.throbber["'+this.id+'"].fade_animate();',(this.size*this.speed)-this.speed);
		}//</fade_animate()>
	}//</class::axs.throbber>
//axs.throbber=new axs.throbber();
$(window).load(function(){	window.axs.throbber.finish=true;	});
//12.07.2011-07-12