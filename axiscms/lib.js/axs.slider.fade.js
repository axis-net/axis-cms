//2020-02-15
axs.slider.fade=Object.create(axs.slider.base);
axs.slider.fade.type='fade';
axs.slider.fade.css={
	'@keyframes axs-slider-type-fade':'{\n'+
	'	0% {	opacity:0;	}\n'+
	'	100% {	opacity:1; }\n'+
	'	}',
	'.axs_slider.js.fade .list':'{	position:relative;	}',
	'.axs_slider.js.fade .list>*':'{	position:absolute;	visibility:hidden;	}',
	'.axs_slider.js.fade .list>.prev':'{	visibility:visible;	z-index:1;	}',
	'.axs_slider.js.fade .list>.current':'{	visibility:visible;	z-index:2;	}',
	'.axs_slider.js.fade.anim .list>.current':'{	animation:axs-slider-type-fade linear;	}'
	};
//2019-04-19