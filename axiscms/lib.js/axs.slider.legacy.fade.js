//2018-01-22
// Crossfae slider script. Requires AXIS CMS JS API "axs.js" included first. 
// These functions use some code form: 
/*****
Image Cross Fade Redux
Version 1.0
Last revision: 02.15.2006
steve@slayeroffice.com
Please leave this notice intact. 
Rewrite of old code found here: http://slayeroffice.com/code/imageCrossFade/index.html
*****/

axs.xfade={
	lists:{},
	init:function(id){
		var a=axs.fn_args(arguments,{id:id,cfg:{}});
		var defaults={interval:3.0,fps:12.5,transition:2.0,random:false,pager:true,auto_height:true};
		for (var k in defaults) a[k]=(a.cfg[k]) ? a.cfg[k]:defaults[k];
		//var container=document.getElementById(id);
		var container=(typeof(id)==='string') ? document.querySelector(id):id;
		if (!container) return;
		axs.class_add(container,'js');
		var list=container.querySelector('.list');
		var items=container.querySelectorAll('.list>*');
		if (a.cfg.random) items=this.shuffle(items);
		this.lists[id]={
			container:container,
			list:list,
			items:items,
			pager:a.pager,
			interval:a.interval*1000,
			transition:a.transition*1000,
			fps:a.fps,
			fdelay:Math.round(1000/a.fps,0),
			auto_height:a.auto_height,
			paused:false
			}
		this.lists[id].transition_step_alpha=1.0/(this.lists[id].transition/this.lists[id].fdelay);
		if (items.length>1){
			list.style.position='relative';
			for (var i=0;i<items.length;i++){
				items[i].style.visibility='hidden';
				items[i].style.position='absolute';
				items[i].xOpacity=0;
				}
			axs.class_add(items[0],'current');
			items[0].style.visibility='visible';
			items[0].xOpacity=1.0-this.lists[id].transition_step_alpha;
			if (this.lists[id].auto_height){
				this.heightSet(id);
				window.addEventListener('resize',function(){
					for (var k in axs.xfade.lists) if (axs.xfade.lists[k].auto_height) axs.xfade.heightSet(k);
					});
				}
			container.addEventListener('mouseover',axs.xfade.statePause);
			container.addEventListener('mouseout',axs.xfade.stateResume);
			setTimeout('axs.xfade.xfade("'+id+'",0);',this.lists[id].interval);
			}
		if (this.lists[id].pager) this.pagerCreate(id);
		var imgs=list.getElementsByTagName('img');
		for (var i=0;i<imgs.length;i++){
			var img=new Image();
			img.container_id=id;
			img.container=container;
			if (this.lists[id].auto_height) img.onload=function(){	axs.xfade.heightSet(this.container_id);	}
			img.src=imgs[i].src;
			}
		},//</init()>
	heightSet:function(id){
		var max_height=0;
		for (var i=0;i<this.lists[id].items.length;i++) if (this.lists[id].items[i].offsetHeight>max_height) max_height=this.lists[id].items[i].offsetHeight;
		if (max_height) this.lists[id].list.style.height=max_height+'px';
		},//</heightSet()>
	pagerCreate:function(id){
		var el=jQuery(this.lists[id].container).find('.slider_pager');
		(el.length) ? $(el).replaceWith($('<ul class="slider_pager"></ul>').html($(el).html())):jQuery(this.lists[id].container).append('<ul class="slider_pager"></ul>');
		var el=jQuery(this.lists[id].container).find('.slider_pager');
		for(var i=0; i<this.lists[id].items.length; i++){
			var p=i+1;
			var tmp=(this.lists[id].pager==='content')?this.lists[id].items[i].innerHTML:p;
			jQuery(el).append('<li class="item'+p+'" title="'+p+'"><a class="scroll" data-container="'+id+'" data-slide="'+p+'" href="#'+this.lists[id].items[i].id+'">'+tmp+'</a></li>');
			}
		/*jQuery(this.lists[id].container).find('.slider_pager a.scroll').click(function(){
			axs.slider.data[id].playing=false;
			axs.slider.animate_scroll(this.getAttribute('data-container'),parseInt(this.getAttribute('data-slide')));
			return false;
			});*/
		jQuery(this.lists[id].container).find('.slider_pager>*:first-child').addClass('current');
		},//</pagerCreate()>
	pagerSetPage:function(id,current){
		var el=jQuery(this.lists[id].container).find('.slider_pager li');
		for(var i=0; i<el.length; i++){
			axs.class_rem(el[i],'current');
			if ((i+1)==current) axs.class_add(el[i],'current');
			}
		},//</pagerSetPage()>
	shuffle:function(array){
		for (var i=array.length-1; i>0; i--){
			var j=Math.floor(Math.random()*(i + 1));
			var temp=array[i];
			array[i]=array[j];
			array[j]=temp;
			}
		return array;
		},//</shuffle()>
	xfade:function(id,current){
		var items=this.lists[id].items;
		//this.pagerSetPage(id,current);
		
		cOpacity=items[current].xOpacity;
		if ((!this.lists[id].paused) && (axs.window_visible())){
			nIndex=items[current+1]?current+1:0;
			
			nOpacity=items[nIndex].xOpacity;
			cOpacity-=this.lists[id].transition_step_alpha;
			nOpacity+=this.lists[id].transition_step_alpha;
			
			items[nIndex].style.visibility="visible";
			items[current].xOpacity=cOpacity;
			items[nIndex].xOpacity=nOpacity;
			
			this.setOpacity(items[current],id); 
			axs.class_rem(items[current],'current');
			this.setOpacity(items[nIndex],id);
			axs.class_add(items[nIndex],'current');
			}
		if (cOpacity<=0){
			items[current].style.visibility="hidden";
			this.pagerSetPage(id,nIndex+1);
			setTimeout('axs.xfade.xfade("'+id+'",'+nIndex+');',this.lists[id].interval);
			}
		else setTimeout('axs.xfade.xfade("'+id+'",'+current+');',this.lists[id].fdelay);
		},//</xfade()>
	setOpacity:function(obj,id){
		if (this.lists[id].auto_height) axs.xfade.heightSet(id);
		if (obj.xOpacity>.99){
			obj.xOpacity=.99;
			return;
			}
		obj.style.opacity=obj.xOpacity;
		obj.style.MozOpacity=obj.xOpacity;
		obj.style.filter="alpha(opacity="+(obj.xOpacity*100)+")";
		},//</setOpacity()>
	statePause:function(){	axs.xfade.lists[this.id].paused=true;	},//</set_pause()>
	stateResume:function(){	axs.xfade.lists[this.id].paused=false;	}//</set_pause()>
	}//</class::slideshow>
//2009