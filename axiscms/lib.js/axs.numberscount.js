teenused={
	interval:{min:25, max:2000, dur:5000},
	nodes:[],
	opts:{  root:null, rootMargin:'0px',  threshold:[0, 1.0]	},
	init:function(e){
		teenused.observer=new IntersectionObserver(teenused.onview,teenused.opts);
		var el=document.querySelectorAll('#content .content.plugin-avaleht-teenused-numbrid .item');
		for(var i=0, l=el.length; i<l; i++){
			teenused.nodes[i]={node:el[i].querySelector('strong')};
			teenused.nodes[i].init=parseInt(teenused.nodes[i].node.innerHTML);
			teenused.nodes[i].speed=teenused.interval.dur/teenused.nodes[i].init;
			if(teenused.nodes[i].speed<teenused.interval.min) teenused.nodes[i].speed=teenused.interval.min;
			if(teenused.nodes[i].speed>teenused.interval.max) teenused.nodes[i].speed=teenused.interval.max;
			console.log(i,teenused.nodes[i].speed);
			el[i].setAttribute('data-n',i);
			teenused.reset(i);
			teenused.observer.observe(el[i]);
			}
		},
	onview:function(entries){
		entries.forEach(entry=>{
			var n=parseInt(entry.target.getAttribute('data-n'));
			if(!entry.intersectionRatio>0) return teenused.reset(n);
			teenused.onviewSet(n);
			//this.unobserve(entry.target);
			});
		},
	onviewSet:function(n){
		if (teenused.nodes[n].current>=teenused.nodes[n].init) return;
		teenused.nodes[n].current++;
		teenused.nodes[n].node.innerHTML=teenused.nodes[n].current;
		window.setTimeout(teenused.onviewSet,teenused.nodes[n].speed,n);
		},
	reset:function(n){
		teenused.nodes[n].node.innerHTML='&nbsp;';
		teenused.nodes[n].current=0;
		}
	};
document.addEventListener('DOMContentLoaded',teenused.init);