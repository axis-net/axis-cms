//2020-02-15
/*Slider base function. Requires AXIS CMS JS API "axs.js" included first. 
Sample HTML: 
<section id="slider1">
	<a class="play" href="#slider1"><img src="slider.play.png" alt="play" lang="en" /></a>
	<a href="#slider1" class="scroll prev"><img src="slider.prev.png" alt="&lt;" /></a>
	<a href="#slider1" class="scroll next"><img src="slider.next.png" alt="&gt;" /></a>
	<h2>{$title}</h2>
	<div class="list setHeight">
		<article>
			<a href="#" class="scroll prev"><img src="slider.prev.png" alt="&lt;" /></a>
			<a href="#" class="scroll next"><img src="slider.next.png" alt="&gt;" /></a> 
			<div class="effect">Lorem ipsum...</div>
		</article>
		<article>
			<a href="#" class="scroll prev"><img src="slider.prev.png" alt="&lt;" /></a>
			<a href="#" class="scroll next"><img src="slider.next.png" alt="&gt;" /></a> 
			<div class="effect">dolor sit amet...</div>
		</article>
	</div>
	<span class="slider-pager"></span>
	<script>axs.slider.init("#slider1",{type:'fade',effect:'pan-zoom',interval:10,play:'auto'});</script>
</section>
Elements can be added or removed as necessary. The items container can also be <ul><li> or any other HTML combination. The pager element will be automatically converted to an <ul> tag and filled with <li> tags. 
Adding the class "setHeight" on the list element sets it's height egual to the tallest slide using an inline style. This is not always necessary. 
For configuration options, look for the following code comments. 
*/
axs.slider={
	sliders:{},
	resizeEvent:null,
	css:{
		'.axs_slider .play':'{	position:absolute;	z-index:1000;	left:10%;	top:5%;	}',
		'.axs_slider .play span.default':'{	display:block;	padding:0.75rem 1.2rem;	width:1.5rem;	height:1.5rem;	background:rgba(0,0,0,0.5);	color:#fff;	text-decoration:none;	font-size:0;	opacity:0.5;	transition:opacity 1s linear;	}',
		'.axs_slider .play span.default::before':'{	display:block;	content:"";	border-left:solid 0.5rem #fff;	border-right:solid 0.5rem #fff;	width:0.5rem;	height:100%;	}',
		'.axs_slider.paused .play span.default::before':'{	margin-left:0.3rem;	border:solid transparent;	border-width:0.75rem 1rem;	border-left-color:#fff;	width:0;	height:0;	}',
		'.axs_slider:hover .play span.default, .axs_slider .play:focus span.default, .axs_slider .play.paused span.default':'{	opacity:1;	}',
		'.axs_slider .slider-pager .current':'{	font-weight:bold;	}'
		},
	init:function(id){
		var a=axs.fn_args(arguments,{container:'',cfg:{}});
		window.addEventListener('load',axs.slider.sizeSetAll);
		if (!this.resizeEvent) this.resizeEvent=window.addEventListener('resize',axs.slider.sizeSetAll);
		this.cssAdd(this.css,'');
		if(!a.cfg.type) a.cfg.type='base';
		var container=(typeof(id)==='string') ? document.querySelector(id):id;
		if (!container) return axs.log('axs.slider.js','Could nof find node for "'+id+'"');
		id=this.base.ID(container,'slider');
		this.sliders[id]=Object.create(this[a.cfg.type]);
		this.sliders[id].init(container,a.cfg);
		return id;
		},//</init()>
	base:{
		cfg:{
			type:'base',//["string"]:Transition effect extension to load. 
				//Currently "fade", "carousel", "scroll" and "marquee" are bundled by default, but other extensions can be added. 
			effect:'',//["pan"|"pan-zoom"|"zoom"]:Use effect on slide content. 
			next_lbl:'&gt;',//["HTML code"]:Scroll next button label
			prev_lbl:'&lt;',//["HTML code"]:Scroll back button label
			play_lbl:'<span class="default" lang="en" title="play">play</span>',//["HTML code"]:Play button label
			pause_lbl:'<span class="default" lang="en" title="pause">pause</span>',//["HTML code"]:Play button label for pause state
			play:'auto',//[true]:Add play button if not present; ["auto"]:Start to play automatically
			playing:false,
			interval:5,//[seconds]:Play speed
			transition:2,//[seconds]:Transition speed
			groupCount:0,//[number]:Group several items as one slide
			groupTag:'<div></div>',//["HTML code"]:HTML container for groups
			pagerCustomize:false,//["selector"|DOM nodes]: Customize pager items content. 
			},
		type:'base',
		css:{
			'.axs_slider.js.base .list>*':'{	display:none;	}',
			'.axs_slider.js.base .list>.current':'{	display:block;	}'
			},
		data:{},
		init:function(container){
			var a=axs.fn_args(arguments,{container:'',cfg:{}});
			this.id=container.id;
			this.data={node:container,prev:false,current:1,next:false};
			for (k in this.cfg) this.data[k]=(typeof(a.cfg[k])!=='undefined')?a.cfg[k]:this.cfg[k];
			for (k in {interval:'',transition:''}) this.data[k]=this.data[k]*1000;
			//<Remove whitespace>
			var tmp=this.data.node.querySelector('.list');
			for(var i=0, l=tmp.childNodes.length; l>i; i++){	if(!tmp.childNodes[i].nodeType) tmp.removeChild(tmp.childNodes[i]);	}
			//</Remove whitespace>
			this.data.list=container.querySelector('.list');
			if(this.data.groupCount) this.group(this.data.groupCount,this.data.groupTag);
			this.data.items=container.querySelectorAll('.list>*');
			if (!this.data.items.length) return;
			
			axs.class_add(container,['axs_slider',this.data.type,'items-count'+this.data.items.length,'js']);
			axs.class_add(this.data.items[0],'current');
			if (axs.class_has(this.data.list,'setHeight')) this.data.setHeight=true;
			this.sizeSet();
			var prev_next=container.querySelectorAll('a.scroll.prev, a.scroll.next');
			if ((this.data.items.length>1)&&(!prev_next.length)){
				this.data.list.parentNode.insertBefore(axs.elementCreate('a',{'className':'scroll prev','data-container':this.id,'href':'#','title':'<'},this.data.prev_lbl),this.data.list);
				this.data.list.parentNode.insertBefore(axs.elementCreate('a',{'className':'scroll next','data-container':this.id,'href':'#','title':'>'},this.data.next_lbl),this.data.list);
				}
			//<Set ID to items and link to them />
			for (var i=0, l=this.data.items.length; i<l; i++) this.ID(this.data.items[i],this.id+'-item'+(i+1));
			for (var i=0, l=this.data.items.length; i<l; i++){
				this.data.items[i].setAttribute('data-container',this.id);
				this.data.items[i].style.animationDuration=this.cfg.transition+'s';
				if(this.data.effect){
					axs.class_add(this.data.items[i],['effect',this.data.effect]);
					var tmp=this.data.items[i].querySelector('.effect');
					if (tmp) tmp.style.animationDuration=(this.data.transition*2+this.data.interval)+'ms';
					}
				this.data.items[i].addEventListener('animationend',function(e){
					axs.class_rem(axs.slider.sliders[e.target.getAttribute('data-container')].data.node,'transitioning');
					});
				var prev=(i<=0) ? this.data.items.length-1:i-1;
				if (el=this.data.items[i].querySelector('a.scroll.prev')) el.setAttribute('href','#'+this.data.items[prev].getAttribute('id'));
				var next=(i>=this.data.items.length-1) ? 0:i+1;
				if (el=this.data.items[i].querySelector('a.scroll.next')) el.setAttribute('href','#'+this.data.items[next].getAttribute('id'));
				}
			var el=container.querySelectorAll('a.scroll.prev');
			if (el) for(var i=0, l=el.length; i<l; i++){
				el[i].setAttribute('data-container',this.id);
				el[i].addEventListener('click',function(e){
					e.preventDefault();
					var id=this.getAttribute('data-container');
					if (axs.slider.sliders[id].data.playing) axs.slider.sliders[id].animateBtn(false);
					return axs.slider.sliders[id].animatePrev();
					});
				}
			var el=container.querySelectorAll('a.scroll.next');
			if (el) for(var i=0, l=el.length; i<l; i++){
				el[i].setAttribute('data-container',this.id);
				el[i].addEventListener('click',function(e){
					e.preventDefault();
					var id=this.getAttribute('data-container');
					if (axs.slider.sliders[id].data.playing) axs.slider.sliders[id].animateBtn(false);
					return axs.slider.sliders[id].animateNext();
					});
				}
			//<Pager>
			this.data.pagerNode=container.querySelector('.slider-pager');
			if (!this.data.pagerNode) {
				this.data.pagerNode=axs.elementCreate('ul',{'className':'slider-pager'});
				this.data.list.parentNode.insertBefore(this.data.pagerNode,this.data.list.nextSibling);
				}
			if (!this.data.pagerNode.children) {
				if (this.data.pagerNode.tagName.toLower()!=='ul') this.data.pagerNode.parentNode.replaceChild(axs.elementCreate('ul',{'className':'slider-pager'}),this.data.pagerNode);
				for(var i=0, l=this.data.items.length; i<l; i++){
					var p=i+1;
					var li=this.data.pagerNode.appendChild(axs.elementCreate('li',{},'<a class="scroll" href="#'+this.data.items[i].id+'" title="'+p+'">'+p+'</a>'));
					}
				}
			for(var i=0, l=this.data.pagerNode.children.length; i<l; i++){
				var p=i+1;
				var li=this.data.pagerNode.children[i];
				var a=li.querySelector('a.scroll');
				axs.class_add(li,'item'+p);
				a.setAttribute('data-container',this.id);
				a.setAttribute('data-slide',p);
				a.addEventListener('click',function(e){
					e.preventDefault();
					var id=this.getAttribute('data-container');
					if (axs.slider.sliders[id].data.playing) axs.slider.sliders[id].animateBtn(true);
					axs.slider.sliders[this.getAttribute('data-container')].animate(parseInt(this.getAttribute('data-slide')));
					});		
				}
			axs.class_add(this.data.pagerNode.children[0],'current');
			if(this.data.pagerCustomize) this.pagerCustomize(this.data.pagerCustomize);
			//</Pager>
			//<Play>
			this.data.playNode=container.querySelector('a.play');
			if(this.data.play){
				var lbl=this.data.play_lbl;
				if (!this.data.playNode) this.data.playNode=this.data.list.parentNode.insertBefore(axs.elementCreate('a',{'className':'play','href':'#'+this.id},lbl),this.data.list);
				if (this.data.play==='auto') this.animateBtn(true);
				}
			if(this.data.playNode){
				this.data.playNode.setAttribute('data-container',this.id);
				if (this.data.play!=='auto') axs.class_add(this.data.playNode,'paused');
				this.data.playNode.addEventListener('click',function(e){
					e.preventDefault();
					axs.slider.sliders[this.getAttribute('data-container')].animateBtn(false);
					});
				}
			//</Play>
			this.initLocal();
			axs.slider.cssAdd(this.css,' '+this.cfg.type);
			},//</init()>
		initLocal:function(){},//Customize init(). 
		ID:function(node){//Get ID of the container or generate it if there is none. 
			if (node.id) return node.id;
			var a=axs.fn_args(arguments,{node:node,base:'id'});
			if(!document.getElementById(a.base))return node.id=a.base;
			var i=1;
			while((!document.getElementById(base+i))&&(i<=1000)) i++;
			return node.id=a.base+i;
			},//</ID()>
		sizeSet:function(){	axs.slider.sizeSet(this);	},//</sizeSet()>
		animate:function(to){
			if (to<1) to=1;
			if (to>this.data.items.length) to=this.data.items.length;
			if (!this.data.prev) axs.class_add(this.data.node,'anim');
			this.data.prev=this.data.current;
			this.data.current=to;
			this.data.next=(this.data.current===this.data.items.length)?1:this.data.current+1;
			axs.class_rem(this.data.items,['prev','next']);
			axs.class_add(this.data.items[this.data.prev-1],'prev');
			axs.class_add(this.data.items[this.data.next-1],'next');
			axs.class_add(this.data.items[this.data.current-1],['current','transitioning']);
			for(var i=0, l=this.data.items.length; i<l; i++) if(i+1!=this.data.current) axs.class_rem(this.data.items[i],'current');
			axs.class_rem(this.data.pagerNode.childNodes,'current');
			axs.class_add(this.data.pagerNode.childNodes[to-1],'current');
			this.animateEffect();
			},//</animate()>
		animateEffect:function(){},//</animateEffect()>
		animateBtn:function(wait){
			if (this.data.playing){
				this.data.playing=false;
				axs.class_add(this.data.node,'paused');
				this.data.playNode.innerHTML=this.data.play_lbl;
				}
			else{
				this.data.playing=true;
				axs.class_rem(this.data.node,'paused');
				this.data.playNode.innerHTML=this.data.pause_lbl;
				(wait)?window.setTimeout('axs.slider.sliders["'+this.id+'"].animatePlay();',this.data.interval+this.data.transition):this.animatePlay();
				}
			},//</animateBtn()>
		animatePlay:function(){
			if (!this.data.playing) return;
			if (axs.window_visible()) this.animateNext();
			window.setTimeout('axs.slider.sliders["'+this.id+'"].animatePlay();',this.data.interval+this.data.transition);
			},//</animatePlay()>
		animatePrev:function(){
			this.animate((this.data.current<=1) ? this.data.items.length:this.data.current-1);
			},//</animatePrev()>
		animateNext:function(){
			this.animate((this.data.current>=this.data.items.length) ? 1:this.data.current+1);
			},//</animateNext()>
		group:function(count,tag){
			var nr=0, p=1;
			for(var i=0, l=this.data.items.length; i<l; i++){
				nr++;
				if(nr===1){	var el=this.data.list.appendChild(axs.elementCreate(tag,{'id':this.id+'-'+p}));	}
				el.appendChild(this.data.items[i]);
				if(nr===(count)){	nr=0;	p++;	}
				}
			},//</group()>
		pagerCustomize:function(s){
			for(var i=0, l=this.data.items.length; i<l; i++){
				this.data.pagerNode.children[i].innerHTML='';
				var el=(typeof(s)==='string') ? this.data.list.items[i].querySelectorAll(s):s[i];
				for(var ii=0, ll=el.length; ii<ll; ii++) this.data.pagerNode.children[i].appendChild(el[ii]);
				}
			}//</pagerCustomize()>
		},//</base>
	cssAdd:function(css,className){
		if(typeof(css)==='string'){
			if(document.querySelector('html>head link.axs_slider'+className)) return;
			document.querySelector('html>head').appendChild(axs.elementCreate('link',{'className':'axs_slider'+className,type:'text/css', href:css,rel:'stylesheet'}));
			}
		else {
			if(document.querySelector('html>head style.axs_slider'+className)) return;
			var code='';
			for (var k in css) code+='	'+k+' '+css[k]+'\n';
			document.querySelector('html>head').appendChild(axs.elementCreate('style',{'className':'axs_slider'+className},'\n'+code));
			}
		},//</cssAdd()>
	sizeSet:function(slider){
		if(!slider.data.setHeight) return;
		var height=0;
		for(var i=0, l=slider.data.items.length; i<l; i++) if(slider.data.items[i].offsetHeight>height) height=slider.data.items[i].offsetHeight;
		slider.data.list.style.height=height+'px';
		//for(var i=0, l=slider.data.items.length; i<l; i++) slider.data.items[i].style.height=height+'px';
		},//</sizeSet()>
	sizeSetAll:function(){	for(k in axs.slider.sliders) axs.slider.sliders[k].sizeSet();	}//</sizeSetAll()>
	}//</class::axs.slider>
//2013-01-20