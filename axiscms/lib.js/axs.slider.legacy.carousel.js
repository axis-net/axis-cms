//2018-12-23
/*Horizontal slider function. Requires AXIS CMS JS API "axs.js" included first. 
Sample HTML: 
<section id="slider1" class="slider">
 <a href="#slider1" class="scroll prev"><img src="slider.prev.png" alt="&lt;" /></a>
 <a href="#slider1" class="scroll next"><img src="slider.next.png" alt="&gt;" /></a>
 <h2>{$title}</h2>
 <div class="list clear-content">
  <article>
   <a href="#" class="scroll prev"><img src="slider.prev.png" alt="&lt;" /></a>
   <a href="#" class="scroll next"><img src="slider.next.png" alt="&gt;" /></a> 
   Lorem ipsum...
  </article>
 </div>
 <a class="play" href="#slider1"><img src="slider.play.png" alt="play" lang="en" /></a>
 <span class="slider_pager"></span>
 <script>axs.sliderLegacy.init("#slider1",{interval:10000,play:'auto'});</script>
</section>
Elements can be added or removed as necessary. The items container can also be <ul><li> or any other HTML combination. The pager element will be automatically converted to an <ul> tag and filled with <li> tags. 
For configuration options, look for the following code comments. 
*/
axs.sliderLegacy={
	cfg:{
		add_container:false,//["tagName"]:Adds a container around items
		prev_lbl:'&lt;',//["HTML code"]:Scroll back button label
		next_lbl:'&gt;',//["HTML code"]:Scroll next button label
		play_lbl:'<span lang="en">play</span>',//["HTML code"]:Play button label
		pause_lbl:'<span lang="en">pause</span>',//["HTML code"]:Play button label for pause state
		play:'auto',//[true]:Add play button if not present; ["auto"]:Start to play automatically
		playing:false,
		interval:5,//[seconds]:Play speed
		group_count:0,//[number]:Group several items as one slide
		group_tag:'<div></div>',//["HTML code"]:HTML container for groups
		pager:true,//[true]:Fill pager with numbers; ["content"]:copy pages content to pager items
		adjust_height:false//[true]:set equal height to all items
		},
	data:{},
	init:function(id){
		var a=axs.fn_args(arguments,{container:'',cfg:{}});
		var container=(typeof(id)==='string') ? document.querySelector(id):id;
		if (!container) return console.log('Could nof find node for "'+id+'"');
		id=this.id_get(container);
		this.data[id]={node:container,current:1};
		for (k in this.cfg) this.data[id][k]=(typeof(a.cfg[k])!=='undefined')?a.cfg[k]:this.cfg[k];
		this.data[id].interval*=1000;
		var list=jQuery(container).find('.list');
		this.data[id].list=list;
		//this.data[id].width=jQuery(list).width();//jQuery(container).width();
		if(this.data[id].group_count) this.group(id,list,this.data[id].group_count,this.data[id].group_tag);
		this.data[id].items=jQuery(list).find('>*');
		if (!this.data[id].items.length) return;
		
		this.data[id].count=this.data[id].items.length;
		if (this.data[id].count>1){
			var tmp=this.data[id].items[0].cloneNode(true);
			if (tmp.id) tmp.id+='-1';
			jQuery(list).append(tmp);
			}
		//var list=jQuery(container).find('.list');
		this.data[id].items=jQuery(list).find('>*');
		//if(this.data[id].group_count) this.group(id,list,this.data[id].group_count,this.data[id].group_tag);
		//this.data[id].width=jQuery(list).width();//jQuery(container).width();
		jQuery(container).addClass('axs_slider js');
		this.size_set();
		var prev_next=jQuery(container).find('a.scroll.prev, a.scroll.next');
		if ((this.data[id].items.length>1)&&(!prev_next.length)){
			jQuery(container/*this.data[id].items[i]*/).append('<a class="scroll prev" data-container="'+id+'" href="#" title="&lt;">'+this.data[id].prev_lbl+'</a>');
			jQuery(container/*this.data[id].items[i]*/).append('<a class="scroll next" data-container="'+id+'" href="#" title="&gt;">'+this.data[id].next_lbl+'</a>');
			}
		for (var i=0; i<this.data[id].items.length; i++){
			jQuery(this.data[id].items[i]).css({'display':'block','float':'left'});
			if (!jQuery(this.data[id].items[i]).attr('id')) jQuery(this.data[id].items[i]).attr('id',id+'-item'+i);
			/*if ((this.data[id].items.length>1)&&(!prev_next.length)){
				jQuery(this.data[id].items[i]).prepend('<a class="scroll next" data-container="'+id+'" href="#" title="&gt;">'+this.data[id].next_lbl+'</a>');
				jQuery(this.data[id].items[i]).prepend('<a class="scroll prev" data-container="'+id+'" href="#" title="&lt;">'+this.data[id].prev_lbl+'</a>');
				}*/
			if (this.data[id].add_container) this.data[id].items[i].innerHTML='<'+a.cfg.add_container+'>'+this.data[id].items[i].innerHTML+'</'+a.cfg.add_container+'>';
			}
		for (var i=0; i<this.data[id].items.length; i++){
			var prev=(i<=0) ? this.data[id].items.length-1:i-1;
			jQuery(this.data[id].items[i]).find('a.scroll.prev').attr('href','#'+jQuery(this.data[id].items[prev]).attr('id'));
			var next=(i>=this.data[id].items.length-1) ? 0:i+1;
			jQuery(this.data[id].items[i]).find('a.scroll.next').attr('href','#'+jQuery(this.data[id].items[next]).attr('id'));
			}
		jQuery(container).css({'overflow':'hidden'});
		//jQuery(list).css({/*'whiteSpace':'nowrap',*/'width':(this.data[id].items.length*this.data[id].width)+'px'});
		jQuery(container).find('a.scroll.prev').attr('data-container',id).click(function(){
			var id=this.getAttribute('data-container');
			if (axs.sliderLegacy.data[id].playing) axs.sliderLegacy.animate_btn(id);
			return axs.sliderLegacy.animate_prev(id);
			});
		jQuery(container).find('a.scroll.next').attr('data-container',id).click(function(){
			var id=this.getAttribute('data-container');
			if (axs.sliderLegacy.data[id].playing) axs.sliderLegacy.animate_btn(id);
			return axs.sliderLegacy.animate_next(id);
			});
		if (this.data[id].play){
			if (!jQuery(container).find('a.play').length) jQuery(container).prepend('<a class="play" data-container="'+id+'" href="#'+id+'">'+this.data[id].play_lbl+'</a>');
			}
		jQuery(container).find('a.play').click(function(){
			return axs.sliderLegacy.animate_btn(jQuery(this).data('container'));
			});
		if (this.data[id].play==='auto') window.setTimeout('axs.sliderLegacy.animate_btn("'+id+'");',this.data[id].interval);
		window.addEventListener('resize',axs.sliderLegacy.size_set);
		jQuery(this.data[id].items[0]).addClass('current');
		if(this.data[id].pager){
			var el=jQuery(container).find('.slider_pager');
			(el.length) ? $(el).replaceWith($('<ul class="slider_pager"></ul>').html($(el).html())):jQuery(container).append('<ul class="slider_pager"></ul>');
			var el=jQuery(container).find('.slider_pager');
			for(var i=1; i<=this.data[id].count; i++){
				var tmp=(this.data[id].pager==='content')?this.data[id].items[i-1].innerHTML:i;
				jQuery(el).append('<li class="item'+i+'"><a class="scroll" data-container="'+id+'" data-slide="'+i+'" href="#'+this.data[id].items[i-1].id+'">'+tmp+'</a></li>');
				}
			jQuery(container).find('.slider_pager a.scroll').click(function(){
				axs.sliderLegacy.data[id].playing=false;
				axs.sliderLegacy.animate_scroll(this.getAttribute('data-container'),parseInt(this.getAttribute('data-slide')));
				return false;
				});
			jQuery(container).find('.slider_pager>*:first-child').addClass('current');
			}
		},//</init()>
	id_get:function(container){
		if (container.id) return container.id;
		var i=1;
		while((!document.getElementById('slider'+i)) && (i<=1000)) i++;
		container.id='slider'+i;
		},
	size_set:function(){
		var tmp;
		for(id in axs.sliderLegacy.data){
			axs.sliderLegacy.data[id].width=jQuery(axs.sliderLegacy.data[id].list).parent().width();
			for (var i=0; i<axs.sliderLegacy.data[id].items.length; i++) {
				jQuery(axs.sliderLegacy.data[id].items[i]).css({'width':axs.sliderLegacy.data[id].width+'px'});
				}
			jQuery(axs.sliderLegacy.data[id].list).css({/*'whiteSpace':'nowrap',*/'width':(axs.sliderLegacy.data[id].items.length*axs.sliderLegacy.data[id].width)+'px'});
			if (axs.sliderLegacy.data[id].adjust_height)jQuery('#'+id+' .list>*').height(jQuery('#'+id+' .list').height());
			}
		},//</size_set()>
	animate_btn:function(id){
		if (axs.sliderLegacy.data[id].playing){
			axs.sliderLegacy.data[id].playing=false;
			jQuery('#'+id+' a.play').html(axs.sliderLegacy.data[id].play_lbl);
			}
		else{
			axs.sliderLegacy.data[id].playing=true;
			jQuery('#'+id+' a.play').html(axs.sliderLegacy.data[id].pause_lbl);
			axs.sliderLegacy.animate_play(id);
			}
		return false;
		},//</animate_btn()>
	animate_play:function(id){
		if (!axs.sliderLegacy.data[id].playing) return;
		if (axs.window_visible()) this.animate_next(id,'next');
		window.setTimeout('axs.sliderLegacy.animate_play("'+id+'");',axs.sliderLegacy.data[id].interval);
		},//</animate_play()>
	animate_prev:function(id){
		axs.sliderLegacy.animate_scroll(id,'prev');
		return false;
		},//</animate_prev()>
	animate_next:function(id){
		axs.sliderLegacy.animate_scroll(id,'next');
		return false;
		},//</animate_prev()>
	animate_scroll:function(id,action){
		if (axs.sliderLegacy.data[id].count<2) return;
		var container=jQuery("#"+id+" .list");
		var w=axs.sliderLegacy.data[id].width;
		var left=Math.abs(parseInt(container.css('marginLeft')));
		var current_gone=axs.sliderLegacy.data[id].current;
		jQuery("#"+id+" .list>*").removeClass('current');
		if (action==='prev') {
			if (left==0) {
				left=w*axs.sliderLegacy.data[id].count;
				container.css({'marginLeft':-w*(axs.sliderLegacy.data[id].items.length-1)+'px'});
				}
			axs.sliderLegacy.data[id].current--;
			var pos={'marginLeft':'+='+w+'px'};
			}
		if (action==='next') {
			if (left>=w*axs.sliderLegacy.data[id].count-1) {
				left=0;
				container.css({'marginLeft':0});
				}
			axs.sliderLegacy.data[id].current++;
			var pos={'marginLeft':'-='+w+'px'};
			}
		if (typeof(action)==='number') {
			axs.sliderLegacy.data[id].current=action;
			var pos={'marginLeft':'-'+(w*(action-1))+'px'};
			}
		container.animate(pos,"slow");
		if (axs.sliderLegacy.data[id].current>axs.sliderLegacy.data[id].count) axs.sliderLegacy.data[id].current=1;
		if (axs.sliderLegacy.data[id].current<1) axs.sliderLegacy.data[id].current=axs.sliderLegacy.data[id].count;
		jQuery(axs.sliderLegacy.data[id].items[axs.sliderLegacy.data[id].current-1]).addClass('current');
		if (axs.sliderLegacy.data[id].current===1) jQuery(axs.sliderLegacy.data[id].items[axs.sliderLegacy.data[id].items.length-1]).addClass('current');
		console.log(axs.sliderLegacy.data[id].current,axs.sliderLegacy.data[id].items.length);
		jQuery("#"+id+" .slider_pager>*").removeClass('current');
		jQuery("#"+id+" .slider_pager>*.item"+axs.sliderLegacy.data[id].current).addClass('current');
		},//</animate_scroll()>
	group:function(id,container,count,tag){
		var list=jQuery(container).find('>*');
		var nr=0, p=1;
		for(var i=0; i<list.length; i++){
			nr++;
			if(nr===1){
				jQuery(tag).attr('id',id+'-'+p).addClass('page').appendTo(container);
				}
			jQuery(list[i]).detach().appendTo('#'+id+'-'+p);
			if(nr===(count)){
				nr=0;
				p++;
				}
			}
		}//</group()>
	}//</class::axs.sliderLegacy>
//2013-01-20