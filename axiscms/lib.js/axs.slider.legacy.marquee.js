//2013-10-08
axs.marquee={
	data:{},
	init:function(id){
		var a=axs.fn_args(arguments,{id:'',opts:{}});
		this.data[id]={};
		var el=document.getElementById(id);
		if (!el) return;
		el.style.cssFloat=el.style.styleFloat='left';//<styleFloat for IE6 />
		el.style.position='relative';
		el.style.overflow='hidden';
		el.style.whiteSpace='nowrap';
		var separ=document.createElement('span');
		separ.className='separator';
		el.appendChild(separ);
		var width=0;
		var items=el.childNodes;
		for (var i=0; i<items.length; i++){
			if (typeof(items[i].tagName)=='undefined') continue;
			items[i].style.display='block';
			items[i].style.cssFloat=items[i].style.styleFloat='left';//<styleFloat for IE6 />
			width+=parseInt(window.getComputedStyle(items[i]).getPropertyValue('margin-left'));
			width+=items[i].offsetWidth;
			width+=parseInt(window.getComputedStyle(items[i]).getPropertyValue('margin-right'));
			}
		if (width<el.parentNode.offsetWidth) width=el.parentNode.offsetWidth;
		el.style.width=width+'px';
		el.parentNode.style.position='relative';
		el.parentNode.style.overflow='hidden';
		el.parentNode.style.height=el.offsetHeight+'px';
		el.parentNode.style.paddingLeft=el.parentNode.style.paddingRight='0';
		this.data[id].fps=40;//<framerate per second />
		this.data[id].pxps=100;//<pixesl per second />
		for(key in a.opts) this.data[id][key]=a.opts[key];
		this.data[id].pxpf=Math.round(this.data[id].pxps/this.data[id].fps);//<pixesl per frame />
		if (this.data[id].pxpf<1) this.data[id].pxpf=1;
		this.data[id].interval=1000/this.data[id].fps;
		var clone=el.cloneNode(true);
		clone.id=clone.id+'_clone';
		clone.style.left=width+'px';
		clone.style.top=-el.offsetHeight+'px';
		el.parentNode.appendChild(clone);
		this.data[id].timer=window.setInterval('axs.marquee.animate("'+id+'");',this.data[id].interval);
		el.onmouseover=clone.onmouseover=function() {
			var id=this.id.replace(/_clone$/,'');
			clearTimeout(axs.marquee.data[id].timer);
			};
		el.onmouseout=clone.onmouseout=function() {
			var id=this.id.replace(/_clone$/,'');
			axs.marquee.data[id].timer=window.setInterval('axs.marquee.animate("'+id+'");',axs.marquee.data[id].interval);
			}
		},//</init()>
	animate:function(id){
		var content1=document.getElementById(id);
		var content2=document.getElementById(id+'_clone');
		var container=content.parentNode;
		if (content1.offsetLeft>-(content1.offsetWidth)){
			content1.style.left=content1.offsetLeft-axs.marquee.data[id].pxpf+"px";
			content2.style.left=content2.offsetLeft-axs.marquee.data[id].pxpf+"px";
			}
		else {
			content1.style.left=0; //container.offsetWidth+"px";
			content2.style.left=content1.offsetWidth+'px';
			}
		},//</marquee()>
	}//</class::axs.marquee>
//2009-05