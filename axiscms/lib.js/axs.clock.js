//2022-01-04
// Sample HTML:
// <time id="time_current" datetime="2015-12-31 23:59:50"><span class="date">31.12.2015</span> <span class="time">23:59:50</span></time>
// <script>axs.clock.datetime('#time_current');</script>
// <time id="time_left" data-time="10">00:10</time>
// <script>axs.clock.counter('#time_left','-',0);</script>
axs.clock={
	inst:{},//Clock instances
	datetime:function(node,sync=0){//sync server in seconds
		var o={
			node:document.querySelector(node),
			syncInterval:sync*1000,
			server:axs.SITE_ROOT+'?axs%5Bgw%5D%5Bserver-time%5D=date'
			};
		if (!o.node) return axs.log('axs.clock','Node not found');
		if (!o.node.getAttribute('datetime')) o.node.setAttribute('datetime',new Date().toISOString());
		o.clock=new Date(o.node.getAttribute('datetime'));
		o.date=o.node.querySelector('.date');
		o.time=o.node.querySelector('.time');
		o.date.innerHTML=this.setDate(o.clock.getFullYear(),o.clock.getMonth()+1,o.clock.getDate());
		o.time.innerHTML=this.setTime(o.clock.getHours(),o.clock.getMinutes(),o.clock.getSeconds());
		o.syncTime=o.clock.getTime();
		o.update=function(){
			if ((o.syncInterval)&&(o.clock.getTime()-o.syncTime>o.syncInterval)) axs.ajax(o.server,o.sync);
			o.clock.setSeconds(o.clock.getSeconds()+1);
			var t={d:o.clock.getDate(), m:o.clock.getMonth()+1, Y:o.clock.getFullYear(), H:o.clock.getHours(), i:o.clock.getMinutes(), s:o.clock.getSeconds() };
			if (((t.i==00)&&(t.s==00))||(o.expired)) o.date.innerHTML=axs.clock.setDate(t.Y,t.m,t.d);
			o.time.innerHTML=axs.clock.setTime(t.H,t.i,t.s);
			o.node.setAttribute('datetime',o.clock.toISOString());
			o.expired=false;
			}//</update()>
		o.sync=function(status,r){
			//console.log(r.responseText);
			if (r.responseText) o.clock=new Date(r.responseText);
			o.syncTime=o.clock.getTime();
			o.expired=true;
			}//</sync()>
		this.inst[node]=o;
		setInterval(this.inst[node].update,1000);
		},//</datetime()>
	counter:function(node,dir,end=0){
		var o={node:document.querySelector(node),dir:dir,end:end,run:true};
		if (!o.node) return axs.log('axs.clock','Node not found');
		o.update=function(){
			if (!o.run) return;
			var t=Math.round(o.node.getAttribute('data-time'));
			if (o.dir==='-'){	if (t<=o.end) return;	else t--;	}
			else {	if (t>=o.end) return;	else t++;	}
			o.node.setAttribute('data-time',t);
			var time={i:Math.floor(t/60),s:t%60};
			o.node.innerHTML=axs.clock.zeroFill(time.i)+':'+axs.clock.zeroFill(time.s);
			}//</update()>
		this.inst[node]=o;
		setInterval(this.inst[node].update,1000);
		},//</counter()>
	setDate:function(Y,m,d){	return this.zeroFill(d)+"."+this.zeroFill(m)+"."+this.zeroFill(Y);	},//</setDate()>
	setTime:function(H,i,s){	return this.zeroFill(H)+":"+this.zeroFill(i)+":"+this.zeroFill(s);	},//</setTime()>
	zeroFill:function(i){	return (i<10) ? i="0"+i:""+i;	}//</zeroFill()>
	}//</class::axs.clock>
//2010-09-28