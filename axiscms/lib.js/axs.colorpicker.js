//2014-06-23
// Colorpicker UI script. Requires AXIS CMS JS API "axs.js" included first. 
var CROSSHAIRS_LOCATION=axs.PATH_CMS+'lib.js/axs.colorpicker-selector.gif';
var HUE_SLIDER_LOCATION=axs.PATH_CMS+'lib.js/axs.colorpicker-hue.png';
var HUE_SLIDER_ARROWS_LOCATION=axs.PATH_CMS+'lib.js/axs.colorpicker-arrows.png';
var SAT_VAL_SQUARE_LOCATION=axs.PATH_CMS+'lib.js/axs.colorpicker-bg.png';

function hexToRgb(hex_string, default_) {
    if (default_ == undefined) default_ = null;
	if (hex_string.substr(0, 1) == '#') hex_string = hex_string.substr(1);
    
    var r;
    var g;
    var b;
    if (hex_string.length == 3) {
        r = hex_string.substr(0, 1);
        r += r;
		g = hex_string.substr(1, 1);
		g += g;
		b = hex_string.substr(2, 1);
		b += b;
		}
    else if (hex_string.length == 6) {
        r = hex_string.substr(0, 2);
        g = hex_string.substr(2, 2);
		b = hex_string.substr(4, 2);
		}
    else return default_;

    r = parseInt(r, 16);
    g = parseInt(g, 16);
	b = parseInt(b, 16);
	if (isNaN(r) || isNaN(g) || isNaN(b)) return default_;
    else {
        return {r: r / 255, g: g / 255, b: b / 255};
		}
	}

function rgbToHex(r, g, b, includeHash) {
    r = Math.round(r * 255);
    g = Math.round(g * 255);
    b = Math.round(b * 255);
    if (includeHash == undefined) includeHash = true;
	
    
    r = r.toString(16);
    if (r.length == 1) {
        r = "0" + r;
    	}
    g = g.toString(16);
    if (g.length == 1) {
        g = "0" + g;
    	}
    b = b.toString(16);
    if (b.length == 1) {
        b = "0" + b;
    	}
    return ((includeHash ? '#' : '') + r + g + b).toUpperCase();
	}

var arVersion = navigator.appVersion.split("MSIE");
var version = parseFloat(arVersion[1]);

function fixPNG(myImage) {
    if ((version >= 5.5) && (version < 7) && (document.body.filters)) {
        var node = document.createElement("span");
        node.id = myImage.id;
        node.className = myImage.className;
        node.title = myImage.title;
        node.style.cssText = myImage.style.cssText;
        node.style.setAttribute("filter", "progid:DXImageTransform.Microsoft.AlphaImageLoader"
                                        + "(src='" + myImage.src + "', sizingMethod='scale')");
        node.style.fontSize = "0";
        node.style.width = myImage.width.toString() + "px";
        node.style.height = myImage.height.toString() + "px";
        node.style.display = "inline-block";
        return node;
    	}
    else {
        return myImage.cloneNode(false);
    	}
	}

function trackDrag(node, handler)
{
    function fixCoords(x, y)
    {
        var nodePageCoords = pageCoords(node);
        x = (x - nodePageCoords.x) + document.documentElement.scrollLeft;
        y = (y - nodePageCoords.y) + document.documentElement.scrollTop;
        if (x < 0) x = 0;
        if (y < 0) y = 0;
        if (x > node.offsetWidth - 1) x = node.offsetWidth - 1;
        if (y > node.offsetHeight - 1) y = node.offsetHeight - 1;
        return {x: x, y: y};
    }
    function mouseDown(ev)
    {
        var coords = fixCoords(ev.clientX, ev.clientY);
        var lastX = coords.x;
        var lastY = coords.y;
        handler(coords.x, coords.y);

        function moveHandler(ev)
        {
            var coords = fixCoords(ev.clientX, ev.clientY);
            if (coords.x != lastX || coords.y != lastY)
            {
                lastX = coords.x;
                lastY = coords.y;
                handler(coords.x, coords.y);
            }
        }
        function upHandler(ev)
        {
            RemoveEventListener(document, "mouseup", upHandler);
            RemoveEventListener(document, "mousemove", moveHandler);
            AddEventListener(node, "mousedown", mouseDown);
        }
        AddEventListener(document, "mouseup", upHandler);
        AddEventListener(document, "mousemove", moveHandler);
        RemoveEventListener(node, "mousedown", mouseDown);
        if (ev.preventDefault) ev.preventDefault();
    }
    AddEventListener(node, "mousedown", mouseDown);
    node.onmousedown = function(e) { return false; };
    node.onselectstart = function(e) { return false; };
    node.ondragstart = function(e) { return false; };
}

var eventListeners = [];

function findEventListener(node, event, handler)
{
    var i;
    for (i in eventListeners)
    {
        if (eventListeners[i].node == node && eventListeners[i].event == event
         && eventListeners[i].handler == handler)
        {
            return i;
        }
    }
    return null;
}
function AddEventListener(node, event, handler)
{
    if (findEventListener(node, event, handler) != null)
    {
        return;
    }

    if (!node.addEventListener)
    {
        node.attachEvent("on" + event, handler);
    }
    else
    {
        node.addEventListener(event, handler, false);
    }

    eventListeners.push({node: node, event: event, handler: handler});
}

function removeEventListenerIndex(index)
{
    var eventListener = eventListeners[index];
    delete eventListeners[index];
    
    if (!eventListener.node.removeEventListener)
    {
        eventListener.node.detachEvent("on" + eventListener.event,
                                       eventListener.handler);
    }
    else
    {
        eventListener.node.removeEventListener(eventListener.event,
                                               eventListener.handler, false);
    }
}

function RemoveEventListener(node, event, handler)
{
    removeEventListenerIndex(findEventListener(node, event, handler));
}

function cleanupEventListeners()
{
    var i;
    for (i = eventListeners.length; i > 0; i--)
    {
        if (eventListeners[i] != undefined)
        {
            removeEventListenerIndex(i);
        }
    }
}
AddEventListener(window, "unload", cleanupEventListeners);

// This copyright statement applies to the following two functions,
// which are taken from MochiKit.
//
// Copyright 2505 Bob Ippolito <bob@redivi.com>
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject
// to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

function hsvToRgb(hue, saturation, value)
{
    var red;
    var green;
    var blue;
    if (value == 0.0)
    {
        red = 0;
        green = 0;
        blue = 0;
    }
    else
    {
        var i = Math.floor(hue * 6);
        var f = (hue * 6) - i;
        var p = value * (1 - saturation);
        var q = value * (1 - (saturation * f));
        var t = value * (1 - (saturation * (1 - f)));
        switch (i)
        {
            case 1: red = q; green = value; blue = p; break;
            case 2: red = p; green = value; blue = t; break;
            case 3: red = p; green = q; blue = value; break;
            case 4: red = t; green = p; blue = value; break;
            case 5: red = value; green = p; blue = q; break;
            case 6: // fall through
            case 0: red = value; green = t; blue = p; break;
        }
    }
    return {r: red, g: green, b: blue};
}

function rgbToHsv(red, green, blue)
{
    var max = Math.max(Math.max(red, green), blue);
    var min = Math.min(Math.min(red, green), blue);
    var hue;
    var saturation;
    var value = max;
    if (min == max)
    {
        hue = 0;
        saturation = 0;
    }
    else
    {
        var delta = (max - min);
        saturation = delta / max;
        if (red == max)
        {
            hue = (green - blue) / delta;
        }
        else if (green == max)
        {
            hue = 2 + ((blue - red) / delta);
        }
        else
        {
            hue = 4 + ((red - green) / delta);
        }
        hue /= 6;
        if (hue < 0)
        {
            hue += 1;
        }
        if (hue > 1)
        {
            hue -= 1;
        }
    }
    return {
        h: hue,
        s: saturation,
        v: value
    };
}

function pageCoords(node)
{
    var x = node.offsetLeft;
    var y = node.offsetTop;
    var parent = node.offsetParent;
    while (parent != null)
    {
        x += parent.offsetLeft;
        y += parent.offsetTop;
        parent = parent.offsetParent;
    }
    return {x: x, y: y};
}

// The real code begins here.
var huePositionImg = document.createElement("img");
huePositionImg.galleryImg = false;
huePositionImg.width = 38;
huePositionImg.height = 11;
huePositionImg.src = HUE_SLIDER_ARROWS_LOCATION;
huePositionImg.style.position = "absolute";

var hueSelectorImg1 = document.createElement("img");
hueSelectorImg1.galleryImg = false;
hueSelectorImg1.width = 30;
hueSelectorImg1.height = 250;
hueSelectorImg1.src = HUE_SLIDER_LOCATION;
hueSelectorImg1.style.display = "block";

var satValImg = document.createElement("img");
satValImg.galleryImg = false;
satValImg.width = 250;
satValImg.height = 250;
satValImg.src = SAT_VAL_SQUARE_LOCATION;
satValImg.style.display = "block";

var crossHairsImg = document.createElement("img");
crossHairsImg.galleryImg = false;
crossHairsImg.width = 13;
crossHairsImg.height = 13;
crossHairsImg.src = CROSSHAIRS_LOCATION;
crossHairsImg.style.position = "absolute";




function makeColorSelector(inputData)
{
    var rgb, hsv
    
    function colorChanged() {
        var hex = rgbToHex(rgb.r, rgb.g, rgb.b);
        var hueRgb = hsvToRgb(hsv.h, 1, 1);
        var hueHex = rgbToHex(hueRgb.r, hueRgb.g, hueRgb.b);
        previewDiv.style.background = hex;
		inputData.style.color = hex;
        inputData.value = hex;
		// <RGB input>
		axs_colorpicker_r.value=Math.round(255*rgb.r);
		axs_colorpicker_g.value=Math.round(255*rgb.g);
		axs_colorpicker_b.value=Math.round(255*rgb.b);
		// </RGB input>
        satValDiv.style.background = hueHex;
        crossHairs.style.left = ((hsv.v*249)-10).toString() + "px";
        crossHairs.style.top = (((1-hsv.s)*249)-10).toString() + "px";
        huePos.style.top = ((hsv.h*249)-5).toString() + "px";
		}
    function rgbChanged() {
        hsv = rgbToHsv(rgb.r, rgb.g, rgb.b);
        colorChanged();
		}
    function hsvChanged() {
        rgb = hsvToRgb(hsv.h, hsv.s, hsv.v);
        colorChanged();
		}
    
    var colorSelectorDiv = document.createElement("div");
    colorSelectorDiv.className = "colorpicker visuallyhidden";
	colorSelectorDiv.style.border = "solid 1px #000";
	colorSelectorDiv.style.padding = "10px";
    colorSelectorDiv.style.position = "absolute";//"relative";
    colorSelectorDiv.style.height = "250px";
    colorSelectorDiv.style.width = "400px";
	colorSelectorDiv.style.backgroundColor = "#fff";
    
	inputData.color_selector=colorSelectorDiv;
	inputData.onfocus=function(){	return axs.colorpicker.ui_open(this.color_selector);	}
	
    var satValDiv = document.createElement("div");
    satValDiv.style.position = "relative";
    satValDiv.style.width = "250px";
    satValDiv.style.height = "250px";
    //satValDiv.style.border = "1px solid black";
    var newSatValImg = fixPNG(satValImg);
    satValDiv.appendChild(newSatValImg);
    var crossHairs = crossHairsImg.cloneNode(false);
    satValDiv.appendChild(crossHairs);
    function satValDragged(x, y) {
        hsv.s = 1-(y/249);
        hsv.v = (x/249);
        hsvChanged();
		}
    trackDrag(satValDiv, satValDragged)
    colorSelectorDiv.appendChild(satValDiv);

    var hueDiv = document.createElement("div");
    hueDiv.style.position = "absolute";
    hueDiv.style.left = "260px";
    hueDiv.style.top = "10px";
    hueDiv.style.width = "30px";
    hueDiv.style.height = "250px";
    var huePos = fixPNG(huePositionImg);

    hueDiv.appendChild(hueSelectorImg1.cloneNode(false));
    hueDiv.appendChild(huePos);
    function hueDragged(x, y) {
        hsv.h = y/249;
        hsvChanged();
		}
    trackDrag(hueDiv, hueDragged);
    colorSelectorDiv.appendChild(hueDiv);
    
    var previewDiv = document.createElement("div");
    previewDiv.style.height = "50px"
    previewDiv.style.width = "50px";
    previewDiv.style.position = "absolute";
    previewDiv.style.top = "10px";
    previewDiv.style.left = "300px";
    previewDiv.style.border = "1px solid black";
    colorSelectorDiv.appendChild(previewDiv);
	
	//<RGB controls>
	function input_rgb_changed() {
		rgb={
			r: 1/255*parseInt(Number(axs_colorpicker_r.value), 10),
			g: 1/255*parseInt(Number(axs_colorpicker_g.value), 10),
			b: 1/255*parseInt(Number(axs_colorpicker_b.value), 10)
			};
        rgbChanged();
    	}
	var axs_colorpicker_rgb=document.createElement("fieldset");
	axs_colorpicker_rgb.style.position="absolute";
    axs_colorpicker_rgb.style.top="65px";
    axs_colorpicker_rgb.style.left="300px";
	axs_colorpicker_rgb.style.margin=axs_colorpicker_rgb.style.padding="0";
	axs_colorpicker_rgb.style.border="none";
	axs_colorpicker_rgb.style.width="50px";
	
	var label=document.createElement("label");
	label.innerHTML='R';
	var axs_colorpicker_r=document.createElement("input");
	axs_colorpicker_r.type='text';
	axs_colorpicker_r.size='3';
	axs_colorpicker_r.style.width="30px";
	AddEventListener(axs_colorpicker_r, "keyup", input_rgb_changed);
	//axs_colorpicker_r.color_selector=inputData;
	//axs_colorpicker_r.onfocus=function(){	return axs.colorpicker.ui_open(this.color_selector);	}
	label.appendChild(axs_colorpicker_r);
	axs_colorpicker_rgb.appendChild(label);
	
	var label=document.createElement("label");
	label.innerHTML='G';
	var axs_colorpicker_g=document.createElement("input");
	axs_colorpicker_g.type='text';
	axs_colorpicker_g.size='3';
	axs_colorpicker_g.style.width="30px";
	AddEventListener(axs_colorpicker_g, "keyup", input_rgb_changed);
	//axs_colorpicker_g.color_selector=inputData;
	//axs_colorpicker_g.onfocus=function(){	axs.colorpicker.ui_open(this.color_selector);	}
	label.appendChild(axs_colorpicker_g);
	axs_colorpicker_rgb.appendChild(label);
	
	var label=document.createElement("label");
	label.innerHTML='B';
	var axs_colorpicker_b=document.createElement("input");
	axs_colorpicker_b.type='text';
	axs_colorpicker_b.size='3';
	axs_colorpicker_b.style.width="30px";
	AddEventListener(axs_colorpicker_b, "keyup", input_rgb_changed);
	//axs_colorpicker_b.color_selector=inputData;
	//axs_colorpicker_b.onfocus=function(){	axs.colorpicker.ui_open(this.color_selector);	}
	label.appendChild(axs_colorpicker_b);
	axs_colorpicker_rgb.appendChild(label);
	colorSelectorDiv.appendChild(axs_colorpicker_rgb);
	//</RGB controls>
	
	//<Disable button>
	var a=document.createElement("a");
	a.href="#";
	a.innerHTML='-';
	a.lang='en';
	a.title='clear / restore';
	a.style.position="absolute";
	a.style.left="300px";
	a.style.top="235px";
	a.style.padding="2px";
	a.style.fontSize="14px";
	a.style.textDecoration="none";
	a.style.color="#F00";
	a.style.border="1px solid #000";
	a.color_selector=inputData;
	a.onclick=function(){
		if (this.color_selector.value) {
			this.color_selector.value_bak=this.color_selector.value;
			this.color_selector.type="text";
			this.color_selector.value="";
			console.log(this.color_selector.value);
			}
		else {
			if (this.color_selector.value_bak) {
				//this.color_selector.type="color";
				if (!this.color_selector.value) this.color_selector.value=this.color_selector.value_bak;
				this.color_selector.value_bak='';
				}
			}
		return false;
		}
	colorSelectorDiv.appendChild(a);
	//</Disable button>
	
	//<Close button>
	var a=document.createElement("a");
	a.href="#";
	a.innerHTML='x';
	a.lang='en';
	a.title='close';
	a.style.position="absolute";
	a.style.left="390px";
	a.style.top="10px";
	a.style.padding="0 2px";
	a.style.background="rgb(240,240,240)";
	a.style.fontSize="14px";
	a.style.textDecoration="none";
	a.style.color="#000";
	a.style.border="1px solid #000";
	a.color_selector=inputData.color_selector;
	a.onfocus=a.onblur=a.onclick=function(){	return axs.colorpicker.ui_close(this.color_selector);	}
	colorSelectorDiv.appendChild(a);
	//</Close button>
	
    function inputDataChanged() {
        rgb = hexToRgb(inputData.value, {r: 0, g: 0, b: 0});
        rgbChanged();
		}
    AddEventListener(inputData, "change", inputDataChanged);
    //inputData.size = 8;
    //inputData.style.position = "absolute";
    //inputData.style.right = "105px";
    //inputData.style.top = "250px";
    //colorSelectorDiv.appendChild(inputData);
    
    inputDataChanged();
    
    return colorSelectorDiv;
	}
//AddEventListener(window, "load", makeColorSelectors);

axs.colorpicker={
	init:function(){
		var a=axs.fn_args(arguments,{selector:'color'});
		if (typeof(a.selector)==='object') return this.add(a.selector);
		switch (a.selector.charAt(0)){
			case '#': return this.add_by_id(a.selector);
			case '.': return this.add_by_class(a.selector);
			default: return this.add_by_type(a.selector);
			}
		},//</init()>
	add:function(node){
		if (node.axs_colorpicker) return;
		node.axs_colorpicker=true;
		var initial_value=node.value;
		var selector=makeColorSelector(node);
		//node.parentNode.insertBefore(selector,(node.previousSibling ? node.previousSibling.nextSibling:null));
		node.parentNode.insertBefore(selector,node.nextSibling);
		node.value=initial_value;
		},//</add()>
	add_by_class:function(class_name){
		var nodes=document.getElementsByTagName("input");
		var regex=new RegExp('\\b'+class_name.replace('.','')+'\\b');
		for (var i=0; i<nodes.length; i++) if (regex.exec(nodes[i].className)) this.add(nodes[i]);
		},//</add_by_class()>
	add_by_id:function(id){
		var node=document.getElementById(id.replace('#',''));
		if (node) this.add(node);
		},//</add_by_id()>
	add_by_type:function(type){
		var nodes=document.getElementsByTagName("input");
		for (var i=0; i<nodes.length; i++) if (nodes[i].getAttribute('type')===type) this.add(nodes[i]);
		},//</add_by_class()>
	ui_close:function(node){	node.className+=' visuallyhidden';	return false;	},//</ui_ui_close()>
	ui_open:function(node){	node.className=node.className.replace(/visuallyhidden/g,'');	return false;	}//</ui_ui_open()>
	}//</class::axs.colorpicker>
//2007-12