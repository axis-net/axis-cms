//2020-06-12
axs.imgPreview={
	groups:[],
	init:function(list,target){
		var a=axs.fn_args(arguments,{setImg:'href'});
		var key=this.groups.length;
		this.groups[key]={
			setImg:a.setImg,
			listSelector:list,
			list:document.querySelector(list).children,
			targetSelector:target,
			target:document.querySelector(target)
			};
		var current=false;
		for(var i=0; i<this.groups[key].list.length; i++){
			this.groups[key].list[i].setAttribute('data-group',key);
			this.groups[key].list[i].addEventListener('click',function(e){
				e.preventDefault();
				axs.imgPreview.set(this);
				});
			if (axs.class_has(this.groups[key].list[i],'current')) current=i;
			}
		if (current===false) axs.class_add(this.groups[key].list[0],'current');
		if (!document.querySelector(target).innerHTML) this.set(this.groups[key].list[0]);
		},//</init()>
	set:function(el){
		var key=parseInt(el.getAttribute('data-group'));
		var list=axs.imgPreview.groups[key].list;
		axs.class_rem(list,'current');
		axs.class_add(el,'current');
		var target=axs.imgPreview.groups[key].target;
		target.href=el.href;
		var targetImg=target.querySelector('img');
		thisImg=el.querySelector('img');
		switch(this.groups[key].setImg){
			case 'src':	var src=thisImg.getAttribute(src);	break;
			default: var src=el.href;
			};
		targetImg.setAttribute('src',src);
		targetImg.setAttribute('alt',thisImg.getAttribute('alt'));
		}//</set()>
	};//</axs.imgPreview>
//2020-06-12