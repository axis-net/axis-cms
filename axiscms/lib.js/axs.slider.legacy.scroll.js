//2018-01-23
/*Horizontal scroll extension to "axs.slider.js". 
*/
axs.slider.scroll=Object.create(axs.slider.base);
axs.slider.scroll.data.currentItem=null;
axs.slider.scroll.cfg.play=false;
axs.slider.scroll.cfg.pager=false;
axs.slider.scroll.cfg.transition=2;
axs.slider.scroll.cfg.css['.axs_slider.scroll']='{	overflow:hidden;	}\n';
axs.slider.scroll.cfg.css['.axs_slider.scroll .list>*']='{	display:block;	float:left;	}\n';
axs.slider.scroll.initLocal=function(){
	this.cfg.css['.axs_slider.scroll .list']='{	transition:margin-left '+this.data.transition+'s ease;	}';
	jQuery(this.data.items).attr('data-container',id).click(function(){
		var id=this.getAttribute('data-container');
		axs.slider.sliders[id].data.currentItem=this;
		if (axs.slider.sliders[id].data.playing) axs.slider.sliders[id].animate_btn(id);
		return axs.slider.sliders[id].animate_scroll(id,'item');
		});
	};//</initLocal()>
axs.slider.scroll.size_set=function(){
	var tmp;
	for(id in axs.slider.sliders) if (axs.slider.sliders[id].data.effect==='scroll'){
		var data=axs.slider.sliders[id].data;
		//var width=data.list.parentNode.offsetWidth;
		var width=jQuery(axs.slider.sliders[id].data.list).parent().width();
		var widthItems=0;
		for (var i=0; i<data.items.length; i++) {
			var w=data.items[i].getBoundingClientRect().width;
			widthItems+=w;
			//data.items[i].style.width=w+'px';
			if (data.adjust_height) data.items[i].style.height=data.list.offsetHeight+'px';
			}
		widthItems=Math.ceil(widthItems/width)*width;
		jQuery(data.list).css({width:widthItems+'px'});
		//data.list.style.width=widthItems+'px';
		axs.slider.sliders[id].data.countPages=Math.ceil(widthItems/width);
		axs.slider.sliders[id].data.width=width;
		axs.slider.sliders[id].data.widthItems=widthItems;
		var el=axs.slider.sliders[id].data.node.querySelectorAll('a.scroll');
		if (widthItems<=width) axs.class_add(el,'disabled');
		else axs.class_rem(el,'disabled');
		}
	};//</size_set()>
axs.slider.scroll.animate_scroll=function(id,action){
	var container=axs.slider.sliders[id].data.list;
	var w=axs.slider.sliders[id].data.width;
	var left=Math.abs(parseInt(container.css('marginLeft')));
	var pos=left;
	if (action==='prev') {	pos=(left==0)?axs.slider.sliders[id].data.widthItems-w:left-w;	}
	if (action==='next') {	pos=(left>=axs.slider.sliders[id].data.widthItems-w)?0:left+w;	}
	if (typeof(action)==='number') pos=w*(action-1);
	if (action==='item') pos=left+axs.slider.sliders[id].data.currentItem.offsetWidth;
	if (pos<0) pos=0;
	if (pos>axs.slider.sliders[id].data.widthItems-w) pos=axs.slider.sliders[id].data.widthItems-w;
	if (left!==pos) document.querySelector('#'+id+' .list').style.marginLeft='-'+pos+'px';
	axs.slider.sliders[id].data.current=(pos<axs.slider.sliders[id].data.widthItems-w)?Math.floor(pos/w)+1:axs.slider.sliders[id].data.countPages;
	if (axs.slider.sliders[id].data.current>axs.slider.sliders[id].data.countPages) axs.slider.sliders[id].data.current=1;
	if (axs.slider.sliders[id].data.current<1) axs.slider.sliders[id].data.current=axs.slider.sliders[id].data.countPages;
	if (axs.slider.sliders[id].data.pager){
		axs.class_rem(document.querySelectorAll("#"+id+" .slider_pager>*"),'current');
		axs.class_add(document.querySelector("#"+id+" .slider_pager>*.item"+axs.slider.sliders[id].data.current),'current');
		}
	};//</animate_scroll()>
//2017-08-14