//2020-01-07
/* logo():
	s:(string) DOM selector
	cfg:{runEvent:'img'|'dom'|'doc'} Run on img load, DOMContentLoaded or document onload
Sample HTML:
<a class="logo" href="./"><img src="logo.svg" alt="Logo" /></a>'."\n".
<script>axs.animationcontrol.logo("a.logo",{runEvent:"doc"});</script>

Sample for list():
CSS:
@keyframes listAnim {
		0%{	opacity:0;	}
		100%{	opacity:1;	}
		}
	#content.anim section,
	#content.anim section.run.wait {
		opacity:0;
		animation-delay:1s;
		animation-duration:0.1s;
		animation-iteration-count:1;
		}
		#content.anim section.run {
			animation-name:listAnim;
			opacity:1;
			}
HTML:
<main>
	<script>axs.animationcontrol.listInit("main");</script>
	axs.animationcontrol.listInit("#content",">section",{delay:0.1,smooth:15});
	<section>Lorem ipsum</section>
	<section>Dolor sit</section>
	<script>axs.animationcontrol.listRun(">section");</script>
</main>
*/
axs.animationcontrol={
	list:[],
	init:function(){
		axs.class_add(document.querySelector('html'),'axs_animationcontrol');
		document.addEventListener('DOMContentLoaded',function(){
			for(var i=0; i<axs.animationcontrol.list.length; i++){	axs.animationcontrol.listRun(i);	}
			});
		},//</init()>
	logo:function(s){
		var a=axs.fn_args(arguments,{s:'',cfg:{runEvent:'img'}});
		var el=document.querySelector(s);
		axs.class_add(el,['anim','wait']);
		var img=el.querySelector('img');
		img.addEventListener('animationstart',function(){	axs.class_rem(this.parentNode,'wait');	});
		img.addEventListener('animationend',function(){	axs.class_add(this.parentNode,'end');	axs.class_rem(this.parentNode,'wait');	});
		switch(a.runEvent){
			case 'dom': document.addEventListener('DOMContentLoaded',function(){	axs.class_add(el,'run');	},el);
				break;
			case 'doc': document.addEventListener('load',function(){	axs.class_add(el,'run');	},el);
				break;
			case 'img':
			default:
			if (img.complete) axs.class_add(el,'run');
			else {
				img.addEventListener('load',function(){	axs.class_add(this.parentNode,'run');	});
				img.addEventListener('error',function(){	axs.class_add(this.parentNode,'run');	});
				}
			}
		},//</animLogo()>
	listInit:function(container,children){
		var a=axs.fn_args(arguments,{container:'',children:'',cfg:{delay:0.4,smooth:15}});
		axs.class_add(document.querySelector(container),['axs','animationcontrol']);
		//if(!this.eventReg) this.eventReg=
		this.list.push({s:container+children,delay:a.cfg.delay,smooth:a.cfg.smooth});
		},//</listInit()>
	listRun:function(item){
		var el=document.querySelectorAll(this.list[item].s);
		if(!el.length) return console.log('No items found for "'+this.list[item].s+'"');
		axs.class_add(el,'wait');
		for(var i=0, l=el.length; i<l; i++){
			var t=parseFloat(window.getComputedStyle(el[i]).getPropertyValue('animation-delay'));
			el[i].style.animationDelay=(t+i*this.list[item].delay)+'s';
			if(this.list[item].smooth){
				var t=parseFloat(window.getComputedStyle(el[i]).getPropertyValue('animation-duration'));
				el[i].style.animationDuration=(t+Math.exp(i/this.list[item].smooth))+'s';
				}
			el[i].addEventListener('animationstart',function(){	axs.class_rem(this,'wait');	});
			}
		axs.class_add(el,'run');
		}//</listRun()>
	}//</class::axs.animationcontrol>
axs.animationcontrol.init();
//2020-01-07