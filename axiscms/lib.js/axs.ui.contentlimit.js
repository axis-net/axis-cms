//2021-10-16
/*Limit content size with open/close switches. 
HTML structure: 
<div class="text">
	<div>{$text}</div>
</div>
<script>axs.ui.contentlimit.pixels('div.text',{'open.btn':'<span>Open</span>','close.btn':'<span>Close</span>'});</script>
CSS: 
html.axs_ui_contentlimit div.text {	max-height:...;	}
*/
axs.ui.contentlimit={
	init:function(){
		axs.class_add(document.querySelector('html'),'axs_ui_contentlimit');
		document.querySelector('html>head').innerHTML+=
		'<style>\n'+
		'html.axs_ui_contentlimit .contentlimit.pixels.open { max-height:none;	}\n'+
		'.contentlimit.pixels { position:relative;	overflow:hidden;	}\n'+
		'	.contentlimit.pixels span.switch {	display:block;	width:100%;	}\n'+
		'	.contentlimit.pixels span.switch.js_hide {	display:none;	}\n'+
		'	.contentlimit.pixels span.switch.open {	position:absolute;	bottom:0; left:0;	right:0;	}\n'+
		'	.contentlimit.pixels span.switch button {	cursor:poiner;	}\n'+
		'</style>\n';
		},//</init()>
	pixels:function(selector){
		var a=axs.fn_args(arguments,{selector:'',labels:{}});
		var labels={'open.btn':'<span lang="en">open</span>','close.btn':'<span lang="en">close</span>'};
		for(k in labels){	if(typeof(a.labels[k])==='undefined') a.labels[k]=labels[k];	}
		var el=document.querySelectorAll(selector);
		for (var l=el.length, i=0; i<l; i++) {
			axs.class_add(el[i],['contentlimit','pixels']);
			var btn=document.createElement('span');
			btn.className='switch close';
			btn.innerHTML='<button>'+a.labels['close.btn']+'</button>';
			el[i].appendChild(btn);
			el[i].querySelector('span.switch.close>button').onclick=function(){	return axs.ui.contentlimit.pixelsClose(this.parentNode.parentNode);	}
			var btn=document.createElement('span');
			btn.className='switch open';
			btn.innerHTML='<button>'+a.labels['open.btn']+'</button>';
			el[i].appendChild(btn);
			el[i].querySelector('span.switch.open>button').onclick=function(){	return axs.ui.contentlimit.pixelsOpen(this.parentNode.parentNode);	}
			this.pixelsClose(el[i]);
			}
		window.addEventListener('resize',function(){
			var el=document.querySelectorAll(selector);
			for (var l=el.length, i=0; i<l; i++) this.pixelsClose(el[i]);
			});
		},//</pixels()>
	pixelsClose:function(node){
		axs.class_rem(node,['open','overflow']);
		axs.class_add(node,'close');
		axs.class_add(node.querySelectorAll('span.switch'),'js_hide');
		if (node.clientHeight>=node.children[0].offsetHeight) return false;
		axs.class_add(node,'overflow');
		axs.class_rem(node.querySelector('span.switch.open'),'js_hide');
		return false;
		},//</pixelsClose()>
	pixelsOpen:function(node){
		axs.class_rem(node,['close','overflow']);
		axs.class_add(node,'open');
		axs.class_add(node.querySelector('span.switch.open'),'js_hide');
		axs.class_rem(node.querySelector('span.switch.close'),'js_hide');
		return false;
		}//</pixelsOpen()>
	};//</class::contentlimit>
axs.ui.contentlimit.init();
//2017-10-09