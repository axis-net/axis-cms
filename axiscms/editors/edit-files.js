//2022-05-09
axs.filemanager={
	init:function(){
		$('#hdir_btn').addClass('visuallyhidden');
		$('#hdir').attr('onchange','submit();');
		$('#del_lbl').addClass('visuallyhidden');
		$('#del_btn').click(function(){
			if (confirm($('#del_lbl').text())) $('#del').attr('checked',true);
			else return false;
			});
		$('#edit-files td.edit input, #edit-files td.edit_btn input').click(function(){
			var tr=this.closest('tr');
			var chk=tr.querySelector('td.edit input');
			if (chk.checked) $(tr).addClass('checked');
			else $(tr).removeClass('checked');
			if (this.getAttribute('type')=='submit') $(chk).attr('checked',true);
			
			/*var id=this.id.replace(/[^0-9]/g,'');
			if ($('#f'+id+':checked').val()!==undefined) $('#nr'+id).addClass('checked');
			else $('#nr'+id).removeClass('checked');
			if (this.getAttribute('type')=='submit') $('#f'+id).attr('checked',true);*/
			});
		var browse=axs.url_get('browse');
		if (browse){
			$('#edit-files td.file a').removeAttr('target');
			switch(browse){
				case 'f':
				case 'f2':
					$('#edit-files td.type a, #edit-files td.file a').click(axs_tab_edit.file_set);
					break;
				case 'img':
				case 'link':
				case 'swf':
					$('#edit-files td.type a, #edit-files td.file a').click(axs.textedit.file_set);
					break;
				}
			}
		}//</init()>
	};//</class::axs.filemanager>
//2011-06-13