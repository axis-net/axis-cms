<?php #2016-06-29
class axs_edit_system extends axs_config_edit {
	function __construct(&$e) {
		global $axs;
		$this->e=&$e;
		$this->post=$_POST;
		$this->post_form=axs_get('form_id', $axs['get']);
		foreach (array('ftp') as $v) if (isset($_POST[$v])) $_POST[$v]='*';
		
		$this->url=$axs['url'];
		$this->inc=axs_get('s1', $_GET);
		$this->url['s1']=urlencode($this->inc);
		$this->form=new axs_form_edit(1, $this->url, false, '', $_POST);
		} #</axs_edit_system()>
	function ui_groups($f) {
		global $axs;
		$tpl=array('table'=>
		'     <form id="'.$f.'" class="form" action="{$formact_url}" method="post">'."\n".
		'{$msg}'.
		'      <table class="table">'."\n".
		'       <caption>{$groups.'.$f.'.title}</caption>'."\n".
		'       <tr>'."\n".
		'        <th><label for="group_add">{$groups.key.lbl}</label></th>'."\n".
		'        <th><label for="group_order">{$groups.order.lbl}</label></th>'."\n".
		'        <th><label for="group_add_lbl">{$groups.label.lbl}</label></th><th>{$groups.del.lbl}</th>'."\n".
		'       </tr>'."\n".
		'{$table}      <tr>'."\n".
		'        <th><input id="group_add" name="group_add" size="10" value="{$group_add}" /></th>'."\n".
		'        <td><select id="group_add_after" name="group_add_after">{$group_add_after}"</select></td>'."\n".
		'        <td><input id="group_add_lbl" name="group_add_lbl" size="50" value="{$group_add_lbl}" /></td>'."\n".
		'       </tr>'."\n".
		'      </table>'."\n".
		'      <input name="save" type="submit" value="{$groups.save.lbl}" />'."\n".
		'     </form>'."\n",
		'row'=>
		'       <tr>'."\n".
		'        <th class="left"><label for="{$key}">{$key}</label></th>'."\n".
		'        <td><input id="{$key}_order" name="{$key}[order]" type="text" size="3" value="{$order}"{$disabled} /></td>'."\n".
		'        <td><input name="{$key}[orig]" type="hidden" value="{$label}"{$disabled} /><input id="{$key}" name="{$key}[new]" type="text" size="50" value="{$label}"{$disabled} /></td>'."\n".
		'        <td><label><input name="{$key}[del]" type="checkbox" value="1"{$disabled} />{$groups.del.lbl}</label></td>'."\n".
		'       </tr>'."\n",
		);
		$vr=array('formact_url'=>'?'.axs_url($this->url, array('axs'=>array('form_id'=>$f, ))), );
		foreach (array('','_lbl') as $v) $vr['group_add'.$v]=($f===$this->post_form) ? axs_get('group_add'.$v, $_POST):'';
		$nr=$reorder=0;
		$vr['table']=$vr['group_add_after']='';
		foreach ($axs['cfg'][$f] as $k=>$v) {
			$nr++;
			$v=array('key'=>$k, 'order'=>$nr, 'label'=>axs_html_safe($v));
			$post=array('group_order'=>'', );
			foreach ($post as $kk=>$vv) $post[$kk]=axs_get($kk, $_POST);
			$v['disabled']=(isset($this->protected[$f][$k])) ? ' disabled="disabled"':'';
			$vr['table'].=axs_tpl_parse($tpl['row'], $v);
			$v['selected']=(($post['group_order']==$k) or (!$post['group_order'] && count($axs['cfg'][$f])<=$nr)) ? ' selected="selected"':'';
			if ($nr>=count($this->protected[$f])) $vr['group_add_after'].='<option value="'.$k.'" lang="en"'.$v['selected'].'>AFTER `'.$k.'`</option>';
			if (($f===$this->post_form) && (isset($_POST['save'])) && (!$v['disabled'])) {
				foreach (array('order'=>'', 'new'=>'', 'orig'=>'', ) as $kk=>$vv) $post[$k][$kk]=(isset($_POST[$k][$kk])) ? $_POST[$k][$kk]:'';
				if ($post[$k]['order']!=$nr) axs_config_edit::groups_order($k, intval($post[$k]['order']), $f);//$reorder=true;
				if (($post[$k]['new']) && ($post[$k]['new']!=$post[$k]['orig'])) {
					if (array_search($value, $axs['cfg'][$f])) $this->form->msg('', $this->e->tr('groups.'.$f.'_exists.msg'));
					else axs_config_edit::groups_edit($k, $post[$k]['new'].'', $f);
					}
				if (!empty($_POST[$k]['del'])) {
					if ($err=axs_config_edit::groups_del($k, $f)) $this->form->msg('', $err);
					}
				}
			}
		if ($f===$this->post_form) {
			if ((!empty($_POST['group_add'])) && (!empty($_POST['group_add_lbl']))) {
				$key=axs_valid::id($_POST['group_add']);
				$label=axs_html_safe($_POST['group_add_lbl']);
				if ($_POST['group_add']!==$key) $this->form->msg('', $this->e->tr('groups.key.msg'));
				if ((isset($axs['cfg'][$f][$key])) or (array_search($label, $axs['cfg'][$f]))) $this->form->msg('', $this->e->tr('groups.'.$f.'_exists.msg'));
				if (empty($this->form->msg)) axs_config_edit::groups_add($key, $label, axs_get('group_add_after', $_POST), $f);
				}
			if (empty($this->form->msg)) exit(axs_redir('?'.axs_url($this->url, array(), true)));
			}
		$vr['msg']=($f===$this->post_form) ? $this->form->msg_html():'';
		return axs_tpl_parse($tpl['table'], $vr+$this->e->tr->tr);
		} #</ui_groups()>
	} #</class::axs_edit_system>
#2013-07-14 ?>