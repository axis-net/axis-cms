<?php #2018-09-22
//$axs['page']['title']['s1']='SQL brauser/import/export';
$axs['page']['head']['index.content.css']='<link href="'.AXS_PATH_CMS_HTTP.'index.content.css" rel="stylesheet" type="text/css" media="screen" />'."\n";
$axs['page']['head']['edit-sql']='<style type="text/css">
	<!--
	a.act {
		color:#999999;
		font-weight:bold;
		}
	form.edit label {
		display:block;
		}
	#profiles {	float:right;	}
	#db {
		border:1px solid rgb(200,200,200);
		padding:5px 5px 0 5px;
		background:rgb(240,240,240);
		}
		#db h1 {
			margin:0;
			padding:0;
			font-size:12px;
			}
		#db ul li {	display:inline;	}
	#table {
		float:left;
		border:1px solid rgb(200,200,200);
		padding:5px;
		width:200px;
		background:rgb(240,240,240);
		}
		#table h2 {
			margin:0;
			font-size:12px;
			}
	#container {
		margin-left:220px;
		}
	.search_hilight {
		color:#FF0000;
		font-weight:bold;
		text-decoration:blink;
		}
	-->
</style>
<script>
<!--
function blinkIt(class_name) {
	var inputNodes = document.getElementsByTagName("span");
    var i;
    for (i = 0; i < inputNodes.length; i++) {
        var node = inputNodes[i];
        if (node.className!=class_name) continue;
		node.style.visibility=(node.style.visibility=="visible")?"hidden":"visible";
    	}
	}
// -->
</script>'."\n";
$export=htmlspecialchars(axs_get('export', $_GET));
//if ($export) axs_form_edit::progress_set('import'); #Clear progress indicator

if (defined('AXS_OUTPUT_START')) {
class axs_edit_sql {
	static function import_csv_header($line, $spr, $charset_to, $charset_from) {
		$line=str_getcsv($line, $spr);
		$h=array();
		foreach ($line as $k=>$v) {	$h[addslashes(trim(axs_valid::convert($v, $charset_to, $charset_from)))]=$k;	}
		return $h;
		} #</import_csv_header()>
	static function import_csv_line($line, $h, $spr, $charset_to, $charset_from) {
		$line=str_getcsv($line, $spr);
		$cl=array();
		foreach ($h as $k=>$v) {	$cl[$k]=axs_valid::convert($line[$v], $charset_to, $charset_from);	}
		return $cl;
		} #</import_csv_line()>
	} #</class:axs_edit_sql>

function search_hilight($search, &$str, $class_name='search_hilight') {
	$pattern='#(>[^<]*)('.quotemeta(str_replace('#', '\#', $search)).')#i';
	$replacement='\\1<mark class="'.$class_name.'">\\2</mark>';
	if ($search) $str=preg_replace($pattern, $replacement, $str);
	}
function progress($nr, $total, $msg='') {
	$msg=str_replace(array("\r","\n"), array('\n','\n'), addslashes($msg));
	return '<a id="nr'.$nr.'">'.$nr.'/'.$total.'</a>'."\n".
	'<script>'."\n".
	'	document.getElementById("progress").innerHTML="'.$nr.'/'.$total.' '.$msg.'";'."\n".
	'</script><br />'."\n";
	}
$cfg=$axs['cfg']['db'];
$dbnr=(isset($cfg[axs_get('db_nr', $_GET)])) ? $_GET['db_nr']+0:key($cfg);
$db_name=axs_get('db', $_GET);
//$link=axs_db_query(false, 0, $dbnr, __FILE__, __LINE__, 0);
$sql=new axs_sql_structure($dbnr);
$export=htmlspecialchars(axs_get('export', $_GET));
$convert=htmlspecialchars(axs_get('convert', $_GET));
$edit=axs_get('edit', $_GET, null);
$url=array_merge($this->url, array('db_nr'=>$dbnr, 'db'=>'', /*'export'=>$export,*/ ));
?>
<div id="db">
     <p id="profiles">Andmebaasiprofiilid:
<?php
$tmp=array();
foreach ($cfg as $k=>$v) {
	if ($k==$dbnr) {
		$act=' class="act"';
		$v='<strong>'.$k.'</strong>';
		}
	else {
		$act='';
		$v=$k;
		}
	$tmp[]='<a href="'.'?'.axs_url($url, array('db_nr'=>$k)).'"'.$act.'>'.$v.'</a>';
	}
echo implode(', ', $tmp);
?>
     </p>
<?php 
if (!$dbnr) {	echo '<p class="msg">SQL servereid ei ole seadistatud.</p>';	return;	}
axs_db_query('', '', $dbnr, __FILE__, __LINE__, array('error_handle'=>1));
?>
<h1>Andmebaasid profiilis: <?php echo $dbnr; ?></h1>
<?php
foreach ($axs['db'][$dbnr]->axs_error as $k=>$v) echo '      <pre>'.$v.'</pre>'."\n";
if ($dbnr) {
	$db_list=$sql->list_db();
	if (!empty($db_list)) {
		echo '     <ul>'."\n";
		foreach ($db_list as $k=>$v) {
			$lbl=($db_name==$v) ? '<strong>'.$v.'</strong>':$v;
			echo '      <li><a href="'.'?'.axs_url($url, array('db'=>$v)).'#table">'.$lbl.'</a></li>'."\n";
			}
		echo '     </ul>'."\n";
		}
	}
?>
</div>
<?php
if (!empty($db_list[$db_name])) {
	$db=$db_list[$db_name];
	$tbl=axs_get('table', $_GET);
	$url['db']=urlencode($db);
	echo '<div id="table"><h2>Tabelid andmebaasis "'.htmlspecialchars($db).'"</h2>
	<ol>'."\n";
	$tables=array();
	$result=axs_db_query('SHOW TABLES FROM `'.$db.'`', 1, $dbnr, __FILE__, __LINE__);
	foreach ($result as $k=>$v) {
		$v=current($v);
		$tables[$v]=$v;
		$act=($tbl==$v) ? 'act':'';
		echo ' <li><a href="'.'?'.axs_url($url, array('db'=>$db, 'table'=>$v)).'#table" class="'.$act.'">'.$v.'</a></li>'."\n";
		}
	echo ' </ol>
	</div>
	<div id="container">'."\n";
	
	$table=(isset($tables[$tbl])) ? $tables[$tbl]:false;
	//if ($table==axs_user::$table) {	if (!axs_user::permission_get(array(), 'adm')) $table=false;	}
	
	if ($table) {
		$url['table']=urlencode($table);
		axs_db_query("USE `".$db."`", '', $dbnr, __FILE__, __LINE__, array('error_handle'=>1));
		$result=axs_db_query('SHOW COLUMNS FROM `'.$table.'`', 1, $dbnr, __FILE__, __LINE__);
		$cols=array();
		foreach ($result as $cl) $cols[$cl['Field']]='`'.$cl['Field'].'`';
		if ($table===axs_user::$table) unset($cols['pass']);
		$total=axs_db_query('SELECT COUNT(*) FROM `'.$table.'`', 'cell', $dbnr, __FILE__, __LINE__);
		echo '<h3>Tabel <a href="'.'?'.axs_url($url, array('db'=>$db, )).'#table">"'.$table.'"</a></h3>'."\n";
		$nav=array('t'=>'vaata sisu', 'export'=>'import/eksport', 'convert'=>'convert charset', );
		$current=key($nav);
		foreach ($nav as $k=>$v) if (isset($_GET[$k])) $current=$k;
		foreach ($nav as $k=>$v) {
			if ($k===$current) $v='<strong>'.$v.'</strong>';
			$tmp=($k) ?  array($k=>1):array();
			$nav[$k]='<a href="'.'?'.axs_url($url, $tmp).'#table">'.$v.'</a>';
			}
		echo '<nav>'.implode(' ', $nav).'</nav><br />'."\n";
		if ($edit!==null) {
			if (!empty($_POST['save'])) {
				$cl=array();
				foreach ($cols as $k=>$v) $cl[$k]=$k.'=\''.addslashes($_POST[$k]).'\'';
				if ($edit) $result=axs_db_query("UPDATE `".$table."` SET ".implode(', ', $cl)." WHERE `id`='".$edit."'", '', $dbnr, __FILE__, __LINE__);
				else {
					unset($cl['id']);
					$result=axs_db_query("INSERT INTO `".$table."` SET ".implode(', ', $cl), '', $dbnr, __FILE__, __LINE__);
					}
				}
			if (!empty($_POST['del'])) {
				$result=axs_db_query('DELETE FROM `'.$table.'` WHERE id=\''.$edit.'\'', '', $dbnr, __FILE__, __LINE__);
				}
			}
			
		if ($export) {
			$start=(!empty($_POST['start'])) ? $_POST['start']+0:0;
			$stop=(!empty($_POST['stop'])) ? $_POST['stop']+0:$total;
			$d=axs_dir('cfg');
			$f=$d.$db.'.'.$table.'_'.$start.'-'.$stop.'.sql';
			if (!file_exists($f)) $f=strtolower($f);
			if (!file_exists($f)) $f='./'.$db.'/'.$table.'.sql';
			if (!file_exists($f)) $f=strtolower($f);
			if (!file_exists($f)) $f='';
			if (!empty($_POST['f'])) $f=$_POST['f'];
			$charset=axs_get('charset', $_POST, 'utf-8');
			$charsets='';
			foreach (mb_list_encodings() as $k=>$v) {
				$k=strtolower($v);
				$s=($k===$charset) ? ' selected="selected"':'';
				$charsets.='<option value="'.htmlspecialchars($k).'"'.$s.'>'.htmlspecialchars($v).'</option>';
				}
			$spr=axs_get('spr', $_POST['csv'], ',');
			echo '
			<form id="import-export" action="'.'?'.axs_url($url, array('export'=>1)).'#nr'.$stop.'" method="post" enctype="multipart/form-data">
			<input type="text" name="f" value="'.htmlspecialchars($f).'" size="40" placeholder="file name" /></label>
			<label>character set <select name="charset">'.$charsets.'</select>
			<filedset>
				<legend>rows</legend>
				<input type="number" name="start" value="'.$start.'" step="1" placeholder="from" /> - 
				<input type="number" name="stop" value="'.$stop.'" step="1" placeholder="to" /> 
			</fieldset>
			<filedset>
				<legend>import</legend>
				<label>CSV header <input type="checkbox" name="csv[hdr]" value="1" checked="checked" disabled="disabled" /></label>
				<label>CSV separator <input type="text" name="csv[spr]" value="'.htmlspecialchars($spr).'" size="1" /></label>
				<input type="submit" name="import" value="import" />
			</fieldset>
			<filedset>
				<legend>export</legend>
				<input type="submit" name="export" value="export" />
				<input type="submit" name="export_append" value="export+append" />
			</fieldset>
			<progress class="progress hidden" value="0" max="100"></progress> <output class="progress hidden"></output>'."\n".
			'		<script src="'.axs_dir('lib', 'http').'axs.form.js"></script>'."\n".
			'		<script>'."\n".
			'			document.querySelector(\'#import-export input[name="import"]\').addEventListener("click",function(){'."\n".
			'				axs.class_rem(document.querySelectorAll("#import-export .progress"),"hidden");'."\n".
			'				axs.form.progressGet("import",".progress");'."\n".
			'				})'."\n".
			'		</script>'."\n".
			'</form>'."\n";
			
			if (!empty($_POST['export']) or !empty($_POST['export_append'])) {
				#if (!is_dir($d=axs_dir('cfg').$db)) mkdir($d, 0777);
				$f=$d.$db.'.'.$table.(($start>0 or $stop<$total) ? '_'.$start.'-'.$stop:'').'.sql';
				if (!empty($_POST['export_append'])) {
					$f=$d.$db.'.sql';
					if (!file_exists($f)) fopen($f, 'w+');
					$fp2=fopen($f, 'a+');
					}
				$fp=fopen($f, 'w+');
				if (!empty($_POST['export'])) {
					$cl=axs_db_query("SHOW CREATE TABLE `".$table."`", 'row', $dbnr, __FILE__, __LINE__);
					fwrite($fp, str_replace(array('CREATE TABLE ', "\r", "\n"), array('CREATE TABLE IF NOT EXISTS ', '', ''	), $cl['Create Table']).";\r\n");
					}
				$begin='INSERT INTO `'.$table.'` VALUES ';
				if (!empty($_POST['export_append'])) fwrite($fp2, "\r\n".$begin);
				for ($start; $start<$stop; $start++) {
					ini_set('max_execution_time', (ini_get('max_execution_time')+2));
					$cl=axs_db_query('SELECT * FROM `'.$table.'` LIMIT '.$start.',1', 'row', $dbnr, __FILE__, __LINE__);
					if (!empty($cl)) {
						foreach ($cl as $k=>$v) $cl[$k]='\''.str_replace(array("\r","\n"), array('\r','\n'), addslashes($v)).'\'';
						$data=$begin.'('.implode(', ', $cl).');'."\r\n";
						}
					else {
						$data='-- no data';
						}
					fwrite($fp, $data);
					if (!empty($_POST['export_append'])) fwrite($fp2, $data);
					$nr=$start+1;
					echo progress($nr, $total);
					}
				$data=($total) ? '-- END `'.$table.'` loop':'-- empty table';
				fwrite($fp, $data);
				if (!empty($_POST['export_append'])) fwrite($fp2, $data);
				echo '<a href="#table"><strong>END</strong> &uarr;&nbsp;&uarr;</a>'."\n";
				}
			if (!empty($_POST['import'])) {
				if (!file_exists($f=$d.axs_valid::f_name($_POST['f']))) echo 'Faili ei leitud "'.$f.'"';
				else {
					if ($fp=fopen($f, 'r')) {
						$format=axs_valid::f_ext($f);
						echo $f.'<br />'."\n";
						$nr=0;
						$header=$cols=array();
						while (!feof($fp)) {
							ini_set('max_execution_time', (ini_get('max_execution_time')+2));
							if (!$line=fgets($fp)) continue;
							if (($format==='csv') && (!$header)) {
								$header=axs_edit_sql::import_csv_header($line, $spr[0], $axs['cfg']['charset'], $charset);
								continue;
								}
							$nr++;
							if (($start=axs_get('start', $_POST)) && ($nr<$start)) continue;
							if ($format==='csv') {
								if (!$cols) $cols='"'.implode('","', array_keys($header)).'"';
								$line=axs_edit_sql::import_csv_line($line, $header, $spr[0], $axs['cfg']['charset'], $charset);
								foreach ($line as $k=>$v) {	$line[$k]='\''.addslashes($v).'\'';	}
								$line='INSERT INTO "'.$table.'" ('.$cols.') VALUES ('.implode(',', $line).');';
								}
							$inserted=axs_db_query($line, 'affected_rows', $dbnr, __FILE__, __LINE__, array('error_handle'=>1));
							if ($error=axs_db_query('', 'error', $dbnr, __FILE__, __LINE__, array('error_handle'=>1))) {
								$msg='<pre>'.$error.' ('.$line.')</pre>'."\n";
								echo progress($nr, '', $msg);
								echo $msg;
								break;
								}
							if (($stop=axs_get('stop', $_POST)) && ($nr>=$stop)) break;
							if (!$inserted) continue;
							//echo progress($nr, '', $msg);
							//axs_form_edit::progress_set('import', $nr, 0, $nr.'/?');
							}
						//echo progress($nr, '', '<a href="#table">END &uarr;&nbsp;&uarr;</a>'."\n");
						//axs_form_edit::progress_set('import', $nr, $nr, $nr.'/'.$nr);
						fclose($fp);
						}
					}
				}
			}
		elseif ($convert) { #<Convert>
			$input=array();
			foreach (array('from'=>'latin1', 'to'=>'utf8', ) as $k=>$v) $input[$k]=axs_get($k, $_POST, $v);
			if (isset($_POST['convert'])) $sql->convert($input['from'], $input['to'], $table);
			$msg='';
			if (!empty($sql->msg)) $msg='			<ul class="msg"><li>'.implode('</li><li>', $sql->msg).'</li></ul>'."\n";
			echo 
			'	<form method="post" action="'.'?'.axs_url($url, array('convert'=>1, )).'#table">'."\n".
			'		<fieldset>'."\n".
			'			<legend>convert charset</legend>'."\n".
			$msg.
			'			<label>from <input type="text" name="from" value="'.htmlspecialchars($input['from']).'" list="charsets" /></label>'."\n".
			'			<label>to <input type="text" name="to" value="'.htmlspecialchars($input['to']).'" list="charsets" /></label>'."\n".
			'			<datalist id="charsets">'."\n".
			'				<option value="latin1" />'."\n".
			'				<option value="utf8mb4" />'."\n".
			'				<option value="utf8" />'."\n".
			'			</datalist>'."\n".
			'			<input type="submit" name="convert" value="convert" />'."\n".
			'		</fieldset>'."\n".
			'	</form>'."\n";
			} #</Convert>
		else {
			echo '<form method="get" action="#table">'."\n";
			foreach ($url as $k=>$v) echo '      <input type="hidden" name="'.$k.'" value="'.urldecode($v).'" />'."\n";
			if (isset($cols['id'])) echo '<label>ID#<input type="text" name="id" value="'.htmlspecialchars(axs_get('id', $_GET)).'" size="3" /></label>'."\n";
			echo '<label>fraas: <input type="text" name="txt" value="'.htmlspecialchars(axs_get('txt', $_GET)).'" size="50" /></label>
			<select name="ps">'."\n";
			foreach (array(10,25,50,100,250,500,1000) as $v) {
				$selctd=($_GET['ps']==$v) ? ' selected="selected"':'';
				echo ' <option value="'.$v.'"'.$selctd.'>'.$v.'</option>'."\n";
				}
			echo '<input type="submit" name="s" value="otsi" />'."\n";
			
			$where=array();
			$s='';
			if (!empty($_GET['txt'])) {
				$s=addslashes($_GET['txt']);
				$url['txt']=urlencode($_GET['txt']);
				foreach ($cols as $k=>$v) $where[]='`'.$k.'` LIKE \'%'.$s.'%\'';
				}
			if (isset($cols['id'])) {
				if (!empty($_GET['id'])) {
					$url['id']=urlencode($_GET['id']);
					$where[]='id=\''.($_GET['id']+0).'\'';
					}
				}
			$where=(!empty($where)) ? ' WHERE '.implode(' OR ', $where):'';
			$pager=new axs_pager(axs_get('p', $_GET), array(25,50,100), 50);
			$p=$pager->p;	$start=$pager->start;	$limit=$pager->psize;	//$dsc=$pager->desc;
			$result=axs_db_query('SELECT SQL_CALC_FOUND_ROWS * FROM `'.$table.'` '.$where.' LIMIT '.$start.','.$limit, 1, $dbnr, __FILE__, __LINE__);
			$found=axs_db_query('', 'found_rows', $dbnr, __FILE__, __LINE__);
			$data=array();
			if ($found>$limit) {
				$data['pages']=$pager->pages($found, '?'.axs_url($url, array('txt'=>$s, 'p'=>'', )));
				$data['prev']=$data['pages']['prev'];
				$data['next']=$data['pages']['next'];
				$data['pages']=$data['pages']['pages'];
				}
			else $data['prev']=$data['pages']=$data['next']='';
			if ($where) echo '(leitud <strong>'.$found.'</strong>)';
			echo '			</form>';
			echo $data['prev'].$data['next'].$data['pages']."\n";
			echo '<table border="1">
			 <caption><a href="'.'?'.axs_url($url, array('db'=>$db, )).'#table">"'.$table.'"</a> ('.$total.' kirjet) <a href="'.'?'.axs_url($url, array('edit'=>'', 'b'=>urlencode('?'.$_SERVER['QUERY_STRING'].'#table'))).'#table" title="add new record" lang="en">+</a></caption>
			 <thead>
			  <tr valign="top">
			  ';
			foreach ($cols as $k=>$v) echo '<th scope="col">'.$k.'</th>';
			echo "\n".'</tr>
			</thead>'."\n";
			$table='';
			$nr=0;
			$result=array_merge(array(array()), $result);
			foreach ($result as $cl) {
				$nr++;
				$table.=' <tr id="row'.$nr.'" valign="top">'."\n";
				if ((($nr==1) && ($edit==='')) or ((isset($cl['id']) && $edit==$cl['id']))) {
					$b=($_GET['b']) ? htmlspecialchars($_GET['b']):'?'.axs_url($url);
					$table.='<td colspan="'.count($cl).'">
					<form action="'.'?'.axs_url($url, array('edit'=>axs_get('id', $cl, ''))).'#row'.$nr.'" method="post" enctype="multipart/form-data" class="edit">
					<a href="'.$b.'">tagasi</a>'."\n";
					foreach ($cols as $k=>$v) $table.='<label>'.$k.'<br />
					<textarea name="'.$k.'" cols="45" rows="5">'.htmlspecialchars(axs_get($k, $cl)).'</textarea>
					</label>'."\n";
					$table.='<input type="submit" name="save" value="salvesta" />
					<input type="submit" name="del" value="kustuta kirje" />
					</form>
					</td>'."\n";
					}
				if ($cl) {
					$row='';
					if (isset($cl['id'])) $cl['id']='<a href="'.'?'.axs_url($url, array('edit'=>$cl['id'], 'b'=>'?'.$_SERVER['QUERY_STRING'].'#row'.$cl['id'])).'#row'.$nr.'">'.$cl['id'].'</a>';
					foreach ($cols as $k=>$v) $row.='<td>'.$cl[$k].'</td>'."\n";
					search_hilight($s, $row);
					$table.=$row;
					}
				$table.=' </tr>'."\n";
				}
			echo $table.
			'</table>'."\n";
			echo $data['prev'].$data['next'].$data['pages']."\n";
			}
		}
	echo '</div>'."\n";
	}
}
else return '';
#2008-12-17 ?>