<?php #2017-09-05
defined('AXS_PATH_CMS') or exit(require('log.php')); #<Prevent direct access />
if ($tmp=axs_admin::no_permission(array(), false, __FILE__, __LINE__)) return $tmp; #<Permission check />

$this->tr_load(axs_tr::$lang, preg_replace('/\.php$/', '', basename(__FILE__)));

$axs['page']['title']=array($this->tr('feedbackto_txt'), );
$axs['page']['head']['css']='<style type="text/css">
	<!--
	body {
		margin:0;
		padding:0;
		width:auto;
		min-width:100%;
		}
	#feedback_form .msg {	color:#f00;	}
	#feedback_form #feedback {
		box-sizing:border-box;
		width:100%;
		height:350px;
		}
	#feedback_form #send {
		float:left;
		margin:0 5px 0 0;
		}
	#feedback_form #contact {
		margin:0 0 5px 0;
		}
	ul.msg {
		margin:50px 25px 5px 25px;
		padding-bottom:5px;
		}
	p.back {	margin:5px 25px;	}
	-->
</style>'."\n";

$feedback=axs_get('feedback', $_POST);
$disable_edit=$disable_send='';
if (!$axs['mail()']) {
	$disable_edit=' readonly="readonly"';
	$disable_send=' disabled="disabled"';
	}
$mail_disabled_txt=($disable_send) ? '	   <p class="msg">'.$this->tr('mail_disabled').'</p>'."\n":'';
$msg_mail=axs_user::get('@');
if (!$msg_mail) $msg_mail=$this->tr('email_empty');
$msg="\n"."\n".$axs_user['fstname'].' '.$axs_user['lstname'].' &lt;'.$msg_mail.'&gt; ('.$axs_user['user'].')
 '.($footerlennght=str_pad(' '.$this->tr('bginfo').' ', 40, '-', STR_PAD_BOTH)).'
'.htmlspecialchars($feedback).'
 '.str_repeat('-', strlen($footerlennght));
$user_agent='';
foreach (array('screen_size'=>'?', 'window_size'=>'?', 'pixel_ratio'=>'?', ) as $k=>$v) {
	if (isset($_REQUEST['axs'][$k])) $v=htmlspecialchars($_REQUEST['axs'][$k]);
	$user_agent.='	   <input name="axs['.$k.']" type="hidden" value="'.$v.'" />'."\n";
	}
$mail_addr=array();
foreach ($axs['cfg']['emails'] as $v) $mail_addr[]='<a href="mailto:'.$v.'?subject='.urlencode($_SERVER['SERVER_NAME'].$axs['http_root'].' '.$this->tr('feedbackto_txt')).'&amp;body='.urlencode($msg).'">'.$v.'</a>';
$mail_addr=implode(', ', $mail_addr);

if (!empty($_POST['send'])) {
	unset($_POST['feedback'], $_POST['send']);
	axs_log(__FILE__, __LINE__, 'feedback', $feedback);
	return 
	' <ul class="msg"><li>'.$this->tr('msg_sent').'</li></ul>'."\n".
	' <p class="back"><a href="?'.htmlspecialchars($_SERVER['QUERY_STRING']).'" onclick="window.close();" />'.$this->tr('close').'</a></p>'."\n";
	}
$data='     <form id="feedback_form" method="post" action="?'.htmlspecialchars($_SERVER['QUERY_STRING']).'">
'.$mail_disabled_txt.
'	   <label for="feedback">'.$this->tr('feedback_lbl').'</label><br />
	   <textarea id="feedback" name="feedback"'.$disable_edit.'>'.$msg.'</textarea><br />'.
$user_agent.'
       <input id="send" name="send" type="submit" value="'.$this->tr('send').'"'.$disable_send.' />
       <p id="contact">'.$mail_addr.' '.$axs['cfg']['admin_contact'].'</p>
    </form>'."\n";
return $data;
#2006-02 ?>