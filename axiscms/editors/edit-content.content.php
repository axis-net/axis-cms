<?php #2022-05-10
defined('AXS_PATH_CMS') or exit(require('log.php')); # <Prevent direct access />

$this->url['s2']=(isset($_GET['s2'])) ? $_GET['s2']:'';
if (axs_get('add', $_GET)) $this->url['s2']='m';
$this->url['c']=$axs['c'];
$this->url['l']=$axs['l'];
$this->url['d']=$this->content->d;

foreach ($axs_content['content'][$axs['c']]['path'] as $k=>$v) $this->ui['menu_path'][$k]=$v['title'];
$tmp=array();
foreach (array('c','l') as $v) if ($axs[$v]) $tmp[]=$v.'='.urlencode($axs[$v]);
if (!empty($tmp)) $this->ui['site.preview.url'].='?'.implode('&', $tmp);

$this->vr=array('lang.lbl'=>$this->tr('content-menu_lang_lbl'), 'lang.current'=>htmlspecialchars($this->content->site['langs'][$axs['l']]['l']), 'lang.list'=>'', );
foreach ($this->content->site['langs'] as $k=>$v) {
	$v['class']=array();
	$v['l']=axs_html_safe($v['l']);
	if ($v['h']) {	$v['class'][]='_disabled';	$v['l']='<del>'.$v['l'].'</del>';	}
	if ($k===$axs['l']) { $v['class'][]='current';	$v['l']='<strong>'.$v['l'].'</strong>';	}
	$this->vr['lang.list'].='<li class="'.implode(' ', $v['class']).'"><a href="'.'?'.axs_url($this->url, array('l'=>$k)).'" lang="'.$k.'">'.$v['l'].'</a></li>';
	}

	$this->vr['menu']=$this->content->ui_menu_build($axs_content['index'], 1, $this->content->dir_user, $this->url, $this->tr);
$tabs=array('m'=>'page_settings', ''=>'content', 'tr'=>'tr', 'p'=>'posts', 'f'=>'files', 'ie'=>'import-export', 'r'=>'search_replace', );
foreach ($tabs as $k=>$v) $tabs[$k]=array(
	'url'=>'?'.axs_url($this->url, array('s2'=>$k)),
	'label'=>$this->tr('content-tabs_'.$v.'_lbl'),
	'act'=>$this->url['s2'],
	);
if (($axs_content['plugin']==='menu') or ($axs_content['plugin']==='menu-page')) $this->content->dir_filemanager=$this->content->d.$axs['c'].'/';
else {
	$this->content->dir_filemanager=($this->content->dir_page_files!=$this->content->dir_page) ? $this->content->d.$axs['c'].'_files/':$this->content->d;
	}
$tabs['f']['url']='?'.axs_url($this->url, array('s2'=>'f', 'dir'=>$this->content->dir_filemanager));
if (($axs_content['plugin']==='menu') or ($axs_content['plugin']==='menu-page') or (!$axs['c'])) foreach (array('','tr','p','ie',) as $v)  {
	$tabs[$v]['url']=false;
	$tabs[$v]['attribs']['class']='_disabled';
	}
$this->vr['tabs']=$this->menu_build($tabs, '      ', array('class'=>'menu'));
$this->vr['content']='';

if (isset($_GET['c'])) {
if (isset($_GET[$this->content->key_ajax])) $this->content->ui_ajax();
$this->vr['text']=$this->vr['form']='';
#<Permission check for content>
$role=($this->content->save) ? 'w':'r';
if ($tmp=$this->no_permission($this->content->restrict, $role)) {
	$this->vr['content']=$tmp;
	$this->content->save=false;
	} #</Permission check for content>
else switch ($this->url['s2']) {
	case 'm':
		if ($this->content->save) $this->content->save_menu();
		$this->form->css_js(false, true);
		$this->tpl_set('m', '     <form id="page-settings" class="form" action="{$formact}" method="post" enctype="multipart/form-data">'."\n".
		'       <input name="b" type="hidden" value="{$b}" />'."\n".
		'       <input name="b_del" type="hidden" value="{$b_del}" />'."\n".
		'       <a id="menu_add_link" href="#menu_add">- {$menu_add_lbl} -</a>'."\n".
		'       <a id="del_link" href="#del">- {$del_lbl} -</a>'."\n".
		'       <span class="clear"></span>'."\n".
		'{$msg}'.
		'       <div class="left">'."\n".
		'        <label for="title">{$title_lbl}<em class="input_required"><abbr title="{$input_required_lbl}">*</abbr></em></label>'."\n".
		'        <input id="title" name="title" value="{$title}" type="text" size="40" maxlength="255" />'."\n".
		'       </div>'."\n".
		'       <div class="right">'."\n".
		'        <label for="c">{$c_lbl}</label>'."\n".
		'        <input id="c" name="c"  value="{$c}" type="text" size="10" maxlength="{$c_len}"{$c_locked} />'."\n".
		'       </div>'."\n".
		'       <div class="left">'."\n".
		'        <label for="url">{$url_lbl}</label>'."\n".
		'        <input id="url" name="url" value="{$url}" type="text" size="40" maxlength="255" />'."\n".
		'       </div>'."\n".
		'       <div class="right">'."\n".
		'        <label for="target">{$target_lbl}</label>'."\n".
		'        <select id="target" name="target">{$target}</select>'."\n".
		'       </div>'."\n".
		'       <div id="dir_container" class="left element text-join">'."\n".
		'        <label for="dir">{$dir_lbl}</label>'."\n".
		'        <input name="dir" type="text" id="dir" value="{$dir}" size="2" {$dir_locked} />'."\n".
		'        <a id="dir.browse_link" class="browse" href="{$dir_browse_url}" target="_blank"><span>{$dir_browse_lbl}</span></a>'."\n".
		'       </div>'."\n".
		'       <div class="right">'."\n".
		'        <label for="menu_add">{$menu_add_lbl}</label>'."\n".
		'        <input id="menu_add" name="menu_add" value="1" type="checkbox"{$menu_add_locked} />'."\n".
		'       </div>'."\n".
		'       <div class="left">'."\n".
		'        <label for="nr">{$nr_lbl}</label>'."\n".
		'        <input name="nr" type="number" id="nr" value="{$nr}" min="1" step="1" />'."\n".
		'       </div>'."\n".
		'       <div class="right">'."\n".
		'        <label for="mkdir">{$mkdir_lbl}</label>'."\n".
		'        <input id="mkdir" name="mkdir" value="1" type="checkbox"{$mkdir}{$mkdir_locked} />'."\n".
		'       </div>'."\n".
		'       <div class="left">'."\n".
		'        <label for="plugin">{$plugin_lbl}</label>'."\n".
		'        <select id="plugin" name="plugin"{$plugin_locked}>{$plugin}</select>'."\n".
		'       </div>'."\n".
		'       <div class="left">'."\n".
		'        <label for="style">{$style_lbl}</label>'."\n".
		'        <select id="style" name="style">{$style}</select>'."\n".
		'       </div>'."\n".
		'       <span class="clear"></span>'."\n".
		'       <fieldset id="publ" class="timestamp"><legend class="clickable">{$publ_lbl}</legend>'."\n".
		'        <label for="publ_date" class="visuallyhidden">yyyy-mm-dd</label>'."\n".
		'        <input id="publ_date" name="publ[date]" class="datepicker" placeholder="yyyy-mm-dd" type="date" value="{$publ_date}"{$time_locked} />'."\n".
		'        <label for="publ_h" class="visuallyhidden">H</label>'."\n".
		'        <input id="publ_h" name="publ[h]" title="H" type="number" value="{$publ_h}" min="0" max="24"{$time_locked} />:'."\n".
		'        <label for="publ_i" class="visuallyhidden">i</label>'."\n".
		'        <input id="publ_i" name="publ[i]" title="i" type="number" value="{$publ_i}" min="0" max="59"{$time_locked} />&nbsp;&nbsp;'."\n".
		'       </fieldset>'."\n".
		'       <fieldset id="hidden" class="timestamp"><legend class="clickable">{$hide_lbl}</legend>'."\n".
		'        <label for="hidden_date" class="visuallyhidden">yyyy-mm-dd</label>'."\n".
		'        <input id="hidden_date" name="hidden[date]" class="datepicker" placeholder="yyyy-mm-dd" type="date" value="{$hidden_date}"{$time_locked} />'."\n".
		'        <label for="hidden_h" class="visuallyhidden">H</label>'."\n".
		'        <input id="hidden_h" name="hidden[h]" title="H" type="number" value="{$hidden_h}" min="0" max="59"{$time_locked} />:'."\n".
		'        <label for="hidden_i" class="visuallyhidden">i</label>'."\n".
		'        <input id="hidden_i" name="hidden[i]" title="i" type="number" value="{$hidden_i}" min="0" max="59"{$time_locked} />'."\n".
		'       </fieldset>'."\n".
		'       <a id="lock" href="#publ_date">{$lock_lbl}</a>'."\n".
		'       <fieldset id="seo"><legend lang="en"><abbr title="Search Engine Optimization">SEO</abbr></legend>'."\n".
		'       <div class="left">'."\n".
		'        <label for="title_bar">{$title_bar_lbl}</label>'."\n".
		'        <input id="title_bar" name="title_bar" value="{$title_bar}" type="text" size="75" maxlength="200" />'."\n".
		'       </div>'."\n".
		'       <div class="left">'."\n".
		'        <label for="description">{$description_lbl} <span class="comment" lang="en">&lt;meta description&gt;</span></label>'."\n".
		'        <input id="description" name="description" value="{$description}" type="text" size="75" maxlength="200" />'."\n".
		'       </div>'."\n".
		'       </fieldset>'."\n".
		'       <fieldset id="menu-pic"><legend>{$menu-pic_lbl}</legend>'."\n".
		'        <p>{$menu-pic_size}</p>'."\n".
		'        <input name="menu-pic_ext" value="{$menu-pic_ext}" type="hidden" />'."\n".
		'        '."\n".
		'        <label>{$menu-pic_lbl}1 {$menu-pic}<input id="f1" name="f1" type="file"{$fup_disabled} /></label>'."\n".
		'        <label>{$menu-pic_lbl}2 {$menu-pic_act}<input id="f2" name="f2" type="file"{$fup_disabled} /></label>'."\n".
		'        <label><input name="menu-pic_del" type="checkbox" value="1" />{$menu-pic_del_lbl}</label>'."\n".
		'       </fieldset>'."\n".
		'       <fieldset id="restrict"><legend>{$restrict_lbl}</legend>'."\n".
		'        <label for="pw">{$restrict_pass_lbl}</label>'."\n".
		'        <input id="pw" name="pw" type="text" value="{$pw}" size="20" maxlength="10" />'."\n".
		'        <table>'."\n".
		'         <caption>{$restrict_group_lbl}</caption>'."\n".
		'         <thead>'."\n".
		'          <tr><th>{$restrict_group_th_lbl}</th>{$roles}</tr>'."\n".
		'         </thead>'."\n".
		'         <tbody>'."\n".
		'{$groups}'.
		'         </tbody>'."\n".
		'        </table>'."\n".
		'       </fieldset>'."\n".
		'       <div id="bottom">'."\n".
		'        <input type="submit" name="save" value="{$save_lbl}" />'."\n".
		'        <input type="hidden" name="axs_save" value="{$save_lbl}" />'."\n".
		'        <label for="del">{$m-del_confirm_lbl}</label>'."\n".
		'        <select id="del" name="del"{$del_act}>'."\n".
		'         <option value="">-</option>'."\n".
		'         <option value="r"{$del_recycle_disabled}>{$del_recycle_lbl}</option>'."\n".
		'         <option value="p">{$del_permanently_lbl}</option>'."\n".
		'        </select>'."\n".
		'        <input id="del_confirm" name="del_confirm" type="submit" value="{$del_lbl}"{$del_act} /><br />'."\n".
		'        <span id="updated">{$updated_lbl}: <span>{$updated_user}</span> {$updated_time}</span>'."\n".
		'       </div>'."\n".
		'       <span class="clear"></span>'."\n".
		'      </form>'."\n".
		'      <script>axs.form.init("page-settings");</script>');
		$this->tpl_set('m.group', '          <tr id="restrict_{$k}"><th>{$k} ({$v})</th>{$roles}</tr>'."\n");
		$cl=array();
		foreach (array(
			'input_required', 'menu_add', 'del', 'del_recycle', 'del_permanently', 'title', 'c', 'url', 'target',
			'description', 'plugin', 'style', 'title_bar', 'publ', 'hide', 'lock', 'dir', 'nr', 'mkdir',
			'restrict', 'restrict_pass', 'restrict_group', 'restrict_group_th', 'restrict_login_th', 'menu-pic', 'menu-pic_del', 'save', 'updated',
			) as $v) $cl[$v.'_lbl']=$this->tr('m-'.$v.'_lbl');
		$cl['c_len']=axs_content::$c_length;
		$cl['save_lbl']=$this->tr('save.lbl');
		
		$frbd=(isset($_GET['add'])) ? $this->content->d:axs_get('dir', $axs_content['content'][$axs['c']]);
		$frbd=($this->content->content_status_deletable($frbd)) ? '':' disabled="disabled"';

		$vl=$axs_content['content'][$axs['c']];
		$fields=array('c','title','url','target','description','plugin','style','dir','nr','mkdir','title_bar',);
		foreach ($fields as $v) if (!isset($vl[$v])) $vl[$v]='';
		if (!$frbd && (isset($_GET['add']) or ($this->content->save))) {
			foreach ($fields as $v) $vl[$v]=axs_html_safe(axs_get($v, $_REQUEST));
			if (!$vl['dir']) $vl['dir']=axs_get('d', $_GET);
			if (!$vl['nr']) $vl['nr']=intval(axs_get('add', $_GET));
			}
		$cl['c_locked']=($frbd) ? ' disabled="disabled"':'';
		$cl['dir_browse_lbl']=$this->ui['menu_path'];
		unset($cl['dir_browse_lbl'][0]);
		array_pop($cl['dir_browse_lbl']);
		if (empty($cl['dir_browse_lbl'])) $cl['dir_browse_lbl'][]='&nbsp;/&nbsp;';
		$cl['dir_browse_lbl']=implode('&gt;', $cl['dir_browse_lbl']);
		$cl['dir']=$cd=($vl['dir']) ? $vl['dir']:$axs['c'];
		$cl['dir_browse_url']='?'.axs_url($this->url, array('cd'=>$cd, 'value'=>$cl['dir'], 'text'=>$cl['dir_browse_lbl'], 'browse'=>'dir', $this->content->key_ajax=>''));
		foreach ($fields as $v) $cl[$v]=axs_get($v, $vl);
		$cl['target']='';
		foreach (array(''=>'-', '_blank'=>'', 'popup'=>'', 'overlay'=>'', ) as $k=>$v) {
			$cl['target'].='<option value="'.$k.'"'.(($vl['target']===$k)?' selected="selected"':'').'>'.(($k)?$this->tr('m-target.'.$k.'.lbl'):$v).'</option>';
			}
		$cl['mkdir']=(axs_get('mkdir', $vl)) ? ' checked="checked"':'';
		$cl['plugin']='';
		if (($vl['plugin']==='menu') or ($vl['plugin']==='menu-page')) $this->content->plugins=array(
			'menu'=>array('name'=>$this->tr('m-plugin_submenu_lbl')),
			'menu-page'=>array('name'=>$this->tr('m-plugin_menu-page_lbl')),
			);
		else {
			$this->content->plugins_get();
			$this->content->plugins=array_merge(array(''=>array('name'=>'-')), $this->content->plugins);
			}
		foreach ($this->content->plugins as $k=>$v) {
			$v['selected']=($k==$vl['plugin']) ? ' selected="selected"':'';
			$cl['plugin'].='<option value="'.$k.'"'.$v['selected'].'>'.$v['name'].'</option>';
			}
		$cl['plugin_locked']=($axs['c'] /*or $vl['plugin']=='menu'*/) ? ' disabled="disabled"':'';
		if (axs_user::permission_get('dev')) $cl['plugin_locked']='';
		
		$cl['style']='';
		$this->content->styles_get();
		$this->content->styles=array_merge(array(''=>array('name'=>'-')), $this->content->styles);
		foreach ($this->content->styles as $k=>$v) {
			$v['selected']=($k===$vl['style']) ? ' selected="selected"':'';
			$cl['style'].='<option value="'.$k.'"'.$v['selected'].'>'.$v['name'].'</option>';
			}
		#$cl['style_locked']=($vl['plugin']=='menu') ? ' disabled="disabled"':'';
		
		$cl['dir_locked']=(!$axs['c'] or $frbd) ? ' disabled="disabled"':'';
		$cl['menu_add_locked']=((!$axs['c']) or ($vl['plugin']==='menu') or ($vl['plugin']=='menu-page') or ($frbd)) ? ' disabled="disabled"':'';
		$cl['mkdir_locked']='';
		if (($vl['plugin']==='menu') or ($vl['plugin']==='menu-page') or (is_dir($this->content->dir_page.$axs['c'].'_files'))) {
			$cl['mkdir']=' checked="checked"';
			$cl['mkdir_locked']=' disabled="disabled"';
			}
		$cl['time_locked']=($frbd) ? ' disabled="disabled"':'';
		foreach (array('date'=>'Y-m-d','h'=>'H','i'=>'i') as $k=>$v) {
			if ($this->content->save) {
				$cl['publ_'.$k]=$_POST['publ'][$k];
				$cl['hidden_'.$k]=$_POST['hidden'][$k];
				}
			else {
				$cl['publ_'.$k]=(!empty($vl['publ'])) ? date($v, $vl['publ']):'';
				$cl['hidden_'.$k]=(!empty($vl['hidden'])) ? date($v, $vl['hidden']):'';
				}
			}
		
		if (isset($_POST['axs_save'])) $vl['restrict']=axs_get('restrict', $_POST, array());
		$cl['pw']=axs_html_safe((isset($_POST['axs_save'])) ? $_POST['pw']:axs_get('pw', $vl));
		$cl['roles']='';
		foreach ($axs['cfg']['roles'] as $k=>$v) $cl['roles'].='<th scope="col">'.axs_html_safe($v).'</th>';
		$cl['groups']='';
		$tmp=$axs_content['content'][$axs['c']]['path'];
		array_pop($tmp);
		//foreach ($tmp as $k=>$v) foreach ($v['restrict'] as $kk=>$vv) foreach ($vv as $kkk=>$vvv) $restrict_prev[$kk][$kkk]=$vvv;
		$restrict_prev=axs_user::permission_compact($tmp, 'restrict');
		foreach (array('-'=>$this->tr('m-restrict_login_th_lbl'))+$axs['cfg']['groups'] as $k=>$v) {
			$v=array('label'=>axs_html_safe($v), 'roles'=>'', );
			foreach ($axs['cfg']['roles'] as $kk=>$vv) {
				$vv=array('label'=>axs_html_safe($vv.': '.$v['label']), 'checked'=>'', 'disabled'=>'', );
				#<Can't set "-" if the current role has restrictions in parent node(s) />
				if ($k==='-') foreach ($restrict_prev as $kkk=>$vvv) if (!empty($vvv[$kk])) $vv['disabled']=' disabled="disabled"';
				else {
					if (isset($restrict_prev['-'])) {
						if ((empty($restrict_prev['-'])) or (isset($restrict_prev['-'][$kk]))) $vv['disabled']=' disabled="disabled"';
						}
					}
				#<Show that this restriction is inherited from parent node(s) />
				if (isset($restrict_prev[$k][$kk])) {
					$vv['disabled']=' disabled="disabled"';
					$vv['checked']=' checked="checked"';
					}
				#<Current node's settings />
				if (isset($vl['restrict'][$k][$kk])) $vv['checked']=' checked="checked"';
				$v['roles'].='<td class="'.$kk.'" title="'.$vv['label'].'"><label><input id="restrict_'.$k.'-'.$kk.'" name="restrict['.$k.']['.$kk.']" type="checkbox" value="'.$kk.'" '.$vv['checked'].$vv['disabled'].' /><span class="visuallyhidden">'.$vv['label'].'</span></label></td>';
				}
			$cl['groups'].=axs_tpl_parse($this->templates['m.group'], array('k'=>$k, 'v'=>$v['label'], 'roles'=>$v['roles'], ));
			}
		$cl['menu-pic']=$cl['menu-pic_act']=$w=$h='';
		$ext=$axs_content['menu-pic_ext'][$axs['c']];
		# <menu default backgroud picture properties />
		if ($ext) {
			if (axs_function_ok('getimagesize')) list($w, $h)=getimagesize($this->content->dir_content.$axs_content['content'][$axs['c']]['path'][1]['c'].'_'.$axs['l'].'.'.$ext);
			}
		$cl['menu-pic_size']=$ext.(($w && $h) ? ' '.$w.'&times;'.$h.'px':'');
		if ($axs['c']) foreach (axs_content::$menu_pic_ext as $v) foreach (array('','_act') as $vv) {
			if (file_exists($f=$this->content->dir_content.$vl['dir'].$axs['c'].'_'.$axs['l'].$vv.'.'.$v)) $cl['menu-pic'.$vv]=$f;
			}
		$cl['menu-pic_ext']='';
		foreach (array('menu-pic'=>$cl['menu-pic'], 'menu-pic_act'=>$cl['menu-pic_act']) as $k=>$v) {
			if ($v) {
				$s=$this->media_size($v, 100, 100);
				if ($s['w']<$tmp=20) $s['w']=$tmp;
				if ($s['h']<$tmp) $s['h']=$tmp;
				$cl[$k]='<a href="?'.urlencode('axs[gw]').'='.urlencode($v).'&amp;txt=" target="popup">
				<img src="'.$v.'" width="'.$s['w'].'" height="'.$s['h'].'" alt="'.basename($v).'" />
				</a>';
				}
			}
		$cl['fup_disabled']=(!$axs['file_uploads']) ? ' disabled="disabled"':'';
		
		$url_dir=(isset($axs_content['content'][$axs['c']]['dir'])) ? $axs_content['content'][$axs['c']]['dir']:$this->content->d;
		if (($vl['plugin']==='menu') or ($vl['plugin']==='menu-page')) $url_dir=$this->content->d;
		$cl['formact']='?'.axs_url($this->url, array('add'=>axs_get('add', $_GET), 'edit'=>$axs['c'], 'd'=>$url_dir, 'plugin'=>$vl['plugin'], ));
		$cl['b']=htmlspecialchars((axs_get('b', $_POST)) ? axs_get('b', $_POST):'?'.axs_url($this->url, array('c'=>$axs['c'], ), false));
		$cl['b_del']=htmlspecialchars('?'.axs_url($this->url, array('c'=>false, 's2'=>''), false));
		$cl['m-del_confirm_lbl']=(($vl['plugin']==='menu') or ($vl['plugin']==='menu-page')) ? $this->tr('m-del_confirm_menu_lbl'):$this->tr('m-del_confirm_page_lbl');
		$cl['del_act']=($frbd or !$axs['c']) ? ' disabled="disabled"':'';
		$cl['del_recycle_disabled']=$this->content->content_status_recycled($this->content->d_page) ? ' disabled="disabled"':'';
		$cl['updated_user']=axs_get('user', $axs_content['content'][$axs['c']]);
		$cl['updated_time']=($tmp=axs_get('updated', $axs_content['content'][$axs['c']])) ? date('d.m.Y H:i:s', $tmp):'';
		$cl['msg']=$this->form->msg_html();
		$this->form->msg=array();
		$this->vr['content'].=axs_tpl_parse($this->templates['m'], $cl);
		break;
	case 'tr':
		$f=$this->content->plugin_tr_get($axs_content['content'][$axs['c']]['plugin']);
		if (!$f) $f=$this->content->content_tr_get($axs['c']);
		include(AXS_PATH_CMS.'edit-tr.php');
		$f=new axs_tr_edit($this, $f);
		$this->vr['content'].=$f->ui();
		break;
	case 'f':
		require_once(axs_dir('editors').'edit-files.php');
		$f=new axs_filemanager($this->site_nr, $axs_user['home']['dir'], $this->content->dir, $this->url, axs_get('f', $_POST));
		$this->vr['content'].=$f->ui(false, $this->content);
		break;
	case 'p':
		if ($this->content->save) $this->content->save_posts();
		$this->tpl_set('posts', '     <form id="posts" action="{$formact}" method="post" class="pager">'."\n".
		'      <fieldset><legend>{$reorder_lbl}</legend>'."\n".
		'       <label><input name="reorder" type="text" size="1" maxlength="4" value="{$psize}" />{$reorder_pp}</label>'."\n".
		'       <input name="axs_save" type="submit" value="{$reorder_lbl}" />'."\n".
		'		<input name="b" type="hidden" value="{$b}" />'."\n".
		'      </fieldset>'."\n".
		'      <p class="pages">{$prev}{$current}{$next} {$pages}</p>'."\n".
		'     </form>'."\n".
		'{$post}'.
		'     <div class="pager">'."\n".
		'      <p class="pages">{$prev}{$current}{$next} {$pages}</p>'."\n".
		'     </div>');
		$psize=0;
		if (isset($axs_content['content'][$axs['c']]['tr']['save_posts'])) $psize=intval(trim($axs_content['content'][$axs['c']]['tr']['save_posts']));
		if ($psize<=0) $psize=20;
		list($total, $p, $start, $limit, $desc)=$this->content->posts_get($axs['c'], axs_get('p', $_GET), $psize);
		$pager=new axs_pager($p, $psize, $psize);
		$data=array(
			'formact'=>$data['b']='?'.axs_url($this->url, array('p'=>$p)),
			'total'=>$total,
			'psize'=>$psize,
			'reorder_lbl'=>$this->tr('content-posts_reorder_lbl'),
			'reorder_pp'=>$this->tr('content-posts_reorder_pp_lbl'),
			'b'=>'?'.axs_url($this->url, array('p'=>$p), false),
			);
		$data+=$pager->pages($total, '?'.axs_url($this->url, array('p'=>'')));
		$media=$this->media_settings($this->content->site_nr, array(), $axs['c']);
		$tab=new axs_tab_edit('', $this->content->site_nr, $this->content->dir_page_files, $axs['cfg']['cms_lang'], $media);
		$tab->msg=&$this->form->msg;
		$data['post']=$tab->ui($axs_content['content'][$axs['c']]['post'], $_GET, $_POST, $this->url+array('p'=>$p), $this->ui['site.preview.url'].'&p='.$p);
		$this->vr['content'].=axs_tpl_parse($this->templates['posts'], $data);
		break;
	case 'ie':
		$this->form->css_js();
		$form=array(
			''=>array(
				//'action'=>'?'.axs_url($this->url, array('write'=>'', )),
				'tr'=>$this->tr,
				),
			'b'=>array('type'=>'hidden', 'value'=>'?'.axs_url($this->url), ),
			'action'=>array('type'=>'select', 'options'=>array(
				'export'=>array('value'=>'export', ),
				'import'=>array('value'=>'import', ),
				), ),
			'position'=>array('type'=>'radio', 'value'=>'', 'comment'=>'',  'options'=>array(
				''=>array('value'=>'', ),
				'append'=>array('value'=>'append', ),
				'clear'=>array('value'=>'clear', ),
				), ),
			'f'=>array('type'=>'file', 'value'=>'', 'accept'=>'csv', ),
			'charset'=>array('type'=>'select', 'value'=>'utf-8', 'options'=>array(), ),
			'separator'=>array('type'=>'select', 'value'=>'', 'options'=>axs_form_importexport::$separator, ),
			'axs_save'=>array('type'=>'submit', ),
			);
		foreach ($form as $k=>$v) {
			$form[$k]['label']=$this->tr('ie.'.$k.'.lbl');
			if ($this->tr->has('ie.'.$k.'.comment')) $form[$k]['comment']=$this->tr->s('ie.'.$k.'.comment');
			if (isset($form[$k]['options'])) foreach ($form[$k]['options'] as $kk=>$vv) {
				if ($this->tr->has($tmp='ie.'.$k.'.'.$kk.'.lbl')) $form[$k]['options'][$kk]['label']=$this->tr->s($tmp);
				}
			}
		foreach (mb_list_encodings() as $k=>$v) $form['charset']['options'][strtolower($v)]=array('value'=>strtolower($v), 'label'=>$v, );
		ksort($form['charset']['options']);
		$this->form->structure_set($form);
		$this->form->vl=$this->form->form_input();
		if ($this->content->save) {
			$this->form->validate();
			if (empty($this->form->msg)) $this->content->import_export($axs['c']);
			}
		$this->vr['content'].=$this->form->form_parse_html();
		break;
	case 'r':
		$f=axs_get('search', $_POST);
		$r=axs_get('replace', $_POST);
		$d=(isset($_POST['dir'])) ? $this->f_secure($_POST['dir']):$this->content->dir_filemanager;
		if ($this->content->save) {
			if (strlen($f)) $this->content->content_replace(array($f,urlencode($f)), array($r,urlencode($r)), '', $axs['l'], $d);
			}
		$this->form->css_js();
		$this->tpl_set('form',
		'      <form id="search-replace" class="form" action="{$action}" method="post" enctype="multipart/form-data">'."\n".
		'       {$msg}'.
		'       <div id="dir_container" class="element text-join">'."\n".
		'        <label for="dir">{$r-dir_lbl}</label>'."\n".
		'        <input name="dir" type="text" id="dir" value="{$dir}" size="75" />'."\n".
		'        <a id="dir.browse_link" class="browse" href="{$dir_browse_url}" target="_blank"><span>{$dir_browse_lbl}</span></a>'."\n".
		'       </div>'."\n".
		'       <div id="search_container" class="element text">'."\n".
		'        <label for="search">{$r-search_lbl}</label>'."\n".
		'        <input type="text" name="search" id="search" value="{$search}" size="75" />'."\n".
		'       </div>'."\n".
		'       <div id="replace_container" class="element text">'."\n".
		'        <label for="replace">{$r-replace_lbl}</label>'."\n".
		'        <input type="text" name="replace" id="replace" value="{$replace}" size="75" />'."\n".
		'       </div>'."\n".
		'       <div id="save_container" class="element submit">'."\n".
		'        <input type="submit" name="axs_save" id="save" value="{$r-search_replace_lbl}" />'."\n".
		'       </div>'."\n".
		'      </form>'."\n".
		'      <script>axs.form.init("search-replace");</script>');
		$cl=array(
			'action'=>'?'.axs_url($this->url, array('write'=>'', )),
			'dir_browse_url'=>'?'.axs_url($this->url, array('value'=>$d, 'text'=>$d, 'browse'=>'dir', $this->content->key_ajax=>'')),
			'dir_browse_lbl'=>axs_html_safe($d),
			'dir'=>axs_html_safe($d), 'search'=>axs_html_safe($f), 'replace'=>axs_html_safe($r),
			);
		$cl['msg']=$this->form->msg_html();
		$this->vr['content'].=axs_tpl_parse($this->templates['form'], $cl+$this->tr->tr);
		break;
	case '':
	default:
		$this->plugin_editor=array('tr'=>axs_get('tr', $axs_content['content'][$axs['c']], array()), 'text'=>'', );
		foreach (axs_content::plugin_dirs() as $k=>$v) {
			if (file_exists($tmp=$v.$axs_content['content'][$axs['c']]['plugin'].'.edit.php')) {
				$this->f_path=$v;
				$this->plugin_editor['text']=$this->content->load_editor($tmp);
				break;
				}
			}
		#<Start output />
		if ($this->vr['text']!==false) {
			$this->content->tab=new axs_tab_edit('', $this->content->site_nr, $this->content->d_page_files, $axs['cfg']['cms_lang']);
			if ($this->content->save==='content') $this->content->save_();
			//$this->content->tab->msg+=$this->form->msg;
			$this->vr['content'].=$this->content->tab->ui($axs_content['content'][$axs['c']]['text'], $_GET, $_POST, $this->url, $this->ui['site.preview.url']);
			}
		$this->vr['content'].=$this->plugin_editor['text'];
		if ($this->vr['form']!==false) $this->vr['form']=include(__DIR__.'/edit-form.php');
		break;
	} #</switch ($s2)>

$this->tpl_set('content',
'			<nav id="menu-page" class="axs-ui-menu tabs">'."\n".
'				<h2 class="title visuallyhidden">{$site.menu.title}</h2>'."\n".
'{$tabs}'.
'				<script>axs.menu.attach("menu-page");</script>'."\n".
'			</nav>'."\n".
'			<div class="tab">'."\n".
'{$msg}'.
'{$content}'."\n".
'{$form}'."\n".
'			</div>');
$this->vr['content']=axs_tpl_parse($this->templates['content'], array(
	'tabs'=>$this->vr['tabs'],
	'msg'=>$this->content->form->msg_html(),
	'content'=>$this->vr['content'],
	'form'=>$this->vr['form'],
	));
} #</isset($_GET['c'])>
#if (isset($this->popup)) return $this->vr['content'];
$this->tpl_set('frame',
'	<div>'."\n".
'		<nav id="menu-left" class="axs-ui-menu vertical toggle open">'."\n".
'			<form class="sticky" action="?" method="get">'."\n".
'				<h2 class="title"><a class="toggle-switch" href="#menu-left">{$menu.title}</a></h2>'."\n".
'				<div id="menu-langs" class="axs menu mobile_toggle screen_toggle animation expand-down js">'."\n".
'					<h3 class="title"><a class="toggle-switch arrow down" href="#menu-langs">{$lang.lbl}: <strong>{$lang.current}</strong></a></h3>'."\n".
'					<ul>{$lang.list}</ul>'."\n".
'				</div>'."\n".
'      <ul class="menu">'."\n".
'{$menu}      </ul>'."\n".
'			</form>'."\n".
'			<script>axs.menu.attach("menu-left");</script>'."\n".
'		</nav>'."\n".
' 		<div id="content-editor" class="axs-ui-content content">'."\n".
'{$content}'."\n".
'		</div>'."\n".
'	</div>'."\n");
return axs_tpl_parse($this->templates['frame'], $this->vr);
#2006 ?>