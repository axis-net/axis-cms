<?php #2021-08-12
if (!defined('AXS_PATH_CMS')) exit(require('index.php')); # Prevent direct access

$this->tr_load(axs_tr::$lang, preg_replace('/\.php$/', '', basename(__FILE__)));
$axs['page']['head']['style_content']='<link href="'.AXS_PATH_CMS_HTTP.'index.content'.'.css" rel="stylesheet" type="text/css" media="screen" />'."\n";
$axs['page']['head']['edit-system']='<link href="'.axs_dir('editors', 'h').'edit-system'.'.css" rel="stylesheet" type="text/css" media="screen" />'."\n";

if (isset($_GET['s1'])) $this->url['s1']=urlencode($_GET['s1']);
require_once(__DIR__.'/edit-system.class.php');
$o=new axs_edit_system($this);
$this->s1=$o->inc;
switch ($o->inc) {
	case 'sysinfo':
		if ($tmp=axs_admin::no_permission(array('dev'=>'', 'admin'=>'', ))) return $tmp; #<Permission check />
		if (isset($_GET['phpinfo'])) exit(phpinfo());
		$table=array(
			1=>array('dt'=>'PHP version', 'dd'=>phpversion()),
			array('dt'=>'Server API', 'dd'=>PHP_SAPI),
			array('dt'=>'HTTP Server', 'dd'=>axs_get('SERVER_SOFTWARE', $_SERVER)), 
			array('dt'=>'Server OS', 'dd'=>PHP_OS),
			array('dt'=>'Filesystem root', 'dd'=>dirname(dirname(dirname(str_replace('\\', '/', __FILE__))).'/')),
			array('dt'=>'Login method', 'dd'=>($axs['session()']) ? 'PHP session':'Browser cookies'),
			array('dt'=>'session.gc_maxlifetime', 'dd'=>ini_get('session.gc_maxlifetime')/60),
			array('dt'=>'Safe mode', 'dd'=>(ini_get('safe_mode')) ? 'on':'off'), 
			array('dt'=>'register_globals', 'dd'=>(ini_get('register_globals')) ? 'on':'off'),
			array('dt'=>'magic_quotes_gpc', 'dd'=>(ini_get('magic_quotes_gpc')) ? 'on':'off'),
			array('dt'=>'magic_quotes_runtime', 'dd'=>(ini_get('magic_quotes_runtime')) ? 'on':'off'),
			array('dt'=>'Script memory limit', 'dd'=>ini_get('memory_limit')),
			array('dt'=>'function mail()', 'dd'=>($axs['mail()']) ? 'enabled':'disabled'),
			array('dt'=>'function getimagesize()', 'dd'=>(axs_function_ok('getimagesize')) ? 'enabled':'disabled'),
			array('dt'=>'function imagecreatetruecolor()', 'dd'=>(axs_function_ok('imagecreatetruecolor')) ? 'enabled':'disabled'),
			array('dt'=>'function imagecopyresampled()', 'dd'=>(axs_function_ok('imagecopyresampled')) ? 'enabled':'disabled'),
			array('dt'=>'function imagejpeg()', 'dd'=>(axs_function_ok('imagejpeg')) ? 'enabled':'disabled'),
			array('dt'=>'file_uploads', 'dd'=>(ini_get('file_uploads')) ? 'enabled':'disabled'),
			array('dt'=>'max_file_uploads', 'dd'=>ini_get('max_file_uploads')),
			array('dt'=>'upload_max_filesize', 'dd'=>ini_get('upload_max_filesize')),
			array('dt'=>'tracking of file upload progress', 'dd'=>(ini_get('session.upload_progress.enabled')) ? 'enabled':'disabled'),
			array('dt'=>'post_max_size', 'dd'=>ini_get('post_max_size')),
			array('dt'=>'allow_url_fopen', 'dd'=>(ini_get('allow_url_fopen')) ? 'on':'off'),
			'db'=>array('dt'=>'database profiles', 'dd'=>''),
			);
		$th=array_merge(array('nr'=>''), $axs['cfg']['db'][1], array('version'=>'', 'conn-info'=>'', ));
		//$table['db']['dd']='<th scope="col">nr</th>';
		foreach ($th as $k=>$v) $table['db']['dd'].='<th scope="col">'.$k.'</th>';
		//$table['db']['dd'].='<th scope="col">server version</th><th scope="col">connection collation</th>';
		$table['db']['dd']='<table class="table"><tr>'.$table['db']['dd'].'</tr>';
		foreach ($axs['cfg']['db'] as $k=>$v) {
			//$v['nr']=$k;
			$v=array_merge(array('nr'=>$k), $v, array('version'=>'', 'conn-info'=>'', ));
			if (strlen($v['pass'])) $v['pass']='*';
			if ($v['type']) {
				if ($tmp=axs_db_query(null, 0, $k, __FILE__, __LINE__, array('error_handle'=>0))) {
					$v['version']=$tmp->getAttribute(PDO::ATTR_SERVER_VERSION);
					if ($v['type']==='mysql') {
						$r=axs_db_query("SHOW VARIABLES WHERE Variable_name LIKE 'character\_set\_%' OR Variable_name LIKE 'collation%'", 1, $k, __FILE__, __LINE__, array('error_handle'=>0));
						foreach ($r as $cl) $v['conn-info'].='	<dt>'.$cl['Variable_name'].'</dt><dd>'.$cl['Value'].'</dd>';
						if ($v['conn-info']) $v['conn-info']='<dl>'.$v['conn-info'].'</dl>';
						}
					}
				else foreach (array('version'=>'?', 'conn-info'=>'Unable to connect', ) as $kk=>$vv) $v[$kk]=$vv;
				}
			$tmp=array();
			foreach ($th as $kk=>$vv) $tmp[$kk]='<td>'.axs_get($kk, $v).'</td>';
			$table['db']['dd'].='<tr>'.implode('', $tmp).'</tr>';
			}
		$table['db']['dd'].='</table>';
		foreach ($table as $k=>$v) $table[$k]='      <dt>'.$v['dt'].'</dt><dd>'.$v['dd'].'</dd>'."\n";
		return '     <h1>System info</h1>'."\n".
			'      <dl>'."\n".
			implode('', $table).'</dl>'."\n".
			'     <p><a href="?'.axs_url($o->url, array('phpinfo'=>'', )).'" target="_blank">phpinfo()</a></p>';
		break; #</case:sysinfo>
	case 'sql':
		if ($tmp=axs_admin::no_permission(array('dev'=>'', 'admin'=>'', ))) return $tmp; #<Permission check />
		$inc='edit-sql';
		break; #</case:sql>
	case 'filemonitor':
		$content='';
		$tmp=array('admin/', $axs['dir_cms'].' x:'.$axs['dir_cfg'], $axs['dir_site'].' x:content/', 'index.php', 'axiscms.php', 'robots.txt', '.htaccess', '.htpasswd', '_htaccess', '_htpasswd', );
		$files=array();
		foreach ($axs['cfg']['site'] as $k=>$v) foreach ($tmp as $kk=>$vv) $files[]=$v['dir_fs_root'].$v['dir'].$vv;
		$files=array_merge($files, $axs['cfg']['filemonitor']);
		$files=array_unique($files);
		$fn=new axs_filemonitor(AXS_SITE_ROOT);
		if (isset($_REQUEST['check'])) {
			$content=$fn->check($files);
			axs_log(__FILE__, __LINE__, (!$fn->compare_count_diff)?'filemonitor':'filemonitor-alert', $content);
			if (AXS_LOGINCHK!==1) exit('-');
			}
		if ($tmp=axs_admin::no_permission(array('admin'=>'', ))) return $tmp; #<Permission check />
		if (isset($_REQUEST['check_list'])) $content=$fn->checklist_display();
		if (isset($_REQUEST['check_save'])) {
			$content=$fn->check_save($files);
			axs_log(__FILE__, __LINE__, 'filemonitor-alert', 'Filemonitor checklist updated.');
			}
		return 
		'    <form class="form" action="'.'?'.axs_url($o->url, array('axs'=>array($o->form->key_form_id=>$o->form->form_id))).'" method="post" lang="en">'."\n".
		'     <h2>Filesystem monitor</h2>'."\n".
		'      <pre>'.$content.'</pre>'."\n".
		'     <input name="check_list" type="submit" value="View files checklist" />'."\n".
		'     <input name="check" type="submit" value="Check for changed files" />'."\n".
		'     <input name="check_save" type="submit" value="Save files checklist" />'."\n".
		'    </form>';
		break; #</case:filemonitor>
	case 'modules':
		if ($tmp=axs_admin::no_permission(array('dev'=>'', 'admin'=>'', ))) return $tmp; #<Permission check />
		$o=new axs_form_header_edit($o->url);
		return $o->editor();
		break; #</case:modules>
	case 'logview':
		if ($tmp=axs_admin::no_permission(array('dev'=>'', 'admin'=>'', ))) return $tmp; #<Permission check />
		$inc='edit-log';
		break; #</case:logview>
	case 'cfg':
	default:
		if ($tmp=axs_admin::no_permission(array('dev'=>'', 'admin'=>'', ))) return $tmp; #<Permission check />
		$o->form->css_js();
		$this->tpl_set($this->name.'_form', '     <form id="edit-system" class="edit clear-content" action="{$formact_url}" method="post">'."\n".
		'     <h1>{$cfg.system_edit.title}</h1>'."\n".
		'{$msg}'."\n".
		'{$elements}      '."\n".
		'     </form>'."\n");
		$this->vars['formact_url']='?'.axs_url($o->url, array('axs'=>array($o->form->key_form_id=>$o->form->form_id)));
		$values=(file_exists($tmp=AXS_PATH_CMS.'data/index.php')) ? (array)include($tmp):array();
		$values['emails']=implode(', ', (array)$values['emails']);
		$values['dontmail']=implode(', ', (array)$values['dontmail']);
		$values['filemonitor']=implode("\n", (array)$values['filemonitor']);
		$change=array();
		
		foreach ($o->fields as $k=>$v) {
			$o->fields[$k]['label']=$this->tr('cfg.'.$k.'.lbl');
			if ($this->tr->has('cfg.'.$k.'.comment')) $o->fields[$k]['comment']=$this->tr('cfg.'.$k.'.comment');
			if (!in_array($v['type'], array('fieldset', 'fieldset_end'), true)) {
				$o->fields[$k]['value']=(!empty($_POST['save'])) ? $o->post[$k]:$values[$k];
				$change[$k]=$o->fields[$k]['value'];
				}
			}
		$o->fields['timezone']['txt']='<a href="'.htmlspecialchars($o->timezone_url).'" target="_blank">'.htmlspecialchars($o->timezone_url).'</a>';
		# <List available CMS languages>
		$f='index';
		foreach (axs_admin::f_list(AXS_PATH_CMS, '/^'.$f.'\.tr\...\.php$/') as $k=>$v) {
			$v=substr(preg_replace('/^'.$f.'\.tr\.|\.php$/', '', $v), 0, 2);
			$o->fields['cms_lang']['options'][$v]=array('value'=>$v, 'label'=>$v, );
			}
		ksort($o->fields['cms_lang']['options']);
		# </List available CMS languages>
		$o->fields['db']=array('type'=>'fieldset', 'label'=>$this->tr('cfg.db.lbl'), );
		foreach ($values['db'] as $k=>$v) {
			if (isset($_POST['db_'.$k.'_pass'])) $_POST['db_'.$k.'_pass']='*';
			$o->fields['db_'.$k]=array('type'=>'fieldset', 'label'=>$k.'.', );
			foreach ($o->fields_db as $kk=>$vv) {
				$o->fields['db_'.$k.'_'.$kk]=$vv;
				$o->fields['db_'.$k.'_'.$kk]['label']=$this->tr('cfg.db.'.$kk.'.lbl');
				$o->fields['db_'.$k.'_'.$kk]['value']=$values['db_'.$k.'_'.$kk]=(!empty($_POST['save'])) ? $o->form->user_input['db_'.$k.'_'.$kk]:$v[$kk].'';
				$change['db'][$k][$kk]=$o->fields['db_'.$k.'_'.$kk]['value'].'';
				}
			$o->fields['db_'.$k.'_end']=array('type'=>'fieldset_end', );
			}
		$o->fields['db_end']=array('type'=>'fieldset_end', );
		$o->fields['site']=array('type'=>'fieldset', 'label'=>$this->tr('cfg.site.lbl'), );
		foreach ($values['site'] as $k=>$v) {
			$o->fields['site_'.$k]=array('type'=>'fieldset', 'label'=>$k.'.', );
			foreach ($o->fields_site as $kk=>$vv) {
				$o->fields['site_'.$k.'_'.$kk]=$vv;
				$o->fields['site_'.$k.'_'.$kk]['label']=$this->tr('cfg.site.'.$kk.'.lbl');
				if ($this->tr->has('cfg.site.'.$kk.'.comment')) $o->fields['site_'.$k.'_'.$kk]['comment']=$this->tr('cfg.site.'.$kk.'.comment');
				//$o->fields['site_'.$k.'_'.$kk]['value']=($_POST['save']) ? $o->post['site_'.$k.'_'.$kk]:$v[$kk];
				$values['site_'.$k.'_'.$kk]=$change['site'][$k][$kk]=(!empty($_POST['save'])) ? $o->form->user_input['site_'.$k.'_'.$kk]:$v[$kk];
				}
			foreach ($axs['cfg']['db'] as $db=>$tmp) $o->fields['site_'.$k.'_db']['options'][$db]=array('value'=>$db, 'label'=>$db);
			$o->fields['site_'.$k.'_timezone']['txt']='<a href="'.htmlspecialchars($o->timezone_url).'" target="_blank">'.htmlspecialchars($o->timezone_url).'</a>';
			$o->fields['site_'.$k.'_end']=array('type'=>'fieldset_end', );
			}
		$o->fields['site_end']=array('type'=>'fieldset_end', );
		$o->fields['save']=array('type'=>'submit', 'label'=>$this->tr('save.lbl'), );
		$o->form->structure_set($o->fields);
		
		if (!empty($_POST['save'])) {
			axs_admin::$user_log=false;
			if ($tmp=axs_config_edit::write($change, $this->tr)) $o->form->msg($tmp);
			#<Install SQL tables>
			if ((empty($o->form->msg)) && ($axs['cfg']['db'][1]['type'])) {
				$sql=new axs_sql_structure(1);
				if (!$sql->table_install($sql->tables_site(include(__DIR__.'/edit-system.sql.php'), 1), true)) $o->form->msg+=(array)$sql->msg;
				}
			#</Install SQL tables>
			if (empty($o->form->msg)) exit(axs_redir('?'.axs_url($o->url, array(), false)));
			}
		$this->vars['msg']=$o->form->msg_html();
		
		$this->vars['elements']='';
		foreach ($o->form->structure_get() as $k=>$v) $this->vars['elements'].=$o->form->element_html($k, $v, $values);
		return axs_tpl_parse($this->templates[$this->name.'_form'], $this->vars+$this->tr->tr);
		break; #</case 'cfg'>
	}
return include(__DIR__.'/'.$inc.'.php');
#2008 ?>