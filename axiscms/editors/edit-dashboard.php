<?php #2021-01-07
defined('AXS_PATH_CMS') or exit(require('index.php')); #<Prevent direct access />
if (AXS_LOGINCHK!==1) return //'	<h2 id="system_title">'.trim($_SERVER['SERVER_NAME'].$axs['http_root'], '/').
'</h2>'."\n".axs_user_login::form($this->l);

$e=preg_replace('/^edit-|\.php$/', '', basename(__FILE__));

$axs['page']['head'][$this->name]='<link href="'.axs_dir('editors', 'h').'edit-'.$e.'.css" rel="stylesheet" type="text/css" media="screen" />'."\n";

$html=[];
foreach ($this->structure as $k=>$v) if (isset($v['show']['d'])) {
	$v['class']=$k;
	if (empty($v['img'])) {
		$v['img']=(file_exists(AXS_PATH_CMS.'gfx/edit-'.$k.'.svg')) ? AXS_PATH_CMS_HTTP.'gfx/edit-'.$k.'.svg':'';
		}
	if (($k==='content') && (!empty($v['submenu']))) foreach ($v['submenu'] as $kk=>$vv) {
		$html[]=['class'=>$v['class'], 'url'=>$vv['url'], 'label'=>$v['txt'], 'txt'=>$vv['label'], 'img'=>$v['img'], ];
		}
	else $html[]=$v;
	}
foreach ($html as $k=>$v) {
	if ($v['img']) $v['img']='<img src="'.$v['img'].'" alt="'.htmlspecialchars($v['label']).'" />';
	$html[$k]=
	'		<section class="'.$v['class'].'">'."\n".
	'			<a href="'.$v['url'].'">'."\n".
	'				'.$v['img']."\n".
	'				<h2 title="'.htmlspecialchars($v['label']).'">'.htmlspecialchars($v['label']).'</h2>'."\n".
	'				'.axs_get('txt', $v, '')."\n".
	'			</a>'."\n".
	'		</section>'."\n";
	}
return 
'		<script>axs.animationcontrol.listInit("#content",">section",{delay:0.1,smooth:15});</script>'."\n".
implode('', $html)."\n";
#2021-01-07 ?>