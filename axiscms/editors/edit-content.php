<?php #2022-05-10
defined('AXS_PATH_CMS') or exit(require('index.php')); # <Prevent direct access />

if ($tmp=axs_admin::no_permission(array('admin'=>'', 'cms'=>'', ), false, __FILE__, __LINE__)) return $tmp; #<Permission check />
if (empty($axs_user['homedirs'])) return ''; # This editor can't be used wthout having any home directories
else $this->url['h']=$axs_user['home_nr'];

if (!isset($_POST['axs_save']) && !isset($_POST['axs_save_form'])) {
	$this->content=new axs_content_edit($this->site_nr, axs_get('l', $_GET), $axs_user['home']['dir']);
	}
else {
	$this->content=new axs_content_write($this->site_nr, axs_get('l', $_GET), $axs_user['home']['dir']);
	}

# <Redirect if site is on another domain>
if ($this->content->site['domain']) {
	if (preg_replace('/^.*\/\/|\/.*$/', '', $this->content->site['domain'])!=$_SERVER['SERVER_NAME']) {
		$tmp=$this->site;
		if (!preg_match('/^http|^\/\//', $tmp['domain'])) $tmp['domain']='http://'.$tmp['domain'];
		if (!preg_match('/\/$/', $tmp['domain'])) $tmp['domain'].='/';
		$tmp=htmlspecialchars($domain.$this->dir_admin.'?'.$_SERVER['QUERY_STRING']);
		return '<p><a href="'.$tmp.'">'.$tmp.'</a></p>';
		}
	} # </ Redirect if site is on another domain>

$this->editor=preg_replace('/\.php$/', '', basename(__FILE__));
$this->tr_load('en', $this->editor);
$axs['page']['head']['style_content']='<link href="'.AXS_PATH_CMS_HTTP.'index.content'.'.css" rel="stylesheet" type="text/css" media="screen" />'."\n";
$axs['page']['head'][$this->name]='<link href="'.axs_dir('editors', 'h').'edit-content'.'.css" rel="stylesheet" type="text/css" media="screen" />'."\n";

$axs['page']['class']['axs-ui-menu-left']='axs-ui-menu-left';
$this->tpl_set('content',
'	<div>'."\n".
'		<nav id="menu-left" class="axs-ui-menu vertical toggle open">'."\n".
'			<div class="sticky">'."\n".
'				<h2 class="title"><a class="toggle-switch" href="#menu-left">{$menu.title}</a></h2>'."\n".
'{$menu}'.
'			</div>'."\n".
'			<script>axs.menu.attach("menu-left");</script>'."\n".
'		</nav>'."\n".
' 		<div id="content-editor" class="axs-ui-content content">'."\n".
'{$content}'."\n".
'		</div>'."\n".
'	</div>'."\n");

$this->form=new axs_form_edit($this->site_nr, $this->url, array(''=>array('tr'=>'class')), '', $_POST);
$this->form->url=&$this->url;
$this->content->msg=&$this->form->msg;
$this->content->form=&$this->form;

$s1=(isset($_GET['s1'])) ? $_GET['s1']:'';
if ($axs['e']=='files') $s1='files';
switch ($s1) {
	case 'files':
		$s1='files';
		$include='edit-files.php';
		break;
	case 'settings':
		$s1='settings';
		$include='edit-content.settings.php';
		break;
	default:
		$s1='content';
		$include='edit-content.content.php';
		break;
	}
$this->url['s1']=$s1;
$axs['page']['class'][$s1]=$s1;
$this->ui=array(
	'site.menu.title'=>htmlspecialchars($this->content->site['title']),
	'site.menu'=>array('content'=>false, 'files'=>'files', 'settings'=>'settings', ),
	'site.preview.url'=>$axs['http_root'].$this->content->site['dir'],
	'site.preview.txt'=>$this->tr('site.preview.txt'),
	'menu_path'=>array(0=>$this->content->site['title']),
	'content'=>'',
	'menu.title'=>$this->tr('menu.'.$s1.'.lbl'),
	);
foreach ($this->ui['site.menu'] as $k=>$v) $this->ui['site.menu'][$k]=array(
	'act'=>$s1,
	'url'=>'?'.axs_url($this->url, array('s1'=>$v)),
	'label'=>$this->tr('menu.'.$k.'.lbl'),
	);
$this->breadcrumb[$this->editor]=$this->ui['site.menu'][$s1]+array('submenu'=>$this->ui['site.menu']);
$this->ui['site.menu']=$this->menu_build($this->ui['site.menu'], '     ', array('class'=>'menu'));

if ($include=='edit-files.php') {
	require_once(axs_dir('editors').'edit-files.php');
	$f=new axs_filemanager($this->site_nr, $axs_user['home']['dir'], $this->content->dir, $this->url, axs_get('f', $_POST));
	$this->ui['content']=$f->ui();
	}
else $this->ui['content']=require(axs_dir('editors').$include);

$this->ui['site.preview.url']=htmlspecialchars($this->ui['site.preview.url']);
$this->ui['menu_path']=implode(' &gt; ', $this->ui['menu_path']);

if (axs_get('section', $axs['get'])==='content') return $this->ui['content'];
$this->tpl_set('', 
'	<nav id="menu-top" class="axs-ui-menu tabs">'."\n".
'		<h2 class="title visuallyhidden">{$site.menu.title}</h2>'."\n".
'		<ul class="tools"><li><a class="a" href="{$site.preview.url}" target="_blank">{$menu_path} &gt; {$site.preview.txt}</a></li></ul>'."\n".
'{$site.menu}'.
'		<script>axs.menu.attach("menu-top");</script>'."\n".
'	</nav>'."\n".
'{$content}'.
'	<script src="'.axs_dir('editors', 'h').'edit-content.js"></script>');
return axs_tpl_parse($this->templates[''], $this->ui);
#2006 ?>