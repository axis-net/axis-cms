<?php #2020-07-09
class axs_tr_edit {
	function __construct($e, $structure) {
		global $axs, $axs_content;
		$this->e=&$e;
		$this->structure=array();
		$i=1;
		foreach ($structure as $k=>$v) {	$this->structure[$k]=str_replace('.', '_', $k).$i;	$i++;	}
		$e->form->css_js();
		$this->tpl=array(
			'form'=>
			'		<form id="edit-tr" method="post" action="{$formact}" class="form">'."\n".
			'{$msg}'.
			'{$elements}'.
			'			<div class="element submit">'."\n".
			'{$reset}'.
			'				<input id="tr_save" name="axs_save_tr" type="submit" value="'.$e->tr('save_lbl').'" />'."\n".
			'			</div>'."\n".
			'			<input name="b" type="hidden" value="{$b}" />'."\n".
			'			<input name="axs_save" type="hidden" value="-" />'."\n".
			'		</form>'."\n".
			'		<script>axs.form.init("edit-tr");</script>'."\n",
			/*'element'=>
			'      <div class="container">'."\n".
			'       <label>{${$k}} <textarea id="{$k}" name="{$k}" cols="10" rows="2">{$v}</textarea></label>'."\n".
			'{$del}'.
			'      </div>'."\n",*/
			'element_add'=>
			'			<div class="element_add">'."\n".
			'				<label>'.$e->tr('tr.add_new.lbl').' <input id="add_new-{$nr}" name="add_new-{$nr}" type="text" size="20" value="{$v}" /></label>'."\n".
			'				<input name="axs_save_tr" type="submit" value="+" title="'.$e->tr('tr.add_new.lbl').'" />'."\n".
			'			</div>'."\n",
			'element_del'=>'			<input name="_del_{$k}" type="submit" value="X" title="'.$e->tr('tr.del.lbl').'" />'."\n",
			);
		$this->vr=array(
			'formact'=>'?'.axs_url($e->url, array('plugin'=>$axs_content['plugin'], )),
			'b'=>'?'.axs_url($e->url, array('plugin'=>$axs_content['plugin']), false),
			'elements'=>'',
			'reset'=>'',
			);
		if ($this->vl=$this->e->content->plugin_tr_get($axs_content['content'][$axs['c']]['plugin'])) {
			$this->tpl['element_add']=$this->tpl['element_del']='';
			$this->vr['reset']=
			'				<label><input name="_reset_empty" type="checkbox" value="1" />'.$e->tr('tr.reset_empty.lbl').'</label>'."\n".
			'				<label><input name="_reset_all" type="checkbox" value="1" />'.$e->tr('tr.reset_all.lbl').'</label>'."\n";
			}
		} #</__construct()>
	public function ui() {
		global $axs, $axs_content;
		if (!is_array($this->structure)) return '';
		$vl=$axs_content['content'][$axs['c']]['tr'];
		#<save>
		if ($this->e->content->save) {
			#<add new key>
			for ($i=1; count($this->structure)>=$i-1; $i++) if ($v=axs_get('add_new-'.$i, $_POST)) {
				if (!isset($this->structure[$v])) {
					$this->structure=axs_fn::array_el_add($this->structure, $i, $v, '');
					}
				else $this->e->form->msg('add_new-'.$i, $this->e->tr('tr.add_new.msg'), array('v'=>$v, 'nr'=>$i));
				} #</add new key>
			#<del key>
			foreach ($this->structure as $k=>$v) if (isset($_POST['_del_'.$k])) unset($this->structure[$k]);
			#</del key>
			foreach ($this->structure as $k=>$v) {
				$vl[$k]=axs_get($v, $_POST, '');
				if (!empty($_POST['_reset_empty']) && (!strlen($vl[$k]))) $vl[$k]=axs_get($k, $this->vl, '');
				if (!empty($_POST['_reset_all'])) $vl[$k]=axs_get($k, $this->vl, '');
				}
			if (empty($this->e->form->msg)) $this->e->content->save_tr($this->structure, $vl);
			}
		#</save>
		$this->vr['msg']=$this->e->form->msg_html($this->e->form->msg);
		$i=1;
		$this->vr['elements'].=axs_tpl_parse($this->tpl['element_add'], array('nr'=>$i, 'v'=>htmlspecialchars(axs_get('add_new-'.$i, $_POST))));
		foreach ($this->structure as $k=>$v) {
			$i++;
			$v=array(
				'name'=>$v, 'type'=>'textarea', 'cols'=>75, 'rows'=>2, 'label'=>'{$'.$k.'}', 'label.html'=>'{&dollar;'.$k.'}', 
				//'value'=>preg_replace('#<br />|<br/>|<br>#i', "\n", $axs_content['content'][$axs['c']]['tr'][$k]),
				'value'=>axs_get($k, $vl),
				'txt'=>axs_tpl_parse($this->tpl['element_del'], array('k'=>$k)),
				);
			$v['input']=(substr($k, -5)==='.html') ? axs_textedit::editor($v['name'], $v['value'], 'html'):'<textarea id="'.$v['name'].'" name="'.$v['name'].'" cols="'.$v['cols'].'" rows="'.$v['rows'].'">'.$v['value'].'</textarea>';
			$this->e->form->element($k, $v);
			$this->vr['elements'].=
				axs_tpl_parse($this->e->form->templates($v), $v).
				axs_tpl_parse($this->tpl['element_add'], array('nr'=>$i, 'v'=>htmlspecialchars(axs_get('add_new-'.$i, $_POST))));
			}
		return axs_tpl_parse($this->tpl['form'], $this->vr);
		} #</ui()>
	} #</class::axs_tr_edit>
#2007 ?>