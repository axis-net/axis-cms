//2013-07-12
axs.edit_content={
	init:function (){
		this.zerofill=function(nr){
			if(nr<10) nr='0'+nr+'';
			return nr+'';
			}//</zerofill()>
		var el;
		$('#content-menu select').change(function(){	document.getElementById('content-menu').submit();	});
		$('#content-menu input[type="submit"]').addClass('visuallyhidden');
		var d=new Date();
		this.date={
			'date':d.getFullYear()+'-'+this.zerofill(d.getMonth()+1)+'-'+this.zerofill(d.getDate()),
			'h':this.zerofill(d.getHours()),
			'i':this.zerofill(d.getMinutes()),
			's':this.zerofill(d.getSeconds())
			};
		$('#page-settings #publ legend').click(function(){
			for (var k in axs.edit_content.date) {
				$('#publ_'+k).val(axs.edit_content.date[k]);
				$('#hidden_'+k).val('');
				}
			});
		$('#page-settings #hidden legend').click(function(){
			for (var k in axs.edit_content.date) {
				$('#hidden_'+k).val(axs.edit_content.date[k]);
				}
			});
		$('#page-settings #lock').click(function(){
			for (var k in axs.edit_content.date){
				$('#publ_'+k).val('');
				$('#hidden_'+k).val('');
				}
			return false;
			});
		$('#page-settings #restrict table input').each(function(){
			if (/\[-\]/.exec(this.name)) $(this).click(function(){
				var col=this.parentNode.parentNode.className;
				this.axs_value=(this.checked) ? this.checked:false;
				if (this.checked) $('#page-settings #restrict td.'+col+' input').not(':first').each(function(){ //<Uncheck all />
					this.axs_value=(this.checked) ? this.checked:false;
					if (!this.disabled) this.checked=false;
					});
				else $('#page-settings #restrict td.'+col+' input').not(':first').each(function(){ //<Restore value all />
					if (!this.disabled) this.checked=this.axs_value;
					});
				});
			else $(this).click(function(){
				var col=this.parentNode.parentNode.className;
				this.axs_value=(this.checked) ? this.checked:false;
				if (this.checked) $('#page-settings #restrict td.'+col+' input:first').not(':disabled').attr('checked',false);
				});
			});
		el=$('#page-settings');
		if (el.length){
			$('#page-settings label[for="del"]').addClass('visuallyhidden');
			$('#del_link').click(function(event){
				event.preventDefault();
				$('#del_confirm').trigger('click');
				});
			$('#del_confirm').unbind('click');
			$('#del_confirm').click(function(){
				if ($('#del_confirm').is(':disabled')) return false;
				var val=$('#del').val();
				if (val) return true;
				if (confirm($('label[for="del"]').text())) {
					var val=(!$('#del option[value="r"]').is(':disabled')) ? 'r':'p';
					if (!$('#del').val()) $("#del").val(val);
					return true;
					}
				else return false;
				});
			}
		el=$('#menu_add_link');
		if (el.length){
			if ($('#menu_add').is(':disabled')) {
				el.click(function(){	return false;	});
				el.addClass('disable');
				}
			else el.click(function(){
				$('#menu_add').attr('checked','true');
				document.getElementById('page-settings').submit();
				});
			}
		//<edit-form>
		$('#form-editor input.del').click(function(){
			if ($('#form-editor label.del input').is(':checked')) {
				$('#form-editor .element input, #form-editor .element select, #form-editor .element textarea').attr('disabled','disabled');
				return true;
				}
			else return false;
			});
		//</edit-form>
		}//</init()>
	}//</class::axs.edit_content>
axs.edit_content.init();
//2011-06-09