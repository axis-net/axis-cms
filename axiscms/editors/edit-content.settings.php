<?php #2022-05-10
if ($tmp=axs_admin::no_permission(array('dev'=>'', 'admin'=>'', 'cms'=>'', ), false, __FILE__, __LINE__)) return $tmp; #<Permission check />

$this->tpl_set('form',
'			<form id="settings-form" class="form tab {$class}" action="{$action}" method="post" enctype="multipart/form-data">'."\n".
'{$msg}'.
'{$elements}'.
'				<input name="b" type="hidden" value="{$b}" />'."\n".
'				<script>axs.form.init("settings-form");</script>'."\n".
'			</form>'."\n");
$form=array('action'=>'', 'msg'=>'', 'elements'=>'', 'b'=>'', );
$form_save_btn=array('type'=>'submit', 'label'=>$this->tr('save.lbl'), );

$vr=array(
	'menu'=>array('langs'=>'', 'tr'=>'', 'favicon'=>'', 'socialmedia'=>'', 'media'=>'', 'theme'=>'', 'code'=>'', ),
	'content'=>'',
	);
$s2=(isset($vr['menu'][axs_get('s2', $_GET)])) ? $_GET['s2']:key($vr['menu']);
$this->url['s2']=$s2;
foreach ($vr['menu'] as $k=>$v) $vr['menu'][$k]=array(
	'act'=>$s2,
	'url'=>'?'.axs_url($this->url, array('s2'=>$k)),
	'label'=>$this->tr('settings.menu.'.$k.'.lbl'),
	);
$this->breadcrumb[$s2]=$vr['menu'][$s2]+array('submenu'=>$vr['menu']);
$vr['menu']=$this->menu_build($vr['menu'], '     ', array('class'=>'menu'), 'settings-menu');

$this->form->css_js();

switch ($s2) {
	case 'langs':
		$iso_code_url=htmlspecialchars(axs_file_array_get(axs_dir('lib').'axs.dict.php', 'languages_iso_code2_url'));
		$this->form->structure['iso_codes']=array('type'=>'content', 'txt'=>'<p><a href="'.$iso_code_url.'" target="_blank">'.$this->tr('settings.langs.iso_code.lbl').' '.$iso_code_url.'</a></p>'."\n", );
		$this->form->structure['langs']=array('type'=>'table', 'label'=>$this->tr('settings.menu.langs.lbl'), 'options'=>array(
				'nr'=>array('type'=>'number', 'label'=>$this->tr('settings.langs.nr.lbl'), 'scope'=>'col', ),
				'code'=>array('type'=>'text', 'label'=>$this->tr('settings.langs.code.lbl'), 'scope'=>'col', ),
				'l'=>array('type'=>'text', 'label'=>$this->tr('settings.langs.l.lbl'), 'scope'=>'col', ),
				'abbr'=>array('type'=>'text', 'label'=>$this->tr('settings.langs.abbr.lbl'), 'scope'=>'col', ),
				'h'=>array('type'=>'checkbox', 'label'=>$this->tr('settings.langs.h.lbl'), 'scope'=>'col', ),
				'img'=>array('type'=>'file', 'label'=>$this->tr('settings.langs.img.lbl'), 'scope'=>'col', ),
				'del'=>array('type'=>'checkbox', 'label'=>$this->tr('settings.langs.del.lbl'), 'scope'=>'col', ),
				),
			'value'=>'',
			);
		$val=$this->content->site['langs']+array(''=>array('code'=>'', 'l'=>''));
		if (isset($_POST['axs_save'])) {
			$add=false;
			$del=$val_act=array();
			$dir=axs_dir('site');
			foreach ($val as $k=>$v) {
				# <Get input values />
				foreach (array('nr','code','l','abbr','h','del') as $vv) $v[$vv]=axs_html_safe(axs_get($k.'_'.$vv, $_POST));
				$v['nr']=intval($v['nr']);
				$v['h']=($v['h']) ? true:false;
				$v['file']=(isset($_FILES[$k.'_img']['name'])) ? $k.'_img':false;
				$v['file_act']=(isset($_FILES[$k.'_img_act']['name'])) ? $k.'_img_act':false;
				# <Validate />
				if ($v['code'] && !$v['l']) $this->form->msg($k.'_l', $v['nr'].'. '.$this->tr('settings.langs.l.msg'));
				if ($v['code']) {
					if (strlen($v['code'])!=2) $this->form->msg($k.'_code', $v['nr'].'. '.$this->tr('settings.langs.code.msg'));
					if (isset($val[$v['code']])) $this->form->msg($k.'_code', $v['nr'].'. '.$this->tr('settings.langs.code_exist.msg'));
					if (empty($this->form->msg)) $add=$v;
					}
				if ($v['h']) {	if (count($val)<2) $this->form->msg($k.'_h', $this->tr('settings.langs.h.msg'));	}#Can't hide if only one language!
				if ($v['del']) $del[$k]=$k;
				if (($k) && (!$v['h']) && (!$v['del'])) $val_act[$k]=$k;
				$val[$k]=$v;
				}
			if (count($val_act)<1) $this->form->msg('del', $this->tr('settings.langs.h.msg'));
			if (empty($this->form->msg)) { # <Save if no errors />
				if ($this->content->db) {
					$this->header=new axs_form_header_edit($this->url, $this->content->site_nr);
					}
				if ($add) {
					$val[$add['code']]=$add;
					$val['']['nr']=$add['nr']+1;
					}
				if (!empty($del)) { # <Delete language>
					foreach ($del as $v) {
						unset($val[$v]);
						# <Alter SQL structure />
						if ($this->content->db) $this->header->lang_del($v);
						# <Delete content />
						$index=$this->content->menu_get('', $v);
						foreach ($index as $kk=>$vv) $this->content->content_del($vv['c'], '', $v);
						if (empty($this->form->msg)) $this->content->fs->f_del('index_'.$v.'.php', false);
						if (is_file($dir.$v)) {
							$tmp=true;
							foreach ($axs['cfg']['site'] as $kk=>$vv) if ($kk!==$this->site_nr) foreach ($vv['langs'] as $kkk=>$vvv) if ($kkk===$v) $tmp=false;
							if ($tmp) unlink($dir.$v);
							}
						}
					//$axs['msg_error']+=$this->content->msg;
					} # </Delete language>
				# <Reorder />
				$val=$sorted=axs_fn::array_sort($val, 'nr');
				unset($sorted['']);
				# <Save />
				if (empty($this->form->msg)) $this->form->msg+=axs_config_edit::write(array('site'=>array($this->content->site_nr=>array('langs'=>$sorted))));
				if (empty($this->form->msg)) { # <Save img files if no errors />
					$ext=false;
					foreach ($sorted as $k=>$v) foreach (axs_admin::$ftypes_img as $vv) { # <Get extesion of img file />
						if (file_exists($this->content->dir_content.'lang_'.$k.'.'.$vv)) {
							$ext=$vv;
							break 2;
							}
						}
					foreach ($sorted as $k=>$v) foreach (array('','_act') as $vv) {
						if (isset($_POST[$k.'_img_del'])) $this->content->fs->f_del('lang_'.$k.$vv.'.'.$ext, false);
						if (!$f_ext=$this->f_ext($_FILES[$v['file'.$vv]]['name'])) continue;
						if ($ext) {
							if ($ext!=$f_ext) {
								$this->form->msg($k.'_img', $v['nr'].'. '.$this->tr('settings.langs.img_ext.msg').' .'.$ext.'!');
								continue;
								}
							}
						$this->content->fs->f_upl($v['file'.$vv], 'lang_'.$k.$vv.'.'.$f_ext);
						}
					if ($add) { #<Add new language>
						# <Alter SQL structure />
						if ($this->content->db) $this->header->lang_add($add['code']);
						//$this->content->fs->dir_set($d_bak);
						# <Add/copy content>
						$this->content->site_title_update($this->content->site_title_get(), $add['code']);
						$index=$this->content->menu_get('');
						$nr=0;
						foreach ($index as $k=>$v) {
							$nr++;
							$this->content->structure_update($v, array('add'=>$nr, ), $add['code'], '');
							if (!empty($this->content->msg)) break;
							$v['files']=array();
							if ($v['plugin']!='menu') {
								#<Copy content />
								$v['data']=$this->content->content_get($v['c'], false, '');
								$this->content->content_upd($v['data'], $v['c'], $add['code'], '');
								if ($v['plugin']) {
									#<Load tr if plugin defines it />
									$tmp=$this->content->plugin_tr_get($v['plugin'], $add['code']);
									if (!empty($tmp)) $v['files'][$v['c'].'_'.$add['code'].'.tr.php']='<?php'."\n".'return '.axs_fn::array_code($tmp).'?>';
									#<Load form if plugin defines it />
									$tmp=$this->content->plugin_form_get($v['plugin'], $add['code']);
									if (!empty($tmp)) $v['files'][$v['c'].'_'.$add['code'].'.form.php']='<?php'."\n".'return '.axs_fn::array_code($tmp).'?>';
									}
								}
							#<Load menu pics>
							$ext='';
							foreach (axs_content::$menu_pic_ext as $vv) {
								if (file_exists($f=$this->content->dir_content.$v['c'].'_'.$this->content->l.'.'.$vv)) {
									$ext=$this->f_ext($f);
									break;
									}
								}
							foreach (array(
								$v['c'].'_'.$this->content->l.'.'.$ext=>$v['c'].'_'.$add['code'].'.'.$ext,
								$v['c'].'_'.$this->content->l.'_act.'.$ext=>$v['c'].'_'.$add['code'].'_act.'.$ext,
								) as $kk=>$vv) if (file_exists($f=$this->content->dir_content.$kk)) $v['files'][$vv]=implode(file($f));
							#</Load menu pics>
							#<Write files />
							foreach ($v['files'] as $kk=>$vv) $this->content->fs->f_save($this->content->dir_content.$kk, $vv);
							} # </Add/copy content files>
						} #</Add new language>
					}
				} #</Save>
			}
		$fup_disabled=($axs['file_uploads']) ? '':' disabled="disabled"';
		$nr=1;
		foreach ($val as $k=>$v) {
			if (!isset($v['nr'])) $v['nr']=$nr;
			$v['code']=$k;
			foreach ($this->form->structure['langs']['options'] as $kk=>$vv) if (!isset($v[$kk])) $v[$kk]='';
			if (!$k) $v['code']='<input name="'.$k.'_code" size="2" maxlength="2" type="text" value="'.axs_html_safe($v['code']).'" />';
			$v['h']=($v['h']) ? ' checked="checked"':'';
			$v['class']=($v['h']) ? ' disabled':'';
			$v['img']='';
			foreach (axs_admin::$ftypes_img as $vv) {
				foreach (array('','_act') as $vvv) if (file_exists($this->content->dir_content.($f='lang_'.$k.$vvv.'.'.$vv))) $v['img'].='<img src="'.htmlspecialchars($this->content->dir_content_http.$f).'" alt="'.htmlspecialchars($f).'" />';
				if ($v['img']) {
					$v['img']=' <label>'.$v['img'].' '.$vv.' <input name="'.$k.'_img_del" type="checkbox" value="1" />'.$this->tr('settings.langs.img_del.lbl').'</label>';
					break;
					}
				}
			//$v['img_del']='';
			$v['del']=($k) ? '<input name="'.$k.'_del" type="checkbox" value="1" />':'';
			$this->form->structure['langs']['value'].='      <tr class="top'.$v['class'].'">'."\n".
			'       <td><input name="'.$k.'_nr" size="2" type="text" value="'.$v['nr'].'" /></td>'."\n".
			'       <td>'.$v['code'].'</td>'."\n".
			'       <td><input name="'.$k.'_l" size="10" type="text" value="'.$v['l'].'" /></td>'."\n".
			'       <td><input name="'.$k.'_abbr" size="3" type="text" value="'.$v['abbr'].'" /></td>'."\n".
			'       <td><input name="'.$k.'_h" type="checkbox" value="1"'.$v['h'].' /></td>'."\n".
			'       <td class="img"><input name="'.$k.'_img" size="5" type="file"'.$fup_disabled.' /><input name="'.$k.'_img_act" size="5" type="file"'.$fup_disabled.' />'.$v['img'].'</td>'."\n".
			'       <td class="del">'.$v['del'].'</td>'."\n".
			'      </tr>'."\n";
			$nr++;
			}
		$this->form->structure['axs_save']=$form_save_btn;
		break; #</case 'langs'>
	case 'favicon':
		$this->form->structure=array(
			'file'=>array(
				'type'=>'file', 'size'=>0, 'label'=>$this->tr->t('settings.favicon.file.lbl'), 'comment'=>'('.implode(', ', axs_content::$favicon).')', 'accept'=>implode(',', axs_content::$favicon), 'name_file'=>'', 'add_fn'=>true,
				't'=>array(''=>array('ext'=>'', 'name_base'=>'', 'alt'=>'', )),
				),
			'axs_save'=>$form_save_btn,
			);
		foreach (axs_content::$favicon as $k=>$v) if (file_exists(axs_dir('content').'favicon.'.$v)) {
			$this->form->structure['file']['name_file']=$this->form->structure['file']['t']['']['name_base']=$this->form->structure['file']['t']['']['alt']='favicon.'.$v;
			break;
			}
		if (isset($_POST['axs_save'])) {
			$this->form->values=$this->form->form_input();
			$this->form->validate($this->form->values);
			if (empty($this->form->msg)) {
				if ($f=$this->form->values['file']['name']) $this->form->structure['file']['name_file']=$f='favicon.'.$this->form->fs->f_ext($f);
				$this->form->form_save_files($this->form->values);
				if ($f) foreach (axs_content::$favicon as $k=>$v) if ('favicon.'.$v!==$f) $this->form->fs->del('favicon.'.$v, false);
				axs_exit(axs_redir('#settings-form'));
				}
			}
		break; #</case 'favicon'>
	case 'socialmedia':
		$file='socialmedia.og.image.';
		$this->form->structure=array(
			'file'=>array(
				'type'=>'file', 'size'=>0, 'label'=>$this->tr->t('settings.socialmedia.file.lbl'), 'comment'=>'('.implode(', ', axs_content::$socialmedia_og_image).')', 'accept'=>implode(',', axs_content::$socialmedia_og_image), 'name_file'=>'', 'add_fn'=>true,
				't'=>array(''=>array('ext'=>'', 'name_base'=>'', 'alt'=>'', )),
				),
			'fb_app_id'=>array('type'=>'text', 'size'=>50, 'maxlength'=>255, 'label'=>$this->tr->t('settings.socialmedia.fb_app_id.lbl'), 'value'=>$this->content->site_cfg['fb_app_id'], ),
			'axs_save'=>$form_save_btn,
			);
		foreach (axs_content::$socialmedia_og_image as $k=>$v) if (file_exists(axs_dir('content').$file.$v)) {
			$this->form->structure['file']['name_file']=$this->form->structure['file']['t']['']['name_base']=$this->form->structure['file']['t']['']['alt']=$file.$v;
			break;
			}
		if (isset($_POST['axs_save'])) {
			$this->form->values=$this->form->form_input();
			$this->form->validate($this->form->values);
			if (empty($this->form->msg)) {
				if ($f=$this->form->values['file']['name']) $this->form->structure['file']['name_file']=$f=$file.$this->form->fs->f_ext($f);
				$this->form->form_save_files($this->form->values);
				if ($f) foreach ($accept as $k=>$v) if ($file.$v!==$f) $this->form->fs->del($file.$v, false);
				$this->content->site_cfg_update(array('fb_app_id'=>$this->form->values['fb_app_id']));
				axs_exit(axs_redir('#settings-form'));
				}
			}
		break; #</case 'socialmedia'>
	case 'media':
		$this->form->user_input+=$vl=$this->content->media_settings($this->content->site_nr, $_POST);
		unset($this->form->user_input['img']);
		if (isset($_POST['axs_save'])) $this->content->site_cfg_update(array('media'=>$vl));
		$this->form->structure+=array(
			'img'=>array(
				'type'=>'content',
				'txt'=>axs_filesystem_img::img_proc_ui($vl),
				),
			'mp_w'=>array('type'=>'number', 'label.html'=>$this->tr('settings.media.mp_w.lbl'), 'min'=>1, 'max'=>20000, 'step'=>1, ),
			'mp_h'=>array('type'=>'number', 'label.html'=>$this->tr('settings.media.mp_h.lbl'), 'min'=>1, 'max'=>20000, 'step'=>1, ),
			'mp_play'=>array('type'=>'checkbox', 'label.html'=>$this->tr('settings.media.mp_play.lbl'), 'value'=>1, ),
			'axs_save'=>$form_save_btn,
			);
		break; #</case 'media'>
	case 'theme':
		if (isset($_POST['axs_save'])) { #<Save>
			$this->content->site_cfg_update(array('theme'=>addslashes($_POST['theme']), ));
			}
		$css=new axs_css(false);
		$selectors=array(
			'css-html'=>array('s'=>'html', ), 'css-body'=>array('s'=>'body', ),
			'css-h1'=>array('s'=>'h1', ), 'css-h2'=>array('s'=>'h2', ), 'css-h3'=>array('s'=>'h3', ), 'css-h4'=>array('s'=>'h4', ), 'css-h5'=>array('s'=>'h5', ), 'css-h6'=>array('s'=>'h6', ), 
			'css-p'=>array('s'=>'p', ), 
			'css-ul'=>array('s'=>'ul', ), 'css-ul-li'=>array('s'=>'ul li', ), 'css-ol'=>array('s'=>'ol', ), 'css-ol-li'=>array('s'=>'ol li', ), 
			'css-address'=>array('s'=>'address', ), 'css-form'=>array('s'=>'form', ), 'css-blockquote'=>array('s'=>'blockquote', ), 
			'css-a'=>array('s'=>'a', ), 'css-a_hover'=>array('s'=>'a:hover', ), 'css-a_visited'=>array('s'=>'a:visited', ), 'css-img'=>array('s'=>'img', ), 
			'css-header'=>array('s'=>'header', ), 'css-menu_path'=>array('s'=>'#menu_path', ), 'css-menu1'=>array('s'=>'#menu1', ), 'css-content'=>array('s'=>'#content', ), 'css-footer'=>array('s'=>'footer', ), 
			);
		$selector=(isset($selectors[$tmp=axs_get('edit', $_GET)])) ? $tmp:false;
		$rules=array();
		$unit='(123'.implode(' / ', $css->units).')';
		$webfonts=array(
			'"Arial", "Helvetica", sans-serif',
			'"Arial Black", "Gadget", sans-serif',
			'"Comic Sans MS", cursive',
			'"Courier New", "Courier", monospace',
			'"Georgia", serif',
			'"Impact", "Charcoal", sans-serif',
			'"Lucida Console", "Monaco", monospace',
			'"Lucida Sans Unicode", "Lucida Grande", sans-serif',
			'"Palatino Linotype", "Book Antiqua", "Palatino", serif',
			'"Tahoma", "Geneva", sans-serif',
			'"Times New Roman", "Times", serif',
			'"Trebuchet MS", "Helvetica", sans-serif',
			'"Verdana", "Geneva", sans-serif',
			'"Symbol", symbol',
			'"Webdings", "Zapf Dingbats", symbol',
			'"MS Sans Serif", "Geneva", sans-serif',
			'"MS Serif", "New York", serif',
			);
		$this->form->structure['theme']=array('type'=>'select', 'label'=>$this->tr('settings.theme.theme.lbl'), 'options'=>array(''=>array('value'=>'', 'label'=>'-')), 'value'=>$this->content->site_cfg['theme'], );
		foreach (axs_config_edit::site_themes_get(/*$axs['site_nr']*/) as $k=>$v) {
			$this->form->structure['theme']['options'][$k]=array('value'=>$v['value'], 'label'=>$v['label']);
			}
		$this->form->structure['css']=array('type'=>'fieldset', );
		$nr=0;
		$style=(file_exists($f=axs_dir('content').'index.css')) ? file_get_contents($f):'';
		if ($style) $css->ParseStr($style);
		foreach ($selectors as $k=>$v) {
			$v['dir']=axs_dir('content', 'h');
			$v['rules']=($style) ? $css->rule_set_dir($css->GetSection($v['s'])):array();
			$v['code-inline']=$v['code-txt']='';
			foreach ($v['rules'] as $kk=>$vv) {
				$v['code-inline'].=$kk.':'.$vv.';';
				$v['code-txt'].='	'.$kk.':'.$vv.';'."\n";
				}
			if ($v['code-txt']) $v['code-txt']="\n".$v['code-txt'].'	';
			$this->form->structure[$k]=array(
				'type'=>'fieldset',
				'label'=>$v['s'],
				'txt'=>'<pre style="'.axs_html_safe($v['code-inline']).'">'.axs_html_safe($v['s'].' {'.$v['code-txt'].'}').'</pre>',
				);
			$v['url']=array('edit'=>$k, );//'b'=>urlencode('?'.$_SERVER['QUERY_STRING'].'#'.$k));
			if ($selector===$k) {
				$this->url+=$v['url'];
				$v['url']=array('edit'=>false, );//'b'=>false, );
				$this->form->structure[$k]['class']='open';
				#<Margin, padding />
				foreach (array('margin','padding') as $vv) {
					$this->form->structure[$vv]=array('type'=>'fieldset', );
					foreach (array('top','right','bottom','left') as $vvv) {
						$rules[$vv.'-'.$vvv]=$this->form->structure[$vv.'-'.$vvv]=array('type'=>'text', 'size'=>5,
						'value'=>($style) ? $css->Get($v['s'], $vv.'-'.$vvv):'',
						'txt'=>$unit, );
						}
					$this->form->structure[$vv.'/']=array('type'=>'fieldset_end', );
					}
				#<Background />
				$this->form->structure['background']=array('type'=>'fieldset', );
				$rules['background-color']=
				$this->form->structure['background-color']=array('type'=>'color', 'size'=>8, 'value'=>$css->Get($v['s'], 'background-color'), );
				$rules['background-image']=
				$this->form->structure['background-image']=array('type'=>'file', 'accept'=>'image.*', 'name_file'=>$k.'.background.{$ext}', 'add_fn'=>true, 'txt'=>'', );
				if ($val=$css->Get($v['s'], 'background-image')) {
					$val=$css->rule_set_dir($css->rule_get_value_url($val), $v['dir']);
					$this->form->structure['background-image']['name_file']=basename($val);
					$cl=array('s'=>$this->media_size($val, 500, 150), );
					$cl['h']=($cl['s']['h']>=20) ? $cl['s']['h']:20;
					$cl['w']=($cl['s']['w']>=20) ? $cl['s']['w']:20;
					$this->form->structure['background-image']['txt'].=
					'<p><a id="background-image_pic" href="'.$val.'" target="_blank"><img src="'.axs_dir('content', 'http').$val.'" width="'.$cl['w'].'" height="'.$cl['h'].'" alt="'.$val.'" /></a></p>';
					}
				$rules['background-repeat']=$this->form->structure['background-repeat']=array('type'=>'select', 'value'=>$css->Get($v['s'], 'background-repeat'), 'options'=>array(
						''=>array('value'=>'', 'label'=>''),
						'repeat'=>array('value'=>'repeat', 'label'=>$this->tr('settings.theme.background-repeat-both.lbl')),
						'repeat-x'=>array('value'=>'repeat-x', 'label'=>$this->tr('settings.theme.background-repeat-x.lbl')),
						'repeat-y'=>array('value'=>'repeat-y', 'label'=>$this->tr('settings.theme.background-repeat-y.lbl')),
						'no-repeat'=>array('value'=>'no-repeat', 'label'=>$this->tr('settings.theme.background-repeat-.lbl')),
						),
					);
				$val=explode(' ', $css->Get($v['s'], 'background-position'));
				foreach (array('x'=>0,'y'=>1) as $kk=>$vv) {
					$val[$kk]=trim(axs_get($vv, $val));
					$val[$kk.'-nr']='';
					foreach ($css->units as $vvv) if (preg_match('/[0-9]'.$vvv.'$/', $val[$kk])) {
						$val[$kk.'-nr']=$val[$kk];	break;
						}
					}
				$rules['background-position']=array('xy'=>true, );
				$this->form->structure['background-position-x']=array('type'=>'select', 'value'=>$val['x'], 'options'=>array(
						''=>array('value'=>'', 'label'=>''),
						'left'=>array('value'=>'left', 'label'=>$this->tr('settings.theme.background-position_left.lbl')),
						'center'=>array('value'=>'center', 'label'=>$this->tr('settings.theme.background-position-center.lbl')),
						'right'=>array('value'=>'right', 'label'=>$this->tr('settings.theme.background-position-right.lbl')),
						),
					);
				$this->form->structure['background-position-x-nr']=array('type'=>'text', 'size'=>5, 'value'=>$val['x-nr'], 'txt'=>$unit, );
				$this->form->structure['background-position-y']=array('type'=>'select', 'value'=>$val['y'], 'options'=>array(
						''=>array('value'=>'', 'label'=>''),
						'top'=>array('value'=>'top', 'label'=>$this->tr('settings.theme.background-position_top.lbl')),
						'center'=>array('value'=>'center', 'label'=>$this->tr('settings.theme.background-position-center.lbl')),
						'bottom'=>array('value'=>'bottom', 'label'=>$this->tr('settings.theme.background-position-bottom.lbl')),
						),
					);
				$this->form->structure['background-position-y-nr']=array('type'=>'text', 'size'=>5, 'value'=>$val['y-nr'], 'txt'=>$unit, );
				$rules['background-size']=array('xy'=>true, );
				$this->form->structure['background-size']=array('type'=>'radio', 'value'=>$css->Get($v['s'], 'background-size'), 'options'=>array(
						''=>array('value'=>'', 'label'=>' - '),
						'cover'=>array('value'=>'cover', 'label'=>$this->tr('settings.theme.background-size-cover.lbl')),
						'contain'=>array('value'=>'contain', 'label'=>$this->tr('settings.theme.background-size-contain.lbl')),
						),
					);
				$val=explode(' ', $css->Get($v['s'], 'background-size'));
				foreach (array('x'=>0,'y'=>1) as $kk=>$vv) {
					$val[$kk]=trim(axs_get($vv, $val));
					$vv='';
					foreach ($css->units as $vvv) if (preg_match('/[0-9]'.$vvv.'$/', $val[$kk])) {	$vv=$val[$kk];	break;	}
					$this->form->structure['background-size-'.$kk]=array('type'=>'text', 'size'=>5, 'value'=>$vv, 'txt'=>$unit, );
					}
				$rules['background-attachment']=
				$this->form->structure['background-attachment']=array('type'=>'select', 'value'=>$css->Get($v['s'], 'background-attachment'), 'options'=>array(
						''=>array('value'=>'', 'label'=>''),
						'scroll'=>array('value'=>'scroll', 'label'=>$this->tr('settings.theme.background-scroll.lbl')),
						'fixed'=>array('value'=>'fixed', 'label'=>$this->tr('settings.theme.background-fixed.lbl')),
						),
					);
				$this->form->structure['background/']=array('type'=>'fieldset_end', );
				#<Font />
				$this->form->structure['font']=array('type'=>'fieldset', );
				$rules['font-family']=
				$this->form->structure['font-family']=array(
					'type'=>'select', 'value'=>$css->Get($v['s'], 'font-family'), 'options'=>array(''=>array('value'=>'', 'label'=>''), ),
					);
				foreach ($webfonts as $kk=>$vv) $this->form->structure['font-family']['options'][]=array(
					'value'=>$vv, 'label'=>$vv, 'style'=>'font-family:'.htmlspecialchars($vv).';',
					);
				$rules['font-size']=$this->form->structure['font-size']=array('type'=>'text', 'size'=>5, 'value'=>$css->Get($v['s'], 'font-size'), 'txt'=>$unit, );
				$rules['color']=
				$this->form->structure['color']=array('type'=>'color', 'size'=>8, 'value'=>$css->Get($v['s'], 'color'), );
				$this->form->structure['font/']=array('type'=>'fieldset_end', );
				$this->form->structure['axs_save']=$form_save_btn;
				if (isset($_POST['axs_save'])) { #<Save>
					$this->form->fs_init();
					$data=$css->GetSection($s=$selectors[$selector]['s']);
					foreach ($rules as $kk=>$vv) {
						$r=axs_get($kk, $this->form->user_input);
						if (!empty($vv['xy'])) {
							//$r='';
							foreach (array('x', 'y') as $vvv) {
								$vv[$vvv]=axs_get($kk.'-'.$vvv, $this->form->user_input);
								if (strlen($tmp=axs_get($kk.'-'.$vvv.'-nr', $this->form->user_input))) {
									$vv[$vvv]=$tmp;
									//$r='';
									}
								}
							if (strlen($vv['x'].$vv['y'])) $r=$vv['x'].' '.$vv['y'];
							}
						if ($vv['type']==='file') {
							$r=$data[$kk];
							if (!empty($this->form->user_input[$kk]['_delete'])) {
								$r='';
								foreach (axs_filesystem::$ftypes['image.*'] as $vvv) $this->form->fs->f_del($css->selector_f_name($s).'.background.'.$vvv, false);
								}
							$f=false;
							if (!empty($_FILES[$kk]['name'])) $f=$_FILES[$kk]['name'];
							if (!empty($_POST[$kk]['name'])) $f=$_POST[$kk]['name'];
							if ($f) {
								$this->form->fs->f_upl($kk, $tmp=$css->selector_f_name($s).'.background.'.$this->form->fs->f_ext($f));
								$r='url('.$tmp.')';
								}
							}
						if (strlen($r)) $data[$kk]=$r;
						else unset($data[$kk], $css->css[$s][$kk]);
						}
					foreach ($data as $kk=>$vv) $data[$kk]='	'.$kk.':'.$vv.';'."\n";
					if (!empty($data)) $css->Add($s, implode('', $data));
					else unset($css->css[$s]);
					//$this->content->site_cfg_update(array('style'=>$css->GetCSS()));
					$tmp=$css->GetCSS();
					if ($tmp) $this->form->fs->f_save('index.css', $tmp);
					else $this->form->fs->del('index.css', false);
					exit(axs_redir('?'.axs_url($this->url, array('edit'=>false, ), false).'#'.$s));
					} #</Save>
				}
			$this->form->structure[$k]['label.html']='<a href="'.'?'.axs_url($this->url, $v['url']).'#'.$k.'">'.htmlspecialchars($v['s']).'</a>';
			$this->form->structure[$k.'/']=array('type'=>'fieldset_end', );
			}
		$this->form->structure['css/']=array('type'=>'fieldset_end', );
		$this->form->structure['axs_save']=$form_save_btn;
		foreach ($this->form->structure as $k=>$v) if (!isset($v['label'])) {
			$this->form->structure[$k]['label']=$this->tr('settings.theme.'.$k.'.lbl');
			}
		break; #</case 'theme'>
	case 'code':
		$this->form->structure=array(
			'code_head'=>array('type'=>'textarea', 'size'=>75, 'rows'=>10, 'label'=>$this->tr->t('settings.code.code_head.lbl'), 'comment'=>' (...</head><body>)', 'value'=>$this->content->site_cfg['code_head'], ),
			'code_bottom'=>array('type'=>'textarea', 'size'=>75, 'rows'=>10, 'label'=>$this->tr->t('settings.code.code_bottom.lbl'), 'comment'=>' (...</body></html>)', 'value'=>$this->content->site_cfg['code_bottom'], ),
			'axs_save'=>$form_save_btn,
			);
		if (isset($_POST['axs_save'])) {
			$this->content->site_cfg_update(array('code_head'=>$_POST['code_head'], 'code_bottom'=>$_POST['code_bottom'], ));
			}
		break; #</case 'code'>
	case 'tr':
	case '':
	default:
		$this->form->structure['title_site']=array('type'=>'fieldset', 'label'=>$this->tr('settings.tr.title_site.lbl'), );
		foreach ($this->content->site['langs'] as $k=>$v) {
			$v['value']=$this->content->site_title_get($k);
			$this->form->structure[$k]=array('type'=>'text', 'size'=>75, 'maxlength'=>255, 'label'=>$v['l'], 'value'=>$v['value'], );
			}
		$this->form->structure['title_end']=array('type'=>'fieldset_end', );
		$this->form->structure['axs_save']=$form_save_btn;
		if (isset($_POST['axs_save'])) {
			foreach ($this->content->site['langs'] as $k=>$v) $this->content->site_title_update(axs_html_safe($_POST[$k]), $k);
			}
		break;
	} #</switch($s2)>
$this->form->structure_set($this->form->structure);
$this->form->values=$this->form->form_input();
$form['class']=$s2;
$form['action']='?'.axs_url($this->url, array('axs'=>array($this->form->key_form_id=>$this->form->form_id)));
$form['msg']=$this->form->msg_html();
foreach ($this->form->structure_get() as $k=>$v) $form['elements'].=$this->form->element_html($k, $v, $this->form->values);
$vr['content']=axs_tpl_parse($this->templates['form'], $form);
return axs_tpl_parse($this->templates['content'], $vr);
#2011-05-28 ?>