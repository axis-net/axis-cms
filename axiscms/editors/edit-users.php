<?php #2022-05-03
if (!defined('AXS_PATH_CMS')) exit(require('log.php')); # Prevent direct access

$this->tr_load(axs_tr::$lang, preg_replace('/\.php$/', '', basename(__FILE__)));
$axs['page']['head']['style_content']='<link href="'.AXS_PATH_CMS_HTTP.'index.content'.'.css" rel="stylesheet" type="text/css" media="screen" />'."\n";
$axs['page']['head']['edit-system']='<link href="'.AXS_PATH_CMS_HTTP.'edit-users'.'.css" rel="stylesheet" type="text/css" media="screen" />'."\n";

if ($axs['e']==='user') {
	if ($tmp=axs_admin::no_permission(array('dev'=>'', 'admin'=>'', 'cms'=>'', ), false, __FILE__, __LINE__)) return $tmp; #<Permission check />
	$data=new axs_users_edit('', $this->tr, 'auto', array('data'=>''));
	return $data->editor(axs_user::get('user'), axs_user::get('id'), $data->form->user_input);
	}

if (isset($_GET['s1'])) $this->url['s1']=urlencode($_GET['s1']);
require_once('edit-system.class.php');
$o=new axs_edit_system($this);
$this->tpl_set($this->name, 
'		<nav id="menu-tabs" class="axs-ui-menu tabs">'."\n".
'			<h2 class="title visuallyhidden">'.htmlspecialchars($axs['editor']->breadcrumb['users']['label']).'</h2>'."\n".
'{$menu}'."\n".
'		<script>axs.menu.attach("menu-tabs");</script>'."\n".
'		</nav>'."\n".
'		<div class="tab">'."\n".
'{$content}'."\n".
'		</div>'."\n");
$this->vars['menu']=array(''=>'users', 'groups'=>'groups', );
$tmp=axs_get('s2', $_GET);
$o->url['s2']=(isset($this->vars['menu'][$tmp])) ? urlencode($tmp):key($this->vars['menu']);
foreach ($this->vars['menu'] as $k=>$v) $this->vars['menu'][$k]=array(
	'url'=>'?'.axs_url($o->url, array('s2'=>$k)),
	'label'=>$this->tr('title.'.$v.'.lbl'),
	'act'=>$o->url['s2'],
	);
$axs['page']['title']=array_merge(array(0=>$this->vars['menu'][$o->url['s2']]['label']), $axs['page']['title']);
$axs['editor']->breadcrumb[$o->url['s2']]=array_merge($this->vars['menu'][$o->url['s2']], array('submenu'=>$this->vars['menu']));
$this->vars['menu']=$this->menu_build($this->vars['menu'], '     ', array('class'=>'menu'));
$this->vr=array();
switch ($o->url['s2']) {
	case 'groups':
		if ($tmp=axs_admin::no_permission(array('dev'=>'', 'admin'=>'', ))) return $tmp; #<Permission check />
		$this->vars['content']='';
		foreach (array('groups','roles') as $v) $this->vars['content'].=$o->ui_groups($v);
		break;
	default:
		if ($tmp=axs_admin::no_permission(array('dev'=>'', 'admin'=>'', 'cms'=>'', ), false, __FILE__, __LINE__)) return $tmp; #<Permission check />
		if (isset($_GET['ajax'])) {
			require_once('edit-files.php');
			foreach (array('h','site','value','ajax') as $k=>$v) $o->url[$v]=urlencode(axs_get($v, $_GET));
			$o=new axs_filemanager(axs_get('site', $_GET), '', $_GET['value'], $o->url, '');
			$o->ajax_tree();
			}
		$get=$_GET;
		#<Set data source>
		$db_set=$axs['cfg']['db'][1]['type'];
		if ($db_set) {	$db=$get['db']=(strlen(axs_get('db', $_GET))) ? $_GET['db']+0:1;	}
		else $db=$get['db']=0;
		if (isset($_GET['db'])) $o->url['db']=$_GET['db'];
		#</Set data source>
		$fn=new axs_users_edit($db, $this->tr, 'auto');
		$fn->structure['pass']['required']=$fn->structure['pass']['min']=0;
		$fn->url=$o->url;
		if ((isset($_GET['u'])) or (isset($_GET['id']))) {
			foreach (array('u','id') as $k=>$v) $fn->url[$v]=axs_get($v, $_GET);
			return $fn->editor(axs_get('u', $_GET), axs_get('id', $_GET), $_POST, '?'.axs_url($o->url, array(), false));
			}
		else { # <List users>
			$this->tpl_set($this->name.'_users_list',
			'     {$search}'."\n".
			'     <table id="users" class="table selectable">'."\n".
			'     <caption id="found">{$title.users.lbl} <strong>({$total})</strong> <a href="{$user.add_url}">{$user.add.lbl}</a></caption>'."\n".
			'     <tr>{$header}</tr>'."\n".
			'{$rows}      </table>'."\n".
			'			<div class="pager">'."\n".
			'				<p class="found">{$search.found.lbl}<strong>{$total}</strong> <span class="prev-next">{$prev}{$current}{$next}</span></p>'."\n".
			'				<p class="pages">{$pages}</p>'."\n".
			'			</div>'."\n");
			#<Search form>
			$form=array(
				'db'=>array('type'=>'select', 'search'=>1, 'lang'=>'en', 'options'=>array(
					0=>array('value'=>0, 'label'=>'filesystem'), 1=>array('value'=>1, 'label'=>'SQL'), ),
					),
				'user'=>array('type'=>'text', 'search'=>4, 'size'=>10, ),
				'status'=>array('type'=>'select', 'search'=>1, 'options'=>array(
						''=>array('value'=>'', 'label'=>''),
						0=>array('value'=>'0', 'label'=>$this->tr('user.status0.lbl')),
						1=>array('value'=>'1', 'label'=>$this->tr('user.status1.lbl')),
						),
					),
				'lock'=>array('type'=>'select', 'search'=>1, 'options'=>array(
						''=>array('value'=>'', 'label'=>''),
						1=>array('value'=>'1', 'label'=>'+'),
						0=>array('value'=>'0', 'label'=>'-'),
						),
					),
				'g'=>array('type'=>'multi-checkbox', 'search'=>1, 'options'=>array(), ),
				);
			foreach ($axs['cfg']['groups'] as $k=>$v) $form['g']['options'][$k]=array('value'=>$k, 'label'=>$v);
			foreach ($form as $k=>$v) $form[$k]['label']=$this->tr('user.'.$k.'.lbl');
			$this->tr->tr['search_lbl']='>';
			$search=new axs_form_search($form, $get, $o->url, false);
			if (!$search->vl['db']) foreach (array('status','user') as $v) $search->structure[$v]['disabled']='disabled';
			if (!$axs['cfg']['db'][1]['type']) $search->structure['db']['options'][1]['disabled']='disabled';
			$search->elements();
			$this->vr['search']=$search->form(true, $this->tr->tr);
			#</Search form>
			$this->vr['user.add_url']='?'.axs_url($o->url, array('u'=>'', 'b'=>'?'.$_SERVER['QUERY_STRING']));
			$this->vr['header']=$this->vr['rows']='';
			
			foreach ($form as $k=>$v) {
				if (strlen(axs_get($k, $search->url))) $o->url[$k]=$search->url[$k];
				}
			unset($search->sql['db']);
			//exit(dbg($search->sql));
			$fn->fields=array('status'=>'')+$fn->form->structure_get_thead();
			foreach ($fn->fields as $k=>$v) if ($k) $this->vr['header'].='<th scope="col" class="'.$k.'">'.$this->tr('user.'.$k.'.lbl').'</th>';
			$users=$fn->list_get($search->pager->p, $search);
			$this->vr['total']=$users['total'];
			foreach ($users['list'] as $cl) {
				$this->vr['rows'].='     <tr id="u_'.$cl['user'].'" class="top status'.$cl['status'].(($cl['lock'])?' disabled':'').'">';
				$cl['lock']=($cl['lock']) ? '+':'-';
				$cl['status']=(strlen($cl['status'])) ? $this->tr('user.status'.$cl['status'].'.lbl'):'?';
				$cl['user']='<a href="'.'?'.axs_url($o->url, array('u'=>$cl['user'], 'id'=>$cl['id'], 'b'=>'?'.$_SERVER['QUERY_STRING'].'#u_'.$cl['user'])).'">'.(($cl['user'])?$cl['user']:'-').'</a>';
				$cl['homedirs']=preg_split("/, \n/", $cl['homedirs'], -1, PREG_SPLIT_NO_EMPTY);
				foreach ($cl['homedirs'] as $k=>$v) $cl['homedirs'][$k]='<li>'.axs_html_safe($v).'</li>';
				$cl['homedirs']=($cl['homedirs']) ? '<ul>'.implode('', $cl['homedirs']).'</ul>':'';
				foreach ($fn->fields as $k=>$v) if ($k) $this->vr['rows'].='<td class="'.$k.'">'.$cl[$k].'</td>';
				$this->vr['rows'].='</tr>'."\n";
				}
			# <Pager />
			if ($users['total']) {
				$p=new axs_pager($fn->p, array($fn->psize), $fn->psize);
				$this->vr+=$p->pages($users['total'], '?'.axs_url($o->url, array('p'=>''))); 
				}
			else $this->vr['prev']=$this->vr['current']=$this->vr['next']=$this->vr['pages']='';
			$this->vars['content']=axs_tpl_parse($this->templates[$this->name.'_users_list'], $this->vr);
			break;
		} #</switch ($o->url['s2'])>
	} # </List users>
return axs_tpl_parse($this->templates[$this->name], $this->vars+$this->tr->tr);
#2021-01-07 ?>