<?php #2020-02-15 utf-8
defined('AXS_PATH_CMS') or exit(require('log.php')); # Prevent direct access

class axs_edit_form extends axs_form_edit {
	public $form_id='form-editor';
	var $key_add='content_form_add';
	var $key_del='content_form_del';
	var $key_edit='content_form_element_edit';
	var $key_edit_add='content_form_element_add';
	var $key_save='axs_save_form';
	var $form_def=array(
		''=>array(
			'mailto'=>'',
			'mail_name_field'=>'name',
			'posts_save'=>'',
			'posts_show'=>'',
			'tr'=>array(
				'msg'=>array('et'=>'Teade saadetud! Teate kättesaamise kinnituseks saadetakse koopia Teie e-posti aadressile.', 'en'=>'Your message has been sent! To confirm the delivery of the message a copy will be sent to Your e-mail address.', ),
				'msg_value_required'=>array('en'=>'Input required!', 'et'=>'Välja täitmine on kohustuslik!', ),
				'msg_value_invalid'=>array('en'=>'Invalid value!', 'et'=>'Sisestatud väärtus ei ole sobiv!', ),
				'msg_value_unique'=>array('en'=>'Value must be unique!', 'et'=>'Väärtus peab olema kordumatu!', ),
				),
			'locked'=>false,
			),
		'name'=>array(
			'type'=>'text',
			'label'=>array('en'=>'name', 'et'=>'nimi', ),
			'value'=>array(''=>'', ),
			'size'=>50,
			'required'=>1,
			),
		'email'=>array(
			'type'=>'email',
			'label'=>array('en'=>'e-mail', 'et'=>'e-post', ),
			'size'=>50,
			'required'=>1,
			),
		'message'=>array(
			'type'=>'textarea',
			'label'=>array('en'=>'message', 'et'=>'teade', ),
			'required'=>1,
			'size'=>40,
			'rows'=>5,
			'required'=>1,
			),
		/*'captcha'=>array(
			'type'=>'text-captcha',
			'size'=>5,
			'label'=>array('en'=>'captcha', 'et'=>'kontrollkood', ),
			'msg'=>array('en'=>'captcha characters do not match!', 'et'=>'kontrollkood ei klapi!', ),
			'required'=>1,
			),*/
		'post'=>array(
			'type'=>'submit',
			'label'=>array('en'=>'send', 'et'=>'saada', ),
			),
		);
	function __construct(&$e) {
		global $axs, $axs_content;
		$this->e=&$e;
		axs_form::__construct($this->e->site_nr, $this->e->content->url, false, $axs_content['content'][$axs['c']]['dir'], $_POST);
		$this->css_js();
		//$this->url=$this->e->content->url;
		$this->tr=new axs_tr(false, false, $axs['cfg']['cms_lang'], false, 'edit-form');
		$this->tpl=array(
			'form'=>
				'      <form id="'.$this->form_id.'" class="form" method="post" action="{$formact_url}#'.$this->form_id.'" enctype="multipart/form-data">'."\n".
				'{$msg}{$elements}'.
				'      </form>'."\n".
				'      <script>axs.form.init("'.$this->form_id.'");</script>',
			'element_add'=>'       <div class="element_add"><a href="{$add_url}">{$element-add_lbl}</a></div>'."\n",
			'element_edit'=>'       <a class="edit_element" href="{$edit_url}#'.$this->form_id.'">{$element-edit_lbl} "{$edit_label}"</a>'."\n",
			);
		$this->back_link='       <div><a href="'.'?'.axs_url($this->url, array($this->key_edit=>false, $this->key_edit_add=>false)).'#'.$this->form_id.'">&lt;'.$this->tr->s('back_lbl').'</a></div>'."\n";
		//$this->required=axs_tpl_parse($this->templates('required'), array('input_required_lbl'=>$this->tr->s('element-required_lbl')));
		$this->content_form=(isset($axs_content['content'][$axs['c']]['form'])) ? $axs_content['content'][$axs['c']]['form']:array();
		$nr=1;
		foreach ($this->content_form as $k=>$v) if ($k) {	$this->content_form[$k]['nr']=$nr;	$nr++;	}
		} #</__construct()>
	//function tr($key, $vars=false) {	return axs_admin::tr($key, $vars);	}
	//function br2nl($str) {	return preg_replace('#<br />|<br/>|<br>#i', "\n", $str);	}
	function data_get($n=false) {
		if (!$this->content_form) return false;
		if ($n===false) {
			$vl=$this->content_form;
			unset($vl['']);
			}
		else {
			if (!isset($this->content_form[$n])) axs_log(__FILE__, __LINE__, 'edit-form', 'Invalid ID "'.$n.'"', true);
			$vl=$this->content_form[$n];
			}
		return $vl;
		} #</data_get()>
	function ui() {
		global $axs, $axs_content;
		if (isset($_GET[$this->key_edit])) {
			if ($_GET[$this->key_edit]==='') return $this->ui_edit_config();
			else {
				if (isset($_GET[$this->key_edit_add])) return $this->ui_edit_element_add($_GET[$this->key_edit_add]);
				else return $this->ui_edit_element($_GET[$this->key_edit]);
				}
			}
		if (isset($_POST[$this->key_save])) {
			if (isset($_GET[$this->key_add])) $this->save_add();
			if (isset($_POST[$this->key_del])) $this->save_del();
			}
		$this->site_form=new axs_form_site($axs_content, $this->content_form, $_POST);
		$vr=array('elements'=>'       <a class="edit_element" href="{$config_url}#'.$this->form_id.'">{$form-config_lbl}</a>'."\n", );
		$form=$this->data_get();
		$cfg=$this->data_get('');
		if (!is_array($form)) {
			$this->url[$this->key_add]='';
			$vr['elements']='       <div class="container content_form_add"><input id="'.$this->key_add.'" name="'.$this->key_save.'" type="submit" value="{$form-add_lbl}" /></div>'."\n";
			}
		else {
			if ((!empty($form)) && ($this->site_form->disabled)) $this->msg('msg_config');
			foreach ($this->site_form->structure_get() as $k=>$v) {
				//$this->site_form->element($k, $v);
				if ($this->site_form->disabled) $v['disabled']='disabled';
				$v['edit_label']=axs_get('label', $v, '');
				//$v['label']=$v['nr'].'. '.$v['edit_label'];
				//$v['input']=$this->site_form->element_input_html($k, $v);
				$v['edit_url']='?'.axs_url($this->url, array($this->key_edit=>$k));
				$v['edit']=$this->tpl['element_edit'];
				$v['add_url']='?'.axs_url($this->url, array($this->key_edit=>$v['nr'], $this->key_edit_add=>$v['nr']));
				$vr['elements'].=axs_tpl_parse($this->tpl['element_add'].$v['edit'], $this->site_form->_to_string($v));
				//$vr['elements'].=axs_tpl_parse($this->site_form->templates($v), $v);
				$vr['elements'].=$this->site_form->element_html($k, $v);
				}
			if (!isset($v)) $v=array('nr'=>0, );
			$v['add_url']='?'.axs_url($this->url, array($this->key_edit=>$v['nr']+1, $this->key_edit_add=>$v['nr']+1));
			$vr['elements'].=axs_tpl_parse($this->tpl['element_add'], $v);
			if (empty($cfg['locked'])) $vr['elements'].='       <label class="del"><input id="'.$this->key_del.'" name="'.$this->key_del.'" type="checkbox" value="1" />{$form-del_confirm_lbl}</label>'."\n".
			'       <input id="'.$this->key_save.'" name="'.$this->key_save.'" type="submit" value="{$form-del_lbl}" class="del" />'."\n";
			}
		$vr['formact_url']='?'.axs_url($this->url);
		$vr['config_url']='?'.axs_url($this->url, array($this->key_edit=>'', 'axs'=>array($this->key_form_id=>$this->form_id, )));
		$vr['msg']=$this->msg_html();
		return axs_tpl_parse($this->tpl['form'], $vr+$this->tr->tr);
		} #</ui()>
	function ui_edit_config() {
		global $axs, $axs_content;
		#<Structure>
		#	<default>
		$structure_def=array(
			'mailto'=>array('type'=>'email', 'size'=>50, 'multiple'=>'multiple', ),
			'mail_name_field'=>array('type'=>'select', 'options'=>array(''=>array('value'=>'', 'label'=>'')), ),
			'posts_save'=>array('type'=>'select', 'options'=>array(
				''=>array('value'=>'', 'label'=>''),
				5=>array('value'=>5, 'label'=>'5'),
				10=>array('value'=>10, 'label'=>'10'),
				25=>array('value'=>25, 'label'=>'25'),
				50=>array('value'=>50, 'label'=>'50'),
				), 'value_type'=>'i', ),
			'posts_show'=>array('type'=>'select', 'options'=>array(
				''=>array('value'=>'', 'label'=>'-', ),
				'asc'=>array('value'=>'asc', 'label'=>$this->tr->s('config.asc_lbl'), ),
				'desc'=>array('value'=>'desc', 'label'=>$this->tr->s('config.desc_lbl'), ),
				), ),
			'locked'=>array('type'=>'checkbox', 'value'=>'1', 'disabled'=>'disabled', ),
			);
		foreach ($this->form_def['']['tr'] as $k=>$v) $structure_def['tr'][$k]=(isset($v[$axs_content['l']])) ? $v[$axs_content['l']]:current($v);
		#	</default>
		# Config structure comes from $plugin_def or system default (self::$form_def). 
		$form=$this->e->content->plugin_form_get($axs_content['content'][$axs['c']]['plugin']);
		$this->structure=($form) ? $form['']:array();
		if (!$this->structure) $this->structure=$structure_def;
		$tr=(isset($this->structure['tr'])) ? $this->structure['tr']:null;
		unset($this->structure['tr']);
		dbg($this->structure);
		foreach ($this->structure as $k=>$v) {
			if (!is_array($v)) $v=array('value'=>$v);
			if (isset($structure_def[$k])) $v=array_merge($structure_def[$k], $v);
			if (!isset($v['type'])) $v=array_merge(array('type'=>'text', 'size'=>50, ), $v);
			if (!isset($v['label'])) {
				if (isset($this->e->plugin_editor['tr']['config.'.$k.'_lbl'])) $v['label']=$this->e->plugin_editor['tr']['config.'.$k.'_lbl'];
				else $v['label']=$this->tr->s('config.'.$k.'_lbl');
				}
			$this->structure[$k]=$v;
			}
		if (isset($this->structure['mail_name_field'])) foreach ($this->e->content->content_form_get($axs['c']) as $k=>$v) if (($k) && ($v['type']==='text') && (empty($v['disabled']))) {
			$this->structure['mail_name_field']['options'][$k]=array('value'=>$k, 'label'=>$v['label']);
			if (!isset($v['label'])) dbg($v);
			}
		#	<tr>
		if ($tr!==null) {
			if (count($form)>1) $tr=array_merge($this->form_def['']['tr'], $tr); # Merge default $tr only if there is form, but not when it's only config. 
			$this->structure['tr_fieldset']=array('type'=>'fieldset', 'label'=>$this->tr->s('config.tr_lbl'), );
			foreach($tr as $k=>$v) $this->structure['tr_'.$k]=array('type'=>'textarea', 'size'=>50, 'rows'=>2, 'label'=>$k, 'required'=>true, );
			$this->structure['tr_fieldset_end']=array('type'=>'fieldset_end');
			}
		#	</tr>
		$this->structure[$this->key_save]=array('type'=>'submit', 'label'=>$this->tr->s('save_lbl'), );
		$this->structure_set($this->structure);
		#</Structure>
		# <Values />
		$f=$this->data_get();
		$cfg=$this->data_get('');
		if (empty($_POST)) {
			$vl=$cfg;
			if (isset($cfg['tr'])) foreach($cfg['tr'] as $k=>$v) $vl['tr_'.$k]=$vl['tr'][$k];
			}
		else $vl=$this->form_input();
		#<Validate and save />
		if ($this->save) {
			//foreach($cfg['tr'] as $k=>$v) $vl['tr'][$k]=str_replace(array("\n","\r"), '', nl2br($vl['tr_'.$k]));
			$this->content_form['']['locked']=($cfg['locked']) ? true:false;
			unset($vl['locked']);
			$this->validate($vl);
			if (empty($this->msg)) {
				foreach ($tr as $k=>$v) {
					$vl['tr'][$k]=$vl['tr_'.$k];
					unset($vl['tr_'.$k]);
					}
				unset($vl['tr_fieldset'], $vl['tr_fieldset_end']);
				$this->save_edit('', $vl);
				}
			}
		#<Build HTML form />
		if (!$axs['mail()']) {
			#$this->structure['mailto']['readonly']='readonly';
			$this->msg('config.msg_mailto');
			}
		$vr=array('elements'=>'', );
		$vr['elements'].=$this->back_link;
		dbg($this->structure_get());
		foreach ($this->structure_get() as $k=>$v) {
			//$this->element($k, $v);
			//$v['input']=$this->element_input_html($k, $v, $vl);
			$vr['elements'].=$this->element_html($k, $v, $vl);//axs_tpl_parse($this->templates($v), $v);
			}
		$vr['formact_url']='?'.axs_url($this->url, array($this->key_edit=>'', 'axs'=>array($this->key_form_id=>$this->form_id, )));
		$vr['msg']=$this->msg_html();
		return axs_tpl_parse($this->tpl['form'], $vr);
		} #</ui_edit_config()>
	function ui_edit_element($n) {
		global $axs, $axs_content;
		# <Structure />
		$this->structure=array(
			'id'=>array('type'=>'text', 'size'=>50, 'required'=>1, 'readonly'=>'readonly', ),
			'type'=>array('type'=>'text', 'size'=>50, 'required'=>1, 'readonly'=>'readonly', ),
			'nr'=>array('type'=>'number', 'size'=>3, 'required'=>0, ),
			'label'=>array('type'=>'textarea', 'size'=>50, 'rows'=>2, 'required'=>1, ),
			'placeholder'=>array('type'=>'checkbox', 'size'=>1, 'value'=>1, ),
			'comment'=>array('type'=>'textarea', 'size'=>50, 'rows'=>2, 'required'=>0, ),
			'msg'=>array('type'=>'textarea', 'size'=>50, 'rows'=>2, 'required'=>0, ),
			'required'=>array('type'=>'checkbox', 'value'=>'1', ),
			'disabled'=>array('type'=>'checkbox', 'value'=>'disabled', ),
			'options'=>array('type'=>'table', 'options'=>array(
				'nr'=>array('type'=>'number', 'label'=>$this->tr->s('element-options_nr_lbl')),
				'label'=>array('type'=>'text', 'label'=>$this->tr->s('element-options_label_lbl')),
				'size'=>array('type'=>'number', 'label'=>$this->tr->s('element-options_size_lbl')),
				'_add'=>array('type'=>'submit', 'label'=>$this->tr->s('element-options_add_lbl')),
				'_del'=>array('type'=>'checkbox', 'label'=>$this->tr->s('element-options_del_lbl')),
				), 'value'=>'1', ),
			'size'=>array('type'=>'text', 'size'=>3, 'required'=>0, ),
			'maxlength'=>array('type'=>'text', 'size'=>3, 'required'=>0, ),
			'txt'=>array('type'=>'wysiwyg', 'size'=>50, 'rows'=>5, 'required'=>0, 'url'=>array('dir'=>urlencode($this->e->content->dir_filemanager)), ),
			'delete'=>array('type'=>'checkbox', 'value'=>'true', ),
			$this->key_save=>array('type'=>'submit', 'label'=>$this->tr->s('save_lbl'), /*'add_fn'=>true,*/ ),
			);
		foreach ($this->structure as $k=>$v) if (!isset($this->structure[$k]['label'])) $this->structure[$k]['label']=$this->tr->s('element-'.$k.'_lbl');
		$this->structure_set($this->structure);
		# <Values />
		$vl=($this->submit) ? $this->form_input():$this->data_get($n);
		//dbg($this->content_form[$n]['type']);
		//if ($this->content_form[$n]['type']==='radio') exit(dbg($this->types[$this->content_form[$n]['type']]));
		if (!empty($this->types[$this->content_form[$n]['type']]['options'])) {
			$nr=1;
			if (!empty($_POST)) {
				foreach ((array)$vl['options'] as $k=>$v) { #<Add/remove options />
					if (isset($this->content_form[$n]['options'][$k])) $this->content_form[$n]['options'][$k]['nr']=$v['nr'];
					if (isset($_POST['options'][$k]['del'])) unset($vl['options'][$k]);
					}
				if (isset($_POST['options_add'])) foreach ($_POST['options_add'] as $k=>$v) {
					if (isset($vl['options'][$k])) $vl['options'][$k]['nr'].='.1';
					$vl['options']=axs_fn::array_el_add($vl['options'], 0, $k, array('value'=>$k, 'label'=>''));
					}
				#<Sort options />
				$vl['options']=axs_fn::array_sort($vl['options'], 'nr');
				$vl['options']=array_merge(array(0=>true), $vl['options']);
				unset($vl['options'][0]);
				foreach ($vl['options'] as $k=>$v) $vl['options'][$k]['value']=$k;
				$this->content_form[$n]['options']=axs_fn::array_sort($this->content_form[$n]['options'], 'nr');
				$this->content_form[$n]['options']=array_merge(array(0=>true), $this->content_form[$n]['options']);
				unset($this->content_form[$n]['options'][0]);
				}
			else foreach ((array)$vl['options'] as $k=>$v) {
				$vl['options'][$k]['nr']=$nr++;
				}
			foreach ((array)$vl['options'] as $k=>$v) {
				$v['lbl']=$this->structure['options']['options'];
				$this->structure['options']['value'].=
				'       <tr>'.
				'<td><label><span class="visuallyhidden">'.$v['lbl']['nr'].'</span><input name="options['.$k.'][nr]" type="text" size="2" value="'.axs_html_safe($v['nr']).'" /></label></td>'.
				//'<td><label><span class="visuallyhidden">'.$v['lbl']['name'].'</span><input name="options['.$k.'][name]" type="text" size="2" value="'.axs_html_safe($k).'" /></label></td>'.
				//'<td><label><span class="visuallyhidden">'.$v['lbl']['type'].'</span>'.axs_form::input('options['.$k.'][type]', array('type'=>'select', 'options'=> 'value'=>$v['type'], )).'</label></td>'.
				'<td><label><span class="visuallyhidden">'.$v['lbl']['label'].'</span><input name="options['.$k.'][label]" type="text" size="50" value="'.axs_html_safe($vl['options'][$k]['label']).'" /></label></td>'.
				'<td><label><span class="visuallyhidden">'.$v['lbl']['size'].'</span><input name="options['.$k.'][size]" type="text" size="2" value="'.intval($v['size']).'" /></label></td>'.
				'<td><input name="options_add['.($k+1).']" type="submit" value="'.axs_html_safe($this->tr->s('element-options_add_lbl')).'" /></td>'.
				'<td><label><span class="visuallyhidden">'.$v['lbl']['del'].'</span><input name="options['.$k.'][del]" type="checkbox" value="'.$k.'" /></label></td>'.
				'</tr>'."\n";
				}
			}
		else unset($this->structure['options']);
		#<Validate and save />
		if ($vl['nr']<1) $vl['nr']=1;
		if ($this->save) $this->validate($vl);
		if (($this->save) && (empty($this->msg))) {
			if ($vl['nr']) $this->content_form=axs_fn::array_el_move($this->content_form, $n, $vl['nr']+1);
			if (!empty($vl['delete'])) unset($this->content_form[$n]);
			$this->save_edit($n, $vl);
			}
		#<Build HTML form />
		$vl['id']=$n;
		$vr=array('elements'=>'', );
		$vr['elements'].=$this->back_link;
		foreach ($this->structure as $k=>$v) $vr['elements'].=$this->element_html($k, $v, $vl);
		$vr['formact_url']='?'.axs_url($this->url, array($this->key_edit=>$n, 'axs'=>array($this->key_form_id=>$this->form_id, )));
		$vr['msg']=$this->msg_html();
		return axs_tpl_parse($this->tpl['form'], $vr);
		} #</ui_edit_element()>
	function ui_edit_element_add($nr) {
		global $axs, $axs_content;
		# <Structure />
		$this->structure=array(
			'nr'=>array('type'=>'text', 'size'=>3, 'required'=>0, ),
			'label'=>array('type'=>'textarea', 'size'=>50, 'rows'=>2, 'required'=>1, ),
			'id'=>array('type'=>'text', 'size'=>50, 'required'=>0, ),
			'type'=>array('type'=>'table', 'required'=>0, 'value'=>array(), 'options'=>array(
					'type'=>array('type'=>'radio', 'required'=>0, 'label'=>$this->tr->s('element_add-type_type_lbl'), ),
					'sample'=>array('type'=>'content', 'label'=>$this->tr->s('element_add-type_sample_lbl'), ),
					),
				),
			$this->key_save=>array('type'=>'submit', 'label'=>$this->tr->s('save_lbl'), ),
			);
		foreach ($this->types as $k=>$v) {
			if (isset($v['content-form'])) {	if (!$v['content-form']) continue;	}
			$tr=$this->structure['type']['options'];
			$v['label']=$v['label.html']='label';
			$tr['type']='<label>'.$this->input_type_radio_html('', 'type', axs_get($k, $_POST, ''), array('value'=>$k, )).$k.'</label>';
			$this->element('sample_'.$k, $v);
			if ($k==='text-captcha') $v['disabled']='disabled';
			if (isset($v['options'])) foreach ($v['options'] as $kk=>$vv) $v['options'][$kk]['label.html']=axs_html_safe($vv['label']);
			//$v['input']=$this->element_input_html('sample_'.$k, $v);
			//$tr['sample']=axs_tpl_parse($this->templates($v), $v);
			$tr['sample']=$this->element_html('sample_'.$k, $v);
			if ($k==='fieldset') $tr['sample'].='</fieldset>';
			if ($k==='fieldset_end') $tr['sample']='<fieldset></fieldset>';
			foreach ($tr as $kk=>$vv) $tr[$kk]='<td class="'.$kk.'">'.$vv.'</td>';
			$this->structure['type']['value'][]='      <tr id="type_'.$k.'" lang="en">'.implode('', $tr).'</tr>';
			}
		$this->structure['type']['value']=implode("\n", $this->structure['type']['value']);
		foreach ($this->structure as $k=>$v) if (!isset($this->structure[$k]['label'])) $this->structure[$k]['label']=$this->tr->s('element_add-'.$k.'_lbl');
		$this->structure_set($this->structure);
		# <Values />
		$vl=$this->form_input();
		if (!strlen($vl['nr'])) $vl['nr']=$nr;
		#<Validate and save />
		if (isset($_POST[$this->key_save])) {
			if (!$vl['id']) $vl['id']=axs_valid::id($vl['label']);
			else {	if ($vl['id']!==axs_valid::id($vl['id'])) $this->msg('element_add-msg_id_invalid');	}
			if (isset($this->content_form[$vl['id']])) $this->msg('element_add-msg_id_exists');
			$vl['type']=(isset($this->types[$_POST['type']])) ? $_POST['type']:'text';
			if ($vl['nr']<1) $vl['nr']=1;
			$vl=array_merge($this->types[$vl['type']], $vl);
			$this->validate($vl);
			if (empty($this->msg)) {
				$this->content_form=axs_fn::array_el_add($this->content_form, $vl['nr']+1, $vl['id'], $vl);
				$this->save_edit($vl['id'], $vl);
				}
			}
		#<Build HTML form />
		$vr=array('elements'=>'', );
		$vr['elements'].=$this->back_link;
		foreach ($this->structure as $k=>$v) $vr['elements'].=$this->element_html($k, $v, $vl);
		$vr['formact_url']='?'.axs_url($this->url, array($this->key_edit=>$nr, $this->key_edit_add=>$nr, 'axs'=>array($this->key_form_id=>$this->form_id, )));
		$vr['msg']=$this->msg_html();
		return axs_tpl_parse($this->tpl['form'], $vr);
		} #</ui_edit_element_add()>
	function save_add() {
		global $axs, $axs_content;
		if ($this->content_form) return;
		$form=$this->e->content->plugin_form_get($axs_content['content'][$axs['c']]['plugin']);
		if (!$form) $form=$this->e->content->plugin_form_get_parse($this->form_def, $axs['l']);
		$this->e->content->save_form($form);
		exit(axs_redir('?'.axs_url($this->url, array($this->key_add=>false, ), false)));
		} #</save_add()>
	function save_del() {
		if ($this->cfg['locked']) return;
		$this->e->content->save_form_del();
		exit(axs_redir('?'.axs_url($this->url, array(), false)));
		} #</save_del()>
	function save_edit($n, $element) {
		if (!empty($this->msg)) return;
		if (isset($this->content_form[$n])) {
			//foreach ($this->content_form[$n] as $k=>$v) if (isset($element[$k])) $this->content_form[$n][$k]=$element[$k];
			foreach ($element as $k=>$v) {
				if (!is_array($v)) $this->content_form[$n][$k]=$v;
				else foreach ($v as $kk=>$vv) {
					if (!is_array($vv)) $this->content_form[$n][$k][$kk]=$vv;
					else foreach ($vv as $kkk=>$vvv) $this->content_form[$n][$k][$kk][$kkk]=$vvv;
					}
				}
			if ($n) $this->content_form[$n]['required']=intval(axs_get('required', $this->content_form[$n]));
			if (isset($this->content_form[$n]['size'])) $this->content_form[$n]['size']=intval($this->content_form[$n]['size']);
			if (isset($this->content_form[$n]['txt'])) $this->content_form[$n]['txt']=trim($this->content_form[$n]['txt']);
			if (isset($this->content_form[$n]['options'])) foreach ($this->content_form[$n]['options'] as $k=>$v) {
				if ($this->content_form[$n]['type']==='table') {	if (empty($v['type'])) $this->content_form[$n]['options'][$k]['type']='text';	}
				$this->content_form[$n]['options'][$k]['required']=intval($this->content_form[$n]['options'][$k]['required']);
				if (isset($this->content_form[$n]['options'][$k]['size'])) $this->content_form[$n]['options'][$k]['size']=intval($this->content_form[$n]['options'][$k]['size']);
				if (isset($this->content_form[$n]['options'][$k]['txt'])) $this->content_form[$n]['options'][$k]['txt']=trim($this->content_form[$n]['options'][$k]['txt']);
				}
			}
		foreach ($this->content_form as $k=>$v) {
			unset($this->content_form[$k]['nr'], $this->content_form[$k]['sql']);
			if (isset($v['options'])) foreach ($v['options'] as $kk=>$vv) unset($this->content_form[$k]['options'][$kk]['nr'], $this->content_form[$k]['options'][$kk]['sql']);
			}
		foreach (array('delete','id',$this->key_save,) as $v) unset($this->content_form[$n][$v]);
		foreach (array('disabled','maxlength') as $v) if (!$this->content_form[$n][$v]) unset($this->content_form[$n][$v]);
		
		$this->e->content->save_form($this->content_form);
		$a=($n) ? '#'.$n:'';
		exit(axs_redir('?'.axs_url($this->url, array($this->key_edit=>false, $this->key_edit_add=>false, ), false).$a));
		} #</save_edit()>
	} #</class::axs_form_edit>
$data=new axs_edit_form($this);
return $data->ui();
#2009 ?>