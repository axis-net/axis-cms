<?php #2022-05-10
class axs_filemanager extends axs_admin {
	function __construct($site_nr, $dir_user, $d, $url, $f='') {
		global $axs, $axs_user;
		$this->name='edit-files';
		$this->site_nr=$site_nr;
		$this->site=$axs['cfg']['site'][$this->site_nr];
		$this->d=axs_admin::f_secure($d);
		$this->dir=$this->site['dir_fs_root'].$this->site['dir'].$axs['dir_c'].$dir_user;
		$this->dir_http=$axs['http_root'].$this->site['dir'].$axs['dir_c'].$dir_user;
		$this->path=preg_split('~/~', '-/'.$this->d, -1, PREG_SPLIT_NO_EMPTY);
		unset($this->path[0]);
		$this->f=($f) ? (array)$f:array();
		$this->url_root='?';
		$this->url=$url;
		$this->url['dir']=$this->d;
		if (isset($_GET['browse'])) $this->url['browse']=$_GET['browse'];
		# <Redirect if another site>
		if ($this->site['domain']) {
			if (preg_replace('/^.*\/\/|\/.*$/', '', $this->site['domain'])!=$_SERVER['SERVER_NAME']) exit(axs_redir($this->site['domain'].preg_replace('/^.*\//', '', dirname($_SERVER['SCRIPT_NAME'])).'/'.'?'.$_SERVER['QUERY_STRING']));
			}
		if (isset($_POST['h'])) {
			$r=false;
			if ($_POST['h']!=$_GET['h']) {
				if (isset($axs_user['homedirs'][intval($_POST['h'])])) $r=$axs['cfg']['site'][$axs_user['homedirs'][intval($_POST['h'])]['site']];
				}
			if ($r) exit(axs_redir($r['domain'].$r['dir'].$this->url_root.axs_url($this->url, array('h'=>intval($_POST['h']), 'dir'=>false, ), false)));
			} # </Redirect if another site>
		//$this->tr_load(AXS_PATH_CMS.'index.tr', $axs['cfg']['cms_lang'], $this->name);
		$this->tr=new axs_tr(false, false, false, false, $this->name);
		$this->thumb_w=$this->thumb_h=50;
		$this->form=new axs_form_edit($this->site_nr, $this->url, false, $this->dir, $_POST, $this->name);
		} #</__construct()>
	function ajax_tree() {
		global $axs, $axs_content, $axs_user;
		header('Content-Type: text/html; charset='.$axs['cfg']['charset']);
		$this->ajax=true;
		exit(axs_html_doc($this->tree(), 'browse'));
		} #</ajax()>
	function ui($tree=true, &$content_editor=false) {
		global $axs;
		$this->post=$_POST;
		if (isset($_GET['fget'])) $this->fget($_GET['fget'], axs_get('worig', $_GET), axs_get('horig', $_GET));
		$axs['page']['head'][$this->name.'.css']='<link href="'.axs_dir('editors','http').$this->name.'.css" rel="stylesheet" type="text/css" media="screen" />'."\n";
		$axs['page']['head'][$this->name.'.js']='<script src="'.axs_dir('editors','http').$this->name.'.'.'js'.'"></script>'."\n";
		$this->browse=(isset($this->url['browse'])) ? $this->url['browse']:false;
		if ($this->browse) {
			$axs['page']['title'][]=$this->tr('title_bar');
			$axs['page']['head'][]='<script>axs.window_resize({"ow":1200,"force":true});</script>'."\n";
			if ($this->browse=='img' or $this->browse=='link' or $this->browse=='swf') {
				$this->url=axs_textedit::url($this->url);
				}
			if ($this->browse=='f' or $this->browse=='f2') $axs['page']['head']['axs.tab.edit.js']='<script src="'.axs_dir('lib', 'h').'axs.tab.edit.js'.'"></script>'."\n";
			}
		$this->content_edit=(($content_editor) && (!empty($axs['c']))) ? true:false;
		$msg=$content='';
		$this->form->css_js(false, true);
		$this->fs=new axs_filesystem_img($this->dir.$this->d, $this->site_nr);
		if ($this->browse==='upload') $this->editor_upload_CKE();
		if (isset($_POST['save']) or !empty($this->f)) {
			if ($_POST['mkdir']) $this->editor_mkdir();
			if (isset($_POST['del'])) $this->editor_del();
			if ((!empty($_FILES['files']['name'][0])) or (!empty($_POST['files']['name'][0]))) $this->editor_upload();
			if (($this->content_edit) && ((isset($_POST['pref_save'])) or (isset($_POST['pref_clear'])))) {
				$text=axs_tab_edit::pref_set(axs_content::text_get(), $_POST);
				$content_editor->content_upd($text, $axs['c'], $l=false, $d=false);
				}
			if (!empty($this->f)) $content=$this->editor();
			}
		if (!$content) {
			$form=array(
				'files'=>array('type'=>'file', 'multiple'=>'multiple', 'attr'=>array('id'=>'files', )),
				'save'=>array('type'=>'submit', 'label'=>'>', 'label.html'=>'&gt;', ),
				);
			$this->form->structure_set($form);
			$this->form->values=$this->form->form_input();
			$msg=$this->form->msg_html($axs['msg']);
			foreach ($this->form->structure_get() as $k=>$v) {
				$this->el[$k]=$this->form->element_input_html($k, $v, $this->form->values);
				}
			$content=$this->listd();
			}
		$html=$class='';
		if ($tree) {
			$class=' tree';
			$tree=
			'		<nav id="'.$this->name.'-tree" class="axs-ui-menu vertical toggle open">'."\n".
			'			<div class="sticky">'."\n".
			'				<h2 class="title"><a class="toggle-switch" href="#'.$this->name.'-tree">'.$this->tr('toolbar-title').'</a></h2>'."\n".
			$this->tree().
			'			</div>'."\n".
			'			<script>axs.menu.attach("'.$this->name.'-tree");</script>'."\n".
			'		</nav>'."\n";
			}
		$breadcrumb=array();
		$dir='';
		foreach ($this->path as $v) {
			$dir.=$v.'/';
			$breadcrumb[]='<a href="'.$this->url_root.axs_url($this->url, array('dir'=>$dir)).'">'.htmlspecialchars($v).'</a>';
			}
		$html=
		'	<div>'."\n".
		$tree.
		'		<section class="axs-ui-content'.$class.'">'."\n".
		'			<form id="'.$this->name.'" class="tab" action="'.$this->url_root.axs_url($this->url).'" method="post" enctype="multipart/form-data">'."\n".
		'				<header>'."\n".
		'					<h1><a href="'.$this->url_root.axs_url($this->url, array('dir'=>'')).'">'.htmlspecialchars(rtrim($axs['http'].'://'.$_SERVER['SERVER_NAME'].$this->dir_http,'/')).'</a><span>/</span>'.implode('<span>/</span>', $breadcrumb).'</h1>'."\n".
		$msg.
		'				</header>'."\n".
		$content."\n".
		'			<script>axs.form.init("'.$this->name.'");</script>'."\n".
		'			<script>axs.filemanager.init();</script>'."\n".
		'			</form>'."\n".
		'		</section>'."\n".
		'	</div>'."\n";
		return $html;
		} #</ui()>
	function fget($f, $w, $h) {
		$this->fs=new axs_filesystem_img($this->dir.$this->d, $this->site_nr);
		$this->fs->img_proc($f, false, array('w'=>$w, 'h'=>$h, 'q'=>75));
		} #</fget()>
	function tree($d='', $level=1) {
		global $axs;
		$dirs=axs_admin::f_list($this->dir.$d, '', 'dir');
		if (empty($dirs)) return '';
		asort($dirs);
		if ($level<=1) $dirs=array_merge(array('/'=>'/'), $dirs);
		$s=str_repeat(' ', $level);
		$html='';
		$class=($level==1) ? ' class="tree"':'';
		$class_links=(!empty($this->ajax)) ? 'update browser':'';
		$html.='      '.$s.'<ul'.$class.'>'."\n";
		foreach($dirs as $k=>$v) {
			$v=array();
			$v['dir']=($k=='/') ? '':$d.$k.'/';
			$url=$this->url_root.axs_url($this->url, array('dir'=>$v['dir'], 'value'=>$v['dir'], 'text'=>$v['dir']));
			$ico='<a class="ico '.$class_links.'" href="'.$url.'"><img src="'.AXS_PATH_CMS_HTTP.'gfx/icon.ftype.dir.cls.png" alt="[+]" /></a>';
			$sub='';
			if ((isset($this->path[$level])) && ($k==$this->path[$level])) {
				$tmp=rtrim(dirname($v['dir']), '/');
				$tmp=($tmp=='.') ? '':$tmp.'/';
				$ico='<a class="ico '.$class_links.'" href="'.$this->url_root.axs_url($this->url, array('dir'=>$tmp, 'value'=>$tmp, 'text'=>$v['dir'])).'"><img src="'.AXS_PATH_CMS_HTTP.'gfx/icon.ftype.dir.opn.png" alt="[-]" /></a>';
				$sub="\n".$this->tree($d.$k.'/', $level+1);
				}
			$html.='      '.$s.'<li>'.$ico.'<a class="name '.$class_links.'" href="'.$url.'">'.htmlspecialchars($k).'</a>'.$sub.'</li>';
			}
		$html.='      '.$s.'</ul>'."\n";
		return $html;
		} #</tree()>
	function listd() {
		global $axs, $axs_user, $axs_content;
		$html=$site='';
		if (isset($_GET['w'])) {
			$tmp=array();
			foreach ($axs['site_select'] as $k=>$v) {
				$s=($k==$axs_user['home_nr']) ? ' selected="selected"':'';
				$tmp[$k]='<option value="'.$k.'"'.$s.'>'.$v.'</option>'."\n";
				}
			$site=' - <label>'.$this->tr('toolbar-site_lbl').'<select name="h" id="hdir">'.implode('', $tmp).'</select></label><input id="hdir_btn" name="hdir_btn" type="submit" value="&gt" />'."\n";
			}
		$fup_disabled=($axs['file_uploads']) ? '':' disabled="disabled"';
		$disk_total=disk_total_space($this->dir);
		$disk_free=disk_free_space($this->dir);
		$disk_total_f=axs_filesystem::size_format($disk_total, 2);
		$disk_free_f=axs_filesystem::size_format($disk_free, 2, $disk_total_f['unit']);
		$disk_free_percent=round($disk_free*100/$disk_total, 3);
		$disk_txt=$disk_free_f['size'].'/'.$disk_total_f['size'].$disk_total_f['unit'];
		$content=new axs_content_edit($this->site_nr, axs_get('l', $_GET), $axs_user['home']['dir']);
		$img_proc=axs_admin::$media_defaults['img'];
		//$img_proc['s']['proc']=0;
		$img_proc=$this->fs->img_proc_ui($content->media_settings($this->site_nr, $_POST, $axs['c']), false, $img_proc);
		$html.=
		'      <fieldset id="fup">'."\n".
		'       <legend><label for="files">'.$this->tr('upload-title_lbl').'</label></legend>'."\n".
		'       <p>'."\n".
		'        '.$this->tr('disk-quota_txt').' <strong>'.$disk_txt.'</strong><br />'."\n".
		'        <meter min="0" max="100" value="'.(100-$disk_free_percent).'" high="90">'.$disk_txt.'</meter><br />'."\n".
		'        '.$this->tr('upload-max_filesize_txt').' '.axs_filesystem::max_upl_get('MiB').'MiB/'.axs_filesystem::max_upl_get('MiB','post_max_size').'MiB'."\n".
		'       </p>'."\n".
		'       <label><span class="visuallyhidden">'.$this->tr('upload-title_lbl').'</span> '.$this->el['files'].'</label>'."\n".
		#'       <fieldset><legend>'.$this->tr('upload-img_edit_lbl').'</legend>'."\n".
		$img_proc.
		(($this->content_edit) ?
		'        <label for="pref_save"><input name="pref_save" type="checkbox" id="pref_save" value="1" />'.$this->tr('upload-img_pref_save_lbl').'</label>'."\n".
		'        <label for="pref_clear"><input name="pref_clear" type="checkbox" id="pref_clear" value="1" />'.$this->tr('upload-img_pref_clear_lbl').'</label>'."\n":''
		).
		#'       </fieldset>'."\n".
		'       <input type="submit" name="save" value="'.$this->tr('upload-files_btn').'" />'."\n".
		'       <input type="hidden" name="axs_save" value="1" />'."\n".
		'       <a id="ftp_link" href="'.'?'.axs_url($this->url, array('e'=>'ftp', 'dir'=>$this->dir)).'" target="_blank">FTP link</a>'."\n".
		'      </fieldset>'."\n";
		$mkdir=(!empty($axs['msg'])) ? axs_get('mkdir', $_POST):'';
		$html.=
		'      <table>'."\n".
		'       <caption>'."\n".
		'         <span class="visuallyhidden">'.$this->tr('toolbar-title').$site.'</span>'."\n".
		'         <label id="mkdir_lbl"><span class="visuallyhidden">'.$this->tr('toolbar-mkdir_lbl').'</span> <input name="mkdir" type="text" size="25" value="'.axs_html_safe($mkdir).'" /></label><input name="save" type="submit" value="'.$this->tr('toolbar-mkdir_lbl').'" />'."\n".
		'          <label id="del_lbl"><input id="del" name="del" type="checkbox" value="1" />'.$this->tr('toolbar-del_confirm_lbl').'</label><input id="del_btn" name="save" type="submit" value="X" title="'.$this->tr('toolbar-del_lbl').'" />'."\n".
		'       </caption>'."\n".
		'       <thead>'."\n".
		'        <tr>
		<th class="type" scope="col">'.$this->tr('th_type_lbl').'</th>
		<th class="name" scope="col">'.$this->tr('th_file_lbl').'</th>
		<th class="size" scope="col">'.$this->tr('th_size_lbl').'</th>
		<th class="info" scope="col">'.$this->tr('th_info_lbl').'</th>
		<th class="select" scope="col">'.$this->tr('th_select_lbl').'</th>
		<th class="edit" scope="col">'.$this->tr('th_edit_lbl').'</th>
		</tr>'."\n".
		'       </thead>'."\n".
		'       <tbody>'."\n";
		$nr=1;
		# <Directorys>
		$dirs=axs_admin::f_list($this->dir.$this->d, '', 'dir');
		asort($dirs);
		$tmp=dirname($this->d);
		$dirs=array('/'=>'','..'=>'')+$dirs;
		foreach ($dirs as $k=>$v) {
			$v=array();
			$v['label']=htmlspecialchars($k);
			$v['edit']=$v['edit_btn']=$v['disabled']='';
			if ($k=='/') {	$v['label']='[&nbsp;/&nbsp;]';	$v['dir']='';	$v['disabled']=' disabled="disabled"';	}
			elseif ($k=='..') {	$v['label']='[&uarr;..]';	$v['dir']=(count($this->path)>1) ? dirname($this->d).'/':'';	$v['disabled']=' disabled="disabled"';	}
			else {
				$v['edit']='<input id="f'.$nr.'" name="f['.$nr.']" type="checkbox" value="'.htmlspecialchars($k).'"'.$v['disabled'].' />';
				$v['edit_btn']='<input id="f'.$nr.'edit" name="edit" type="submit" value="'.$this->tr('th_edit_lbl').'"'.$v['disabled'].' />';
				$v['dir']=$this->d.$k.'/';
				}
			$v['url']=htmlspecialchars($this->url_root.axs_url($this->url, array('dir'=>$v['dir']), false));
			$v['ico']='<a href="'.$v['url'].'"><img src="'.AXS_PATH_CMS_HTTP.'gfx/icon.ftype.dir.cls.png" alt="&lt;dir&gt;" /></a>';
			$html.='        <tr id="nr'.$nr.'">'."\n".
			'        <td class="type">'.$v['ico'].'</td>'."\n".
			'        <td class="name dir" title="'.$v['label'].'"><label for="f'.$nr.'"><a href="'.$v['url'].'">'.$v['label'].'</a></label></td>'."\n".
			'        <td class="size"></td>'."\n".
			'        <td class="info"></td>'."\n".
			'        <td class="select">'.$v['edit'].'</td>'."\n".
			'        <td class="edit">'.$v['edit_btn'].'</td>'."\n".
			'       </tr>'."\n";
			$nr++;
			} # </Directorys>
		# <Files>
		$files=axs_admin::f_list($this->dir.$this->d, '', 'file');
		asort($files);
		foreach ($files as $k=>$v) {
			$v=array();
			$v['ext']=$this->f_ext($k);
			if ($this->browse) {
				if ($this->browse=='img') {	if ((!in_array($v['ext'], axs_filesystem::$ftypes['image.*'])) && ($v['ext']!=='svg')) continue;	}
				if ($this->browse=='swf') {	if ($v['ext']!='swf') continue;	}
				}
			$v['url']=htmlspecialchars($this->dir_http.$this->d.$k);
			$v['ico']=$this->listd_icon(htmlspecialchars($this->dir_http.$this->d.$k));
			$v['size']=filesize($this->dir.$this->d.$k);
			$v['size']=implode('', axs_filesystem::size_format($v['size'], 2));
			$v['info']='';
			if (in_array($v['ext'], axs_filesystem::$ftypes['image.*'])) {
				$tmp=$this->media_size($this->dir.$this->d.$k, $this->thumb_w, $this->thumb_h);
				if ($tmp['w_orig'] && $tmp['h_orig']) $v['info']=$tmp['w_orig'].'&times;'.$tmp['h_orig'].'px';
				}
			$v['disabled']=(in_array($this->f_ext($k), axs_filesystem::$ftypes['blacklist'])) ? ' disabled="disabled"':'';
			$html.=
			'        <tr id="nr'.$nr.'">'."\n".
			'         <td class="type">'.$v['ico'].'</td>'."\n".
			'         <td class="name file" title="'.htmlspecialchars($k).'"><label for="f'.$nr.'"><a href="'.$v['url'].'" target="_blank">'.htmlspecialchars($k).'</a></label></td>'."\n".
			'         <td class="size" title="'.$v['size'].'">'.$v['size'].'</td>'."\n".
			'         <td class="info" title="'.$v['info'].'">'.$v['info'].'</td>'."\n".
			'         <td class="select"><input id="f'.$nr.'" name="f['.$nr.']" type="checkbox" value="'.htmlspecialchars($k).'"'.$v['disabled'].' /></td>'."\n".
			'         <td class="edit"><input id="f'.$nr.'edit" name="edit" type="submit" value="'.$this->tr('th_edit_lbl').'"'.$v['disabled'].' /></td>'."\n".
			'       </tr>'."\n";
			$nr++;
			} # </Files>
		$html.='      </table>'."\n";
		return $html;
		} #</listd()>
	function listd_icon($f) {
		$fb=basename($f);
		$ftype=axs_filesystem::f_ext($f);
		if (in_array($ftype, axs_filesystem::$ftypes['image.*'])) {
			$tmp=$this->media_size($this->dir.$this->d.$fb, $this->thumb_w, $this->thumb_h);
			$url=(filesize($this->dir.$this->d.$fb)>(1048576/2)) ? $this->url_root.axs_url($this->url, array('fget'=>$fb, 'worig'=>$tmp['w_orig'], 'horig'=>$tmp['h_orig'])):$this->dir_http.$this->d.$fb;
			return '<a href="'.htmlspecialchars($f).'" target="_blank"><img src="'.$url.'" alt="[img]" width="'.$tmp['w'].'" height="'.$tmp['h'].'" /></a>';
			}
		switch ($ftype) {
			case 'au':
			case 'flac':
			case 'mp2':
			case 'mp3':
			case 'ogg':
			case 'ra':
			case 'wav':
			case 'wma':
				$icon='audio';
				break;
			case 'asf':
			case 'avi':
			case 'flv':
			case 'mov':
			case 'mp4':
			case 'mpg':
			case 'mpeg':
			case 'wmv':
				$icon='video';
				break;
			case 'doc':
			case 'docx':
			case 'odt':
			case 'pdf':
			case 'rtf':
			case 'sxw':
			case 'txt':
				$icon='txt';
				break;
			case 'swf':
			case 'tif':
			case 'tiff':
				$icon='img';
				break;
			case 'htm':
			case 'html':
			case 'php':
				$icon='page';
				break;
			case 'csv':
			case 'ods':
			case 'xls':
			case 'xlsx':
				$icon='table';
				break;
			default:
				$icon='file';
				break;
			}
		$html='<a href="'.$f.'"><img src="'.AXS_PATH_CMS_HTTP.'gfx/icon.ftype.'.$icon.'.png" alt="['.$icon.']" /></a>';
		return $html;
		} # <listd_icon()>
	function editor() {
		global $axs, $axs_user;
		$back_url=$this->url_root.axs_url($this->url, array(), false);
		$html='      <div class="editor">'."\n".
		'             <a href="'.htmlspecialchars($back_url).'">&lt;'.$this->tr('toolbar-back_lbl').'</a>'."\n";
		foreach ($this->f as $k=>$v) {
			if (!file_exists($this->dir.$this->d.$v)) continue;
			$disable=$this->editor_disabled($v);
			$msg=array();
			$n=(isset($_POST['f'.$k.'new'])) ? $_POST['f'.$k.'new']:$v;
			$d=(isset($_POST['d'][$k])) ? $_POST['d'][$k]:$this->d;
			if (substr($d, -1)!='/') $d.='/';
			if (isset($_POST['save']) && !$disable) {
				if ($v!==$n) {
					if (axs_valid::f_name($n)!=$n) $axs['msg'][]=$msg[]=$this->tr('msg_f_name');
					if ($axs_user['user']!='adm') {
						if (!is_dir($this->dir.$this->d.$this->f_secure($v))) {
							if (in_array($tmp=$this->f_ext($n), axs_filesystem::$ftypes['blacklist'])) $axs['msg'][]=$msg[]=$this->tr('msg_f_ext').' ('.htmlspecialchars($tmp).')';
							}
						}
					if (empty($msg)) $this->fs->rename($this->f_secure($v), $this->f_secure($n));
					}
				if ($d!==$this->d) {
					if ($this->f_secure($d)!=$d) $axs['msg'][]=$msg[]=$this->tr('msg_d_name');
					if (empty($msg)) rename($this->dir.$this->d.$this->f_secure($n), $this->dir.$this->f_secure($d.$n));
					}
				}
			$html.='       <fieldset>'."\n".
			axs_form::msg_html($msg).
			'        <label for="f'.$k.'">'.$this->tr('edit-name_lbl').'</label>'."\n".
			'        <input id="f'.$k.'new" name="f'.$k.'new" type="text" size="25" maxlength="250" value="'.htmlspecialchars($n).'"'.$disable.' />'."\n".
			'        <input id="f'.$k.'" name="f['.$k.']" type="hidden" value="'.htmlspecialchars($v).'"'.$disable.' />'."\n".
			'        <label for="d'.$k.'">'.$this->tr('edit-dir_lbl').'</label>'."\n".
			'        <input id="d'.$k.'" name="d['.$k.']" type="text" size="50" value="'.htmlspecialchars($d).'"'.$disable.' />'."\n".
			'       </fieldset>'."\n";
			}
		if (!empty($this->f)) $html.='       <input id="save" name="save" type="submit" value="'.$this->tr('toolbar-save_lbl').'" />'."\n";
		$html.='      </div>';
		if (isset($_POST['save']) && empty($axs['msg'])) exit(axs_redir($back_url));
		return $html;
		} #</editor()>
	function editor_del() {
		global $axs;
		foreach ($this->f as $k=>$v) {
			if ($this->editor_disabled($v)) {
				$axs['msg'][]=$this->tr('msg_del_disabled');
				continue;
				}
			$this->fs->del($v);
			}
		$this->f=array();
		} #</editor_del()>
	function editor_disabled($f) {
		if (is_dir($this->dir.$this->d.$f)) {
			$tmp=axs_admin::f_list($this->dir.$this->d.$f, '/.*\.php$/i', 'file');
			if (!empty($tmp)) return ' disabled="disabled"';
			}
		else {	if (in_array($this->f_ext($f), axs_filesystem::$ftypes['blacklist'])) return ' disabled="disabled"';	}
		return '';
		} #</editor_disabled()>
	function editor_mkdir() {
		global $axs;
		$n=axs_valid::f_name($_POST['mkdir']);
		if (!$n) $axs['msg'][]=$this->tr('msg_mkdir_name_empty');
		if (empty($axs['msg'])) $this->fs->dir_mk($n, true);
		$this->f=array();
		} #</editor_mkdir()>
	function editor_upload($key='files') {
		global $axs, $axs_user, $axs_content;
		$this->fs->f_upl($key);
		$settings=axs_admin::media_settings($this->site_nr, $this->post, $axs['c']);
		//axs_log(__FILE__, __LINE__, '-', $settings);
		$f=array();
		foreach ((array)$this->fs->f as $k=>$v) if (in_array($this->f_ext($v), axs_filesystem::$ftypes['image.*'])) {
			$f[$k]=$this->fs->img_proc_ui_save($v, $settings['img'], axs_admin::$media_defaults);
			}
		$this->f=array();
		return $f;
		} #</editor_upload()>
	function editor_upload_CKE() {
		$this->post['img']['0']['proc']=$this->post['img']['s']['proc']=1;
		$f=$this->editor_upload('upload');
		$f=current($f);
		if (empty($this->fs->msg)) $msg=array('uploaded'=>1, 'fileName'=>$f['t'], 'url'=>$this->dir_http.$this->d.$f['t'], );
		else {
			$msg=array('uploaded'=>0, 'error'=>array('message'=>$this->fs->msg_html()));
			}
		axs_exit(json_encode($msg));
		} #</editor_upload_CKE()>
	} #</class::axs_filemanager>
#2005 ?>