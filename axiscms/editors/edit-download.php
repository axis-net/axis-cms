<?php #2018-02-14
defined('AXS_PATH_CMS') or exit(require('index.php'));

class axs_cms_download extends axs_form_edit {
	var $axiscms_zip='';
	var $ftp_user='';
	var $ftp_pass='';
	var $ftp_chmod=array('content', 'axiscms/data', '_htaccess', '.htaccess', 'et', );
	var $hosting=array(
		'zone'=>array(
			'label'=>'<a href="http://www.zone.ee" target="_blank">zone.ee</a> tasuta kodulehe majutus',
			'ftp_host'=>'web.zone.ee',
			'ftp_dir'=>'',
			),
		'000webhost'=>array(
			'label'=>'<a href="http://www.000webhost.com" target="_blank">000webhost.com</a> tasuta kodulehe majutus',
			'ftp_host'=>'?',
			'ftp_dir'=>'public_html',
			),
		'freehostia'=>array(
			'label'=>'<a href="http://www.freehostia.com" target="_blank">freehostia.com</a> tasuta kodulehe majutus',
			'ftp_host'=>'ftp.freehostia.com',
			'ftp_dir'=>'?',
			),
		'planet'=>array(
			'label'=>'<a href="http://www.planet.ee" target="_blank">planet.ee</a>',
			'ftp_host'=>'ftp.planet.ee',
			'ftp_dir'=>'htdocs',
			),
		'netpoint'=>array(
			'label'=>'<a href="http://www.netpoint.ee" target="_blank">netpoint.ee</a>',
			'ftp_host'=>'?',
			'ftp_dir'=>'public_html', //'\'domains/\'+url.replace(/^www\./, \'\')+\'/public_html\';',
			),
		'datazone'=>array(
			'label'=>'<a href="http://zone.ee" target="_blank">zone.ee DataZone</a>',
			'ftp_host'=>'?',
			'ftp_dir'=>'htdocs',
			),
		'eenet'=>array(
			'label'=>'<a href="http://www.eenet.ee" target="_blank">EENET</a>',
			'ftp_host'=>'?',
			'ftp_dir'=>'html',
			),
		'alfanet'=>array(
			'label'=>'<a href="http://alfanet.ee" target="_blank">alfanet.ee</a>',
			'ftp_host'=>'?',
			'ftp_dir'=>'public_html',
			),
		''=>array(
			'label'=>'muu teenus',
			'ftp_host'=>'?',
			'ftp_dir'=>'',
			),
		);
	function ftp() {
		global $axs;
		if (!$this->axiscms_zip) return;
		$ftp_host=str_replace('ftp://', '', $_POST['ftp_host']);
		$this->ftp_user=$_POST['ftp_user'];
		$this->ftp_pass=$_POST['ftp_pwd'];
		if ($_POST['ftp_user']) $_POST['ftp_user']='*';
		if ($_POST['ftp_pwd']) $_POST['ftp_pwd']='*';
		$conn_id=@ftp_connect($ftp_host);
		//$conn_id=ftp_ssl_connect($ftp_host, 22);
		if (!$conn_id) return $axs['msg'][]='Ei saanud FTP &uuml;hendust serveriga "'.htmlspecialchars($_POST['ftp_host']).'"';
		$login=@ftp_login($conn_id, $this->ftp_user, $this->ftp_pass);
		if (!$login) return $axs['msg'][]='FTP serverisse sisselogimine eba&otilde;nnestus!';
		$ftp_dir=(substr($_POST['ftp_dir'], -1, 1)=='/') ? $_POST['ftp_dir']:$_POST['ftp_dir'].'/';
		
		/*$contents=ftp_rawlist($conn_id, $ftp_dir);
		if (!empty($contents)) $axs['msg'][]='FTP kataloog ei ole t&uuml;hi! Enne paigaldamist tuleb vana koduleht eemaldada.';*/
		
		$http=preg_replace('#/$#', '', $_POST['http']);
		if (substr($http, 0, 7)!='http://' && substr($http, 0, 8)!='https://') $http='http://'.$http;
		
		$dirs=$files=array();
		$zip=new Archive_Zip($this->axiscms_zip);
		foreach ($zip->listContent() as $k=>$v) {
			if ($v['folder']) $dirs[]=$v['filename'];
			else $files[]=$v['filename'];
			}
		sort($dirs);
		sort($files);
		#<Progress bar>
		echo '      <div id="progress_bar"><div id="progress"></div></div><span id="percent">0%</span>'."\n";
		//'      <div class="msg">';
		#</Progress bar>
		/* siggi AT june systems DOT com @ 04-Mar-2005 07:54
		After searching through the PHP site, google and various forums, not finding a solution to my script not outputting 
		anything while calling flush and ob_flush, I thought of trying to tell PHP to call: session_write_close();
		before starting echo'ing. It worked like a charm. I couldn't find any references to this, so I hope this note will help 
		someone in the future.*/
		session_write_close();
		flush();
		
		foreach ($dirs as $k=>$v) { #<Create dirs>
			@ini_set('max_execution_time', (ini_get('max_execution_time')+30));
			@ftp_mkdir($conn_id, $ftp_dir.$v); /*if (!) {
				$axs['msg'][]='Kataloogi tegemine "'.$ftp_dir.$v.'" eba&otilde;nnestus! Kontrollige FTP kataloogi ja &otilde;igusi.';
				break;
				}*/
			} #</Create dirs>
		#<Upload files>
		$total=count($files);
		$done=$percent=0;
		$fp=tmpfile(); //@fopen($f_tmp, 'w+');
		foreach ($files as $k=>$v) {
			@ini_set('max_execution_time', (ini_get('max_execution_time')+30));
			
			//$f_tmp=AXS_PATH_CMS.'data/tmp/ftp_inst_'.uniqid(basename($v.'_')).'.tmp';
			$file=current($zip->extract(array('add_path'=>AXS_PATH_CMS.'data/', 'by_name'=>array($v), 'extract_as_string'=>true, )));
			if ($file['filename']=='axiscms/data/index.php') $file['content']=str_replace('\'ftp\'=>\'\'', '\'ftp\'=>\'ftp://'.$this->ftp_user.':'.$this->ftp_pass.'@'.$ftp_host.'/'.$ftp_dir.'\'', $file['content']);
			@fwrite($fp, $file['content']) or $axs['msg'][$file['filename']]='Error @ fwrite("'.$file['filename'].'")';
			fseek($fp, 0);
			if ($axs['msg'][$file['filename']]) break;
			
			if (!ftp_fput($conn_id, $ftp_dir.$v, $fp, FTP_BINARY)) $axs['msg'][$file['filename']]='Faili &uuml;leslaadimine "'.htmlspecialchars($ftp_dir).$v.'" eba&otilde;nnestus! Kontrollige FTP kataloogi ja &otilde;igusi.';
			//unlink($f_tmp);
			@ftruncate($fp, 0);
			@rewind($fp);
			if ($axs['msg'][$file['filename']]) break;

			$done++;
			$tmp=ceil(($done*100)/$total);
			if ($tmp>$percent) {
				$percent=$tmp;
				$tmp='<script>axs_cms_download.progress_bar('.$percent.');</script><noscript>'.$percent.'% &hellip; </noscript><!-- '.$file['filename'].' -->';
				echo $tmp.str_repeat(' ', 5000-strlen($tmp))."\n";
				flush();
				//usleep(500000);
				}
			}
		@fclose($fp);
		#</Upload files>
		#<Set file permissions>
		$chmod_err='';
		if (empty($axs['msg'])) foreach ($dirs+$files as $v) {
			foreach ($this->ftp_chmod as $vv) if (strncmp($v, $vv, strlen($vv))==0) {
				if (!@ftp_site($conn_id, 'CHMOD 0777 '.$ftp_dir.$v)) {
					if (!$chmod_err) axs_log(__FILE__, __LINE__, 'installer', $chmod_err='chmod 0777 failed on "'.htmlspecialchars($ftp_dir).$v.'"');
					}
				break 1;
				}
			}
		//ftp_close($conn_id);
		#</Set file permissions>
		//echo '      </div>'."\n";
		if (empty($axs['msg'])) $axs['msg'][]='AXIS CMS edukalt paigaldatud <a href="'.htmlspecialchars($http).'" target="_blank">'.htmlspecialchars($http).'</a>';
		axs_log(__FILE__, __LINE__, 'downl_ftp', $axs['msg']);
		} #</ftp()>
	function zip_find() {
		$handle=opendir(AXS_PATH_CMS);
		while (($file=readdir($handle))!==false) {
			if (substr($file, -3)=='zip') {
				if (strncmp($file, 'axiscms-', 8)==0) {
					$this->axiscms_zip=$file;
					break;
					}
				}
			}
		if (!$this->axiscms_zip or !file_exists($this->axiscms_zip)) axs_log(__FILE__, __LINE__, 'downlerror', 'zip file not found', false, '<p class="msg">zip file not found</p>');
		} #</zip_find()>
	function zip_make() {
		$filename='axiscms.zip';
		$ver=str_replace(' ', '', AXS_CMS_VER);
		if (!file_exists($filename)) die('"'.$filename.'" not found!');
		rename($filename, $new='axiscms-'.$ver.uniqid('tmp').'.zip');
		exit((file_exists($new)) ? 'Dowload zip created OK <a href="'.$new.'">'.$new.'</a>':'Error renaming to "'.$new.'"');		
		} #</zip_make()>
	function zip_download() {
		if (!$this->axiscms_zip) return;
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Content-Description: File Transfer');
		header('Content-type: application/zip'); 
		header('Content-Transfer-Encoding: binary');
		header('Content-Disposition: attachment; filename="'.preg_replace('/tmp.*$/', '', $this->axiscms_zip).'.zip"');
		header('Content-Length: '.filesize($this->axiscms_zip));
		readfile($this->axiscms_zip);
		axs_log(__FILE__, __LINE__, 'downl_zip', '', true);
		} #</zip_download()>
	function form_css_js() {
		global $axs;
		axs_form_edit::css_js();
		$get=axs_get('get', $_GET);
		if (!$get) $axs['page']['head']['install']='<link href="'.AXS_PATH_CMS_HTTP.'?'.axs_url($axs['url'], array('get'=>'css')).'" rel="stylesheet" type="text/css" media="screen" />'."\n".
		'<script src="'.AXS_PATH_CMS_HTTP.'?'.axs_url($axs['url'], array('get'=>'js')).'"></script>'."\n";
		if ($get=='css') {
		header("Content-Type: text/css");
		exit('#progress_bar {'."\n".
		'	float:left;'."\n".
		'	margin:0 0 5px 165px;'."\n".
		'	border:solid 1px #F00;'."\n".
		'	padding:1px;'."\n".
		'	width:200px;'."\n".
		'	height:15px;'."\n".
		'	}'."\n".
		'	#progress_bar div {'."\n".
		'		width:1px;'."\n".
		'		height:15px;'."\n".
		'		background:#F00;'."\n".
		'		}'."\n".
		'#percent {'."\n".
		'	padding-left:5px;'."\n".
		'	}');
		}
		if ($get=='js') exit('function axs_cms_download(){'."\n".
		'	this.init=function(){'."\n".
		'		document.getElementById("ftp-install").onsubmit=function(){'."\n".
		'			if (!axs_cms_download.get_url()) return false;'."\n".
		'			if (document.getElementById("ftp-install").ftp_host.value=="" || document.getElementById("ftp-install").ftp_user.value=="" || document.getElementById("ftp-install").ftp_pwd.value=="") {'."\n".
		'				alert(document.getElementById("submit_lbl").innerHTML);'."\n".
		'				return false;'."\n".
		'				}'."\n".
		'			return true;'."\n".
		'			}'."\n".
		'		}'."\n".
		'	this.get_url=function(){'."\n".
		'		var url=document.getElementById("ftp-install").elements.http.value;'."\n".
		'		url=url.replace(/http:\/\//i, "");'."\n".
		'		url=url.replace(/\/$/, "");'."\n".
		'		url=url.replace(/\/.*$/, "");'."\n".
		'		if (url=="") {'."\n".
		'			alert("Palun sisestage oma kodulehe aadress!");'."\n".
		'			return false;'."\n".
		'		}'."\n".
		'		var RegExp=/^(([\w]+:)?\/\/)?(([\d\w]|%[a-fA-f\d]{2,2})+(:([\d\w]|%[a-fA-f\d]{2,2})+)?@)?([\d\w][-\d\w]{0,253}[\d\w]\.)+[\w]{2,4}(:[\d]+)?(\/([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)*(\?(&?([-+_~.\d\w]|%[a-fA-f\d]{2,2})=?)*)?(#([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)?$/;'."\n".
		'   	 if (!RegExp.test(url)) {'."\n".
		'			alert("Kontrollige kodulehe aadressi!");'."\n".
		'			return false;'."\n".
		'			}'."\n".
		'		return url;'."\n".
		'		}'."\n".
		'	this.hosting_select=function(el,ftp_host,ftp_dir){'."\n".
		'		var url=this.get_url();'."\n".
		'		if (!url) {'."\n".
		'			el.checked="";'."\n".
		'			}'."\n".
		'		else {'."\n".
		'			if (ftp_host=="?") ftp_host=url;'."\n".
		'			document.getElementById("ftp-install").ftp_host.value=ftp_host;'."\n".
		'			if (ftp_dir=="?") ftp_dir=url;'."\n".
		'			document.getElementById("ftp-install").ftp_dir.value=ftp_dir;'."\n".
		'			}'."\n".
		'		url=document.getElementById("http").value;'."\n".
		'		if (el.id==="zone") document.getElementById("ftp_user").value=url.replace(/^.+\//,"");'."\n".
		'		}'."\n".
		'	this.progress_bar=function(percent){'."\n".
		'		document.getElementById("progress").style.width=percent+"%";'."\n".
		'		document.getElementById("percent").innerHTML=percent+"%";'."\n".
		'		}'."\n".
		'	this.validate=function(){'."\n".
		'		if (!this.get_url()) return false;'."\n".
		'		if (document.getElementById("ftp-install").ftp_host.value=="" || document.getElementById("ftp-install").ftp_user.value=="" || document.getElementById("ftp-install").ftp_pwd.value=="") {'."\n".
		'			alert(document.getElementById("submit_lbl").innerHTML);'."\n".
		'			return false;'."\n".
		'			}'."\n".
		'		return true;'."\n".
		'		}'."\n".
		'	}//</class::axs_cms_download()>'."\n".
		'var axs_cms_download=new axs_cms_download();');
		} #</css_js()>
	function form($url) {
		global $axs;
?>
     <form id="ftp-install" class="form" method="post" action="<?php echo $url; ?>" onsubmit="return axs_cms_download.validate();">
<?php
if (isset($_POST['submit'])) axs_cms_download::ftp();
echo axs_form::msg_html($axs['msg']);
?>
     <div class="element">
      <label for="http">Teie kodulehe address<em class="input_required"><abbr title="kohustuslik v&auml;li">*</abbr></em></label>
      <input name="http" type="text" id="http" value="<?php echo htmlspecialchars(axs_get('http', $_POST)); ?>" size="40" />
     </div>
     <fieldset class="element radio">
      <legend>Serveriteenus</legend>
<?php
foreach ($this->hosting as $k=>$v) {
	$id=($k) ? $k:'_';
	$chkd=(axs_get('hosting', $_POST)===$k) ? ' checked="checked"':'';
	echo '      <label><input type="radio" name="hosting" value="'.$k.'" id="'.$id.'"'.$chkd.' onclick="axs_cms_download.hosting_select(this,\''.$v['ftp_host'].'\',\''.$v['ftp_dir'].'\');" />'.$v['label'].'</label>'."\n";
	}
?>     </fieldset>
     <div class="element">
      <label for="ftp_host">FTP serveri aadress<em class="input_required"><abbr title="kohustuslik v&auml;li">*</abbr></em></label>
      <input name="ftp_host" type="text" id="ftp_host" value="<?php echo htmlspecialchars(axs_get('ftp_host', $_POST)); ?>" size="40" />
     </div>
     <div class="element">
      <label for="ftp_user">FTP Kasutajatunnus<em class="input_required"><abbr title="kohustuslik v&auml;li">*</abbr></em></label>
      <input name="ftp_user" type="text" id="ftp_user" value="<?php echo htmlspecialchars($this->ftp_user); ?>" size="40" />
     </div>
     <div class="element">
      <label for="ftp_pwd">FTP parool<em class="input_required"><abbr title="kohustuslik v&auml;li">*</abbr></em></label>
      <input name="ftp_pwd" type="text" id="ftp_pwd" value="<?php echo htmlspecialchars($this->ftp_pass); ?>" size="40" />
     </div class="element">
     <div class="element">
      <label for="ftp_dir">Kaust FTP seerveris, kuhu paigadada kodulehe failid</label>
      <input name="ftp_dir" type="text" id="ftp_dir" value="<?php echo htmlspecialchars(axs_get('ftp_dir', $_POST)); ?>" size="40" />
     </div>
     <div class="element">
      <label id="submit_lbl" for="submit" class="js_hide">Palun t&auml;itke k&otilde;ik kohustuslikud v&auml;ljad!</label>
      <input type="submit" name="submit" id="submit" value="Paigalda" />
     </div class="element">
    </form>
    <script>axs_cms_download.init()</script>
    	<?php
    	}
	} #</class::axs_cms_download>

if (AXS_LOCALHOST!==true) exit(header('Location: http://'.AXS_DOMAIN.'/axiscms/?s1=install'));
$download=axs_get('download', $_REQUEST);
$axs['download']=new axs_cms_download();
if ($download=='make' && $_SERVER['SERVER_NAME']=='localhost') $axs['download']->zip_make();
else $axs['download']->zip_find();
if ($axs['e']=='download') $axs['download']->zip_download();
if ($this->s1=='install') $axs['download']->form_css_js();
#2007-11 ?>