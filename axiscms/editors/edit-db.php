<?php #2020-03-05
if (!defined('AXS_PATH_CMS')) exit(require('index.php')); # Prevent direct access
if ($tmp=axs_admin::no_permission(array('admin'=>'', 'cms'=>'', ), false, __FILE__, __LINE__)) return $tmp; #<Permission check />

if (defined('AXS_OUTPUT_START')) {	echo 'AXS_OUTPUT_START defined';	return;	}

$axs['page']['head']['index.content.css']='<link href="'.AXS_PATH_CMS_HTTP.'index.content'.'.css" rel="stylesheet" type="text/css" media="screen" />'."\n";
$axs['page']['head'][$this->name]='<link href="'.AXS_PATH_CMS_HTTP.'edit-db'.'.css" rel="stylesheet" type="text/css" media="screen" />'."\n";

$this->tr_load(axs_tr::$lang, preg_replace('/\.php$/', '', basename(__FILE__)));
$this->initialize('edit-db', axs_dir('plugins'), false, '', 1);

$this->header=new axs_form_header(array('site_nr'=>$this->site_nr, ));
$sql_result=false;
$header=array();
$this->modules=($axs['cfg']['db'][1]['type']) ? $this->header->sql_header_modules_list():array();
$this->module='';

$this->vars=array('m'=>'', 'menu'=>'', 'content'=>'', );

# Single module mode
#if (isset($this->module)) return $this->load_editor($this->f_path.$axs['f_px'].$this->module.'.module.php');

foreach($this->f_list($this->f_path, '/^.+\.module\.php$/') as $file) { #<Read directory cycle>
	$m=preg_replace('/^'.$axs['f_px'].'|\.module\.php$/', '', $file);
	if (!isset($this->modules[$m])) $this->modules[$m]['label']=$m;
	} # </Read directory cycle>

reset($this->modules);
$this->module=(isset($this->modules[axs_get($this->header->key_m, $_GET)])) ? $_GET[$this->header->key_m].'':@key($this->modules);
$this->vars['m']=$this->url[$this->header->key_m]=$this->module;

foreach ($this->modules as $k=>$v) {
	if (!empty($v['disable'])) {	unset($this->modules[$k]);	continue;	} #<Remove disabled modules />
	$this->modules[$k]=$v['']+array('forms'=>array(), );
	foreach ($v as $kk=>$vv) if ($kk) $this->modules[$k]['forms'][$kk]=$vv[''];
	$this->act=($this->module===$k) ? $k:'';
	$this->forms=$this->modules[$k]['forms'];
	reset($this->forms);
	$this->f=(isset($this->forms[axs_get($this->header->key_f, $_GET)])) ? $_GET[$this->header->key_f]:key($this->forms);
	
	$this->modules[$k]['file']=(file_exists($tmp=$this->f_path.$k.'.module.php')) ? $tmp:'';
	$this->modules[$k]['class']=false;
	if (file_exists($tmp=$this->f_path.$k.'.module.class.php')) {	require_once($tmp);	$this->modules[$k]['class']=$k;	}
	if (file_exists($tmp=$this->f_path.$k.'.'.$this->f.'.module.class.php')) {	require_once($tmp);	$this->modules[$k]['class']=$k.'_'.$this->f;	}
	
	$class=($this->modules[$k]['class']) ? 'axs_'.$this->modules[$k]['class'].'_module':'axs_db_edit';
	$this->m=new $class($this, $k, $k, '.module');
	$this->m->act=$this->act;
	$this->m->url=array_merge($this->m->url, array($this->header->key_m=>$k, $this->header->key_f=>$this->f));
	$this->m->menu=array(
		$k=>array(
			'url'=>'?'.axs_url($this->m->url), 'label'=>$this->modules[$k]['label.html'], 'act'=>$this->act, 'submenu'=>array(),
			),
		);
	//$this->breadcrumb['db']['submenu'][$k]=$this->m->menu[$k];
	if ((count($this->forms)>1) || (key($this->forms)!==$k)) foreach ($this->forms as $kk=>$vv) {
		$act=($this->act) ? $this->f:false;
		$this->m->menu[$k]['submenu'][$kk]=array(
			'url'=>'?'.axs_url($this->m->url, array($this->header->key_f=>$kk)), 'label'=>$vv['label.html'], 'act'=>$act,
			);
		if ($act===$kk) {
			$this->m->menu[$k]['act']=true;
			}
		}
	$this->breadcrumb['db']['submenu'][$k]=$this->m->menu[$k];
	if ($this->act) { #<Continue module execution only if this is the current active module />
		$axs['page']['title'][]=$this->modules[$k]['label'];
		if (count($this->modules[$k]['forms'])>1) $axs['page']['title'][]=$this->forms[$this->f]['label'];
		if ($tmp=axs_admin::no_permission(array('admin'=>'', 'cms'=>''), 'r', __FILE__, __LINE__)) return $tmp; #<Permission check />
		$this->breadcrumb['db-'.$k]=$this->m->menu[$k];
		if (!empty($this->m->menu[$k]['submenu'][$this->f])) $this->breadcrumb['db.'.$k.'.'.$this->f]=$this->m->menu[$k]['submenu'][$this->f];
		$this->vars['content'].=($this->modules[$k]['file']) ? include($this->modules[$k]['file']):$this->m->ui();
		}
	$this->m->module_menu();
	}
//dbg($this->breadcrumb);
foreach ($this->modules as $k=>$v) $this->vars['menu'].=$this->modules[$k]['menu'];
if (empty($this->modules)) $this->vars['content'].='<p class="msg">'.$this->tr('modules_not_installed.txt').'</p>';
//dbg($this->tr->tr);

if (!empty($axs['get']['section'])) return $this->vars['content'];
else {
	$axs['page']['class'][]='axs-ui-menu-left';
	$this->tpl_set($this->name,
	'	<div>'."\n".
	'		<nav id="menu-left" class="axs-ui-menu vertical module-{$m} toggle open">'."\n".
	'			<div class="sticky">'."\n".
	'				<h2 class="title"><a class="toggle-switch" href="#db_menu">{$db.menu.title}</a></h2>'."\n".
	'{$menu}'.
	'			</div>'."\n".
	'			<script>axs.menu.attach("menu-left");</script>'."\n".
	'		</nav>'."\n".
	'		<div id="content-editor" class="axs-ui-content module-{$m}">'."\n".
	'{$content}'."\n".
	'		</div>'."\n".
	'	<div>'."\n");
	return $this->finish();
	}
#2008-12-29 ?>