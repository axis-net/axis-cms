<?php #2015-07-13
defined('AXS_PATH_CMS') or exit(require('index.php')); #<Prevent direct access />

if (!defined('AXS_OUTPUT_START')) {
	$this->tr_load('', '', preg_replace('/\.php$/', '', basename(__FILE__)));
	$axs['page']['title']=array($axs['editor']->tr('title_txt'), );
	$axs['page']['head']['index.content.css']='<link href="'.AXS_PATH_CMS_HTTP.'index.content'.'.css" rel="stylesheet" type="text/css" media="screen" />'."\n";
	$this->s1=axs_get('s1', $_GET);
	if (isset($_GET['s1'])) $this->url['s1']=htmlspecialchars(urlencode($this->s1));
	if (AXS_LOCALHOST===true) include_once(AXS_PATH_CMS.'edit-download.php');
	return '';
	}
else {
if (AXS_LOGINCHK!==1) {
	echo '<h2 id="system_title">'.trim($_SERVER['SERVER_NAME'].$axs['http_root'], '/').'</h2>'."\n";
	echo axs_user_login::form($this->l);
	}
$doc=new axs_doc(array(
	'axiscms'=>array('dir'=>AXS_PATH_CMS.'doc/', 'px'=>''),
	'axiscms_api'=>array('dir'=>AXS_PATH_CMS.'doc/', 'px'=>''),
	), $this->l, ' ', 1, $this->url, $_GET);
if (AXS_LOCALHOST!==true) unset($doc->data['install'],$doc->data['download']);
# <Intranet modules docs>
if (defined('AXS_PATH_EXT')) {
	$data=$tmp=$doc->data_list(AXS_PATH_EXT, $axs['f_px']);
	if (!empty($data)) {
		if (!isset($this->db_module_order)) $this->db_module_order=array();
		foreach ($data as $k=>$v) if (!isset($this->db_module_order[$k])) $this->db_module_order[$k]=$k;
		asort($this->db_module_order);
		$data=array();
		foreach ($this->db_module_order as $k=>$v) $data[$k]=$tmp[$k];
		$doc->data['userguide']['content']['edit-db']['content']['modules']['content']=array();
		foreach ($data as $k=>$v) $doc->data['userguide']['content']['edit-db']['content']['modules']['content']+=$doc->data_load($v['f'], $v['l']);
		}
	} # </Intranet modules docs>
if ((AXS_LOCALHOST===true) or (AXS_LOGINCHK===1)) echo $doc->contents_html().$doc->content_html();
if (AXS_LOCALHOST===true) {	if ($this->s1=='install') $axs['download']->form('?'.axs_url($this->url, array('s1'=>$this->s1, 'download'=>'ftp')).'#ftp-install');	}
}
#2005 ?>