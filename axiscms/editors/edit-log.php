<?php #2021-09-03
if (!defined('AXS_PATH_CMS')) exit(require('log.php')); # Prevent direct access
if ($tmp=axs_admin::no_permission(array('dev'=>'', 'admin'=>'', ))) return $tmp; #<Permission check />

//$this->tr_load(axs_tr::$lang, preg_replace('/\.php$/', '', basename(__FILE__)));
#<CSS />
$axs['page']['head'][]=
'<style type="text/css">
	h1 {	font-size:14px;	}
	pre {
		margin:0;
		white-space:pre-wrap;
		}
	pre:empty {	display:none;	}
	#send_entry fieldset {
		margin-top:10px;
		}
	#send_entry label {
		display:block;
		}
	#send_entry input[type="text"], #send_entry textarea {
		display:block;
		width:90%;
		}
	table.table {	width:100%;	}
		table.table tr {	vertical-align:top;	}
			table.table tr td.time {	white-space:nowrap;	}
				table.table tr .request button {
					border:none;
					padding:0;
					background:none;
					}
				table.table tr td.request div.closed pre {
					max-height:5em;
					overflow:hidden;
					}
				table.table tr td.request div.closed::after {
					display:block;
					content:"\2026\0020";
					font-weight:bold;
					}
</style>'."\n";
$axs['page']['head'][]=
'<script>
document.addEventListener("DOMContentLoaded",function(){
	var el=document.querySelector("table.table tr th.request");
	if (el) {
		el.innerHTML+=" <button>[-]</button>";
		el=document.querySelector("table.table tr th.request button").addEventListener("click",function(){
			if (axs.class_has(this.parentNode,"closed")) {
				axs.class_rem(document.querySelectorAll("table.table tr th.request, table.table tr td.request div"),"closed");
				var el=document.querySelectorAll("table.table tr .request button");
				for (var i=0, l=el.length; i<l; i++) el[i].innerHTML="[-]";
				}
			else {
				axs.class_add(document.querySelectorAll("table.table tr th.request, table.table tr td.request div"),"closed");
				var el=document.querySelectorAll("table.table tr .request button");
				for (var i=0, l=el.length; i<l; i++) el[i].innerHTML="[+]";
				}
			});
		}
	var el=document.querySelectorAll("table.table tr td.request div strong");
	for (var i=0, l=el.length; i<l; i++) el[i].innerHTML+=" <button>[-]</button>";
	var el=document.querySelectorAll("table.table tr td.request div strong button");
	for (var i=0, l=el.length; i<l; i++) el[i].addEventListener("click",function(){
		if (axs.class_has(this.parentNode.parentNode,"closed")) {
			axs.class_rem(this.parentNode.parentNode,"closed");
			this.innerHTML="[-]";
			}
		else {
			axs.class_add(this.parentNode.parentNode,"closed");
			this.innerHTML="[+]";
			}
		});
	});
</script>'."\n";

class axs_log_view {
	function __construct($url) {
		$this->url=$url;
		#<Get IP location service url />
		$this->ip_location_url=axs_file_array_get(axs_dir('lib').'axs.dict.php', 'ip_location_url');
		} #</axs_log_view()>
	function entry($r) {
		global $axs;
		$entry=array();
		foreach (axs_log::$file_format as $k=>$v) {
			$entry[$k]=axs_get($v['k'], $r, '');
			if (!empty($v['urlenc'])) $entry[$k]=axs_log::array_decode($entry[$k]);
			}
		if ($tmp=$entry['time']) {
			$entry['time']=DateTime::createFromFormat('Y-m-d H:i:s', $entry['time']);
			if (!$entry['time']) $entry['time']=new DateTime('@'.$tmp);
			$entry['time']->setTimezone(new DateTimeZone($axs['cfg']['timezone']));
			}
		return $entry;
		} #</entry()>
	function entry_display(&$data, $entry) {
		global $axs;
		$entry+=0;
		$row=self::entry(explode('|', $data[$entry]));
		$url=$this->url+array('entry'=>$entry, 'axs'=>['section'=>'content'], );	
		$lastcheck=($tmp=axs_get('axs_lastcheck', $row['cookies'])) ? date('d.m.Y H:i:s', intval($tmp)):'';
		$ip_location_url=$this->entry_ip_location($row['ipaddr']);
		if (isset($_GET['mail_entry'])) { #<Send entry>
			$to=axs_get('to', $_POST);
			$replyto=axs_get('replyto', $_POST);
			$subject=axs_get('subject', $_POST);
			$msg=axs_get('msg', $_POST);
			$error='';
			if (!empty($_POST['mail'])) {
				if ($to && $replyto) axs_mail($to, $subject, $msg, '',
					'AXIS CMS log(at)'.$_SERVER['SERVER_NAME'], 'log@'.$_SERVER['SERVER_NAME'],
					array(), array('Reply-To'=>$replyto, )
					);
				else $error='Missing "To" or "Reply to"!';
				}
			$logsubject='system message: '.$row['msgtype'].' - "'.$row['msg'].'"';
			if (strlen($logsubject)>60) $logsubject=substr($logsubject, 0, 60).'..."';
			return '     <form id="send_entry" method="post" action="'.'?'.axs_url($url, array('mail_entry'=>$entry)).'">'."\n".
			(($error) ? '      <div class="msg">'.$error.'</div>'."\n":'').
			'       <label for="to">to</label>'."\n".
			'       <input type="email" name="to" id="to" size="100" value="'.htmlspecialchars(implode(', ', $axs['cfg']['emails'])).'" />'."\n".
			'       <label for="replyto">reply to</label>'."\n".
			'       <input type="email" name="replyto" id="replyto" size="100" value="'.$replyto.'" />'."\n".
			'       <label for="subject">subject</label>'."\n".
			'       <input type="text" name="subject" id="subject" size="100" value="'.htmlspecialchars($logsubject).'" />'."\n".
			'       <label for="msg">message</label>'."\n".
			'       <textarea name="msg" id="msg" cols="50" rows="20">AXIS CMS log @ '.$_SERVER['SERVER_NAME'].$axs['http_root']."\n".
			"\n".
			(($row['time'])?$row['time']->format('d.m.Y H:i:s'):'')."\n".
			"\n".
			'Message type: '.$row['msgtype']."\n".
			'Message: '.str_replace('<br />', "\n", $row['msg'])."\n".
			'Script file name: '.$row['fname'].' ('.$row['line'].')'."\n".
			'Request: '.$row['request']."\n".
			'Cookie(s):'."\n".
			str_replace('<br />', "\n", axs_log::array_dump($row['cookies']))."\n".
			'Post:'."\n".
			str_replace('<br />', "\n", axs_log::array_dump($row['post']))."\n".
			'Files:'."\n".
			str_replace('<br />', "\n", axs_log::array_dump($row['files']))."\n".
			'Referer: '.$row['referer']."\n".
			'User: '.$row['usr'].' (last online check: '.$lastcheck.')'."\n".
			'Host: '.$row['host'].' ('.$row['ipaddr'].') Geographical location: '.$ip_location_url."\n".
			'User agent: '.$row['user_agent']."\n".
			'AXIS CMS ver: '.$row['cms_ver']."\n".
			'</textarea>'."\n".
			'       <input type="submit" name="mail" value="Send"'.(($axs['mail()']) ? '':' disabled="disabled"').' />'."\n".
			'       <a href="?'.axs_url($url).'">&lt;back</a>'."\n".
			'     </form>'."\n";
			} #</Send entry>
		//$post='';
		//foreach ($row['post'] as $k=>$v) $post='					<input name="'.$k.'" value="'.$v.'" type="hidden" />'."\n";
		return '     <script>axs.window_resize({"ow":1200});</script>'."\n".
		'		<h1>AXIS CMS</b> log @ '.$_SERVER['SERVER_NAME'].$axs['http_root'].'</h1>'."\n".
		'		<p class="time">'.(($row['time'])?$row['time']->format('d.m.Y H:i:s'):'').'</p>'."\n".
		'		<dl>'."\n".
		'			<dt>Message type</dt><dd>'.$row['msgtype'].'</dd>'."\n".
		'			<dt>Message</dt><dd><pre>'.$row['msg'].'</pre></dd>'."\n".
		'			<dt>Script file name</dt><dd>'.$row['fname'].' <strong>('.$row['line'].')</strong></dd>'."\n".
		'			<dt>Request</dt><dd><a href="'.$row['request'].'" target="_blank">'.$row['request'].'</a></dd>'."\n".
		'			<dt>Cookie(s)</dt><dd><pre>'.axs_log::array_dump($row['cookies']).'</pre></dd>'."\n".
		'			<dt>Post</dt>'."\n".
		'			<dd>'."\n".
		'				<pre>'.axs_log::array_dump($row['post']).'</pre>'."\n".
		'				<form action="'.$row['request'].'" method="post" enctype="multipart/form-data" target="_blank">'."\n".
		self::form_array($row['post']).
		'					<input type="submit" value="POST" />'."\n".
		'				</form>'."\n".
		'			</dd>'."\n".
		'			<dt>Files</dt><dd><pre>'.axs_log::array_dump($row['files']).'</pre></dd>'."\n".
		'			<dt>Referer</dt><dd><a href="'.$row['referer'].'" target="_blank">'.$row['referer'].'</a></dd>'."\n".
		'			<dt>User</dt><dd>'.$row['usr'].' (last online check: '.$lastcheck.')</dd>'."\n".
		'			<dt>Host</dt><dd>'.$row['host'].' ('.$row['ipaddr'].') <a href="'.$ip_location_url.'" target="_blank">'.$ip_location_url.'</a></dd>'."\n".
		'			<dt>User agent</dt><dd>'.$row['user_agent'].'</dd>'."\n".
		'			<dt>Server software</dt><dd>'.$row['server'].'</dd>'."\n".
		'			<dt>AXIS CMS ver</dt><dd>'.$row['cms_ver'].'</dd>'."\n".
		'		</dl>'."\n".
		'		<p><a href="?'.axs_url($url, array('mail_entry'=>$entry)).'">Mail this entry&gt;&gt;</a></p>'."\n";
		} #</entry_display()>
	function entry_ip_location($ip) {
		return htmlspecialchars(str_replace('{$ip}', $ip, $this->ip_location_url));
		} #</entry_ip_location()>
	static function form_array($array, $px='') {
		$input='';
		foreach ($array as $k=>$v) {
			if ($px) $k='['.$k.']';
			if (is_array($v)) $input.=self::form_array($v, $px.$k);
			else $input.='					<input name="'.$px.$k.'" type="hidden" value="'.$v.'" />'."\n";
			}
		return $input;
		} #</form_array()>
	} #</class::axs_log_view>

$menu=array('s'=>array(), 'u'=>array(), );
$tmp=axs_get('s2', $_GET);
$url=array_merge($this->url, array('s1'=>'logview', 's2'=>(isset($menu[$tmp])) ? urlencode($tmp):key($menu), ));
foreach ($menu as $k=>$v) $menu[$k]=array(
	'url'=>'?'.axs_url($url, array('s2'=>$k)),
	'label'=>$this->tr('logview.menu.'.$k.'.lbl'),
	'act'=>$url['s2'],
	);
$axs['page']['title']=array_merge(array(0=>$menu[$url['s2']]['label']), $axs['page']['title']);
$axs['editor']->breadcrumb['logview']=array_merge($menu[$url['s2']], array('submenu'=>$menu));
$fn=new axs_log_view($url);
switch ($url['s2']) {
	case 'u': #View user log
		if (!$axs['cfg']['db'][1]['type']) {	$html='<p class="msg" lang="en">ERROR! This plugin requires SQL database.</p>';	break;	}
		$structure=array(
			''=>array('layout'=>'table', 'tr'=>$this->tr->get(), 'sql'=>array('table'=>$axs['cfg']['db'][1]['px'].'users_log', ), ),
			'id'=>array('type'=>'number', 'size'=>10, 'thead'=>1, 'search'=>1, 'DESC'=>'DESC', ),
			'time'=>array('type'=>'timestamp', 'size'=>10, 'thead'=>1, 'search'=>1, 'sort'=>1, 'format'=>'y-m-d H:i:s', ),
			'uid'=>array('type'=>'number', 'size'=>10, 'thead'=>1, 'search'=>1, 'sort'=>1, ),
			'user'=>array('type'=>'text', 'size'=>25, 'thead'=>1, 'search'=>1, 'sort'=>1, ),
			'type'=>array('type'=>'text', 'size'=>10, 'thead'=>1, 'search'=>1, 'sort'=>1, ),
			'request'=>array('type'=>'url', 'size'=>75, 'thead'=>1, 'search'=>1, ),
			'post'=>array('type'=>'textarea', 'size'=>30, 'thead'=>0, 'search'=>1, ),
			'files'=>array('type'=>'textarea', 'size'=>30, 'thead'=>0, 'search'=>1, ),
			'ip'=>array('type'=>'text', 'size'=>25, 'thead'=>1, 'search'=>1, 'sort'=>1, ),
			);
		$html=array('th'=>'', 'tr'=>'', );
		foreach ($structure as $k=>$v) if ($k) $structure[$k]['label']=$k;
		$form=new axs_form(array('structure'=>$structure, ));
		foreach ($form->structure_get_thead() as $k=>$v) $html['th'].='<th class="'.$k.'">'.$k.'</th>';
		$search=new axs_form_search($form->structure, $_GET, $url, false, array(100, 25, 250, 500));
		$pager=new axs_pager(axs_get('p', $_GET), array(100, 25, 250, 500), axs_get('ps', $_GET));
		$search->elements();
		$r=axs_db_query($q="SELECT SQL_CALC_FOUND_ROWS * FROM `".$form->sql_table."`\n".
		"	".$search->sql_where('WHERE ')."\n".
		"	".$search->sql_order('ORDER BY ')." LIMIT ".$pager->start.','.$pager->psize, 1, 1, __FILE__, __LINE__);
		#dbg($q,$r);
		$r=$form->sql_result_set($r);
		foreach ($r as $cl) {
			$td=$vr=array();
			foreach ($form->structure_get_thead() as $k=>$v) $td[$k]=$form->value_display($k, $v, $cl, $vr);
			foreach (array('post'=>'$_POST', 'files'=>'$_FILES', ) as $k=>$v) {
				#if (strlen($cl[$k])) axs_db_query($q="UPDATE `".$form->sql_table."` SET `".$k."`='".addslashes(axs_log::array_dump(unserialize($cl[$k])))."' WHERE id='".$cl['id']."'", '', 1, __FILE__, __LINE__);
				if ($cl[$k]) $td['request'].='<div><strong>'.$v.'</strong>:<pre>'.$cl[$k].'</pre></div>';
				}
			$td['ip']='<a href="'.$fn->entry_ip_location($cl['ip']).'" target="_blank">'.$td['ip'].'</a>';
			foreach ($td as $k=>$v) $td[$k]='				<td class="'.$k.'">'.$v.'</td>'."\n";
			$html['tr'].='			<tr id="id'.$cl['id'].'">'."\n".implode('', $td)."\n".'			</tr>'."\n";
			}
		$html+=$pager->pages($form->sql_found_rows, '?'.axs_url($url, array('ps'=>$pager->psize, 'p'=>'')));
		$html['search']=$search->form();
		$html=axs_tpl_parse(
		'{$search}'."\n".
		'	<table id="userslog" class="table">'."\n".
		'		<caption>{$total}</caption>'."\n".
		'		<thead><tr>{$th}</tr></thead>'."\n".
		'		<tbody>'."\n".
		'{$tr}'.
		'		</tbody>'."\n".
		'	</table>'."\n".
		'			<div class="pager">'."\n".
		'				<p class="found"><!--{$search.found.lbl} --><strong>{$total}</strong> <span class="prev-next">{$prev}{$current}{$next}</span></p>'."\n".
		'				<p class="pages">{$pages}</p>'."\n".
		'			</div>'."\n",
		$html);
		break;
	default: #View system log
		#<Get logfiles />
		$logfiles=axs_log::file_list();
		$logfiles[]=true;
		$logfiles=array_reverse($logfiles);
		unset($logfiles[0]);
		$p=(isset($_GET['p'])) ? intval($_GET['p']):1;
		$data=((isset($logfiles[$p])) && (is_file(AXS_PATH_CMS.'data/'.$logfiles[$p]))) ? array_reverse(file(AXS_PATH_CMS.'data/'.$logfiles[$p]), true):array();
		#<Display entry>
		if (isset($_GET['entry'])) return $fn->entry_display($data, $_GET['entry']);
		#<Search form />
		$sform_files=array();
		foreach ($logfiles as $k=>$v) {
			$selected=($k==$p) ? ' selected="selected"':'';
			$sform_files[$k]='<option value="'.$k.'"'.$selected.'>'.$v.'</option>';
			}
		$sform=array();
		foreach ($this->url as $k=>$v) $sform[$k]='			<input type="hidden" name="'.$k.'" value="'.axs_html_safe($v).'" /></label>'."\n";
		$s=array('msgtype'=>'', 'usr'=>'', 'ipaddr'=>'', );
		foreach ($s as $k=>$v) {
			$s[$k]=$v=axs_get($k, $_GET);
			$sform[$k]='			<label>'.$k.' <input type="text" name="'.$k.'" value="'.axs_html_safe($v).'" size="10" /></label>'."\n";
			}
		#<Search filter />
		foreach($data as $nr=>$row) {
			$data[$nr]=$fn->entry(explode('|', $row));
			foreach ($s as $k=>$v) {
				if (($v) && ($data[$nr][$k]!==$v)) {	unset($data[$nr]);	continue 2;	}
				}
			}
		$html=
		'		<form action="?" method="get" id="log_search" class="search">'."\n".
		'			<label>file <select name="p">'.implode("\n".'        ', $sform_files).'</select></label>'."\n".
		implode('', $sform).
		'			<input type="submit" value="&gt;" />'."\n".
		'		</form>'."\n".
		'		<table id="logfile" class="table selectable">'."\n".
		'			<caption>'.htmlspecialchars(axs_get($p,$logfiles)).' ('.count($data).')</caption>'."\n".
		'			<tr><th>time</th><th>msgtype</th><th>msg</th><th>file</th><th>line</th><th>usr</th><th>host</th></tr>'."\n";
		#<Display table />
		foreach($data as $rownr=>$row) {
			$row['time']=($row['time']) ? $row['time']->format('d.m.y H:i:s'):'';
			if (!$row['msgtype']) $row['msgtype']='- ? -';
			if (!$row['msg']) $row['msg']=$row['referer'];
			$row['msg']=wordwrap($row['msg'], 70, '<br />', 1);
			if (!$row['usr']) $row['usr']='&nbsp;';
			$html.='      <tr lang="en"><td class="time">'.$row['time'].'</td><td class="msgtype"><a href="?'.axs_url($url, array('p'=>$p, 'entry'=>$rownr, 'axs'=>['section'=>'content'])).'" target="popup">'.$row['msgtype'].'</a></td><td class="msg">'.$row['msg'].'</td><td class="file"><abbr title="'.$row['fname'].'">'.basename($row['fname']).'</abbr></td><td class="line">'.$row['line'].'</td><td class="usr">'.$row['usr'].'&nbsp;</td><td class="host"><a href="'.$fn->entry_ip_location($row['ipaddr']).'" target="_blank">'.$row['ipaddr'].'</a> '.$row['host'].'</td></tr>'."\n";
			}
		$html.='     </table>'."\n";
		break;
	}
return 
'		<nav id="menu-tabs" class="axs-ui-menu tabs">'."\n".
'			<h2 class="title visuallyhidden">'.htmlspecialchars($axs['editor']->breadcrumb['system.logview']['label']).'</h2>'."\n".
axs_admin::menu_build($menu, '     ', array('class'=>'menu')).
'		<script>axs.menu.attach("menu-tabs");</script>'."\n".
'		</nav>'."\n".
'		<div class="tab">'."\n".
$html.
'		</div>'."\n";
#2005 ?>