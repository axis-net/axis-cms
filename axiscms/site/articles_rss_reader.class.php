<?php #2019-09-02
include_once('articles.base.class.php');
class axs_articles_rss_reader extends axs_articles_base {
	public $expire=300;
	public $qr_code=array();
	public $qr_code_files=array();
	public $content_search=array('title'=>'', 'summary'=>'', );
	public $fields_article=array(
		''=>array('sql_table'=>'{$px}{$name}', 'dir_entry'=>'{$id}/', ),
		'id'=>array('type'=>'number', 'size'=>10, 'required'=>0, 'readonly'=>'readonly', 'thead'=>0, ),
		'publ'=>array('type'=>'number', 'size'=>5, 'required'=>1, 'thead'=>1, 'rank'=>true, 'ASC'=>'ASC', ),
		'title'=>array('type'=>'text', 'size'=>75, 'required'=>1, 'maxlength'=>255, 'thead'=>1, ),
		'url'=>array('type'=>'url', 'size'=>75, 'required'=>1, 'maxlength'=>255, 'thead'=>1, ),
		'summary'=>array('type'=>'wysiwyg', 'size'=>75, 'required'=>0, 'rows'=>2, 'thead'=>1, ),
		'seo'=>array('type'=>'fieldset', 'size'=>0, 'required'=>0, ),
		'title_alias'=>array('type'=>'text', 'size'=>50, 'required'=>0, 'maxlength'=>255, 'sql'=>array(
			'_charset'=>'ASCII',
			) ),
		'meta_description'=>array('type'=>'text', 'size'=>50, 'required'=>0, 'maxlength'=>255, ),
		'seo_end'=>array('type'=>'fieldset_end', 'size'=>0, 'required'=>0, ),
		'limit'=>array('type'=>'number', 'size'=>10, 'required'=>0, 'thead'=>1, ),
		'_disable'=>array('type'=>'checkbox', 'size'=>1, 'required'=>0, 'thead'=>1, ),
		'updated'=>array('type'=>'timestamp-updated', 'size'=>50, 'required'=>0, 'thead'=>1, ),
		'save'=>array('type'=>'submit', 'size'=>0, 'required'=>0, 'add_fn'=>true, ),
		);
	public $fields_comment=array();
	static $tr=array(
		'en'=>array(
			'form.title'=>'RSS reader',
			'id.lbl'=>'ID',
			'publ.lbl'=>'nr',
			'title.lbl'=>'title',
			'url.lbl'=>'URL',
			'summary.lbl'=>'summary',
			'seo.lbl'=>'SEO',
			'title_alias.lbl'=>'URL alias',
			'meta_description.lbl'=>'<meta description>',
			'limit.lbl'=>'max articles limit',
			'_disable.lbl'=>'hidden',
			'save.lbl'=>'save',
			'updated.lbl'=>'updated',
			'read_more.lbl'=>'read more',
			),
		'et'=>array(
			'form.title'=>'RSS lugeja',
			'id.lbl'=>'ID',
			'publ.lbl'=>'nr',
			'title.lbl'=>'pealkiri',
			'url.lbl'=>'URL',
			'seo.lbl'=>'SEO',
			'summary.lbl'=>'sissejuhatus',
			'title_alias.lbl'=>'URL alias',
			'meta_description.lbl'=>'<meta description>',
			'limit.lbl'=>'max artiklite arv',
			'_disable.lbl'=>'peidetud',
			'save.lbl'=>'salvesta',
			'updated.lbl'=>'muudetud',
			'read_more.lbl'=>'loe edasi',
			),
		);
	#<List articles>
	function list_articles_row(&$cl, &$vr=array()) {
		parent::{__FUNCTION__}($cl, $vr);
		if (!isset($this->tpl_feed)) $this->tpl_feed=axs_tpl(false, $this->axs_local['plugin'].'.list.feed.tpl');
		$vr['list']='';
		if (!$xml=$this->list_articles_row_feed($cl)) return;
		$count=1;
		foreach($xml->entry as $item) {
			if(($cl['limit']) && ($count>$cl['limit'])) break;
			$v=array(
				'lang'=>(!empty($item->language))?' lang="'.$item->language.'"':'',
				'article.url'=>htmlspecialchars($item->link),
				'title'=>htmlspecialchars($item->title),
				'summary'=>(string)$item->description,
				'time'=>(string)$item->pubDate,
				'time.datetime'=>(string)$item->pubDate,
				);
			$vr['list'].=axs_tpl_parse($this->tpl_feed, $v);
			$count++;
			}
		} #</list_articles_row()>
	function list_articles_row_feed($cl) {
		global $axs;
		$file=axs_dir('content').$this->fields_article['']['dir'].$cl['id'].'/cache.xml';
		$xml='';
		$file_time=(file_exists($file)) ? filemtime($file):0;
		if($axs['time']-$this->expire<$file_time) $xml=file_get_contents($file);
		if (!$xml) {
			if ($xml=file_get_contents($cl['url'])) file_put_contents($file, $xml);
			else axs_log(__FILE__, __LINE__, $this->axs_local['plugin'], 'Error loading feed');
			}
		return ($xml) ? simplexml_load_string($xml):false;//simplexml_load_file
		} #</list_articles_row_feed()>
	} #</class::axs_articles_rss_reader>
/*CREATE TABLE `axs_articles_rss_reader` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site` tinyint(3) unsigned NOT NULL,
  `disable` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `c` varchar(255) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '' COMMENT '=content.cid',
  `l` char(2) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `rank` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `url` text COLLATE utf8_estonian_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_estonian_ci DEFAULT NULL,
  `updated` int(10) unsigned NOT NULL DEFAULT '0',
  `updated_uid` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_estonian_ci;*/
#2010-01-06 ?>