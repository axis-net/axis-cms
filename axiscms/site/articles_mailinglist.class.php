<?php #2016-09-18
require_once('articles.base.class.php');

class axs_articles_mailinglist extends axs_articles_base {
	var $folders=false;
	var $fields_article=array(
		''=>array('dir_entry'=>'', 'layout'=>'table', 'sql'=>array('table'=>'{$px}{$plugin}', 'attr'=>array('COMMENT'=>'AXIS CMS plugin articles_mailinglist'))),
		'id'=>array('type'=>'number', 'size'=>10, 'required'=>0, 'readonly'=>'readonly', 'thead'=>0, 'search'=>1, 'sql'=>array(
			'_type'=>'int', '_value'=>10, '_attribute'=>'unsigned', 'Key'=>'PRI', 'Extra'=>'AUTO_INCREMENT', 
			), ),
		//'fields'=>array('type'=>'fieldset', ),
		'email'=>array('type'=>'email', 'size'=>75, 'required'=>1, 'maxlength'=>255, 'thead'=>null, 'search'=>1, 'sort'=>true, 'export'=>1, ),
		//'fields/'=>array('type'=>'fieldset_end', ),
		'time_subscribe'=>array('type'=>'timestamp', 'size'=>10, 'required'=>1, 'format'=>'d.m.Y H:i:s', 'thead'=>null, 'search'=>1, 'sort'=>true, ),
		'disable'=>array('type'=>'checkbox', 'size'=>1, 'required'=>0, 'thead'=>null, 'value'=>1, 'search'=>1, 'sort'=>true, ),
		'save'=>array('type'=>'submit', 'size'=>0, 'required'=>0, 'add_fn'=>true, ),
		);
	var $fields_comment=array();
	/*function __constructor(&$axs_local) {
		}*/ #</__constructor()>
	function banner_init() {
		global $axs;
		//axs_articles_base::banner_start();
		//$axs['page']['head'][$this->axs_local['plugin'].'.js']='<script src="'.axs_dir('plugins', 'http').$this->axs_local['plugin'].'.js'.'"></script>'."\n";
		} #</banner_start()>
	function banner_parse() {
		global $axs, $axs_content;
		$this->vr['url']=$this->url_root.axs_url($this->url, array('c'=>$this->axs_local['c']));
		$this->vr['title']=$axs_content['content'][$this->axs_local['c']]['title'];
		return $this->content_display('.banner');
		} #</banner_parse()>
	function content_display($tpl='') {
		global $axs, $axs_content;
		$this->vr['c']=$this->axs_local['c'];
		$this->uform=$axs_content['content'][$this->axs_local['c']]['form'];
		unset($axs_content['content'][$this->axs_local['c']]['form']);
		//require_once(AXS_PATH_CMS.'site.form.php');
		$this->uform=new axs_form_site($this->axs_local, $this->uform, $_POST);
		$this->uform->disabled=false;
		$this->uform->url['c']=$this->axs_local['c'];
		$this->uform->values=$this->uform->form_input();
		if ($this->uform->save) $this->uform->validate($this->uform->values);
		if (($this->uform->save) && (empty($this->uform->msg))) {
			if (isset($this->uform->user_input['subscribe'])) $this->list_subscribe();
			if (isset($this->uform->user_input['unsubscribe'])) $this->list_unsubscribe();
			}
		$this->uform->target_id=$this->axs_local['c'].'_form';
		$this->vr['form']=$this->uform->build();
		return axs_tpl_parse(axs_tpl(false, $this->axs_local['plugin'].$tpl.'.tpl'), $this->vr+$this->form->tr->get());
		} #</content()>
	function list_command_send($command) { #<Send command to list server />
		//dbg($this->uform->cfg);
		if (!$mailto=$this->uform->cfg['list_address_'.$command]) return;
		if ($this->uform->cfg['mailto']) $mailto.=', '.$this->uform->cfg['mailto'];
		axs_mail(
			$mailto,
			ucfirst($command),
			$command.' '.$this->uform->cfg['list_name'].' '.$this->uform->values['email'],
			'',
			preg_replace('/^www\./', '', $_SERVER['SERVER_NAME']),
			$this->uform->values['email']
			);
		} #</list_command_send()>
	function list_insert() {
		global $axs;
		$found=axs_db_query("SELECT `id` FROM `".$this->form->sql_table."` WHERE `email`='".addslashes($this->uform->values['email'])."'".$this->form->sql_add(' AND ', '', ' AND ', array('disable'=>false, )), 'cell', $this->db, __FILE__, __LINE__);
		if ($found) return $found;
		return axs_db_query("INSERT INTO `".$this->form->sql_table."`\n".
		"	SET ".$this->uform->sql_values_set($this->uform->values).", `time_subscribe`='".$axs['time']."'".
		$this->form->sql_add(', ', '' , ', '), 'insert_id', $this->db, __FILE__, __LINE__);
		} #</list_subscribe()>
	function list_subscribe() {
		global $axs;
		$this->list_command_send('subscribe');
		#<Set form messages />
		$this->uform->msg('', $this->uform->tr->s('msg_subscribe'));
		#<Update SQL table />
		if (!$this->db) return;
		$id=$this->list_insert();
		axs_db_query("UPDATE `".$this->form->sql_table."` SET `disable`='0', `time_subscribe`='".$axs['time']."' WHERE `id`='".$id."'", '', $this->db, __FILE__, __LINE__);
		} #</list_subscribe()>
	function list_unsubscribe() {
		$this->list_command_send('unsubscribe');
		#<Set form messages />
		$this->uform->msg('', $this->uform->tr->s('msg_unsubscribe'));
		#<Update SQL table />
		if (!$this->db) return;
		$id=$this->list_insert($this->uform->values['email']);
		axs_db_query("UPDATE `".$this->form->sql_table."` SET `disable`='1' WHERE `id`='".$id."'", '', $this->db, __FILE__, __LINE__);
		} #</list_unsubscribe()>
	} #</class::axs_articles_mailinglist>
/*CREATE TABLE IF NOT EXISTS `axs_articles_mailinglist` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '=content.id',
  `c` varchar(255) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '' COMMENT '=content.cid',
  `l` char(2) CHARACTER SET ascii COLLATE ascii_bin DEFAULT '',
  `time_subscribe` int(10) unsigned NOT NULL DEFAULT '0',
  `email` varchar(50) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `disable` tinyint(3) unsigned NOT NULL,
  `updated_uid` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site-c-l` (`site`,`c`,`l`),
  KEY `time_subscribe` (`time_subscribe`),
  KEY `email` (`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_estonian_ci;*/
#2013-03-03 ?>