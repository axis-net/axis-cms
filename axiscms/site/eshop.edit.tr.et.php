<?php #26.02.2010
return array(
	'form_config_lbl'=>'seaded',
	'form_delivery_lbl'=>'kauba k&auml;ttetoimetamine',
	
	'doc_lbl'=>'kasutusjuhend',
	'back_lbl'=>'tagasi',
	'reset_lbl'=>'reset',
	'save_lbl'=>'salvesta',
	'del_lbl'=>'kustuta',
	'del_confirm_lbl'=>'Kas oled kindel, et soovid antud artikli kustutada? Koos sellega kustutatakse ka k&otilde;ik selle juurde kuuluvad failid!',
	'input_required_lbl'=>'Kohustuslik v&auml;li',
	'top_lbl'=>'&uuml;les',
	'found_lbl'=>'leitud',
	'add_lbl'=>'lisa uus',
	'add_confirm'=>'Kas soovid lisada uue artikli?',
	'edit_lbl'=>'muuda',
	'preview_lbl'=>'kontrolli',
	
	'msg_empty_field'=>'Palun t&auml;itke rida',
	);
#26.02.2010 ?>