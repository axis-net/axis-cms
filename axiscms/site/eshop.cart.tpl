		<h2>{$cart.txt}</h2>
		<form id="cart" method="post" action="{$form.action}#cart">
			{$msg}
			<table>
				<caption><strong>{$count}</strong> {$cart.items.txt}</caption>
				<tfoot>
					<tr>
						<td colspan="9">
							<output>
								{$cart.sum.txt}: <span>{$sum}{$currency.symbol}</span><br />
								{$cart.sum.vat.txt}: <span>{$sum.vat}{$currency.symbol}</span><br />
								{$cart.sum+vat.txt}: <strong>{$sum+vat}{$currency.symbol}</strong>
							</output>
							<input name="checkout" type="submit" value="{$cart.checkout.lbl}" />
						</td>
					</tr>
				</tfoot>
				<thead>
					<tr>
						<th class="nr"><abbr title="{$nr.txt}">{$nr.abbr}</abbr></th>
						<th class="pic">{$cart.pic.lbl}</th>
						<th class="product">{$cart.product.lbl}</th>
						<th class="price">{$cart.price.lbl}</th>
						<th class="amount">{$cart.amount.lbl}</th>
						<th class="vat"><abbr title="{$cart.vat.lbl}">{$cart.vat.abbr}</abbr></th>
						<th class="sum">{$cart.sum.lbl}</th>
						<th class="edit">{$cart.edit.lbl}</th>
						<th class="del">{$cart.del.lbl}</th>
					</tr>
				</thead>
{$cart}
			</table>
		</form>
		<script>axs.eshop.cart('#cart');</script>