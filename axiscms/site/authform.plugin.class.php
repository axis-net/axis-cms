<?php #2017-11-27
class axs_authform_page {
	function __construct(&$axs_local, &$plugin_def) {
		global $axs;
		$this->axs_local=&$axs_local;
		$this->plugin_def=&$plugin_def;
		$this->db=($axs['cfg']['db'][$axs['cfg']['users_db']]['type']) ? $axs['cfg']['users_db']:false;
		$this->url=array_merge($axs['url'], array('c'=>$axs_local['c'], 's'=>axs_get('s', $_GET)));
		} #</__construct()>
	function content_include($f) {
		global $axs;
		return include($f);
		} #</content_include()>
	} #</class::axs_authform_page>
#2007-08 ?>