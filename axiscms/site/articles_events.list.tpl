		<article id="id{$id}" class="item {$_class}">
			<h2><a href="{$article_url}">{$title}</a></h2>
			<a class="menu_pic" href="{$menu_url}" title="{$menu_title}"><img src="{$menu_img_act}" alt="{$menu_title}" /></a>
			<div class="summary clear-content-left">
				{$file}
				<time class="publ" datetime="{$date.datetime}">{$date} {$time}</tome>
				{$summary}
			</div>
		</article>
