<?php #2020-02-03
$plugin_def=array(); # plugin default data
$plugin_def['name']['en']='Multi-page (One-pager)';
$plugin_def['name']['et']='Multi-leht (One-pager)';
$plugin_def['title']['']='';
$plugin_def['help']['']='';
$plugin_def['mkdir']='';
#$plugin_def['content']['']='<?php #|'.$axs['time'].',,'.axs_get('user', $axs_user).',,||? >'."\n";

if (defined('AXS_SITE_NR')) {
require_once(str_replace('.plugin.php', '.class.php', basename(__FILE__)));
$data=new axs_menu_page($axs_local);
$data->vr+=array('c'=>$axs_local['c'], 'title'=>axs_html_safe($axs_local['title']), 'nav'=>'', 'pages'=>'', );
foreach (array(''=>'', 'nav'=>'.', 'section'=>'.') as $k=>$v) {
	$tmp=array();
	if (!empty($axs_local['style'])) $tmp[]=$axs_local['style'].'.plugin'.$v.$k.'.tpl';
	$tmp[]=$data->plugin.'.'.$axs_local['section'].$v.$k.'.tpl';
	$tmp[]=$data->plugin.'.banner'.$v.$k.'.tpl';
	$tmp[]=$data->plugin.$v.$k.'.tpl';
	$tmp[]=$data->name.$v.$k.'.tpl';
	$data->tpl[$k]=axs_tpl(false, array_unique($tmp), __FILE__, __LINE__);
	}
$nr=0;
foreach ($data->index_get() as $k=>$v) {
	$nr++;
	//if ($k===$axs['c']) continue;
	foreach ($v as $kk=>$vv) if (is_array($vv)) unset($v[$kk]);
	$v['class']=array('plugin-'.$v['plugin'], 'style-'.$v['style'], );
	$vr=array('c'=>$k, 'class'=>$v['class'], 'url'=>array_merge($axs['url'], array('c'=>$k, )), 'title'=>axs_html_safe($v['title']), 'content'=>'', );
	$vr['content']=axs_content::plugin_load($k, $axs_local['section'], array(), $vr);
	$vr['class']=implode(' ', $vr['class']);
	$vr['url']=(is_array($vr['url'])) ? axs_content::url($vr['url']):htmlspecialchars($vr['url']);
	$vr=axs_tpl_parse($data->tpl['section'], $vr);
	if (axs_tpl_has($data->tpl[''], 'section.nr.'.$nr)) $data->vr['section.nr.'.$nr]=$vr;
	elseif (axs_tpl_has($data->tpl[''], 'section.'.$k)) $data->vr['section.'.$k]=$vr;
	else $data->vr['pages'].=$vr;
	$v['title']=axs_html_safe($v['title']);
	$data->vr['nav'].=axs_tpl_parse($data->tpl['nav'], $v);
	}
//eval('unset('.$data->path_index.');');
return axs_tpl_parse($data->tpl[''], $data->vr);
}
#2010-10-10 ?>