       <article id="id{$id}" class="item clear-content {$_class}">
        <h2>
         <time class="publ" datetime="{$publ.datetime}">{$publ.lbl} {$publ}</time>
         <a href="{$article_url}">{$title}</a>
        </h2>
		{$file.t1}
		{$summary}
       </article>
