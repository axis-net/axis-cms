<?php #2019-03-13
class axs_eshop_delivery_dpdee {
	public $url='ftp://ftp.dpd.ee/parcelshop/psexport_latest.csv';
	public $country='0560';
	public $robot=false;
	function __construct($parent_id) {
		global $axs;
		$this->parent_id=$parent_id;
		$this->get();
		} #</__construct()>
	function get() {
		global $axs;
		$this->table=$groups=array();
		$data_old=axs_eshop_plugin::delivery_options_choice_query($this->parent_id, true);
		$fp=fopen($this->url, 'r');
		if ($fp!==false) while (($cl=fgetcsv($fp, 1000, '|'))!==false) {
			if ($cl[1].''!==$this->country) continue;
			$v=array('local_id'=>$cl[0], 'group_name'=>$cl[5], 'label'=>$cl[2].', '.$cl[3], );
			if (!isset($this->table[$v['group_name']])) {
				$group_id=(isset($data_old[$v['group_name']])) ? $data_old[$v['group_name']]['local_id']:'1'.$v['local_id'];
				$this->table[$v['group_name']]=array('local_id'=>$group_id, 'group'=>0, 'label'=>$v['group_name'], );
				$groups[$group_id]=0;
				}
			$v['group']=$this->table[$v['group_name']]['local_id'];
			$this->table[$v['local_id']]=array('local_id'=>$v['local_id'], 'group'=>$v['group'], 'label'=>$v['label'], );
			}
		else return;
		fclose($fp);
		#<Remove options according to $robot />
		foreach ($this->table as $k=>$v) if ($v['group']) {
			$robot=(strpos($v['label'], 'Starship')===false) ? false:true;
			if ((($this->robot) && (!$robot)) || ((!$this->robot) && ($robot))) unset($this->table[$k]);
			else $groups[$v['group']]++;
			}
		#<Remove empty groups />
		foreach ($this->table as $k=>$v) if ((!$v['group']) && (!$groups[$v['local_id']])) unset($this->table[$k]);
		#<Convert encoding />
		foreach ($this->table as $k=>$v) foreach ($v as $kk=>$vv) $this->table[$k][$kk]=mb_convert_encoding($vv, $axs['cfg']['charset'], 'ISO-8859-4');
		} #</get()>
	}#</class::axs_eshop_delivery_dpdee>
#2018-04-24 ?>