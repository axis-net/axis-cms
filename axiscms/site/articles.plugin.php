<?php #2015-10-31 utf-8
$plugin_def=array(); # plugin default data
$plugin_def['name']['en']='Articles';
$plugin_def['name']['et']='Artiklid';
$plugin_def['title']['']='';
$plugin_def['help']['']='';
$plugin_def['mkdir']='files';
$plugin_def['content']['']='<?php #|'.$axs['time'].',,'.axs_get('user', $axs_user).',,||?>'."\n";
require_once('articles.base.class.php');
$plugin_def['tr']=axs_articles_base::$tr;

if (defined('AXS_SITE_NR')) {
$data=new axs_articles_base($axs_local);
if ($tmp=$data->error()) return $tmp;
$data->vr['_content']=include('deflt.plugin.php');
if ($axs_local['section']!=='content') {
	$data->banner_init();
	return $data->banner_parse();
	}
$data->page_init($plugin_def);
return $data->page_parse();
}
#2008.06 ?>