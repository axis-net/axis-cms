		<nav class="tree">
			<h2>{$tree.lbl}</h2>
			{$_nav_tree}
		</nav>
		<h1>{$title}</h1>
		{$_content}
		{$_search}
		<div id="articles_list" class="list">
{$_articles}
		</div>
		<div class="pager">
			{$pager.prev}
			<span class="pages">{$pager.pages}</span>
			{$pager.next}
		</div>