<?php #2022-04-29
class axs_articles_base {
	public $name='articles'; #Legacy
	public $_class='articles';
	public $_class_base='articles';
	public $class='articles';
	public $mkdir='files';
	public $key_article='id';
	public $sql_limit=array(25,50,100,250,500,100);
	public $sql_limit_banner=10;
	public $sql_limit_comments=25;
	public $sql_fields=array(
		'site'=>array('_type'=>'tinyint', '_value'=>3, '_attribute'=>'unsigned', 'Null'=>false, 'Comment'=>'', ),
		'plugin'=>array('_type'=>'enum', '_value'=>array('', ), 'Null'=>false, 'Default'=>'', '_charset'=>'ASCII', 'Comment'=>'', ),
		'l'=>array('_type'=>'char', '_value'=>2, 'Null'=>false, 'Default'=>'', '_charset'=>'ASCII', 'Comment'=>'Language', ),
		'c'=>array('_type'=>'varchar', '_value'=>255, 'Null'=>false, 'Default'=>'', '_charset'=>'ASCII', 'Comment'=>'Content item ID', ),
		'_type'=>array('_type'=>'enum', '_value'=>array('', '_folder', ), 'Default'=>'', '_charset'=>'ASCII', 'Null'=>false, 'Comment'=>'Record type', ),
		'_parent'=>array('_type'=>'int', '_value'=>10, 'Null'=>false, 'Default'=>0, '_attribute'=>'unsigned', 'Comment'=>'Parent ID from the same table', ),
		);
	public $sql_where=array();
	public $date_new=259200;
	public $rownr=0;
	public $menu_path_add=true;
	public $log_id=false;
	public $tpl=array();
	public $vr=array('js'=>'', 'summary'=>'', );
	public $content_index=array();
	public $content_search=array('title'=>'', 'summary'=>'', 'text'=>'', );
	public $socialmedia=array();
	public $qr_code=array();#array('title'=>"\n", 'publ'=>' ', 'time'=>"\n", );
	public $qr_code_files=array('qr1.png'=>2,'qr2.png'=>25);
	public $module=false;
	public $forms=array();
	public $f=false;
	public $db_visibility_site=array('site', );
	public $db_visibility_site_search=array('search', 'site-search', );
	public $db_visibility_admin=array('admin', );
	public $db_visibility_admin_search=array('search', 'admin-search', );
	public $level=1;
	public $folders='closed';#'open'|false
	public $_type='';
	public $article_first=null;
	public $article_parent=false;
	public $articles_index=array();
	public $lang_multi=false;
	public $lightbox=false;
	//public $display_article=''; #'selected':show selected or first in list; 'first_page':show first item on first page;
	//public $display_content='first_page';
	public $fields_folder=array(
		'id'=>array('type'=>'number', 'size'=>10, 'required'=>0, 'readonly'=>'readonly', 'thead'=>1, ),
		'title'=>array('type'=>'text', 'size'=>50, 'required'=>1, 'maxlength'=>255, 'thead'=>1, 'title_item'=>1, ),
		'nr'=>array('type'=>'number', 'size'=>5, 'required'=>1, 'thead'=>1, 'sort'=>1, 'ASC'=>'ASC', 'rank'=>true, ),
		'file'=>array('type'=>'file', 'size'=>0, 'required'=>0, 'accept'=>'image.*', 'thead'=>1, 'add_fn'=>true, 'multiple'=>false, 't'=>array(
				't1'=>array('w'=>400, 'h'=>400, 'ext'=>'jpg', 'crop_pos'=>'50%', 'crop_size'=>':', 'q'=>90, 'name_base'=>'{$id}', 'alt'=>'{$id}.t1.{$ext}', 'dw'=>200, 'dh'=>200, 'link'=>array('c'=>'{$c}', 't'=>'{$title_alias}', 'id'=>'{$id}'), 'link_target'=>'', ),
				't2'=>array('w'=>2000, 'h'=>2000, 'ext'=>'jpg', 'q'=>90, 'name_base'=>'{$id}', 'alt'=>'{$id}.t2.{$ext}', 'dw'=>200, 'dh'=>200, 'link'=>'t2', 'link_target'=>'_blank', ),
				),
			),
		'summary'=>array('type'=>'wysiwyg', 'size'=>75, 'required'=>0, 'rows'=>2, 'thead'=>0, ),
		'seo'=>array('type'=>'fieldset', 'size'=>0, 'required'=>0, ),
		'title_alias'=>array('type'=>'text', 'size'=>50, 'required'=>0, 'maxlength'=>255, 'sql'=>array(
			'_charset'=>'ASCII',
			) ),
		'meta_description'=>array('type'=>'text', 'size'=>50, 'required'=>0, 'maxlength'=>255, ),
		'seo_end'=>array('type'=>'fieldset_end', 'size'=>0, 'required'=>0, ),
		'updated'=>array('type'=>'timestamp-updated', 'size'=>50, 'required'=>0, 'thead'=>0, 'sort'=>1, 'DESC'=>'DESC', 'add_fn'=>true, ),
		'save'=>array('type'=>'submit', 'size'=>0, 'required'=>0, 'add_fn'=>true, ),
		);
	public $fields_article=array(
		''=>array('dir_entry'=>'{$id}/', 'layout'=>'list', 'sql'=>array(
			'table'=>'{$px}{$name}', 'attr'=>array('COMMENT'=>'AXIS CMS plugin articles'), 
			), ),
		//'article_head'=>array('type'=>'fieldset', 'size'=>0, 'required'=>0, ),
		'id'=>array('type'=>'number', 'size'=>10, 'required'=>0, 'readonly'=>'readonly', 'thead'=>0, 'search'=>1, 'sql'=>array(
			'_type'=>'int', '_value'=>10, '_attribute'=>'unsigned', 'Key'=>'PRI', 'Extra'=>'AUTO_INCREMENT', 
			), ),
		/*'_parent'=>array('type'=>'number', 'size'=>10, 'search'=>1, 'sql'=>array(
			'_type'=>'int', '_value'=>10, '_attribute'=>'unsigned', 'Key'=>'INDEX', 
			), ),*/
		'nr'=>array('type'=>'number', 'size'=>5, 'required'=>1, 'thead'=>0, 'sort'=>1, 'ASC'=>'ASC', 'rank'=>true, ),
		'title'=>array('type'=>'text', 'size'=>50, 'required'=>1, 'maxlength'=>255, 'thead'=>0, 'search'=>1, 'title_item'=>1, ),
		'file'=>array('type'=>'file', 'size'=>0, 'required'=>0, 'accept'=>'image.*', 'thead'=>1, 'add_fn'=>true, 'multiple'=>false, 't'=>array(
				't1'=>array('w'=>400, 'h'=>400, 'ext'=>'jpg', 'crop_pos'=>'50%', 'crop_size'=>':', 'q'=>90, 'name_base'=>'{$id}', 'alt'=>'{$id}.t1.{$ext}', 'dw'=>200, 'dh'=>200, 'link'=>array('c'=>'{$c}', 't'=>'{$title_alias}', 'id'=>'{$id}'), 'link_target'=>'', ),
				't2'=>array('w'=>2000, 'h'=>2000, 'ext'=>'jpg', 'q'=>90, 'name_base'=>'{$id}', 'alt'=>'{$id}.t2.{$ext}', 'dw'=>200, 'dh'=>200, 'link'=>'t2', 'link_target'=>'_blank', ),
				),
			),
		'file_caption'=>array('type'=>'text', 'size'=>50, 'required'=>0, 'maxlength'=>255, 'thead'=>1, 'search'=>1, ),
		'summary'=>array('type'=>'wysiwyg', 'size'=>75, 'required'=>0, 'rows'=>2, 'thead'=>1, ),
		//'article_head_end'=>array('type'=>'fieldset_end', 'size'=>0, 'required'=>0, ),
		'text'=>array('type'=>'wysiwyg', 'size'=>75, 'required'=>0, ),
		'files'=>array('type'=>'table', 'size'=>1, 'required'=>0, 'thead'=>0, 'add_fn'=>false, 'options'=>array(
				'nr'=>array('type'=>'number', 'required'=>1, 'min'=>0, 'max'=>255, 'size'=>3, 'rank'=>true, 'ASC'=>'ASC', ),
				'file'=>array('type'=>'file', 'size'=>0, 'required'=>0, 'accept'=>'image.*', 'thead'=>1, 'add_fn'=>true, 't'=>array(
						't1'=>array('w'=>600, 'h'=>400, 'ext'=>'jpg', 'crop_pos'=>'', 'crop_size'=>'', 'q'=>90, 'name_base'=>'{$_field}.{$_key}', 'alt'=>'{$_title} {$_key}', 'dw'=>200, 'dh'=>200, /*'link'=>'t2', 'link_target'=>'popup',*/ ),
						't2'=>array('w'=>2000, 'h'=>2000, 'ext'=>'jpg', 'q'=>90, 'name_base'=>'{$_field}.{$_key}', 'alt'=>'{$title} {$_key}', 'dw'=>200, 'dh'=>200, 'link'=>'', 'link_target'=>'', ),
						),
					),
				'_add'=>array('type'=>'submit', ),
				'_del'=>array('type'=>'checkbox', ),
				),
			),
		'seo'=>array('type'=>'fieldset', 'size'=>0, 'required'=>0, ),
		'title_alias'=>array('type'=>'text', 'size'=>50, 'required'=>0, 'maxlength'=>255, 'sql'=>array(
			'_charset'=>'ASCII',
			) ),
		'meta_description'=>array('type'=>'text', 'size'=>50, 'required'=>0, 'maxlength'=>255, ),
		'seo_end'=>array('type'=>'fieldset_end', 'size'=>0, 'required'=>0, ),
		'comments'=>array('type'=>'checkbox', 'size'=>1, 'required'=>0, 'value'=>1, 'thead'=>1, 'search'=>1, 'sql'=>array(
			'_type'=>'tinyint', '_value'=>3, '_attribute'=>'UNSIGNED',
			) ),
		'comments_mail'=>array('type'=>'email', 'size'=>20, 'required'=>0, 'maxlength'=>255, 'multiple'=>'multiple', ),
		'_disabled'=>array('type'=>'checkbox', 'size'=>1, 'required'=>0, 'value'=>1, 'thead'=>1, 'search'=>1, 'sql'=>array(
			'_type'=>'tinyint', '_value'=>1, '_attribute'=>'UNSIGNED',
			) ),
		'updated'=>array('type'=>'timestamp-updated', 'size'=>50, 'required'=>0, 'thead'=>0, 'sort'=>1, 'DESC'=>'DESC', 'add_fn'=>true, ),
		'save'=>array('type'=>'submit', 'size'=>0, 'required'=>0, 'add_fn'=>true, ),
		);
	public $fields_comment=array(
		''=>array('sql'=>array('table'=>'', 'attr'=>array('COMMENT'=>'AXIS CMS plugin comments'), 'cols'=>array(
			'id'=>array('_type'=>'int', '_value'=>10, '_attribute'=>'unsigned', 'Key'=>'PRI', 'Extra'=>'AUTO_INCREMENT', ),
			'parent_id'=>array('_type'=>'int', '_value'=>10, '_attribute'=>'unsigned', ),
			))),
		'name'=>array('thead'=>1, 'type'=>'text', 'size'=>15, 'required'=>1, 'maxlength'=>255, ),
		'time'=>array('thead'=>1, 'type'=>'timestamp', 'size'=>10, 'required'=>0, 'format'=>'d.m.Y H:i', ),
		'message'=>array('thead'=>1, 'type'=>'textarea', 'size'=>50, 'required'=>1, 'rows'=>5, 'bbcode'=>true, ),
		'captcha'=>array('thead'=>0, 'type'=>'text-captcha', 'size'=>0, 'required'=>1, ),
		'post'=>array('thead'=>0, 'type'=>'submit', 'size'=>0, 'required'=>0, ),
		'user_ip'=>array('thead'=>0, 'type'=>'text', 'size'=>15, 'required'=>0, 'sql'=>array(
			'_type'=>'int', '_value'=>10, '_attribute'=>'UNSIGNED',
			), ),
		'hostname'=>array('thead'=>0, 'type'=>'text', 'size'=>255, 'required'=>0, 'sql'=>array(
			'_charset'=>'ASCII',
			), ),
		);
	static $tr=array(
		'en'=>array(
			'form.title'=>'articles',
			'id.lbl'=>'ID',
			'_parent.lbl'=>'folder',
			'title.lbl'=>'title',
			'nr.lbl'=>'nr',
			'_disabled.lbl'=>'hidden',
			'file.lbl'=>'picture',
			'file_caption.lbl'=>'picture caption',
			'summary.lbl'=>'summary',
			'text.lbl'=>'text',
			'files.lbl'=>'pictures',
			'files.nr.lbl'=>'nr',
			'files.file.lbl'=>'file',
			'seo.lbl'=>'SEO',
			'title_alias.lbl'=>'URL alias',
			'meta_description.lbl'=>'<meta description>',
			'rank_up.lbl'=>'rank up',
			'rank_down.lbl'=>'rank down',
			'comments.lbl'=>'allow comments',
			'comments_mail.lbl'=>'send comments to e-mail',
			'updated.lbl'=>'updated',
			'save.lbl'=>'save',
			'read_more.lbl'=>'read more',
			'prev.lbl'=>'previous',
			'next.lbl'=>'next',
			'comments.form.title'=>'comments',
			'comments.id.lbl'=>'ID',
			'comments.time.lbl'=>'time',
			'comments.name.lbl'=>'name',
			'comments.message.lbl'=>'comment',
			'comments.captcha.lbl'=>'verification code',
			'comments.post.lbl'=>'post',
			'comments.user_ip.lbl'=>'IP',
			'comments.hostname.lbl'=>'domain',
			
			'_folder.id.lbl'=>'ID',
			'_folder._parent.lbl'=>'folder',
			'_folder.title.lbl'=>'title',
			'_folder.nr.lbl'=>'nr',
			'_folder._disabled.lbl'=>'hidden',
			'_folder.file.lbl'=>'picture',
			'_folder.summary.lbl'=>'summary',
			'_folder.seo.lbl'=>'SEO',
			'_folder.title_alias.lbl'=>'URL alias',
			'_folder.meta_description.lbl'=>'<meta description>',
			'_folder.updated.lbl'=>'updated',
			'_folder.save.lbl'=>'save',
			
			'rank_up.lbl'=>'rank up',
			'rank_down.lbl'=>'rank down',
			'comments.lbl'=>'allow comments',
			'comments_mail.lbl'=>'send comments to e-mail',
			
			'msg'=>'Comment has been saved!',
			'msg_value_required'=>'input required!',
			'msg_value_invalid'=>'invalid input!',
			'msg_value_unique'=>'input value must be unique!',
			'back.lbl'=>'back',
			),
		'et'=>array(
			'form.title'=>'artiklid',
			'id.lbl'=>'ID',
			'_parent.lbl'=>'kaust',
			'title.lbl'=>'pealkiri',
			'nr.lbl'=>'nr',
			'_disabled.lbl'=>'peidetud',
			'file.lbl'=>'pildifail',
			'file_caption.lbl'=>'pildiallkiri',
			'summary.lbl'=>'sissejuhatus',
			'article_content.lbl'=>'artikli sisu',
			'text.lbl'=>'sisutekst',
			'files.lbl'=>'pildid',
			'files.nr.lbl'=>'nr',
			'files.file.lbl'=>'fail',
			'seo.lbl'=>'SEO',
			'title_alias.lbl'=>'URL alias',
			'meta_description.lbl'=>'<meta description>',
			'rank_up.lbl'=>'liiguta üles',
			'rank_down.lbl'=>'liiguta alla',
			'comments.lbl'=>'luba kommentaarid',
			'comments_mail.lbl'=>'saada kommentaarid e-postiga',
			'updated.lbl'=>'muudetud',
			'save.lbl'=>'salvesta',
			'read_more.lbl'=>'loe edasi',
			'prev.lbl'=>'eelmine',
			'next.lbl'=>'järgmine',
			'comments.form.title'=>'Kommentaarid',
			'comments.id.lbl'=>'ID',
			'comments.time.lbl'=>'aeg',
			'comments.name.lbl'=>'nimi',
			'comments.message.lbl'=>'kommentaar',
			'comments.captcha.lbl'=>'kontrollkood',
			'comments.post.lbl'=>'postita',
			'comments.user_ip.lbl'=>'IP',
			'comments.hostname.lbl'=>'domeen',
			
			'_folder.id.lbl'=>'ID',
			'_folder._parent.lbl'=>'kaust',
			'_folder.title.lbl'=>'pealkiri',
			'_folder.nr.lbl'=>'nr',
			'_disabled.lbl'=>'peidetud',
			'_folder.file.lbl'=>'pildifail',
			'_folder.summary.lbl'=>'sissejuhatus',
			'_folder.seo.lbl'=>'SEO',
			'_folder.title_alias.lbl'=>'URL alias',
			'_folder.meta_description.lbl'=>'<meta description>',
			'_folder.updated.lbl'=>'muudetud',
			'_folder.save.lbl'=>'salvesta',
			
			'msg'=>'Kommentaar salvestatud!',
			'msg_value_required'=>'välja tämine on kohustuslik!',
			'msg_value_invalid'=>'sisestatud väärtus ei ole sobiv!',
			'msg_value_unique'=>'väärtus peab olema kordumatu!',
			'back.lbl'=>'tagasi',
			),
		);
	function __construct(&$axs_local) {
		global $axs, $axs_content;
		if (isset($this->axs_articles_base)) return;
		else $this->axs_articles_base=true;
		$this->axs_local=&$axs_local;
		
		$this->site_nr=intval($axs['site_nr']);
		$this->db=$axs['cfg']['site'][$this->site_nr]['db'];
		$this->px=$axs['cfg']['site'][$this->site_nr]['prefix'];
		if (!isset($this->get)) $this->get=$_GET;
		$this->url_root=$axs['http_root'].$axs['site_dir'].'?';
		$this->url=(isset($this->e->url)) ? $this->e->url:$axs['url'];
		
		$this->names=array($this->axs_local['plugin'], $this->name);
		if (isset($this->fields_article['']['sql']['cols']['plugin'])) $this->fields_article['']['sql']['cols']['plugin']['_value'][]=$this->axs_local['plugin'];
		$sql_qry=array('site'=>$this->site_nr, 'plugin'=>$this->axs_local['plugin'], 'l'=>$this->axs_local['l'], 'c'=>$this->axs_local['c'], '_type'=>'', '_parent'=>0, );
		//foreach ($this->form->sql_add as $k=>$v) $this->form->sql_add[$k]=(isset($this->form->sql_add[$k])) ? $v:false;
		$this->sql_qry_banner=array('site'=>$this->site_nr, 'plugin'=>$this->axs_local['plugin'], 'l'=>$this->axs_local['l'], '_type'=>'', );
		if (empty($this->b)) $this->b='?'.axs_url($axs['url']);
		$this->fields_article['']['dir']=(is_dir(axs_dir('content').($tmp=$axs_local['dir'].$this->axs_local['c'].'_files/'))) ? $tmp:$axs_local['dir'];
		#<Customize>
		foreach (array_unique(array('articles.base', $this->_class, $this->_class_base, $this->axs_local['plugin'])) as $k=>$v) {
			foreach (array_reverse(axs_content::plugin_dirs()) as $kk=>$vv) {	if (file_exists($tmp=$vv.$v.'.config.php')) include($tmp);	}
			}
		#</Customize>
		#<Make content accessible for all languages />
		if ($this->lang_multi) {
			foreach (array('l', 'c') as $v) $sql_qry[$v]=$this->sql_qry_banner[$v]='';
			$this->fields_article['']['dir']='module.'.$this->_class.'/';
			$types=array('text'=>'text', 'textarea'=>'textarea', 'wysiwyg'=>'wysiwyg', );
			foreach ($this->fields_article as $k=>$v) if ((!empty($v['type'])) && (in_array($v['type'], $types))) {
				if (!isset($v['lang-multi'])) {
					$this->fields_article[$k]['lang-multi']=array();
					if (!isset($v['lang'])) $this->fields_article[$k]['lang']='';
					}
				if ((isset($v['lang-multi'])) && ($v['lang-multi']===false)) unset($this->fields_article[$k]['lang-multi']);
				}
			foreach ($this->fields_folder as $k=>$v) if ((!empty($v['type'])) && (in_array($v['type'], $types))) {
				if (!isset($v['lang-multi'])) {
					$this->fields_folder[$k]['lang-multi']=array();
					if (!isset($v['lang'])) $this->fields_folder[$k]['lang']='';
					}
				if ((isset($v['lang-multi'])) && ($v['lang-multi']===false)) unset($this->fields_folder[$k]['lang-multi']);
				}
			}
		#<Determine if an article is open. Don't open article in a banner section and prevent opening an article from another module. />
		$this->article=(($this->axs_local['c']===$axs['c']) && (!empty($_GET[$this->key_article]))) ? intval($_GET[$this->key_article]):null;
		if ($this->article) $this->url[$this->key_article]=$this->article;
		if (defined('AXS_SITE_NR')) {
			$this->form=new axs_form_header(array('site_nr'=>$this->site_nr, 'url'=>$this->url, 'dir'=>$this->fields_article['']['dir'], 'visibility'=>$this->db_visibility_site));
			}
		else $this->form=new axs_form_edit(array('site_nr'=>$this->site_nr, 'url'=>$this->url, 'dir'=>$this->fields_article['']['dir'], 'visibility'=>$this->db_visibility_admin, 'user_input'=>$_POST, ));
		if ($this->module) {
			if (!$this->f) $this->f=(isset($this->forms[axs_get('f', $this->get)])) ? $_GET['f']:key($this->forms);
			$this->form->sql_header_get($this->module, $this->f, false);
			$this->form->tr->set($this->form->structure_get_labels());
			foreach ($this->forms as $k=>$v) {
				$this->forms[$k]['label']=$this->form->sql_header_cfg_get($this->module, $k, 'label');
				$this->forms[$k]['label.html']=$this->forms[$k]['label.html']=axs_html_safe($this->forms[$k]['label']);
				}
			}
		else {
			$tmp=array('px'=>$this->px, 'name'=>$this->name, 'plugin'=>$this->axs_local['plugin'], );
			if (isset($this->fields_article[''])) $this->fields_article['']=axs_form::structure_set_cfg_parse($this->fields_article[''], $tmp);
			if (isset($this->fields_comment[''])) $this->fields_comment['']=axs_form::structure_set_cfg_parse($this->fields_comment[''], $tmp);
			$this->form->structure_set($this->fields_article);
			$this->forms=array($this->axs_local['plugin']=>array('label'=>$this->form->tr->t('form.title')), );
			}
		if ($tmp=axs_content::tr_get(array(), __FILE__, __LINE__, $axs_local['c'], $axs_local['dir'], $axs_local['l'])) $this->form->tr->set($tmp);
		$this->form->structure_set_labels();
		$this->form->sql_add=$sql_qry;
		if (!empty($this->fields_comment)) $this->fields_comment['']['sql']['table']=$this->form->structure['']['sql']['table'].'_comments';
		if (defined('AXS_SITE_NR')) foreach(array('_enabled'=>1, '_disabled'=>0, ) as $k=>$v) {
			if (isset($this->form->structure[$k])) $this->form->sql_add[$k]=$this->sql_qry_banner[$k]=$v;
			}
		$this->search=new axs_form_search($this->form->structure, $this->get, $this->url, false, $this->sql_limit, ((defined('AXS_SITE_NR'))?$this->db_visibility_site_search:$this->db_visibility_admin_search));
		$this->pager=new axs_pager(axs_get('p', $_GET), (array)$this->sql_limit, axs_get('ps', $_GET));
		$this->vr['title']=axs_get('title', current(axs_content::index_c_find($this->axs_local['c'])));
		$this->vr['url']=axs_content::url($axs['url'], array('c'=>$this->axs_local['c']));
		} #</__construct()>
	function banner_init() {
		global $axs, $axs_content;
		if ($tmp=axs_file_choose($this->_class.'.js', 'http')) $axs['page']['head'][$this->_class.'.js']='<script src="'.$tmp.'"></script>'."\n";
		$this->vr['plugin']=preg_replace('/_banner$/', '', $this->axs_local['plugin']);
		$this->content_index+=axs_content::index_c_find('', $this->vr['plugin']);
		$this->c=key($this->content_index);
		$this->axs_local['vr']['url']=$this->url_root.axs_url($this->url, array('c'=>$this->c, ), false);
		$this->vr['url']=htmlspecialchars($this->axs_local['vr']['url']);
		$this->vr['title']=axs_html_safe($this->axs_local['title']);
		$this->vr['list']='';
		$this->vr['c']=$this->axs_local['c'];
		$this->vr['/']=$axs['http_root'];
		foreach ($this->banner_init_tpl() as $k=>$v) $this->tpl[$k]=$v;
		if (isset($this->sql_qry_banner['plugin'])) $this->sql_qry_banner['plugin']=$this->vr['plugin'];
		$this->form->sql_add=$this->sql_qry_banner;
		$this->limit=($this->sql_limit_banner) ? ' LIMIT '.$this->sql_limit_banner:'';
		$this->sql_query="SELECT * FROM `".$this->form->sql_table."`\n".
		"	WHERE `_type`!='_folder'".$this->form->sql_add(' AND ', '' , ' AND ')."\n".
		"	".$this->form->sql_order('ORDER BY ').$this->limit;
		$this->list_articles_data=false;
		//$this->_lightbox($this->axs_local['c'].'_banner');
		} #</banner_init()>
	function banner_init_tpl() {
		global $axs, $axs_content;
		$a=array(''=>'', 'list.item'=>'list.', 'list.item._folder'=>'list._folder.');
		if (!$this->folders) unset($a['list.item._folder']);
		foreach ($a as $k=>$v) {
			$tmp=($this->axs_local['style']) ? array($this->axs_local['style'].'.banner.'.$v.'tpl', ):array();
			$tmp+=array($this->axs_local['plugin'].'.banner.'.$v.'tpl', $this->_class.'.banner.'.$v.'tpl', $this->_class_base.'.banner.'.$v.'tpl', );
			$tpl[$k]=axs_tpl(false, $tmp, __FILE__, __LINE__);
			}
		return $tpl;
		} #</banner_init_tpl()>
	function banner_parse() {
		global $axs, $axs_content;
		#<Closable banner>
		$open=(isset($_COOKIE[$this->axs_local['c']])) ? $_COOKIE[$this->axs_local['c']]:'open';
		if ($axs['c']==$this->axs_local['c']) {
			$open='open';
			if (isset($_COOKIE[$this->axs_local['c']])) setcookie($this->axs_local['c'], $open, 0);
			}
		if ($open==='closed') return '';
		#</Closable banner>
		if ($tmp=$this->error()) return $tmp;
		if (!is_array($this->list_articles_data)) $this->list_articles_data=axs_db_query($this->sql_query, 1, $this->db, __FILE__, __LINE__);
		foreach ($this->list_articles_data as $cl) {
			$vr=array();
			$this->list_articles_row($cl, $vr);
			$vr['_class']=implode(' ', $vr['_class']);
			$this->vr['list'].=axs_tpl_parse($this->tpl['list.item'.(($cl['_type'])?'.'.$cl['_type']:'')], array_merge($cl, $vr));
			}
		return axs_tpl_parse($this->tpl[''], $this->vr+$this->form->tr->get());
		} #</banner_parse()>
	function banner_folders() {
		global $axs;
		//$this->form->sql_add['plugin']=$this->_class;
		$vr=array(
			'c'=>key(axs_content::index_c_find(false, $this->_class)),
			'title'=>axs_html_safe($this->axs_local['title']),
			);
		$this->article=($vr['c']===$axs['c']) ? intval(axs_get('id', $_GET)):false;
		$vr['menu']=$this->nav_html($this->nav_tree());
		return axs_tpl_parse(axs_tpl(false, $this->axs_local['plugin'].'.tpl', __FILE__, __LINE__), $vr);
		} #</banner_folders()>
	function calendar_init() {
		global $axs, $axs_content;
		$this->calendar=true;
		include_once(axs_file_choose($axs['f_px'].'calendar.class.php'));
		$this->calendar=new axs_calendar($axs['l'], axs_get('month', $_REQUEST), axs_get('year', $_REQUEST), axs_get('week', $_REQUEST), axs_get('day', $_REQUEST), axs_content::tr_get());
		$this->vr['c']=$this->calendar->url['c']=$this->axs_local['c'];
		//$this->calendar->dates_get_holiday();
		//$this->calendar->dates_get_national();
		//$this->calendar->dates_get_folklore();
		$field=$this->form->structure_get_by_attr(array('order','ASC','DESC'));
		$col=key($field);
		$field=current($field);
		if (preg_match('/.*-between$/', $field['type'])) {
			$col_nr='_1';
			$where=" AND (`".$col."_2`=0 OR `".$col."_2` BETWEEN '".$this->calendar->cal_start_timestamp."' AND '".$this->calendar->cal_end_timestamp."')";
			}
		else $col_nr=$where='';
		$this->calendar_sql="SELECT `id`, `".$col.$col_nr."` AS `time`, `title` AS `text` FROM `".$this->form->sql_table."`\n".
		"	WHERE `site`='".$this->site_nr."' AND `l`='".$axs['l']."' AND `".$col.$col_nr."` BETWEEN '".$this->calendar->cal_start_timestamp."' AND '".$this->calendar->cal_end_timestamp."'".$where."\n".
		"	".$this->form->sql_order('ORDER BY ');
		
		if (array_sum($this->calendar->selected)) $this->sql_where['calendar']=" `publ` BETWEEN '".$this->calendar->start_timestamp."' AND '".$this->calendar->end_timestamp."'";
		else {
			$this->sql_where['calendar']=" `publ`>'".($axs['time']-$this->date_new)."'";
			$this->vr['time_between']=date('d.m.Y').'&hellip;';
			}
		} #</calendar_init()>
	function calendar_parse($cal_id=false) {
		global $axs, $axs_content;
		if ($cal_id) $this->calendar->cal_id=$cal_id;
		if ($this->axs_local['section']!=='content') {
			if ($tmp=axs_file_choose(array($this->axs_local['plugin'].'.banner.tpl', $this->name.'.banner.tpl'))) $this->calendar->templates['cal']=axs_tpl('', $tmp);
			}
		$this->vr['time_between']=$this->calendar->time_between_get('d.m.Y', ' - ');
		$dates=($this->calendar_sql) ? $this->calendar->dates_get_sql('', $this->calendar_sql):array();
		foreach ($dates as $month=>$days) foreach ($days as $date=>$day) foreach ($day as $k=>$cl) {
			$cl['time']=date('H:i', $cl['time']);
			$cl['url']='?'.axs_url($axs['url'], array('c'=>$this->axs_local['c'], 'year'=>urlencode($_REQUEST['year']), 'month'=>urlencode($_REQUEST['month']), 'day'=>urlencode($_REQUEST['day']), ));
			$dates[$month][$date][$k]=array('class'=>$cl['class'], 'text'=>'<a href="'.$cl['url'].'">'.$cl['time'].'</a> '.nl2br($cl['text']), );
			}
		$this->calendar->dates_set($dates);
		$this->calendar->url_set_date_events('?', array_merge($axs['url'], array('c'=>$this->axs_local['c'])));
		$axs['page']['head']['calendar_css']=$this->calendar->css_get();
		return $this->calendar->calendar_code_get('compact');
		} #</calendar_parse()>
	function page_init($plugin_def) {
		global $axs;
		$this->plugin_def=$plugin_def;
		$js=(isset($axs['page']['head'][$this->axs_local['plugin'].'.plugin.js'])) ? $axs['page']['head'][$this->axs_local['plugin'].'.plugin.js']:'';
		unset($axs['page']['head'][$this->axs_local['plugin'].'.plugin.js']);
		if ($tmp=axs_file_choose($this->_class_base.'.plugin.js', 'http')) $axs['page']['head'][$this->_class_base.'.plugin.js']='<script src="'.$tmp.'"></script>'."\n";
		if ($js) $axs['page']['head'][$this->axs_local['plugin'].'.plugin.js']=$js;
		if ($tmp=axs_file_choose(array($this->axs_local['plugin'].'.plugin.css', $this->_class_base.'.plugin.css'), 'http')) $axs['page']['head'][$this->axs_local['plugin'].'.plugin.css']='<link type="text/css" rel="stylesheet" href="'.$tmp.'" media="all" />'."\n";
		$this->_lightbox($this->axs_local['c'].'_list');
		} #</page_init()>
	function page_parse() {
		$tpl=array();
		foreach ($this->names as $k=>$v) {	$tpl[]=$v.'.tpl';	}
		$tpl=array('list'=>axs_tpl(false, $tpl, __FILE__, __LINE__), );
		if (axs_tpl_has($tpl['list'], '_article')) return axs_tpl_parse($this->list_articles(), $this->vr/*+$this->form->tr->get()*/);
		if (($this->article) && /*(!isset($_GET['p'])) &&*/ ($this->axs_local['section']==='content')) $html=$this->display_article($this->article);
		else $html=$this->list_articles();
		return axs_tpl_parse($html, $this->vr/*+$this->form->tr->get()*/);
		} #</page_parse()>
	
	#<List articles>
	function list_articles() {
		global $axs;
		$tpl=array();
		foreach ($this->names as $k=>$v) {
			if (($this->pager->p===1) && ($this->level===1)) $tpl[]=$v.'.first-page.tpl';
			if ($this->level>1) $tpl[]=$v.'.level2.tpl';
			$tpl[]=$v.'.tpl';
			}
		$tpl=array('list'=>axs_tpl(false, $tpl, __FILE__, __LINE__), );
		$tpl['list.item']=axs_tpl(false, array($this->axs_local['plugin'].'.list.tpl', $this->name.'.list.tpl'), __FILE__, __LINE__);
		if ($this->folders) {
			foreach (array(''=>'', '.thumb'=>'') as $k=>$v) $tpl['list.item._folder'.$k]=axs_tpl(false, array($this->axs_local['plugin'].'.list._folder'.$k.'.tpl', $this->name.'.list._folder'.$k.'.tpl'), __FILE__, __LINE__);
			$this->tpl['thumbs']=$tpl['list.item._folder.thumb'];
			}
		$vr=array('title'=>axs_html_safe($this->vr['title']), 'url'=>$this->vr['url'], '_articles'=>'', '_class'=>array(), '/'=>$axs['http_root'], 'c'=>$this->axs_local['c'], );
		if (axs_tpl_has($tpl['list'], '_content')) $vr['_content']=$this->vr['_content'];
		if (axs_tpl_has($tpl['list'], '_nav_tree')) $vr['_nav_tree']=$this->nav_html($this->nav_tree());
		if (axs_tpl_has($tpl['list'], '_nav_years')) $vr['_nav_years']=$this->nav_html($this->nav_years());
		#<Search>
		$this->search->elements('t');
		if ($tmp=$this->search->sql_where('')) $this->sql_where[]=$tmp;
		$vr['_search']=$this->search->form();
		if ($this->search->search_count) unset($this->form->sql_add['_parent']);
		#</Search>
		$this->list_articles_query($this->sql_where);
		
		$article=null;
		if (axs_tpl_has($tpl['list'], '_article')) {
			reset($this->list_articles_data);
			$this->article_first=axs_get('id', current($this->list_articles_data), false);
			$article=($this->article) ? $this->article:$this->article_first;
			}
		
		$urls=array();
		$nr=1;
		foreach ($this->list_articles_data as $cl) {
			$vl=array('nr'=>$nr++, );
			$this->list_articles_row($cl, $vl);
			$vl['_class']=implode(' ', $vl['_class']);
			$urls[$cl['id']]=$cl['back_url'];
			$vr['_articles'].=axs_tpl_parse($tpl['list.item'.(($cl['_type'])?'.'.$cl['_type']:'')], array_merge($cl, $vl));
			}
		$vr['search.found']=$vr['pager.total']=$this->form->sql_found_rows;
		if ($vr['pager.total']>$this->pager->psize) {
			$tmp=$this->search->url;
			if (!$this->search->search_count) unset($tmp[urlencode('axs[search]')]);
			$vr['pager.pages']=$this->pager->pages($vr['pager.total'], $this->url_root.axs_url($tmp, array('p'=>'')));
			$vr['pager.prev']=$vr['pager.pages']['prev'];
			$vr['pager.next']=$vr['pager.pages']['next'];
			$vr['pager.pages']=$vr['pager.pages']['pages'];
			}
		else $vr['pager.prev']=$vr['pager.next']=$vr['pager.pages']='';
		$this->pager->back_url_set($this->axs_local['c'], $urls);
		$vr['_back']=($this->level<2) ? '':'<a class="back" href="'.htmlspecialchars($this->pager->back_url_get(
			$this->axs_local['c'],
			$this->article_parent['id'],
			$this->url_root.axs_url($this->url, array('t'=>$this->article_parent['title_alias'], 'id'=>$this->article_parent['id'], ))
			)).'">'.$this->form->tr->s('back.lbl').'</a>';
		if ($article!==null) {
			$vr['_article']=$this->display_article($article);
			if (!$this->article) $this->level--;
			}
		$tmp=count($this->list_articles_data);
		$vr['_class'][]='count'.$tmp;
		$vr['_class'][]=($tmp&1) ? 'odd':'even';
		$vr['_class']=implode(' ', $vr['_class']);
		$axs['page']['class']['articles-level']='articles-level'.$this->level;
		$axs['page']['class']['articles-page']='articles-page'.$this->pager->p;
		return axs_tpl_parse($tpl['list'], $vr+$this->form->tr->get());
		} #</list_articles()>
	function list_articles_query($sql=array()) {
		global $axs;
		$q=(!is_string($sql)) ? "SELECT SQL_CALC_FOUND_ROWS t.* FROM `".$this->form->sql_table."` AS t\n".
		$this->form->sql_add(' AND ', 't' , '	WHERE ', $sql)."\n".
		$this->search->sql_order('	ORDER BY ')." LIMIT ".$this->pager->start.','.$this->pager->psize:$sql;
		$r=axs_db_query($q, 1, $this->db, __FILE__, __LINE__);
		$r=$this->form->sql_result_set($r);
		#dbg($sql,$q,$r);
		return $this->list_articles_data=$r;
		} #</list_articles_query()>
	function list_articles_row(&$cl, &$vr=array()) {
		global $axs;
		$vr['_class']=array();
		if (!empty($cl['_type'])) $vr['_class']['_type']=$cl['_type'];
		if (($this->article==$cl['id']) or ((!$this->article) && ($this->article_first==$cl['id']))) $vr['_class']['current']='current';
		$tmp=(isset($cl['publ_1'])) ? $cl['publ_1']:axs_get('publ', $cl);
		if ($tmp) {	if ($tmp+$this->date_new>$axs['time']) $vr['_class']['_new']='_new';	}
		$this->_type_set($cl['_type']);
		$this->_article_meta($cl);
		#$cl['url']=$this->vr['url'];
		foreach (array('_title', 'article_url', 'article_parent_url', ) as $v) $vr[$v]=htmlspecialchars($cl[$v]);
		foreach ($this->form->structure_get() as $k=>$v) {
			$vr[$k]=$this->form->value_display($k, $v, $cl, $vr);
			}
		$vr['menu_url']='?'.axs_url($axs['url'], array('c'=>$cl['c']));
		$vr['menu_title']=$this->content_index[$cl['c']]['title'];
		$tmp=(isset($this->content_index[$cl['c']]['path'])) ? $this->content_index[$cl['c']]['path']:array();
		foreach (axs_content::menu_pic_get($tmp) as $k=>$v) $vr['menu_img'.$k]=($v) ? $axs['http_root'].$axs['site_dir'].$axs['dir_c'].$v:'';
		$vr['menu_img_if']=($vr['menu_img']) ? $vr['menu_img']:$vr['menu_title'];
		if (isset($vr['thumbs'])) $vr['thumbs']=$this->list_articles_folder_thumbs($cl);
		$cl['back_url']=$_SERVER['REQUEST_URI'].'#id'.$cl['id'];
		} #</list_articles_row()>
	function list_articles_folder_thumbs($cl) {
		if (empty($cl['thumbs'])) return '';
		if (!isset($this->list_articles_thumbs)) {
			$id=$this->list_articles_thumbs=array();
			foreach ($this->list_articles_data as $v) if (!empty($v['thumbs'])) {
				$where=(isset($v['_disable'])) ? " AND !`_disable`":'';
				$id[$v['id']]="(SELECT `id`, `_parent` FROM `".$this->form->sql_table."` WHERE `_parent`='".$v['id']."' AND `_type`!='_folder'".$where." ORDER BY `".$this->form->rank."` LIMIT ".$v['thumbs'].")";
				}
			$result=axs_db_query(implode("\n	UNION\n", $id), 1, $this->db, __FILE__, __LINE__);
			foreach ($result as $k=>$v) $this->list_articles_thumbs[$v['_parent']][$v['id']]=$v;
			}
		$html='';
		if (!isset($this->tpl['thumbs'])) $this->tpl['thumbs']=axs_tpl(axs_dir('site.base'), 'articles.list._folder.thumb.tpl');
		if (!empty($this->list_articles_thumbs[$cl['id']])) foreach ($this->list_articles_thumbs[$cl['id']] as $k=>$v) {
			$v['file']=$v['id'].'.t1.jpg';
			$v['url']=$this->form->dir_http.$v['id'].'/'.$v['file'];
			foreach ($v as $kk=>$vv) $v[$kk]=axs_html_safe($vv);
			$html.=axs_tpl_parse($this->tpl['thumbs'], $v);
			}
		return $html;
		} #</list_articles_folder_thumbs()>
	#</List articles>
	#<Display article>
	function display_article($id=false) {
		global $axs, $axs_content;
		if ($id===false) return '';
		$this->tpl['article']=axs_tpl(false, array($this->axs_local['plugin'].'.article.tpl', $this->name.'.article.tpl'));
		$cl=$this->display_article_query($id);
		if (empty($cl['id'])) {
			if ($this->log_id) axs_log(__FILE__, __LINE__, $this->name, 'Invalid ID @ '.$this->axs_local['plugin'].'.plugin');
			header('HTTP/1.0 404 Not Found');
			return '<p class="msg" lang="en">404 Not Found</p>';
			}
		if (!isset($cl['_type'])) $cl['_type']='';
		$this->vr['title']=$cl['_title']=$this->form->structure_get_item_title($cl);
		foreach (array('title_alias'=>'', 'meta_description'=>'', ) as $k=>$v) $cl[$k]=$this->_field_get($cl, $k);
		foreach ($this->nav_path_get($cl, 1) as $k=>$v) $this->nav_path_set($v);
		if ((isset($this->form->structure['meta_description'])) && ($cl['meta_description'])) $axs['page']['description']=axs_html_safe($cl['meta_description']);
		$vr=array('_class'=>array(), '_title'=>axs_html_safe($this->vr['title']), '/'=>$axs['http_root'], );
		//$this->vr['_articles_parent']=$this->list_articles();
		if ($cl['_type']==='_folder') {
			$this->level++;
			$this->article_parent=$cl;
			if ($this->folders==='open') {
				$this->form->sql_add['_parent']=$this->nav_tree_children($id);
				$this->form->sql_add['_type']='';
				}
			else $this->form->sql_add['_parent']=$id;
			$this->url['t']=$cl['title_alias'];
			$this->url['id']=$cl['id'];
			$this->_type_set($cl['_type']);
			foreach ($this->form->structure_get() as $k=>$v) {
				$this->vr[$k]=$this->form->value_display($k, $v, $cl, $vr);
				}
			$this->_type_set('');
			return $this->list_articles();
			}
		$this->display_article_vars($cl, $vr);
		$vr['_class']=implode(' ', $vr['_class']);
		return axs_tpl_parse($this->tpl['article'], array_merge($cl, $vr)+$this->form->tr->get());
		} #</display_article()>
	function display_article_query($id=false) {
		$where=array();
		foreach ($this->form->sql_add as $k=>$v) $where[$k]="`".$k."`='".addslashes($v)."'";
		unset($where['_parent']);
		$prevnext=(axs_tpl_has($this->tpl['article'], 'article.prev')) ? ",\n".$this->form->sql_select_prev_next($this->form->sql_table, $this->form->rank, 't', implode(' AND ', $where)):'';
		$this->cl=$cl=axs_db_query($q="SELECT `_type`, `_parent`, ".$this->form->sql_select('t').
		$prevnext."\n".
		"	FROM `".$this->form->sql_table."` AS t\n".
		"	".$this->form->sql_tables_join('t')."\n".
		"	WHERE t.`id`='".$id."'".(($where) ? ' AND '.implode(' AND ', $where):''), 'row', $this->db, __FILE__, __LINE__);
		if (!empty($cl['id'])) $this->form->sql_query_element_table($cl['id'], $cl);
		$cl=$this->form->sql_values_get($cl);
		#dbg($q, $cl);
		return $cl;
		} #</display_article_query()>
	function display_article_vars(&$cl=array(), &$vr=array()) {
		global $axs, $axs_content;
		$vr['article_url']=$this->url_root.axs_url($this->url, array('id'=>$cl['id'], 'p'=>false, ));
		$vr['back']=$this->pager->back_url_get($this->axs_local['c'], $cl['id'], $this->url_root.axs_url($this->url, array('t'=>false, 'id'=>false, )));
		if (isset($cl['title_alias'])) $this->url['t']=$cl['title_alias'];
		$this->search->elements('t');
		$this->article=$this->comments_id=$cl['id'];
		$this->url['id']=$cl['id'];
		
		$this->pic=false;
		foreach ($this->form->structure_get() as $k=>$v) {
			if ($v['type']==='table') {
				if ($tmp=axs_file_choose(array(
					$this->axs_local['plugin'].'.article.'.$k.'.item.tpl', $this->_class_base.'.article.'.$k.'.item.tpl', $this->axs_local['plugin'].'.article.element.table.item.tpl'
					))) $this->form->structure[$k]['tpl']=$v['tpl']=axs_tpl('', $tmp);
				}
			$vr[$k]=$this->form->value_display($k, $v, $cl, $vr);
			if ($v['type']==='file') foreach ($v['t'] as $kk=>$vv) {
				if (!$this->pic) $this->pic=array_merge(array('key'=>$k.'.'.$kk), $vv); #This has to be after axs_form::element_file_data() has run!
				}
			}
		$vr['article.prev']=(!empty($cl['_prev'])) ? 
			'<a href="'.$this->url_root.axs_url($this->url, array('id'=>$cl['_prev'])).'#article" class="prev">'.$this->form->tr->s('prev.lbl').'</a>':
			'';
		$this->vr['article.next']=(!empty($cl['_next'])) ? 
			'<a href="'.$this->url_root.axs_url($this->url, array('id'=>$cl['_next'])).'#article" class="next">'.$this->form->tr->s('next.lbl').'</a>':
			'';
		$vr['article.total']=axs_get('_total', $cl);
		$this->display_article_xtab('text', $cl, $vr);
		$this->display_article_comments('comments', $cl, $vr);
		if (axs_tpl_has($this->tpl['article'], 'qr_code')) $vr['qr_code']=$this->_qr_code_files($this->qr_code, $cl);
		
		$tmp=array('title'=>$vr['_title'], 'type'=>'article', 'description'=>axs_get('meta_description', $cl), 'updated_time'=>(!empty($this->cl['updated']))?date('Y-m-d\\TH:i', $this->cl['updated']):'', );
		if (!empty($vr[$this->pic['key'].'.img'])) {
			$tmp['image']=$vr[$this->pic['key'].'.url'];
			$tmp['image.file']=$this->pic['f_fs'];
			}
		axs_socialmedia::og_meta($tmp);
		$vr['_socialmedia']=implode("\n", axs_socialmedia::btn($this->socialmedia));
		
		if (axs_tpl_has($this->tpl['article'], '_articles')) $vr['_articles']=$this->list_articles();
		$axs['page']['class']['articles-level']='articles-level-article';
		} #</display_article_vars()>
	function display_article_xtab($col, &$cl=array(), &$vr=array()) {
		global $axs;
		if ((empty($this->fields_article[$col])) or ($this->fields_article[$col]['type']!=='hidden')) return '';
		return $vr[$col]=axs_tab($cl[$col], axs_tpl(false, 'deflt.tpl'), '', array('time'=>$axs['time'], ));
		} #</display_article_xtab()>
	function display_article_comments($col, &$cl=array(), &$vr=array()) {
		global $axs, $axs_content;
		$vr[$col.'.count']=0;
		if (empty($cl[$col])) return $vr[$col]='';
		$this->fields_comment['']['mailto']=$cl['comments_mail'];
		$this->fields_comment['']['mail_name_field']='name';
		$this->fields_comment['']['posts_save']=$this->sql_limit_comments;
		$this->fields_comment['']['posts_show']='asc';
		$this->fields_comment['']['tr']=$this->form->tr;
		$this->fields_comment['']['sql']['table']=$this->form->sql_table.'_comments';
		foreach ($this->fields_comment as $k=>$v) if ($k) $this->fields_comment[$k]['label']=$this->form->tr->s('comments.'.$k.'.lbl');
		$f=new axs_form_site($this->axs_local, $this->fields_comment, $_POST);
		$f->url=$this->url;
		$f->sql_parent_id=$this->comments_id;
		foreach (array('form.posts'=>'comments.form','form.posts.item'=>'comments.list') as $k=>$v) if ($tmp=axs_file_choose(array($this->axs_local['plugin'].'.'.$v.'.tpl', $this->name.'.'.$v.'.tpl'))) $f->tpl[$k]=axs_tpl('', $tmp);
		$f->post();
		//return $vr[$col]=$f->build();
		$vars=array('comments.form.title'=>$this->form->tr->s('comments.form.title'), 'comments'=>$f->build(), 'total'=>$f->vr['total'], );
		$vr[$col.'.count']=$f->vr['total'];
		return $vr[$col]=axs_tpl_parse(axs_tpl(false, array($this->axs_local['plugin'].'.comments.tpl', $this->name.'.comments.tpl')), $vars);
		} #</display_article_comments()>
	#</Display article>
	#<Nav/structure functions>
	function nav_path_get($table, $get=false) {
		if ($get) $table=array($get=>$table);
		if (($this->article_parent) && (empty($this->articles_index[$this->article_parent['id']]))) $this->articles_index[$this->article_parent['id']]=$this->article_parent;
		foreach ($table as $cl){
			if (!isset($cl['_parent'])) $cl['_parent']=0;
			if (($cl['_parent']) && (!isset($this->articles_index[$cl['_parent']]))) $this->articles_index[$cl['_parent']]=array();
			}
		$q=array();
		foreach ($this->articles_index as $k=>$v) if (empty($v)) $q[$k]="`id`='".($k+0)."'";
		if ($q) $q=axs_db_query("SELECT * FROM `".$this->form->sql_table."` WHERE ".implode(" OR ", $q), 1, $this->db, __FILE__, __LINE__);
		foreach ($q as $cl) $this->articles_index[$cl['id']]=$cl;
		foreach ($table as $k=>$cl) {
			$cl['_path']=array($cl);
			for ($i=0, $p=$cl['_parent']; ($p) && ($i<100); $i++) {
				if (!isset($cl['_parent'])) $cl['_parent']=0;
				if (empty($this->articles_index[$p])) $this->articles_index[$p]=axs_db_query("SELECT * FROM `".$this->form->sql_table."` WHERE `id`='".($p+0)."'", 'row', $this->db, __FILE__, __LINE__);
				$cl['_path'][]=$this->articles_index[$p];
				$p=$this->articles_index[$p]['_parent'];
				}
			foreach ($cl['_path'] as $kk=>$vv) $cl['_path'][$kk]=array('id'=>$vv['id'], '_title'=>$this->form->structure_get_item_title($vv), 'title_alias'=>$this->_field_get($vv, 'title_alias'), );
			$table[$k]['_path']=array_reverse($cl['_path']);
			}
		if ($get) return $table[$get]['_path'];
		return $table;
		} #</nav_path_get()>
	function nav_path_set($cl) {
		global $axs, $axs_content;
		$axs_content['content'][$axs['c']]['path'][]=array(
			'c'=>$axs['c'], 'title'=>$cl['_title'], 'url'=>$this->url_root.axs_url($this->url, array('t'=>$cl['title_alias'], 'id'=>$cl['id'], 'p'=>null), false),
			);
		} #</nav_path_set();
	function nav_cat($id, $select_first=false) {
		global $axs;
		if (($select_first) && (!isset($this->get[$id]))) {
			$tmp=$this->form->structure[$id]['options'];
			unset($tmp['']);
			reset($tmp);
			$tmp=key($tmp);
			$this->get[$id]=$this->search->vl[$id]=$tmp.'';
			}
		if (isset($this->get[$id])) $this->url[$id]=$this->get[$id];
		$this->vr[$id]='      <ul id="'.$id.'">';
		foreach ($this->form->structure[$id]['options'] as $k=>$v) {
			$v['url']=($k) ? array($id=>$k):array($id=>'');
			if (axs_get($id, $this->get)===$k.'') {
				$this->form->sql_add[$id]=$k;
				$v['current']=' current';
				$v['label.html']='<strong>'.$v['label.html'].'</strong>';
				}
			else $v['current']='';
			$this->vr[$id].='      <li class="'.$id.$k.$v['current'].'"><a href="'.$this->url_root.axs_url($this->url, $v['url']).'">'.$v['label.html'].'</a></li>'."\n";
			}
		$this->vr[$id].='      </ul>';
		return $this->vr[$id];
		} #</nav_cat()>
	function nav_html($menu, $level=1, $spc='	') {
		$html=$spc.'<ul>';
		foreach ($menu as $k=>$v) {
			$v['class']=array();
			if (is_array($v['url'])) $v['url']='?'.axs_url($this->url, $v['url'], false);
			$v['title']=axs_html_safe($v['title']);
			if (isset($v['submenu'])) {
				$v['class'][]='submenu';
				if (!empty($v['open'])) $v['class'][]='open';
				$v['submenu']=$this->nav_html($v['submenu'], $level+1, $spc.'	');
				}
			else $v['submenu']='';
			if ($v['current']) {
				$v['class'][]='current';
				$v['title']='<strong>'.$v['title'].'</strong>';
				}
			$html.=$spc.'	<li class="'.implode(' ', $v['class']).'"><a href="'.htmlspecialchars($v['url']).'">'.$v['title'].'</a>'.$v['submenu'].'</li>'."\n";
			}
		$html.=$spc.'</ul>';
		return $html;
		} #</nav_html()>
	function nav_tree($parent=0) {
		global $axs, $axs_content;
		if (!isset($this->nav_tree)) {
			$order=(!empty($this->form->rank)) ? ", t.`".$this->form->rank."`":'';
			$this->nav_tree=axs_db_query($q="SELECT * FROM `".$this->form->sql_table."` AS t".$this->form->sql_add(' AND ', 't' , ' WHERE ', array('_type'=>'_folder', '_parent'=>null, ))." ORDER BY t.`_parent`".$order, 'k', $this->db, __FILE__, __LINE__);
			}
		#dbg($q, $this->nav_tree);
		$menu=array();
		foreach ($this->nav_tree as $id=>$cl) if (intval($cl['_parent'])===intval($parent)) {
			$this->_article_meta($cl);
			$cl['title']=$cl['_title'];
			$cl['url']=$cl['article_url'];
			$cl['current']=(($cl['c']===$axs['c']) && ($cl['id'].''===$this->article.'')) ? true:false;
			$menu[$id]=$cl;
			}
		foreach ($menu as $id=>$cl) if ($tmp=$this->nav_tree($cl['id'])) {
			foreach ($tmp as $k=>$v) if ((!empty($v['open'])) || (!empty($v['current']))) $menu[$id]['open']=true;
			$menu[$id]['submenu']=$tmp;
			}
		if ($parent==0) $this->nav_tree=$menu;
		return $menu;
		} #</nav_tree()>
	function nav_tree_children($parent, $node=false, $current=false) {
		if (!$parent) return array();
		if ($node===false) {
			if (!isset($this->nav_tree)) $this->nav_tree();
			$node=$this->nav_tree;
			}
		$found=array();
		foreach ($node as $id=>$cl) {
			if ($cl['_parent']+0===$parent+0) $current=true;
			if (($id+0===$parent+0) or ($current)) $found[$id]=$id;
			if (!empty($cl['submenu'])) $found+=$this->nav_tree_children($parent, $cl['submenu'], $current);
			}
		return $found;
		} #</nav_tree_children()>
	function nav_years($col='publ', $where=false) {
		if (($tmp=axs_get($col, $this->form->structure)) && ($tmp['type']==='timestamp-between')) $col.='_1';
		if ($where===false) $where=$this->form->sql_add(' AND ', '' , ' AND ');
		$a=$years=axs_db_query("SELECT FROM_UNIXTIME(MIN(`".$col."`),'%Y') AS `min`, FROM_UNIXTIME(MAX(`".$col."`),'%Y') AS `max` FROM `".$this->form->sql_table."` WHERE `".$col."`>0".$where, 'row', $this->db, __FILE__, __LINE__);
		if (empty($years['max'])) $years=array('min'=>date('Y'), 'max'=>date('Y'), );
		//$tmp=($years['max']-$years['min']<1) ? 1:$years['max']-$years['min'];
		$years=array_reverse(array_keys(array_fill($years['min'], $years['max']-$years['min']+1, '')));
		$this->nav_years=array('current'=>(!empty($_GET['y'])) ? intval($_GET['y']):current($years), 'list'=>array(), );
		$this->nav_years+=array('min'=>mktime(0, 1, 1, 1, 1, $this->nav_years['current']), 'max'=>mktime(23, 59, 59, 12, 31, $this->nav_years['current']), );
		$this->form->sql_add[/*'_nav_years'*/]="`".$col."` BETWEEN '".$this->nav_years['min']."' AND '".$this->nav_years['max']."'";
		foreach ($years as $k=>$v) $this->nav_years['list'][$v]=array('url'=>array('y'=>$v), 'title'=>$v, 'current'=>($v==$this->nav_years['current']), );
		$this->vr['Y']=$this->url['y']=$this->nav_years['current'];
		return $this->nav_years['list'];
		} #</nav_years()>
	#</Nav/structure functions>
	#<CMS content search functions>
	function content_search() {
		global $axs, $axs_content;
		if (empty($this->content_search)) return;
		$where=array();
		$this->pager->psize=1000;
		foreach ($this->content_search as $k=>$v) if ((isset($this->form->structure[$k])) && (!empty($this->form->structure[$k]['lang-multi']))) {
			$this->content_search[$k.'_'.$axs['l']]='';
			unset($this->content_search[$k]);
			}
		foreach ($this->content_search as $k=>$v) $where[$k]="`".$k."` LIKE '%".addslashes($axs_content['search']['str'])."%'";
		$this->tpl['search']=($tmp=axs_file_choose(array($this->axs_local['plugin'].'.search.tpl', $this->name.'.search.tpl'))) ? axs_tpl('', $tmp):false;
		$data=array();
		unset($this->form->sql_add['_type'], $this->form->sql_add['_parent']);
		$result=$this->list_articles_query(array('('.implode(' OR ', $where).')'));
		foreach ($result as $cl) {
			$vr=array();
			$this->content_search_row($cl, $vr);
			$cl['title']=$cl['_title'];
			$cl['url']='&'.axs_url(array('t'=>$cl['title_alias'], 'id'=>$cl['id']), array(), false);
			$cl['path']=array($cl, );
			$data[$cl['c'].$cl['id']]=$cl;
			}
		return($data);
		} #</search()>
	function content_search_row(&$cl, &$vr) {
		global $axs, $axs_content;
		$this->list_articles_row($cl, $vr);
		//$cl['title'].=' ('.$cl['publ'].')';
		if ((isset($this->form->structure['text'])) && ($this->form->structure['text']['type']==='hidden')) $cl['text']=axs_tab_proc($cl['text'], $axs['time'], "\n");
		$tmp=$this->content_search;
		foreach ($tmp as $k=>$v) $tmp[$k]=$cl[$k];
		unset($tmp['title']);
		$cl['text']=axs_content_search::text_chop(strip_tags(implode(' ', $tmp)), strtolower($axs_content['search']['str']));
		if ($this->tpl['search']) $cl['text']=axs_tpl_parse($this->tpl['search'], array_merge($cl, $vr));
		} #</search_row()>
	#</CMS content search functions>
	#<Misc. functions>
	function error() {
		if (!$this->db) return '<p class="msg" lang="en">ERROR! This plugin requires SQL database.</p>';
		} #</error()>
	function _article_meta(&$cl) {
		if (!isset($cl['plugin'])) $cl['plugin']=$this->axs_local['plugin'];
		if (empty($cl['c'])) /*if ((empty($cl['c'])) || (empty($this->content_index[$cl['c']])))*/ {
			if (empty($this->content_index)) $this->content_index+=axs_content::index_c_find(false, $cl['plugin']);
			foreach ($this->content_index as $k=>$v) if ($v['plugin']===$cl['plugin']) {	$cl['c']=$v['c'];	break;	}
			}
		if (empty($cl['c'])) axs_log(__FILE__, __LINE__, $this->axs_local['plugin'], '$c not found!', 1);
		if (!isset($this->content_index[$cl['c']])) $this->content_index+=axs_content::index_c_find($cl['c']);
		if ((!$this->module) && (!$this->lang_multi)) $this->form->structure_set_dir($this->content_index[$cl['c']]['dir'].$cl['c'].'_files/');
		$cl['_title']=$this->form->structure_get_item_title($cl);
		$cl['title_alias']=($tmp=$this->_field_get($cl, 'title_alias')) ? $tmp:axs_valid::id($cl['_title']);
		$cl['article_url']=array('c'=>$cl['c'], 't'=>$cl['title_alias'], 'id'=>$cl['id'], );
		if (!empty($this->article_first)) $cl['article_url']['p']=$this->pager->p;
		$cl['article_url']=$this->url_root.axs_url($this->url, $cl['article_url'], false);
		$cl['article_parent_url']=$this->url_root.axs_url($this->url, array('c'=>$cl['c'], 'id'=>($cl['_parent']) ? $cl['_parent']:null, ), false);
		} #</_article_meta()>
	function _field_get($cl, $n, $get='v') {
		if (isset($cl[$n])) $key=$n;
		else $key=(isset($cl[$n.'_'.$this->axs_local['l']])) ? $n.'_'.$this->axs_local['l']:$n;
		return ($get==='k') ? $key:axs_get($key, $cl);
		} #</_field_get()>
	#<Set entry type (article, folder, etc.) />
	function _type_set($_type) {
		if (!isset($this->structure[''])) $this->structure['']=$this->form->structure;
		if (!isset($this->structure[$_type])) {
			$this->form->structure_set($this->{'fields'.$_type});
			$this->form->structure_set_labels(false, $_type.'.');
			$this->structure[$_type]=$this->form->structure;
			}
		if ($this->_type===$_type) return;
		else $this->form->structure=$this->structure[$_type];
		$this->_type=$_type;
		} #</_type_set()>
	function _lightbox($id=false) {
		global $axs;
		if (empty($this->lightbox)) return;
		$axs['page']['head']['jquery.js']='<script src="?'.urlencode('axs[gw]').'=jquery.js"></script>'."\n";
		//$axs['page']['head']['jquery.elevatezoom.js']='<script src="'.axs_dir('site.base', 'http').$this->class.'.jquery.elevatezoom.js"></script>'."\n";
		$axs['page']['head']['jquery.fancybox.js']='<script src="'.axs_dir('lib.js', 'http').'fancybox/jquery.fancybox.pack.js"></script>'."\n";
		$axs['page']['head']['jquery.fancybox.css']='<link href="'.axs_dir('lib.js', 'http').'fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css" media="screen" />'."\n";
		$axs['page']['head']['swiped-events.js']='<script src="'.axs_dir('lib.js', 'http').'swiped-events.js"></script>'."\n";
		$this->vr['js'].='<script>axs.articles.lightbox();</script>';
		return true;
		} #</_lightbox()>
	function _qr_code_files(array $fields, array $cl) {
		global $axs;
		if (empty($fields)) return '';
		foreach ($this->qr_code_files as $file=>$size) if (!file_exists($f=$this->form->dir_fs.$cl['id'].'/'.$file)) {
			require_once(axs_dir('lib').'phpqrcode.php');
			$text='';
			foreach($fields as $k=>$v) $text.=$this->form->value_display($k, $v, $cl, $cl, false).$v;
			QRcode::png($text.$axs['http'].'://'.$_SERVER['SERVER_NAME'].'/'.'?'.axs_url($this->url, array(), false), $f, QR_ECLEVEL_L, $size, 1);
			}
		return '<a class="qr_code" href="'.$this->form->dir_http.$cl['id'].'/qr2.png" target="_blank"><img src="'.$this->form->dir_http.$cl['id'].'/qr1.png" alt="QR code" lang="en" /></a>';
		} #</_qr_code_files()>
	#</Misc. functions>

	} #</class::axs_articles_base>
#2008-06 ?>