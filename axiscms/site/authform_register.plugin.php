<?php #2022-03-22
$plugin_def=array(); # <plugin default data />
$plugin_def['name']['et']='Kasutaja login vorm > registreerumine';
$plugin_def['title']['et']='Kasutaja registreerimine';
$plugin_def['help']['']='';
$plugin_def['mkdir']='';
$plugin_def['tr']['en']=array(
	'login.title'=>'Log in',
	'register.title'=>'Register',
	'msg.ok'=>'Account created successfully. You can now log in.',
	'msg.user_invalid'=>'Invalid user/e-mail!',
	'msg.user_exists'=>'This user/e-mail is already registered. Please use the link "Forgot password?" on the login form!',
	'msg.pass_invalid'=>'Invalid password!',
	'msg.pass_short'=>'Password must be at least {$min} characters long!',
	'msg.pass_repeat'=>'Passwords do not match!',
	'msg.email'=>'Invalid e-mail 2 (please use a comma to separate multiple e-mail addresses)!',
	
	'msg_value_required'=>'Input is required!',
	'msg_value_invalid'=>'Invalid value!',
	'msg_value_unique'=>'Value must be unique!',
	);
$plugin_def['tr']['et']=array(
	'login.title'=>'Logi sisse',
	'register.title'=>'Registreeru',
	'msg.ok'=>'Kasutaja edukalt registreeritud. Palun logige sisse.',
	'msg.user_invalid'=>'Kasutaja/e-post ei ole korrektne!',
	'msg.user_exists'=>'Selline Kasutaja/e-post on juba registreeritud! Palun kasutage login vormil olevat linki "Unustasid parooli?"',
	'msg.pass_invalid'=>'Parool sisaldab keelatud sümboleid!',
	'msg.pass_short'=>'Parooli pikkus peab olema vähemalt {$min} tähemärki!',
	'msg.pass_repeat'=>'Paroolid ei klapi!',
	'msg.email'=>'E-post 2 sisaldab keelatud sümboleid (mitme aadressi puhul kasutage eraldajana koma)!',
	
	'msg_value_required'=>'Välja täitmine on kohustuslik.',
	'msg_value_invalid'=>'Sisestatud väärtus ei ole sobiv.',
	'msg_value_unique'=>'Väärtus peab olema kordumatu.',
	);
$plugin_def['config']['tabs']=array('account'=>'', );
if ($tmp=axs_file_choose($axs_local['plugin'].'.plugin.config.php')) include($tmp);

if (defined('AXS_SITE_NR')) {
require_once(axs_dir('site_base').'authform.plugin.class.php');
$o=new axs_authform_page($axs_local, $plugin_def);
if (!$o->db) return '<p class="msg" lang="en">ERROR! This plugin requires SQL database.</p>';
if (!defined('AXS_LOGINCHK')) axs_user::init();
if ($axs_local['section']==='content') { #<Main content area>
	$o->tr=axs_content::tr_get();
	$form=axs_users_edit::register_user_structure($o->url, $o->tr);
	reset($plugin_def['config']['tabs']);
	if (AXS_LOGINCHK===1) {
		$cl=axs_db_query("SELECT * FROM `".axs_users_edit::_table()."` WHERE `id`='".axs_user::get('id')."'", 'row', $axs['cfg']['users_db'], __FILE__, __LINE__);
		if (!isset($plugin_def['config']['tabs'][$o->url['s']])) $o->url['s']=key($plugin_def['config']['tabs']);
		$tpl=axs_tpl(false, $axs_local['plugin'].'.tpl');
		$vr=$o->tr->get()+array('tabs'=>'', 'title'=>'', 'content'=>'', );
		foreach ($form->structure_get() as $k=>$v) if (axs_tpl_has($tpl, 'user.'.$k)) $vr['user.'.$k]=$form->value_display($k, $v, $cl, $vr);
		foreach ($plugin_def['config']['tabs'] as $k=>$v) {
			$v=array('url'=>'?'.axs_url($o->url, array('s'=>$k, )), 'title'=>$o->tr->s('tabs.'.$k.'.title'), 'current'=>'', );
			if ($o->url['s']===$k) {
				$vr['tab.current']=$k;
				$vr['title']=$v['title'];
				$v['title']='<strong>'.$v['title'].'</strong>';
				$v['current']=' current';
				}
			$v['link']='<a href="'.$v['url'].'" class="'.$k.$v['current'].'">'.$v['title'].'</a>';
			foreach ($v as $kk=>$vv) if (axs_tpl_has($tpl, 'tabs.'.$k.'.'.$kk)) $vr['tabs.'.$k.'.'.$kk]=$vv;
			$vr['tabs'].='<li class="'.$k.$v['current'].'">'.$v['link'].'</li>';
			}
		if ($tmp=axs_file_choose($axs_local['plugin'].'.tabs.'.$o->url['s'].'.php')) $vr['content']=$o->content_include($tmp);
		else { #<Edit user profile>
			if (axs_user::get('id')) {
				if (isset($_POST['submit'])) axs_users_edit::register_user($form, $axs_user['id'], '?'.axs_url($o->url));
				else $form->vl=$form->sql_values_get(axs_db_query("SELECT * FROM `".axs_users_edit::_table()."` WHERE `id`='".intval($axs_user['id'])."'", 'row', $axs['cfg']['users_db'], __FILE__, __LINE__));
				if (isset($form->structure['terms_agree'])) $form->vl['terms_agree']=1;
				} #</Edit user profile>
			$vr['content']=$form->form_parse_html(false, false);
			} #</Edit user profile>
		return axs_tpl_parse($tpl, $vr);
		}
	else {
		$tmp=array('register'=>'', 'login'=>'', );
		if (!isset($tmp[$o->url['s']])) $o->url['s']=key($tmp);
		switch($o->url['s']) {
			case 'login': return include('authform.plugin.php');
			default: #<Register new user>
				$form=axs_users_edit::register_user_structure($o->url, $o->tr);
				if (isset($_POST['submit'])) {
					if ($tmp=axs_users_edit::register_user($form, ''/*, '?'.axs_url($o->url, array(), '&')*/)) $form->msg('msg.ok');
					}
				return $form->form_parse_html(false, false);
				#</Register new user>
			} #</switch($o->url['s'])>	
		}
	} #</Main content area>
else { #<Banner>
	if (AXS_LOGINCHK!==1) { #<Show content and profile links />
		$html=axs_tpl(false, $axs_local['plugin'].'.banner.tpl');
		$vars=axs_content::tr_get()->get()+array('login.url'=>'?'.axs_url($o->url, array('s'=>'login', )), 'register.url'=>'?'.axs_url($o->url, array('s'=>null, )));
		if (axs_tpl_has($html, 'login')) $vars['login']=include('authform.plugin.php');
		return axs_tpl_parse($html, $vars);
		}
	else return include('authform.plugin.php');
	} #</Banner>
}
#2007-08 ?>