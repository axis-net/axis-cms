		<article class="article">
			<h2>{$_title}</h2>
			<div class="socialmedia">{$socialmedia}</div>
			<div class="summary">
				{$file.t2}
				{$times}
				{$summary}
			</div>
			{$text}
			<p class="back"><a href="{$back}" rel="nofollow">&lt; {$back.lbl}</a></p>
			{$qr_code}
		<article>
{$comments}