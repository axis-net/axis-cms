<?php #2019-05-16
require_once('articles.base.class.php');

class axs_articles_petition extends axs_articles_base {
	var $folders=false;
	var $fields_article=array(
		''=>array('dir_entry'=>'', 'layout'=>'table', 'sql'=>array('table'=>'{$px}{$plugin}', 'attr'=>array('COMMENT'=>'AXIS CMS plugin articles_mailinglist'))),
		'id'=>array('type'=>'number', 'size'=>10, 'required'=>0, 'readonly'=>'readonly', 'thead'=>0, 'search'=>1, 'sql'=>array(
			'_type'=>'int', '_value'=>10, '_attribute'=>'unsigned', 'Key'=>'PRI', 'Extra'=>'AUTO_INCREMENT', 
			), ),
		//'fields'=>array('type'=>'fieldset', ),
		'email'=>array('type'=>'email', 'size'=>75, 'required'=>1, 'maxlength'=>255, 'thead'=>null, 'search'=>1, 'sort'=>true, 'export'=>1, ),
		//'fields/'=>array('type'=>'fieldset_end', ),
		'time'=>array('type'=>'timestamp', 'size'=>10, 'required'=>1, 'format'=>'d.m.Y H:i:s', 'thead'=>null, 'search'=>1, 'sort'=>true, ),
		'confirm'=>array('type'=>'checkbox', 'size'=>1, 'required'=>0, 'thead'=>null, 'value'=>1, 'search'=>1, 'sort'=>true, ),
		'save'=>array('type'=>'submit', 'size'=>0, 'required'=>0, 'add_fn'=>true, ),
		);
	var $fields_comment=array();
	/*function __constructor(&$axs_local) {
		}*/ #</__constructor()>
	function banner_init() {
		global $axs;
		//axs_articles_base::banner_start();
		//$axs['page']['head'][$this->axs_local['plugin'].'.js']='<script src="'.axs_dir('plugins', 'http').$this->axs_local['plugin'].'.js'.'"></script>'."\n";
		} #</banner_start()>
	function banner_parse() {
		global $axs, $axs_content;
		$this->vr['url']=$this->url_root.axs_url($this->url, array('c'=>$this->axs_local['c']));
		$this->vr['title']=$axs_content['content'][$this->axs_local['c']]['title'];
		return $this->content_display('.banner');
		} #</banner_parse()>
	function content_display($tpl='') {
		global $axs, $axs_content;
		$this->vr['c']=$this->axs_local['c'];
		$this->uform=$axs_content['content'][$this->axs_local['c']]['form'];
		unset($axs_content['content'][$this->axs_local['c']]['form']);
		//require_once(AXS_PATH_CMS.'site.form.php');
		$this->uform=new axs_form_site($this->axs_local, $this->uform, $_POST);
		$this->uform->disabled=false;
		$this->uform->url['c']=$this->axs_local['c'];
		$this->uform->target_id=$this->axs_local['c'].'_form';
		$this->uform->values=$this->uform->form_input();
		if ($this->uform->save) $this->uform->validate($this->uform->values);
		if (($this->uform->save) && (empty($this->uform->msg))) {
			if (isset($this->uform->user_input['subscribe'])) $this->join();
			}
		if (isset($_GET['confirm'])) $this->join_confirm();
		$this->vr['form']=$this->uform->build();
		return axs_tpl_parse(axs_tpl(false, $this->axs_local['plugin'].$tpl.'.tpl'), $this->vr+$this->form->tr->get());
		} #</content()>
	function join() {
		global $axs;
		if (!$this->db) return;
		#<Set form messages />
		$axs['page']['class'][]='form_ok';
		$this->uform->msg('', $this->uform->tr->s('msg_join'));
		axs_log(__FILE__, __LINE__, $this->axs_local['plugin'].'.join');
		#<Update SQL table />
		$cl=$this->join_insert();
		$txt=
		'Lgp '.$cl['fstname'].' '.$cl['lstname'].'!'."\n".
		"\n".
		"\n".
		$axs['http'].'://'.$_SERVER['SERVER_NAME'].$axs['http_root'].'?'.axs_url($axs['url'], array('confirm'=>$cl['code']), false).'#'.$this->uform->target_id."\n".
		"\n".
		'';
		axs_mail(
			$cl['email'],
			'Petitsioon "Rahvusringhääling peab täitma rahvusringhäälingu seadust!"',
			$txt,
			'',
			'(Name)',
			'(email)'
			);
		} #</join()>
	function join_confirm() {
		global $axs;
		axs_log(__FILE__, __LINE__, $this->axs_local['plugin'].'.confirm');
		$cl=axs_db_query("SELECT * FROM `".$this->form->sql_table."` WHERE `code`='".addslashes($_GET['confirm'])."' LIMIT 1", 'row', $this->db, __FILE__, __LINE__);
		if (empty($cl['id'])) return $this->uform->msg('', 'Kinnituskoodi ei leitud. Palun täitke liitumisvorm uuesti, et saaksime saata uue lingi.');
		#<Set form messages />
		$axs['page']['class'][]='form_ok';
		$this->uform->msg('', $this->uform->tr->s('msg_confirm'));
		if (!empty($cl['confirm'])) return;
		axs_db_query("UPDATE `".$this->form->sql_table."` SET `confirm`='1' WHERE `id`='".$cl['id']."'", '', $this->db, __FILE__, __LINE__);
		$to='erik.roose@err.ee, riina.roomus@err.ee, urmas.oru@err.ee, joel.sarv@err.ee';
		//$cc='rein.veidemann@tlu.ee, agu@uudelepp.ee, heidy.purga@riigikogu.ee, kalvi.kova@riigikogu.ee, kristaaru@gmail.com, marika.tuus-laul@riigikogu.ee, martin.helme@riigikogu.ee, paavo.nogene@gmail.com, pille.vengerfeldt@ut.ee, viktoria.ladonskaja@riigikogu.ee';
		$cc='rein.veidemann@tlu.ee, agu@uudelepp.ee, marika.tuus-laul@riigikogu.ee, paavo.nogene@gmail.com, pille.vengerfeldt@ut.ee, viktoria.ladonskaja@riigikogu.ee';
		#$to='varro.vooglaid@saptk.ee';
		#$cc='ivar.tohvelmann@gmail.com';
		if ($cl['forward']) axs_mail(
			$to,
			'Petitsioon "Rahvusringhääling peab täitma rahvusringhäälingu seadust!"',
			'PETITSIOON
Eesti Rahvusringhäälingu juhatusele

Rahvusringhäälingu seaduse paragrahvi 6 kohaselt peavad rahvusringhäälingu programmid olema mitmekülgsed ja ühiskonnaelu teemade käsitlus neis peab olema tasakaalustatud. Samuti peavad need aitama kaasa ühiskonna sidususele, kajastama erinevaid arvamusi ja tõekspidamisi ning olema poliitiliselt tasakaalustatud.

Lisaks deklareerib rahvusringhäälingu eesmärke selgitav seaduse paragrahv 4, et rahvusringhäälingu programmid peavad väärtustama Eesti riigi ja eesti rahvuse kestmise tagatisi ning osutama asjaoludele, mis võivad ohustada Eesti riigi ja eesti rahvuse püsimist. Ühtlasi peavad need aitama kaasa demokraatliku riigikorralduse edendamisele ning väärtustama perekonnal põhinevat ühiskonnamudelit.

Paraku on rahvusringhäälingu tegevuses olnud nende nõuete täitmisel juba pikema aja jooksul näha tõsiseid puudujääke. Eriti teravalt on torganud silma programmide poliitilise mitmekülgsuse ja tasakaalustatuse nõude eiramine ja rikkumine. Niisugune asjade seis ei ole vastuvõetav, sest rahvusringhäälingu seadus pole mõeldud pelgalt soovituslikuks juhiseks, mida võib vastavalt paremale äranägemisele järgida või mitte järgida. Tegu on seadusega, mille täitmine on rahvusringhäälingu juhtkonnale ja töötajatele rangelt kohustuslik.

Olukord, kus rahvusringhäälingu raadiokanalites on nädalavahetusel kaks nädala poliitikateemade kommenteerimisele pühendatud saadet – „Sildam ja Samost“ ning „Olukorrast riigis“ –, mida mõlemat juhivad ainult liberaalsete vaadetega ja liberaalseid hoiakuid edendavad inimesed, on lubamatu. Samuti on üksnes liberaalide juhtida ETV saated „Esimene stuudio“, „Foorum“ ja „Ringvaade“. Kõnekas on tõsiasi, et rahvusringhäälingust pole teada ühtegi selgelt rahvuslik-konservatiivsete vaadetega eestikeelsete poliitikateemaliste saadete juhti, ei raadios ega televisioonis.

Rahvusringhäälingus, mis tegutseb meie kõigi maksurahast, peavad saama reaalselt ja tasakaalustatult kajastatud poliitilise ja ilmavaatelise spektri erinevad positsioonid. Muuhulgas on see vajalik ühiskonna lõhestumise pidurdamiseks ning sotsiaalse sidususe suurendamiseks. Seetõttu palume tungivalt, et rahvusringhäälingu poliitikateemaliste saadete juhtidena kaasa taks liberaalide kõrval ka selgelt rahvuslik-konservatiivsete vaadetega inimesi ning et liberaalide juhitud saadetega paralleelselt kutsutaks ellu samalaadsed saated, mille juhtimine antaks konservatiivide kätte. Rahvuslik-konservatiivse ilmavaate süstemaatiline tõrjumine ja marginaliseerimine peab lõppema.

Käesolev pöördumine on tehtud viisakas ja lugupidavas toonis ning see on kantud mitte konfliktide süvendamise, vaid konstruktiivses vaimus õiglase tulemuse saavutamise eesmärgist. Loodan siiralt, et minu ja paljude teiste inimeste mure ei ole teile ükskõik ning et võtate seda tõsiselt, astudes samme probleemile lahenduste leidmiseks ning rahvusringhäälingu seaduse mõtte ja sättega kooskõlas oleva olukorrani jõudmiseks.

Lugupidamisega

'.$cl['fstname'].' '.$cl['lstname'],
			'',
			$cl['fstname'].' '.$cl['lstname'],
			$cl['email'],
			array(),
			array('Cc'=>$cc, 'Bcc'=>$cl['email'].', varro.vooglaid@saptk.ee', )
			);
		} #</join_confirm()>
	function join_insert() {
		global $axs, $axs_user;
		$cl=axs_db_query($q="SELECT * FROM `".$this->form->sql_table."` WHERE `email`='".addslashes(trim($this->uform->values['email']))."'".$this->form->sql_add(' AND ', '', ' AND ', array('disable'=>false, ))."", 'row', $this->db, __FILE__, __LINE__);
		//if (!empty($axs_user['user'])) dbg($q,$cl);
		if (!empty($cl['id'])) {
			//axs_db_query("UPDATE `".$this->form->sql_table."`\n".
			//"	SET ".$this->uform->sql_values_set($this->uform->values)." ".$this->form->sql_add(', ', '' , ', '),
			//" WHERE `id`='".$cl['id']."'", $this->db, __FILE__, __LINE__);
			return $cl=array_merge($cl, $this->uform->values);
			}
		$cl=array_merge($this->uform->values, array('code'=>md5($axs['time'].$this->uform->values['email'])));
		$cl['id']=axs_db_query("INSERT INTO `".$this->form->sql_table."`\n".
		"	SET ".$this->uform->sql_values_set($this->uform->values).", `code`='".$cl['code']."', `time`='".$axs['time']."'".
		$this->form->sql_add(', ', '' , ', '), 'insert_id', $this->db, __FILE__, __LINE__);
		return $cl;
		} #</join_insert()>
	} #</class::axs_articles_petition>
/*CREATE TABLE IF NOT EXISTS `axs_articles_petition` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '=content.id',
  `c` varchar(255) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '' COMMENT '=content.cid',
  `l` char(2) CHARACTER SET ascii COLLATE ascii_bin DEFAULT '',
  `time` int(10) unsigned NOT NULL DEFAULT '0',
  `email` varchar(50) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `disable` tinyint(3) unsigned NOT NULL,
  `updated_uid` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site-c-l` (`site`,`c`,`l`),
  KEY `time` (`time`),
  KEY `email` (`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_estonian_ci;*/
#2019-05-16 ?>