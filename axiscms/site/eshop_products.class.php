<?php #2018-10-21
require_once('articles.base.class.php');
class axs_eshop_products extends axs_articles_base {
	public $_class='eshop_products';
	public $sql_limit=array(20,40,60,2);
	public $module='eshop';
	public $f='products';
	public $forms=array('products'=>array(), );
	public $content_search=array('title'=>'', 'summary'=>'', );
	public $lang_multi=true;
	public $amount_step=1;
	public $amount_unit='';
	public $amount_unit_type=1;
	public $lightbox=true;
	public $fields_folder=array(
		'id'=>array('type'=>'number', 'size'=>10, 'required'=>0, 'readonly'=>'readonly', 'thead'=>0, ),
		'nr'=>array('type'=>'number', 'size'=>5, 'required'=>1, 'min'=>1, 'step'=>1, 'rank'=>true, 'ASC'=>'ASC', ),
		'title'=>array('type'=>'text', 'size'=>50, 'required'=>1, 'maxlength'=>255, 'thead'=>0, 'title_item'=>1, 'lang-multi'=>true, ),
		'file'=>array('type'=>'file', 'size'=>0, 'required'=>0, 'accept'=>'image.*', 'thead'=>0, 'add_fn'=>true, 'multiple'=>false, 't'=>array(
				't1'=>array('w'=>400, 'h'=>400, 'ext'=>'jpg', 'crop_pos'=>'50%', 'crop_size'=>':', 'q'=>90, 'name_base'=>'{$id}', 'alt'=>'{$id}.t1.{$ext}', 'dw'=>200, 'dh'=>200, 'link'=>array('c'=>'{$c}', 't'=>'{$title_alias}', 'id'=>'{$id}'), 'link_target'=>'', ),
				't2'=>array('w'=>2000, 'h'=>2000, 'ext'=>'jpg', 'q'=>90, 'name_base'=>'{$id}', 'alt'=>'{$id}.t2.{$ext}', 'dw'=>200, 'dh'=>200, 'link'=>'t2', 'link_target'=>'_blank', ),
				),
			),
		'thumbs'=>array('type'=>'number', 'size'=>3, 'required'=>0, 'thead'=>0, 'sort'=>1, 'value'=>1, 'sql'=>array(
			'_type'=>'tinyint', '_value'=>3, '_attribute'=>'unsigned', 'Null'=>true,
			), ),
		
		'summary'=>array('type'=>'wysiwyg', 'size'=>75, 'required'=>0, 'rows'=>2, 'thead'=>1, 'lang-multi'=>true, ),
		'seo'=>array('type'=>'fieldset', 'size'=>0, 'required'=>0, ),
		'title_alias'=>array('type'=>'text', 'size'=>50, 'required'=>0, 'maxlength'=>255, 'sql'=>array(
			'_charset'=>'ASCII',
			) ),
		'meta_description'=>array('type'=>'text', 'size'=>50, 'required'=>0, 'maxlength'=>255, ),
		'seo_end'=>array('type'=>'fieldset_end', 'size'=>0, 'required'=>0, ),
		'updated'=>array('type'=>'timestamp-updated', 'size'=>50, 'required'=>0, 'thead'=>0, 'sort'=>1, 'DESC'=>'DESC', 'add_fn'=>true, ),
		'save'=>array('type'=>'submit', 'size'=>0, 'required'=>0, 'add_fn'=>true, ),
		);
	public $fields_article=array(
		''=>array('dir'=>'module.{$plugin}', 'dir_entry'=>'{$id}/', 'layout'=>'list', 'sql'=>array(
			'table'=>'{$px}{$name}', 'attr'=>array('COMMENT'=>'AXIS CMS plugin eshop_products'), 
			), ),
		'id'=>array('type'=>'number', 'size'=>10, 'required'=>0, 'readonly'=>'readonly', 'thead'=>1, 'search'=>1, 'sql'=>array(
			'_type'=>'int', '_value'=>10, '_attributes'=>'unsigned', 'Key'=>'PRI', 'Extra'=>'AUTO_INCREMENT', 
			), ),
		'nr'=>array('type'=>'number', 'required'=>1, 'min'=>1, 'step'=>1, 'rank'=>true, 'ASC'=>'ASC', ),
		'title'=>array('type'=>'text', 'size'=>50, 'required'=>1, 'maxlength'=>255, 'thead'=>1, 'search'=>1, 'lang-multi'=>true, 'ASC'=>'ASC', ),
		'brand'=>array('type'=>'text', 'size'=>20, 'required'=>1, 'maxlength'=>255, 'thead'=>1, 'search'=>1, ),
		'size'=>array('type'=>'fieldset', 'size'=>0, 'required'=>0, ),
		'size_x'=>array('type'=>'number', 'required'=>1, 'step'=>1, 'min'=>0,  ),
		'size_y'=>array('type'=>'number', 'required'=>1, 'step'=>1, 'min'=>0,  ),
		'size_z'=>array('type'=>'number', 'required'=>1, 'step'=>1, 'min'=>0,  ),
		'weight'=>array('type'=>'number', 'required'=>0, 'step'=>0.01, 'min'=>0,  ),
		'size_end'=>array('type'=>'fieldset_end', 'size'=>0, 'required'=>0, ),
		'stock'=>array('type'=>'number', 'size'=>5, 'required'=>0, 'thead'=>1, 'search'=>1, 'sql'=>array(
			'_type'=>'smallint', '_value'=>5, '_attributes'=>'unsigned', 'Null'=>'NULL', 
			), ),
		'delivery'=>array('type'=>'number', 'size'=>2, 'required'=>0, 'thead'=>1, 'search'=>1, 'sql'=>array(
			'_type'=>'tinyint', '_value'=>3, '_attributes'=>'unsigned', 'Null'=>'NULL', 
			), ),
		'price'=>array('type'=>'number', 'size'=>9, 'required'=>1, 'thead'=>1, 'search'=>1, 'sql'=>array(
			'_type'=>'decimal', '_value'=>'7,2', '_attributes'=>'unsigned', 
			), ),
		'price_discount'=>array('type'=>'number', 'size'=>9, 'required'=>1, 'thead'=>1, 'search'=>1, 'sql'=>array(
			'_type'=>'decimal', '_value'=>'7,2', '_attributes'=>'unsigned', 
			), ),
		'vat'=>array('type'=>'select', 'size'=>2, 'required'=>0, 'thead'=>0, 'search'=>0, 'options'=>array(), 'sql'=>array(
			'_type'=>'tinyint', '_value'=>3, '_attributes'=>'unsigned', 
			), ),
		'transport_cat'=>array('type'=>'select', 'size'=>2, 'required'=>1, 'thead'=>0, 'search'=>1, 'options'=>array(
			1=>array('value'=>1, 'label'=>'S'), 2=>array('value'=>2, 'label'=>'M'), 3=>array('value'=>3, 'label'=>'L'), 4=>array('value'=>4, 'label'=>'XL'),
			), 'sql'=>array(
			'_type'=>'tinyint', '_value'=>3, '_attributes'=>'unsigned', 
			), ),
		'options'=>array('type'=>'table', 'size'=>0, 'required'=>0, 'thead'=>0, 'options'=>array(
			'nr'=>array('type'=>'number', 'required'=>1, 'rank'=>true, 'ASC'=>'ASC', ),
			'label'=>array('type'=>'text', 'size'=>50, 'maxlength'=>255, 'required'=>0, 'thead'=>1, ),
			'_add'=>array('type'=>'submit', ),
			'_del'=>array('type'=>'checkbox', ),
			), ),
		'file'=>array('type'=>'file', 'size'=>0, 'required'=>0, 'accept'=>'image.*', 'thead'=>1, 'add_fn'=>true, 't'=>array(
				't1'=>array('w'=>250, 'h'=>250, 'ext'=>'jpg', 'crop_pos'=>'50%', 'crop_size'=>':', 'q'=>90, 'name_base'=>'{$id}', 'alt'=>'{$_title}', 'dw'=>200, 'dh'=>200, 'link'=>array('c'=>'c', 't'=>'title_alias', 'id'=>'id'), 'link_target'=>'', ),
				't2'=>array('w'=>2000, 'h'=>2000, 'ext'=>'jpg', 'q'=>90, 'name_base'=>'{$id}', 'alt'=>'{$_title}', 'dw'=>200, 'dh'=>200, 'link'=>'t2', 'link_target'=>'_blank', ),
				),
			),
		'files'=>array('type'=>'table', 'size'=>1, 'required'=>0, 'thead'=>1, 'add_fn'=>true, 'options'=>array(
				'nr'=>array('type'=>'number', 'required'=>1, 'rank'=>true, 'ASC'=>'ASC', ),
				'file'=>array('type'=>'file', 'size'=>0, 'required'=>0, 'accept'=>'image.*', 'thead'=>1, 'add_fn'=>true, 't'=>array(
						't1'=>array('w'=>240, 'h'=>160, 'ext'=>'jpg', 'crop_pos'=>'', 'crop_size'=>'', 'q'=>90, 'name_base'=>'{$_parent_id}.{$_field}.{$_key}.t1', 'alt'=>'{$_title} {$_key}', 'dw'=>200, 'dh'=>200, 'link'=>'t2', 'link_target'=>'_blank', ),
						't2'=>array('w'=>1000, 'h'=>1000, 'ext'=>'jpg', 'q'=>90, 'name_base'=>'{$_parent_id}.{$_field}.{$_key}.t2', 'alt'=>'{$title} {$_key}', 'dw'=>200, 'dh'=>200, 'link'=>'', 'link_target'=>'', ),
						),
					),
				'_add'=>array('type'=>'submit', ),
				'_del'=>array('type'=>'checkbox', ),
				),
			),
		'summary'=>array('type'=>'wysiwyg', 'size'=>75, 'required'=>0, 'rows'=>2, 'thead'=>1, 'lang-multi'=>true, ),
		'seo'=>array('type'=>'fieldset', 'size'=>0, 'required'=>0, ),
		'title_alias'=>array('type'=>'text', 'size'=>50, 'required'=>0, 'maxlength'=>255, 'lang-multi'=>true, ),
		'meta_description'=>array('type'=>'text', 'size'=>50, 'required'=>0, 'maxlength'=>255, 'lang-multi'=>true, ),
		'seo_end'=>array('type'=>'fieldset_end', 'size'=>0, 'required'=>0, ),
		'comments'=>array('type'=>'checkbox', 'size'=>1, 'required'=>0, 'value'=>1, 'thead'=>1, 'search'=>1, ),
		'similar'=>array('type'=>'table', 'size'=>0, 'required'=>0, 'thead'=>0, 'options'=>array(
			'product_id'=>array('type'=>'text-join', 'size'=>70, 'required'=>1, 'thead'=>1,
				'f'=>array(
					'title'=>array('module'=>'eshop', 'form'=>'products', 'table'=>'eshop_products', 'table_alias'=>'t', ),
					),
				'concat'=>'t.title', 'limit'=>10,
				'sql'=>array('_type'=>'int', '_value'=>10, '_attribute'=>'unsigned', ),
				),
			'_add'=>array('type'=>'submit', ),
			'_del'=>array('type'=>'checkbox', ),
			) ),
		'publ'=>array('type'=>'timestamp', 'size'=>10, 'required'=>1, 'format'=>'d.m.Y', 'thead'=>1, 'search'=>1, 'sort'=>1, 'value'=>'time()', ),
		'disable'=>array('type'=>'checkbox', 'size'=>1, 'required'=>0, 'thead'=>1, ),
		'updated'=>array('type'=>'timestamp-updated', 'size'=>50, 'required'=>0, 'thead'=>1, 'search'=>1, 'sort'=>1, ),
		'save'=>array('type'=>'submit', 'size'=>0, 'required'=>0, 'add_fn'=>true, ),
		);
	static $tr=array(
		'et'=>array(
			'stock.available.txt'=>'laos',
			'stock.order.txt'=>'ettetellimisel',
			'add_to_cart.txt'=>'Lisa ostukorvi',
			'amount.lbl'=>'kogus',
			'read_more.lbl'=>'vaata lähemalt',
			'prev.txt'=>'eelmine',
			'next.txt'=>'järgmine',
			'comments.form.title'=>'Kommentaarid',
			'comments.name.lbl'=>'Nimi',
			'comments.message.lbl'=>'Kommentaar',
			'comments.captcha.lbl'=>'Kontrollkood',
			'comments.post.lbl'=>'Salvesta',
			'comments.time.lbl'=>'Aeg',
			'search.lbl'=>'Otsi',
			'search.found.txt'=>'Leitud tooteid',
			'search.order.lbl'=>'Sordi',
			'msg'=>'Kommentaar salvestatud!',
			'msg_value_required'=>'Välja tämine on kohustuslik!',
			'msg_value_invalid'=>'Sisestatud väärtus ei ole sobiv!',
			'msg_value_unique'=>'Väärtus peab olema kordumatu!',
			'back.lbl'=>'tagasi',
			
			'_folder.id.lbl'=>'id',
			'_folder.nr.lbl'=>'nr',
			'_folder.title.lbl'=>'kategooria nimetus',
			'_folder.file.lbl'=>'pilt',
			'_folder.thumbs.lbl'=>'näita eelvaate pilte kataloogist',
			'_folder.summary.lbl'=>'kirjeldus',
			'_folder.seo.lbl'=>'SEO',
			'_folder.title_alias.lbl'=>'URL alias',
			'_folder.meta_description.lbl'=>'<meta description>',
			'_folder.updated.lbl'=>'muudetud',
			'_folder.save.lbl'=>'salvesta',
			),
		);
	/*function __construct(&$axs_local) {
		global $axs, $axs_content;
		axs_articles_base::__construct($axs_local);
		unset($this->sql_qry['l'], $this->sql_qry_banner['l']);
		//$this->form->tr->set(axs_content::tr_get(array('tr'=>true), __FILE__, __LINE__, $axs_local['c'], $axs_local['dir'], $axs_local['l']));
		}*/ #</__construct()>
	function banner_categorys() {
		global $axs;
		$this->form->sql_add['plugin']=$this->_class;
		$vr=array(
			'c'=>key(axs_content::index_c_find(false, $this->_class)),
			'title'=>axs_html_safe($this->axs_local['title']),
			);
		$this->article=($vr['c']===$axs['c']) ? intval(axs_get('id', $_GET)):false;
		$vr['menu']=$this->nav_html($this->nav_tree());
		return axs_tpl_parse(axs_tpl(false, $this->axs_local['plugin'].'.tpl', __FILE__, __LINE__), $vr);
		} #</banner_categorys()>
	function data_format(&$cl, &$vr=array()) {
		foreach (array('', '_discount') as $v) {
			$cl['price'.$v.'.vat']=$cl['price'.$v]/100*$cl['vat'];
			$cl['price'.$v.'+vat']=$cl['price'.$v]+$cl['price'.$v.'.vat'];
			$vr['price'.$v.'+vat']=($cl['price'.$v.'+vat']>0) ? '&euro; '.number_format($cl['price'.$v.'+vat'], 2, '.', ' '):'';
			$vr['price'.$v.'+vat']='<span>'.$vr['price'.$v.'+vat'].'</span>';
			}
		if ($cl['price_discount']>0) $vr['price+vat']='<del>'.$vr['price+vat'].'</del>';
		$vr['stock.available']=$this->form->tr->s(($cl['stock']>0) ? 'stock.available.txt':'stock.order.txt');
		//require_once(axs_dir('site_base').'eshop.class.php');
		//$cl['_in_cart']=axs_eshop::cart_get_product($cl['id']);
		} #</data_format()>
	function list_articles_row(&$cl, &$vr=array()) {
		global $axs;
		//$cl['title_alias']=$cl['title_alias_'.$axs['l']];
		parent::list_articles_row($cl, $vr);
		$this->data_format($cl, $vr);
		//unset($cl['_in_cart']);
		} #</list_articles_row()>
	function display_article_similar($cl) {
		global $axs;
		$html='';
		$tpl=axs_tpl(false, array($this->axs_local['plugin'].'_banner.banner.list.tpl', $this->name.'_banner.banner.list.tpl'), __FILE__, __LINE__);
		$where=array();
		foreach ($cl['similar'] as $k=>$v) $where[]="p.`id`='".$v['product_id']."'";
		$result=axs_db_query("SELECT p.* FROM `".$this->form->sql_table."` AS p\n".
		"	INNER JOIN `".$this->form->sql_table."_table` AS t ON t.`product_id`=p.`id`\n".
		"	WHERE ".implode(" OR ", $where)." GROUP BY p.`id` ORDER BY t.`nr`", 1, $this->db, __FILE__, __LINE__);
		foreach ($result as $v) {
			$vr=array();
			$this->list_articles_row($v, $vr);
			$html.=axs_tpl_parse($tpl, $vr);
			}
		return $html;//axs_tpl_parse(, $vr+$this->form->tr->tr);
		} #</display_article_similar()>
	function display_article_vars(&$cl=array(), &$vr=array()) {
		global $axs;
		$vr['msg']=array();
		$this->display_article_query($id);
		$data=axs_eshop::cart_get_item(axs_get('cart_id', $_GET));
		if (!isset($data['data']['options'])) $data['data']=array('options'=>'', );
		if (!empty($_POST['_cart_add'])) {
			$data['amount']=$_POST['amount']+0;
			$data['data']=array('options'=>$_POST['options']+0, );
			}
		$data['label']=array('title'=>'', 'id'=>'', 'options'=>'', );
		foreach ($cl['options'] as $k=>$v) {
			if ($k.''===$data['data']['options'].'') {
				$data['label']['options']='; '."\n".$this->form->tr->t('options.lbl').': '.$this->form->value_display('label', $this->form->structure['options']['options']['label'], $v);
				$cl['options'][$k]['checked']=' checked="checked"';
				$cl['options'][$k]['selected']=' selected="selected"';
				}
			else $cl['options'][$k]['checked']=$cl['options'][$k]['selected']='';
			}
		
		parent::display_article_vars($cl, $vr);
		$this->data_format($cl, $vr);
		$vr['form.action']=$this->url_root.axs_url($this->url, array('cart_id'=>$data['cart_id']));
		$vr['form.disabled']=($cl['stock']>0) ? '':' disabled="disabled"';
		$vr['options.required']=($cl['options']) ? ' required="required"':'';
		$vr['amount']=($data['amount']>0) ? $data['amount']+0:1;
		$vr['similar']=(!empty($cl['similar'])) ? $this->display_article_similar($cl):'';
		if (isset($_POST['_cart_add'])) {
			#<Validate>
			if ($vr['options.required']) {
				if (!$data['data']['options']) $vr['msg']['options']=$this->form->structure['options']['label'].': '.$this->form->tr->t('msg_value_required');
				}
			#if ($this->cl['stock']<$this->cl['amount']) $vr['msg']['stock']='Product out of stock!';
				
			#</Validate>
			if (empty($vr['msg'])) $vr['msg']+=axs_eshop::cart_edit($data['cart_id'], array(
				'label'=>implode('', array(
					'title'=>html_entity_decode($vr['title']),
					'id'=>'; '."\n".$this->form->tr->t('id.lbl').': '.$vr['id'],
					$data['label']['options'],
					)),
				'price'=>($cl['price_discount']>0) ? $cl['price_discount']:$cl['price'],
				'vat'=>$cl['vat'],
				'amount'=>$data['amount'],
				'amount_step'=>$this->amount_step,
				'amount_unit'=>$this->amount_unit,
				'amount_unit_type'=>$this->amount_unit_type,
				'product_size_x'=>$cl['size_x'],
				'product_size_y'=>$cl['size_y'],
				'product_size_z'=>$cl['size_z'],
				'product_weight'=>$cl['weight'],
				'product_form'=>$this->axs_local['plugin'],
				'product_id'=>$cl['id'],
				'product_url'=>$this->url_root.axs_url($this->url, array('cart_id'=>'', ), false),
				'product_img_url'=>($vr[$this->pic['key']]) ? $vr[$this->pic['key'].'.url']:'',
				'product_img_file'=>(file_exists($this->pic['f_fs'])) ? $this->pic['f_fs']:'',
				'product_table'=>$this->form->sql_table,
				'product_table_stock_col'=>'',#'stock',
				'data'=>$data['data'],
				), __FILE__, __LINE__, $this->url_root.axs_url($this->url, array('id'=>false, 'cart_id'=>false, ), false).'#id'.$cl['id']);
			}
		$vr['msg']=$this->form->msg_html($vr['msg']);
		unset($vr['_in_cart']);
		} #</display_article_vars()>
	} #</class::axs_eshop_products>
#2015-05-15 ?>