<?php #2015-12-21
defined('AXS_PATH_CMS') or exit(require('../index.php')); #<Prevent direct access />

#<Class "axs_articles_base_edit" extends "axs_articles_mailinglist" through "axs_articles" />
include_once('articles_mailinglist.class.php');
class axs_articles extends axs_articles_mailinglist {}
include_once('articles.base.edit.class.php');

#<Customized editor class>
class axs_articles_mailinglist_edit extends axs_articles_base_edit {
	var $sql_desc=' DESC';
	function __construct(&$e) {
		global $axs, $axs_content;
		$f=new axs_form();
		$f=$f->structure_get_thead($e->content_form_get($axs['c']));
		foreach ($f as $k=>$v) {
			$f[$k]['search']=1;
			if (isset($this->fields_article[$k])) $f[$k]=array_merge($this->fields_article[$k], $f[$k]);
			}
		$this->fields_article=axs_fn::array_el_add(
			$this->fields_article,
			axs_fn::array_el_pos($this->fields_article, 'email', 'end')+1,
			$f
			);
		axs_articles_base_edit::__construct($e);
		} #</axs_articles_mailinglist_edit()>
	} #</class::axs_articles_mailinglist_edit>
class axs_articles_edit extends axs_articles_mailinglist_edit {}
#</Customized editor class>

return new axs_articles_edit($this);
#2013-03-03 ?>