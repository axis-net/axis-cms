     <article class="clear-content">
      <header>
	   <h1>{$title}</h1>
       <time class="publ" datetime="{$publ.datetime}">{$publ}</time>
	   <div class="summary">
	    <a href="?axs%5Bgw%5D={$file.t2.url}" target="popup">{$file.t1}</a>
		{$summary}
       </div>
	  </header>
	  <div class="text">
       {$text}
      </div>
      <a class="back" href="{$back}" rel="nofollow">&lt; {$back_lbl}</a>
	  <!--{$qr_code}-->
	 </article>
{$comments}