<?php #2014-01-08 utf-8
return array(
	'article.lbl'=>'artikkel',
	'comments.lbl'=>'kommentaarid',
	'back.lbl'=>'tagasi',
	'reset.lbl'=>'reset',
	'top.lbl'=>'üles',
	'rank.up.lbl'=>'liiguta üles',
	'rank.down.lbl'=>'liiguta alla',
	
	'add.lbl'=>'lisa uusi artikleid',
	'add.article.lbl'=>'lisa uus artikkel',
	'add.batch.lbl'=>'lisa hulgi',
	'add.folder.lbl'=>'lisa uus kaust',
	'add.confirm'=>'Kas soovid lisada uue artikli?',
	'edit.lbl'=>'muuda',
	'edit_article_head.lbl'=>'muuda artikli päist',
	'preview.lbl'=>'eelvaade',
	'del.lbl'=>'kustuta',
	'del_confirm.lbl'=>'kas oled kindel, et soovid artikli kustutada?',
	
	'header_edit.lbl'=>'muuda struktuuri',
	'search.lbl'=>'otsi',
	'search.found.lbl'=>'leitud',
	'search.order.lbl'=>'järjesta',
	
	'input_file_del_lbl'=>'kustuta fail',
	'msg_value_required'=>'välja täitmine on kohustuslik!',
	'msg_value_invalid'=>'sisestatud väärtus ei ole sobiv!',
	'msg_value_unique'=>'väärtus peab olema kordumatu!',
	);
#2009-02-26 ?>