<?php #2015-03-23
defined('AXS_SITE_ROOT') or exit(require('../index.php')); #<Prevent direct access />

#<Class "axs_articles_base_edit" extends "axs_articles_albums" through "axs_articles" />
include_once('articles_albums.class.php');
class axs_articles extends axs_articles_albums {}
include_once('articles.base.edit.class.php');

#<Customized editor class>
class axs_articles_albums_edit extends axs_articles_base_edit {
	/*function list_articles_row(&$cl, &$vr=array()) {
		parent::list_articles_row($cl, $vr);
		if (!empty($cl['thumbs'])) $vr['thumbs']=$vr['_data']['thumbs']='<a href="'.$vr['article.url'].'">'.$this->thumbs($cl).'</a>';
		return $vr;
		}*/ #</list_articles_row()>
	function installer_upgrade_albums_f() {
		global $axs;
		$q="SELECT t.id, t.id_new, t.parent_id FROM `".$this->px."gallerys_db_list` AS t INNER JOIN `axs_gallerys_db` AS p ON p.id=t.parent_id WHERE p.site='".$this->site_nr."' AND p.lang='".$axs['l']."' AND p.cid='".$this->axs_local['c']."'";
		#exit(dbg($q));
		$r=axs_db_query($q, 1, $this->db, __FILE__, __LINE__);
		$dir=axs_dir('content').$this->dir_content_files;
		if (!is_dir($bak=$dir.'!old/')) mkdir($bak);
		foreach ($r as $cl) {
			$d=$dir.$cl['parent_id'].'/';
			if (is_dir($d)) rename($d, $bak.$cl['parent_id']);
			$f=$cl['parent_id'].'/'.$cl['id'];
			if (!is_dir($new=$dir.$cl['id_new'].'/')) mkdir($new);
			foreach (array('.t1.jpg'=>'_t2.jpg', '.t2.jpg'=>'.jpg') as $k=>$v) {
				$fold=$bak.$f.$v;
				$fnew=$new.$cl['id_new'].$k;
				dbg($fold, file_exists($fold));
				if ((file_exists($fold)) && (!file_exists($fnew))) copy($fold, $fnew);
				}
			}
		} #</installer_upgrade_albums_f()>
	} #</class::axs_articles_albums_edit>
#</Customized editor class>

return new axs_articles_albums_edit($this);
/*
#import folders
ALTER TABLE `axs_gallerys_db` ADD `id_new` INT UNSIGNED NOT NULL AFTER `id`
ALTER TABLE `axs_articles` ADD `id_old` INT UNSIGNED NOT NULL AFTER `id`
INSERT INTO `axs_articles` 
	(`id_old`, `site`, `c`, `l`, `plugin`, `_type`, `_parent`, `title`, `thumbs`, `date`, `comments`, `comments_mail`, `updated`, `updated_uid`)
SELECT 
	id, site, cid, lang, 'articles_albums', '_folder', 0, title, thumbs, `time`, comments, comments_mail, modified, modified_uid
	FROM `axs_gallerys_db` ORDER BY `time` DESC
UPDATE `axs_articles` AS n, `axs_gallerys_db` AS o SET o.`id_new`=n.`id` WHERE o.`id`=n.`id_old` AND `plugin`='articles_albums' AND `_type`='_folder'
#import articles
ALTER TABLE `axs_gallerys_db_list` ADD `id_new` INT UNSIGNED NOT NULL AFTER `id`
INSERT INTO `axs_articles` 
	(`id_old`, `site`, `c`, `l`, `plugin`, `_type`, `_parent`, `title`, `date`, `updated`, `updated_uid`, `publ`, `text`)
SELECT 
	a.id, f.site, f.cid, f.lang, 'articles_albums', '', f.`id_new`, a.title, a.`time`, a.modified, a.modified_uid, a.rank, a.html
	FROM `axs_gallerys_db_list` AS a LEFT JOIN `axs_gallerys_db` AS f ON f.id=a.parent_id
UPDATE `axs_articles` AS n, `axs_gallerys_db_list` AS o SET o.`id_new`=n.`id` WHERE o.`id`=n.`id_old` AND n.`plugin`='articles_albums' AND n.`_type`=''
UPDATE `axs_articles` SET `title_alias`=TRIM(`title_alias`)
*/
#2010-05-13 ?>