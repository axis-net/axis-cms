				<article class="item clear-left"{$lang}>
					<h4><a href="{$article.url}" target="_blank">{$title}</a></h4>
					<div class="description">{$summary}</div>
					<time datetime="{$time.datetime}">{$time}</time>
				</article>
