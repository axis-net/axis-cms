<!DOCTYPE html>
<html lang="{$l}" class="{$class}">
<head>
<meta charset="{$charset}" />
<title>{$title_menu} - {$title_site}</title>
<meta name="description" content="{$description}" />
<meta name="generator" content="AXIS CMS" />
<!--base href="{$/}" /-->
<meta name="HandheldFriendly" content="True" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<link type="text/css" href="{$/}?axs%5Bgw%5D=axs.css" rel="stylesheet" media="screen" />
<link type="text/css" href="{$/}axs_site/index.css" rel="stylesheet" media="screen" />
<link type="text/css" href="{$/}axs_site/index.print.css" rel="stylesheet" media="print" />
<script src="{$/}?axs%5Bgw%5D=axs.js"></script>
<script src="{$/}?axs%5Bgw%5D=axs.menu.js"></script>
{$head}
{$code.head}
</head>

<body>
<script>axs.mediaState();</script>
<nav class="axs inpage content" lang="en">
	<a href="#content">Jump to main content</a> <a href="#menu1">Jump to main navigation</a> <a href="#langs">Choose language</a> 
</nav>
<nav id="langs" class="langs">
	<h2 class="visuallyhidden"><span lang="en">Language</span>: {$lang.current}</h2>
	<ul>{$lang.list}</ul>
</nav>
<header>
{$content.header}
</header>
<div class="login">{$login}</div>
<nav id="menu1" class="axs menu default vertical mobile_fixed mobile_toggle mobile_toggleout animation fade">
	<h2 class="title"><a class="toggle-switch"><span class="icon" title="{$menu.menu1.title}"></span> {$menu.menu1.title}</a></h2>
{$menu.menu1}
	<script>axs.menu.attach("menu1");</script>
</nav>
<main id="content" class="content {$class}">
	<div id="breadcrumb">{$breadcrumb}</div>
	<h1 id="content_title">{$content..title}</h1>
{$content}
</main>
<section id="banner1">
	<h2 class="section-title">{$content.banner1.title}</h2>
{$content.banner1}
</section>
<footer>
{$content.footer}
</footer>
<small id="dev">
	<a class="admin" href="{$/}admin/" rel="nofollow">- admin -</a>
	<a class="axs" href="https://www.veebid.eu/axiscms/" title="Powered by AXIS CMS" target="_blank" lang="en"><em>powered by</em> AXIS CMS</a>
</small>
<nav class="axs inpage top" lang="en"><a href="#">Go to top</a></nav>
<script>axs.menu.inpageTopNavInit(document.querySelector("nav.inpage.top"));</script>
{$code.bottom}
</body>
</html>