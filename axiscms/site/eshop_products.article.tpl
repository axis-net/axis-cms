		<article class="article product clear-content {$_class}">
			<header class="clear-content">
				<h1>{$title}</h1>
				<div class="socialmedia">{$_socialmedia}</div>
			</header>
			<div id="product-images" class="left">
				<a href="{$file.t2.url}" class="fancybox" rel="g1"><img id="product-images-big" src="{$file.t1.url}" data-zoom-image="{$file.t2.url}" alt="{$title}" /></a>
				<div id="product-images-list" class="slider display-table-row">
					<a class="scroll prev" href="#pics">&lt;</a>
					<div>
						<div class="list clear-content">
{$files}
						</div>
					</div>
					<a class="scroll next" href="#pics">&gt;</a>
				</div>
				<script>axs.eshop_products.init('product-images');</script>
				<script src="axiscms/lib.js/axs.slider.js"></script>
				<script>axs.slider.init("#pics",{group_count:3});</script>
			</div>
			<div class="right">
				<dl>
					<dt>{$brand.lbl}</dt><dd>{$brand}</dd>
					<dt>{$id.lbl}</dt><dd>{$id}</dd>
					<dt>{$stock.comment}</dt><dd>{$stock.available}</dd>
					<dt>{$delivery.lbl}</dt><dd>{$delivery} {$delivery.txt}</dd>
				</dl>
				<dl class="price"><dt>{$price.lbl}</dt><dd>{$price.vat} {$price_discount.vat}{$currency.symbol}</dd></dl>
				<form id="cart_add" class="cart_add" action="{$form.action}" method="post" ectype="multipat/form-data"{$form.disabled}>
					<input name="product_id" value="{$id}" type="hidden" />
					{$msg}
					<label for="options">{$options.lbl}: 
						<select id="options" name="options"{$options.required}>
							<option value=""> - </option>
{$options}
						</select>
					</label>
					<label>{$amount.lbl}: <input id="amount" name="amount" type="number" value="{$amount}" required="required" step="{$amount.step}" min="{$amount.step}" /></label>
					<input name="_cart_add" type="submit" value="{$add_to_cart.txt}" class="button" />
				</form>
			</div>
			<div id="tabs" class="tabs clear-content">
				<ul class="nav">
					<li class="current"><a href="#text">{$summary.lbl}</a></li>
					<li><a href="#comments">{$comments.form.title}({$comments.count})</a></li>
				</ul>
				<div id="text" class="tab current clear-content">
					<h2>{$summary.lbl}</h2>
					{$summary}
				</div>
				<div id="comments" class="tab clear-content">
					{$comments}
				</div>
				<script>axs.menu.tabs("#tabs");</script>
			</div>
			<h2>{$similar.lbl}</h2>
			{$similar}
		</article>