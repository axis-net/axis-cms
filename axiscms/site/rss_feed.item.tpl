  <item>
   <title>{$title}</title>
   <link>{$link}</link>
   <description>{$description}</description>
   <pubDate>{$pubDate}</pubDate>
   <guid>{$link}</guid>
  </item>
