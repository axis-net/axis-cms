<?php #2014-07-05
$plugin_def=array(); # plugin default data
$plugin_def['name']['et']='Blog';
$plugin_def['title']['']='';
$plugin_def['help']['']='';
$plugin_def['mkdir']='files';
$plugin_def['content']['']='<?php #|'.$axs['time'].',,'.axs_get('user', $axs_user).',,||?>'."\n";
$plugin_def['tr']['et']=array(
	'form.title'=>'blog',
	'article_head.lbl'=>'Artikli päis',
	'id.lbl'=>'ID',
	'title.lbl'=>'pealkiri',
	'publ.lbl'=>'postitatud',
	'file.lbl'=>'pildifail',
	'summary.lbl'=>'sissejuhatus',
	'article_content.lbl'=>'artikli sisu',
	'text.lbl'=>'sisutekst',
	'seo.lbl'=>'SEO',
	'title_alias.lbl'=>'URL alias',
	'meta_description.lbl'=>'<meta description>',
	'comments.lbl'=>'luba kommentaarid',
	'comments_mail.lbl'=>'saada kommentaarid e-postiga',
	'updated.lbl'=>'muudetud',
	'save.lbl'=>'salvesta',
	'read_lbl'=>'loe lähemalt',
	'prev_lbl'=>'eelmine',
	'next_lbl'=>'järgmine',
	'comments.form.title'=>'Kommentaarid',
	'comments.id.lbl'=>'ID',
	'comments.time.lbl'=>'aeg',
	'comments.name.lbl'=>'nimi',
	'comments.message.lbl'=>'kommentaar',
	'comments.captcha.lbl'=>'kontrollkood',
	'comments.post.lbl'=>'postita',
	'comments.user_ip.lbl'=>'IP',
	'comments.hostname.lbl'=>'domeen',
	'msg'=>'Kommentaar salvestatud!',
	'msg_value_required'=>'välja tämine on kohustuslik!',
	'msg_value_invalid'=>'sisestatud väärtus ei ole sobiv!',
	'msg_value_unique'=>'väärtus peab olema kordumatu!',
	'back_lbl'=>'tagasi',
	);
$plugin_def['tr']['en']=array(
	'form.title'=>'blog',
	'article_head.lbl'=>'article head',
	'id.lbl'=>'ID',
	'title.lbl'=>'title',
	'publ.lbl'=>'rank',
	'file.lbl'=>'picture',
	'summary.lbl'=>'summary',
	'article_content.lbl'=>'article content',
	'text.lbl'=>'text',
	'seo.lbl'=>'SEO',
	'title_alias.lbl'=>'URL alias',
	'meta_description.lbl'=>'<meta description>',
	'rank_up_lbl'=>'rank up',
	'rank_down_lbl'=>'rank down',
	'comments.lbl'=>'allow comments',
	'comments_mail.lbl'=>'send comments to e-mail',
	'updated.lbl'=>'updated',
	'save.lbl'=>'save',
	'read_lbl'=>'read more',
	'prev_lbl'=>'previous',
	'next_lbl'=>'next',
	'comments.form.title'=>'comments',
	'comments.id.lbl'=>'ID',
	'comments.time.lbl'=>'time',
	'comments.name.lbl'=>'name',
	'comments.message.lbl'=>'comment',
	'comments.captcha.lbl'=>'Verification code',
	'comments.post.lbl'=>'post',
	'comments.user_ip.lbl'=>'IP',
	'comments.hostname.lbl'=>'domain',
	'msg'=>'Comment has been saved!',
	'msg_value_required'=>'input required!',
	'msg_value_invalid'=>'invalid input!',
	'msg_value_unique'=>'input value must be unique!',
	'back_lbl'=>'back',
	);

if (defined('AXS_SITE_NR')) {
require_once('articles_blog.class.php');
$data=new axs_articles_blog($axs_local);
if ($tmp=$data->error()) return $tmp;
if ($axs_local['section']!=='content') {
	$data->banner_init();
	return $data->banner_parse();
	}
$data->page_init($plugin_def);
$data->vr['content']=include('deflt.plugin.php');
return $data->page_parse();
}
#2012-11 ?>