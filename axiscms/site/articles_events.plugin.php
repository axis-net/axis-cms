<?php #2016-01-27
$plugin_def=array(); # plugin default data
$plugin_def['name']['et']='üritused';
$plugin_def['title']['']='';
$plugin_def['help']['']='';
$plugin_def['mkdir']='files';
$plugin_def['content']['']='<?php #|'.$axs['time'].',,'.axs_get('user', $axs_user).',,||?>'."\n";
$plugin_def['tr']['et']=array(
	'form.title'=>'üritused',
	'article_head.lbl'=>'Artikli päis',
	'id.lbl'=>'ID',
	'title.lbl'=>'pealkiri',
	'promote.lbl'=>'näita bänneris',
	'hide_home.lbl'=>'ära näita avalehel',
	'publ.lbl'=>'avaldatud',
	'times.lbl'=>'toimumise ajad',
	'times.date.lbl'=>'kuupäev',
	'times.time.lbl'=>'kellaaeg',
	'date.lbl'=>'kuupäev',
	'time.lbl'=>'kellaaeg',
	'file.lbl'=>'pildifail',
	'summary.lbl'=>'sissejuhatus',
	'text.lbl'=>'sisutekst',
	'seo.lbl'=>'SEO',
	'title_alias.lbl'=>'URL alias',
	'meta_description.lbl'=>'<meta description>',
	'comments.lbl'=>'luba kommentaarid',
	'comments_mail.lbl'=>'saada kommentaarid e-postiga',
	'updated.lbl'=>'muudetud',
	'save.lbl'=>'salvesta',
	
	'new.lbl'=>'toimuvad üritused',
	'archive.lbl'=>'varem toimunud',
	
	'read_more.lbl'=>'loe lähemalt',
	'prev.lbl'=>'eelmine',
	'next.lbl'=>'järgmine',
	'comments.form.title'=>'Kommentaarid',
	'comments.id.lbl'=>'ID',
	'comments.time.lbl'=>'aeg',
	'comments.name.lbl'=>'nimi',
	'comments.message.lbl'=>'kommentaar',
	'comments.captcha.lbl'=>'kontrollkood',
	'comments.post.lbl'=>'postita',
	'comments.user_ip.lbl'=>'IP',
	'comments.hostname.lbl'=>'domeen',
	'msg'=>'Kommentaar salvestatud!',
	'msg_value_required'=>'välja tämine on kohustuslik!',
	'msg_value_invalid'=>'sisestatud väärtus ei ole sobiv!',
	'msg_value_unique'=>'väärtus peab olema kordumatu!',
	'back.lbl'=>'tagasi',
	);

if (defined('AXS_SITE_NR')) {
require_once('articles_events.class.php');
$data=new axs_articles_events($axs_local);
if ($tmp=$data->error()) return $tmp;
if ($axs_local['section']!=='content') {
	$data->banner_init();
	return $data->banner_parse();
	}
$data->page_init($plugin_def);
$data->vr['content']=include('deflt.plugin.php');
return $data->page_parse();
}
#2011-09-21 ?>