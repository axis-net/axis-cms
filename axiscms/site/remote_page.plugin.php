<?php #2019-05-24
$plugin_def=array(); # plugin default data
$plugin_def['name']['en']='Page from another site';
$plugin_def['name']['et']='Lehekülg teiselt saidilt';
$plugin_def['title']['']='Remote page';
$plugin_def['help']['']='';
$plugin_def['mkdir']='files';
$plugin_def['content']['']='<?php #|'.$axs['time'].',,'.axs_get('user', $axs_user).',,||?>'."\n";
# media=media&img_proc_t=&img_w_t=&img_h_t=&img_proc=&img_w=&img_h=

if (defined('AXS_SITE_NR')) {
$content='';
$file_time=0;
$file=axs_dir('content').$axs_local['dir'].$axs_local['c'].'_files/cache.html';
if (file_exists($file)) {
	$content=file_get_contents($file);
	$file_time=filemtime($file);
	}
#<Update expired content>
if ((date('Ymd', $axs['time'])!==date('Ymd', $file_time)) || ($axs['time']-43200<$file_time) || (!$content)) {
	$new='';
	foreach ((array)axs_tab::proc(axs_content::text_get(), array('time'=>$axs['time'])) as $v) {
		$v=explode(' ', trim(html_entity_decode(strip_tags($v[1]))), 2);
		if (!$url=axs_get(0, $v)) continue;
		$selector=axs_get(1, $v);
		$html=file_get_contents($url);
		if (($html) && ($selector)) {
			$html=str_replace('<meta charset="', '<meta http-equiv="Content-Type" content="text/html; charset=', $html);
			$dom=new DomDocument();
			#$dom->preserveWhiteSpace=false;
			libxml_use_internal_errors(true);
			$dom->loadHTML($html);
			libxml_use_internal_errors(false);
			$html='';
			$xpath=new DOMXpath($dom);
			$nodes=$xpath->query($selector);
			foreach ($nodes as $node) $html.=$dom->saveHTML($node);
			}
		if ($html) $new.=$html;
		else axs_log(__FILE__, __LINE__, $axs_local['plugin'], 'Error loading remote content from '.$url.' '.$selector);
		}
	if ($new) {
		file_put_contents($file, $new);
		$content=$new;
		}
	}
#</Update expired content>
return $content;
}
#2019-05-24 ?>