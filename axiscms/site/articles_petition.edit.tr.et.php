<?php #03.03.2013 utf-8
return array(
	'form.title'=>'listiga liitunud',
	'fields_lbl'=>'vormi väljad',
	'time_subscribe_lbl'=>'liitunud',
	'disable_lbl'=>'eemaldatud',
	'config.list_name_lbl'=>'listi nimi',
	'config.list_type_lbl'=>'listiserveri tüüp',
	'config.list_address_subscribe_lbl'=>'liitumise aadress',
	'config.list_address_unsubscribe_lbl'=>'eemaldamise aadress',
	);
#03.03.2013 ?>