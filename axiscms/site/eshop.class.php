<?php #2018-08-31
class axs_eshop {
	static $name='eshop';
	static $site_nr;
	static $db;
	static $px;
	static $c='eshop';
	static $cart_fields=array(
		'cart_id'=>'', 'product_form'=>'', 'product_id'=>true, 'product_url'=>true, 'product_img_url'=>'', 'product_img_file'=>'', 'label'=>true,
		'price'=>true, 'vat'=>true, 'amount'=>true, 'amount_unit'=>true, 'amount_unit_type'=>true,  'amount_step'=>true, 'vat'=>false, 
		'product_size_x'=>true, 'product_size_y'=>true, 'product_size_z'=>true, 'product_weight'=>false,
		'product_table'=>'', 'product_table_stock_col'=>'', 'data'=>false, /*'currency'=>'',*/
		);
	static $cfg=array();
	//static $delivery_cat=array(1, 2, 3, 4, 5, );
	static $tr;
	static $banklink=false;
	static function cart_init() {
		if (!isset($_SESSION)) session_start();
		if (!isset($_SESSION['eshop'])) $_SESSION['eshop']=array(
			'cart'=>array(), 'client'=>array('delivery'=>'', ), 'orders'=>array(), 'order_files'=>array(), 'currency'=>'',
			);
		if (empty(self::$cfg)) self::conf_get();
		}
	static function cart_banner(&$axs_local) {
		global $axs;
		$vr=array(
			'form.action'=>htmlspecialchars('?'.$_SERVER['QUERY_STRING']),
			'count'=>count($_SESSION['eshop']['cart']), 'sum'=>0, 'sum.vat'=>0, 'sum+vat'=>0, 'cart'=>'', 'orders'=>'', 'c'=>$axs_local['c'],
			'link'=>'?'.axs_url($axs['url'], array('c'=>$axs_local['c'])),
			'currency'=>self::$cfg['currency'], 'currency.symbol'=>self::$cfg['currency.symbol'],
			);
		$vr+=self::$tr->get();
		$tpl=array(''=>'banner.tpl', 'cart.item'=>'banner.item.tpl', 'orders'=>'cart.orders.tpl', 'orders.item'=>'cart.orders.item.tpl', 'bank'=>'bank.tpl', );
		foreach ($tpl as $k=>$v) $tpl[$k]=axs_tpl(false, 'eshop.'.$v);
		$nr=0;
		foreach ($_SESSION['eshop']['cart'] as $k=>$v) {
			$v['nr']=++$nr;
			self::cart_item_format($k, $v);
			$v['url']=$vr['link'].'#cart-item'.$nr;
			$vr['cart'].=axs_tpl_parse($tpl['cart.item'], $v);
			}
		foreach (self::cart_sum() as $k=>$v) $vr[$k]=number_format($v, 2, '.', ' ');
		if (!empty($_SESSION['eshop']['orders'])) {
			$tmp=array('orders'=>'', );
			foreach ($_SESSION['eshop']['orders'] as $v) {
				$tmp['orders'].=axs_tpl_parse($tpl['orders.item'], $v+array(
					'url'=>'?'.axs_url($axs['url'], array('c'=>$axs_local['c'], 'order_nr'=>$v['order_nr'])), 'time'=>'',
					));
				}
			$vr['orders']=axs_tpl_parse($tpl['orders'], $tmp);
			}
		if (axs_tpl_has($tpl[''], 'bank')) {
			if (!self::$banklink) self::$banklink=new axs_banklink($axs['l'], self::$site_nr);
			self::$banklink->list_get('pay');
			$vr['bank']='';
			foreach (self::$banklink->bank_list as $k=>$v) {
				foreach ($v as $kk=>$vv) if (!is_scalar($vv)) unset($v[$kk]);
				$v['img']=($v['img']) ? '<img src="'.$v['img'].'" alt="'.htmlspecialchars($v['text']).'" />':'';
				$vr['bank'].=axs_tpl_parse($tpl['bank'], $v);
				}
			}
		return axs_tpl_parse($tpl[''], $vr);
		} #</cart_banner()>
	static function conf_get() {
		global $axs;
		self::$site_nr=AXS_SITE_NR;
		self::$db=$axs['cfg']['site'][self::$site_nr]['db'];
		self::$px=$axs['cfg']['site'][self::$site_nr]['prefix']; 
		$result=axs_db_query("SELECT t.*, cur.`symbol` AS `currency.symbol`, `vat`.`vat`\n".
		"	FROM `".self::$px.self::$name."_config` AS t\n".
		"	LEFT JOIN `".self::$px."business_currency` AS `cur` ON `cur`.`currency`=t.`val`\n".
		"	LEFT JOIN `".self::$px."business_vat` AS `vat` ON `vat`.`code`=t.`val`\n".
		"	WHERE t.`site`='".self::$site_nr."'", 1, self::$db, __FILE__, __LINE__);
		foreach ($result as $cl) {
			self::$cfg[$cl['key']]=$cl['val'];
			foreach (array('currency'=>'currency.symbol', 'vat'=>'vat', ) as $k=>$v) if ($cl['key']===$k) self::$cfg[$v]=$cl[$v];
			}
		return self::$cfg;
		} #</conf_get()>
	static function cart_del($cart_id) { #<Delete items from shopping cart />
		if (!is_array($cart_id)) $cart_id=array($cart_id=>$cart_id);
		foreach ($cart_id as $k=>$v) unset($_SESSION['eshop']['cart'][$k]);
		} #</cart_del()>
	static function cart_edit($cart_id, $item, $file='', $line='', $back_url='') { # <add/edit shopping cart items />
		global $axs;
		self::cart_init();	
		$item['cart_id']=$cart_id;
		if ($err=self::cart_validate_item($item, $file, $line)) return $err;
		if (isset($_SESSION['eshop']['cart'][$cart_id])) $_SESSION['eshop']['cart'][$cart_id]=$item;
		else {
			$_SESSION['eshop']['cart'][]=$item;
			foreach ($_SESSION['eshop']['cart'] as $k=>$v) $cart_id=$k;
			$_SESSION['eshop']['cart'][$cart_id]['cart_id']=$cart_id;
			}
		axs_exit(axs_redir('?'.axs_url($axs['url'], array('c'=>self::$c, 'b'=>urlencode($back_url)), false)));
		} #</cart_edit()>
	static function cart_edit_amount($set, $file='', $line='') { # <edit shopping cart items amount />
		global $axs;
		foreach ($set as $k=>$v) if (isset($_SESSION['eshop']['cart'][$k])) {
			$v=array_merge(self::cart_get_item($k), array('amount'=>$v+0));
			if ($err=self::cart_validate_item($v, $file, $line)) continue;
			else $_SESSION['eshop']['cart'][$k]['amount']=$v['amount'];
			}
		} #</cart_edit_amount()>
	static function cart_get() {
		return (isset($_SESSION['eshop']['cart'])) ? $_SESSION['eshop']['cart']:array();
		}
	static function cart_get_item($cart_id) {
		if (isset($_SESSION['eshop']['cart'][$cart_id])) return $_SESSION['eshop']['cart'][$cart_id];
		$tmp=self::$cart_fields;
		foreach ($tmp as $k=>$v) $tmp[$k]='';
		return $tmp;
		} #</cart_get_item()>
	static function cart_get_product($product_id) {
		foreach ($_SESSION['eshop']['cart'] as $k=>$v) if ($v['product_id']===$product_id) return array_merge(array('cart_id'=>$k), $v);
		$tmp=self::$cart_fields;
		foreach ($tmp as $k=>$v) $tmp[$k]='';
		return $tmp;
		}
	static function cart_item_format($cart_id, &$v) {
		foreach (array('nr'=>'')+axs_eshop::$cart_fields as $kk=>$vv) if (!isset($v[$kk])) $v[$kk]='';
		$v['cart_id']=$cart_id;
		$v['price+vat']=axs_eshop::tax($v['price'], $v['vat']);
		$v['sum']=$v['price']*$v['amount'];
		$v['sum.vat']=$v['sum']/100*$v['vat'];
		$v['sum+vat']=$v['sum']+$v['sum.vat'];
		$v['vat']=($v['vat']==0) ? '-':$v['vat'].'%';
		foreach (array('price', 'price+vat', 'sum', 'sum.vat', 'sum+vat') as $vv) $v[$vv]=number_format($v[$vv], 2, '.', ' ');
		$v['label']=axs_html_safe($v['label']);
		$v['pic']=($v['product_img_url']) ? '<img src="'.htmlspecialchars($v['product_img_url']).'" alt="'.htmlspecialchars($v['product_id']).'" />':'';
		//if (!strpos($v['product_url'], '#')) $v['product_url'].=$k;
		$v['product_url']=htmlspecialchars($v['product_url']);
		unset($v['data']);
		} #</cart_item_format>
	static function cart_item_label_html($str) {
		$str=axs_html_safe($str);
		return '<span>'.str_replace("; \n", '</span> <span>', $str).'</span>';
		} #</cart_item_label_html()>
	static function cart_sum($prefix='', $format=false) {
		$cl=array('sum'=>0.00, 'sum.vat'=>0.00, 'sum+vat'=>0.00, );
		foreach ($_SESSION['eshop']['cart'] as $k=>$v) {
			foreach (axs_eshop::$cart_fields as $kk=>$vv) if (!isset($v[$kk])) $v[$kk]='';
			$cl['sum']+=$v['price']*$v['amount'];
			$cl['sum.vat']+=($v['price']*$v['amount'])/100*$v['vat'];
			$cl['sum+vat']+=round(self::tax($v['price'], $v['vat']), 2)*$v['amount'];
			}
		return $cl;
		} #</cart_sum>
	static function cart_validate(&$msg) {
		if (empty($_SESSION['eshop']['cart'])) $msg['error_cart']=self::$tr->s('msg_cart_empty');
		$nr=1;
		foreach ($_SESSION['eshop']['cart'] as $k=>$v) {
			if (self::cart_validate_item($v, __FILE__, __LINE__)) $msg['error_cart_'.$k]=self::$tr->s('msg_cart_error').
			' <a href="#cart-item'.$nr.'">'.$nr.'. '.axs_html_safe($v['label']).'</a>';
			$nr++;
			}
		if (!self::$cfg['currency']) $msg[]=axs_log(__FILE__, __LINE__, __CLASS__, 'No currency specified!');
		} #</cart_validate>
	static function cart_validate_item($entry, $file, $line) {
		global $axs, $axs_content;
		$error=array();
		foreach (self::$cart_fields as $k=>$v) if (!array_key_exists($k, $entry)) $error[]='Key not set: ['.$k.']!';
		foreach (array('price', ) as $v) if ($entry[$v]<=0) $error[]='invalid: ['.$v.']="'.$entry[$v].'"!';
		if ($entry['amount']<$entry['amount_step']) $error[]='Invalid amount in shopping cart! (amount='.$entry['amount'].'; min required='.$entry['amount_step'].')';
		if (($tmp=fmod($entry['amount'], $entry['amount_step']))!==0.0) $error[]='invalid amount in shopping cart! (amount='.$entry['amount'].'; step required='.$entry['amount_step'].'; modulus='.$tmp.')';
		if ($entry['product_table_stock_col']) {
			if (axs_db_query("SELECT `".$entry['product_table_stock_col']."` FROM `".$entry['product_table']."` WHERE `id`='".$entry['product_id']."'", 'cell', 1, __FILE__, __LINE__)<$entry['amount']) $error['stock']='Product out of stock!';
			}
		if (!empty($error)) {
			//dbg($entry);
			axs_log(__FILE__, __LINE__, __CLASS__, 'Error in e-shop cart: '.implode(', ', $error));
			}
		return $error;
		} #</cart_validate_item()>
	static function delivery_cat($sizes) {
		$out=array('cat'=>1, 'sum'=>0, 'sum.vat'=>0, 'sum+vat'=>0, );
		if (empty($sizes)) return $out;
		$out['cat']=0;
		#<Get cart products max size />
		$s=array('x'=>0, 'y'=>0, 'z'=>0, );
		$weight=0;
		foreach (self::cart_get() as $k=>$v) {
			foreach ($s as $kk=>$vv) if ($v['product_size_'.$kk]>$vv) $s[$kk]=floatval($v['product_size_'.$kk]);
			if ($v['product_weight']>$weight) $weight=floatval($v['product_weight']);
			}
		//dbg($s,self::cart_get());
		#if ((array_sum($s)<=0) && ($weight<=0)) return $out;
		#<Find size cat />
		//dbg($s,$weight);
		foreach ($sizes as $k=>$v) {
			$size_ok=$weight_ok=false;
			if (($v['x']+$v['y']+$v['z']==0) || ((array_sum($s)>0) && ($s['x']<=$v['x']) && ($s['y']<=$v['y']) && ($s['z']<=$v['z']))) $size_ok=true;
			if ((!$v['weight']) || (($weight>0) && ($weight<=$v['weight']))) $weight_ok=true;
			//dbg($k,$v);
			if (($size_ok) && ($weight_ok)) {
				$v['cat']=$k;
				$out=$v;
				break;
				}
			}
		return $out;
		} #</delivery_cat()>
	static function tax($num, $percent, $format=false) {
		$num+=($num/100)*$percent;
		if ($format) $num=number_format($num, 2 ,'.', ' ');
		return $num;
		}
	}#</class::axs_eshop>
#2008-07-02 ?>