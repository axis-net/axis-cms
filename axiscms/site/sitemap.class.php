<?php #2014-12-10
class axs_sitemap {
	var $vr=array(), $tpl=array();
	function menu_html($dir, $path=false) {
		global $axs;
		$html='';
		$index=axs_content::menu_get($dir, $path);
		if (empty($index)) return '';
		foreach ($index as $k=>$cl) {
			if (!axs_content::published($cl, $axs['time'])) continue;
			if (!$cl['url']) $cl['url']=axs_content::url($axs['url'], array('c'=>$cl['c']));
			$cl['pic_if']=axs_content::menu_pic_get($cl['path'], '_if');
			$cl['pic_if']=($cl['pic_if']) ? axs_admin::media_html(axs_dir('content', 'http').$cl['pic_if'], $cl['title']):'';
			$cl['submenu']=($cl['plugin']==='menu') ? $this->menu_html($dir.$cl['c'].'/', $cl['path']):'';
			foreach ($cl as $kk=>$vv) if (is_array($vv)) unset($cl[$kk]);
			$html.=axs_tpl_parse($this->tpl['ul.li'], $cl);
			}
		return axs_tpl_parse($this->tpl['ul'], array('submenu'=>$html, ));
		} #</menu_html()>
	} #</axs_sitemap>
#2014-12-10 ?>