<?php #2019-10-10
$plugin_def=array(); # plugin default data
$plugin_def['name']['et']='E-pood > Ostukorv';
$plugin_def['name']['en']='E-shop > Cart';
$plugin_def['title']['']='';
$plugin_def['help']['']='';
$plugin_def['mkdir']='';
$plugin_def['content']['']='<?php #|'.$axs['time'].',,'.axs_get('user', $axs_user).',,||?>'."\n";
$plugin_def['tr']['et']=array(
	'calc.lbl'=>'arvuta',
	'nr.txt'=>'Number',
	'nr.abbr'=>'Nr',
	# menu
	'menu.cart'=>'Ostukorv',
	'menu.checkout'=>'Vormista tellimus',
	'menu.orders'=>'Sinu tellimused',
	# cart
	'cart.txt'=>'Ostukorv',
	'cart.items.txt'=>'toode(t)',
	'cart.pic.lbl'=>'Pilt',
	'cart.product.lbl'=>'Toote nimetus',
	'cart.price.lbl'=>'Hind',
	'cart.amount.lbl'=>'Kogus',
	'cart.unit.lbl'=>'Ühik',
	'cart.vat.lbl'=>'Käibemaks',
	'cart.vat.abbr'=>'KM',
	'cart.sum.lbl'=>'Summa',
	'cart.edit.lbl'=>'Vaata / muuda',
	'cart.del.lbl'=>'Kustuta',
	'cart.sum.txt'=>'Summa käibemaksuta',
	'cart.sum.vat.txt'=>'Käibemaks',
	'cart.sum+vat.txt'=>'Kokku',
	'cart.checkout.lbl'=>'Maksma',
	'cart.open_orders.txt'=>'Sul on tasumata tellimusi!',
	'cart.back.lbl'=>'Tagasi tootekataloogi',
	# checkout
	'checkout.delivery.lbl'=>'Transpordi valik',
	'checkout.delivery.cost.lbl'=>'maksumus',
	'checkout.delivery.choice.lbl'=>'valikud',
	'checkout.client.lbl'=>'Tellija andmed',
	'checkout.client_name.lbl'=>'Tellija nimi',
	'checkout.client_address.lbl'=>'Aadress',
	'checkout.client_postcode.lbl'=>'Postiindeks',
	'checkout.client_phone.lbl'=>'Telefon',
	'checkout.client_email.lbl'=>'E-post',
	'checkout.terms.lbl'=>'tellimistingimustega',
	'checkout.terms-agree.lbl'=>'Kinnitan, et nõustun',
	'checkout.payment.lbl'=>'Maksumus',
	'checkout.sum.cart.txt'=>'Toodete maksumus',
	'checkout.sum.delivery.txt'=>'Transpordi maksumus',
	'checkout.sum.txt'=>'Summa kokku',
	'checkout.payment.txt'=>'Makseviisid',
	'checkout.order.lbl'=>'Maksma',
	'checkout.notify_new_order.txt'=>'sisestati uus tellimus',
	# orders view & PDF
	'orders.order_id.lbl'=>'Tellimus',
	'orders.order_nr.lbl'=>'Tellimuse number',
	'orders.search.lbl'=>'Otsi',
	'orders.time.lbl'=>'Aeg',
	'orders.payment_method.lbl'=>'Makseviis',
	'orders.revenue.lbl'=>'Laekunud',
	'orders.invoice_id.lbl'=>'Arve',
	'orders.status.lbl'=>'Staatus',
	'orders.status.-.lbl'=>'-',
	'orders.status.accept.lbl'=>'töös',
	'orders.status.problem.lbl'=>'probleem',
	'orders.status.complete.lbl'=>'valmis',
	'orders.status.cancel.lbl'=>'tühistatud',
	'orders.status.draft.lbl'=>'mustand',
	
	'orders.data.txt'=>'Tellimuse andmed',
	'orders.content.lbl'=>'Tellimuse sisu',
	'orders.price.lbl'=>'Hind',
	'orders.amount.lbl'=>'Kogus',
	'orders.amount_unit.lbl'=>'Ühik',
	'orders.vat.lbl'=>'Käibemaks',
	'orders.vat.abbr'=>'KM%',
	'orders.sum.lbl'=>'Summa',
	
	'orders.sum.txt'=>'Summa ilma käibemaksuta',
	'orders.sum.vat.txt'=>'Käibemaksu summa',
	'orders.sum+vat.txt'=>'Summa kokku',
	'orders.ref.txt'=>'Hiljem saate soovi korral tellimuse juurde tagasi pöörduda kasutades tellimuse numbrit.',
	'orders.tools.order_save.lbl'=>'salvesta tellimuse kinnitus PDF',
	'orders.tools.order_mail.lbl'=>'saada tellimuse kinnitus e-postiga',
	'orders.tools.order_del.lbl'=>'tühista tellimus',
	'orders.tools.invoice_save.lbl'=>'salvesta arve PDF',
	'orders.tools.invoice_mail.lbl'=>'saada arve e-postiga',
	# Orders payment options
	'orders.payment.lbl'=>'Vali makseviis',
	'orders.pay.lbl'=>'Alusta maksmist',
	'orders.paid.txt'=>'Tellimus on tasutud.',
	# error messages
	'msg_cart_empty'=>'Ostukorv on tühi!',
	'msg_cart_error'=>'Viga tellimuses:',
	'msg_payment'=>'Palun valige makseviis!',
	'msg_tems-agree'=>'Jätkamiseks peate nõustuma tellimistingimustega!',
	'msg_value_required'=>'Palun täitke',
	'msg_order_not_found'=>'Sellise numbriga tellimust ei leitud.',
	'msg_order_sent'=>'Tellimus on saadetud tellija e-posti aadressile.',
	'msg_invoice_sent'=>'Arve on saadetud tellija e-posti aadressile.',
	'msg_banklink_ok'=>'Tehing on kinnitatud.',
	'msg_banklink_fail'=>'Tehingut ei toimunud.',
	'msg_banklink_tech'=>'Tehingu kinnitamisel tekkis tehniline probleem.',
	);

if (defined('AXS_SITE_NR')) {
require_once('eshop.class.php');
axs_eshop::cart_init();
if (isset($_POST['cart_amount'])) axs_eshop::cart_edit_amount($_POST['cart_amount'],  __FILE__, __LINE__); #<Edit amount of items />
if (isset($_POST['cart_del'])) axs_eshop::cart_del($_POST['cart_del']); #<Delete items from shopping cart />
axs_eshop::$tr=axs_content::tr_get($plugin_def, __FILE__, __LINE__);
if ($axs_local['section']!=='content') return axs_eshop::cart_banner($axs_local);

require_once($axs_local['plugin'].'.plugin.class.php');
$o=new axs_eshop_plugin($axs_local);

if ($o->action==='terms') return include('deflt.plugin.php');
if ($o->action==='product-img') $o->product_img($_GET['img']); #<display product image />

if ($o->action==='bank-return') { # read bank reply
	$error=array();
	if (!axs_eshop::$banklink) axs_eshop::$banklink=new axs_banklink($axs['l'], axs_eshop::$site_nr);
	$reply=axs_eshop::$banklink->reply_get($_GET['bank-return']);
	$order=axs_business_orders::nr_get($reply['order_nr']);
	if (!$order['id']) $error[]='Invalid order_id="'.$order['id'].'"';
	if (($reply['authentic']) && ($reply['status']==='ok')) $o->invoice_create($order, $reply, $error); # compile invoice and mail it
	# redir back to order with msg
	axs_redir('?'.axs_url($o->url, array('order_nr'=>$reply['order_nr'], 'msg'=>'banklink_'.$reply['status'], 'rand'=>uniqid('')), false));
	# log
	if (!empty($error)) axs_log(__FILE__, __LINE__, 'eshop', $error);
	axs_exit();
	} #</bank-reply>

$o->vr['back']=(!empty($_REQUEST['b'])) ? '<a id="back" href="'.htmlspecialchars($_REQUEST['b']).'">'.axs_eshop::$tr->s('cart.back.lbl').'</a>':'';
# Menu
$tpl=axs_tpl(false, 'eshop.menu.tpl');
foreach (array('cart'=>array(), 'checkout'=>array('action'=>'checkout'), 'orders'=>array('order_nr'=>''), ) as $k=>$v) {
	$v=array('url'=>'?'.axs_url($o->url, $v), 'label'=>axs_eshop::$tr->s('menu.'.$k), 'act'=>($k===$o->action) ? 'current':'', );
	$o->vr['menu'].=axs_tpl_parse($tpl, $v);	
	}

$form=new axs_form_edit(array('site_nr'=>axs_eshop::$site_nr, 'user_input'=>$_POST, ));

#<Shopping cart>
if ($o->action==='cart') {
	if (isset($_POST['checkout'])) { #<Proceed to checkout>
		axs_eshop::cart_validate($form->msg);
		if (empty($form->msg)) axs_exit(axs_redir('?'.axs_url($o->url, array('action'=>'checkout'), false)));
		} #</Proceed to checkout>
	$cl=array('form.action'=>$axs['http_root'].'?'.axs_url($o->url), 'cart'=>'', 'count'=>count($_SESSION['eshop']['cart']), );
	if (!empty($_SESSION['eshop']['orders'])) {
		$tmp=array('orders'=>'', );
		$tpl=axs_tpl(false, 'eshop.cart.orders.item.tpl');
		foreach ($_SESSION['eshop']['orders'] as $v) {
			$tmp['orders'].=axs_tpl_parse($tpl, $v+array('url'=>'?'.axs_url($o->url, array('order_nr'=>$v['order_nr'])), 'time'=>''));
			}
		$o->vr['orders']=axs_tpl_parse(axs_tpl(false, 'eshop.cart.orders.tpl'), $tmp);
		}
	$tpl=array('cart'=>axs_tpl(false, 'eshop.cart.tpl'), 'cart.item'=>axs_tpl(false, 'eshop.cart.item.tpl'), );
	$nr=1;
	$cl['cart']='';
	foreach ($_SESSION['eshop']['cart'] as $k=>$v) {
		$v['nr']=$nr++;
		axs_eshop::cart_item_format($k, $v);
		$cl['cart'].=axs_tpl_parse($tpl['cart.item'], $v);
		}
	foreach (axs_eshop::cart_sum() as $k=>$v) $cl[$k]=number_format($v, 2, '.', ' ');
	foreach (array('currency', 'currency.symbol') as $v) $cl[$v]=htmlspecialchars(axs_eshop::$cfg[$v]);
	$cl['msg']=(!empty($form->msg)) ? $form->msg_html($form->msg):'';
	$o->vr['content']=axs_tpl_parse($tpl['cart'], $cl);
	} #</Shopping cart>

if ($o->action==='checkout') {
	$o->url['action']=$o->action;
	$form->structure_set(axs_business_orders::$client_fields);
	$tpl=array('checkout'=>axs_tpl(false, 'eshop.checkout.tpl'), );
	$cl=array(
		'form.action'=>$axs['http_root'].'?'.axs_url($o->url), 'delivery'=>'', 'client_data'=>'', 'payment'=>'', 'terms.url'=>'?'.axs_url($o->url, array('action'=>'terms')), 
		);
	
	$client=array('id'=>0, );
	if (AXS_LOGINCHK===1) {
		$sql=axs_db_query("SELECT *, CONCAT(`fstname`, ' ', `lstname`) AS `name`\n".
		"	FROM `axs_users` WHERE `id`='".$axs_user['id']."' AND `user`='".$axs_user['user']."'", 'row', 1, __FILE__, __LINE__);
		$client['id']=intval(axs_get('id', $sql));
		if (strpos(axs_get('user', $sql), '@')) $sql['email']=$sql['user'];
		}
	foreach (array('delivery'=>'', 'delivery_choice'=>array(), 'terms-agree'=>'', )+axs_business_orders::$client_fields as $k=>$v) {
		$client[$k]=axs_get($k, $_SESSION['eshop']['client'], '');
		if (isset($_POST[$k])) $client[$k]=$_SESSION['eshop']['client'][$k]=$_POST[$k];
		if (!is_array($client[$k])) {
			if ((!strlen($client[$k])) && (isset($sql[$k]))) $client[$k]=$sql[$k];
			if (strpos($client[$k], ' ')!==false) $client[$k]=preg_replace('/ +/', ' ', trim($client[$k]));
			}
		}
	foreach ($o->delivery_options($client) as $k=>$v) {
		foreach ($v['options'] as $kk=>$vv) {
			$choice_class='';
			if (is_array($vv['choice'])) {
				if (empty($vv['choice'])) $vv['choice']='<input name="delivery'.$kk.'-choice" class="choice" type="submit" value="'.axs_eshop::$tr->s('checkout.delivery.choice.lbl').'" />';
				else {
					$options=array('				<option value="">-</option>', );
					foreach ($vv['choice'] as $kkk=>$vvv) {
						if (isset($vvv['sub'])) {
							$options[]='				<optgroup label="'.axs_html_safe($vvv['label']).'">'."\n";
							foreach ($vvv['sub'] as $kkkk=>$vvvv) $options[]='					<option value="'.$vvvv['local_id'].'"'.$vvvv['selected'].'>'.axs_html_safe($vvvv['label']).'</option>'."\n";
							$options[]='				</optgroup>'."\n";
							}
						else $options[]='				<option value="'.$vvv['local_id'].'"'.$vvv['selected'].'>'.axs_html_safe($vvv['label']).'</option>'."\n";
						}
					$vv['choice']=
					'			<label><span>'.axs_eshop::$tr->s('checkout.delivery.choice.lbl').'</span><select name="delivery_choice['.$vv['id'].']" required="required">'."\n".
					implode("\n", $options)."\n".
					'			</select></label>';
					$choice_class='choice';
					}
				}
			$v['options'][$kk]='			<div id="delivery'.$kk.'-container"><label><input id="delivery'.$kk.'" name="delivery" type="radio" value="'.$vv['id'].'" class="'.$choice_class.'"'.$vv['selected'].$vv['disabled'].' data-cat="'.$vv['cat'].'" /> '.htmlspecialchars($vv['label']).' ('.axs_eshop::$tr->s('checkout.delivery.cost.lbl').': '.$vv['sum.fmt'].')</label>'.$vv['choice'].'</div>'."\n";
			}
		$cl['delivery'].=
		'		<fieldset class="'.$k.'">'."\n".
		'			<legend>'.axs_html_safe($v['label']).'</legend>'."\n".
		implode('', $v['options']).
		'		</fieldset>'."\n";
		}
	
	$form->vl=$client;
	foreach ($form->structure_get() as $k=>$v) {
		$form->structure[$k]['label.html']=$v['label.html']=axs_eshop::$tr->s('checkout.client_'.$k.'.lbl');
		$cl['client_data'].=$form->element_html($k, $v, $form->vl);
		}
	$cl['terms-agree']=($client['terms-agree']) ? ' checked="checked"':'';
	foreach (axs_eshop::cart_sum() as $k=>$v) $cl['cart.'.$k]=$v;
	$delivery=$o->delivery_option_get($client['delivery']);
		
	$cl['delivery.vat']=($delivery['vat']==0) ? '-':$delivery['vat'].'%';
	$cl['sum.delivery']=$delivery['sum'];
	$cl['sum.delivery+vat']=$delivery['sum+vat'];
	$cl['sum.delivery.vat']=$delivery['sum.vat'];
	//$cl['delivery.vat']=($cl['delivery.vat']==0) ? '-':$cl['delivery.vat'].'%';
	
	if (!axs_eshop::$banklink) axs_eshop::$banklink=new axs_banklink($axs['l'], axs_eshop::$site_nr);
	axs_eshop::$banklink->list_get();
	
	if (isset($_POST['order_post'])) { #<order post>
		#<validate order>
		axs_eshop::cart_validate($form->msg);
		if ((!$client['delivery']) or (
			(($client['delivery']) && ($delivery['function']) && (!axs_get($client['delivery'], $client['delivery_choice'])))
			)) $form->msg['delivery']='"'.axs_eshop::$tr->s('checkout.delivery.lbl').'": '.axs_eshop::$tr->s('msg_value_required').'!';
		if (!$client['terms-agree']) $form->msg['terms-agree']=axs_eshop::$tr->s('msg_terms-agree');
		$payment=axs_get('payment', $_POST);
		if (!isset(axs_eshop::$banklink->list[$payment])) $form->msg['payment']=axs_eshop::$tr->s('msg_payment');
		$form->validate($form->vl);
		#</validate order>
		if (empty($form->msg)) { # <save order>
			$axs_order=new axs_business_orders(axs_eshop::$site_nr, $axs['l'], axs_eshop::$tr->filter(array('checkout.', 'orders.')));
			$qry=array();
			foreach (axs_business_orders::$client_fields as $k=>$v) $qry[$k]="`client_".$k."`='".addslashes($client[$k])."'";
			$cl['order_id']=axs_db_query("INSERT INTO `".axs_eshop::$px."business_orders`\n".
			"	SET time='".$axs['time']."', `profile_id`='".axs_eshop::$cfg['profile_id']."', `client_id`='".$client['id']."', `status`='-', `client_ip`='".axs_business_orders::ip_to_number()."',\n".
			implode(', ', $qry).",\n".
			"	`currency`='".axs_eshop::$cfg['currency']."'", 'insert_id', axs_eshop::$db, __FILE__, __LINE__);
			# <order rows>
			# <add delivery cost to cart>
			$cart=$_SESSION['eshop']['cart'];
			$cart[]=array(
				'product_id'=>$client['delivery'], 'product_form'=>axs_eshop::$name,
				'label'=>axs_eshop::$tr->t('checkout.delivery.lbl').': '.$delivery['label'].' '.$delivery['choice']['label'].(($delivery['cat'])?' ('.$delivery['cat'].')':''),
				'price'=>$delivery['sum'],
				'vat'=>$delivery['vat'],
				'amount'=>1,
				'amount_unit_type'=>$delivery['unit_type'],
				);
			# <order header>
			$fs=new axs_filesystem_img('', axs_eshop::$site_nr);
			$nr=1;
			foreach ($cart as $k=>$v) {
				$id=axs_db_query("INSERT INTO `".axs_eshop::$px."business_orders_table`\n".
				"	SET `nr`='".$nr."', `_parent_id`='".intval($cl['order_id'])."', `product_id`='".($v['product_id']+0)."',\n".
				"	`product_form`='".addslashes($v['product_form'])."', `text`='".addslashes($v['label'])."', `price`='".($v['price']+0)."',\n".
				"	`vat`='".intval($v['vat'])."', `amount`='".($v['amount']+0)."', `amount_unit`='".($v['amount_unit_type']+0)."'",
				'insert_id', axs_eshop::$db, __FILE__, __LINE__);
				if ((!empty($v['product_img_file'])) && (file_exists($v['product_img_file']))) $fs->img_proc(
					$v['product_img_file'], axs_dir('content').axs_business_orders::$d.$cl['order_id'].'-'.$id.'.jpg', axs_business_orders::$img
					);
				$nr++;
				}
			$order_data=axs_business_orders::order_get(axs_eshop::$site_nr, $axs['l'], $cl['order_id'], $axs['time'], $axs_order->tr->get(false));
			# <Empty cart and set up open orders to session>
			$_SESSION['eshop']['cart']=array();
			$_SESSION['eshop']['delivery']='';
			$_SESSION['eshop']['orders'][$cl['order_id']]=array('order_nr'=>$order_data['header']['order_nr']);
			# notify about new order
			$mail=(axs_eshop::$cfg['notify_email_order']) ? array(axs_eshop::$cfg['notify_email_order']):array();
			$mail[]=$order_data['header']['client_email'];
			foreach ($mail as $v) $o->mail($axs_order, $order_data, $v, axs_eshop::$tr->t('checkout.notify_new_order.txt'), axs_eshop::$tr->t('checkout.notify_new_order.txt')."\n");
			# <Create invoice if bank='-' />
			if (axs_eshop::$banklink->list[$payment]['bank']==='-') $o->invoice_create(array('id'=>$cl['order_id'], 'time'=>$axs['time']), array('bank_id'=>axs_eshop::$banklink->list[$payment]['id'], 'transaction_id'=>'', 'sum'=>0, ), $form->msg);
			axs_exit(axs_redir('?'.axs_url($o->url, array('order_nr'=>$order_data['header']['order_nr'], 'b'=>false, 'rand'=>uniqid('')), false)));
			} #</save order>
		} #</order post>
	foreach (array('', '.vat', '+vat') as $v) $cl['sum'.$v]=$cl['cart.sum'.$v]+$cl['sum.delivery'.$v];
	foreach (array('cart.sum', 'cart.sum.vat', 'cart.sum+vat', 'sum.delivery', 'sum.delivery.vat', 'sum.delivery+vat', 'sum', 'sum.vat','sum+vat') as $v) $cl[$v]=number_format($cl[$v], 2, '.', ' ');
	foreach (array('currency', 'currency.symbol') as $v) $cl[$v]=htmlspecialchars(axs_eshop::$cfg[$v]);
	# <Show payment options>
	if (axs_tpl_has($tpl['checkout'], 'payment')) {
		$tpl['bank']=axs_tpl(false, 'eshop.cart.bank.tpl');
		$cl['payment']='';
		foreach (axs_eshop::$banklink->list as $k=>$v) {
			foreach ($v as $kk=>$vv) if (!is_scalar($vv)) unset($v[$kk]);
			$v['checked']=(axs_get('payment', $_POST)===$v['bank']) ? ' checked="checked"':'';
			$v['label']=(!empty($v['img.pay'])) ? '<img src="'.$v['img.pay'].'" alt="'.htmlspecialchars($v['comment']).'" />':htmlspecialchars($v['comment']);
			$cl['payment'].=axs_tpl_parse($tpl['bank'], $v);
			}
		# </show payment options>
		}
	$cl['msg']=(!empty($form->msg)) ? $form->msg_html($form->msg):'';
	$o->vr['content']=axs_tpl_parse($tpl['checkout'], $cl);
	} #</checkout>

if ($o->action==='orders') { # Show orders
	$order_nr=trim(axs_get('order_nr', $_REQUEST));
	$cl=array('order_nr'=>htmlspecialchars($order_nr), 'orders'=>'', 'url'=>'', );
	foreach ($axs['url'] as $k=>$v) $cl['url'].='			<input name="'.$k.'" value="'.$v.'" type="hidden" />'."\n";
	# Show orders list only if logged in and no order nr submitted
	if ((AXS_LOGINCHK===1) && (!$order_nr)) {
		$tpl=axs_tpl(false, 'eshop.orders.tr.tpl');
		$result=axs_db_query("SELECT `id`, `time`, `status`, `invoice_id`, `revenue` FROM `".axs_eshop::$px."business_orders` WHERE client_id='".$axs_user['id']."'\n".
		"	ORDER BY time DESC", 1, axs_eshop::$site_nr, __FILE__, __LINE__);
		foreach ($result as $v) {
			$v['order_nr']=axs_business_orders::nr_make($v['id'], $v['time']);
			$v['time']=date('d.m.Y H:i', $v['time']);
			$v['revenue.fmt']=($v['revenue']!=0) ? number_format($v['revenue'], 2, '.', ' '):'-';
			$v['url']='?'.axs_url($o->url, array('order_nr'=>$v['order_nr']));
			$v['status']=axs_eshop::$tr->s('orders.status.'.$v['status'].'.lbl');
			$cl['orders'].=axs_tpl_parse($tpl, $v);
			}	
		$o->vr['content']=axs_tpl_parse(axs_tpl(false, 'eshop.orders.tpl'), $cl);
		} # END Show orders list
	#</order_list>
	else { # Search order by order_nr
		$axs_order=new axs_business_orders(axs_eshop::$site_nr, $axs['l']);//, axs_eshop::$tr->filter(array('checkout.', 'orders.')));
		$ordr=axs_business_orders::nr_get($order_nr);
		$order_data=$axs_order->order_get(axs_eshop::$site_nr, $axs['l'], $ordr['id'], $ordr['time']);
		$permission_admin=axs_user::permission_get('admin');
		$permission_user=((AXS_LOGINCHK===1) && (($order_data['header']['client_id']==$axs_user['id']) || $permission_admin)) ? true:false;
		# show order data if order nr found
		if ((!empty($order_data['header']['id'])) && ($ordr['id']) && ($ordr['time'])) {
			$cl['order_url']='?'.axs_url($o->url, array('order_nr'=>$order_nr));
			foreach (array('date','status.txt','currency','sum.fmt','sum.vat.fmt','sum+vat.fmt') as $k=>$v) $cl[$v]=htmlspecialchars($order_data['header'][$v]);
			foreach (axs_business_orders::$client_fields as $k=>$v) $client[$k]=$order_data['header']['client_'.$k];
			$cl['rows']='';
			$_SESSION['eshop']['order_files']=array(); # specify product images to show
			$tpl=axs_tpl(false, 'eshop.orders.view.item.tpl');
			foreach ($order_data['rows'] as $v) {
				$v['product_img_url']='';
				if (file_exists($f=axs_dir('content').axs_business_orders::$d.$order_data['header']['id'].'-'.$v['id'].'.jpg')) {
					if (AXS_LOGINCHK===1) {
						$_SESSION['eshop']['order_files'][$v['id']]=$f;
						$v['product_img_url']='?'.axs_url($o->url, array('action'=>'img', 'order_nr'=>false, 'img'=>$v['id']));
						$alt=$order_data['header']['id'].'-'.$v['id'].'.jpg';
						}
					else {
						$v['product_img_url']=$axs['dir_cms'].'gfx/icon.login.png';
						$alt='LogIn';
						}
					$v['product_img_url']='<img src="'.$axs['http_root'].$v['product_img_url'].'" alt="'.$alt.'" title="'.$alt.'" />';
					$v['text']=nl2br($v['text']);
					}
				$cl['rows'].=axs_tpl_parse($tpl, $v);
				}
			unset($result, $v);
			#<Save and mail>
			$cl['url_formact']='?'.axs_url($o->url, array('order_nr'=>$order_data['header']['order_nr']));
			$cl['tools']=array();
			$mail=false;
			if ($order_data['header']['invoice_id']) { #<Save and mail invoice>
				if (isset($_POST['invoice'])) {
					$i=new axs_business_invoices(axs_eshop::$site_nr, $axs['l']);
					$i_data=$i->invoice_get($order_data['header']['invoice_id']);
					}
				if ($permission_user) {
					$cl['tools']['invoice_save']='invoice[save]';
					if (isset($_POST['invoice']['save'])) $i->export_pdf($order_data['header']['invoice_id'], true);
					}
				$cl['tools']['invoice_mail']='invoice[mail]';
				if (isset($_POST['invoice']['mail'])) $mail=$o->mail($i, $i_data, $order_data['header']['client_email'], axs_eshop::$tr->t('orders.invoice_id.lbl'));
				} #</If invoice exists>
			#</Save and mail invoice>
			
			#<Save and mail order>
			if ($permission_user) {
				$cl['tools']['order_save']='order[save]';
				if (isset($_POST['order']['save'])) axs_business_orders::order_output($order_data, 'pdf', true);
				}
			$cl['tools']['order_mail']='order[mail]';
			if (isset($_POST['order']['mail'])) $mail=$o->mail($axs_order, $order_data, $order_data['header']['client_email'], axs_eshop::$tr->t('orders.order_id.lbl'));
			#</Save and mail order>
			
			if (!empty($mail)) axs_exit(axs_redir('?'.axs_url($o->url, array('order_nr'=>$order_data['header']['order_nr'], 'msg'=>$mail.'_sent'), false)));
			
			#<Delete order />
			if ((!$order_data['header']['status']) && (!$order_data['header']['invoice_id'])) {
				$cl['tools'][]='order_del';
				if (isset($_POST['order_del'])) $axs_order->del($order_data['id']);
				}
			
			foreach ($cl['tools'] as $k=>$v) $cl['tools'][$k]='		<li><input id="'.$k.'" name="'.$v.'" type="submit" value="'.axs_eshop::$tr->s('orders.tools.'.$k.'.lbl').'" /></li>'."\n";
			$cl['tools']=implode(' ', $cl['tools']);
			
			# show customer data only proper permission
			$cl['client_data']='';
			foreach (axs_business_orders::$client_fields as $k=>$v) {
				$v=($permission_user) ? $client[$k]:'***';
				$cl['client_data'].='<dt>'.axs_eshop::$tr->s('checkout.client_'.$k.'.lbl').'</dt><dd>'.htmlspecialchars($v).'</dd>'."\n";
				}
			
			# <show payment options>
			$cl['orders.payment.status']='';
			if ($order_data['header']['revenue']>=$order_data['header']['sum+vat']) $cl['orders.payment.status']=axs_eshop::$tr->s('orders.paid.txt'); # If already paid
			$cl['payment']='';
			if (!axs_eshop::$banklink) axs_eshop::$banklink=new axs_banklink($axs['l'], axs_eshop::$site_nr);
			if ((!$order_data['header']['payment_method']) && (!empty($_REQUEST['bank']))) {
				$tmp=$axs['http'].'://'.$_SERVER['SERVER_NAME'].axs_dir('site', 'http').'?'.axs_url($o->url, array('order_nr'=>$order_data['header']['order_nr'], 'bank-return'=>$_REQUEST['bank'], ), false);
				axs_exit(axs_eshop::$banklink->form_html($_REQUEST['bank'], 'pay', array(
					'order_nr'=>$order_data['header']['order_nr'],
					'sum'=>$order_data['header']['sum.total'],
					'currency'=>$order_data['header']['currency'],
					'msg'=>$order_data['header']['order_nr'],
					'lang'=>$axs['l'],
					'return'=>$tmp,
					'cancel'=>$tmp,
					)));
				}
			$tmp='?'.axs_url($o->url, array('order_nr'=>$cl['order_nr'], ));
			$tpl=axs_tpl(false, 'eshop.banklink.tpl');
			foreach (axs_eshop::$banklink->list_get() as $k=>$v) {
				$v['class'].=' '.$v['protocol'];
				if ($v['img.pay']) $v['class'].=' img';
				$v['disabled']='';
				if ($order_data['header']['payment_method']===$v['id']) {	$v['class'][]='disabled';	$v['disabled']=' disabled="disabled"';	}
				$v['button']=($v['img.pay']) ? '<img src="'.$v['img.pay'].'" alt="'.htmlspecialchars($v['title']).'" />':htmlspecialchars($v['title']);
				$cl['payment'].=axs_tpl_parse($tpl, $v+array('form_act'=>$tmp, 'button_lbl'=>axs_eshop::$tr->s('orders.pay.lbl'), ));
				}
			# </show payment options>
			
			$o->vr['content']=axs_tpl_parse(axs_tpl(false, 'eshop.orders.view.tpl'), $cl);
			} # END show order data if order nr found
		else {
			if ($order_nr) {
				unset($_SESSION['eshop']['orders'][$ordr['id']]);
				$form->msg['msg_order_not_found']=axs_eshop::$tr->s('msg_order_not_found');
				}
			$o->vr['content']=axs_tpl_parse(axs_tpl(false, 'eshop.orders.tpl'), $cl);
			} # END Search order by order_nr
		unset($o->vr['msg_order_not_found']);
		}
	//dbg($order_data['header']['id'], $ordr['id'], $ordr['time'], $ordr['token']);
	if (!empty($_GET['msg'])) $form->msg[$_GET['msg']]=axs_eshop::$tr->s('msg_'.$_GET['msg']);
	$o->vr['msg']=(!empty($form->msg)) ? $form->msg_html($form->msg):'';
	} #</orders>

return axs_tpl_parse(axs_tpl(false, 'eshop.tpl'), $o->vr);
}
# ------------------- if bank reply -------------------
/*if (!defined('AXS_SITE_ROOT')) {
	define('AXS_BANKLINK_VERIFY', true);
	$_GET['c']=$plugin_def['config']['cid'];
	$_SERVER['REQUEST_URI'].='?c='.$plugin_def['config']['cid'];
	include('index.php');
	}*/
#cart rewrite class(cart lubab tellida tooteid, mis on vahepeal kustutatud, user order_del kui kinnitamata), 
#2008-07-02 ?>