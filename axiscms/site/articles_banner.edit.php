<?php #2019-04-22
defined('AXS_PATH_CMS') or exit(require('../index.php')); #<Prevent direct access />

#<Class "axs_articles_base_edit" extends "axs_articles_news" through "axs_articles" />
include_once('articles.base.class.php');
class axs_articles extends axs_articles_base {}
include_once('articles.base.edit.class.php');

#<Customized editor class>
class axs_articles_edit extends axs_articles_base_edit {}
#</Customized editor class>

$o=new axs_articles_edit($this);
return $o->edit_banner();
#2014-02 ?>