<?php #2016-01-27
$plugin_def=array(); # plugin default data
$plugin_def['name']['et']='üritused > kalender';
$plugin_def['help']['']='';
$plugin_def['mkdir']='files';
$plugin_def['tr']['et']=array(
	'id.lbl'=>'ID',
	'title.lbl'=>'pealkiri',
	'promote.lbl'=>'näita bänneris',
	'hide_home.lbl'=>'ära näita avalehel',
	'publ.lbl'=>'avaldatud',
	'times.lbl'=>'toimumise ajad',
	'times.date.lbl'=>'kuupäev',
	'times.time.lbl'=>'kellaaeg',
	'date.lbl'=>'kuupäev',
	'time.lbl'=>'kellaaeg',
	'file.lbl'=>'pildifail',
	'summary.lbl'=>'sissejuhatus',
	'text.lbl'=>'sisutekst',
	'seo.lbl'=>'SEO',
	'title_alias.lbl'=>'URL alias',
	'meta_description.lbl'=>'<meta description>',
	'comments.lbl'=>'luba kommentaarid',
	'comments_mail.lbl'=>'saada kommentaarid e-postiga',
	'updated.lbl'=>'muudetud',
	'save.lbl'=>'salvesta',
	
	'show_all_lbl'=>'näita kõiki üritusi',
	);
if (!defined('AXS_SITE_NR')) $plugin_def['tr']['et']=array_merge(
	$plugin_def['tr']['et'],
	(isset(axs_calendar::$tr[$axs['l']])) ? axs_calendar::$tr[$axs['l']]:current(axs_calendar::$tr)
	);
	
if (defined('AXS_SITE_NR')) {
require_once('articles_events.class.php');
$data=new axs_articles_events($axs_local);
if ($tmp=$data->error()) return $tmp;
$data->calendar_init();
if ($axs_local['section']!=='content') {
	$data->vr['url']=$data->url_root.axs_url($data->url, array('c'=>$axs_local['c'], ));
	$data->vr['c']=$axs_local['c'];
	//$data->vr['title']=axs_html_safe($axs_local['title']);
	$data->vr['_calendar']=$data->calendar_parse();
	return axs_tpl_parse(axs_tpl(false, $data->axs_local['plugin'].'.banner.tpl'), $data->vr);
	}
$data->page_init($plugin_def);
$data->vr['content']=include('deflt.plugin.php');
$data->vr['arh_link']='';
$data->vr['calendar']=$data->calendar_parse('content_'.$axs_local['c']);
return $data->page_parse();
}
#2011-09-21 ?>