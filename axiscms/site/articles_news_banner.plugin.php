<?php #2020-02-03 utf-8
$plugin_def=array(); # plugin default data
$plugin_def['name']['et']='Uudised > Bänner';
$plugin_def['name']['en']='News > Banner';
$plugin_def['title']['']='';
$plugin_def['help']['']='';
$plugin_def['mkdir']='';
$plugin_def['content']['']='<?php #|'.$axs['time'].',,'.axs_get('user', $axs_user).',,||?>'."\n";
require_once('articles_news.class.php');
$plugin_def['tr']=axs_articles_news::$tr;

if (defined('AXS_SITE_NR')) {
$data=new axs_articles_news($axs_local);
if (!$data->db) return '<p class="msg" lang="en">ERROR! This plugin requires SQL database.</p>';
$data->banner_init();
//$data->vr['url']=$data->url_root.axs_url($data->url, array('c'=>key(axs_content::index_c_find(false, $data->vr['plugin'])).''));
if (axs_tpl_has($data->tpl[''], 'content')) $data->vr['content']=include('deflt.plugin.php');
return $data->banner_parse();
}
#2017-10-22 ?>