<?php #03.03.2013 utf-8
return array(
	'form.title'=>'list subscribers',
	'fields_lbl'=>'form fields',
	'time_subscribe_lbl'=>'time subscribed',
	'disable_lbl'=>'unsubscribed',
	'config.list_name_lbl'=>'list name',
	'config.list_type_lbl'=>'list server type',
	'config.list_address_subscribe_lbl'=>'subscribe address',
	'config.list_address_unsubscribe_lbl'=>'unsubscribe address',
	);
#03.03.2013 ?>