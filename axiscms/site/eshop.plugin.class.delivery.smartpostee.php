<?php #2018-04-27
class axs_eshop_delivery_smartpostee {
	#http://uus.smartpost.ee/ariklient/ostukorvi-rippmenuu-lisamise-opetus
	public $url='http://smartpost.ee/places.xml';
	#public $url='http://smartpost.ee/fi_apt.xml';
	function __construct($parent_id) {
		global $axs;
		$this->parent_id=$parent_id;
		$this->get();
		} #</__construct()>
	function get() {
		$data=file_get_contents($this->url);
		$data=axs_eshop_plugin::xml2array($data, 1, "place");
		$this->table=array();
		foreach ($data['places_info']['place'] as $k=>$v) {
			if (!isset($this->table[$v['group_id']['value']])) $this->table[$v['group_id']['value']]=array('local_id'=>$v['group_id']['value'], 'group'=>0, 'label'=>$v['group_name']['value'], );
			$this->table[$v['place_id']['value']]=array('local_id'=>$v['place_id']['value'], 'group'=>$v['group_id']['value'], 'label'=>$v['name']['value'], );
			}
		} #</get()>
	}#</class::axs_eshop_delivery_smartpostee>
#2018-04-24 ?>