<?php #2014-01-08 utf-8
return array(
	'article.lbl'=>'article',
	'comments.lbl'=>'comments',
	'back.lbl'=>'back',
	'reset.lbl'=>'reset',
	'rank.up.lbl'=>'move up',
	'rank.down.lbl'=>'move down',
	
	'add.lbl'=>'add new',
	'add.article.lbl'=>'add new article',
	'add.batch.lbl'=>'batch add',
	'add.folder.lbl'=>'new folder',
	'add.confirm'=>'Add new?',
	'edit.lbl'=>'edit',
	'edit_article_head.lbl'=>'edit article header',
	'preview.lbl'=>'preview',
	'del.lbl'=>'delete',
	'del_confirm.lbl'=>'Delete article?',
	
	'header_edit.lbl'=>'edit structure',
	'search.lbl'=>'search',
	'search.found.lbl'=>'found',
	'search.order.lbl'=>'order',
	
	'input_file_del_lbl'=>'delete file',
	'msg_value_required'=>'Input required!',
	'msg_value_invalid'=>'Invalid input!',
	'msg_value_unique'=>'Unique value required!',
	);
#2009-02-26 ?>