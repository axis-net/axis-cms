<?xml version="1.0"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
 <channel>
  <atom:link href="{$url}" rel="self" type="application/rss+xml" />
  <title>{$title}</title>
  <link>{$home_url}</link>
  <description>{$description}</description>
  <language>{$l}</language>
  <lastBuildDate>{$date}</lastBuildDate>
  <generator>AXIS CMS</generator>
{$items} </channel>
</rss>