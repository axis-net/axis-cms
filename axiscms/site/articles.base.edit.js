//2016-07-01
axs.articles=function(id){
	this.id=id;
	this.select=function(){
		$('#'+axs.articles.edit.id+' input.select').each(function(){
			$('#id'+this.value+' input.delete').prop('disabled',($(this).is(':checked')) ? false:true);
			});
		var selected=$('#'+axs.articles.edit.id+' input.select:checked').length;
		if (!selected) $('#'+axs.articles.edit.id+' input.delete').prop('disabled',false);
		}//</select()>
	this.delete=function(){
		var selected=$('#'+axs.articles.edit.id+' input.select:checked').length;
		var current=$(this).closest('.item').find('input.select');
		var currentSelected=$(current).is(':checked');
		if (!selected){
			if (confirm(this.getAttribute('title'))){
				$(current).prop('checked',true);
				return true;
				}
			}
		if ((selected)&&(!currentSelected)) {
			console.log('Current not selected (selected:'+selected,'currentSelected:'+currentSelected+')');
			return false;
			}
		}//</delete()>
	$('#'+this.id+' input.select').click(this.select);
	//$('#'+this.id+' input.delete').prop('disabled',true);
	$('#'+this.id+' input.delete').click(this.delete);
	}//</class::axs.articles.edit>
//2012-01-25