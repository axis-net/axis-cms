<?php #2018-04-27
class axs_eshop_delivery_omnivaee {
	public $url='https://www.omniva.ee/locations.json';
	public $country='EE';
	public $type='0';
	function __construct($parent_id) {
		global $axs;
		$this->parent_id=$parent_id;
		$this->get();
		} #</__construct()>
	function get() {
		$data=file_get_contents($this->url);
		$data=json_decode($data, true);
		$data_old=axs_eshop_plugin::delivery_options_choice_query($this->parent_id, true);
		$this->table=array();
		foreach ($data as $k=>$v) if (($v['A0_NAME']===$this->country) && ($v['TYPE']===$this->type)) {
			if (!isset($this->table[$v['A1_NAME']])) {
				$group_id=(isset($data_old[$v['A1_NAME']])) ? $data_old[$v['A1_NAME']]['local_id']:'1'.$v['ZIP'];
				$this->table[$v['A1_NAME']]=array('local_id'=>$group_id, 'group'=>0, 'label'=>$v['A1_NAME'], );
				}
			$this->table[$v['ZIP']]=array('local_id'=>$v['ZIP'], 'group'=>$this->table[$v['A1_NAME']]['local_id'], 'label'=>$v['NAME'], );
			}
		} #</get()>
	}#</class::axs_eshop_delivery_omnivaee>
#2018-04-24 ?>