//2016-07-07
axs.eshop_products={
	init:function(id){
		//initiate the plugin and pass the id of the div containing gallery images
		$('#'+id+'-big').elevateZoom({gallery:id+'-list', cursor:'pointer', galleryActiveClass:'active', imageCrossfade:true, loadingIcon:'http://www.elevateweb.co.uk/spinner.gif'});
		//pass the images to Fancybox
		$('#'+id+'-big').bind("click", function(e) {  
			var ez =   $('#'+id+'-big').data('elevateZoom');	
			$.fancybox(ez.getGalleryList());
			return false;
			});
		}//</init()>
	}//</class::axs.eshop_products>
//2016-07-07