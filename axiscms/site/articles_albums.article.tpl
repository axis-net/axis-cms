		<article id="article" class="align-center {$_class}">
			<p class="prev-next">{$article.prev} {$article.next} <span class="count">({$nr}/{$article.total})</span> <a class="back" href="{$back}" rel="nofollow">{$back.lbl}</a></p>
			<h2 class="align-center">{$title}</h2>
			<div class="text">
				{$file.t2}
				{$text}
			</div>
			<time datetime="{$date.datetime}">{$date}</time>
		</article>