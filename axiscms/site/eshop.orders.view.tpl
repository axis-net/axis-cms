		{$msg}
		<div id="order-view">
			<form class="order-content" action="{$url_formact}" method="post" enctype="application/x-www-form-urlencoded">
				<h2 id="order">{$orders.data.txt}</h2>
				<p class="msg">{$orders.ref.txt}</p>
				<table>
					<caption><a href="{$order_url}">{$orders.order_nr.lbl}: <strong>{$order_nr}</strong></a></caption>
					<tfoot>
						<tr>
							<th scope="row" colspan="5" class="right">{$orders.sum.txt}</th>
							<td class="nowrap right">{$sum.fmt}{$currency.symbol}</td>
						</tr>
						<tr>
							<th scope="row" colspan="5" class="right">{$orders.sum.vat.txt}</th>
							<td class="nowrap right">{$sum.vat.fmt}{$currency.symbol}</td>
						</tr>
						<tr>
							<th scope="row" colspan="5" class="right">{$orders.sum+vat.txt}</th>
							<td class="nowrap right"><strong>{$sum+vat.fmt}</strong>{$currency.symbol}</td>
						</tr>
					</tfoot>
					<thead>
						<tr>
							<th><abbr title="{$nr.txt}">{$nr.abbr}</abbr></th>
							<th>{$orders.content.lbl}</th>
							<th>{$orders.price.lbl}</th>
							<th>{$orders.amount.lbl}</th>
							<th><abbr title="{$orders.vat.lbl}">{$orders.vat.abbr}</abbr></th>
							<th>{$orders.sum.lbl}</th>
						</tr>
					</thead>
					{$rows}
				</table>
				<dl class="client">
					<dt>{$orders.time.lbl}</dt><dd>{$date}</dd>
					<dt>{$orders.status.lbl}</dt><dd>{$status.txt}</dd>
{$client_data}
				</dl>
				<ul class="tools">
					{$tools}
				</ul>
			</form>
			
			<div class="payment">
				<h2>{$orders.payment.lbl}</h2>
				<p>{$orders.payment.status}</p>
				{$payment}
				<script>axs.eshop.payment_banklink("#order-view .payment");</script>
			</div>
		</div>