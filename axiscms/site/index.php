<?php #2021-11-30
defined('AXS_PATH_CMS') or exit();
if (!defined('AXS_START_TIME')) define('AXS_START_TIME', array_sum(explode(' ', microtime())));

#<Site structure />
if (!defined('AXS_SITE_NR')) define('AXS_SITE_NR', 1);
require(AXS_PATH_CMS.'lib/axs.php'); # <Get basic framework />
$axs['ob']=axs_ob_catch(); # <START output buffering for error logging />
if (($axs['cfg']['site'][AXS_SITE_NR]['timezone']) && ($axs['ini_set()'])) ini_set('date.timezone', $axs['cfg']['site'][$axs['site_nr']]['timezone']);

#<Frame variables />
$axs['page']=array(
	'charset'=>'', 'title_menu'=>'', 'title_site'=>'', 'description'=>'', 'head'=>array(), 'c'=>'', /*'l'=>'',*/
	'theme'=>'', 'code.head'=>'', 'code.bottom'=>'', 'class'=>array(), 'lang.current'=>'', 'lang.current.abbr'=>'', 'lang.list'=>array(), 'breadcrumb'=>'', 'content..title'=>'',
	'menu.pic.file'=>'', 'menu.pic.img'=>'', 'login'=>'', '/'=>'', '/cms'=>axs_dir('cms', 'http'),
	'menu'=>array(
		'menu1'=>array(
			'index'=>'menu1', 'level'=>2, 'max_level'=>0, 'tpl'=>'menu', 'type'=>null, 'date_format'=>'d.m.Y H:i', 'pic_set_on'=>false, 'pic'=>'',
			),
		/*'menu1sub'=>array(
			'index'=>'menu1', 'level'=>3, 'max_level'=>0, 'tpl'=>'menu1', 'type'=>false, 'date_format'=>'d.m.Y H:i', 'pic_set_on'=>false, 'pic'=>'', 
			),*/
		),
	'content'=>array('content'=>array(), ),
	);
$axs['page']['charset']=$axs['cfg']['charset'];
$axs['page']['/']=$axs['http_root'].$axs['site_dir'];
$axs['page']['menu']['prototype']=$axs['page']['menu']['menu1'];
$axs['menu_tpl']=array(); #<List menu templates />

if (isset($_POST['axs']['login'])) axs_user_login::login($_POST);
if (isset($_GET['axs']['login']['out'])) axs_user_login::logout($_GET['axs']['login']['out']);
if (isset($_GET['axs']['login']['keepalive'])) axs_user::keepalive();
if (isset($axs['get']['gw'])) require(AXS_PATH_CMS.'index.gateway.php');
if ((isset($_COOKIE[session_name()])) || (isset($_GET[session_name()]))) { #<Check user login if active session found />
	if (!isset($_SESSION)) session_start();
	if (!empty($_SESSION['axs_'.$axs['http_root']]['user'])) axs_user::init();
	}
else {	if (!empty($_COOKIE['axs_user_user'])) axs_user::init();	}
require_once('site.menu.php');

#<Site config />
$cfg=axs_content::site_cfg_get();
if ($axs['cfg']['site'][AXS_SITE_NR]['url_fake']) require(axs_dir('site_base').'site.url.php'); # <"Friendly" URL handling functions. />
foreach (array('theme'=>'theme', 'code.head'=>'code_head', 'code.bottom'=>'code_bottom', ) as $k=>$v) $axs['page'][$k]=$cfg[$v];

#<Get theme class />
require_once('site.theme.base.php');
if ($tmp=axs_file_choose('index.php')) include_once($tmp);
if (!class_exists('axs_theme')) {	class axs_theme extends axs_theme_base {}	}
$axs['theme']=new axs_theme;

$axs['c']=htmlspecialchars(substr(axs_get('c', $_GET), 0, axs_content::$c_length));
# <Define language>
if ((empty($axs_user)) || (!axs_user::permission_get(array('dev'=>'', 'admin'=>'', 'cms'=>'', )))) foreach ($axs['cfg']['site'][AXS_SITE_NR]['langs'] as $k=>$v) if ($v['h']) unset($axs['cfg']['site'][AXS_SITE_NR]['langs'][$k]);
reset($axs['cfg']['site'][AXS_SITE_NR]['langs']);
if (!empty($_GET['l'])) {
	$axs['l']=(isset($axs['cfg']['site'][AXS_SITE_NR]['langs'][$_GET['l'][0].$_GET['l'][1]])) ? $_GET['l'][0].$_GET['l'][1]:false;
	if (!$axs['l']) {
		$axs['l']=key($axs['cfg']['site'][AXS_SITE_NR]['langs']);
		axs_redir('?');
		axs_log(__FILE__, __LINE__, (strlen($_GET['l'])==2) ? 'url':'xssalert', 'Invalid language or Possible XSS or PHP injection attempt', true);
		}
	}
else $axs['l']=key($axs['cfg']['site'][AXS_SITE_NR]['langs']); # </Define language>

$axs['url']=array('c'=>$axs['c'], 'l'=>$axs['l'], );
#<Read frame template />
$axs['template']=axs_tpl(false, 'index.tpl');
#<Get menus config />
$tmp=array();
$axs['template']=axs_tpl_tags($axs['template'], 'menu.', $tmp, true);
foreach ($tmp as $k=>$v) {
	unset($tmp[$k]);
	if (count($k=explode('.', $k))===2) $tmp[$k[1]]=$v;
	}
axs_menu_struct($tmp);
#<Customize structure />
$axs['theme']->site_structure();
foreach ($axs['plugins'] as $k=>$v) $v->site_structure();
if (file_exists($tmp=axs_dir('cfg').'index.customize.php')) include_once($tmp);
#</Site structure>

# <Site content>
axs_content::init(AXS_SITE_NR);
if (isset($axs_content['content'][$axs['c']])) $axs['page']['content..title']=axs_html_safe($axs_content['content'][$axs['c']]['title']);
$axs['theme']->site_content(); # Customize content
foreach ($axs['plugins'] as $k=>$v) $v->site_content(); # Customize content
# </Site content>

#<Site parse>
if ($axs_content['frontpage']===$axs['c']) $axs['page']['class']['frontpage']='frontpage';
$axs['page']['title_site']=axs_html_safe($axs_content['title_site']);
$axs['page']['description']=axs_html_safe($axs_content['content'][$axs['c']]['description']);
$axs['page']['class']['plugin-'.$axs_content['content'][$axs['c']]['plugin']]='plugin-'.$axs_content['content'][$axs['c']]['plugin'];
$axs['page']['class']['page-'.$axs['c']]='page-'.$axs['c'];
$axs['page']['c']=$axs['c'];
$axs['page']['l']=$axs['l'];

#<Build content />
$section=axs_get('section', $axs['get']);
foreach ($axs['page']['content'] as $k=>$v) if ($k!='content') {
	if (($section) && ($section!==$k)) continue;
	foreach (array('include'=>false, 'axs_local'=>array(), 'vr'=>array(), ) as $kk=>$vv) if (!isset($v[$kk])) $v[$kk]=$vv;
	$axs['page']['content.'.$k]=axs_content::plugin_load($k, $k, $v['axs_local'], $v['vr'], $v['include']);
	}
$axs['page']['content']=axs_content::plugin_load($axs['c'], 'content');

#<User login/logout />
if ((defined('AXS_LOGINCHK')) && (AXS_LOGINCHK===1)) {
	$axs['page']['class']['user-login']='user-login';
	if (axs_tpl_has($axs['template'], 'login')) $axs['page']['login']=axs_content::plugin_load('index-login', 'login');
	}

#<Site theme />
if ($axs['page']['theme']) {
	$tmp=$axs['dir_site'].'theme.'.$axs['page']['theme'].'/index.css';
	if (file_exists(axs_dir('site').$tmp)) $axs['page']['head']['theme']='<link type="text/css" rel="stylesheet" href="'.axs_dir('site', 'http').$tmp.'" />'."\n";
	}
#<User-defined style />
if (file_exists(axs_dir('content').'index.css')) $axs['page']['head']['style']='<link type="text/css" rel="stylesheet" href="'.axs_dir('content', 'http').'index.css'.'" />'."\n";

#<Build breadcrumb />
$tpl=array('title'=>axs_tpl(false, 'index.breadcrumb.tpl'), 'separator'=>axs_tpl(false, 'index.breadcrumb.sepr.tpl'), );
$axs['page']['breadcrumb']=$axs['page']['title_menu']=array();
$menu_path=axs_content::path_get();
foreach ($menu_path as $k=>$v) {
	if (empty($v['url'])) $v['url']=axs_content::url($axs['url'], array('c'=>$v['c']), '&').'#content';
	if (is_array($v['url'])) $v['url']=axs_content::url(array_merge($axs['url'], array('c'=>$v['c'])), $v['url'], '&').'#content';
	$v=array('url'=>htmlspecialchars($v['url']), 'title'=>axs_html_safe($v['title']), 'level'=>$k, );
	$axs['page']['breadcrumb'][]=axs_tpl_parse($tpl['title'], $v);
	$axs['page']['title_menu'][]=$v['title'];
	}
$axs['page']['title_menu']=($axs_content['title_bar']) ? $axs_content['title_bar']:implode(axs_tpl(false, 'index.title_menu.sepr.tpl'), array_reverse($axs['page']['title_menu']));
$axs['page']['breadcrumb']=implode($tpl['separator'], $axs['page']['breadcrumb']);
unset($menu_path);

#<Build menus>
unset($axs['page']['menu']['prototype']);
#	<Build menu template sets>
foreach ($axs['page']['menu'] as $k=>$v) {
	$tpl_set=(is_array($v['tpl'])) ? key($v['tpl']):$v['tpl'];
	if (isset($axs['menu_tpl'][$tpl_set])) continue; #One set can be created only once.
	if (!is_array($v['tpl'])) $v['tpl']=array($v['tpl']=>array(2=>array(), ));#Make tpl_set array of levels
	foreach ($v['tpl'] as $tpl_set=>$levels) foreach ($levels as $level=>$set) {
		foreach (array(''=>'','cls'=>'.','act'=>'.','scl'=>'.','sop'=>'.','opn'=>'.') as $kk=>$vv) {
			if (!isset($v['tpl'][$tpl_set][$level][$kk])) $v['tpl'][$tpl_set][$level][$kk]=$vv;
			$axs['menu_tpl'][$tpl_set][$level][$kk]=axs_tpl(
				false,
				array(
					'index.menu.'.$tpl_set.'.lvl'.$level.$vv.$kk.'.tpl',
					'index.menu.'.$tpl_set.$vv.$kk.'.tpl',
					'index.menu'.$vv.$kk.'.tpl',
					)
				);
			}
		}
	}
#	</Build menu template sets>
foreach ($axs['page']['menu'] as $k=>$v) {
	$opts=array('date_format'=>$v['date_format'], );
	if (axs_get('plugin', $axs_content['index'][$v['index']])==='menu-page') { #<One-pager menu>
		$opts['url']='';
		if (isset($_GET['c'])) {
			$opts['url']=array(
				'c'=>(($v['index']==='menu1') || ($v['index']===$axs_content['frontpage'])) ? null:$v['index'],
				'l'=>(isset($_GET['l'])) ? $axs['l']:null,
				);
			$opts['url']=axs_content::url($axs['url'], $opts['url'], '&');
			if (!$opts['url']) $opts['url']=$axs['http_root'];
			}
		} #</One-pager menu>
	$axs['page']['menu.'.$k.'.title']=array();
	if ($v['level']==2) {
		$index=axs_get('submenu', $axs_content['index'][$v['index']]);
		$axs['page']['menu.'.$k.'.title'][]=axs_html_safe($axs_content['index'][$v['index']]['title']);
		}
	else {
		$index=array();
		$tmp=(isset($axs_content['content'][$axs['c']]['path'][1]['c'])) ? $axs_content['content'][$axs['c']]['path'][1]['c']:'';
		if ((isset($axs_content['content'][$axs['c']]['path'][$v['level']-1])) && ($axs_content['content'][$axs['c']]['path'][$v['level']-1]['plugin']==='menu-page')) $tmp='';
		if ((!$v['index']) or ($v['index']===$tmp)) {
			$index='$axs_content[\'index\']';
			foreach ($axs_content['content'][$axs['c']]['path'] as $kk=>$vv) if ($kk<$v['level']) {
				if ($kk>=2) $axs['page']['menu.'.$k.'.title'][]=axs_html_safe($vv['title']);
				$index.='[\''.$vv['c'].'\'][\'submenu\']';
				}
			}
		if ($index) $index=(array)eval('if (isset('.$index.')) return '.$index.';');
		}
	if (!empty($axs['menu_tpl'][$v['tpl']][$v['level']])) $v['templates']=$axs['menu_tpl'][$v['tpl']][$v['level']];
	else {
		reset($axs['menu_tpl'][$v['tpl']]);
		$v['templates']=current($axs['menu_tpl'][$v['tpl']]);
		foreach($axs['menu_tpl'][$v['tpl']] as $level=>$set) if ($level<=$v['level']) $v['templates']=$set;
		}
	$axs['page']['menu.'.$k.'.title']=implode($tpl['separator'], $axs['page']['menu.'.$k.'.title']);
	$axs['page']['menu.'.$k]=axs_menu_build($k, $index, $v['level'], $v['max_level'], $v['tpl'], $v['templates'], $v['type'], '	', $opts);
	if ($index) $axs['page']['class'][$k]=$k;
	if ($v['pic']) {
		if (!isset($v['pic_set_on'])) $v['pic_set_on']='#'.$k;
		if ($v['pic_set_on']) $axs['page']['head'][$v['pic_set_on']]='<style type="text/css">'."\n".'	<!--'."\n".
		'	'.$v['pic_set_on'].' {	background-image:url('.axs_dir('content', 'http').$v['pic'].');	}'."\n".'	-->'."\n".'</style>'."\n";
		}
	}
unset($axs['menu_tpl'], $axs['page']['menu'], $kk, $vv, $kkk, $vvv);
#</Build menus>

if ($axs_content['menu.pic']) {
	$axs['page']['menu.pic.file']=axs_dir('content', 'http').$axs_content['menu.pic'];
	$axs['page']['menu.pic.img']='<img class="menu-pic" src="'.$axs['page']['menu.pic.file'].'" alt="'.axs_html_safe($axs_content['content'][$axs['c']]['title']).'" />';
	}

#<Build sites choice if more than one site configured />
if (axs_tpl_has($axs['template'], 'sites')) {
	$tpl=array('li'=>axs_tpl(false, 'index.site_select.tpl'), 'li.act'=>axs_tpl(false, 'index.site_select.act.tpl'), 'separator'=>axs_tpl(false, 'index.site_select.sepr.tpl'), );
	foreach ($axs['cfg']['site'] as $k=>$v) {
		$v['lang_code']=key($v['langs']);
		$v['lang_label']=$v['langs'][$v['lang_code']]['l'];
		foreach ($v as $kk=>$vv) if (is_array($vv)) unset($v[$kk]);
		$v['url']=(($v['domain']) ? rtrim('http://'.$v['domain'], '/').'/':'').$v['dir'];
		$axs['page']['sites'][$k]=axs_tpl_parse(((AXS_SITE_NR===$k) ? $tpl['li.act']:$tpl['li']), $v);
		}
	$axs['page']['sites']=implode($tpl['separator'], $axs['page']['sites']);
	}

#<Build languages choice />
$tpl=array(
	'li'=>axs_tpl(false, 'index.lang.tpl'),
	'li.current'=>axs_tpl(false, 'index.lang.act.tpl'),
	'separator'=>axs_tpl(false, 'index.lang.sepr.tpl'),
	);
foreach ($axs['cfg']['site'][AXS_SITE_NR]['langs'] as $k=>$v) {
	$v['class']=($v['h']) ? 'hidden':'';
	if ($axs['l']===$k) {
		$tmp='.current';
		$axs['page']['lang.current']=axs_html_safe($v['l']);
		$axs['page']['lang.current.abbr']=axs_html_safe($v['abbr']);
		}
	else $tmp='';
	$axs['page']['lang.list'][]=axs_tpl_parse($tpl['li'.$tmp], array('code'=>$k, 'class'=>$v['class'], 'label'=>axs_html_safe($v['l']), 'abbr'=>axs_html_safe($v['abbr']), ));
	}
$axs['page']['lang.list']=implode($tpl['separator'], $axs['page']['lang.list']);
unset($tpl);

$axs['theme']->site_finish(); # Customize

#<Socialmedia OpenGraph tags>
if (!isset($axs['page']['head']['og:title'])) $axs['page']['head']['og:title']='<meta property="og:title" content="'.htmlspecialchars(
	(strlen($axs_content['content'][$axs['c']]['title_bar'])) ? $axs_content['content'][$axs['c']]['title_bar']:implode('>', axs_content::path_get(false, 'title'))
	).'" />'."\n";
if (!isset($axs['page']['head']['og:type'])) $axs['page']['head']['og:type']='<meta property="og:type" content="website" />'."\n";   
if (!isset($axs['page']['head']['og:url'])) $axs['page']['head']['og:url']='<meta property="og:url" content="'.htmlspecialchars($axs['http'].'://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']).'" />'."\n"; 
if ((!isset($axs['page']['head']['og:description'])) && (strlen($axs_content['content'][$axs['c']]['description']))) $axs['page']['head']['og:description']='<meta property="og:description" content="'.htmlspecialchars($axs_content['content'][$axs['c']]['description']).'" />'."\n";
if (!isset($axs['page']['head']['og:site_name'])) $axs['page']['head']['og:site_name']='<meta property="og:site_name" content="'.$axs['page']['title_site'].'" />'."\n";
if (!isset($axs['page']['head']['og:image'])) foreach (axs_content::$socialmedia_og_image as $k=>$v) {
	if (file_exists(axs_dir('content').'socialmedia.og.image.'.$v)) {
		$axs['page']['head']['og:image']='<meta property="og:image" content="'.htmlspecialchars($axs['http'].'://'.$_SERVER['SERVER_NAME'].axs_dir('content', 'http').'socialmedia.og.image.'.$v).'" />'."\n";
		$axs['page']['head']['og:image:alt']='<meta property="og:image:alt" content="'.htmlspecialchars(
			(strlen($axs_content['content'][$axs['c']]['title_bar'])) ? $axs_content['content'][$axs['c']]['title_bar']:implode('>', axs_content::path_get(false, 'title'))
			).'" />'."\n";
		break;
		}
	}
if ((!isset($axs['page']['head']['fb:app_id'])) && (!empty($cfg['fb_app_id']))) $axs['page']['head']['fb:app_id']='<meta property="fb:app_id" content="'.htmlspecialchars($cfg['fb_app_id']).'" />'."\n";
#</Socialmedia OpenGraph tags>

$axs['page']['class']=implode(' ', $axs['page']['class']);
$axs['page']['head']=implode('', $axs['page']['head']);
$axs['page']['code.head']=implode("\n", $axs['debug']).$axs['page']['code.head'];

if (isset($axs['ob'])) $axs['ob']=axs_ob_catch($axs['ob']);
#<If page element requested by ajax />
if (($section) && (isset($axs['page'][$section]))) return axs_html_doc($axs['page'][$section], $section);
#<Parse site template with the page elements />
$html=axs_tpl_parse($axs['template'], $axs['page']);
if (defined('AXS_START_TIME')) { #<Measure performance>
	$tmp='exec:'.round((array_sum(explode(' ', microtime()))-AXS_START_TIME), 3).'s';
	if (function_exists('memory_get_usage')) $tmp.=' mem:'.number_format(memory_get_usage()/1024, 2, '.', '').'K';
	if (function_exists('memory_get_peak_usage')) $tmp.=' peak:'.number_format(memory_get_peak_usage()/1024, 2, '.', '').'K';
	$html=str_replace('</html>', '<!-- '.$tmp." -->\n".'</html>', $html);
	} #</Measure performance>
return $html;
#</Site parse>
#2008-07 ?>