<?php #27.07.2009
$plugin_def=array(); # plugin default data
$plugin_def['name']['']='RSS feed';
$plugin_def['title']['']='RSS feed';
$plugin_def['help']['']='';
$plugin_def['mkdir']='';
$plugin_def['content']['']='';

if (defined('AXS_SITE_NR')) {
class axs_rss_feed {
	function axs_rss_feed() {
		global $axs;
		$this->site_nr=AXS_SITE_NR;
		$this->db=$axs['cfg']['site'][$this->site_nr]['db'];
		$this->px=$axs['cfg']['site'][$this->site_nr]['prefix'];
		$this->url=htmlspecialchars($axs['http'].'://'.$_SERVER['SERVER_NAME'].$axs['http_root'].$axs['site_dir']);
		} # </axs_rss()>
	function load($f) {
		global $axs, $axs_content, $axs_user;
		return (array)include($f);
		} # </load()>
	} # </class::axs_rss>
$data=new axs_rss_feed();
$data->vr=array(
	'charset'=>$axs['cfg']['charset'],
	'url'=>$data->url,
	'title'=>$axs_content['content'][$axs['c']]['title'],
	'home_url'=>$data->url.'?'.axs_url($axs['url']),
	'l'=>$axs['l'],
	'date'=>date('r'), # RFC 2822 formatted date (Mon, 30 Sep 2002 11:00:00 GMT)
	'items'=>'',
	);
$data->plugins=$data->items=array();
$data->dir=AXS_SITE_ROOT.$axs['site_dir'].$axs['dir_plugins'];
$data->handle=opendir(($data->dir) ? $data->dir:'./');
if ($data->handle) while (($file=readdir($data->handle))!==false) { # <Read directory cycle>
	if (substr($file, -12)=='.rss.ext.php') {
		$data->load_plugin=preg_replace('/\.rss\.ext\.php$/', '', $file);
		$data->items[$data->load_plugin]=$data->load($data->dir.$file);
		if (strlen($data->items[$data->load_plugin]['rank'])) $data->plugins[$data->load_plugin]=$data->items[$data->load_plugin]['rank'];
		}
	} # </Read directory cycle>
closedir($data->handle);
asort($data->plugins);
$data->tpl=axs_tpl(AXS_PATH, $axs_content['content'][$axs['c']]['plugin'].'.item.tpl');
foreach ($data->plugins as $k=>$v) foreach ((array)$data->items[$k]['data'] as $cl) {
	foreach (array('title','link','description','pubDate',) as $v) if (!isset($cl[$v])) $cl[$v]='';
	$data->vr['items'].=axs_tpl_parse($data->tpl, $cl);
	}
header('Content-Type: application/xml');
$axs['template']=axs_tpl_parse(axs_tpl(AXS_PATH, $axs_content['content'][$axs['c']]['plugin'].'.tpl'), $data->vr);
return '';
}
#07.2006 ?>