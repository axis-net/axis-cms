<?php #2018-02-14
//require_once(AXS_PATH_CMS.'fn.form.php');
class axs_comments extends axs_form {
	var $fields=array(
		'id'=>array('type'=>'text', 'size'=>10, 'readonly'=>'readonly'),
		'time'=>array('type'=>'timestamp', 'timestamp'=>'d.m.Y\&\n\b\s\p;\&\n\b\s\p;H:i:s'),
		'name'=>array('type'=>'text', 'size'=>25, 'maxlength'=>25),
		'message'=>array('type'=>'wysiwyg'),
		'user_ip'=>array('type'=>'hidden', 'size'=>10),
		'hostname'=>array('type'=>'hidden', 'size'=>255),
		);
	function axs_comments($site_nr, $url, $tr, $table, $parent_id, $fields=array()) {
		global $axs;
		$this->axs=&$axs;
		$this->cfg=&$axs['cfg'];
		$this->site_nr=$site_nr;
		$this->db=$this->cfg['site'][$this->site_nr]['db'];
		$this->px=$this->cfg['site'][$this->site_nr]['prefix'];
		$this->parent_id=$parent_id+0;
		$this->table=$table;
		$this->url=$url;
		$this->tr=(array)$tr;
		$this->b='?'.axs_url($this->url, array('comment_edit'=>false), false);
		foreach ($fields as $k=>$v) $this->fields[$k]=$v;
		foreach ($this->fields as $k=>$v) $this->fields[$k]['label']=$tr->s('comments_'.$k.'_lbl');
		$this->tpl_get();
		} # </axs_comments()>
	function list_get($psize=25, $sort='time', $dsc=true) { # Show list
		$data=array('comments.form.title'=>axs_admin::tr('comments.form.title'), 'back_url'=>$back_url, 'back_lbl'=>axs_admin::tr('back_lbl'), 'header'=>'', 'list'=>'', );
		
		foreach ($this->fields as $k=>$v) $data['header'].='<th scope="col" class="'.$k.'">'.$v['label'].'</th>';
		//require_once(AXS_PATH_CMS.'fn.pager.php');
		$pager=new axs_pager(axs_get('p', $_GET), array(25,50,100), 50, true, 'time');
		$result=axs_db_query('SELECT SQL_CALC_FOUND_ROWS t.* FROM `'.$this->px.$this->table.'` AS t'."\n".
		'	WHERE parent_id=\''.$this->parent_id.'\' ORDER BY t.`'.$pager->sort_col.'` '.$pager->desc.' LIMIT '.$pager->start.','.$pager->psize,
		1, $this->db, __FILE__, __LINE__);
		foreach ($result as $cl) {
			$vl=array();
			foreach ($this->fields as $k=>$v) $vl[$k]=axs_form::input_value_text($this->fields, $k, $cl);
			$vl['user_ip']=$this->ip_from_number($cl['user_ip']);
			$vl['user_ip']='<a href="'.str_replace('{$ip}', $vl['user_ip'], $this->cfg['ip_location_url']).'" target="_blank">'.$vl['user_ip'].'</a>';
			$cl=array(
				'id'=>$cl['id'],
				'cl'=>'',
				'edit_url'=>'?'.axs_url($this->url, array('comment_edit'=>$cl['id'], 'b'=>'?'.$_SERVER['QUERY_STRING'].'#id'.$cl['id'])),
				'edit_lbl'=>axs_admin::tr('edit_lbl'),
				);
			foreach ($this->fields as $k=>$v) $cl['cl'].='<td class="'.$k.'">'.$vl[$k].'</td>';
			$data['list'].=axs_tpl_parse($this->templates['item'], $cl);
			}
		unset($result);
		# generate pages
		$data['total']=axs_db_query('', 'found_rows', $this->db, __FILE__, __LINE__);
		if ($data['total']) {
			$data['pages']=$pager->pages($data['total'], '?'.axs_url($this->url, array('p'=>'')));
			$data['prev']=$data['pages']['prev'];
			$data['current']=$data['pages']['current'];
			$data['next']=$data['pages']['next'];
			$data['pages']=($data['total']>$pager->psize) ? $data['pages']['pages']:'';
			}
		else $data['prev']=$data['current']=$data['next']=$data['pages']='';
		return axs_tpl_parse($this->templates[''], $data);
		} # </list>
	function edit($id) {
		$cl=array();
		foreach ($this->fields as $k=>$v) $cl[$k]=axs_get($k, $_REQUEST);
		$cl['id']=$id+0;
		if ($_POST['save']) { # <Save>
			axs_db_query('UPDATE `'.$this->px.$this->table.'` AS t SET '.axs_form::sql_values_set('t', $this->fields, $cl).''."\n".
			'	WHERE t.id=\''.$cl['id'].'\' AND t.parent_id=\''.$this->parent_id.'\'', '', $this->db, __FILE__, __LINE__);
			exit(axs_redir($this->b));
			} # </Save>
		# <Read editor>
		if (empty($_POST) && $cl['id']) {
			$cl=axs_db_query('SELECT t.* FROM `'.$this->px.$this->table.'` AS t WHERE t.id=\''.$cl['id'].'\' AND t.parent_id=\''.$this->parent_id.'\'', 'row', $this->db, __FILE__, __LINE__);
			$cl=axs_form::sql_values_get($this->fields, $cl);
			}
		$cl['url_formact']='?'.axs_url($this->url, array('comment_edit'=>$cl['id'], 'b'=>$this->b, ));
		$cl['back_url']=htmlspecialchars($this->b);
		$required=$this->templates('required');
		$cl['elements']='';
		foreach ($this->fields as $k=>$v) {
			if ($v['type']=='hidden') continue;
			foreach (array('label','required','comment','txt') as $vv) if (!isset($cl[$vv])) $cl[$vv]='';
			$v['input']=axs_form::input($k, $v, $cl);
			$v['name']=$k;
			$v['required']=($v['required']) ? $required:'';
			$cl['elements'].=axs_tpl_parse($this->templates($v), $v);
			}
		$cl['input_required']=$required;
		axs_form::css_js();
		return axs_tpl_parse($this->templates['edit'], $cl+$this->tr); # </Read editor>
		} # </edit>
	function edit_del($id) { # <Delete comment>
		if (!$id+0) return;
		axs_db_query('DELETE t FROM `'.$this->px.$this->table.'` AS t WHERE t.id=\''.$id.'\' AND t.parent_id=\''.$this->parent_id.'\'', '', $this->db, __FILE__, __LINE__);
		exit(axs_redir($this->b));
		} # </edit_del>
	function ui($psize=25, $sort='time', $dsc=true) {
		if ($_POST['del'] && $_POST['del_confirm']) return $this->edit_del($_POST['del']);
		if ($_GET['comment_edit']) return $this->edit($_GET['comment_edit']);
		return $this->list_get($psize, $sort, $dsc);
		} # </ui()>
	function ip_to_number($ip) { # Convert IP to integer
		$ip=ip2long($ip);
		if ($ip==-1 or $ip===FALSE) return 0;
		return sprintf("%010u", $ip);
		} # </ip_to_number()>
	function ip_from_number($number) {
		$oct4=($number-fmod($number, 16777216))/16777216;
		$oct3=(fmod($number, 16777216)-(fmod($number, 16777216)%65536))/65536;
		$oct2=(fmod($number, 16777216)%65536-(fmod($number, 16777216)%65536%256))/256;
		$oct1=fmod($number, 16777216)%65536%256;
		$ip = $oct1.'.'.$oct2.'.'.$oct3.'.'.$oct4;
		return $ip;
		} # </ip_from_number()>
	function tpl_get($name=false) {
		if (!isset($this->templates)) $this->templates=array(
			''=>'  <div class="pager">'."\n".
				'   <p class="found"><strong>{$total}</strong> <span class="prev-next">{$prev}{$current}{$next}</span></p>'."\n".
				'   <p class="pages">{$pages}</p>'."\n".
				'  </div>'."\n".
				'  '."\n".
				'  <table class="table" border="1">'."\n".
				'   <tr>{$header}<th></th></tr>'."\n".
				'{$list}'."\n".
				'  </table>'."\n".
				'  '."\n".
				'  <div class="pager">'."\n".
				'   <p class="found"><strong>{$total}</strong>&nbsp;&nbsp;<span class="prev-next">{$prev}{$current}{$next}</span></p>'."\n".
				'   <p class="pages">{$pages}</p>'."\n".
				'  </div>'."\n",
			'item'=>'  <tr id="id{$id}" style="vertical-align:top">{$cl}<td class="edit"><a href="{$edit_url}">{$edit_lbl}</a></td></tr>'."\n",
			'edit'=>'<form id="editform" action="{$url_formact}" method="post" enctype="multipart/form-data" class="form">'."\n".
				' <a href="{$back_url}" class="back">&lt;{$back_lbl}</a>'."\n".
				'{$elements}'."\n".
				' <div class="container footer">'."\n".
				'  <p id="save_container">'."\n".
				'   <input name="save" type="submit" class="submit" id="save" value="{$save_lbl}" />'."\n".
				'   <label id="del_container" for="del">'."\n".
				'    {$comments_del_lbl}<input type="checkbox" class="confirm" name="del" id="del" value="{$id}" />'."\n".
				'   </label>'."\n".
				'   <input name="del_confirm" type="submit" class="submit" id="del_confirm" value="{$del_lbl}" />'."\n".
				'  </p>'."\n".
				'  &nbsp;<a href="#">{$top_lbl}&nbsp;&uarr;</a></p>'."\n".
				' </div>'."\n".
				'<script>var axs_editform=new axs_form(\'editform\');</script>'."\n".
				'</form>'."\n",
			);
		} # </tpl_get()>
	} # </class:axs_comments>
#2009-06-28 ?>