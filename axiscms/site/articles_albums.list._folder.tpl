		<article id="id{$id}" class="clear-left {$_class}">
			<h2><a href="{$article_url}">{$title}</a> <time datetime="{$date.datetime}">{$date}</time></h2>
			<a href="{$article_url}" class="thumbs">{$thumbs}</a>
			<div class="summary">{$summary}</div>
		</article>
