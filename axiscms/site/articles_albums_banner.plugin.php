<?php #2019-05-10
$plugin_def=array(); # plugin default data
$plugin_def['name']['en']='Picture and media galleries > Banner > Pictures';
$plugin_def['name']['et']='Pildi- ja meediaalbumid > Bänner >Pildid';
$plugin_def['title']['']='';
$plugin_def['help']['']='';
$plugin_def['mkdir']='files';
$plugin_def['content']['']='<?php #|'.$axs['time'].',,'.axs_get('user', $axs_user).',,||?>'."\n";
require_once('articles_albums.class.php');
$plugin_def['tr']=axs_articles_albums::$tr;

if (defined('AXS_SITE_NR')) {
$data=new axs_articles_albums($axs_local);
if ($tmp=$data->error()) return $tmp;
$data->banner_init();
return $data->banner_parse();
}
#2019-05-10 ?>