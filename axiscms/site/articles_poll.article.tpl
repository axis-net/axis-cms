		<form id="{$section}-{$id}" class="{$_class} post-ajax" action="{$article_url}#{$section}-{$id}" method="post">
			<fieldset>
				<legend>{$title}</legend>
				<p class="publ">{$publ.lbl} {$publ} {$arh.lbl} {$arh}</p>
				{$_choice}
			</fieldset>
			<script>axs.form.init("{$section}-{$id}");</script>
		</form>