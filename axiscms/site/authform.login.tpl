	<div class="text">{$content}</div>
	<form id="login_form" action="{$url_formact}" method="post" class="login">
		<fieldset><legend>{$user_login.lbl}</legend>
			{$msg}
			<div class="text user">
				<label for="{$section}-axs_user" class="user">{$user.lbl}<em class="input_required"><abbr title="{$user_msg_value_required}">*</abbr></em></label>
				<input id="{$section}-axs_user" name="axs[user]" type="text" size="10" value="{$user}" required="required" />
			</div>
			<div class="password pass">
				<label for="{$section}-axs_pass" class="pass">{$pass.lbl}<em class="input_required"><abbr title="{$user_msg_value_required}">*</abbr></em></label>
				<input id="{$section}-axs_pass" name="axs[pass]" type="password" size="10" />
			</div>
			<div class="checkbox user_remember">
				<label for="{$section}-axs_user_remember" class="user_remember"><input id="{$section}-axs_user_remember" name="axs[user_remember]" type="checkbox" value="1"{$user_remember} />{$user_remember.lbl}</label>
			</div>
			<div class="checkbox pass_send">
				<label for="{$section}-axs_pass_send" class="pass_send"><input id="{$section}-axs_pass_send" name="axs[pass_send]" type="checkbox" value="1"{$pass_send} />{$pass_send.lbl}</label>
			</div>
			<div class="submit login">
				<input id="{$section}-axs_login" name="axs[login]" type="submit" value="{$submit.lbl}" />
			</div>
		</fieldset>
	</form>
	<script>axs.user.window_size_send("login_form");</script>