<?php #2015-05-31
$plugin_def=array(); # plugin default data
$plugin_def['name']['et']='Pildigalerii (automaatne piltide nimekiri)';
$plugin_def['title']['']='';
$plugin_def['help']['']='';
$plugin_def['mkdir']='files';
$plugin_def['content']['']='<?php #|,'.$axs['time'].','.axs_get('user', $axs_user).',,|<p class="comment">Lisa pildid paneeli &quot;Failid&quot; kaudu</p>|?>'."\n".
'<?php #|'.$axs['time'].',,'.axs_get('user', $axs_user).',,||?>'."\n";

if (defined('AXS_SITE_NR')) {
# Text content at the beginnig of page
$axs_content['content'][$axs_local['c']]['template']='deflt';
$data=include('deflt.plugin.php');

# Read directory for image files
$dir=$axs_local['dir'].$axs_local['c'].'_files/';
$dir_fs=axs_dir('content').$dir;
$dir_http=axs_dir('content', 'h').$dir;
$files=$files_t=array();
if (is_dir($dir_fs)) {
	$handle=opendir($dir_fs);
	while (($f=readdir($handle))!==false) {
		if (is_file($dir_fs.'/'.$f)) {
			$tmp=explode('.', $f);
			$f_ext=(strpos(' '.$f, '.')) ? strtolower(array_pop($tmp)):'';
			if (in_array($f_ext, array('bmp', 'gif', 'jpg', 'jpeg', 'png', 'tif', 'tiff', 'wbmp', 'xbm'))) {
				if (preg_match('/_t\.'.$f_ext.'$/', $f)) $files_t[$f]=$f_ext; # Add to thumbs array
				else $files[$f]=$f_ext; # Add to big images array
				}
			}
		}
	closedir($handle);
	} # END Read directory
# Add big image links to thumbs and remove those from $files
foreach ($files_t as $k=>$v) {
	$key=preg_replace('/_t\.'.$v.'$/i', '.'.$v, $k);
	if (isset($files[$key])) {
		$files_t[$k]=$key;
		unset($files[$key]);
		}
	else $files_t[$k]=false;
	}
# Add big images that were left without thumbs
foreach ($files as $k=>$v) {
	$files_t[$k]=false;
	}
unset($f_ext, $key, $files);
ksort($files_t);

$tpl=axs_tpl(false, $axs_local['plugin'].'.tpl');
$tpl_popup=axs_tpl(false, $axs_local['plugin'].'.popup.tpl');
$tmp='';
foreach ($files_t as $k=>$v) {
	if ($v) $tmp.=axs_tpl_parse($tpl_popup, array('file_t'=>$k, 'file'=>urlencode($dir_http.$v), 'url'=>'?'.urlencode('axs[gw]').'='.urlencode($dir_http.$v), ));
	else $tmp.=axs_tpl_parse($tpl, array('file_t'=>$k, ));
	}
$tmp=axs_tpl_parse($tmp, array('dir'=>$dir_http, ));
return $data.$tmp;
}
#2007 ?>