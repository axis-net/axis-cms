<?php #2018-10-21
(defined('AXS_PATH_CMS')) or exit(require('../index.php'));

#<Edit>
if (isset($_POST['_add_new'])) {
	$id=intval(key($_POST['_add_new']));
	axs_db_query("UPDATE `".$this->px."business_orders` SET `status`='".addslashes(axs_business_orders::$statuses[$_POST['status'][$id]])."' WHERE `id`='".$id."'", '', $this->db, __FILE__, __LINE__);
	axs_exit(axs_redir('?'.axs_url($this->url, array('_edit'=>false, ), false).'#id'.intval($_GET['_edit'])));
	}
#</Edit>

$tr=new axs_tr($axs_content['content'][$axs['c']]['tr']);

#<Export>
if ($tmp=axs_get('_pdf_invoice', $_GET)) {
	$i=new axs_business_invoices($this->site_nr, $axs['l']/*, $tr->filter(array('checkout.', 'orders.'))*/);
	$i->export_pdf($tmp, true);
	}
#</Export>

$tpl=axs_db_edit::templates(array(''=>'', 'data'=>'data.table', 'item'=>'data.table.item', 'cell'=>'data.table.item.cell', ));
$vr=array(
	'forms'=>'', 'tools'=>'', 'search'=>'', 'search.found.lbl'=>'', 'total'=>'', 'prev'=>'', 'current'=>'', 'next'=>'', 'pages'=>'',
	'data'=>$tpl['data'],
	'edit.url'=>'?'.axs_url($this->url, array('_edit'=>1)),
	'form.title'=>'tellimused', 'thead'=>'', 'add'=>'muuda', 'list'=>'', 
	);
$o=new axs_business_orders($this->site_nr, $axs['l']/*, $tr->filter(array('checkout.', 'orders.'))*/);
$thead=array(
	'order_nr'=>'order_nr', 'time'=>'time.fmt', 'client_name'=>'client_name', 'client_email'=>'client_email', 'payment_method'=>'payment_method.fmt', 'revenue'=>'revenue.fmt',
	'status'=>'status.fmt', 'invoice_id'=>'invoice_id',
	);
foreach ($thead as $k=>$v) $vr['thead'].='<th class="'.$k.'">'.$o->tr->s($k.'.lbl').'</th>';
foreach ($o->list_get() as $id=>$cl) {
	$vl=$cl;
	$vl['_data']=array();
	foreach ($thead as $k=>$v) $vl['_data'][$k]=htmlspecialchars($cl[$v]);
	$vl['_data']['order_nr']='<a href="'.htmlspecialchars($cl['order_url']).'" target="_blank">'.$vl['_data']['order_nr'].'</a>';
	if ($cl['revenue']==0) $vl['_data']['revenue']='-';
	$vl['_data']['status']='';
	foreach (axs_business_orders::$statuses as $k=>$v) {
		$v=($cl['status']===$k) ? ' selected="selected"':'';
		$vl['_data']['status'].='<option value="'.$k.'"'.$v.'>'.$o->tr->s('status.'.$k.'.lbl').'</option>';
		}
	$vl['_data']['status']='<label for="status'.$id.'" class="visuallyhidden">'.$o->tr->s('status.lbl').'</label><select id="status'.$id.'" name="status['.$id.']">'.$vl['_data']['status'].'</select>';
	$vl['_data']['invoice_id']=($cl['invoice_id']) ? '<a href="'.'?'.axs_url($this->url, array('_pdf_invoice'=>$cl['invoice_id'])).'">'.$o->tr->s('invoice_id.lbl').'</a>':'-';
	$vl['_tools']='';
	$vl['_add']='<input name="_add_new['.$id.']" type="submit" value="muuda" />';
	$vl['_data']=implode('', axs_db_edit::table_row($vl['_data']));
	$vr['list'].=axs_tpl_parse($tpl['item'], $vl);
	}
return axs_tpl_parse($tpl[''], $vr);
#2009-11-05 ?>