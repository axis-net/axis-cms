		<article id="id{$id}" class="item clear-left {$_class}">
			<a href="?axs[gw]={$file.t2.url}" title="{$title}" class="axs img" target="overlay">{$file.t1}</a>
			<h2>{$title}</h2>
			<time datetime="{$date.datetime}">{$date}</time>
			<div class="summary">{$summary}</div>
		</article>
