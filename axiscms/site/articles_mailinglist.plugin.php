<?php #2019-03-30 utf-8
$plugin_def=array(); # plugin default data
$plugin_def['name']['et']='Postiloend';
$plugin_def['name']['et']='Mailinglist';
$plugin_def['help']['']='';
$plugin_def['mkdir']='';
$plugin_def['content']['']='<?php #|'.$axs['time'].',,'.axs_get('user', $axs_user).',,||?>'."\n";
$plugin_def['tr']['en']=array(
	'form.title'=>'subscribers',
	'id.lbl'=>'id',
	'email.lbl'=>'e-mail',
	'subscribe.lbl'=>'subscribe',
	'unsubscribe.lbl'=>'unsubscribe',
	'time_subscribe.lbl'=>'time',
	'disable.lbl'=>'disabled',
	'save.lbl'=>'save',
	'config.list_address_subscribe_lbl'=>'list server subscribe address',
	'config.list_address_unsubscribe_lbl'=>'list server unsubscribe address',
	'config.list_name_lbl'=>'list name on the list server',
	);
$plugin_def['tr']['et']=array(
	'form.title'=>'liitunud aadressid',
	'id.lbl'=>'id',
	'email.lbl'=>'e-post',
	'subscribe.lbl'=>'liitu uudiskirjaga',
	'unsubscribe.lbl'=>'eemalda listist',
	'time_subscribe.lbl'=>'aeg',
	'disable.lbl'=>'eemaldatud',
	'save.lbl'=>'salvesta',
	'config.list_address_subscribe_lbl'=>'postiloendi serveri liitumisaadress',
	'config.list_address_unsubscribe_lbl'=>'postiloendi serveri listist lahkumise aadress',
	'config.list_name_lbl'=>'listi nimi postiloendi serveris',
	);
$plugin_def['form']=array(
	''=>array(
		'mailto'=>'',
		//'mail_name_field'=>'',//current($plugin_def['name']),
		//'list_type'=>'majordomo',
		'list_address_subscribe'=>array('type'=>'email', 'size'=>50, ),
		'list_address_unsubscribe'=>array('type'=>'email', 'size'=>50, ),
		'list_name'=>array('type'=>'text', 'size'=>50, 'value'=>'list', ),
		//'posts_save'=>25,
		'locked'=>true,
		'tr'=>array(
			'msg'=>array('et'=>'Teie liitumissoov on saadetud, täname!', 'en'=>'Your subscription request has been sent, thank You!', ),
			'msg_subscribe'=>array('et'=>'Teie liitumissoov on saadetud, täname!', 'en'=>'Your subscription request has been sent, thank You!', ),
			'msg_unsubscribe'=>array('et'=>'Nimekirjast eemaldamise soov on saadetud, täname!', 'en'=>'Your unsubscription request has been sent, thank You!', ),
			'msg_value_required'=>array('en'=>'input is required!', 'et'=>'välja täitmine on kohustuslik!', ),
			'msg_value_invalid'=>array('en'=>'invalid value!', 'et'=>'sisestatud väärtus ei ole sobiv!', ),
			'msg_value_unique'=>array('en'=>'value must be unique!', 'et'=>'väärtus peab olema kordumatu!', ),	
			),
		),
	'email'=>array('type'=>'email', 'size'=>50, 'required'=>1, 'label'=>array('et'=>'e-post', 'en'=>'e-mail'), ),
	'subscribe'=>array('type'=>'submit', 'label'=>array('et'=>'liitu', 'en'=>'subscribe'), ),
	'unsubscribe'=>array('type'=>'submit', 'label'=>array('et'=>'eemalda listist', 'en'=>'unsubscribe'), ),
	);

if (defined('AXS_SITE_NR')) {
require_once('articles_mailinglist.class.php');
$data=new axs_articles_mailinglist($axs_local);
if ($tmp=$data->error()) return $tmp;
$data->vr['_content']=include('deflt.plugin.php');
if ($axs_local['section']!=='content') {
	$data->banner_init();
	return $data->banner_parse();
	}
$data->page_init($plugin_def);
return $data->content_display();
}
#2013-03-03 ?>