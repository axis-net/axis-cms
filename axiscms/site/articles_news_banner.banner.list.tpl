			<article id="id{$id}" class="item {$_class}">
				<h3>
					<a href="{$article_url}">
						{$title}
						<time class="publ" datetime="{$publ.datetime}">{$publ}</time>
					</a>
				</h3>
				<div class="summary">
					{$file.t1}
					{$summary}
				</div>
			</article>
