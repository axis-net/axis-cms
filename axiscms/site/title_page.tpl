<!DOCTYPE html>
<!-- Do not modify this file, place a local copy in "axs_site/" or theme's directory! -->
<html lang="{$l}" class="{$class}">
<head>
<meta charset="{$charset}" />
<title>{$title_menu} - {$title_site}</title>
<meta name="description" content="{$description}" />
<meta name="generator" content="AXIS CMS" />
<base href="{$/}" />
<meta name="HandheldFriendly" content="True" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<link type="text/css" href="?axs%5Bgw%5D=axs.css" rel="stylesheet" media="screen,projection,tv" />
<link type="text/css" href="{$/}axiscms/files/index.responsive.css" rel="stylesheet" media="screen,projection,tv" />
<link type="text/css" href="{$/}axiscms/files/index.css" rel="stylesheet" media="screen,projection,tv" />
<link type="text/css" href="{$/}axiscms/files/index.print.css" rel="stylesheet" media="print" />
<script src="?axs%5Bgw%5D=axs.js"></script>
<script src="?axs%5Bgw%5D=axs.menu.js"></script>
<script src="?axs%5Bgw%5D=mediaplayer.js"></script>
{$head}<!--[if lte IE 7]><p class="msg ie"><strong>You are using an outdated web browser. For best web browsing experience, please <a href="http://www.microsoft.com/windows/internet-explorer" target="_blank">update Your web browser</a>!</strong></p><![endif]-->
<!--[if lte IE 8]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<script src="{$/}axiscms/lib.js/css3-mediaqueries.js"></script>
<![endif]-->
<style type="text/css">
<!--
html {
	min-height:100.20%;
	background:#000;
	color:#fff;
	}
img {
	border:none;
	}
#logo {
	padding:100px 0 20px 0;
	text-align:center;
	}
-->
</style>
</head>

<body>{$debug}
<img src="model/axs_gfx/logo.png" alt="{$title_site}" />
<div id="content" class="content_plugin">
{$content}
</div>
<ul id="menu1">
{$menu1}
</ul>
</body>
</html>