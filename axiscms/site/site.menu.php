<?php #2020-02-04
function axs_menu_build($menu_id, $index, $level=1, $max_level=0, $tpl_set='menu', $tpl='', $open_all=false, $spc='	', $opts=array()) {
	global $axs, $axs_content;
	foreach (array('date_format'=>'d.m.Y H:i', 'url'=>false, ) as $k=>$v) if (!isset($opts[$k])) $opts[$k]=$v;
	if (!empty($axs['menu_tpl'][$tpl_set][$level])) $tpl=$axs['menu_tpl'][$tpl_set][$level];
	if (!is_array($index)) return '';
	//dbg($tpl);
	foreach (array('act'=>'cls', 'scl'=>'cls', 'sop'=>'cls', 'opn'=>'act', ) as $k=>$v) if (!$tpl[$k]) $tpl[$k]=$tpl[$v];
	if (!$max_level) $max_level=1000;
	//$url_fake=(defined('AXS_URL_FAKE')) ? true:false;
	$menu='';
	$rownr=0;
	foreach($index as $k=>$cl) { # begin cycle over all rows
		$rownr++;
		if (($axs['time']) && ($cl['publ']>$axs['time'] or ($cl['hidden'] && $cl['hidden']<$axs['time']))) continue;
		$cl['nr']=$rownr;			
		$cl['publ']=($cl['publ']) ? date($opts['date_format'], $cl['publ']):'';
		$cl['hidden']=($cl['hidden']) ? date($opts['date_format'], $cl['hidden']):'';
		if (!$cl['url']) {
			$cl['url']=($opts['url']!==false) ? $opts['url'].'#'.$cl['c']:axs_content::url($axs['url'], array('c'=>$cl['c']), false);
			}
		$cl['url']=htmlspecialchars($cl['url']);
		if (!isset($cl['class'])) $cl['class']=array();
		$cl['class'][$cl['plugin']]=$cl['plugin'];
		$cl['class']=implode(' ', $cl['class']);
		$cl['target']=($cl['target']) ? ' target="'.$cl['target'].'"':'';
		$cl['title']=(isset($cl['title.html'])) ? $cl['title.html']:htmlspecialchars($cl['title']);
		if (($cl['plugin']==='menu') or ($cl['plugin']==='menu-page')) $open=(isset($axs_content['content'][$axs['c']]['path'][$level]['c'])) ? $axs_content['content'][$axs['c']]['path'][$level]['c']:false;
		if ($cl['plugin']==='menu-page') {
			$cl['tpl']=($k===$open) ? 'act':'cls';
			unset($cl['submenu']);
			}
		if (!isset($cl['submenu'])) {
			if (!isset($cl['tpl'])) {	$cl['tpl']=($k===$axs['c']) ? 'act':'cls';	}
			}
		else {
			if ($level<=$max_level) {
				//$tmp=(isset($axs_content['content'][$axs['c']]['path'][$level]['c'])) ? $axs_content['content'][$axs['c']]['path'][$level]['c']:false;
				if (($open_all) or ($cl['c']===$open)) {
					if ($level<$max_level) {
						if (!isset($cl['submenu'])) $cl['submenu']=array();
						$cl['submenu']=axs_menu_build($menu_id, $cl['submenu'], $level+1, $max_level, $tpl_set, $tpl, $open_all, $spc.'	', $opts);
						if (!isset($cl['tpl'])) {
							$cl['tpl']=($cl['c']===$open) ? 'opn':'sop';
							}
						}
					else {
						$cl['submenu']='';
						if (!isset($cl['tpl'])) {
							$cl['tpl']=($cl['c']===$open) ? 'opn':'scl';
							//if ($level>=$max_level) $cl['tpl']='scl'; #needs testing!
							}
						}
					}
				else {
					if (!isset($cl['tpl'])) $cl['tpl']='scl';
					unset($cl['submenu']);
					}
				}
			else {
				if (!isset($cl['tpl'])) $cl['tpl']='scl';
				unset($cl['submenu']);
				}
			}
		$row_tpl=$cl['tpl']; 
		unset($cl['plugin'], $cl['tpl']);
		$pics=array();
		foreach (axs_content::$menu_pic as $kk=>$vv) if ($tmp=axs_tpl_has($tpl[$row_tpl], 'pic'.(($kk)?'.'.$kk:''))) {
			$pics[$kk]=$vv;
			}
		if ($pics) {
			$tmp=axs_content::menu_pic_get($cl['path']);
			foreach ($pics as $kk=>$vv) $cl['pic'.(($kk)?'.'.$kk:'')]=($tmp[$kk]) ? '<img src="'.axs_dir('content', 'http').$tmp[$kk].'" alt="'.strip_tags($cl['title']).'" title="'.strip_tags($cl['title']).'" />':'';
			}
		unset($cl['path']);
		$menu.=axs_tpl_parse($tpl[$row_tpl], $cl);
		} # end cycle over all rows
	return ($menu) ? axs_tpl_parse($tpl[''], array('submenu'=>$menu, 'menu_id'=>$menu_id, '/'=>axs_dir('content', 'http'), 'l'=>$axs['l'], 's'=>$spc, )):'';
	} #</axs_menu_build()>
# <Function to configure menus structure. Some examples: 
# - Add menu2 
# axs_menu_struct(array(
#	'menu2'=>array('index'=>'menu2', ),
#	));
# - Add a level 2 submenu to menu1 and a specific template set for it 
# axs_menu_struct(array(
#	'menu1'=>array('max_level'=>2, ),
#	'menu1sub'=>array('index'=>'menu1', 'level'=>3, 'tpl'=>array('menu1sub'=>array(3=>'1sub')), ),
#	));
# />
function axs_menu_struct($customize) {
	global $axs;
	foreach ($customize as $k=>$v) {
		foreach ($v as $kk=>$vv) $axs['page']['menu'][$k][$kk]=$vv;
		foreach ($axs['page']['menu']['prototype'] as $kk=>$vv) if (!isset($axs['page']['menu'][$k][$kk])) $axs['page']['menu'][$k][$kk]=$vv;
		}
	$cfg=array();
	foreach ($axs['page']['menu'] as $k=>$v) if ($k!=='prototype') {
		foreach (array('dropdown'=>'type', 'dropout'=>'dropout', 'drophover'=>'drophover', 'toggle'=>'toggle', 'fixed'=>'fixed', ) as $kk=>$vv) {
			if (isset($v[$vv])) $cfg[$k][$kk]='"'.$kk.'":true';
			}
		if (!empty($cfg[$k])) $cfg[$k]='	"'.$k.'":{'.implode(',', $cfg[$k]).'}';
		}
	if (!empty($cfg)) $axs['page']['head']['axs.menu.cfg']='<script>'."\n".
		'axs.menu.cfg={'."\n".
		implode(",\n", $cfg)."\n".
		'	}'."\n".
		'</script>'."\n";
	} #</axs_menu_struct()>
#2006-08 ?>