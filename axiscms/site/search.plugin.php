<?php #2016-11-07
$plugin_def=array(); # plugin default data
$plugin_def['name']['et']='Otsing';
$plugin_def['title']['et']='Otsingu mooduli tekstid';
$plugin_def['help']['']='';
$plugin_def['mkdir']='';
$plugin_def['content']['']='<?php #|'.$axs['time'].',,'.axs_get('user', $axs_user).',,||?>'."\n";
$plugin_def['tr']['en']=array(
	'search.lbl'=>'search',
	'search_submit.lbl'=>'search',
	'search_result.txt'=>'Search results',
	'search_found.txt'=>'Search returned the following results:',
	'search_found_none.txt'=>'No results found.',
	);
$plugin_def['tr']['et']=array(
	'search.lbl'=>'otsi',
	'search_submit.lbl'=>'otsi',
	'search_result.txt'=>'Otsingutulemus',
	'search_found.txt'=>'Otsing andis järgmised tulemused:',
	'search_found_none.txt'=>'Otsing ei andnud tulemusi.',
	);

if (defined('AXS_SITE_NR')) {
$vr=axs_content::tr_get($plugin_def)->get();
$vr['/']=$axs['page']['/'];
$vr['text']=include('deflt.plugin.php');
$vr['search']=axs_html_safe($axs_content['search']['str']);
$vr['formact']='';
foreach (array_merge($axs['url'], array('c'=>$axs_local['c'])) as $k=>$v) $vr['formact'].='<input name="'.$k.'" type="hidden" value="'.htmlspecialchars(urldecode($v)).'" />';
if ($axs_local['section']!=='content') {
	return axs_tpl_parse(axs_tpl(false, 'search.banner.tpl'), $vr);
	}
$data=new axs_content_search;
$data->tpl=array('list'=>axs_tpl(false, 'search.list.tpl'), 'link_sepr'=>axs_tpl(false, 'search.link_sepr.tpl'), );
$vr['list']=$data->result_html();
if (!$vr['list']) {
	if ($axs_content['search']['str']) $vr['list']=$vr['search_found_none.txt'];
	}
unset($vr['search_found_none.txt']);
return axs_tpl_parse(axs_tpl(false, 'search.tpl'), $vr);
}
/* 3. Full-text searching
Consider our table above. What if we need to search for keywords in both the title and the body? An easy solution is to add a fulltext index:
1	ALTER TABLE mysqltest.articles ADD FULLTEXT alltext (title, body); 
	view plain | print


ALTER TABLE mysqltest.articles ADD FULLTEXT alltext (title, body);

We can now find all articles that feature the words “database” and/or “article” using:
1	SELECT * FROM mysqltest.articles 
2	WHERE MATCH(title, body) AGAINST ('database article'); 
	view plain | print


SELECT * FROM mysqltest.articles
WHERE MATCH(title, body) AGAINST ('database article');

We can even order articles by the most relevant first to create a simple search engine:
1	SELECT *, MATCH(title, body) AGAINST ('database article') AS rel 
2	FROM mysqltest.articles 
3	ORDER BY rel DESC; */
#2008-08-04 ?>