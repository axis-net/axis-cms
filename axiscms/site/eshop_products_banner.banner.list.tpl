		<article id="id{$id}" class="item product {$_class}">
			<form action="" method="post" name="cc175" id="ostukorvi{$id}">
				<div class="prod_pic"><a href="{$article_url}"><img src="{$file.t1.url}" alt="{$title}" border="0" /></a></div>
				<h2><a href="{$article_url}">{$title}</a></h2>
				<p class="avalehe_hind">{$price.lbl} {$price.vat}</p>
				<p class="avalehe_hind soodus">{$price_discount.lbl}: <ins>{$price_discount.vat}</ins></p>
				<p><a href="{$article_url}" class="add">{$add_to_cart.txt}</a></p>
			</form>
		</article>
