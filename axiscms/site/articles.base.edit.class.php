<?php #2021-11-30
include_once('articles.base.class.php');
if (!class_exists('axs_articles')) {	class axs_articles extends axs_articles_base{}	}
class axs_articles_base_edit extends axs_articles {
	var $key_article='article';
	var $key_edit='edit_article';
	var $key_comments='_comments';
	function __construct(&$e) {
		global $axs, $axs_content;
		$this->e=&$e;
		parent::__construct($axs_content['content'][$axs['c']]);
		$this->e->initialize($this->name.'.edit', dirname(__FILE__).'/', 'en'); #<Initialize editor />
		$this->e->header=&$this->form;
		if ($this->name!=$this->axs_local['plugin']) $this->e->tr->load($this->e->f_path.$this->axs_local['plugin'].'.edit.tr', $this->e->l, false, false); #<Load extending editor's translations />
		//$this->e->tr->set($this->tr);
		//if (isset($axs['editor']->plugin_editor['tr'])) 
		$this->e->plugin_editor['tr']=$this->e->tr;
		$this->url=$this->e->url;
		if ($this->f) $this->url['f']=$this->search->url['f']=$this->f;
		$this->vr['tools']=array();
		$this->reset='?'.$_SERVER['QUERY_STRING'];
		$this->b=(!empty($_REQUEST['b'])) ? $_REQUEST['b']:'?'.axs_url($this->url, array(), false);
		//$this->form=new axs_form_edit($this->site_nr, $this->url, $this->fields_article, $this->dir_content_files, $_POST);
		//$this->form->tr=&$this->e->tr;
		//if ($this->module) {
		//	$this->form->sql_header_get($this->module, $this->f, false);
		//	}
		//else {
		if (!$this->module) {
			$this->f=$this->axs_local['plugin'];
			$this->forms[$this->f]['label']=$this->form->tr->t('form.title');
			$this->forms[$this->f]['label.html']=$this->form->tr->s('form.title');
			}
		$this->form->sql_add['_parent']=$this->url['_parent']=axs_get('_parent', $_GET, 0);
		$this->title=array(1=>array('url'=>'?'.axs_url($this->url), 'title'=>$this->forms[$this->f]['label'], ));
		//$this->thead=$this->form->structure_get_thead();
		$this->labels=$this->form->structure_get_labels('.lbl');
		//$this->search=new axs_form_search($this->form->structure, $this->get, $this->url);
		$this->search->export=true;
		
		$axs['page']['head'][$this->name.'.base.edit.css']='<link type="text/css" rel="stylesheet" href="'.axs_file_choose($this->name.'.base.edit.css', 'http').'" />'."\n";
		$axs['page']['head'][$this->name.'.base.edit.js']='<script src="'.axs_file_choose($this->name.'.base.edit.js', 'http').'"></script>'."\n";
		$this->vr['js']='<script>axs.articles.edit=new axs.articles("list");</script>'."\n";
		} #</__construct()>
	function preview_url($cl) {
		global $axs;
		$url=(isset($cl['title_alias'])) ? array('t'=>$cl['title_alias'], ):array();
		return $axs['http_root'].$axs['site_dir'].'?'.axs_url($url=array('c'=>$this->axs_local['c'], 'l'=>$axs['l'], 'id'=>$cl['id'], ), $url, false);
		} #</preview_url()>
	function tabs() { #<tabs />
		$this->vr['forms']=$this->forms;
		foreach ($this->vr['forms'] as $k=>$v) {
			$this->vr['forms'][$k]=array('label'=>$v['label'], 'url'=>'?'.axs_url($this->url, array('f'=>$k)), 'act'=>$this->f);
			}
		$this->vr['forms']=$this->e->menu_build($this->vr['forms'], ' ', array('class'=>'tabs'));
		$this->vr['form.title']=array();
		foreach ($this->title as $k=>$v) $this->vr['form.title'][$k]='<a href="'.$v['url'].'">'.axs_html_safe($v['title']).'</a>';
		$this->vr['form.title']=implode(' &gt; ', $this->vr['form.title']);
		} #</tabs()>
	#<List articles ----------------------------------------------------------- />
	function list_articles() {
		global $axs;
		if ($this->pager->p>1) $this->e->content_unset();
		$this->tabs();
		#$this->installer_import_folder(0);
		#<Search>
		$this->search->elements('t');
		$this->vr['search']=$this->search->form();
		if (isset($axs['get'][$this->form->key_search])) unset($this->form->sql_add['_parent']);
		#</Search>
		$this->templates();//array(''=>'', 'item'=>'item', 'item.cell'=>'item.cell', ));
		$this->vr['edit.url']=$this->form->form_action_url(array_merge($this->url, array($this->form->key_edit=>'', 'b'=>'?'.$_SERVER['QUERY_STRING'])));
		$this->vr['_id']=0;
		foreach ($this->list_articles_data=$this->list_articles_query() as $cl) {
			$vr=array('tabs'=>'', '_class'=>array(), '_tools'=>array(), '_parent'=>axs_get('_parent', $cl, 0), '_rank'=>$this->form->rank, '_data'=>array(), );
			$this->list_articles_row($cl, $vr);
			//$vr['_class']=implode(' ', (array)$vr['_class']);
			//$vr['_title']=$this->form->structure_get_item_title($cl);
			$this->list_item_header($vr, $cl);
			foreach ($this->form->structure_get_thead() as $k=>$v) {
				//$v=$this->form->structure[$k];
				$vr['_data'][$k]=axs_tpl_parse($this->tpl['item.cell'], array('class'=>$k, 'label'=>$v['label.html'], 'value'=>$vr[$k]));
				}
			$vr['_data']=implode('', $vr['_data']);
			$this->vr['list'].=axs_tpl_parse($this->tpl['item'], $cl+$vr+$this->labels);
			}
		#<Generate pages>
		$this->vr['total']=$this->search->sql_found_rows;
		if ($this->vr['total']>$this->pager->psize) {
			$this->vr['pages']=$this->pager->pages($this->vr['total'], '?'.axs_url($this->url, array('p'=>'')));
			$this->vr['prev']=$this->vr['pages']['prev'];
			$this->vr['current']=$this->vr['pages']['current'];
			$this->vr['next']=$this->vr['pages']['next'];
			$this->vr['pages']=($this->vr['total']>$this->pager->psize) ? $this->vr['pages']['pages']:'';
			}
		else $this->vr['prev']=$this->vr['current']=$this->vr['next']=$this->vr['pages']='';
		#</Generate pages>
		return axs_tpl_parse($this->tpl[''], $this->vr+$this->e->tr->tr);
		} #</list_articles()>
	function list_articles_query($sql=array()) {
		if (is_string($sql)) $q=$sql;
		else {
			if (isset($this->form->structure['comments'])) {
				$select=', COUNT(*) AS post, c.id AS post_id';
				$join='	LEFT JOIN `'.$this->form->sql_table.'_comments` AS c ON c.parent_id=t.id'."\n";
				}
			else $select=$join='';
			$q="SELECT SQL_CALC_FOUND_ROWS t.*"./*$this->form->sql_select('t').*/$select.", `_type`, `_parent`\n".
			"	FROM `".$this->form->sql_table."` AS t\n".
			'	'.$this->form->sql_tables_join('t')."\n".$join.
			"	WHERE 1 ".$this->form->sql_add(' AND ', 't', ' AND ').$this->search->sql_where(' AND ')."\n".
			"	GROUP BY t.`id` ".$this->search->sql_order('ORDER BY ', 't', $this->form->structure)." LIMIT ".$this->pager->start.",".$this->pager->psize;
			}
		$r=axs_db_query($q, 'k', $this->db, __FILE__, __LINE__);
		$this->search->sql_result_set($r);
		#dbg($q,$r);
		return $r;
		} #</list_articles_query()>
	function list_articles_row(&$cl, &$vr=array()) {
		foreach (array('_type'=>'', '_parent'=>0) as $k=>$v) if (!isset($cl[$k])) $cl[$k]=$v;
		$this->_type_set($cl['_type']);
		$this->e->tabs=array(''=>array('url'=>'#id'.$cl['id'], 'label'=>$this->e->tr->t('article.lbl'), ), );
		if ($this->fields_comment) $this->e->tabs['_comments']=array('url'=>'?'.axs_url($this->url, array($this->key_comments=>$cl['id'])), 'label'=>$this->e->tr->t('comments.lbl').' ('.((!empty($cl['post_id'])) ? $cl['post']:0).')', );
		$this->e->tab='';
		if ($cl['_type']) $vr['_class']['_type']=$cl['_type'];
		//foreach ($this->form->structure_get_by_type('checkbox') as $k=>$v) if (!empty($cl[$k])) $vr['_class'][$k]=$k;
		$vr['_tools']['preview']=array('url'=>$this->preview_url($cl), 'label'=>$this->e->tr->t('preview.lbl'), 'attribs'=>array('target'=>'_blank'), );
		foreach ($this->form->structure_get_thead() as $k=>$v) {
			//$v=$this->form->structure[$k];
			$vr[$k]=$vr['_data'][$k]=$this->form->value_display($k, $v, $cl, $vr);	
			}
		if (isset($vr['thumbs'])) $vr['thumbs']=$vr['_data']['thumbs']=$this->list_articles_folder_thumbs($cl);
		$cl['b']='?'.$_SERVER['QUERY_STRING'].'#id'.$cl['id'];
		$vr['add.url']='?'.axs_url($this->url, array($this->form->key_edit=>'', '_parent'=>$cl['_parent'], 'b'=>$cl['b']));
		$vr['edit.url']='?'.axs_url($this->url, array($this->form->key_edit=>$cl['id'], '_parent'=>axs_get('_parent', $cl, 0), 'b'=>$cl['b']));
		$vr['article.url']='?'.axs_url($this->url, array('article'=>$cl['id'], '_parent'=>$cl['_parent'], 'b'=>$cl['b']));
		$vr['title.url']=($cl['_type']==='_folder') ? $vr['article.url']:$vr['edit.url'];
		$vr['_id']=$cl['id'];
		if (isset($vr['id'])) $vr['id']='<a href="'.$vr['edit.url'].'">'.$vr['id'].'</a>';
		if ($this->form->rank) {
			$vr['rank+']=$cl[$this->form->rank]+1;
			$vr['rank-']=$cl[$this->form->rank]-1;
			}
		return $vr;
		} #</list_articles_row()>
	function list_item_header(&$vl, &$cl) {
		$vl['_title']=$this->form->structure_get_item_title($cl);
		foreach ($vl['_tools'] as $k=>$v) {
			$v['attribs']=(!empty($v['attribs'])) ? $this->form->attributes($v['attribs']):'';
			$vl['_tools'][$k]='<a href="'.htmlspecialchars($v['url']).'"'.$v['attribs'].'>'.axs_html_safe($v['label']).'</a>';
			}
		$vl['_tools']=implode(' ', $vl['_tools']);
		$vl['tabs']='';//$this->list_item_tabs($cl);
		//if (count($this->tabs)>1) $vl['_class']['tab']='tab';
		if (is_array($vl['_class'])) $vl['_class']=implode(' ', $vl['_class']);
		} #</list_item_header()>
	#<Open article ----------------------------------------------------------- />
	function display_article_query($id=false) {
		global $axs;
		$select=$join='';
		if (isset($this->form->structure['comments'])) {
			$select=", COUNT(*) AS `post`, c.`id` AS `post_id`";
			$join="LEFT JOIN `".$this->form->sql_table."_comments` AS c ON c.`parent_id`=t.`id`";
			}
		$this->cl=axs_db_query($q="SELECT t.*, m.`user` AS `updated_text`".$select."\n".
		"	FROM `".$this->form->sql_table."` AS t ".$join." LEFT JOIN `".$axs['cfg']['db'][1]['px']."users` AS m ON m.`id`=t.`updated_uid`\n".
		"	WHERE t.`id`='".$this->article."'".$this->form->sql_add(' AND ', 't' , ' AND ')." GROUP BY t.`id`",
		'row', $this->db, __FILE__, __LINE__);
		#dbg($q);
		return $this->cl;
		} #</display_article_query()>
	function display_article_vars(&$cl=array(), &$vr=array()) {
		$this->url[$this->key_article]=$this->article;
		$this->url['b']=$_GET['b'];
		$this->b=($_GET['b']) ? $_GET['b']:'?'.axs_url($this->url, array($this->key_article=>false), false);
		$this->vr['back_url']=htmlspecialchars($this->b);
		$this->display_article_query();
		if (!$this->cl['id']) exit(axs_redir($this->b));
		foreach ($this->form->structure as $k=>$v) $this->vr[$k]=$this->form->value_display($k, $v, $this->cl);
		$this->vr['edit_url']='?'.axs_url($this->url, array($this->form->key_edit=>$this->cl['id'], 'b'=>'?'.$_SERVER['QUERY_STRING'].'#id'.$this->cl['id']));
		$this->vr['preview_url']=$this->preview_url($this->cl);
		$this->vr['post']=($this->cl['post_id']) ? $this->cl['post']:0;
		$this->vr['comments_url']='?'.axs_url($this->url, array('article'=>false, $this->key_comments=>$this->cl['id']));
		foreach (array('back.lbl','edit_article_head.lbl') as $v) $this->vr[$v]=$this->e->tr($v);
		$this->display_article_xtab('text');
		return axs_tpl_parse($this->tpl['article'], $this->vr);
		} #</display_article_vars()>
	function display_article_xtab($col, &$cl=array(), &$vr=array()) {
		global $axs, $axs_user;
		if ((empty($this->form->structure[$col])) or ($this->form->structure[$col]['type']!=='hidden')) return;
		$tab=new axs_tab_edit('xtab', $this->site_nr, $this->dir_content_files.$this->cl['id'].'/', $this->e->l);
		if (isset($_POST['axs_save'])) {
			$this->cl[$col]=$tab->content_edit($this->cl[$col], $_GET, $_POST);
			if (empty($tab->msg)) {
				axs_db_query("UPDATE `".$this->form->sql_table."`\n".
				"	SET `".$col."`='".addslashes($tab->tab_merge($this->cl[$col]))."', `updated`='".$axs['time']."', `updated_uid`='".$axs_user['id']."'\n".
				"	WHERE `id`='".$this->cl['id']."'", '', $this->db, __FILE__, __LINE__);
				$tab->b_redir();
				}
			}
		$this->vr[$col]=$tab->ui($this->cl[$col], $_GET, $_POST, $this->url, html_entity_decode($this->preview_url($this->cl)));
		} #</display_article_xtab()>
	function display_article_comments($col, &$cl=array(), &$vr=array()) {
		$this->e->content_unset();
		$parent_id=$_GET[$this->key_comments]+0;
		$this->url[$this->key_comments]=$parent_id;
		$vr=array('back_url'=>'?'.axs_url($this->url, array($this->key_comments=>false)).'#id'.$parent_id, );
		require_once($this->name.'.base.edit-post.php');
		$editor=new axs_comments($this->site_nr, $this->url, $this->form->tr, $this->name.'_comments', $parent_id);
		$vr['comments']=$editor->ui(25);
		return axs_tpl_parse($this->tpl['comments'], $vr+$this->e->tr);
		} #</display_article_comments()>
	function display_article_parse($id=false) {
		global $axs, $axs_content;
		$this->e->content_unset();
		$cl=$this->display_article_query();
		if (!$cl['id']) {
			axs_log(__FILE__, __LINE__, $this->name, 'Article ID#'.$id.' not found @ '.$this->axs_local['plugin'].'.plugin', true);
			return '<p class="msg">Article ID#'.$id.' not found!</p>';
			}
		if ($cl['_type']==='_folder') {
			$this->level++;
			$this->article_parent=$cl;
			$this->form->sql_add['_parent']=$this->url['_parent']=$id;
			foreach ($this->nav_path_get($cl, 1) as $k=>$v) $this->title[]=array('url'=>'?'.axs_url($this->url, array('article'=>$v['id'], '_parent'=>null)), 'title'=>$v['_title']);
			return $this->list_articles();
			}
		$this->templates(array('article'=>'article', ));
		$this->display_article_vars();
		$this->display_article_xtab('text');
		return axs_tpl_parse($this->tpl['article'], $this->vr+$this->e->tr);
		} #</display_article_parse()>
	
	#<Installer>
	function installer(&$e) {
		global $axs;
		if ($tmp=$this->error()) return $tmp;
		if (!empty($this->form->structure)) {
			if (!isset($this->form->structure['']['sql'])) $this->form->structure['']['sql']=array( #legacy
				'table'=>axs_get('sql_table', $this->form->structure['']), 'attr'=>axs_get('sql_attr', $this->form->structure[''], array()),
				);
			$this->form->structure['']['sql']['cols']=array();
			foreach ($this->form->sql_add as $k=>$v) {	$this->form->structure['']['sql']['cols'][$k]=$this->sql_fields[$k];	}
			if (isset($this->form->structure['']['sql']['cols']['plugin'])) $this->form->structure['']['sql']['cols']['plugin']['_value'][$this->axs_local['plugin']]=$this->axs_local['plugin'];
			$e->installer_sql($this->form->sql_structure($this->form->structure));
			}
		if (!empty($this->folders)) {
			//foreach ($this->form->structure['']['sql'] as $k=>$v) if (!isset($this->fields_folder['']['sql'][$k])) $this->fields_folder['']['sql'][$k]=$v;
			if (!isset($this->fields_folder[''])) $this->fields_folder['']=array();
			$this->fields_folder['']=array_merge_recursive($this->form->structure[''], $this->fields_folder['']);
			$e->installer_sql($this->form->sql_structure($this->fields_folder));
			}
		if (!empty($this->fields_comment)) $e->installer_sql($this->form->sql_structure($this->fields_comment));
		return $this;
		} #</installer()>
	function installer_add(&$e, $c) {
		} #</installer_add()>
	function installer_delete(&$e, $c) {
		if (!$this->db) return;
		if (empty($this->form->sql_add['c'])) return; 
		$q=(empty($this->fields_comment)) ? 
		"DELETE t FROM `".$this->form->sql_table."` AS t ":
		"DELETE t, c\n".
		"	FROM `".$this->form->sql_table."` AS t LEFT JOIN `".$this->form->sql_table."_comments` AS c ON c.`parent_id`=t.`id`\n".
		"	";
		axs_db_query($q."WHERE ".$this->form->sql_add(' AND ', 't'), '', $this->db, __FILE__, __LINE__);
		} #</installer_del()>
	function installer_move(&$e, $old, $new) {
		global $axs;
		if (!$this->db) return;
		$this->installer_replace($e, $axs['dir_c'].$old, $axs['dir_c'].$new);
		} #</installer_move()>
	function installer_rename(&$e, $c, $new) {
		global $axs;
		if (!$this->db) return;
		$this->installer_replace($e, $axs['dir_c'].$this->axs_local['dir'].$c.'_files/', $axs['dir_c'].$this->axs_local['dir'].$new.'_files/');
		axs_db_query("UPDATE `".$this->form->sql_table."` AS t SET t.`c`='".addslashes($new)."' WHERE t.`c`='".$c."'".$this->form->sql_add(' AND ', 't', ' AND '), '', $this->db, __FILE__, __LINE__);
		} #</installer_rename()>
	function installer_replace(&$e, $find, $replace) {
		if (!$this->db) return;
		$cols=array();
		foreach ($this->form->structure_get() as $k=>$v) if ((axs_form_edit::sql_writable($v)) && (($v['type']==='textarea') || ($v['type']==='wysiwyg'))) {
			if (!empty($v['lang-multi'])) foreach ($e->site['langs'] as $kk=>$vv) $cols[$k.'_'.$kk]=$v;
			else $cols[$k]=$v;
			}
		if (empty($cols)) return;
		$r=array($find=>$replace, );
		$r[urlencode($find)]=urlencode($replace);
		foreach ($r as $k=>$v) {
			$sql=array();
			foreach ($cols as $kk=>$vv) $sql[]="t.`".$kk."`=REPLACE(t.`".$kk."`, '".addslashes($k)."', '".addslashes($v)."')"; 
			axs_db_query($q[]="UPDATE `".$this->form->sql_table."` AS t SET ".implode(', ', $sql)." WHERE t.`c`='".$this->axs_local['c']."'".$this->form->sql_add(' AND ', 't', ' AND '), '', $this->db, __FILE__, __LINE__);
			}
		} #</installer_replace()>
	function installer_import_f($field=false, $img_recreate=false, $copy_files='', $start=0, $limit=1000000) {
		unset($this->form->sql_add['_parent']);
		$q="SELECT * FROM `".$this->form->sql_table."` AS t".$this->form->sql_add(' AND ', 't' , '	WHERE ')." LIMIT ".$start.",".$limit;
		$q="SELECT _parent_id, _parent_id AS id, id AS `_key`, _file FROM `".$this->form->sql_table."_table` AS t WHERE _field='files' LIMIT ".$start.",".$limit;
		#exit(dbg($q));
		$r=axs_db_query($q, 1, $this->db, __FILE__, __LINE__);
		foreach ($r as $cl) {
			$this->form->form_save_dir($cl);
			$el=$this->form->element_file_data($field, $this->form->structure['files']['options'][$field], $cl);
			if (($copy_files) && ($cl['_file'])) {
				$tmp='../_tmp/'.$cl['_file'];
				if (!$this->form->fs->exists($tmp)) dbg('File not found: "'.$this->form->fs->d.$tmp.'"');
				else $this->form->fs->copy($tmp, $el['t'][$copy_files]['name']);
				}
			if ($img_recreate) {
				$big=false;
				if ($field) foreach ($this->form->structure[$field]['t'] as $kk=>$vv) $big=$kk;
				if ($big) foreach ((array)$img_recreate as $kk=>$vv) $this->form->fs->img_proc($el['t'][$big]['name'], $el['t'][$vv]['name'], $el['t'][$vv]);
				}
			//if ($dir) $this->form->fs->dir_cd('../');
			}
		} #</installer_import_f()>
	function installer_import_folder($commit=0) {
		global $axs;
		$a=array('c'=>$axs['c'], 'c_p'=>end($this->e->path), 'nr'=>axs_get('nr', $this->e->c_get('', $axs['c'], $axs['l'], true), 0), 'title'=>array(), );
		foreach ($axs['cfg']['site'][$this->e->site_nr]['langs'] as $k=>$v) {
			$v=axs_get('title', $this->e->c_get('', $a['c'], $k, true));
			$a['title'][$k]="title_".$k."='".addslashes($v)."', title_alias_".$k."='".addslashes(strtolower(axs_valid::id($v)))."'";
			}
		$a['parent']=axs_db_query("SELECT id, title_".$axs['l']." FROM `".$this->form->sql_table."` WHERE c='".$a['c_p']."'", 'row', $this->db, __FILE__, __LINE__);
		dbg($a);
		if ($commit) {
			$exists=axs_db_query("SELECT id FROM `".$this->form->sql_table."` WHERE _type='_folder' AND c='".$a['c']."'", 1, $this->db, __FILE__, __LINE__);
			if (!$exists) axs_db_query("INSERT INTO `".$this->form->sql_table."` SET site=1, plugin='eshop_products', _type='_folder', _parent='".axs_get('id', $a['parent'], 0)."', c='".$a['c']."', nr='".$a['nr']."', ".implode(", ", $a['title']).", `updated`='".$axs['time']."'", 1, $this->db, __FILE__, __LINE__);
			}
		} #</installer_import_folder()>
	function installer_import_html($data) {
		$data=explode('<!-- axs_article -->', $data);
		$i=1;
		foreach ($data as $cl) if (strlen($cl)) {
			$this->form->vl=array('title'=>' ', 'summary'=>$cl, );
			if ($this->form->rank) $this->form->vl[$this->form->rank]=$i++;
			//$this->form->form_save_insert_item($this->form->sql_add(', ', '' , ', '));
			}
		} #</installer_import_html()>
	function installer_upgrade_files($commit=false, $t=array(1,2,3)) {
		$path=realpath(axs_dir('content').$this->form->structure['']['dir']);
		dbg($path);
		$objects=new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
		foreach($objects as $n => $object) {
			foreach ($t as $v) if (strpos($n, '_t'.$v.'.')!==false) {
				$tmp=str_replace('_t'.$v.'.', '.t'.$v.'.', $n);
				if ($commit) rename($n, $tmp);
				dbg($n, $tmp);
				}
			/*if (strpos($n, '.t3.')!==false) {
				if ($commit) rename($n, $tmp=str_replace('.t3.', '.t2.', $n));
				dbg($n, $tmp);
				}*/
			}
		} #</installer_upgrade_files()>
	function installer_upgrade_xtab_rem() {
		global $axs;
		$r=axs_db_query($q="SELECT id, text FROM `".$this->form->sql_table."` WHERE text LIKE '<?php #%'", 1, $this->db, __FILE__, __LINE__);
		#dbg(count($r), $q);
		foreach ($r as $cl) axs_db_query($q="UPDATE `".$this->form->sql_table."` SET text='".addslashes(axs_tab::parse($cl['text'], '{$cell1}', array('time'=>$axs['time'])))."' WHERE id='".$cl['id']."'", '', $this->db, __FILE__, __LINE__);
		} #</installer_upgrade_xtab_rem()>
		
	#</Installer>
	#<Edit article ----------------------------------------------------------- />
	function edit_article() {
		global $axs;
		//exit(dbg('a'));
		#$this->installer_upgrade_files(1);
		#$this->installer_import_folder(1);
		//$this->installer_upgrade_albums_f();
		//$this->installer_upgrade_xtab_rem();
		$this->e->content_unset();
		$this->edit_article_vars();
		$this->form->url=array_merge($this->url, $this->form->url, array($this->form->key_edit=>$this->form->vl['id'], 'b'=>$this->b, ));
		$this->form->input_type_text_join_browse();
		
		#<Save article>
		$this->edit_article_add();
		if ($this->form->save_rank) { #<Change article rank />
			$this->form->sql_rank_step($this->b, array_merge($this->form->sql_add, array('_parent'=>$_POST['_'][$this->form->save_rank]['_parent']+0)));
			}
		if ($this->form->save_delete) $this->edit_article_del($this->form->user_input['_select']); #<Delete article />
		if ($this->form->save) $this->form->validate($this->form->vl); #<Error check article />
		if (!empty($this->form->msg)) $this->form->save=false;
		if ($this->form->vl['id']) $this->form->form_save_dir($this->form->vl);
		if ($this->form->save) {
			$this->form->sql_rank_set('', $this->form->rank, $this->form->sql_add, $this->form->vl['id'], axs_get($this->form->rank, $this->form->vl)); #<Change article rank />
			axs_db_query(array('UPDATE "'.$this->form->sql_table.'"'."\n".
			'	SET '.$this->form->sql_values_set($this->form->vl)."\n".
			'	WHERE "id"=\''.$this->form->vl['id'].'\''.$this->form->sql_add(' AND ', '', ' AND '), $this->form->sql_p), '', $this->db, __FILE__, __LINE__);
			$this->form->form_save_tables($this->form->vl);
			$this->form->form_save_files($this->form->vl);
			$this->edit_article_files_qr($this->form->vl);
			if (empty($this->form->msg)) axs_exit(axs_redir($this->b));
			} #</Save article>
			
		#<Read article editor>
		if ((!$this->form->submit) && ($this->form->vl['id'])) {
			$this->form->vl=axs_db_query($q="SELECT ".$this->form->sql_select('t')."\n".//p.*, u.`user` AS `updated_text`\n".
			"	FROM `".$this->form->sql_table."` AS t ".$this->form->sql_tables_join('t')."\n".
			"	WHERE t.`id`='".$this->form->vl['id']."'".$this->form->sql_add(' AND ', 't', ' AND '), 'row', $this->db, __FILE__, __LINE__);
			$this->form->vl=$this->form->sql_values_get($this->form->vl);
			#dbg($q,$this->form->vl);
			}
		$this->form->vr['class']['edit']='edit';
		$this->form->vr['tools']=array(
			'back'=>array('url'=>$this->b, 'label'=>'<'.$this->e->tr->t('back.lbl'), 'class'=>'left', ),
			'reset'=>array('url'=>$this->reset, 'label'=>$this->e->tr->t('reset.lbl'), ),
			);
		return $this->form->form_parse_html();
		#</Read editor>
		} #</edit_article()>
	function edit_article_add() {
		if (!isset($_POST[$this->form->key_add])) return;
		$key=key($_POST[$this->form->key_add]);
		foreach (array('article'=>'', 'folder'=>'_folder', ) as $k=>$v) if (isset($_POST[$this->form->key_add][$key][$k])) $this->form->sql_add['_type']=$v;
		$this->_type_set($this->form->sql_add['_type']);
		$this->form->form_save_add();
		} #</edit_article_add()>
	function edit_article_vars() {
		$cl=array('id'=>(!empty($_GET[$this->form->key_edit])) ? $_GET[$this->form->key_edit]+0:'', '_parent'=>0, '_type'=>'', );
		if ($cl['id']) $cl=array_merge($cl, axs_db_query("SELECT `id`, `_parent`, `_type` FROM `".$this->form->sql_table."` WHERE `id`='".$cl['id']."'".$this->form->sql_add(' AND ', '', ' AND ', $this->form->sql_add), 'row', $this->db, __FILE__, __LINE__));
		$this->_type_set($cl['_type']);
		$this->form->vl=$this->form->form_input($_POST, $_FILES);
		if ($this->form->vl['id']) $this->form->vl=array_merge($this->form->vl, $cl);
		if ($this->form->rank) {
			$this->form->vl[$this->form->rank]=intval($this->form->vl[$this->form->rank]);
			if ($this->form->vl[$this->form->rank]<1) $this->form->vl[$this->form->rank]=1;
			}
		if (isset($this->form->structure['title'])) {
			$tmp=(empty($this->form->structure['title']['lang-multi'])) ? array(''=>''):$this->form->structure['title']['lang-multi'];
			foreach ($tmp as $k=>$v) {
				if ($k) $k='_'.$k;
				$this->form->vl['title'.$k]=trim($this->form->vl['title'.$k]);
				if (isset($this->form->vl['title_alias'.$k])) $this->form->vl['title_alias'.$k]=strtolower(axs_valid::id(($this->form->vl['title_alias'.$k]) ? $this->form->vl['title_alias'.$k]:$this->form->vl['title'.$k]));
				}
			}
		} #</edit_article_vars()>
	function edit_article_del($id, $recursion=0) {
		#<Validate id />
		$id=(array)$id;
		$where=array();
		foreach ($id as $k=>$v) $where[$k]="`id`='".($v+0)."'";
		if ((empty($where)) && (!$recursion)) axs_exit(axs_redir($this->b));
		$parent=$this->form->sql_add['_parent'];
		unset($this->form->sql_add['_parent']);
		foreach (axs_db_query("SELECT * FROM `".$this->form->sql_table."` WHERE (".implode(" OR ", $where).")".$this->form->sql_add(' AND ', '', ' AND '), 1, $this->db, __FILE__, __LINE__) as $cl) {
			#<Delete files />
			if ($tmp=$this->form->dir_entry($cl)) {
				$this->form->fs_init();
				$this->form->fs->dir_rm($tmp, false);
				}
			if (!empty($this->form->msg)) return;
			#<Delete folder />
			if ($cl['_type']==='_folder') {
				$children=axs_db_query("SELECT `id` FROM `".$this->form->sql_table."` WHERE `_parent`='".$cl['id']."'", 'list', $this->db, __FILE__, __LINE__);
				if ($children) $this->{__FUNCTION__}($children, $recursion+1);
				}
			#<Delete from SQL />
			$this->edit_article_del_sql($cl);
			}
		if (!$recursion) {
			$this->form->sql_add['_parent']=$parent;
			$this->form->sql_rank_set('', $this->form->rank, $this->form->sql_add); #<reorder all />
			if (empty($this->form->msg)) axs_exit(axs_redir($this->b));
			}
		} #</edit_article_del()>
	function edit_article_del_sql($cl) {
		if ($this->form->structure_get_by_type('table')) axs_db_query("DELETE t FROM `".$this->form->sql_table."_table` AS t WHERE t.`_parent_id`='".$cl['id']."'", '', $this->db, __FILE__, __LINE__);
		if (isset($this->form->structure['comments'])) axs_db_query("DELETE c FROM `".$this->form->sql_table."_comments` AS c WHERE c.`parent_id`='".$cl['id']."'", '', $this->db, __FILE__, __LINE__);
		axs_db_query("DELETE t FROM `".$this->form->sql_table."` AS t WHERE t.`id`='".$cl['id']."'", '', $this->db, __FILE__, __LINE__);
		} #</edit_article_del_sql()>
	function edit_article_files_qr($cl) {
		foreach ($this->qr_code_files as $file=>$size) if (file_exists($f=$this->form->dir_fs.$cl['id'].'/'.$file)) {
			$this->form->fs->f_del($file, false);
			}
		} #</edit_article_files_qr()>
	function edit_banner() {
		$title='			<h2>'.$this->form->tr->s('form.title').':</h2>'."\n";
		$ul='';
		$c=axs_content::index_c_find(false, $this->name);
		foreach ($c as $k=>$v) {
			foreach ($v['path'] as $kk=>$vv) $v['path'][$kk]=$vv['title'];
			$ul.='			<li><a href="'.'?'.axs_url($this->e->url, array('d'=>$v['dir'], 'c'=>$k)).'">'.axs_html_safe(implode(' > ', $v['path'])).'</a></li>'."\n";
			}
		if ($ul) $ul='		<ul>'."\n".$ul.'		</ul>'."\n";
		return $title.$ul;
		} #</edit_banner()>
	#<Templates ------------------------------------------------------ />
	/*function tpl($get) {
		$tmp=axs_get('layout', $this->form->structure[''], '');
		return $this->tpl[$tmp.$get];
		}*/ #</tpl()>
	function templates($get=array()) {
		$this->tpl=axs_db_edit::templates();
		$this->templates_custom(); #<Set custom templates />
		axs_db_edit::templates_prep($this, $get);
		} #</templates()>
	function templates_custom() {
		//$this->tpl=axs_db_edit::templates();
		$this->tpl['article']=
			'      <fieldset>'."\n".
			'       <legend>'."\n".
			'        <a href="{$back_url}" class="back">&lt;{$back.lbl}</a> - '."\n".
			'        <a href="{$edit_url}" class="edit">{$edit_article_head.lbl}</a> - '."\n".
			'        <a href="{$preview_url}" target="_blank">{$preview.lbl} ({$id_lbl}#{$id})</a>'."\n".
			'       </legend>'."\n".
			'      <h2>{$title}</h2>'."\n".
			'      <p class="file">{$file}</p>'."\n".
			'      <div class="summary">{$summary}</div>'."\n".
			'      </fieldset>'."\n".
			'      &nbsp;'."\n".
			'      {$text}'."\n".
			'      <p><a href="{$comments_url}">{$comments.form.title}({$post})</a></p>'."\n".
			'      <a href="{$back_url}" class="back">&lt;{$back.lbl}</a>'."\n".
			'      <p>{$publ_lbl}:{$publ} | {$arh_lbl}:{$arh} | {$comments_lbl}:{$comments} | {$updated_lbl}:{$updated}</p>';
		$this->tpl['comments']=
			' <div class="comments">'."\n".
			'  <h1>{$comments.form.title}</h1>'."\n".
			'  <p><a href="{$back_url}">&lt;{$back.lbl}</a><p>'."\n".
			'  {$comments}'."\n".
			' </div>';
		} #</templates_custom()>
	function templates_list() {
		$this->layout=axs_get('layout', $this->form->structure[''], 'list');
		return array(''=>'', 'add'=>'add', 'rank'=>'rank', 'data'=>'data.'.$this->layout, 'item'=>'data.'.$this->layout.'.item', 'item.cell'=>'data.'.$this->layout.'.item.cell', );
		} #</templates_list()>
	#<UI>
	function display_article($id=false) {
		global $axs, $axs_content;
		$cl=$this->display_article_query($id);
		if (empty($cl['id'])) {
			if ($this->log_id) axs_log(__FILE__, __LINE__, $this->name, 'Invalid ID @ '.$this->axs_local['plugin'].'.plugin');
			header('HTTP/1.0 404 Not Found');
			return '<p class="msg" lang="en">404 Not Found</p>';
			}
		$this->vr['title']=$cl['_title']=$this->form->structure_get_item_title($cl);
		foreach (array('title_alias'=>'', 'meta_description'=>'', ) as $k=>$v) $cl[$k]=$this->_field_get($cl, $k);
		foreach ($this->nav_path_get($cl, 1) as $k=>$v) $this->nav_path_set($v);
		if ((isset($this->form->structure['meta_description'])) && ($cl['meta_description'])) $axs['page']['description']=axs_html_safe($cl['meta_description']);
		if ($cl['_type']==='_folder') {
			$this->level++;
			$this->article_parent=$cl;
			$this->form->sql_add['_parent']=$id;
			return $this->list_articles();
			}
		$vr=$this->display_article_vars($cl);
		#$cl['text']=$this->display_article_xtab('text');
		#$cl['comments']=$this->display_article_comments('comments');
		return axs_tpl_parse($this->tpl['article'], $vr+$this->form->tr->get());
		} #</display_article()>
	function ui() {
		if ($tmp=$this->error()) return $tmp;
		#<Edit article />
		if (isset($_GET[$this->form->key_edit])) return $this->edit_article();
		#<Open article />
		if ($this->article) return $this->display_article_parse($this->article);
		#<Comments />
		if (isset($_GET[$this->key_comments])) return $this->display_article_comments();
		# <Show articles list />
		return $this->list_articles();
		} #</ui()>
	} #</class::axs_articles_base_edit>
#2008-06 ?>