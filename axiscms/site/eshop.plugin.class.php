<?php #2019-03-13
class axs_eshop_plugin extends axs_eshop {
	function __construct(&$axs_local) {
		global $axs;
		$this->axs_local=&$axs_local;
		//$this->cfg=&axs_eshop::$cfg;
		//$this->tr=&axs_eshop::$tr;
		#<Determine action>
		switch (axs_get('action', $_GET)) {
			case 'checkout':
				$this->action='checkout';
				break;
			case 'img':
				$this->action='product-img';
				break;
			case 'terms':
				$this->action='terms';
				break;
			default: $this->action='cart';
			} #</switch('action')>
		if (isset($_GET['order_nr'])) $this->action='orders';
		if (isset($_GET['bank-return'])) $this->action='bank-return';
		#</Determine action>
		
		#$this->b=(!empty($_REQUEST['b'])) ? $_REQUEST['b']:'?'.$_SERVER['QUERY_STRING'];
		$this->domain=preg_replace('/^www\./', '', $_SERVER['SERVER_NAME']);
		$this->url=$axs['url'];
		#if (isset($_REQUEST['b'])) $this->url['b']=urlencode($_REQUEST['b']);
		$this->vr=array('orders'=>'', 'menu'=>'', 'content'=>'', 'currency.symbol'=>axs_eshop::$cfg['currency.symbol'], )+axs_eshop::$tr->get();
		if (!defined('AXS_LOGINCHK')) axs_user::init();
		} #</__construct()>
	static function delivery_option_get($id, $choice=false) {
		$cart_sum=axs_eshop::cart_sum();
		$option=array('cat'=>0, 'vat'=>0, 'sum'=>0, 'sum.vat'=>0, 'sum+vat'=>0, );
		foreach (self::$cfg['delivery'] as $k=>$v) foreach ($v['options'] as $kk=>$vv) if ($id==$kk) {
			$vv=array_merge($vv, axs_eshop::delivery_cat($vv['sizes']));
			//$vv['cat']=$tmp['cat'];
			$vv['disabled']=($vv['cat']) ? '':' disabled="disabled"';
			foreach (array(''=>'', '.vat'=>'.vat', '+vat'=>'+vat', ) as $kkk=>$vvv) {
				//$vv['sum'.$kkk]=$cl['sum'.$kkk];
				if (($vv['sum_free']>0) && ($cart_sum['sum+vat']>=$vv['sum_free'])) $vv['sum'.$kkk]=0;
				}
			$selected=array('label'=>'', );
			if ($vv['choice']) foreach ($vv['choice'] as $kkk=>$vvv) {
				if ($vvv['sub']) foreach ($vvv['sub'] as $kkkk=>$vvvv) if ($vvvv['selected']) {
					$selected=$vvvv;
					break 2;
					}
				else {
					if (!empty($vvv['selected'])) {
						$selected=$vvv;
						break 1;
						}
					}
				}
			$vv['choice']=$selected;
			$option=$vv;
			break 2;
			}
		return $option;
		} #</delivery_option_get()>
	function delivery_options($input) {
		global $axs;
		//$data=array('table'=>array(), 'groups'=>array(), );
		$result=axs_db_query("SELECT p.*, p.`country_name_".$axs['l']."` AS `country_name`, p.`label_".$axs['l']."` AS `label`, `vat`.`vat`, `units`.`label_".$axs['l']."` AS `unit`, t.`_parent_id`, t.`nr`, t.x, t.y, t.z, t.`weight`, t.`sum`\n".
		"	FROM `".axs_eshop::$px.axs_eshop::$name."` AS p\n".
		"	LEFT JOIN `".axs_eshop::$px."business_vat` AS vat ON vat.`code`=p.`vat_type`\n".
		"	LEFT JOIN `".axs_eshop::$px."business_units` AS units ON units.`id`=p.`unit_type`\n".
		"	LEFT JOIN `".axs_eshop::$px.axs_eshop::$name."_table` AS t ON t.`_parent_id`=p.`id`\n".
		"	WHERE p.`site`='".axs_eshop::$site_nr."' AND !p.`disabled` ORDER BY p.`rank` ASC, t.`nr` ASC", 1, axs_eshop::$db, __FILE__, __LINE__);
		#<Countrys />
		foreach ($result as $cl) if (($cl['country']) && (!isset(axs_eshop::$cfg['delivery'][$cl['country']]))) axs_eshop::$cfg['delivery'][$cl['country']]=array('label'=>$cl['country_name'], 'options'=>array(), );
		array_multisort(axs_eshop::$cfg['delivery'][$cl['country']]);
		#<Options />
		$options=array();
		foreach ($result as $cl) if (!isset($options[$cl['id']])) {
			$options[$cl['id']]=array_merge($cl, array('sizes'=>array(), ));
			}
		#<Options sizes />
		foreach ($result as $cl) if (isset($options[$cl['_parent_id']])) {
			$cl['sum.vat']=$cl['sum']/100*$cl['vat'];
			$cl['sum+vat']=$cl['sum']+$cl['sum.vat'];
			$options[$cl['_parent_id']]['sizes'][$cl['nr']]=array(
				'x'=>floatval($cl['x']), 'y'=>floatval($cl['y']), 'z'=>floatval($cl['z']), 'weight'=>floatval($cl['weight']), 'sum'=>$cl['sum'], 'sum.vat'=>$cl['sum.vat'], 
				'sum+vat'=>$cl['sum+vat'],
				);
			}
		
		foreach ($options as $cl) if (isset(axs_eshop::$cfg['delivery'][$cl['country']])) {
			$cl['selected']=($cl['id'].''===$input['delivery'].'') ? ' checked="checked"':'';
			$cl=array_merge($cl, $this->delivery_cat($cl['sizes']));
			$cl['disabled']=($cl['cat']) ? '':' disabled="disabled"';
			//$cl['delivery_cat']=$tmp['cat'];
			$cl['sum.fmt']=($cl['cat']) ? number_format($cl['sum+vat'], 2, '.', ' '):'';
			$cl['choice']='';
			if ((!$cl['disabled']) && ($cl['function'])) {
				$cl['choice']=($cl['selected']) ? self::delivery_options_choice($cl['id'], $cl['function'], $cl['function_cache'], axs_get($cl['id'], $input['delivery_choice'])):array();
				}
			axs_eshop::$cfg['delivery'][$cl['country']]['options'][$cl['id']]=$cl;
			}
		return axs_eshop::$cfg['delivery'];
		} #</delivery_options()>
	static function delivery_options_choice($parent_id, $function, $function_cache, $selected) {
		global $axs;
		if (($function_cache+86400<$axs['time']) || (isset($_GET['upd']))) self::delivery_options_update($parent_id, $function);
		$data=self::delivery_options_choice_query($parent_id);
		$options=array();
		foreach ($data as $k=>$v) if (!$v['group']) $options[$v['local_id']]=array_merge($v, array('sub'=>array()));
		foreach ($data as $k=>$v) if ($v['group']) {
			$v['selected']=($selected.''===$v['local_id'].'') ? ' selected="selected"':'';
			$options[$v['group']]['sub'][$v['local_id']]=$v;
			}
		return $options;
		} #</delivery_options_choice()>
	static function delivery_options_choice_query($parent_id, $group=false) {
		$where=($group) ? " AND `group`=0":'';
		$data=axs_db_query("SELECT * FROM `".axs_eshop::$px.axs_eshop::$name."_delivery_options` WHERE `parent_id`='".intval($parent_id)."'".$where." ORDER BY `label` ASC", 1, axs_eshop::$db, __FILE__, __LINE__);
		if ($group) foreach ($data as $k=>$v) {
			if (!$v['group']) $data[$v['label']]=$v;
			unset($data[$k]);
			}
		return $data;
		} #</delivery_options_choice_query()>
	static function delivery_options_update($parent_id, $function) {
		global $axs;
		$t=axs_eshop::$px.axs_eshop::$name.'_delivery_options';
		#Empty tmp table
		axs_db_query("TRUNCATE TABLE `".$t."_tmp`", '', axs_eshop::$db, __FILE__, __LINE__);
		require_once('eshop.plugin.class.delivery.'.$function.'.php');
		$tmp='axs_eshop_delivery_'.$function;
		$o=new $tmp($parent_id);
		//return;
		if (!$o->table) return false;
		foreach ($o->table as $cl) {
			foreach (array('local_id'=>'', 'group'=>'', 'label'=>'') as $k=>$v) $cl[$k]="`".$k."`='".addslashes($cl[$k])."'";
			axs_db_query("INSERT INTO `".$t."_tmp` SET `parent_id`='".intval($parent_id)."', ".implode(', ', $cl), '', axs_eshop::$db, __FILE__, __LINE__);
			}
		#Update existing rows
		axs_db_query("UPDATE `".$t."` AS t, `".$t."_tmp` AS `tmp` SET t.`group`=`tmp`.`group`, t.`label`=`tmp`.`label` WHERE t.`parent_id`='".intval($parent_id)."' AND t.`local_id`=`tmp`.`local_id`", '', axs_eshop::$db, __FILE__, __LINE__);
		#Delete rows that are not present in new data
		axs_db_query("DELETE t FROM  `".$t."` AS t LEFT JOIN `".$t."_tmp` AS `tmp` ON t.`local_id`=`tmp`.`local_id`".
		"	WHERE t.`parent_id`='".intval($parent_id)."' AND `tmp`.`local_id` IS NULL", '', axs_eshop::$db, __FILE__, __LINE__);
		#Insert new rows
		axs_db_query("INSERT INTO `".$t."` (`parent_id`, `local_id`, `group`, `label`)\n".
		"	SELECT `tmp`.`parent_id`, `tmp`.`local_id`, `tmp`.`group`, `tmp`.`label`\n".
		"		FROM `".$t."_tmp` AS `tmp` LEFT JOIN (SELECT * FROM `".$t."` WHERE `parent_id`='".intval($parent_id)."') AS t ON t.`local_id`=`tmp`.`local_id`".
		"		WHERE t.`local_id` IS NULL", '', axs_eshop::$db, __FILE__, __LINE__);
		#Empty tmp table
		axs_db_query("TRUNCATE TABLE `".$t."_tmp`", '', axs_eshop::$db, __FILE__, __LINE__);
		#Mark cache updte time
		axs_db_query("UPDATE `".axs_eshop::$px.axs_eshop::$name."` SET `function_cache`='".$axs['time']."' WHERE `id`='".intval($parent_id)."'", '', axs_eshop::$db, __FILE__, __LINE__);
		return true;
		} #</delivery_options_update()>
	function invoice_create($order, $reply, &$error) { # compile invoice and mail it
		global $axs;
		# get/validate order data
		$order_data=axs_business_orders::order_get(axs_eshop::$site_nr, $axs['l'], $order['id'], $order['time'], axs_eshop::$tr->filter(array('checkout.', 'orders.')));
		# don't allow multiple invoices for one order
		if (!empty($order_data['header']['invoice_id'])) $error[]='Banklink duplicate confirm request';
		else {
			foreach (array('id', 'time') as $k=>$v) $order[$v]=$order_data['header'][$v];
			$header=array(
				'time'=>$axs['time'], 'type'=>'debt', 'profile_id'=>$order_data['header']['profile_id'],
				'client_id'=>false, 'client_name'=>false, 'client_addr'=>false, 'client_postcode'=>false,
				'payment_method'=>$reply['bank_id'], 'currency'=>false, 'transaction_id'=>'', 'revenue'=>$reply['sum'],
				//'compiler_id'=>'', 'compiler_name'=>'',
				'order_id'=>$order['id'], 'order_time'=>$order['time'], 'status'=>false,
				'updated'=>$axs['time'], 'updated_uid'=>axs_user::get('id'),
				);
			$rows=$id_rows=array();
			foreach ($order_data['rows'] as $k=>$v) {
				if ($v['product_form']==='eshop' && $v['price']===0) continue; #<Don't add empty options to invoice />
				$rows[$v['id']]=array(
					'img'=>$v['img'], 'nr'=>'', 'product_id'=>'', 'product_form'=>'', 'text'=>'', 'price'=>'', 'vat'=>'',
					'amount'=>'', 'amount_unit'=>'', 'updated'=>$axs['time'], 'updated_uid'=>axs_user::get('id'),
					);
				$id_rows[$v['id']]=$v['id'];
				}
			$i=new axs_business_invoices(axs_eshop::$site_nr, $axs['l']);
			$order['invoice_id']=$i->save(0, $header, $rows, array('table'=>'business_orders', 'id'=>$order['id'], 'rows'=>$id_rows, ));
			axs_db_query("UPDATE `".axs_eshop::$px."business_orders` SET `invoice_id`='".$order['invoice_id']."', `payment_method`='".$reply['bank_id']."', `revenue`='".$reply['sum']."'\n".
			"	WHERE `id`='".$order['id']."' AND `time`='".$order['time']."'", '', axs_eshop::$site_nr, __FILE__, __LINE__);
			# update product tables
			foreach ($order_data['rows'] as $k=>$v) self::load_product_form($v['product_form'], $v);
			# remove open order from session
			unset($_SESSION['eshop']['orders'][$order['id']]);
			$i_data=$i->invoice_get($order['invoice_id']);
			# notify about successful transfer
			$mail=(axs_eshop::$cfg['notify_email_banklink']) ? array(axs_eshop::$cfg['notify_email_banklink']):array();
			$mail[]=$order_data['header']['client_email'];
			foreach ($mail as $v) $this->mail($i, $i_data, $v, axs_eshop::$tr->t('msg_banklink_ok'), axs_eshop::$tr->t('msg_banklink_ok')."\n");
			}
		} #</invoice_create()>
	static function load_product_form($form, $cl) {
		global $axs;
		if (file_exists($tmp=$form.'.eshop.ext.php')) return include($tmp);
		} #</load_product_form()>
	function mail($o, $data, $to, $subject='', $txt='', $attach=true) {
		if ($attach) $attach=array($o->doc_output_filename(axs_eshop::$tr->t('orders.'.$o->module.'_id.lbl'), $data['nr'], 'pdf')=>array(
			'content_type'=>'application/pdf',
			'content'=>$o->doc_output($data, 'pdf'),
			));
		axs_mail($to, $this->domain.': '.$subject.' '.$data['nr'], $txt.$o->doc_output($data, 'txt'), '', $this->domain, $to, $attach);
		return $o->module;
		} #</mail()>
	
	/**
	 * xml2array() will convert the given XML text to an array in the XML structure.
	 * Link: http://www.bin-co.com/php/scripts/xml2array/
	 * Arguments : $contents - The XML text
	 *                $get_attributes - 1 or 0. If this is 1 the function will get the attributes as well as the tag values - this results in a different array structure in the return value.
	 *                $priority - Can be 'tag' or 'attribute'. This will change the way the resulting array sturcture. For 'tag', the tags are given more importance.
	 * Return: The parsed XML in an array form. Use print_r() to see the resulting array structure.
	 * Examples: $array =  xml2array(file_get_contents('feed.xml'));
	 *              $array =  xml2array(file_get_contents('feed.xml', 1, 'attribute'));
	 */
	static function xml2array($contents, $get_attributes=1, $priority = 'tag') {
		if(!$contents) return array();

		if(!function_exists('xml_parser_create')) {
			//print "'xml_parser_create()' function not found!";
			return array();
		}

		//Get the XML parser of PHP - PHP must have this module for the parser to work
		$parser = xml_parser_create('');
		xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, "UTF-8"); # http://minutillo.com/steve/weblog/2004/6/17/php-xml-and-character-encodings-a-tale-of-sadness-rage-and-data-loss
		xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
		xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
		xml_parse_into_struct($parser, trim($contents), $xml_values);
		xml_parser_free($parser);

		if(!$xml_values) return;//Hmm...

		//Initializations
		$xml_array = array();
		$parents = array();
		$opened_tags = array();
		$arr = array();

		$current = &$xml_array; //Refference

		//Go through the tags.
		$repeated_tag_index = array();//Multiple tags with same name will be turned into an array
		foreach($xml_values as $data) {
			unset($attributes,$value);//Remove existing values, or there will be trouble

			//This command will extract these variables into the foreach scope
			// tag(string), type(string), level(int), attributes(array).
			extract($data);//We could use the array by itself, but this cooler.

			$result = array();
			$attributes_data = array();
			
			if(isset($value)) {
				if($priority == 'tag') $result = $value;
				else $result['value'] = $value; //Put the value in a assoc array if we are in the 'Attribute' mode
			}

			//Set the attributes too.
			if(isset($attributes) and $get_attributes) {
				foreach($attributes as $attr => $val) {
					if($priority == 'tag') $attributes_data[$attr] = $val;
					else $result['attr'][$attr] = $val; //Set all the attributes in a array called 'attr'
				}
			}

			//See tag status and do the needed.
			if($type == "open") {//The starting of the tag '<tag>'
				$parent[$level-1] = &$current;
				if(!is_array($current) or (!in_array($tag, array_keys($current)))) { //Insert New tag
					$current[$tag] = $result;
					if($attributes_data) $current[$tag. '_attr'] = $attributes_data;
					$repeated_tag_index[$tag.'_'.$level] = 1;

					$current = &$current[$tag];

				} else { //There was another element with the same tag name

					if(isset($current[$tag][0])) {//If there is a 0th element it is already an array
						$current[$tag][$repeated_tag_index[$tag.'_'.$level]] = $result;
						$repeated_tag_index[$tag.'_'.$level]++;
					} else {//This section will make the value an array if multiple tags with the same name appear together
						$current[$tag] = array($current[$tag],$result);//This will combine the existing item and the new item together to make an array
						$repeated_tag_index[$tag.'_'.$level] = 2;
						
						if(isset($current[$tag.'_attr'])) { //The attribute of the last(0th) tag must be moved as well
							$current[$tag]['0_attr'] = $current[$tag.'_attr'];
							unset($current[$tag.'_attr']);
						}

					}
					$last_item_index = $repeated_tag_index[$tag.'_'.$level]-1;
					$current = &$current[$tag][$last_item_index];
				}

			} elseif($type == "complete") { //Tags that ends in 1 line '<tag />'
				//See if the key is already taken.
				if(!isset($current[$tag])) { //New Key
					$current[$tag] = $result;
					$repeated_tag_index[$tag.'_'.$level] = 1;
					if($priority == 'tag' and $attributes_data) $current[$tag. '_attr'] = $attributes_data;

				} else { //If taken, put all things inside a list(array)
					if(isset($current[$tag][0]) and is_array($current[$tag])) {//If it is already an array...

						// ...push the new element into that array.
						$current[$tag][$repeated_tag_index[$tag.'_'.$level]] = $result;
						
						if($priority == 'tag' and $get_attributes and $attributes_data) {
							$current[$tag][$repeated_tag_index[$tag.'_'.$level] . '_attr'] = $attributes_data;
						}
						$repeated_tag_index[$tag.'_'.$level]++;

					} else { //If it is not an array...
						$current[$tag] = array($current[$tag],$result); //...Make it an array using using the existing value and the new value
						$repeated_tag_index[$tag.'_'.$level] = 1;
						if($priority == 'tag' and $get_attributes) {
							if(isset($current[$tag.'_attr'])) { //The attribute of the last(0th) tag must be moved as well
								
								$current[$tag]['0_attr'] = $current[$tag.'_attr'];
								unset($current[$tag.'_attr']);
							}
							
							if($attributes_data) {
								$current[$tag][$repeated_tag_index[$tag.'_'.$level] . '_attr'] = $attributes_data;
							}
						}
						$repeated_tag_index[$tag.'_'.$level]++; //0 and 1 index is already taken
					}
				}

			} elseif($type == 'close') { //End of tag '</tag>'
				$current = &$parent[$level-1];
			}
		}
		
		return($xml_array);
		}
	static function product_img($row_id) { #<display product image only if logged in>
		$row_id+=0;
		if (!defined('AXS_LOGINCHK')) axs_user::init();
		if ((AXS_LOGINCHK===1) && (isset($_SESSION['eshop']['order_files'][$row_id]))) $f=$_SESSION['eshop']['order_files'][$row_id];
		else $f=AXS_PATH_CMS_HTTP.'gfx/blank.jpg';
		header('Content-type: image/jpeg');
		exit(readfile($f));
		} #</product_img()>
	}#</class::axs_eshop_plugin>
#2015-07-07 ?>