				<article class="item clear-left {$_class}">
					<h3>{$title}</h3>
					<div>
						{$file.t1}
						{$summary}
						<a class="link" href="{$article_url}">{$read_more.lbl}</a>
					</div>
				</article>