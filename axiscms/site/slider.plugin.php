<?php #2017-01-16 utf-8
$plugin_def=array(); # plugin default data
$plugin_def['name']['et']='Slideshow';
$plugin_def['title']['']='';
$plugin_def['help']['']='';
$plugin_def['mkdir']='files';
$plugin_def['content']['']='<?php #|'.$axs['time'].',,'.axs_get('user', $axs_user).',,|||?>'."\n";
//$plugin_def['tr']['et']=array('config-speed.lbl'=>
$plugin_def['form']=array(
	''=>array(
		'type'=>array('type'=>'select', 'size'=>15, 'value'=>'fade', 'label'=>array('en'=>'effect', 'et'=>'efekt', ), 'options'=>array(
			'fade'=>array('value'=>'fade', 'label'=>array('en'=>'fade', 'et'=>'ülesulamine'), ),
			#'slider'=>array('value'=>'slider', 'label'=>array('en'=>'slide', 'et'=>'horisontaalne kerimine'), ),
			''=>array('value'=>'', 'label'=>array('en'=>'-', 'et'=>'-'), ),
			)),
		'interval'=>array('type'=>'number', 'size'=>5, 'value'=>'3.0', 'label'=>array('en'=>'slide speed (s)', 'et'=>'slaidide vaheldumise kiirus (s)', ), ),
		'transition'=>array('type'=>'number', 'size'=>5, 'value'=>'2.5', 'label'=>array('en'=>'transition speed (s)', 'et'=>'ülemineku kestvus (s)', ), ),
		'random'=>array('type'=>'checkbox', 'size'=>1, 'value'=>0, 'label'=>array('en'=>'random order', 'et'=>'juhuslik järjekord', ), ),
		'locked'=>true,
		),
	);
if ($tmp=axs_file_choose(str_replace('.plugin.php', '.config.php', basename(__FILE__)))) include($tmp);

if (defined('AXS_SITE_NR')) {
$cfg=array();
foreach (array('type'=>'','interval'=>0,'transition'=>0,'random'=>0) as $k=>$v) {
	$tmp=axs_get($k, $axs_content['content'][$axs_local['c']]['cfg']);
	$cfg[$k]=(is_numeric($v)) ? +floatval($tmp):$tmp;
	}
$axs['page']['head']['axs.slider.css']='<link type="text/css" href="'.axs_dir('lib.js', 'http').'axs.slider.css" rel="stylesheet" media="screen" />'."\n";
$axs['page']['head']['axs.slider.js']='<script src="'.axs_dir('lib.js', 'http').'axs.slider.js"></script>'."\n";
if ($cfg['type']) $axs['page']['head']['axs.slider.'.$cfg['type'].'.js']='<script src="'.axs_dir('lib.js', 'http').'axs.slider.'.$cfg['type'].'.js"></script>'."\n";
$data=array(
	'cfg'=>','.json_encode($cfg),
	#'effect'=>$cfg['effect'],
	'title'=>$axs_local['title'],
	'url'=>'?'.axs_url($axs['url'], array('c'=>$axs_local['c'], )),
	'list'=>'',
	'list.files'=>'',
	'pager'=>'',
	'text'=>include('deflt.plugin.php'),
	'c'=>$axs_local['c'],
	);
$tpl=array(''=>axs_tpl(false, $axs_local['plugin'].'.tpl'), 'item'=>axs_tpl(false, $axs_local['plugin'].'.item.tpl'), 'pager'=>axs_tpl(false, $axs_local['plugin'].'.pager.tpl'), );

#<text />
$text=axs_content::text_get();
$text=axs_tab::proc($text, array('header'=>'cell', 'time'=>$axs['time'], ));
if (axs_tpl_has($tpl[''], 'list')) {
	foreach ($text as $k=>$v) $data['list'].=axs_tpl_parse($tpl['item'], $v);
	}
#<pager />
if (axs_tpl_has($tpl[''], 'pager')) {
	foreach ($text as $k=>$v) $data['pager'].=axs_tpl_parse($tpl['pager'], array_merge(array('nr'=>$k, ), $v));
	}
#<files />
if (axs_tpl_has($tpl[''], 'list.files')) {
	$files=array();
	$dir=axs_dir('content').$axs_local['dir'].$axs_local['c'].'_files/';
	$dir_http=axs_dir('content', 'http').$axs_local['dir'].$axs_local['c'].'_files/';
	if (is_dir($dir)) foreach (new DirectoryIterator($dir) as $v) {
		if(($v->isFile()) && (in_array(strtolower(pathinfo($f=$v->getFilename(), PATHINFO_EXTENSION)), array( 'gif', 'jpg', 'jpeg', 'png', 'wbmp', 'xbm')))) $files[]=$f;
		}
	sort($files);
	foreach ($files as $k=>$v) {
		$files[$k]=array('file'=>htmlspecialchars($dir_http.$v), 'img'=>'<img src="'.htmlspecialchars($dir_http.$v).'" alt="'.htmlspecialchars($v).'" />', );
		$data['list.files'].=axs_tpl_parse($tpl['item'], $v);
		}
	}
return axs_tpl_parse(axs_tpl(false, $axs_local['plugin'].'.tpl'), $data);
}
#2011-10-31 ?>