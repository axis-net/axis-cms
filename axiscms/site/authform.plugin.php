<?php #2021-02-01
$plugin_def=array(); # <plugin default data />
$plugin_def['name']['et']='Kasutaja login vorm';
$plugin_def['title']['et']='Kasutaja login';
$plugin_def['help']['']='';
$plugin_def['mkdir']='';
$plugin_def['tr']['en']=array(
	'data.lbl'=>'user profile',
	'user_login.lbl'=>'LogIn',
	'user.lbl'=>'e-mail',
	'pass.lbl'=>'password',
	'user_remember.lbl'=>'remember me',
	'pass_send.lbl'=>'Forgot password? Send password',
	'submit.lbl'=>'submit',
	'logout.lbl'=>'logout',
	'msg_failed'=>'Login failed!',
	'msg_denied'=>'Access denied!',
	'msg_pass_send_fail'=>'User not found.',
	'msg_pass_send_ok'=>'Password has been sent to user&#39;s e-mail address.',
	'user_edit.lbl'=>'my profile',
	'fstname.lbl'=>'first name',
	'lstname.lbl'=>'last name',
	'email.lbl'=>'e-mail 2',
	'phone.lbl'=>'phone',
	'user_data.lbl'=>'your user account details',
	'user_save.lbl'=>'save',
	
	'msg.user_invalid'=>'Invalid user/e-mail!',
	'msg.user_exists'=>'This user/e-mail is already registered. Please use the link "Forgot password?" on the login form!',
	'msg.pass_invalid'=>'Invalid password!',
	'msg.pass_short'=>'Password must be at least {$min} characters long!',
	'msg.pass_repeat'=>'Passwords do not match!',
	'msg.email'=>'Invalid e-mail 2 (please use a comma to separate multiple e-mail addresses)!',
	
	'msg_value_required'=>'Input is required!',
	'msg_value_invalid'=>'Invalid value!',
	'msg_value_unique'=>'Value must be unique!',
	);
$plugin_def['tr']['et']=array(
	'data.lbl'=>'kasutaja andmed',
	'user_login.lbl'=>'logi sisse',
	'user.lbl'=>'e-post',
	'pass.lbl'=>'parool',
	'user_remember.lbl'=>'pea mind meeles',
	'pass_send.lbl'=>'Unustasid parooli? Saada parool',
	'submit.lbl'=>'edasi',
	'logout.lbl'=>'logi välja',
	'msg_failed'=>'Sisselogimine ebaõnnestus!',
	'msg_denied'=>'Ligipääs piiratud!',
	'msg_pass_send_fail'=>'Sellist kasutajat ei leitud.',
	'msg_pass_send_ok'=>'Parool saadetud kasutaja e-posti aadressile.',
	'user_edit.lbl'=>'minu profiil',
	'fstname.lbl'=>'eesnimi',
	'lstname.lbl'=>'perenimi',
	'email.lbl'=>'e-post 2',
	'phone.lbl'=>'telefon',
	'user_data.lbl'=>'teie kasutajakonto andmed',
	'user_save.lbl'=>'salvesta',
	
	'msg.user_invalid'=>'Kasutaja/e-post ei ole korrektne!',
	'msg.user_exists'=>'Selline Kasutaja/e-post on juba registreeritud! Palun kasutage login vormil olevat linki "Unustasid parooli?"',
	'msg.pass_invalid'=>'Parool sisaldab keelatud sümboleid!',
	'msg.pass_short'=>'Parooli pikkus peab olema vähemalt {$min} tähemärki!',
	'msg.pass_repeat'=>'Paroolid ei klapi!',
	'msg.email'=>'E-post 2 sisaldab keelatud sümboleid (mitme aadressi puhul kasutage eraldajana koma)!',
	
	'msg_value_required'=>'Välja täitmine on kohustuslik.',
	'msg_value_invalid'=>'Sisestatud väärtus ei ole sobiv.',
	'msg_value_unique'=>'Väärtus peab olema kordumatu.',
	);
$plugin_def['config']=array('usr_format'=>'auto', 'pwd_len'=>4, );
if (file_exists($tmp=axs_dir('site').'authform.config.php')) include_once($tmp);

if (defined('AXS_SITE_NR')) {
if (($axs_local['section']===$axs_local['c']) && (!defined('AXS_LOGINCHK'))) axs_user::init();
	
$vr=axs_content::tr_get($plugin_def, __FILE__, __LINE__, 'index-login', '')->get();
$vr['section']=$axs_local['section'];

if ($axs_local['section']==='content') {
	if (axs_get('s', $_GET)==='profile') {
		if (!defined('AXS_LOGINCHK')) axs_user::init();
		if (AXS_LOGINCHK===1) {
			//if (!class_exists('axs_user_register')) {	class axs_user_register extends axs_users_edit {}	}
			$o=new axs_users_edit(
				'',
				axs_content::tr_get($plugin_def, __FILE__, __LINE__),
				$plugin_def['config']['usr_format'],
				array('data'=>'')
				);
			$o->url['s']='profile';
			$o->_customize($plugin_def);
			return $o->editor(axs_user::get('user'), axs_user::get('id'), $o->form->user_input, '', false);
			}
		}
	else {
		$tpl=array('login');
		$vr['msg']=(isset($axs['msg']['axs_login'])) ? '<ul class="msg"><li>'.$vr['msg_'.$axs['msg']['axs_login']].'</li></ul>':'';
		$vr['user']=htmlspecialchars((isset($_POST['axs']['user'])) ? $_POST['axs']['user']:axs_get('axs_user_remember', $_COOKIE));
		$vr['user_remember']=($vr['user']) ? ' checked="checked"':'';
		$vr['pass_send']=($axs['mail()']) ? '':' disabled="disabled"';
		}
	}

# <if no group restrictions and is password, make password form>
if ((!$axs_content['content'][$axs['c']]['restrict']) && ($axs_content['content'][$axs['c']]['pw'])) {
	$tpl=array('pw');
	$vr['msg']='';
	} # </if no group restrictions and is password, make password form>
else { # <else make login form>
	if ((defined('AXS_LOGINCHK')) && (AXS_LOGINCHK===1)) {
		$tpl=array('logout');
		$vr['c']=($axs_local['plugin']!=='authform') ? $axs_local['c']:'index-login';
		$vr['user_edit.url']='?'.axs_url($axs['url'], array('c'=>$vr['c'], 's'=>'profile'));
		$vr['user_edit.lbl']=axs_html_safe($vr['user_edit.lbl']);
		$vr['logout.url']=$axs['http_root'].'?l='.$axs['l'].'&amp;'.urlencode('axs[login][out]').'='.urlencode('.');
		$vr['logout.lbl']=axs_html_safe($vr['logout.lbl']);
		$vr['fstname']=axs_html_safe($axs_user['fstname']);
		$vr['lstname']=axs_html_safe($axs_user['lstname']);
		$vr['user']=axs_html_safe($axs_user['user']);
		$vr['keepalive.url']=$axs['http_root'].$axs['site_dir'].'?'.axs_url($axs['url'], array('axs'=>array('login'=>array('keepalive'=>0))));
		}
	else {
		$tpl=($axs_local['section']==='content') ? array('login'):array('login.banner', 'login');
		$vr['msg']=(isset($axs['msg']['axs_login'])) ? '<ul class="msg"><li>'.$vr['msg_'.$axs['msg']['axs_login']].'</li></ul>':'';
		$vr['user']=htmlspecialchars((isset($_POST['axs']['user'])) ? $_POST['axs']['user']:axs_get('axs_user_remember', $_COOKIE));
		$vr['user_remember']=($vr['user']) ? ' checked="checked"':'';
		$vr['pass_send']=($axs['mail()']) ? '':' disabled="disabled"';
		}
	} # </else make login form>
$vr['keepalive']=$axs['http_root'].$axs['site_dir'].'?'.axs_url($axs['url'], array('axs'=>array('login'=>array('keepalive'=>1))));
$vr['url_formact']=htmlspecialchars($axs['http'].'://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']);
$vr['content']=include('deflt.plugin.php');
foreach ($tpl as $k=>$v) $tpl[$k]='authform.'.$v.'.tpl';
return axs_tpl_parse(axs_tpl(false, $tpl), $vr);
}
#2007-08 ?>