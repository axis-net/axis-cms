<?php #2017-04-20
require_once(axs_dir('site_base').'articles.base.class.php');

class axs_articles_events extends axs_articles_base {
	var $sql_limit=50;
	var $sql_limit_banner=50;
	var $date_new=86400;
	var $qr_code=array('title'=>"\n", 'publ'=>' ', 'time'=>"\n", );
	var $fields_article=array(
		''=>array('dir_entry'=>'{$id}/', 'layout'=>'list', 'sql'=>array('table'=>'{$px}articles_events', 'attr'=>array('COMMENT'=>'AXIS CMS plugin articles_events'))),
		'article_head'=>array('type'=>'fieldset', 'size'=>0, 'required'=>0, ),
		'id'=>array('type'=>'number', 'size'=>10, 'required'=>0, 'readonly'=>'readonly', 'thead'=>0, 'search'=>1, 'sql'=>array(
			'_type'=>'int', '_value'=>10, '_attribute'=>'unsigned', 'Key'=>'PRI', 'Extra'=>'AUTO_INCREMENT', 
			), ),
		'title'=>array('type'=>'text', 'size'=>50, 'required'=>1, 'maxlength'=>255, 'thead'=>0, 'search'=>1, 'title_item'=>1, ),
		'promote'=>array('type'=>'checkbox', 'size'=>1, 'required'=>0, 'value'=>1, 'thead'=>1, 'search'=>1, ),
		'hide_home'=>array('type'=>'checkbox', 'size'=>1, 'required'=>0, 'value'=>1, 'thead'=>1, 'search'=>1, ),
		'publ'=>array('type'=>'timestamp-between', 'size'=>10, 'required'=>1, 'format'=>'d.m.Y', 'thead'=>1, 'DESC'=>'DESC', ),
		'times'=>array('type'=>'table', 'size'=>10, 'required'=>1, 'thead'=>1, 'options'=>array(
			'date'=>array('type'=>'timestamp', 'size'=>10, 'required'=>1, 'format'=>'d.m.Y', 'thead'=>1, 'ASC'=>'ASC', ),
			'time'=>array('type'=>'timestamp-between', 'size'=>10, 'required'=>0, 'format'=>'H:i', 'ASC'=>'ASC', 'thead'=>1, ),
			'_add'=>array('type'=>'submit', 'add_fn'=>true, ),
			'_del'=>array('type'=>'checkbox', 'add_fn'=>true, 'thead'=>0, ),
			), ),
		'file'=>array('type'=>'file', 'size'=>0, 'required'=>0, 'accept'=>'image.*', 'thead'=>1, 'add_fn'=>true, 't'=>array(
			't1'=>array('w'=>400, 'h'=>400, 'type'=>'jpg', 'q'=>90, 'name_base'=>'{$id}', 'alt'=>'{$id}.t1.{$ext}', 'dw'=>80, 'dh'=>80, 'link'=>array('c'=>'{$c}', 't'=>'{$title_alias}', 'id'=>'{$id}'), 'link_target'=>'', ),
			't2'=>array('w'=>2000, 'h'=>2000, 'type'=>'jpg', 'q'=>90, 'name_base'=>'{$id}', 'alt'=>'{$id}.t2.{$ext}', 'dw'=>200, 'dh'=>200, 'link'=>'t2', 'link_target'=>'_blank', ),
			), ),
		'summary'=>array('type'=>'wysiwyg', 'size'=>75, 'required'=>0, 'rows'=>2, 'thead'=>1, ),
		'article_head_end'=>array('type'=>'fieldset_end', 'size'=>0, 'required'=>0, ),
		'text'=>array('type'=>'wysiwyg', 'size'=>75, 'required'=>0, ),
		'seo'=>array('type'=>'fieldset', 'size'=>0, 'required'=>0, ),
		'title_alias'=>array('type'=>'text', 'size'=>50, 'required'=>0, 'maxlength'=>255, 'sql'=>array(
			'_charset'=>'ASCII',
			) ),
		'meta_description'=>array('type'=>'text', 'size'=>50, 'required'=>0, 'maxlength'=>255, ),
		'seo_end'=>array('type'=>'fieldset_end', 'size'=>0, 'required'=>0, ),
		'comments'=>array('type'=>'checkbox', 'size'=>1, 'required'=>0, 'value'=>1, 'thead'=>1, 'search'=>1, 'sql'=>array(
			'_type'=>'tinyint', '_value'=>3, '_attribute'=>'UNSIGNED',
			) ),
		'comments_mail'=>array('type'=>'email', 'size'=>20, 'required'=>0, 'maxlength'=>255, 'multiple'=>'multiple', ),
		'updated'=>array('type'=>'timestamp-updated', 'size'=>50, 'required'=>0, 'thead'=>0, 'sort'=>1, 'DESC'=>'DESC', 'add_fn'=>true, ),
		'save'=>array('type'=>'submit', 'size'=>0, 'required'=>0, 'add_fn'=>true, ),
		);
	function __construct(&$axs_local) {
		if (defined('AXS_SITE_NR')) foreach (array('date', 'time') as $v) {
			$this->fields_article[$v]=$this->fields_article['times']['options'][$v];
			$this->fields_article[$v]['size']=0;
			unset($this->fields_article[$v]['ASC'], $this->fields_article[$v]['DESC']);
			}
		axs_articles_base::__construct($axs_local);
		//$this->axs_local['plugin']=$this->plugin='articles_events';
		$this->arh=(isset($_GET['arh'])) ? 1:'';
		if ($this->arh) $this->url['arh']=$this->arh;
		} #</__construct()>
	function banner_init() {
		global $axs, $axs_content;
		axs_articles_base::banner_init();
		$this->sql_query="SELECT p.*, t.`date`\n".
		"	FROM `".$this->form->sql_table."` AS p LEFT JOIN `".$this->form->sql_table."_table` AS t ON t.`_parent_id`=p.`id`\n".
		"	WHERE p.`site`='".$this->site_nr."' AND p.`l`='".$this->axs_local['l']."' AND p.`promote` AND (p.`publ_1`>".$current." OR p.`publ_2`>'".$current."' OR t.`date`>".$current.")\n".
		"	GROUP BY p.`id` ORDER BY p.`publ_1` DESC";
		//foreach (array(''=>'','list'=>'list.') as $k=>$v) $this->tpl[$k]=axs_tpl(false, $this->axs_local['plugin'].'.poster.'.$v.'tpl');
		} #</banner_init()>
	//function banner_parse() {
		//$this->poster_start();
		//return $this->poster_parse();
		//} #</banner_parse()>
	function calendar_init() {
		global $axs, $axs_content;
		//$this->calendar=true;
		//if (isset($this->calendar)) return;
		//$tmp=(file_exists(@AXS_PATH_EXT.$axs['f_px'].'calendar.class.php')) ? AXS_PATH_EXT:$this->dir_plugin;
		//include_once($tmp.$axs['f_px'].'calendar.class.php');
		$this->calendar=new axs_calendar(/*$this->url,*/ /*axs_dir('site', 'http').$axs['dir_site'],*/ $axs['l'], $_REQUEST, $this->form->tr->get() /*axs_content::tr_get()*/);
		$this->calendar->url['c']=$this->axs_local['c'];
		//$this->calendar->dates_get_holiday();
		//$this->calendar->dates_get_national();
		//$this->calendar->dates_get_folklore();
		$this->calendar_sql="SELECT p.`id`, d.`date` AS `time`, p.`title` AS `text`\n".
		"	FROM `".$this->form->sql_table."_table` AS d LEFT JOIN `".$this->form->sql_table."` AS p ON p.`id`=d.`_parent_id`\n".
		"	WHERE `site`='".$this->site_nr."' AND `l`='".$axs['l']."' AND d.`date` BETWEEN '".$this->calendar->cal_start_timestamp."' AND '".$this->calendar->cal_end_timestamp."'\n".
		"	ORDER BY d.`date` ASC, d.`time_1` ASC";
		
		if (array_sum($this->calendar->selected)) $this->sql_where['calendar']=" `publ` BETWEEN '".$this->calendar->start_timestamp."' AND '".$this->calendar->end_timestamp."'";
		else {
			$this->sql_where['calendar']=" d.`date`>'".($axs['time']-$this->date_new)."'";
			$this->vr['time_between']=date('d.m.Y').'&hellip;';
			}
		} #</calendar_init()>
	function calendar_parse($cal_id=false) {
		global $axs, $axs_content;
		if ($cal_id) $this->calendar->cal_id=$cal_id;
		if ($this->axs_local['section']!=='content') {
			if (file_exists($tmp=axs_dir('site').$axs['dir_site'].$this->axs_local['plugin'].'.banner.tpl')) $this->calendar->templates['cal']=axs_tpl('', $tmp);
			}
		$this->vr['time_between']=$this->calendar->time_between_get('d.m.Y', ' - ');
		$dates=($this->calendar_sql) ? $this->calendar->dates_get_sql('', $this->calendar_sql):array();
		foreach ($dates as $month=>$days) foreach ($days as $date=>$day) foreach ($day as $k=>$cl) {
			$cl['time']=date('H:i', $cl['time']);
			$cl['url']=$this->url_root.axs_url($axs['url'], array('c'=>$this->axs_local['c'], 'year'=>axs_get('year'), 'month'=>axs_get('month'), 'day'=>axs_get('day'), ));
			if (!isset($cl['class'])) $cl['class']='articles_events';
			$dates[$month][$date][$k]=array('class'=>$cl['class'], 'text'=>'<a href="'.$cl['url'].'">'.$cl['time'].'</a> '.nl2br($cl['text']), );
			}
		$this->calendar->dates_set($dates);
		$this->calendar->url_set_date_events('?', array_merge($axs['url'], array('c'=>$this->axs_local['c'])));
		$axs['page']['head']['calendar.css']=$this->calendar->css_get();
		return $this->calendar->calendar_code_get('compact');
		} #</calendar_parse()>
	function list_articles_query($where=array()) {
		global $axs;
		$order='ASC';
		$this->vr['show_all_link']='';
		if (isset($this->calendar)) {
			$where=array();
			unset($this->form->sql_add['c'], $this->form->sql_add['plugin']);
			$order='ASC';
			if (array_sum($this->calendar->selected)) $where['calendar']="d.`date` BETWEEN '".$this->calendar->start_timestamp."' AND '".$this->calendar->end_timestamp."'";
			else {
				$where['calendar']="d.`date`>'".($axs['time']-$this->date_new)."'";
				$this->vr['time_between']=date('d.m.Y').'&hellip;';
				if (empty($_GET['c'])) {
					$this->vr['show_all_link']='<a href="'.'?'.axs_url($this->url, array('c'=>$axs['c'])).'">'.$this->form->tr->s('show_all_lbl').'&gt;</a>';
					$where['calendar'].=' AND !`hide_home`';
					}
				}
			}
		else {
			if ($this->arh) {
				$where['calendar']="d.`date`<'".$axs['time']."'";
				$order='DESC';
				$this->vr['arh_link']='<a class="arh" href="'.'?'.axs_url($this->url, array('arh'=>false)).'">'.$this->form->tr->s('new.lbl').'</a>';
				}
			else {
				$where['calendar']="d.`date`>'".($axs['time']-$this->date_new)."'";
				$order='ASC';
				$this->vr['arh_link']='<a class="arh" href="'.'?'.axs_url($this->url, array('arh'=>1)).'">'.$this->form->tr->s('archive.lbl').'</a>';
				}
			}
		$where=($where) ? ' AND '.implode(' AND ', (array)$where):'';
		$q="SELECT SQL_CALC_FOUND_ROWS p.*, d.`date`, d.`time_1`, d.`time_2`\n".
		"	FROM `".$this->form->sql_table."_table` AS d LEFT JOIN `".$this->form->sql_table."` AS p ON p.`id`=d.`_parent_id`\n".
		$this->form->sql_add(' AND ', 'p' , '	WHERE ').$where."\n".
		"	ORDER BY d.`date` ".$order.", d.`time_1`, d.`time_2` LIMIT ".$this->pager->start.','.$this->pager->psize;
		$r=axs_articles_base::list_articles_query($q);
		#dbg($q,$r);
		return $r;
		} #</list_articles_query()>
	} #</class::axs_articles_events>
#21.09.2011 ?>