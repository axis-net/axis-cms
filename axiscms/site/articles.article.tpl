		<article class="article clear-content {$_class}">
			<header>
				<h1 class="title">{$title}</h1>
				<div class="socialmedia">{$_socialmedia}</div>
				<div class="summary">
				{$file.t2}
				{$summary}
				</div>
			</header>
			<div class="text">
				{$text}
			</div>
			<div class="files">{$files}</div>
			<p class="back"><a href="{$back}" rel="nofollow">&lt; {$back.lbl}</a></p>
			{$qr_code}
		</article>
{$comments}