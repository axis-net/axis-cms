		<form id="checkout" method="post" action="{$form.action}#checkout">
			{$msg}
			<div class="display-table-single-row">
				<div class="cell delivery">
					<h3>{$checkout.delivery.lbl}</h3>
					{$delivery}
					<input id="calc" name="calc" type="submit" value="{$calc.lbl}" />
				</div>
				<div class="cell client">
					<h3>{$checkout.client.lbl}</h3>
					{$client_data}
					<div id="terms-agree_container" class="element checkbox">
						<label>
							<input name="terms-agree" type="checkbox" value="1" required="required"{$terms-agree} />
							{$checkout.terms-agree.lbl} <br />
							<a href="{$terms.url}" target="_blank"><strong>{$checkout.terms.lbl}</strong></a>
						</label>
					</div>
				</div>
				<div class="cell payment">
					<h3>{$checkout.payment.lbl}</h3>
					<table>
						<tr class="cart"><th scope="row">{$checkout.sum.cart.txt}:</th><td>{$cart.sum+vat}{$currency.symbol}</td></tr>
						<tr class="delivery"><th scope="row">{$checkout.sum.delivery.txt}:</th><td>{$sum.delivery+vat}{$currency.symbol}</td></tr>
						<tr class="sum"><th scope="row">{$checkout.sum.txt}:</th><td>{$sum+vat}{$currency.symbol}</td></tr>
					</table>
					<fieldset>
						<legend>{$orders.payment.lbl}</legend>
						{$payment}
					</fieldset>
				</div>
			</div>
			<input name="order_post" type="submit" value="{$checkout.order.lbl}" />
		</form>
		<script>axs.eshop.cart('#checkout');</script>