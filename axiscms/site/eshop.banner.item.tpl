				<tr id="{$c}_banner-{$nr}">
					<td class="pic">{$pic}</td>
					<td class="product"><a href="{$url}" title="{$label}">{$label}</a></td>
					<td class="amount">&times;{$amount}</td>
					<td class="price">{$currency.symbol}{$price+vat}</td>
					<td class="del"><button name="cart_del[{$cart_id}]" type="submit"><abbr title="{$del_lbl}">x</abbr></button></td>
				</tr>
