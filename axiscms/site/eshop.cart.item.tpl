				<tr id="cart-item{$nr}">
					<th class="nr right" scope="row">{$nr}</th>
					<td class="pic">{$pic}</td>
					<td class="product">{$label}</td>
					<td class="price right nowrap">{$price+vat}</td>
					<td class="amount right nowrap">
						<label><input name="cart_amount[{$cart_id}]" value="{$amount}" type="number" step="{$amount_step}" min="{$amount_step}" />{$amount_unit}</label><button name="calc" type="submit">{$calc.lbl}</button>
					</td>
					<td class="vat right">{$vat}</td>
					<td class="sum right nowrap">{$sum+vat}</td>
					<td class="edit"><a href="{$product_url}">{$cart.edit.lbl}</a></td>
					<td class="del"><button name="cart_del[{$cart_id}]" type="submit">{$cart.del.lbl}</button></td>
				</tr>
