			<article id="id{$id}" class="item clear-left {$_class}">
				<h2><a href="{$article_url}">{$_title}</a></h2>
				<a href="{$article_url}" class="thumbs">{$thumbs}</a>
				<div class="summary">{$file.t1} {$summary}</div>
				<a class="read_more" href="{$article_url}">{$read_more.lbl}</a>
			</article>
