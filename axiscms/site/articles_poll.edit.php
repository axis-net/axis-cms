<?php #2016-09-19
defined('AXS_SITE_ROOT') or exit(require('../index.php')); #<Prevent direct access />

#<Class "axs_articles_base_edit" extends "axs_articles_albums" through "axs_articles" />
include_once('articles_poll.class.php');
class axs_articles extends axs_articles_poll {}
include_once('articles.base.edit.class.php');

#<Customized editor class>
class axs_articles_poll_edit extends axs_articles_base_edit {
	//var $sql_desc=' DESC';
	/*function __construct(&$e) {
		axs_articles_base_edit::__construct($e);
		}*/ #</__construct()>
	function list_articles_query($sql=array()) {
		$r=parent::list_articles_query();
		$this->poll_query_table($r);
		return $r;
		} #</list_articles_query()>
	function list_articles_row(&$cl, &$vr=array()) {
		parent::list_articles_row($cl, $vr);
		$vr['choice']=$this->poll_item_votes($cl['id']);
		} #</list_articles_row()>
	function templates_custom() {
		parent::templates_custom();
		$this->tpl['list.item']=
			'      <div id="id{$id}" class="item clear-content {$class}">'."\n".
			'       <span class="right"><a href="{$edit_url}">(id#{$id})</a> <a href="#">&nbsp;&nbsp;&uarr;&nbsp;</a></span>'."\n".
			'       <h2><a href="{$edit_url}">{$title}&nbsp;</a></h2>'."\n".
			'	    <p>{$publ_lbl} {$publ} | {$arh_lbl} {$arh}</p>'."\n".
			'       <div class="summary">{$summary}</div>'."\n".
			'{$choice}'."\n".
			'	    <p><a href="{$preview_url}" target="_blank">{$preview_lbl}</a> ({$updated_lbl}:{$updated})</p>'."\n".
			'      </div>'."\n";
		} #</templates_custom()>
	} #</class::axs_articles_poll_edit>
return new axs_articles_poll_edit($this);
#2012-12-31 ?>