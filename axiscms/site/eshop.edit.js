/*2015-07-28*/
axs.eshop_edit={
	interval:60000,
	selector:'#content-content table.table',
	refresh:function(){
		if ($(axs.eshop_edit.selector+' select:focus').length) return axs.eshop_edit.refreshNext();
		axs.throbber_set(document.querySelector(axs.eshop_edit.selector));
		$.ajax(window.location.href,{	complete:axs.eshop_edit.refreshNext	}).done(function(data){
			$(axs.eshop_edit.selector).replaceWith($(data).find(axs.eshop_edit.selector));
			});
		},//</refresh()>
	refreshNext:function(){	window.setTimeout(axs.eshop_edit.refresh,axs.eshop_edit.interval);	}
	}//</class::axs.eshop_edit>
window.addEventListener('load',axs.eshop_edit.refreshNext);
/*2015-07-28*/