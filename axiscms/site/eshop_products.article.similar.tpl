		<article id="id{$id}" class="product item clear-content {$_class}">
			<h2><a href="{$article_url}">{$file.t1} {$title}</a></h2>
			<p class="promote">{$promote.lbl}: {$promote}</p>
			<p class="stock">{$stock.comment}: {$stock.available}</p>
			<p class="price">{$price.vat} {$price_discount.vat}</p>
		</article>
