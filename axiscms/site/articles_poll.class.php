<?php #2016-10-10
require_once('articles.base.class.php');

class axs_articles_poll extends axs_articles_base {
	var $secure_interval=3600;
	var $secure_interval_ip=172800;
	var $sql_limit_banner=false;
	var $folders=false;
	var $fields_article=array(
		''=>array('dir_entry'=>'', 'sql'=>array('table'=>'{$px}{$plugin}', 'attr'=>array('COMMENT'=>'AXIS CMS plugin articles_poll'))),
		'id'=>array('type'=>'number', 'size'=>10, 'step'=>1, 'required'=>0, 'readonly'=>'readonly', 'thead'=>0, 'search'=>1, 'sort'=>1, ),
		'title'=>array('type'=>'text', 'size'=>50, 'required'=>1, 'maxlength'=>255, 'thead'=>0, 'search'=>1, 'sort'=>1, ),
		'publ'=>array('type'=>'timestamp', 'size'=>10, 'required'=>1, 'format'=>'d.m.Y', 'thead'=>1, 'DESC'=>'DESC', 'search'=>1, 'sort'=>1, ),
		'arh'=>array('type'=>'timestamp', 'size'=>10, 'required'=>1, 'format'=>'d.m.Y', 'thead'=>1, 'search'=>1, 'sort'=>1, ),
		'summary'=>array('type'=>'wysiwyg', 'size'=>75, 'required'=>0, 'rows'=>3, 'thead'=>1, ),
		'choice'=>array('type'=>'table', 'size'=>10, 'thead'=>1, 'options'=>array(
				//'_nr'=>array(),
				'nr'=>array('type'=>'number', 'size'=>3, 'min'=>0, 'step'=>1, 'required'=>0, 'rank'=>true, 'ASC'=>'ASC', ),
				'label'=>array('type'=>'textarea', 'size'=>50, 'required'=>1, 'rows'=>2, 'thead'=>1, ),
				'votes'=>array('type'=>'number', 'size'=>10, 'min'=>0, 'step'=>1, 'required'=>0, 'ASC'=>'ASC', ),
				'_add'=>array('type'=>'submit', ),
				'_del'=>array('type'=>'checkbox', ),
				),
			),
		'updated'=>array('type'=>'timestamp-updated', 'size'=>50, 'required'=>0, 'thead'=>1, 'search'=>1, 'sort'=>1, ),
		'save'=>array('type'=>'submit', 'size'=>0, 'required'=>0, 'add_fn'=>true, ),
		);
	function __construct(&$axs_local) {
		global $axs;
		parent::__construct($axs_local);
		$this->section=($this->axs_local['section']==='content') ? 'id':$this->axs_local['section'];
		$this->user_ip=axs_form_site::ip_host_get('ip');
		$ip_list=array_unique(preg_split('/ +|,/', axs_get('HTTP_VIA', $_SERVER).','.axs_get('HTTP_X_FORWARDED_FOR', $_SERVER).','.axs_get('HTTP_CLIENT_IP', $_SERVER), -1, PREG_SPLIT_NO_EMPTY));
		$this->user_hash=(!empty($ip_list)) ? md5(implode('', $ip_list)):'';
		$tmp=" AND `hash`='".$this->user_hash."'";
		$this->sql_restrict="(SELECT `time` FROM `".$this->form->sql_table."_restrict` AS t WHERE t.`_parent_id`=p.`id` AND `ip`='".$this->user_ip."' ORDER BY `time` DESC LIMIT 1) AS `user_time`, (SELECT `hash` FROM `".$this->form->sql_table."_restrict` AS t WHERE t.`_parent_id`=p.`id` AND `hash`='".$this->user_hash."' LIMIT 1) AS `user_hash`";
		$axs['page']['head']['axs.form.js']='<script src="'.axs_dir('lib', 'http').'axs.form.js'.'"></script>'."\n";
		} #</axs_articles_base()>
	function banner_init() {
		global $axs;
		parent::banner_init();
		$this->sql_result=$this->list_articles_query(true);
		//$axs['page']['head'][$this->plugin.'.js']='<script src="'.$axs['http_root'].$axs['site_dir'].$axs['dir_plugins'].$this->plugin.'.js'.'"></script>'."\n";
		//$axs['page']['head'][$this->plugin.'.css']='<link type="text/css" rel="stylesheet" href="'.$axs['http_root'].$axs['site_dir'].$axs['dir_plugins'].$this->plugin.'.css'.'" media="screen" />'."\n";
		} #</banner_start()>
	function list_articles_row(&$cl, &$vr=array()) {
		$this->poll_item($cl, $vr);
		axs_articles_base::list_articles_row($cl, $vr);
		} #</list_articles_row()>
	function list_articles_query($banner=false) {
		global $axs;
		$q=($banner) ? 
		"SELECT p.*, ".$this->sql_restrict."\n".
		"	 FROM `".$this->form->sql_table."` AS p\n".
		"	WHERE p.`site`='".$this->site_nr."' AND p.`publ`<'".$axs['time']."' AND (p.`arh`=0 OR `arh`>'".$axs['time']."')\n".
		"	ORDER BY `publ` DESC":$this->poll_query();
		/*"SELECT SQL_CALC_FOUND_ROWS p.*, ".$this->sql_restrict."\n".
		"	FROM `".$this->form->sql_table."` AS p WHERE p.`site`='".$this->site_nr."' AND p.`l`='".$axs['l']."'\n".
		"	ORDER BY p.`publ` DESC LIMIT ".$this->pager->start.','.$this->pager->psize;*/
		$r=parent::list_articles_query($q);
		$this->poll_query_table($r);
		#dbg($q,$r);
		return $r;
		} #</list_articles_query()>
	function display_article_vars(&$cl=array(), &$vr=array()) {
		$this->poll_item($cl, $vr);
		$vr=array_merge($vr, $cl);
		parent::display_article_vars($cl, $vr);
		} #</display_article_vars()>
	function display_article_query($id=false) {
		$this->vote($id);
		$cl=axs_db_query($this->poll_query($id), 'row', $this->db, __FILE__, __LINE__);
		$this->poll_query_table(array($id=>$cl));
		return $cl;
		} #</display_article_vars()>
	
	function poll_item(&$cl, &$vr=array()) {
		$cl['section']=$this->section;
		//$cl['action']=$this->url_root.htmlspecialchars($_SERVER['QUERY_STRING'].'&id='.$cl['id']);
		//$cl['url']=$this->url_root.axs_url($this->url, array('c'=>$cl['c'], )).'#id-'.$cl['id'];
		$cl['vote_open']=$this->vote_open($cl);
		$cl['_choice']=($cl['vote_open']) ? $this->poll_item_form($cl['id']):$this->poll_item_votes($cl['id']);
		} #</poll_item()>
	function poll_item_form($parent_id) {
		$data=(isset($this->sql_result_table[$parent_id])) ? $this->sql_result_table[$parent_id]:array();
		$html='';
		foreach ($data as $r) $html.=
		'       <label><input name="choice" type="radio" value="'.$r['id'].'" />'.axs_html_safe($r['label']).'</label>'."\n";
		return $html.'       <input name="poll" type="submit" value="'.$this->form->tr->s('vote.lbl').'" />';
		} #</poll_item_form()>
	function poll_item_votes($parent_id) {
		$data=(isset($this->sql_result_table[$parent_id])) ? $this->sql_result_table[$parent_id]:array();
		$html='';
		foreach ($data as $r) $html.=
			'        <dt>'.axs_html_safe($r['label']).'</dt>'."\n".
			'        <dd>'.$r['percent'].'% ('.$r['votes'].')<meter min="0" max="100" value="'.$r['percent'].'">'.$r['percent'].'%;"</meter></dd>'."\n";
		return '       <dl>'."\n".
		$html.
		'       </dl>';
		} #</poll_item_votes()>
	function poll_query($id=false) {
		global $axs;
		$where=($id) ? " AND `id`='".$id."'":'';
		return "SELECT SQL_CALC_FOUND_ROWS p.*, ".$this->sql_restrict."\n".
		"	FROM `".$this->form->sql_table."` AS p WHERE p.`site`='".$this->site_nr."' AND p.`l`='".$axs['l']."'".$where."\n".
		"	ORDER BY p.`publ` DESC LIMIT ".$this->pager->start.','.$this->pager->psize;
		} #</poll_query()>
	function poll_query_table($r) {
		$this->sql_result_table=array();
		$id=array();
		foreach ($r as $cl) $id[$cl['id']]=$cl['id'];
		if (empty($id)) return;
		foreach ($id as $k=>$v) $id[$k]="p.`_parent_id`='".$v."'";
		$r=axs_db_query($q="SELECT *, ROUND(`votes`*100/GREATEST(\n".
		"	(SELECT SUM(`votes`) FROM `".$this->form->sql_table."_table` AS s WHERE s.`_parent_id`=p.`_parent_id` GROUP BY s.`_parent_id` LIMIT 1),1),2) AS `percent`\n".
		"	FROM `".$this->form->sql_table."_table` AS p\n".
		"	WHERE ".implode(' OR ', $id)." ORDER BY p.`_parent_id` ASC, p.`nr` ASC, p.`label` ASC", 1, $this->db, __FILE__, __LINE__);
		#dbg($q,$r);
		foreach ($r as $cl) $this->sql_result_table[$cl['_parent_id']][$cl['id']]=$cl;
		} #</poll_query_table()>
	function vote($id) {
		global $axs;
		if ((empty($_POST['choice'])) or (!$id)) return;
		axs_db_query("DELETE FROM `".$this->form->sql_table."_restrict` WHERE `time`<'".($axs['time']-$this->secure_interval_ip)."'", '', $this->db, __FILE__, __LINE__);
		$id+=0;
		$choice=$_POST['choice']+0;
		$cl=axs_db_query("SELECT p.*, ".$this->sql_restrict."\n".
		"	FROM `".$this->form->sql_table."` AS p WHERE p.`site`='".$this->site_nr."' AND p.`id`='".$id."'", 'row', $this->db, __FILE__, __LINE__);
		if (!$this->vote_open($cl)) return;
		axs_db_query("INSERT INTO `".$this->form->sql_table."_restrict` SET `_parent_id`='".$id."', `time`='".$axs['time']."', `ip`='".addslashes($this->user_ip)."', `hash`='".$this->user_hash."'", '', $this->db, __FILE__, __LINE__);
		axs_db_query("UPDATE `".$this->form->sql_table."_table` SET `votes`=(`votes`+1) WHERE `_parent_id`='".$id."' AND `id`='".$choice."'", '', $this->db, __FILE__, __LINE__);
		//if (!isset($_GET['ajax'])) exit(axs_redir($axs['http'].'://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'].'#'.$this->section.'-'.$id));
		} #</vote()>
	function vote_open(&$cl) {
		global $axs;
		if (!$cl['id']) return false;
		if ($cl['user_time']) {
			if ($this->user_hash) {	if ($this->user_hash===$cl['user_hash']) return false;	}
			else {	return ($cl['user_time']<$axs['time']-$this->secure_interval);	}
			}
		if (($cl['publ']<$axs['time']) && (($cl['arh']==0) or ($cl['arh']>$axs['time']))) return true;
		} #</vote_open()>
	} #</class::axs_articles_events>
#2012-12-31 ?>