			<h3>
				{$cart.txt}
				<a href="{$link}" class="toggle-switch"><strong>{$count}</strong> {$cart.items.txt} - <strong><abbr title="{$currency}">{$currency.symbol}</abbr>{$sum+vat}</strong></a>
			</h3>
			<form class="cart toggle-content" method="post" action="{$form.action}#{$c}">
			<table>
				<caption>{$count} {$cart.items.txt}</caption>
				<tfoot>
					<tr class="sum_vat"><th colspan="3">{$cart.sum.vat.txt}:</th><td colspan="2">{$currency.symbol}{$sum.vat}</td></tr>
					<tr class="sum_total"><th colspan="3">{$cart.sum+vat.txt}:</th><td colspan="2">{$currency.symbol}{$sum+vat}</td></tr>
				</tfoot>
				<tr>
					<th class="pic">{$cart.pic.lbl}</th>
					<th class="label">{$cart.product.lbl}</th>
					<th class="amount">{$cart.amount.lbl}</th>
					<th class="price">{$cart.price.lbl}</th>
					<th class="del">{$cart.del.lbl}</th>
				</tr>
		{$cart} 
			</table>
			<a class="button" href="{$link}">{$cart.checkout.lbl}</a>
{$orders}
			</form>
			<script>axs.menu.toggle(document.querySelector('#{$c}'));</script>