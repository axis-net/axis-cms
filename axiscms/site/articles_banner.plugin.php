<?php #2019-03-31 utf-8
$plugin_def=array(); # plugin default data
$plugin_def['name']['et']='Artiklid > Bänner';
$plugin_def['name']['en']='Articles > Banner';
$plugin_def['title']['']='';
$plugin_def['help']['']='';
$plugin_def['mkdir']='files';
$plugin_def['content']['']='<?php #|'.$axs['time'].',,'.axs_get('user', $axs_user).',,||?>'."\n";
require_once('articles.base.class.php');
$plugin_def['tr']=axs_articles_base::$tr;

if (defined('AXS_SITE_NR')) {
require_once('articles.base.class.php');
$data=new axs_articles_base($axs_local);
if (!$data->db) return '<p class="msg" lang="en">ERROR! This plugin requires SQL database.</p>';
$data->banner_init();
$data->vr['url']=$data->url_root.axs_url($data->url, array('c'=>key(axs_content::index_c_find(false, $data->vr['plugin'])).''));
if (axs_tpl_has($data->tpl[''], '_content')) $data->vr['_content']=include('deflt.plugin.php');
return $data->banner_parse();
}
#2008-06 ?>