//29.01.2012
axs.articles={
	lightbox:function(){
		$('#articles_list a.lightbox').fancybox();
		document.querySelector('body').addEventListener('swiped',function(e){
			var el=document.querySelector('div.fancybox-overlay');
			if(el){
				//console.log(e.target); // element that was swiped
				//console.log(e.detail); // event data { dir: 'left', xStart: 196, xEnd: 230, yStart: 196, yEnd: 4 }
				if(e.detail.dir==='left') $.fancybox.next();
				if(e.detail.dir==='right') $.fancybox.prev();
				};
			});
		},//</lightbox()>
	preview:function(container_id){
		var thumbs=$("#"+container_id+" a.preview");
		for (var i=0; i<thumbs.length; i++){
			thumbs[i].container_id=container_id;
			thumbs[i].onclick=axs.articles_albums.preview_img;
			}
		$(thumbs[0]).click();
		},//</preview()>
	preview_img:function(){
		var url=this.href;
		var title=$(this.childNodes[0]).attr("alt");
		var el=document.createElement('a');
		el.href=axs.SITE_ROOT+'?popup='+escape(url)+'&txt='+escape(title);
		el.onclick=axs.window_popup;
		el.innerHTML='<img src="'+url.replace(/_t3\./,'_t2.')+'" alt="'+escape(title)+'" />';
		var container=$("#"+this.container_id+" div.preview")[0];
		$(container).empty();
		$(container).append(el);
		return false;
		}//</preview_img()>
	}//</class::articles>
//25.01.2012