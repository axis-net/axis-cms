<?php #06.03.2013
defined('AXS_PATH_CMS') or exit(require('../index.php')); #<Prevent direct access />

#<Class "axs_articles_base_edit" extends "axs_articles_news" through "axs_articles" />
include_once('articles_blog.class.php');
class axs_articles extends axs_articles_blog {}
include_once('articles.base.edit.class.php');

#<Customized editor class>
class axs_articles_edit extends axs_articles_base_edit {}
#</Customized editor class>

return new axs_articles_edit($this);
#03.03.2013 ?>