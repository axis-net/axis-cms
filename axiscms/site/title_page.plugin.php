<?php #2015-05-31
$plugin_def=array(); # plugin default data
$plugin_def['name']['et']='Tiitelleht';
$plugin_def['title']['']='';
$plugin_def['help']['']='';
$plugin_def['mkdir']='';
$plugin_def['content']['']='<?php #|'.$axs['time'].',,'.axs_get('user', $axs_user).',,||?>'."\n";
# media=media&img_proc_t=&img_w_t=&img_h_t=&img_proc=&img_w=&img_h=

$dir=axs_dir('site');
if (file_exists($tmp=$dir.str_replace('.plugin.php', '.config.php', basename(__FILE__)))) include($tmp);

if (defined('AXS_SITE_NR')) {
$axs['template']=axs_tpl(false, 'title_page.tpl'); # overwrite frame template for title page
if (count($axs['cfg']['site'][AXS_SITE_NR]['langs'])>1) {
	if (file_exists($dir.'title_page.lang.tpl')) {
		$axs['page']['langs_title']=array();
		$lng_tpl=axs_tpl(false, 'title_page.lang.tpl');
		$lngact_tpl=axs_tpl(false, 'title_page.lang_act.tpl');
		$sepr_tpl=''; //axs_tpl(false, 'title_lang_sepr.tpl');
		foreach ($axs['cfg']['site'][AXS_SITE_NR]['langs'] as $key=>$value) {
			$tpl=($l==$value) ? $lngact_tpl:$lng_tpl;
			$axs['page']['langs_title'][]=str_replace(array('{$code}', '{$label}'), array($key, $value['l']), $tpl);
			}
		$axs['page']['langs_title']=implode($sepr_tpl, $axs['page']['langs_title']);
		}
	}
else $axs['page']['langs_title']='';
if (file_exists($dir.'menu1_title_page.tpl')) $axs['menu_tpl']['menu1']=array(
	2=>array('cls'=>'_title_page.tpl', 'act'=>'_title_page.tpl', 'sub'=>'_title_page.tpl', 'opn'=>'_title_page.tpl', 'sepr'=>'', ),
	);
$axs_content['content'][$axs['c']]['template']=(file_exists($dir.($tmp=$axs_local['plugin'].'.row').'.tpl')) ? $tmp:'deflt';
return include('deflt.plugin.php');
}
#2008-05 ?>