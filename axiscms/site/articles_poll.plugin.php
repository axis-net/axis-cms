<?php #2016-09-21
$plugin_def=array(); # plugin default data
$plugin_def['name']['et']='Poll';
$plugin_def['help']['']='';
$plugin_def['mkdir']='';
$plugin_def['tr']['en']=array(
	'form.title'=>'Poll',
	'id.lbl'=>'ID',
	'title.lbl'=>'question',
	'publ.lbl'=>'start',
	'arh.lbl'=>'end',
	'submit.lbl'=>'submit',
	'summary.lbl'=>'text',
	'choice.lbl'=>'choice',
	'choice.id.lbl'=>'id',
	'choice.nr.lbl'=>'nr',
	'choice.label.lbl'=>'answer',
	'choice.votes.lbl'=>'votes',
	'vote.lbl'=>'vote',
	'updated.lbl'=>'updated',
	'save.lbl'=>'salvesta',
	);
$plugin_def['tr']['et']=array(
	'form.title'=>'Küsitlused',
	'id.lbl'=>'ID',
	'title.lbl'=>'küsimus',
	'publ.lbl'=>'algus',
	'arh.lbl'=>'lõpp',
	'submit.lbl'=>'vasta',
	'summary.lbl'=>'tekst',
	'choice.lbl'=>'vastusevariandid',
	'choice.id.lbl'=>'id',
	'choice.nr.lbl'=>'nr',
	'choice.label.lbl'=>'vastus',
	'choice.votes.lbl'=>'hääli',
	'vote.lbl'=>'vasta',
	'updated.lbl'=>'muudetud',
	'save.lbl'=>'salvesta',
	);

if (defined('AXS_SITE_NR')) {
require_once('articles_poll.class.php');
$data=new axs_articles_poll($axs_local);
if ($tmp=$data->error()) return $tmp;
if ($axs_local['section']!=='content') {
	$data->banner_init();
	return $data->banner_parse();
	}
$data->page_init($plugin_def);
$data->vr['content']=include('deflt.plugin.php');
return $data->page_parse();
}
#2012-12-31 ?>