<?php #2014-07-05
require_once(axs_dir('site_base').'articles.base.class.php');
class axs_articles_blog extends axs_articles_base {
	//public $name='articles_news';
	public $sql_limit=array(25,50,100,250,500,100);
	public $fields_article=array(
		''=>array('dir_entry'=>'{$id}/', 'sql'=>array('table'=>'{$px}{$name}', )),
		'article_head'=>array('type'=>'fieldset', 'size'=>0, 'required'=>0, ),
		'id'=>array('type'=>'number', 'size'=>10, 'required'=>0, 'readonly'=>'readonly', 'thead'=>1, 'search'=>1, 'sql'=>array(
			'_type'=>'int', '_value'=>10, '_attribute'=>'unsigned', 'Key'=>'PRI', 'Extra'=>'AUTO_INCREMENT', 
			), ),
		'title'=>array('type'=>'text', 'size'=>50, 'required'=>1, 'maxlength'=>255, 'thead'=>1, 'search'=>1, 'title_item'=>1, ),
		'publ'=>array('type'=>'timestamp', 'size'=>10, 'required'=>1, 'format'=>'d.m.Y H:i', 'thead'=>1, 'search'=>1, 'sort'=>1, 'DESC'=>'DESC', 'value'=>'time()', ),
		//'arh'=>array('type'=>'timestamp', 'size'=>10, 'required'=>0, 'format'=>'d.m.Y H:i', 'thead'=>1, ),
		'file'=>array('type'=>'file', 'size'=>0, 'required'=>0, 'accept'=>'image.*', 'thead'=>1, 'add_fn'=>true, 't'=>array(
				'_t1'=>array('w'=>150, 'h'=>150, 'type'=>'jpg', 'crop_pos'=>'50%', 'crop_size'=>':', 'q'=>90, 'name_base'=>'{$id}', 'alt'=>'{$id}_t1.{$ext}', 'dw'=>200, 'dh'=>200, 'link'=>array('c'=>'c', 't'=>'title_alias', 'id'=>'id'), 'link_target'=>'', ),
				'_t2'=>array('w'=>1000, 'h'=>1000, 'type'=>'jpg', 'q'=>90, 'name_base'=>'{$id}', 'alt'=>'{$id}_t2.{$ext}', 'dw'=>200, 'dh'=>200, 'link'=>'_t1', 'link_target'=>'', ),
				),
			),
		'summary'=>array('type'=>'wysiwyg', 'size'=>75, 'required'=>0, 'rows'=>2, 'thead'=>1, ),
		'article_head_end'=>array('type'=>'fieldset_end', 'size'=>0, 'required'=>0, ),
		'text'=>array('type'=>'wysiwyg', 'size'=>75, 'required'=>0, ),
		'seo'=>array('type'=>'fieldset', 'size'=>0, 'required'=>0, ),
		'title_alias'=>array('type'=>'text', 'size'=>50, 'required'=>0, 'maxlength'=>255, ),
		'meta_description'=>array('type'=>'text', 'size'=>50, 'required'=>0, 'maxlength'=>255, ),
		'seo_end'=>array('type'=>'fieldset_end', 'size'=>0, 'required'=>0, ),
		'comments'=>array('type'=>'checkbox', 'size'=>1, 'required'=>0, 'value'=>1, 'thead'=>1, 'search'=>1, ),
		'comments_mail'=>array('type'=>'email', 'size'=>20, 'required'=>0, 'maxlength'=>255, 'multiple'=>'multiple', ),
		'updated'=>array('type'=>'timestamp-updated', 'size'=>50, 'required'=>0, 'thead'=>1, 'search'=>1, 'sort'=>1, 'DESC'=>'DESC', ),
		'save'=>array('type'=>'submit', 'size'=>0, 'required'=>0, 'add_fn'=>true, ),
		);
	} #</class::axs_articles>
#2014-07-05 ?>