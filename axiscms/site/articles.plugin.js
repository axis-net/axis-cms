//2018-05-11
axs.articles={
	banner_close:function(){
		this.parentNode.parentNode.removeChild(this.parentNode);
		axs.cookie_set(this.parentNode.id,"closed",0);
		return false;
		},//</banner_close()>
	banner_close_init:function(id){
		var a=axs.fn_args(arguments,{id:'',img:false});
		var btn=document.createElement('a');
		btn.href="#";
		btn.className='close';
		btn.onclick=axs.articles.banner_close;
		btn.innerHTML=(a.img) ? '<img src="'+a.img+'" alt="x" />':'x';
		var node=document.getElementById(id);
		(node.firstChild) ? node.insertBefore(btn,node.firstChild):node.appendChild(btn);
		},//</banner_close_init()>	
	imgPreview:function(list,target){
		var thumbs=document.querySelectorAll(list);
		if (!thumbs.length) return;
		var set=function(el){
			var list=document.querySelectorAll(el.getAttribute('data-list'));
			axs.class_rem(list,'current');
			axs.class_add(el,'current');
			var target=document.querySelector(el.getAttribute('data-target'));
			target.href=el.href;
			target.innerHTML=el.innerHTML;
			var targetImg=target.querySelector('img');
			targetImg.removeAttribute('id');
			if (el.getAttribute('data-image')) targetImg.setAttribute('src',el.getAttribute('data-image'));
			};
		var current=false;
		for(var i=0; i<thumbs.length; i++){
			thumbs[i].setAttribute('data-list',list);
			thumbs[i].setAttribute('data-target',target);
			thumbs[i].addEventListener('click',function(e){
				e.preventDefault();
				set(this);
				});
			if (axs.class_has(thumbs[i],'current')) current=i;
			}
		if (current===false) axs.class_add(thumbs[0],'current');
		if (!document.querySelector(target).innerHTML) set(thumbs[0]);
		},//</imgPreview()>
	}//</class::axs.articles>
//2010-11-26