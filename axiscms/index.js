/*2021-01-14*/
axs.feedback_form={
	init:function(){
		document.querySelector('#feedback_form').style.display="none";
		axs.user.window_size_send("feedback_form");
		document.getElementById("feedback_button").addEventListener('click',axs.feedback_form.open);
		var el=document.getElementById("feedback_menu");
		if (el) el.addEventListener('click',axs.feedback_form.open);
		var el=document.querySelector("#content.edit-dashboard section.feedback");
		if (el) el.addEventListener('click',axs.feedback_form.open);
		},
	open:function() {
		var href=document.getElementById("feedback_form").getAttribute("action");
		window.open(href, 'feedbackwin', 'scrollbars=1, location=0, resizable=1, toolbar=0, status=1, width=700, height=500, top=50, left=40');
		document.getElementById("feedback_form").submit();
		return false;
		}
	}//</class::axs.feedback_form>
/*2009-07-28*/