rem 2021-05-04

rem zip.exe - www.info-zip.org
rem wget.exe - http://www.gnu.org/software/wget/

copy site\index.tpl ..\axs_site\index.tpl

cd..
attrib -h -r -s axiscms\*.zip
del axiscms\axiscms*.zip

del axiscms\data\logfile-*.php
set y=%date:~8,2%
type NUL > axiscms\data\log.%y%0001.php
rem echo . > myfile.txt
rem copy /y nul myfile.txt

zip -r -9 -S axiscms\axiscms.zip *.* -x .\!*\*.* \*.git\* .gitignore
zip axiscms\axiscms.zip -d axiscms\data\user.adm.php axiscms\data\user.adm..php axiscms\data\index.customize.php
zip axiscms\axiscms.zip -d axiscms\data\index.filemonitor.php

rem ren "..\_htaccess" "!_htaccess"
wget "http://localhost/axiscms/axiscms/?editor=&s1=download&download=make" -O axiscms\a.html
rem ren "..\!_htaccess" "_htaccess"
del axiscms\a.html
pause