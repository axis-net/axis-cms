<?php #2022-03-29
defined('AXS_PATH_CMS') or exit(require('../../index.php'));
#DOMPDF 1.2.0
require_once(__DIR__.'/autoload.inc.php');
use Dompdf\Dompdf;
use Dompdf\Options;
$options=new Options();
$options->set('isRemoteEnabled', true);
$options->set('tempDir', __DIR__.'/data');
$options->set('logOutputFile', __DIR__.'/data/log.txt');
$options->set('chroot', realpath(__DIR__.'/../../..'));
$options->setIsRemoteEnabled(true);
$dompdf=new Dompdf($options);
$context = stream_context_create([ 
	'ssl' => [ 'verify_peer' => FALSE, 'verify_peer_name' => FALSE,	'allow_self_signed'=> TRUE ], 
]);
$dompdf->setHttpContext($context);
return $dompdf;
#$pdf->load_html($html);
#$pdf->render();
#axs_exit($pdf->stream($stream));
#$pdf->output();
#2022-02-21 ?>