<?php #2015-09-09
class axs_business {
	var $name='business';
	var $l_default='en';
	var $profiles=array();
	function __construct($site_nr=1) {
		global $axs;
		$this->site_nr=$site_nr;
		$this->px=$axs['cfg']['site'][$this->site_nr]['prefix'];
		$this->db=$axs['cfg']['site'][$this->site_nr]['db'];
		/*$this->langs=array();
		foreach ($axs['cfg']['site'][$this->site_nr]['langs'] as $k=>$v) $this->langs[$k]=$v['l'];*/
		$this->name='business';
		$this->table=$this->px.$this->name;
		} #</__construct()>
	/*function fields_get($l=false) {
		if (!in_array($l, $this->langs)) $l=current($this->langs);
		$this->org_fields=array();
		$result=axs_db_query('SELECT `field`, `type`, `label_'.$l.'` AS `label`'."\n".
		'	FROM `'.$this->table.'_profiles_header` ORDER BY `rank` ASC, `id` ASC', 1, $this->db, __FILE__, __LINE__);
		foreach ($result as $cl) {
			$cl['size']=255;
			$this->org_fields[$cl['field']]=$cl;
			}
		$this->org_fields['id']['readonly']='readonly';
		$this->org_fields['disabled']['value']=1;
		unset($this->org_fields['id']['size'], $this->org_fields['disabled']['size'], $this->org_fields['updated']['size']);
		return $this->org_fields;
		} # </fields_get()>*/
	function currencys_get() {
		$this->currency_table=array();
		foreach (axs_db_query('SELECT * FROM `'.$this->table.'_currency` WHERE !disabled ORDER BY currency ASC', 1, $this->db, __FILE__, __LINE__) as $cl) $this->currency_table[$cl['id']]=$cl;
		} # </currencys_get()>
	function currencys_select($value) {
		if (!isset($this->currency_table)) $this->currencys_get();
		$value=($value=='id') ? $value:'currency';
		$select=array();
		foreach ($this->currency_table as $cl) $select[]=array('value'=>$cl[$value], 'label'=>$cl['currency'], );
		return $select;
		} # </currencys_select()>
	function org_get($org_id, $l, $prefix='', $values=true, $labels=true) {
		global $axs;
		//if (!in_array($l, $this->langs)) $l=current($this->langs);
		$this->header=new axs_form_header($this->site_nr);
		$this->header->sql_header_get($this->name, 'profiles');
		$this->fields=$this->header->structure_get();
		$out=array();
		if ($values) {
			$cl=$this->org_get_profile($org_id);
			foreach ($this->fields as $k=>$v) $out[$prefix.$k]=($cl) ? $this->header->value_display($k, $v, $cl, $out, ($values==='html')):'';
			}
		if ($labels) foreach ($this->fields as $k=>$v) $out[$prefix.$k.'.lbl']=$v['label'];
		return $out;
		} # </org_get()>
	function org_get_profile($id, $get=array()) {
		if (!$id+=0) return;
		//dbg($id)
		if (empty($this->profiles[$id])) $this->profiles[$id]=axs_db_query("SELECT * FROM `".$this->table."_profiles` WHERE `id`='".$id."'", 'row', $this->db, __FILE__, __LINE__);
		if (empty($this->profiles[$id])) return axs_log(__FILE__, __LINE__, 'content', 'Empty data @ org_get($id="'.$id.'")', false, '---');
		if ($get) {
			if (!is_array($get)) return $this->profiles[$id][$get];
			$out=array();
			foreach ($get as $k=>$v) $out[$v]=$this->profiles[$id][$v];
			return $out;
			}
		return $this->profiles[$id];
		} #</org_get_profile()>
	function org_list($l) {
		global $axs;
		$result=axs_db_query('SELECT * FROM `'.$this->table.'_profiles` ORDER BY `id`', 1, $this->db, __FILE__, __LINE__);
		$list=array();
		foreach ($result as $cl) $list[$cl['id']]=array('value'=>$cl['id'], 'label'=>$cl['name_'.$l], 'label.html'=>axs_html_safe($cl['name_'.$l]), );
		return $list;
		} # </org_list()>
	function vat_add($num, $percent, $separator='') {
		$num+=($num/100)*$percent;
		$num=($separator) ? str_replace(' ', $separator, number_format($num, 2 ,'.', ' ')):round($num, 2);
		return $num;
		} # </vat_add()>
	function vat_by_code($code) {
		if (!isset($this->vat_table)) $this->vat_table_get();
		return $this->vat_table[$code]['vat'];
		}
	function vat_sum($num, $percent, $separator='') {
		$num=($num/100)*$percent;
		$num=($separator) ? str_replace(' ', $separator, number_format($num, 2 ,'.', ' ')):round($num, 2);
		return $num;
		} # </vat()>
	function vat_table_get() {
		$this->vat_table=array();
		foreach (axs_db_query('SELECT code, vat, disabled FROM `'.$this->table.'_vat` ORDER BY rank ASC', 1, $this->db, __FILE__, __LINE__) as $cl) $this->vat_table[$cl['code']+0]=$cl;
		return $this->vat_table;
		} # </vat_types_get()>
	function vat_types_get($value) {
		if (!isset($this->vat_table)) $this->vat_table_get();
		$array=array();
		foreach ($this->vat_table as $cl) if (!$cl['disabled']) $array[]=array('value'=>$cl[$value], 'label'=>$cl['vat'].'%', );
		return $array;
		} # </vat_types_get()>
	} # </class:axs_business>
/*
--
-- Table structure for table `axs_business_countrys`
--
CREATE TABLE IF NOT EXISTS `axs_business_countrys` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `disable` tinyint(4) NOT NULL DEFAULT '0',
  `code` char(2) CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
  `name` varchar(255) NOT NULL,
  `lang` char(2) CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `lang` (`lang`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;
--
-- Dumping data for table `axs_business_countrys`
--
INSERT INTO `axs_business_countrys` VALUES(1, 0, 'ee', 'Eesti sisene', 'et');
INSERT INTO `axs_business_countrys` VALUES(2, 0, 'lv', 'Läti sisene', 'lv');
INSERT INTO `axs_business_countrys` VALUES(3, 0, '--', 'Euroopa Liidu väline', 'et');
INSERT INTO `axs_business_countrys` VALUES(4, 0, 'eu', 'Euroopa Liidu sisene', 'et');
INSERT INTO `axs_business_countrys` VALUES(5, 0, 'fi', 'Soome', 'et');
--
-- Table structure for table `axs_business_currency`
--
CREATE TABLE IF NOT EXISTS `axs_business_currency` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `currency` char(3) CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
  `symbol` varchar(5) CHARACTER SET utf8 NOT NULL,
  `disabled` tinyint(4) NOT NULL DEFAULT '0',
  `updated` int(10) unsigned NOT NULL,
  `updated_uid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;
--
-- Dumping data for table `axs_business_currency`
--
INSERT INTO `axs_business_currency` VALUES(1, 'EUR', '€', 0, 0, 0);
INSERT INTO `axs_business_currency` VALUES(4, 'EEK', 'EEK', 1, 0, 0);
--
-- Table structure for table `axs_business_profiles`
--
CREATE TABLE IF NOT EXISTS `axs_business_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `disable` tinyint(4) NOT NULL DEFAULT '0',
  `name_et` varchar(255) COLLATE utf8_estonian_ci NOT NULL,
  `name_en` text COLLATE utf8_estonian_ci,
  `name_ru` varchar(255) COLLATE utf8_estonian_ci NOT NULL,
  `brand_et` varchar(255) COLLATE utf8_estonian_ci NOT NULL,
  `brand_en` varchar(255) COLLATE utf8_estonian_ci DEFAULT NULL,
  `brand_ru` varchar(255) COLLATE utf8_estonian_ci NOT NULL,
  `reg_nr` text COLLATE utf8_estonian_ci NOT NULL,
  `vat_nr` text COLLATE utf8_estonian_ci NOT NULL,
  `address` text COLLATE utf8_estonian_ci NOT NULL,
  `phone` text COLLATE utf8_estonian_ci NOT NULL,
  `fax` text COLLATE utf8_estonian_ci NOT NULL,
  `email` text COLLATE utf8_estonian_ci NOT NULL,
  `web` text COLLATE utf8_estonian_ci NOT NULL,
  `bank` text COLLATE utf8_estonian_ci NOT NULL,
  `iban` text COLLATE utf8_estonian_ci NOT NULL,
  `swift` text COLLATE utf8_estonian_ci NOT NULL,
  `updated` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Last update timestamp',
  `updated_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '=users.id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_estonian_ci AUTO_INCREMENT=3 ;
--
-- Dumping data for table `axs_business_profiles`
--
INSERT INTO `axs_business_profiles` VALUES(1, 0, 'Firmanimi OÜ', 'Business Ltd.', 'Business Ltd.', 'Firmanimi', 'Business', 'Business', '11111111', 'EE11111111', '', '', '', 'info@firma.com', 'www.firma.com', 'Bank AS', 'EE1111111111111', 'EE111111111111', 1336642618, 0);
--
-- Table structure for table `axs_business_units`
--
CREATE TABLE IF NOT EXISTS `axs_business_units` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `disabled` tinyint(4) NOT NULL DEFAULT '0',
  `lang` binary(2) NOT NULL DEFAULT '  ',
  `rank` tinyint(4) NOT NULL DEFAULT '0',
  `label` varchar(255) COLLATE utf8_estonian_ci NOT NULL DEFAULT '',
  `comment` varchar(255) COLLATE utf8_estonian_ci NOT NULL DEFAULT '',
  `updated` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Last updated UNIX timestamp',
  `updated_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '=users.id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_estonian_ci AUTO_INCREMENT=4 ;
--
-- Dumping data for table `axs_business_units`
--
INSERT INTO `axs_business_units` VALUES(1, 0, 'et', 1, 'tk', '', 1264499323, 23);
INSERT INTO `axs_business_units` VALUES(2, 0, 'et', 2, 'h', '', 1264499300, 23);
INSERT INTO `axs_business_units` VALUES(3, 1, 'et', 3, 'kg', '', 1404330155, 0);
--
-- Table structure for table `axs_business_vat`
--
CREATE TABLE IF NOT EXISTS `axs_business_vat` (
  `id` smallint(5) NOT NULL AUTO_INCREMENT,
  `code` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `rank` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `vat` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `comment` varchar(255) COLLATE utf8_estonian_ci NOT NULL DEFAULT '',
  `disabled` tinyint(4) NOT NULL DEFAULT '0',
  `updated` int(10) unsigned NOT NULL DEFAULT '0',
  `updated_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '=users.id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_estonian_ci AUTO_INCREMENT=4 ;
--
-- Dumping data for table `axs_business_vat`
--
INSERT INTO `axs_business_vat` VALUES(1, 0, 3, 0, 'Käibemaksuvaba', 0, 1404328264, 0);
INSERT INTO `axs_business_vat` VALUES(2, 1, 2, 20, 'Laiatarbekaubad', 1, 1404328946, 0);
INSERT INTO `axs_business_vat` VALUES(3, 2, 1, 9, 'Ravimid', 0, 0, 0);
*/
#2009-06-25 ?>