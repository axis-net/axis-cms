<?php #14.11.2011
if (!defined('AXS_SITE_ROOT')) exit(require('../axiscms/log.php')); # Prevent direct access

class axs_business {
	var $name='business';
	var $l_default='en';
	function axs_business($site_nr=1) {
		global $axs;
		$this->site_nr=$site_nr;
		$this->px=$axs['cfg']['site'][$this->site_nr]['prefix'];
		$this->db=$axs['cfg']['site'][$this->site_nr]['db'];
		/*$this->langs=array();
		foreach ($axs['cfg']['site'][$this->site_nr]['langs'] as $k=>$v) $this->langs[$k]=$v['l'];*/
		$this->name='business';
		$this->table=$this->px.$this->name;
		}
	/*function fields_get($l=false) {
		if (!in_array($l, $this->langs)) $l=current($this->langs);
		$this->org_fields=array();
		$result=axs_db_query('SELECT `field`, `type`, `label_'.$l.'` AS `label`'."\n".
		'	FROM `'.$this->table.'_profiles_header` ORDER BY `rank` ASC, `id` ASC', 1, $this->db, __FILE__, __LINE__);
		foreach ($result as $cl) {
			$cl['size']=255;
			$this->org_fields[$cl['field']]=$cl;
			}
		$this->org_fields['id']['readonly']='readonly';
		$this->org_fields['disabled']['value']=1;
		unset($this->org_fields['id']['size'], $this->org_fields['disabled']['size'], $this->org_fields['updated']['size']);
		return $this->org_fields;
		} # </fields_get()>*/
	function currencys_get() {
		$this->currency_table=array();
		foreach (axs_db_query('SELECT * FROM `'.$this->table.'_currency` WHERE !disabled ORDER BY currency ASC', 1, $this->db, __FILE__, __LINE__) as $cl) $this->currency_table[$cl['id']]=$cl;
		} # </currencys_get()>
	function currencys_select($value) {
		if (!isset($this->currency_table)) $this->currencys_get();
		$value=($value=='id') ? $value:'currency';
		$select=array();
		foreach ($this->currency_table as $cl) $select[]=array('value'=>$cl[$value], 'label'=>$cl['currency'], );
		return $select;
		} # </currencys_select()>
	function org_get($org_id, $l, $prefix='', $values=true, $labels=true) {
		global $axs;
		//if (!in_array($l, $this->langs)) $l=current($this->langs);
		$this->header=new axs_form_header($this->site_nr);
		$this->header->sql_header_get($this->name, 'profiles');
		$this->fields=$this->header->structure_get();
		$out=array();
		if ($values) {
			$cl=axs_db_query('SELECT * FROM `'.$this->table.'_profiles` WHERE `id`=\''.intval($org_id).'\'', 'row', $this->db, __FILE__, __LINE__);
			foreach ($this->fields as $k=>$v) {
				$out[$prefix.$k]=(isset($cl[$k.'_'.$l])) ? $cl[$k.'_'.$l]:axs_get($k, $cl);
				if ($values=='html') $out[$prefix.$k]=nl2br(axs_html_safe($out[$prefix.$k]));
				}
			}
		if ($labels) {
			foreach ($this->fields as $k=>$v) $out[$prefix.$k.'_lbl']=$v['label'];
			}
		if (!$out[$prefix.'name']) axs_log(__FILE__, __LINE__, 'content', 'Empty data @ org_get($org_id="'.$org_id.'")');
		return $out;
		} # </org_get()>
	function org_list($l) {
		global $axs;
		$result=axs_db_query('SELECT * FROM `'.$this->table.'_profiles` ORDER BY `id`', 1, $this->db, __FILE__, __LINE__);
		$list=array();
		foreach ($result as $cl) $list[$cl['id']]=array('value'=>$cl['id'], 'label'=>$cl['name_'.$l], 'label.html'=>axs_html_safe($cl['name_'.$l]), );
		return $list;
		} # </org_list()>
	function vat_add($num, $percent, $separator='') {
		$num+=($num/100)*$percent;
		$num=($separator) ? str_replace(' ', $separator, number_format($num, 2 ,'.', ' ')):round($num, 2);
		return $num;
		} # </vat_add()>
	function vat_by_code($code) {
		if (!isset($this->vat_table)) $this->vat_table_get();
		return $this->vat_table[$code]['vat'];
		}
	function vat_sum($num, $percent, $separator='') {
		$num=($num/100)*$percent;
		$num=($separator) ? str_replace(' ', $separator, number_format($num, 2 ,'.', ' ')):round($num, 2);
		return $num;
		} # </vat()>
	function vat_table_get() {
		$this->vat_table=array();
		foreach (axs_db_query('SELECT code, vat, disabled FROM `'.$this->table.'_vat` ORDER BY rank ASC', 1, $this->db, __FILE__, __LINE__) as $cl) $this->vat_table[$cl['code']+0]=$cl;
		return $this->vat_table;
		} # </vat_types_get()>
	function vat_types_get($value) {
		if (!isset($this->vat_table)) $this->vat_table_get();
		$array=array();
		foreach ($this->vat_table as $cl) if (!$cl['disabled']) $array[]=array('value'=>$cl[$value], 'label'=>$cl['vat'].'%', );
		return $array;
		} # </vat_types_get()>
	} # </class:axs_business>
#25.06.2009 ?>