{$order_nr.lbl}: {$order_nr}

{$client_name.lbl}: {$client_name}
{$client_phone.lbl}: {$client_phone}
{$client_address.lbl}: {$client_address} {$client_postcode}
{$client_email.lbl}: {$client_email}

{$time.lbl}: {$date}
{$ref.txt} {$order_url}

 {$nr.abbr}	|{$content.lbl}	|{$amount.lbl}	|{$price.lbl}	|{$vat.abbr}	|{$sum.lbl}
{$rows}
{$sum.lbl}: {$sum.fmt}
{$sum.vat.txt}: {$sum.vat.fmt}
{$sum+vat.txt}: {$sum+vat.fmt}

{$org.name}
{$org.reg_nr.lbl} {$org.reg_nr}
{$org.vat_nr.lbl} {$org.vat_nr}
{$org.address.lbl} {$org.address}
{$org.phone.lbl} {$org.phone}
{$org.bank.lbl} {$org.bank}
{$org.iban.lbl} {$org.iban}
{$org.swift.lbl} {$org.swift}
{$org.web.lbl} {$org.web}
{$org.email.lbl} {$org.email}
