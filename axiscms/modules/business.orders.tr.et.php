<?php #22.04.2009
return array(
	'module_title'=>'Tellimused',
	'form_order_lbl'=>'Tellimused',
	'form_order_edit_title_lbl'=>'tellimus',
	
	'order_status_-_lbl'=>' - ',
	'order_status_accept_lbl'=>'t&ouml;&ouml;s',
	'order_status_problem_lbl'=>'probleem',
	'order_status_complete_lbl'=>'valmis',
	'order_status_cancel_lbl'=>'t&uuml;histatud',
	
	'order_by_lbl'=>'sorteeri',
	'asc_lbl'=>'kasvavalt',
	'desc_lbl'=>'kahanevalt',
	'search_lbl'=>'otsi',
	'found_lbl'=>'Leitud',
	
	'table_title'=>'Tellimused. Tellimuse detailide avamiseks vajutage tellimuse numbrile.',
	'doc_lbl'=>'kasutusjuhend',
	'add_lbl'=>'Lisa uus',
	'back_lbl'=>'Tagasi',
	'edit_lbl'=>'muuda',
	'top_lbl'=>'&uuml;les',
	'sum_total_lbl'=>'Kokku',
	
	);
#22.04.2009 ?>