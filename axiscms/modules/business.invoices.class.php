<?php #2022-03-16
class axs_business_invoices {
	var $module_base='business';
	var $module='invoices';
	static $d='module.business_invoice/';
	var $dir='module.business_invoice/';
	static $nr_periods=array('y'=>array('y'), 'm'=>array('m','y'), /*'w'=>'w',*/ 'd'=>array('d','m','y'), 'c'=>array()/*continous*/, );
	var $cols=array(
		'header'=>array(
			'time'=>'', 'nr'=>'', 'type'=>array('debt', 'credit', ), 'profile_id'=>'', 
			'client_id'=>'', 'client_name'=>'', 'client_address'=>'', 'client_city'=>'', 'client_country'=>'', 'client_postcode'=>'', 'client_email'=>'',
			'payment_period'=>'', 'payment_method'=>'', 'currency'=>'', 'sum_text'=>'', 'transaction_id'=>'', 'revenue'=>'',
			'compiler_id'=>'', 'compiler_name'=>'', 'order_id'=>'', 'order_time'=>'', 'status'=>array('', 'draft', 'problem', 'cancel', ),
			'updated'=>'', 'updated_uid'=>'',
			),
		'rows'=>array(
			'nr'=>'', 'product_id'=>'', 'product_form'=>'', 'text'=>'', 'price'=>'', 'vat'=>'', 'amount'=>'', 'amount_unit'=>'', 'updated'=>'', 'updated_uid'=>'',
			),
		);
	var $list=array();
	function __construct($site_nr=1, $l=false, $tr=true) { # class constructor
		global $axs;
		$this->error=array();
		$this->site_nr=$site_nr;
		$this->px=$axs['cfg']['site'][$this->site_nr]['prefix'];
		$this->db=$axs['cfg']['site'][$this->site_nr]['db'];
		$this->table=$this->px.$this->module_base.'_'.$this->module;
		$this->l=($l) ? $l:$axs['l'];
		$this->header=new axs_form_header(array('site_nr'=>$this->site_nr));
		$this->header->sql_header_get($this->module_base, $this->module);
		$this->tr=$tr;
		if ($this->tr===true) $this->tr=new axs_tr(dirname(__FILE__).'/', str_replace('_', '.', get_class($this)).'.class.tr', $l=false, $l_default=false);
		if (is_array($this->tr)) $this->tr=new axs_tr($this->tr);
		$this->invoice_files=axs_dir('content').self::$d;
		$this->invoice_data=array();
		require_once(axs_dir('modules').'business.class.php');
		$this->org=new axs_business($this->site_nr);
		} # </__construct()>
	function _esc($k, $v, $html=true) {
		if ($html) {	return (preg_match('/\.html$/', $k)) ? $v:htmlspecialchars($v);	}
		else {	return (preg_match('/\.html$/', $k)) ? strip_tags($v):$v;	}
		} #</_esc()>
	function log($function, $line, $msg) { # <Log errors />
		if (is_array($msg)) {	$msg=(count($msg)>1) ? "\n".implode("	\n", $msg):implode("	\n", $msg);	}
		axs_log(__FILE__, $line, 'invoice', $msg=get_class($this).'::'.$function.'() line '.$line.': '.$msg);
		return $msg;
		} # </log()>
	static function nr_alpha($n, $baseArray=array('0', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z')) {
		$l=count($baseArray);
		$s='';
		for ($i=1; $n>=0 && $i<10; $i++) {
			$s=$baseArray[($n%pow($l, $i)/pow($l, $i-1))].$s;
			$n-=pow($l, $i);
			}
		return $s;
		} #</nr_alpha()> 
	function nr_display($cl) { # <get formatted invoice nr />
		$f=$this->org->org_get_profile($cl['profile_id'], 'invoice_nr_format');
		$a=array();
		if (axs_tpl_has($f, 'nr')) $a['nr']=$cl['nr'];
		foreach (array(1,2,3,4,5,6,7,8,9,10) as $v) if (axs_tpl_has($f, 'nr.'.$v)) {	$a['nr.'.$v]=substr($cl['nr']+intval('1'.str_repeat('0', $v)), 1);	break;	}
		if (axs_tpl_has($f, 'nr.a')) $a['nr.a']=self::nr_alpha($cl['nr']);
		if (axs_tpl_has($f, 'nr.hex')) $a['nr.hex']=dechex($cl['nr']);
		foreach (array('y', 'Y', 'm', 'd', ) as $v) if (axs_tpl_has($f, $v)) $a[$v]=date($v, $cl['time']);
		$a=axs_tpl_parse($f, $a);
		return (strlen($a)) ? $a:$cl['nr'];
		} # </nr_display()>
	function nr_get($cl) { # <get last invoice nr for given time />
		global $axs;
		if (empty($cl['profile_id'])) {
			$this->log(__FUNCTION__, __LINE__, 'Empty business profile_id!');
			return false;
			}
		if (empty($cl['time'])) $cl['time']=$axs['time'];
		if (is_array($cl['time'])) {
			//$d=(array)date_parse($cl['time']);
			$cl['time']=mktime(intval(axs_get('h', $cl['time'])), intval(axs_get('i', $cl['time'])), intval(axs_get('s', $cl['time'])), intval(axs_get('m', $cl['time'])), intval(axs_get('d', $cl['time'])), intval(axs_get('y', $cl['time'])));
			}
		$period=$this->org->org_get_profile($cl['profile_id'], 'invoice_nr_period');
		if (!isset(self::$nr_periods[$period])) return $this->log(__FUNCTION__, __LINE__, 'Invalid invoice nr_period="'.$period.'" @ profile_id="'.$cl['profile_id'].'"!');
		$where=array("`profile_id`='".($cl['profile_id']+0)."'", );
		if (!empty($cl['id'])) $where[]="`id`!='".($cl['id']+0)."'";
		if (self::$nr_periods[$period]) {
			if (is_numeric($cl['time'])) $cl['time']=array('y'=>date('Y', $cl['time']), 'm'=>date('m', $cl['time']), 'd'=>date('d', $cl['time']));
			$min=array('d'=>1, 'm'=>1, 'y'=>$cl['time']['y']);
			$max=array('d'=>31, 'm'=>12, 'y'=>$cl['time']['y']);
			foreach (self::$nr_periods[$period] as $k=>$v) $min[$v]=$max[$v]=$cl['time'][$v];
			$min=mktime(0, 0, 0, $min['m'], $min['d'], $min['y']);
			$max=mktime(23, 59 , 59, $max['m'], $max['d'], $max['y']);
			$where[]="`time` BETWEEN '".$min."' AND '".$max."'";
			//$q="SELECT `nr` FROM `".$this->px."invoice` WHERE ".implode(' AND ', $where).' LIMIT 1';
			//echo dbg(date('d.m.Y H:i:s',$min),date('d.m.Y H:i:s',$max),date('d.m.Y H:i:s',1441918800),$q,axs_db_query($q, 'cell', $this->db, __FILE__, __LINE__));
			}
		return axs_db_query("SELECT MAX(`nr`) FROM `".$this->px.$this->module_base.'_'.$this->module."` WHERE ".implode(' AND ', $where), 'cell', $this->db, __FILE__, __LINE__);
		} # </nr_get()>
	function save($id, $header=array(), $rows=array(), $select=array()) { # <insert or edit invoice />
		if ($id) {
			$id+=0;
			if ($id) $id=axs_db_query("SELECT `id` FROM `".$this->px.$this->module_base.'_'.$this->module."` WHERE `id`='".$id."'", 'cell', $this->db, __FILE__, __LINE__);
			if (!$id) return $this->log(__FUNCTION__, __LINE__, 'Invalid invoice id="'.func_get_arg(0).'"');
			}
		if ($select['table']) $select['table']=$this->px.$select['table'];
		if ($select['id']) $select['id']+=0;
		$nr=0;
		if (!$id) { # if no invoice exists get new invoice nr and insert new invoice record
			$nr=$this->nr_get($header)+1;
			$id=axs_db_query("INSERT INTO `".$this->px.$this->module_base.'_'.$this->module."` SET `nr`='".$nr."'", 'insert_id', $this->db, __FILE__, __LINE__);
			if (!empty($rows)) foreach ($rows as $rowid=>$row) $rows[$rowid]['id']=0;
			}
		if (!empty($header)) { # update invoice data
			if ($select['id']) {
				$join=", `".$select['table']."`";
				$join_where="AND `".$select['table']."`.`id`='".$select['id']."'";
				}
			else $join=$join_where='';
			$cl=array();
			foreach ($header as $k=>$v) {
				if (isset($this->cols['header'][$k])) {
					if ($select['id']) {
						if ($v!==false) {	$header[$k]=(is_array($v)) ? "t.`".$k."`=".current($v):"t.`".$k."`='".addslashes($v)."'";	}
						else $header[$k]="t.`".$k."`=`".$select['table']."`.`".$k."`";
						}
					else $header[$k]="t.`".$k."`='".addslashes($v)."'";
					}
				else unset($header[$k]);
				}
			$cl=implode(', ', $header);
			//exit('<pre>UPDATE `'.$this->px.'invoice` AS t'.$join."\n".'	SET '.$cl."\n".'	WHERE t.id=\''.$id.'\' '.$join_where);
			axs_db_query("UPDATE `".$this->px.$this->module_base.'_'.$this->module."` AS t ".$join."\n".
			"	SET ".$cl."\n"."	WHERE t.`id`='".$id."' ".$join_where, '', $this->db, __FILE__, __LINE__);
			}
		# insert or update rows
		$fs=new axs_filesystem('', $this->site_nr);
		if (!empty($rows)) foreach ($rows as $rowid=>$row) {
			if ($row['id']) { # update
				$q=array();
				foreach ($row as $k=>$v) {
					if (isset($this->cols['rows'][$k])) $q[$k]=$k.'=\''.addslashes($v).'\'';
					}
				$q=implode(', ', $q);
				axs_db_query("UPDATE `".$this->px.$this->module_base.'_'.$this->module."_table` SET ".$q."\n"."	WHERE `id`='".$row['id']."'", $this->db, __FILE__, __LINE__);
				}
			else { # insert
				$q=array();
				foreach ($row as $k=>$v) {
					if (isset($this->cols['rows'][$k])) {
						if ($select['rows'][$rowid]) {	$q[$k]=($v) ? '\''.addslashes($v).'\'':'s.'.$k;	}
						/*		(is_array($v)) ? 's.'.$k.'='.current($v):'t.'.$k.'=\''.addslashes($v).'\'';
							}
						else $q[$k]='t.'.$k.'=`'.$select['table'].'_table`.`'.$k.'`';
							}*/
						else $q[$k]="'".addslashes($v)."'";
						}
					}
				$cl=implode(', ', array_keys($q));
				$q=implode(', ', $q);
				$from_where=(!$select['rows'][$rowid]) ? '':
					"FROM `".$select['table']."_table` AS s WHERE s.`id`='".$select['rows'][$rowid]."'";
				$qry="INSERT INTO `".$this->px.$this->module_base.'_'.$this->module."_table` (_parent_id, ".$cl.") SELECT '".$id."', ".$q."\n".$from_where;
				//print '<pre>'; exit(var_dump($row['img'], $this->invoice_files.$id.'-'.$insert_id.'.jpg'));
				$insert_id=axs_db_query($qry, 'insert_id', $this->db, __FILE__, __LINE__);
				if ($row['img']) $fs->copy($row['img'], $this->invoice_files.$id.'-'.$insert_id.'.jpg', '');
				}
			}
		return $id;
		} # </save()>
	function doc_output($data, $format, $stream=false) {
		if ($format==='txt') return $this->export_txt($data['id']);
		if ($format==='pdf') return $this->export_pdf($data['id'], $stream);
		} #</doc_output()>
	static function doc_output_filename($lbl, $nr, $format) {
		return axs_order::doc_output_filename($lbl, $nr, $format);
		} #</doc_output_filename()>
	function invoice_get($id, $where=array()) { # <get invoice data as array />
		global $axs;
		#if (!$reload && !empty($this->invoice_data)) return $this->invoice_data;
		$id=intval($id);
		foreach ($where as $k=>$v) $where[$k]=($v) ? " AND h.`".$k."`='".intval($v)."'":'';
		$this->invoice_data=array('header'=>array('sum'=>0.00, 'sum.vat'=>0.00, 'sum.total'=>0.00, ), 'rows'=>array(), 'org'=>array(), );
		$keys=array('label'=>'lbl', 'comment'=>'comment', 'txt'=>'html', );
		foreach ($this->header->structure_get() as $k=>$v) {
			foreach ($keys as $kkk=>$vvv) $this->invoice_data['header'][$k.'.'.$vvv]=$v[$kkk];
			if ($v['type']==='table') foreach ($v['options'] as $kk=>$vv) foreach ($keys as $kkk=>$vvv) $this->invoice_data['header'][$k.'.'.$kk.'.'.$vvv]=$vv[$kkk];
			}
		$q=array();
		foreach ($this->cols['header'] as $k=>$v) $q['h-'.$k]='h.`'.$k.'`';
		foreach ($this->cols['rows'] as $k=>$v) $q['r-'.$k]='r.`'.$k.'`';
		$q['r-nr']='r.`nr` AS `row_nr`';
		unset($q['r-nr']);
		$q=axs_db_query("SELECT h.`id` AS `invoice_id`, ".implode(", ", $q).", r.`id`, u.`label_".$this->l."` AS `amount_unit.txt`, b.`comment_".$this->l."` AS `payment_method.fmt`, c.`symbol` AS `currency.symbol`\n".
		"	FROM `".$this->table."` AS h\n".
		"	LEFT JOIN `".$this->table."_table` AS r ON r.`_parent_id`=h.`id`\n".
		"	LEFT JOIN `".$this->px.$this->module_base."_units` AS u ON u.`id`=r.`amount_unit`\n".
		"	LEFT JOIN `".$this->px.$this->module_base."_currency` AS c ON c.`currency`=h.`currency`\n".
		"	LEFT JOIN `".$this->px."banklink` AS b ON b.`id`=h.`payment_method`\n".
		"	WHERE h.`id`='".$id."'".implode($where)." ORDER BY r.`nr` ASC, r.`text` ASC", 1, $this->db, __FILE__, __LINE__);
		$this->invoice_data['id']=$this->invoice_data['header']['id']=$q[0]['invoice_id'];
		foreach ($this->cols['header']+array('payment_method.fmt'=>'', 'currency.symbol'=>'', ) as $k=>$v) $this->invoice_data['header'][$k]=$q[0][$k];
		$this->invoice_data['header']['client_country.txt']=axs_country::get($this->invoice_data['header']['client_country'], '');
		$nr=1;
		foreach ($q as $cl) {
			$cl['nr']=$nr++;
			$cl['img']=(file_exists($tmp=$this->invoice_files.$this->invoice_data['header']['id'].'-'.$cl['id'].'.jpg')) ? $tmp:'';
			$cl['sum']=$cl['price']*$cl['amount'];
			$cl['sum.vat']=($cl['sum']/100)*$cl['vat'];
			$cl['sum+vat']=$cl['sum']+$cl['sum.vat'];
			$cl['amount.fmt']=preg_replace('/\.0+/', '', $cl['amount']);
			foreach (array('price', 'sum.vat', 'sum', 'sum+vat') as $v) $cl[$v.'.fmt']=number_format($cl[$v], 2, '.', ' ');
			$cl['vat.fmt']=($cl['vat']>0) ? $cl['vat'].'%':'-';
			$this->invoice_data['header']['sum']+=$cl['sum'];
			$this->invoice_data['header']['sum.vat']+=$cl['sum.vat'];
			$this->invoice_data['header']['sum.total']+=$cl['sum']+$cl['sum.vat'];
			$cl['sum.vat']=round($cl['sum.vat'], 2);
			foreach (array('currency'=>'', 'currency.symbol'=>'', ) as $k=>$v) $cl[$k]=axs_get($k, $this->invoice_data['header']);
			foreach (array(
				'id'=>'','nr'=>'','img'=>'','price.fmt'=>'','vat.fmt'=>'','sum.vat'=>'','sum'=>'','sum.fmt'=>'','sum+vat'=>'','sum+vat.fmt'=>'','currency'=>'','currency.symbol'=>'','amount.fmt'=>'', 'amount_unit.txt'=>'',
				)+$this->cols['rows'] as $k=>$v) $this->invoice_data['rows'][$cl['id']][$k]=$cl[$k];
			}
		$this->invoice_data['nr']=$this->invoice_data['header']['invoice_nr']=$this->nr_display($q[0]);
		if ($this->invoice_data['header']['order_id']) $this->invoice_data['header']['order_nr']=axs_order::nr_make($q[0]['order_id'],$q[0]['order_time']);
		else $this->invoice_data['header']['order_nr']='';
		$this->invoice_data['header']['date']=date('d.m.Y', $q[0]['time']);
		$this->invoice_data['header']['payment_period_txt']=($this->invoice_data['header']['payment_period']) ? 
			$this->invoice_data['header']['payment_period'].' '.$this->tr->t('payment_period_unit_lbl'):
			$this->tr->t('payment_period_prepayment_lbl');
		#foreach (array('compiler_name') as $v) if (!$this->invoice_data['header'][$v]) $this->tr[$v.'_lbl']='';
		$this->invoice_data['header']['sum.vat']=round($this->invoice_data['header']['sum.vat'], 2);
		$this->invoice_data['header']['sum.total']=round($this->invoice_data['header']['sum.total'], 2);
		foreach (array('sum', 'sum.vat', 'sum.total', 'revenue') as $v) $this->invoice_data['header'][$v.'.fmt']=number_format($this->invoice_data['header'][$v], 2, '.', ' ');
		$tmp=-1;
		if ($this->invoice_data['header']['revenue']!=0) $tmp=0;
		if ($this->invoice_data['header']['revenue']==$this->invoice_data['header']['sum.total']) $tmp=1;
		$this->invoice_data['paid']=$this->invoice_data['header']['paid']=$tmp;
		# <Org data>
		$this->invoice_data['org']=$this->org->org_get($this->invoice_data['header']['profile_id'], $this->l, 'org.');
		# </Org data>
		if (!$this->invoice_data['id']) $this->log(__FUNCTION__, __LINE__, 'Invalid invoice id="'.func_get_arg(0).'"');
		return $this->invoice_data;
		} # </invoice_get()>
	function invoice_get_compact($data, $html=true) {
		$vr=array();
		foreach ($data['header'] as $k=>$v) $vr[$k]=$this->_esc($k, $v, $html);
		$vr['rows']=array();
		foreach ($data['rows'] as $k=>$v) foreach ($v as $kk=>$vv) $vr['rows'][$k][$kk]=($html) ? htmlspecialchars($vv):$vv;
		foreach ($data['org'] as $k=>$v) $vr[$k]=$this->_esc($k, $v, $html);
		return $vr;
		} #</doc_get_compact()>
	private function invoice_ascii($id, $format, $tpl='') { # <get invoice in HTML format />
		global $axs;
		$formats=array(
			'txt'=>array('mime'=>'text/plain', 'tpl'=>'txt'),
			'html'=>array('mime'=>'text/html', 'tpl'=>'tpl'),
			'pdf'=>array('mime'=>'application/pdf', 'tpl'=>'tpl'),
			);
		if (empty($this->invoice_data)) $this->invoice_get($id);
		foreach ($tpl=array(''=>'', 'row'=>'.row', ) as $k=>$v) $tpl[$k]=axs_tpl(false, str_replace('_', '.', $this->module_base.'.'.$this->module.'.export'.$v.'.'.$formats[$format]['tpl']));
		$data=array('charset'=>$axs['cfg']['charset'], 'l'=>$this->l, 'base_href'=>realpath(AXS_SITE_ROOT).'/', 'rows'=>'');
		foreach ($this->invoice_data['rows'] as $k=>$v) {
			if ($format==='html') {
				$v['text']=nl2br($v['text']);
				$v['img_html']=($v['img']) ? '<img src="'.htmlspecialchars($v['img']).'" alt="'.htmlspecialchars(basename($v['img'])).'" />':'';
				}
			else $v['img_html']='';
			$data['rows'].=axs_tpl_parse($tpl['row'], $v);
			}
		if ($format==='html') $this->invoice_data['org']=$this->org->org_get($this->invoice_data['header']['profile_id'], $this->l, 'org.', 'html');
		return axs_tpl_parse($tpl[''], $data+$this->invoice_data['header']+$this->invoice_data['org']+$this->tr->get());
		} # </invoice_ascii()>
	function export_html($id, $tpl='') { # <get invoice in HTML format />
		return $this->invoice_ascii($id, $format='html', $tpl);
		} # </export_html()>
	function export_pdf($id, $stream=false) { # <get invoice in PDF format />
		if (empty($this->invoice_data)) $this->invoice_get($id);
		require_once(axs_dir('plugins').'dompdf/dompdf_config.inc.php');
		$pdf=new DOMPDF();
		$pdf->set_protocol('file://');
		$pdf->load_html($this->export_html($id));
		$pdf->render();
		if ($stream) {
			if ($stream===true) $stream=preg_replace('/^www\./', '', $_SERVER['SERVER_NAME']).'-'.strtolower(axs_valid::f_name($this->tr->t('invoice_nr_lbl'))).'-'.$this->invoice_data['header']['invoice_nr'].'.pdf';
			exit($pdf->stream($stream));
			}
		else return $pdf->output();
		} # </export_pdf()>
	function export_txt($id, $tpl='') { # <get invoice in TXT format />
		global $axs;
		$txt=$this->invoice_ascii($id, $format='txt', $tpl);
		return html_entity_decode(strip_tags($txt), ENT_QUOTES, $axs['cfg']['charset']);
		} # </export_txt()>
	} # </class::axs_business_invoices>
/*
CREATE TABLE `axs_business_invoices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `time` int(10) unsigned NOT NULL DEFAULT '0',
  `type` enum('','debt','credit') CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `nr` smallint(5) unsigned NOT NULL DEFAULT '0',
  `profile_id` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT '=business_profiles.id',
  `client_id` int(10) unsigned NOT NULL DEFAULT '0',
  `client_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_estonian_ci NOT NULL,
  `client_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_estonian_ci NOT NULL,
  `client_postcode` varchar(255) CHARACTER SET utf8 COLLATE utf8_estonian_ci NOT NULL,
  `payment_period` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `payment_method` tinyint(4) DEFAULT NULL,
  `currency` char(3) CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
  `transaction_id` int(10) unsigned NOT NULL DEFAULT '0',
  `revenue` decimal(7,2) unsigned NOT NULL DEFAULT '0.00',
  `compiler_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'compiled by (=users.id)',
  `compiler_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_estonian_ci NOT NULL COMMENT 'compiled by (person''s name)',
  `order_id` int(10) unsigned NOT NULL DEFAULT '0',
  `order_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '=order.time',
  `status` enum('','draft','problem','cancel') CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '-',
  `updated` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'modified timestamp',
  `updated_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '=users.id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE `axs_business_invoices_table` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `_field` enum('rows') CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT 'rows',
  `_parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `nr` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0',
  `product_form` enum('', 'articles_products') CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '' COMMENT 'which table is product_id from',
  `text` varchar(255) CHARACTER SET utf8 COLLATE utf8_estonian_ci NOT NULL,
  `price` decimal(7,2) NOT NULL DEFAULT '0.00',
  `vat` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'vat %',
  `amount` decimal(12,2) unsigned NOT NULL DEFAULT '0.00',
  `amount_unit` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '=business_units.id',
  `updated` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'modified timestamp',
  `updated_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '=users.id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
*/
#2008-12-03 ?>