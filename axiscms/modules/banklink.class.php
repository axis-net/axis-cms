<?php #2020-05-29
class banklink {
	public $name='banklink';
	public $error=array();
	public $list=array();
	public $bank_id=0;
	static $banks=array(
		'-'=>array('title'=>array('en'=>'Payment by invoice', 'et'=>'Tasumine arve alusel', 'ru'=>'Оплата по счету', ),	)
		);
	public $form=array(
		'order_nr'=>array('k'=>'order_nr', 'len'=>20, 'req'=>1, ),
		'ref'=>array('k'=>'ref', 'len'=>20, 'req'=>0, ),
		'msg'=>array('k'=>'msg', 'len'=>20, 'req'=>0, ),
		'transaction_id'=>array('k'=>'transaction_id', 'len'=>20, 'req'=>0, 'val'=>'', ),
		'amount'=>array('k'=>'amount', 'len'=>10, 'req'=>0, 'val'=>0, ),
		);
	public $log_fields=array('bank_id'=>0, 'status'=>'ERROR', 'authentic'=>0, 'order_nr'=>'', 'msg'=>'', 'amount'=>0, 'ref'=>'', 'card_token'=>'', );
	function __construct($site_nr=1) {
		global $axs;
		$this->dir_http=axs_dir('modules', 'h');
		$this->l=$axs['l'];
		$this->charset=$axs['cfg']['charset'];
		$this->db=$axs['cfg']['site'][$site_nr]['db'];
		$this->px=$axs['cfg']['site'][$site_nr]['prefix'];
		} #</__construct()>
	function log($reply, $data) { #Log transactions
		foreach ($this->log_fields as $k=>$v) if (!isset($reply[$k])) $reply[$k]=$v;
		axs_db_query(array(
			'INSERT INTO '.$this->px.$this->name.'_log (bank_id, status, order_nr, msg, amount, ref, card_token, reply_data, client_ip)'."\n".
			'VALUES (:bank_id, :status, :order_nr, :msg, :amount, :ref, :card_token, :reply_data, :client_ip)',
			array(
				':bank_id'=>$reply['bank_id'],
				':status'=>$reply['status'],
				':order_nr'=>$reply['order_nr'],
				':msg'=>$reply['msg'],
				':amount'=>$reply['amount'],
				':ref'=>$reply['ref'],
				':card_token'=>$reply['card_token'],
				':reply_data'=>$data,
				':client_ip'=>$_SERVER['REMOTE_ADDR'],
				),
			), '', $this->db, __FILE__, __LINE__);
		} #</log()>
	function log_error($file, $line, $msg, $reply_data=false) {
		if (is_array($msg)) {	$msg=(count($msg)>1) ? "\n".implode("	\n", $msg):implode("	\n", $msg);	}
		$this->log(array(
			'bank_id'=>$this->bank_id,
			'status'=>'ERROR',
			'msg'=>$msg."\n\n".'File: '.$file.'('.$line.')'."\n".'Request: '.$_SERVER['REQUEST_URI'],
			), ($reply_data===false) ? json_encode($_POST):$reply_data);
		$this->error=[];
		} #</log_error()>
	function bank_get($id) { #<Get bank data />
		$id=intval($id);
		$data=axs_db_query("SELECT *, `title_".$this->l."` AS `title`, `comment_".$this->l."` AS `comment` FROM `".$this->px.$this->name."` WHERE `_enabled`=1 AND `id`='".$id."'", 'row', $this->db, __FILE__, __LINE__);
		if (empty($data)) return $this->log_error(__FILE__, __LINE__, 'Bank not found');
		$this->bank_id=$data['id'];
		$this->bank_get_data($data);
		if ($data['protocol']) {
			if (!file_exists($tmp=__DIR__.'/'.$this->name.'.'.$data['protocol'].'.class.php')) return $this->log_error(__FILE__, __LINE__, 'Protocol not found: "'.$tmp.'"');
			require_once($tmp);
			$tmp=get_class($this).'_'.$data['protocol'];
			$this->bank=new $tmp($data);
			$this->bank->bank['l']=(isset($this->bank->bank['language'][$this->l])) ? $this->bank->bank['language'][$this->l]:current($this->bank->bank['language']);
			}
		else {
			$this->bank=new stdClass;
			$this->bank->bank=array_merge(self::$banks['-'], $data);
			$this->bank->form=$this->form;
			}
		$this->bank->p=$this;
		return $this->bank_id;
		} #</bank_get()>
	function bank_get_data(&$data) {
		$data['protocol']=preg_replace('/[^a-z0-9]/', '', $data['protocol']);
		$tmp=str_replace('-test', '', strtolower($data['bank']));
		$data['class']=trim($data['protocol'].' '.$data['class']);
		$data['img']='';
		foreach (array('png'=>'', 'svg'=>'', ) as $k=>$v) {
			if (file_exists(__DIR__.'/'.$this->name.'.'.$tmp.'.'.$k)) {	$data['img']=$this->dir_http.$this->name.'.'.$tmp.'.'.$k;	break;	}
			}
		$data['logo']=($data['img']) ? '<img class="logo" src="'.htmlspecialchars($data['img']).'" alt="'.htmlspecialchars($data['title']).'" />':'<span class="logo">'.htmlspecialchars($data['title']).'</span>';
		} #</bank_get_data()>
	function form_html($id, $order_data) { # Create HTML form
		$this->bank_get($id);
		if (empty($this->bank)) return '';
		#<Clean up $order_data />
		foreach ($order_data as $k=>$v) {
			if (!in_array($k, ['order_nr', 'amount', 'currency', 'msg', 'client_email', 'client_address', 'client_city', 'client_country', 'client_postcode', 'return_url', 'cancel_url'])) unset($order_data[$k]);
			}
		$order_data['user_ip']=$_SERVER['REMOTE_ADDR'];
		$this->values=array();
		foreach ($this->bank->form as $k=>$v) {
			$key=$v['k'];
			$this->values[$key]='';
			if (isset($this->bank->bank[$key])) $this->values[$key]=$this->bank->bank[$key];
			if (isset($order_data[$key])) $this->values[$key]=$order_data[$key];
			if (isset($v['val'])) $this->values[$key]=$v['val'];
			}
		if ($this->bank->bank['protocol']) return $this->bank->form_html($order_data);
		$fields=array();
		foreach ($this->form as $k=>$v) {
			$fields[$k]=$this->values[$v['k']];
			}
		$form='';
		foreach ($fields as $k=>$v) $form.='   <input type="hidden" name="'.$k.'" value="'.htmlspecialchars($v).'" />'."\n";
		$form=
		'<form id="banklink" class="'.htmlspecialchars($this->bank->bank['class']).'" method="post" action="'.htmlspecialchars($order_data['return_url']).'" enctype="application/x-www-form-urlencoded">'."\n".
		$form.
		'	<button type="submit">'.$this->bank->bank['logo'].'</button>'."\n".
		'	<script>'."\n".
		'		if (!document.querySelector(\'form#banklink\').classList.contains(\'test\')) {'."\n".
		'			document.querySelector(\'form#banklink *[type="submit"]\').style.display="none";'."\n".
		'			document.querySelector(\'form#banklink\').submit();'."\n".
		'			}'."\n".
		'	</script>'."\n".
		'</form>'."\n";
		return $form;
		} #</form_html()>
	function form_validate(&$fields, $def, $file, $line) { #<Validate form fields />
		foreach ($fields as $k=>$v) {
			if (($def[$k]['req']) && (!strlen($v))) $this->error[]='Empty field "'.$k.'"';
			if (mb_strlen($v)>$def[$k]['len']) $this->error[]='"'.$k.'"="'.$v.'" > limit "'.$def[$k]['len'].'"';
			$fields[$k]=mb_substr($v, 0, $def[$k]['len']);
			}
		if (!empty($this->error)) $this->log_error($file, $line, $this->error);
		} #</form_validate()>
	function list_get() { #<Get list of enabled payment methods />
		$result=axs_db_query("SELECT `id`, `bank`, `class`, `protocol`, `title_".$this->l."` AS `title`, `comment_".$this->l."` AS `comment` FROM `".$this->px.$this->name."` WHERE `_enabled` ORDER BY `_nr`, `title`", 1, $this->db, __FILE__, __LINE__);
		foreach ($result as $cl) {
			$this->bank_get_data($cl);
			$this->list[$cl['id']]=$cl;
			}
		return $this->list;
		} #</list_get()>
	function reply_get($id) { #<Verify bank reply />
		$id=intval($id);
		$reply=array('bank_id'=>0, 'status'=>'ERROR', 'authentic'=>0, 'order_nr'=>'', 'ref'=>'', 'msg'=>'', 'amount'=>0, 'card_token'=>'', );
		$this->bank_get($id);
		if (empty($this->bank)) return $reply;
		$reply['bank_id']=$id;
		if ($this->bank->bank['protocol']) {
			$data=$this->bank->reply_get($reply);
			$reply['ref']=$this->bank->bank['account_ref'];
			if ($reply['authentic']) $this->log($reply, json_encode($data));
			}
		else {
			$reply['status']='OK';
			$reply['authentic']=1;
			$reply['order_nr']=(isset($_POST['order_nr'])) ? $_POST['order_nr']:'';
			$reply['msg']=(isset($_POST['msg'])) ? $_POST['msg']:'';
			$reply['amount']=0;
			}
		if (!empty($this->error)) $this->log_error(__FILE__, __LINE__, $this->error);
		return $reply;
		} #</reply_get()>
	} #</class::banklink>
/*
CREATE TABLE `banklink` (
  `id` int(10) UNSIGNED NOT NULL,
  `_nr` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `_enabled` tinyint(4) NOT NULL DEFAULT '0',
  `class` varchar(255) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `bank` varchar(255) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `protocol` varchar(255) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `title_et` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `title_en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `title_ru` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_et` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_ru` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `my_private_key` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `my_private_key_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `bank_certificate` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `my_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `account_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `account_ref` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `account_owner` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_uid` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '=users.id'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
ALTER TABLE `banklink` ADD PRIMARY KEY (`id`);
ALTER TABLE `banklink` MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
INSERT INTO `banklink` (_nr, _enabled, class, bank, protocol, title_et, title_en, title_ru) VALUES (9, 1, '', '-', '', 'Tasumine arve alusel', 'Payment by invoice', 'Оплата по счету');

CREATE TABLE `banklink_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `bank_id` tinyint(3) NOT NULL DEFAULT '0' COMMENT '=banklink.id',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(255) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `order_nr` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '=order.id',
  `msg` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'Transaction description',
  `amount` decimal(7,2) NOT NULL DEFAULT '0.00',
  `ref` varchar(255) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `card_token` varchar(255) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `reply_data` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Bank reply data JSON encoded',
  `client_ip` varchar(255) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
ALTER TABLE `banklink_log` ADD PRIMARY KEY (`id`);
ALTER TABLE `banklink_log` MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
*/
#2008-10-20 ?>