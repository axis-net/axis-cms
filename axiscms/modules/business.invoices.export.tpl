<!DOCTYPE html>
<html lang="{$l}">
<head>
<meta charset={$charset}" />
<title>{$doc_nr.lbl} {$invoice_nr}</title>
<meta name="generator" content="AXIS CMS" />
<style type="text/css">
	<!-- * {/* This stupid thing here makes UNIX/MS line break safe*/}
	body {
		margin:10mm 20mm;
		padding:0mm;
		font-family:DejaVu Sans, sans-serif;
		font-size:3mm;
		}
	/*div, span {	border:0.1mm dashed;	}*/
	table {
		width:100%;
		}
		table td {
			vertical-align:top;
			}
	#header {
		border-bottom:solid 0.1mm;
		}
		#header #logo {
			margin:0 5mm 1mm 0;
			width:30mm;
			}
		#header span {
			display:block;
			margin:-5mm 0mm 0mm 100mm;
			padding:0mm;
			height:5mm;
			text-align:right;
			}
	#invoice_header {
		margin-top:5mm;
		}
		#invoice_header * {
			vertical-align:top;
			}
		#invoice_header th {
			text-align:left;
			}
		#invoice_header .col1 {
			width:25mm;
			}
		#invoice_header .col3 {
			width:30mm;
			}
		#invoice_header .col4 {
			width:40mm;
			}
	#invoice_body {
		border-collapse:collapse;
		}
		#invoice_body th {
			padding:0mm 1mm;
			border:0.1mm solid;
			}
		#invoice_body td {
			padding:0mm 1mm;
			border-left:0.1mm solid;
			border-right:0.1mm solid;
			}
		#invoice_body td.col2 {
			border-right:none;
			padding:0mm;
			}
			#invoice_body td.col2 img {
				margin:1mm;
				width:15mm;
				}
		#invoice_body td.col3 {	border-left:none;	}
		#invoice_body td.col1,
		#invoice_body td.col4,
		#invoice_body td.col5,
		#invoice_body td.col6,
		#invoice_body td.col7 {
			text-align:right;
			}
		#invoice_body tfoot th {
			text-align:right;
			}
		#invoice_body tfoot th,
		#invoice_body tfoot td {
			border:none;
			}
		#invoice_body tfoot .col7 {
			border-left:0.1mm solid;
			border-right:0.1mm solid;
			}
		#invoice_body tfoot .row1 th,
		#invoice_body tfoot .row1 td {
			border-top:0.1mm solid;
			}
		#invoice_body tfoot .row3 td.col7 {
			border-bottom:0.1mm solid;
			}
	#footer {
		margin-top:10mm;
		border-top:0.1mm solid;
		}
	-->
</style>
</head>

<body>
<header id="header">
	<img id="logo" src="{$base_href}axs_site/gfx/logo.png" alt="{$org.name}" />
	<span></span>
</header>

<table id="invoice_header" cellspacing="0">
 <tr>
  <th class="col1" nowrap="nowrap">{$client_name.lbl}:</th>
  <td class="col2">{$client_name}</td>
  <th class="col3" nowrap="nowrap">{$doc_nr.lbl}:</th>
  <td class="col4"><strong>{$invoice_nr}</strong></td>
 </tr>
 <tr>
   <th nowrap="nowrap">{$client_address.lbl}:</th>
   <td>{$client_address}</td>
   <th nowrap="nowrap">{$date.lbl}:</th>
   <td>{$date}</td>
 </tr>
 <tr>
   <th nowrap="nowrap">{$client_postcode.lbl}:</th>
   <td>{$client_postcode}</td>
   <th nowrap="nowrap">{$payment_period.lbl}:</th>
   <td>{$payment_period.txt}</td>
 </tr>
 <tr>
   <th nowrap="nowrap">{$client_email.lbl}:</th>
   <td>{$client_email}</td>
   <th nowrap="nowrap">{$payment_method.lbl}:</th>
   <td>{$payment_method.fmt}</td>
 </tr>
</table>

<table id="invoice_body" cellspacing="0">
 <thead>
  <tr>
   <th scope="col" class="col1">{$rows.nr.abbr}</th>
   <th scope="col" class="col2">{$rows.text.lbl}</th>
   <th scope="col" class="col4">{$rows.amount.lbl}</th>
   <th scope="col" class="col5">{$rows.price.lbl}</th>
   <th scope="col" class="col6">{$vat.abbr}</th>
   <th scope="col" class="col7">{$rows.sum.lbl}</th>
  </tr>
 </thead>
 <tbody>
  {$rows}
 </tbody>
 <tfoot>
  <tr class="row1">
   <td colspan="3"><strong>{$compiler_name.lbl}:</strong> {$compiler_name}</td>
   <th colspan="2" scope="col">{$sum.lbl}:</th>
   <td class="col7">{$sum.fmt}{$currency.symbol}</td>
  </tr>
  <tr class="row2">
   <td colspan="3"></td>
   <th colspan="2" scope="col">{$sum.vat.lbl}:</th>
   <td class="col7">{$sum.vat.fmt}{$currency.symbol}</td>
  </tr>
  <tr class="row3">
   <td colspan="3"></td>
   <th colspan="2" scope="col" valign="top">{$sum.total.lbl}:</th>
   <td class="col7"><strong>{$sum.total.fmt}{$currency.symbol}</strong></td>
  </tr>
 </tfoot>
</table>

<table id="footer" cellspacing="0">
	<tr>
		<td>
			{$org.name}<br />
			{$org.address.lbl}: {$org.address}<br />
			{$org.phone.lbl}: {$org.phone}<br />
			{$org.web.lbl}: {$org.web}<br />
			{$org.email.lbl}: {$org.email}
		</td>
		<td>
			{$org.reg_nr.lbl}: {$org.reg_nr}<br />
			{$org.vat_nr.lbl}: {$org.vat_nr}
		</td>
		<td>
			{$org.bank.lbl}: {$org.bank}<br />
			{$org.iban.lbl}: {$org.iban}<br />
			{$org.swift.lbl}: {$org.swift}
		</td>
	</tr>
</table>
</body>
</html>
