<?php #25.01.2010
if (!defined('AXS_PATH_EXT')) exit(require('../axiscms/log.php'));

# <Edit>
if (isset($_GET[$f->header->key_edit])) return $f->editor();

# Perform database query
$result=axs_db_query("SELECT SQL_CALC_FOUND_ROWS t.*, m.`user` AS `updated_text`\n".
"	FROM `".$f->table."` AS t LEFT JOIN `".$axs['cfg']['db'][1]['px']."users` AS m ON m.id=t.`updated_uid` ORDER BY `currency` ASC", 1, $f->db, __FILE__, __LINE__);

# <Display result rows>
$table='      <table class="table">'."\n".
'       <caption>'.$m->tr('form_currency_lbl').'</caption>'."\n".
'       <tr>';
foreach ($f->thead as $k=>$v) $table.='<th scope="col">'.$v['label'].'</th>';
$table.='<th>'.$f->add_new_form(array($f->header->key_edit=>'', 'b'=>'?'.$_SERVER['QUERY_STRING'])).'</th></tr>'."\n";
foreach ($result as $cl) {
	$table.='       <tr id="id'.$cl['id'].'" class="'.(($cl['disabled']) ? ' disable':'').'">';
	foreach ($f->thead as $k=>$v) $table.='<td class="'.$k.'">'.$f->header->value_display($k, $v, $cl).'</td>';
	$table.='<td class="edit"><a href="'.'?'.axs_url($f->url, array('id'=>$cl['id'], $f->header->key_edit=>$cl['id'], 'b'=>'?'.$_SERVER['QUERY_STRING'].'#id'.$cl['id'])).'">'.$m->tr('edit_lbl').'</a></td></tr>'."\n";
	}
$table.='      </table>'."\n".'&nbsp;'."\n";
unset($result);
# </Display result rows>

$pager=$f->pager(axs_get('p', $_GET));
return $pager."\n".$table."\n".$pager;
/*
CREATE TABLE IF NOT EXISTS `axs_business_currency` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `currency` char(3) CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
  `disabled` tinyint(4) NOT NULL DEFAULT '0',
  `updated` int(10) unsigned NOT NULL,
  `updated_uid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

INSERT INTO `axs_business_currency` VALUES(1, 'EUR', 0, 0, 0);
INSERT INTO `axs_business_currency` VALUES(4, 'USD', 1, 0, 0);
*/
#08.11.2009 ?>