<?php #2020-04-24
class banklink_ipizza {
	static $banks=array(
		'EE_COOP'=>array(
			'class'=>'',
			'title'=>array('et'=>'COOP Pank', ),
			'comment'=>array('en'=>'Internetipank', 'en'=>'Online bank', ),
			'home'=>'https://www.cooppank.ee',
			'banklink_address'=>'https://i.cooppank.ee/pay',
			'currency'=>array('EUR', ),
			'charset'=>array('UTF-8', 'ISO-8859-1', ),
			'language'=>array('en'=>'ENG', 'et'=>'EST', 'ru'=>'RUS', ),
			),
		'EE_COOP-test'=>array(
			'class'=>'test',
			'title'=>array('et'=>'COOP Pank TEST', ),
			'comment'=>array('en'=>'Internetipank', 'en'=>'Online bank', ),
			'home'=>'https://www.cooppank.ee',
			'banklink_address'=>' https://secure.cooppank.ee/pay',
			'currency'=>array('EUR', ),
			'charset'=>array('UTF-8', 'ISO-8859-1', ),
			'language'=>array('en'=>'ENG', 'et'=>'EST', 'ru'=>'RUS', ),
			),
		'EE_LHV'=>array(
			'class'=>'',
			'title'=>array('et'=>'LHV Pank', ),
			'comment'=>array('en'=>'Internetipank', 'en'=>'Online bank', ),
			'home'=>'https://www.lhv.ee',
			'banklink_address'=>'https://www.lhv.ee/banklink',
			'currency'=>array('EUR', ),
			'charset'=>array('UTF-8', ),
			'language'=>array('en'=>'ENG', 'et'=>'EST', ),
			),
		'EE_LUMINOR'=>array(
			'class'=>'',
			'title'=>array('et'=>'Luminor', ),
			'comment'=>array('en'=>'Internetipank', 'en'=>'Online bank', ),
			'home'=>'https://www.luminor.ee',
			'banklink_address'=>'https://banklink.luminor.ee',
			'currency'=>array('EUR', ),
			'charset'=>array('UTF-8', ),
			'language'=>array('en'=>'ENG', 'et'=>'EST', 'ru'=>'RUS', ),
			),
		'EE_LUMINOR-test'=>array(
			'class'=>'test',
			'title'=>array('et'=>'Luminor TEST', ),
			'comment'=>array('en'=>'Internetipank', 'en'=>'Online bank', ),
			'home'=>'https://www.luminor.ee',
			'banklink_address'=>'https://banklink.luminor.ee/test',
			'currency'=>array('EUR', ),
			'charset'=>array('UTF-8', ),
			'language'=>array('en'=>'ENG', 'et'=>'EST', 'ru'=>'RUS', ),
			),
		'EE_SEB'=>array(
			'class'=>'',
			'title'=>array('et'=>'SEB Pank', ),
			'comment'=>array('en'=>'SEB internet bank', 'et'=>'Siin saate maksta SEB internetipangas', ),
			'home'=>'https://www.seb.ee',
			'banklink_address'=>'https://e.seb.ee/cgi-bin/ipank/ipank.r',
			'currency'=>array('EUR', ),
			'charset'=>array('UTF-8', 'ISO-8859-1', ),
			'language'=>array('en'=>'ENG', 'et'=>'EST', ),
			),
		'EE_SEB-test'=>array(
			'class'=>'test',
			'title'=>array('et'=>'SEB TEST', ),
			'comment'=>array('en'=>'SEB internet bank', 'et'=>'Siin saate maksta SEB internetipangas', ),
			'home'=>'http://www.seb.ee/unet',
			'banklink_address'=>'https://www.seb.ee/cgi-bin/dv.sh/ipank.r',
			'currency'=>array('EUR', ),
			'charset'=>array('UTF-8', 'ISO-8859-1', ),
			'language'=>array('en'=>'ENG', 'et'=>'EST', ),
			),
		'EE_SWED'=>array(
			'class'=>'',
			'title'=>array('et'=>'Swedbank', ),
			'comment'=>array('en'=>'Swedbank internet bank', 'et'=>'Swedbank. Siin saate maksta internetipangas', ),
			'home'=>'https://www.swedbank.ee',
			'banklink_address'=>'https://www.swedbank.ee/banklink',
			'currency'=>array('EUR', ),
			'charset'=>array('UTF-8', 'ISO-8859-1', ),
			'language'=>array('en'=>'ENG', 'et'=>'EST', ),
			),
		);
	public $form=array(
		'VK_SERVICE'=>array('k'=>'service', 'len'=>4, 'req'=>1),
		'VK_VERSION'=>array('k'=>'service_version', 'len'=>3, 'req'=>1, 'val'=>'008', ),
		'VK_SND_ID'=>array('k'=>'my_id', 'len'=>15, 'req'=>1),
		'VK_STAMP'=>array('k'=>'order_nr', 'len'=>20, 'req'=>1),
		'VK_AMOUNT'=>array('k'=>'amount', 'len'=>12, 'req'=>1),
		'VK_CURR'=>array('k'=>'currency', 'len'=>3, 'req'=>1),
		'VK_ACC'=>array('k'=>'account_number', 'len'=>34, 'req'=>1),
		'VK_NAME'=>array('k'=>'account_owner', 'len'=>70, 'req'=>1),
		'VK_REF'=>array('k'=>'account_ref', 'len'=>35, 'req'=>0),
		'VK_MSG'=>array('k'=>'msg', 'len'=>95, 'req'=>1),
		'VK_RETURN'=>array('k'=>'return_url', 'len'=>255, 'req'=>1),
		'VK_CANCEL'=>array('k'=>'cancel_url', 'len'=>255, 'req'=>1),
		'VK_DATETIME'=>array('k'=>'time', 'len'=>24, 'req'=>1),
		'VK_MAC'=>array('k'=>'mac', 'len'=>700, 'req'=>1,),
		'VK_ENCODING'=>array('k'=>'charset', 'len'=>12, 'req'=>0),
		'VK_LANG'=>array('k'=>'l', 'len'=>3, 'req'=>1),
		
		'VK_T_NO'=>array('k'=>'transaction_id', 'len'=>20, 'req'=>1),
		'VK_T_DATETIME'=>array('k'=>'', 'len'=>24, 'req'=>1),
		'VK_REC_ID'=>array('k'=>'bank', 'len'=>15, 'req'=>1),
		'VK_REC_ACC'=>array('k'=>'', 'len'=>34, 'req'=>1),
		'VK_REC_NAME'=>array('k'=>'', 'len'=>70, 'req'=>1),
		'VK_SND_ACC'=>array('k'=>'', 'len'=>34, 'req'=>1),
		'VK_SND_NAME'=>array('k'=>'', 'len'=>70, 'req'=>1),
		'VK_AUTO'=>array('k'=>'', 'len'=>1, 'req'=>0),
		);
	public $services=array(1011=>'pay', 1111=>'OK', 1911=>'FAIL', );
	public $form_services=array(
		1011=>array(
			# Teenindaja saadab panka allkirjastatud maksekorralduse andmed, mida klient internetipangas muuta ei saa. 
			# Peale edukat makset koostatakse kaupmehele päring “1111”, ebaõnnestunud makse puhul “1911”
			'VK_SERVICE'=>true, #Teenuse number (1011)
			'VK_VERSION'=>true, #Kasutatav krüptoalgoritm (008)
			'VK_SND_ID'=>true, #Päringu koostaja ID (Kaupluse ID)
			'VK_STAMP'=>true, #Päringu ID
			'VK_AMOUNT'=>true, #Maksmisele kuuluv summa
			'VK_CURR'=>true, #Valuuta nimi: EUR
			'VK_ACC'=>true, #Saaja konto number
			'VK_NAME'=>true, #Saaja nimi
			'VK_REF'=>true, #Maksekorralduse viitenumber
			'VK_MSG'=>true, #Maksekorralduse selgitus
			'VK_RETURN'=>true, #URL, kuhu vastatakse edukal tehingu sooritamisel
			'VK_CANCEL'=>true, #URL, kuhu vastatakse ebaõnnestunud tehingu puhul
			'VK_DATETIME'=>true, #Päringu algatamise kuupäev ja kellaaeg DATETIME formaadis
			'VK_MAC'=>false, #Kontrollkood e. allkiri
			'VK_ENCODING'=>false, #Sõnumi kodeering. ISO-8859-1 või UTF-8 (vaikeväärtus) või WINDOWS-1257
			'VK_LANG'=>false, #Soovitav suhtluskeel (EST, ENG või RUS)
			),
		1111=>array(
			# Kasutatakse vastamiseks eestisisese maksekorralduse toimumisest.
			'VK_SERVICE'=>true, #Teenuse number (1111)
			'VK_VERSION'=>true, #Kasutatav krüptoalgoritm 008
			'VK_SND_ID'=>true, #Päringu koostaja ID (Panga ID)
			'VK_REC_ID'=>true, #Päringu vastuvõtja ID (Kaupluse ID)
			'VK_STAMP'=>true, #Päringu ID
			'VK_T_NO'=>true, #Maksekorralduse number
			'VK_AMOUNT'=>true, #Makstud summa
			'VK_CURR'=>true, #Valuuta nimi: EUR
			'VK_REC_ACC'=>true, #Saaja konto number
			'VK_REC_NAME'=>true, #Saaja nimi
			'VK_SND_ACC'=>true, #Maksja konto number
			'VK_SND_NAME'=>true, #Maksja nimi
			'VK_REF'=>true, #Maksekorralduse viitenumber
			'VK_MSG'=>true, #Maksekorralduse selgitus
			'VK_T_DATETIME'=>true, #Maksekorralduse kuupäev ja kellaaeg DATETIME formaadis
			'VK_MAC'=>false, #Kontrollkood e. allkiri
			'VK_ENCODING'=>false, #Sõnumi kodeering. ISO-8859-1 või UTF-8 (vaikeväärtus) või WINDOWS-1257
			'VK_LANG'=>false, #Soovitav suhtluskeel (EST, ENG või RUS)
			'VK_AUTO'=>false, #Y = panga poolt automaatselt saadetud vastus. N = vastus kliendi liikumisega kaupmehe lehele
			),
		1911=>array(
			# Kasutatakse ebaõnnestunud tehingust teatamiseks.
			'VK_SERVICE'=>true, #Teenuse number (1911)
			'VK_VERSION'=>true, #Kasutatav krüptoalgoritm (008)
			'VK_SND_ID'=>true, #Päringu koostaja ID (Panga ID)
			'VK_REC_ID'=>true, #Päringu vastuvõtja ID (Kaupluse ID)
			'VK_STAMP'=>true, #Päringu ID
			'VK_REF'=>true, #Maksekorralduse viitenumber
			'VK_MSG'=>true, #Maksekorralduse selgitus
			'VK_MAC'=>false, #Kontrollkood e. allkiri
			'VK_ENCODING'=>false, #Sõnumi kodeering. ISO-8859-1 või UTF-8 (vaikeväärtus) või WINDOWS-1257
			'VK_LANG'=>false, #Soovitav suhtluskeel (EST, ENG või RUS)
			'VK_AUTO'=>false, #Y = panga poolt automaatselt saadetud vastus. N = vastus kliendi liikumisega kaupmehe lehele
			),
		);
	function __construct($data) {
		$this->bank=array_merge(self::$banks[$data['bank']], $data);
		} #</__construct()>
	function form_html($order_data) {
		$fields=array();
		foreach ($this->form_services[1011] as $k=>$v) {
			$fields[$k]=(isset($this->p->values[$this->form[$k]['k']])) ? $this->p->values[$this->form[$k]['k']]:'';
			}
		$fields['VK_SERVICE']='1011';
		$fields['VK_DATETIME']=new DateTime('NOW');
		$fields['VK_DATETIME']=$fields['VK_DATETIME']->format(DateTime::ISO8601);#date('Y-m-dTH:i:sO');#date('c'); #'2013-03-13T07:21:14+0200'
		$fields['VK_ENCODING']=current($this->bank['charset']);
		foreach ($this->bank['charset'] as $v) {
			if (strtolower($v)===strtolower($this->p->charset)) {	$fields['VK_ENCODING']=$v;	break;	}
			}
		$fields['VK_MAC']='';
		$signature='';
		$fields['VK_MAC']=$this->ipizza_mac_string($fields, $fields['VK_SERVICE']);
		$key=@openssl_pkey_get_private($this->bank['my_private_key'], $this->bank['my_private_key_password']);
		if (!@openssl_sign($fields['VK_MAC'], $signature, $key, OPENSSL_ALGO_SHA1)) $this->p->error[]='Unable to generate signature ('.__LINE__.')';
		$fields['VK_MAC']=base64_encode($signature); # </MAC>
		if (!in_array($fields['VK_CURR'], $this->bank['currency'])) $this->p->error[]='Invalid currency "'.$fields['VK_CURR'].'" ('.__LINE__.')';
		$this->p->form_validate($fields, $this->form, __FILE__, __LINE__);
		foreach ($fields as $k=>$v) $fields[$k]='   <input type="hidden" name="'.$k.'" value="'.htmlspecialchars($v).'" />'."\n";
		return
		'<form class="banklink '.htmlspecialchars($this->bank['class']).'" method="post" action="'.htmlspecialchars($this->bank['banklink_address']).'" enctype="application/x-www-form-urlencoded">'."\n".
		implode('', $fields).
		'	<button type="submit">'.$this->bank['logo'].'</button>'."\n".
		'	<script>'."\n".
		'		var banklinkSubmit=document.querySelector(\'form.banklink\');'."\n".
		'		if (!banklinkSubmit.classList.contains(\'test\')) banklinkSubmit.submit();'."\n".
		'	</script>'."\n".
		'</form>'."\n";
		} # </form_html()>
	function reply_get(&$reply) {
		#<Reply data>
		$service_code=((isset($_POST['VK_SERVICE'])) && ($_POST['VK_SERVICE']==='1111')) ? 1111:1911;
		$data=array();
		foreach ($this->form_services[$service_code] as $k=>$v) $data[$k]=(isset($_POST[$k])) ? $_POST[$k]:'';
		foreach (array('order_nr'=>'VK_STAMP', 'msg'=>'VK_MSG', 'amount'=>'VK_AMOUNT', ) as $k=>$v) {
			if (isset($data[$v])) $reply[$k]=$data[$v];
			}
		# </Reply data>
		# <Reply authentication>
		$key=openssl_pkey_get_public($this->bank['bank_certificate']);
		if (openssl_verify($this->ipizza_mac_string($data, $service_code), base64_decode($data['VK_MAC']), $key)) {
			$reply['authentic']=1;
			$reply['status']=$this->services[$service_code];
			}
		else $this->p->error[]='Banklink authentication failed: bank="'.$this->bank['bank'].'" status="'.$reply['status'].'" file:'.__FILE__.'('.__LINE__.')';
		# </Reply authentication>
		return $data;
		} # </reply_get()>
	function ipizza_mac_string($values, $service_code) {
		$data='';
		foreach ($this->form_services[$service_code] as $k=>$v) {
			if ($v) $data.=str_pad(mb_strlen($values[$k]), 3, '0', STR_PAD_LEFT).$values[$k];
			}
		return $data;
		} # </ipizza_mac_string()>
	} # </class::banklink_ipizza>
#2008-10-20 ?>