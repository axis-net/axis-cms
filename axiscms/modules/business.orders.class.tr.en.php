<?php #2018-11-22
return array(
	'nr.lbl'=>'Order number',
	'search.lbl'=>'Search',
	'time.lbl'=>'Date',
	'payment_method.lbl'=>'Payment method',
	'revenue.lbl'=>'Paid',
	'status.lbl'=>'Status',
	'status.-.lbl'=>'-',
	'status.accept.lbl'=>'in progress',
	'status.problem.lbl'=>'problem',
	'status.complete.lbl'=>'ready',
	'status.cancel.lbl'=>'cancel',
	'invoice_id.lbl'=>'invoice',
	
	'nr.abbr'=>'nr',
	'content.lbl'=>'Items',
	'price.lbl'=>'Price',
	'amount.lbl'=>'Amount',
	'amount_unit.lbl'=>'Unit',
	'vat.lbl'=>'Value-added tax',
	'vat.abbr'=>'VAT%',
	'sum.lbl'=>'Sum',
	'sum.vat.lbl'=>'VAT sum',
	'sum+vat.lbl'=>'Total',
	
	'client_name.lbl'=>'Client',
	'client_address.lbl'=>'Address',
	'client_postcode.lbl'=>'Postcode',
	'client_phone.lbl'=>'Phone',
	'client_email.lbl'=>'E-mail',
	'order_nr.lbl'=>'Order number',
	
	'ref.txt'=>'You can find the order later using order number.',
	);
#2008-12-09 ?>