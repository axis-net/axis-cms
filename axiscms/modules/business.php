<?php #2016-04-24

$m=new axs_db(AXS_PATH_EXT, 'business', $axs['f_px']);
$m->url['m']=$m->name;
$m->act=$m->db_current_module();
# Build module menu
$m->menu=array(
	$m->name=>array('url'=>'?'.axs_url($m->url), 'label'=>$m->tr('module_title'), 'act'=>$m->act, ),
	);
$m->db_menu_set(
	$m->menu_build($m->menu, $spc='  ', array('class'=>'vertical'), $m->name, $m->act)
	);

# ---------------------
# Continue module execution only if this is the current requested module
if ($m->act) {
	$axs['page']['head'][]='<link href="'.$m->f_path.$m->f_px.$m->name.'.css" rel="stylesheet" type="text/css" media="screen" />'."\n";
	$axs['page']['title'][]=$m->tr('module_title');
	if ($tmp=axs_admin::no_permission(array('admin'=>'', 'cms'=>''), 'r', __FILE__, __LINE__)) return $tmp; #<Permission check />
	//$this->initialize('business', $this->f_path, 'et', $axs['f_px']);
	
	class axs_business_edit extends axs_form_edit {
		var $forms=array(
			'profiles'=>array(),
			'vat'=>array(
				'id'=>array('type'=>'number', 'size'=>10, 'readonly'=>'readonly', ),
				'code'=>array('type'=>'number', 'size'=>3, 'required'=>true, 'unique'=>true, ),
				'rank'=>array('type'=>'number', ),
				'vat'=>array('type'=>'text', 'size'=>3, 'txt'=>'%', ),
				'comment'=>array('type'=>'text', 'size'=>255, ),
				'disabled'=>array('type'=>'checkbox', 'size'=>1, 'value'=>1, ),
				'updated'=>array('type'=>'timestamp-updated', 'size'=>40, 'add_fn'=>true, ),
				'save'=>array('type'=>'submit', ),
				),
			'currency'=>array(
				'id'=>array('type'=>'number', 'size'=>10, 'readonly'=>'readonly', ),
				'currency'=>array('type'=>'text', 'size'=>3, 'maxlength'=>3, 'required'=>true, ),
				'disabled'=>array('type'=>'checkbox', 'size'=>1, 'value'=>1, ),
				'updated'=>array('type'=>'timestamp-updated', 'size'=>40, 'add_fn'=>true, ),
				'save'=>array('type'=>'submit', ),
				),
			'units'=>array(
				'id'=>array('type'=>'number', 'size'=>10, 'readonly'=>'readonly', ),
				'lang'=>array('type'=>'select', 'size'=>3, 'select'=>array(), ),
				'rank'=>array('type'=>'number', ),
				'label'=>array('type'=>'text', 'size'=>255, ),
				'comment'=>array('type'=>'text', 'size'=>255, ),
				'disabled'=>array('type'=>'checkbox', 'size'=>1, 'value'=>1, ),
				'updated'=>array('type'=>'timestamp-updated', 'size'=>40, 'add_fn'=>true, ),
				'save'=>array('type'=>'submit', ),
				),
			);
		function axs_business_edit(&$e) {
			global $axs;
			$this->e=&$e;
			$this->name=$e->name;
			$this->db=$e->db;
			$this->px=$e->px;
			$this->site_nr=$e->site_nr;
			$this->l=$axs['l'];
			$this->form=(isset($this->forms[axs_get('form', $_GET)])) ? $_GET['form']:key($this->forms);
			$this->url=$e->url;
			$this->url['form']=$this->form;
			$this->forms[$this->form]['']['sql_table']=$this->table=$this->px.$this->name.'_'.$this->form;
			foreach ($this->forms[$this->form] as $k=>$v) $this->forms[$this->form][$k]['label']=$e->tr($this->form.'_'.$k.'_lbl');
			$this->header=new axs_form_edit($e->site_nr, $this->url, $this->forms[$this->form], '', $_POST);
			if (count($this->header->structure)<2) $this->header->sql_header_get($this->name, $this->form);
			$this->thead=$this->header->structure_get_thead();
			} # </axs_business_edit()>
		function editor() {
			global $axs;
			$this->header->form_input();
			$b=axs_get('b', $_GET);
			$this->header->url[$this->header->key_edit]=$this->header->url['id']=$this->header->vl['id'];
			$this->header->url['b']=urlencode($b);
			# <If update>
			if ($this->header->save) {
				$this->header->validate($this->header->vl);
				if (empty($this->header->msg)) {
					if (!$this->header->vl['id']) $this->header->vl['id']=axs_db_query("INSERT INTO `".$this->table."` SET `id`=NULL", 'insert_id', $this->db, __FILE__, __LINE__);
					axs_db_query("UPDATE `".$this->table."` AS t\n".
					"	SET ".$this->header->sql_values_set($this->header->vl, 't')."\n".
					"	WHERE t.`id`='".$this->header->vl['id']."'", '', $this->db, __FILE__, __LINE__);
					if ($b) exit(axs_redir($b));
					}
				} # </If update>
			# <Read values from SQL>
			if ((!$this->header->submit) && ($this->header->vl['id'])) {
				$result=axs_db_query($q="SELECT t.*, m.`user` AS `updated_text`\n".
				"	FROM `".$this->table."` AS t LEFT JOIN `".$axs['cfg']['db'][1]['px']."users` AS m ON m.id=t.updated_uid\n".
				"	WHERE t.id='".$this->header->vl['id']."'", 'row', $this->db, __FILE__, __LINE__);
				$this->header->vl=$this->header->sql_values_get($result);
				} # </Read values from SQL>
			# <Build form>
			//$this->header->vr=array('tools'=>array(
			//	'back'=>array('url'=>$b, 'label'=>'<'.$this->e->tr->s('back_lbl'), 'class'=>'left', ),
			//	'elements'=>'',
			//	));
			return $this->header->form_parse_html();
			} #</editor()>
		function pager($p, $limit=25) { # Generate pages links
			$total=axs_db_query('', 'found_rows', $this->db);
			if ($total) {
				$this->pager=new axs_pager($p, array($limit), $limit);
				$pages=$this->pager->pages($total, '?'.axs_url($this->url, array('p'=>'')));
				$pages='&nbsp;&nbsp;<span class="prev-next">'.$pages['prev'].$pages['current'].$pages['next'].'</span></p>'."\n".
				'  <p class="pages">'.$pages['pages'].'</p>'."\n";
				}
			else $pages='</p>'."\n";
			return ' <div class="pager">'."\n".
			'  <p class="found"><strong>('.$total.')</strong>'.$pages.' </div>';
			} # </pager()>
		function add_new_form($url=array()) {
			$add_url='';
			$tmp=$this->url;
			foreach ($url as $k=>$v) {
				if ($v===false) unset($tmp[$k]);
				else $tmp[$k]=$v;
				}
			foreach ($tmp as $k=>$v) $add_url.=axs_form_edit::input_type_hidden_html('', $k, $v);
			return ' <form action="" method="get" class="add_new">'."\n".
			'  <div>'."\n".
			'   '.$add_url.
			'	<input id="add_new" name="add_new" type="submit" value="{$add_lbl}" />'."\n".
			'  </div>'."\n".
			'  <script>'."\n".
			'	document.getElementById("add_new").onclick=function() {	if (confirm("{$add_confirm_txt}")) return true;	return false;	}'."\n".
			'  </script>'."\n".
			' </form>';
			} # </add_new_form()>
		} # </class:axs_business_edit>
	$f=new axs_business_edit($m);
	$m->form=$f->form;
	
	# Read templates
	$m->tpl_set(array($f->name=>$f->name, ));
	# Content
	$m->vars['content']=include($m->f_path.$m->f_px.$f->name.'_'.$f->form.'.form.php');
	# Tabs
	$menu=array();
	foreach ($f->forms as $k=>$v) $menu[$k]=array(
		'url'=>'?'.axs_url($f->url, array('form'=>$k, $f->header->key_edit=>false, )),
		'label'=>$m->tr('form_'.$k.'_lbl'),
		'act'=>$f->form,
		);
	$m->vars['menu']=$m->menu_build($menu, '  ', array('class'=>'tabs'), $f->name, $f->form);
	# General
	$m->vars+=$m->tr->tr;
	# Finish
	return $m->finish();
	}
else return '';
#2009-11-05 ?>