<?php #24.04.2016
if (!defined('AXS_PATH_EXT')) exit(require('../axiscms/log.php'));

foreach ((array)$axs['cfg']['site'] as $v) foreach ($v['langs'] as $kk=>$vv) $f->header->structure['lang']['options'][$kk]=$f->thead['lang']['options'][$kk]=array('value'=>$kk, 'label'=>$vv['l'], 'label.html'=>axs_html_safe($vv['l']), );

# <Edit>
if (isset($_GET[$f->header->key_edit])) return $f->editor();

# Perform database query
$result=axs_db_query('SELECT SQL_CALC_FOUND_ROWS t.*, m.`user` AS `updated_text`'."\n".
'	FROM `'.$f->table.'` AS t LEFT JOIN `'.$axs['cfg']['db'][1]['px'].'users` AS m ON m.id=t.updated_uid ORDER BY `lang` ASC, `rank` ASC',
1, $f->db, __FILE__, __LINE__);

# <Display result rows>
$table='      <table class="table">'."\n".
'       <caption>'.$m->tr('form_units_lbl').'</caption>'."\n".
'       <tr>';
foreach ($f->thead as $k=>$v) $table.='<th scope="col">'.$v['label'].'</th>';
$table.='<th>'.$f->add_new_form(array($f->header->key_edit=>'', 'b'=>'?'.$_SERVER['QUERY_STRING'])).'</th></tr>'."\n";
foreach ($result as $cl) {
	$table.='       <tr id="id'.$cl['id'].'" class="'.(($cl['disabled']) ? ' disable':'').'">';
	foreach ($f->thead as $k=>$v) $table.='<td>'.$f->header->value_display($k, $v, $cl).axs_get('txt', $v).'</td>';
	$table.='<td class="edit"><a href="'.'?'.axs_url($f->url, array('id'=>$cl['id'], $f->header->key_edit=>$cl['id'], 'b'=>'?'.$_SERVER['QUERY_STRING'].'#id'.$cl['id'])).'">'.$m->tr('edit_lbl').'</a></td></tr>'."\n";
	}
$table.='      </table>'."\n".'&nbsp;'."\n";
unset($result);
# </Display result rows>

$pager=$f->pager(axs_get('p', $_GET));
return $pager."\n".$table."\n".$pager;
/*
CREATE TABLE IF NOT EXISTS `axs_business_units` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `disabled` tinyint(4) NOT NULL DEFAULT '0',
  `nr` tinyint(4) NOT NULL DEFAULT '0',
  `label_en` varchar(255) COLLATE utf8_estonian_ci NOT NULL DEFAULT '',
  `comment_en` varchar(255) COLLATE utf8_estonian_ci NOT NULL DEFAULT '',
  `updated` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Last updated UNIX timestamp',
  `updated_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '=users.id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_estonian_ci AUTO_INCREMENT=4 ;

INSERT INTO `axs_business_units` VALUES(1, 0, 1, 'pc', 'piece', 0, 0);
INSERT INTO `axs_business_units` VALUES(2, 0, 2, 'h', 'hour', 0, 0);
INSERT INTO `axs_business_units` VALUES(3, 1, 3, 'kg', 'kilogram', 0, 0);
*/
#26.01.2010 ?>