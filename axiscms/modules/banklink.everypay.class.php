<?php #2020-05-29
class banklink_everypay {
	static $banks=array(
		'EVERYPAY'=>array(
			'class'=>'',
			'title'=>array('en'=>'EveryPay', ),
			'comment'=>array('en'=>'Credit card payment', 'en'=>'Krediitkaardi makse', ),
			'home'=>'https://www.every-pay.com',
			'banklink_address'=>'https://pay.every-pay.eu/transactions/',
			'currency'=>array('EUR', ),
			'charset'=>array('UTF-8',),
			'language'=>array('en'=>'EN', 'et'=>'ET', 'fi'=>'FI', 'de'=>'DE', 'lv'=>'LV', 'lt'=>'LT', 'ru'=>'RU', 'es'=>'ES', 'sv'=>'SV', 'da'=>'DA', 'pl'=>'PL', ),
			'form'=>'charge',
			),
		'EVERYPAY-test'=>array(
			'class'=>'test',
			'title'=>array('en'=>'EveryPay TEST', ),
			'comment'=>array('en'=>'Credit card payment TEST', 'en'=>'Krediitkaardi makse TEST', ),
			'home'=>'https://www.every-pay.com',
			'banklink_address'=>'https://igw-demo.every-pay.com/transactions',
			'currency'=>array('EUR', ),
			'charset'=>array('UTF-8',),
			'language'=>array('en'=>'EN', 'et'=>'ET', 'fi'=>'FI', 'de'=>'DE', 'lv'=>'LV', 'lt'=>'LT', 'ru'=>'RU', 'es'=>'ES', 'sv'=>'SV', 'da'=>'DA', 'pl'=>'PL', ),
			'form'=>'charge',
			),
		'EVERYPAY_TOKENPAYMENT'=>array(
			'class'=>'',
			'title'=>array('en'=>'EveryPay recurring payment', ),
			'comment'=>array('en'=>'Recurring credit card payment', 'en'=>'Krediitkaardi püsimakse', ),
			'home'=>'https://www.every-pay.com',
			'banklink_address'=>'https://pay.every-pay.eu/transactions',
			'currency'=>array('EUR', ),
			'charset'=>array('UTF-8',),
			'language'=>array('en'=>'EN', 'et'=>'ET', 'fi'=>'FI', 'de'=>'DE', 'lv'=>'LV', 'lt'=>'LT', 'ru'=>'RU', 'es'=>'ES', 'sv'=>'SV', 'da'=>'DA', 'pl'=>'PL', ),
			'form'=>'charge-token',
			),
		'EVERYPAY_TOKENPAYMENT-test'=>array(
			'class'=>'test',
			'title'=>array('en'=>'EveryPay recurring payment TEST', ),
			'comment'=>array('en'=>'Recurring credit card payment TEST', 'en'=>'Krediitkaardi püsimakse TEST', ),
			'home'=>'https://www.every-pay.com',
			'banklink_address'=>'https://igw-demo.every-pay.com/transactions',
			'currency'=>array('EUR', ),
			'charset'=>array('UTF-8',),
			'language'=>array('en'=>'EN', 'et'=>'ET', 'fi'=>'FI', 'de'=>'DE', 'lv'=>'LV', 'lt'=>'LT', 'ru'=>'RU', 'es'=>'ES', 'sv'=>'SV', 'da'=>'DA', 'pl'=>'PL', ),
			'form'=>'charge-token',
			),
		);
	public $form=array(
		'charge'=>array(
			'account_id'=>array('k'=>'account_number', 'len'=>255, 'req'=>1, ),
			'amount'=>array('k'=>'amount', 'len'=>12, 'req'=>1, ),
			'billing_address'=>array('k'=>'client_address', 'len'=>255, 'req'=>0, ),
			'billing_city'=>array('k'=>'client_city', 'len'=>1, 'req'=>0, ),
			'billing_country'=>array('k'=>'client_country', 'len'=>2, 'req'=>0, 'val'=>'EE', ),
			'billing_postcode'=>array('k'=>'client_postcode', 'len'=>255, 'req'=>0, ),
			'callback_url'=>array('k'=>'return_url', 'len'=>255, 'req'=>1, ),
			'customer_url'=>array('k'=>'return_url', 'len'=>255, 'req'=>1, ),
			'email'=>array('k'=>'client_email', 'len'=>255, 'req'=>0, ),
			'order_reference'=>array('k'=>'msg', 'len'=>255, 'req'=>1, ),
			'user_ip'=>array('k'=>'user_ip', 'len'=>255, 'req'=>1, ),
			'hmac_fields'=>array('k'=>'hmac_fields', 'len'=>1000, 'req'=>0, ),#'val'=>'account_id,amount,api_username,callback_url,customer_url,locale,nonce,order_reference,skin_name,timestamp,transaction_type,user_ip', ),
			'skin_name'=>array('k'=>'skin_name', 'len'=>255, 'req'=>0, 'val'=>'default', ),
			),
		'charge-token'=>array(
			'account_id'=>array('k'=>'account_number', 'len'=>255, 'req'=>1, ),
			'amount'=>array('k'=>'amount', 'len'=>12, 'req'=>1, ),
			'billing_address'=>array('k'=>'client_address', 'len'=>255, 'req'=>0, ),
			'billing_city'=>array('k'=>'client_city', 'len'=>1, 'req'=>0, ),
			'billing_country'=>array('k'=>'client_country', 'len'=>2, 'req'=>0, 'val'=>'EE', ),
			'billing_postcode'=>array('k'=>'client_postcode', 'len'=>255, 'req'=>0, ),
			'callback_url'=>array('k'=>'return_url', 'len'=>255, 'req'=>1, ),
			'customer_url'=>array('k'=>'return_url', 'len'=>255, 'req'=>1, ),
			'email'=>array('k'=>'client_email', 'len'=>255, 'req'=>0, ),
			'order_reference'=>array('k'=>'msg', 'len'=>255, 'req'=>1, ),
			'user_ip'=>array('k'=>'user_ip', 'len'=>255, 'req'=>1, ),
			'hmac_fields'=>array('k'=>'hmac_fields', 'len'=>1000, 'req'=>0, ),#'val'=>'account_id,amount,api_username,callback_url,customer_url,locale,nonce,order_reference,skin_name,timestamp,transaction_type,user_ip', ),
			'request_cc_token'=>array('k'=>'', 'len'=>1, 'req'=>0, 'val'=>'1', ),
			'skin_name'=>array('k'=>'skin_name', 'len'=>255, 'req'=>0, 'val'=>'default', ),
			),
		);
	static $status=array(0=>'ERROR', 1=>'OK', 2=>'CANCELLED', 3=>'FAIL', );
	function __construct($data) {
		$this->bank=array_merge(self::$banks[$data['bank']], $data);
		$this->form=$this->form[$this->bank['form']];
		$this->init($this->bank['my_id'], $this->bank['my_private_key'], array('transaction_type'=>'charge', ));
		} #</__construct()>
	function form_html() {
		$fields=array();
		foreach ($this->form as $k=>$v) $fields[$k]=$this->p->values[$v['k']];
		$this->p->form_validate($fields, $this->form, __FILE__, __LINE__);
		$fields=$this->getFields($fields, $this->bank['l']);
		foreach ($fields as $k=>$v) $fields[$k]='				<input name="'.$k.'" type="hidden" value="'.htmlspecialchars($v).'" />'."\n";
		return 
		'			<div id="iframe-payment-container" class="banklink '.htmlspecialchars($this->bank['class']).'" style="width:30em; max-width:100%;">'."\n".
		'				<span class="loading" lang="en">Loading&hellip;</span>'."\n".
		'				<iframe id="iframe-payment" name="iframe-payment" width="460" height="325" style="border:none; width:100%; min-height:450px; width:100%;"></iframe>'."\n".
		'			</div>'."\n".
		'			<form id="iframe_form" action="'.htmlspecialchars($this->bank['banklink_address']).'" method="post" style="display:none" target="iframe-payment">'."\n".
		implode('', $fields).
		'				<script>'."\n".
		'					document.querySelector("#iframe-payment").addEventListener("load",function(){'."\n".
		'						var el=document.querySelector("#iframe-payment-container .loading.loaded");'."\n".
		'						if (el) el.parentNode.removeChild(el);'."\n".
		'						});'."\n".
		'					window.addEventListener("load",function(){'."\n".
		'						document.querySelector("#iframe-payment-container .loading").classList.add("loaded");'."\n".
		'						document.getElementById("iframe_form").submit();'."\n".
		'						});'."\n".
		'				</script>'."\n".
		'				<script src="'.$this->p->dir_http.$this->p->name.'.everypay.js"></script>'."\n".
		'			</form>';
		} #</form_html()>
	function reply_get(&$reply) {
		$state=intval($this->verify($_POST));
		if ($state) $reply['authentic']=1;
		$reply['status']=self::$status[$state];
		if (isset($_POST['order_reference'])) $reply['order_nr']=$_POST['order_reference'];
		if (isset($_POST['payment_reference'])) $reply['msg']=$_POST['payment_reference'];
		if (isset($_POST['amount'])) $reply['amount']=$_POST['amount'];
		$reply['card_token']=(isset($_POST['cc_token'])) ? $_POST['cc_token']:'';
		switch ($state) {
			case 1:
			case 2: $data=array(
				'account_id'=>'', #account id in EveryPay system,
				'amount'=>'', #amount to pay,
				'api_username'=>'', #api username,
				'nonce'=>'', #return nonce,
				'order_reference'=>'', #order reference number,
				'payment_reference'=>'', #payment reference number,
				'payment_state'=>'', #payment state,
				'timestamp'=>'', #timestamp,
				'transaction_result'=>'', #transaction result,
				'hmac_fields'=>'', #string which contains all fields (keys) that are going to be used in hmac calculation, separated by comma.
				);
			default: $data=array(
				'api_username'=>'', #api username,
				'nonce'=>'', #return nonce,
				'order_reference'=>'', #order reference number,
				'payment_state'=>'', #payment state,
				'timestamp'=>'', #timestamp,
				'transaction_result'=>'', #transaction result
				);
			}
		foreach ($data as $k=>$v) {	$data[$k]=(isset($_POST[$k])) ? $_POST[$k]:'';	}
		return $data;
		} #</reply_get()>
	function MIT(&$cl) { #Perform Merchant Initiated Token payment. 
		$this->cc_token=$cl['card_token'];
		$data=array(
			'api_username'=>$this->api_username, 'account_name'=>$this->bank['account_number'], 'amount'=>$cl['amount'], 'token'=>$cl['card_token'],
			'order_reference'=>$cl['order_nr'], 'token_agreement'=>'recurring', 'nonce'=>$this->getNonce(), 'timestamp'=>date(DateTimeInterface::ATOM),
			'email'=>$cl['email'], 'merchant_ip'=>$_SERVER['SERVER_ADDR'], );
		$reply=array();
		$ch=curl_init(str_replace('/transactions', '/api/v3/payments/mit', $this->bank['banklink_address']));
		curl_setopt_array($ch, array(
			CURLOPT_HEADER=>false, CURLOPT_RETURNTRANSFER=>true, CURLOPT_HTTPHEADER=>array('Content-Type: application/json', 'Accept: application/json', ), 
			CURLOPT_HTTPAUTH=>CURLAUTH_BASIC, CURLOPT_USERPWD=>$this->api_username.':'.$this->api_secret, CURLOPT_POST=>true, CURLOPT_POSTFIELDS=>json_encode($data), 
			));
		$r=curl_exec($ch);
		$rcode=curl_getinfo($ch, CURLINFO_RESPONSE_CODE);
		$data=(array)@json_decode($r);
		if (in_array($rcode, array(200, 201, 422), true)) {
			if (($rcode===422) || (!empty($data['error']))) $a=array('status'=>'FAIL', 'msg'=>'', 'amount'=>0.00, );
			else $a=array(
				'status'=>'OK',
				'msg'=>(isset($data['payment_reference'])) ? $data['payment_reference']:'',
				'amount'=>(isset($data['standing_amount'])) ? $data['standing_amount']:0.00,
				);
			foreach ($a as $k=>$v) $cl[$k]=$v;
			$this->p->log($cl, $r);
			}
		else {
			$cl['status']='ERROR';
			$this->p->log_error(__FILE__, __LINE__, get_class($this).'::'.__FUNCTION__.'()'."\n".'HTTP response code:'.$rcode."\n".'curl error: '.curl_error($ch).' ('.curl_errno($ch).')', $r);
			}
		curl_close($ch);
		return $data;
		} #</MIT()>
	
	#<Original EveryPay code>
	const _VERIFY_SUCCESS = 1; // payment successful
    const _VERIFY_CANCEL = 2;  // payment cancelled
    const _VERIFY_FAIL = 3;    // payment failed

    private $api_username;
    private $api_secret;
    private $request_cc_token;
    private $cc_token;
    private $transaction_type;

    /**
      * Initiates a payment.
      *
      * Expects following data as input:
      * 'api_username' => api username,
      * 'api_secret => api secret,
      * array(
      *  'request_cc_token' => request to get cc_token 1,
      *  'cc_token' => token referencing a bank card
      * )
      *
      * @param $api_username string
      * @param $api_secret string
      * @param $data array
      * @example $everypay->init('api_username', 'api_secret', array('cc_token' => 'token'));
      *
      * @return EveryPay
    */

    public function init($api_username, $api_secret, $data = '')
    {
        $this->api_username = $api_username;
        $this->api_secret = $api_secret;

        if (isset($data['request_cc_token'])) {
          $this->request_cc_token = $data['request_cc_token'];
        }

        if (isset($data['cc_token'])) {
          $this->cc_token = $data['cc_token'];
        }

        if (isset($data['transaction_type'])) {
          $this->transaction_type = $data['transaction_type'];
        }
    }

    /**
      * Populates and returns array of fields to be submitted for payment.
      *
      * Expects following data as input:
      * array(
      *  'account_id' => account id,
      *  'amount' => amount to pay,
      *  'billing_address' => billing address street name,
      *  'billing_city' => billing address city name,
      *  'billing_country' => billing address 2 letter country code,
      *  'billing_postcode' => billing address postal code,
      *  'callback_url' => callback url,
      *  'customer_url' => return url,
      *  'delivery_address' => shipping address street name,
      *  'delivery_city' => shipping address city name,
      *  'delivery_country' => shipping address 2 letter country code,
      *  'delivery_postcode' => shipping address postal code,
      *  'email' => customer email address,
      *  'order_reference' => order reference number,
      *  'user_ip' => user ip address,
      *  'hmac_fields' => string which contains all fields (keys) that are going to be used in hmac calculation, separated by comma,
      *  'request_cc_token' => request to get cc_token 1,
      *  'cc_token' => token referencing a bank card
      * );
      *
      * @param $data array
      * @param $language string
      *
      * @return array
    */

    public function getFields(array $data, $language = 'en')
    {
        $data['api_username'] = $this->api_username;
        $data['transaction_type'] = $this->transaction_type;
        $data['nonce'] = $this->getNonce();
        $data['timestamp'] = time();

        if (isset($this->request_cc_token)) {
            $data['request_cc_token'] = $this->request_cc_token;
        }

        if (isset($this->cc_token)) {
            $data['cc_token'] = $this->cc_token;
        }

        $keys = array_keys($data);
		// ensure that hmac_fields itself is mentioned in hmac_fields
        if (!in_array('hmac_fields', $keys)){
          $keys[] = 'hmac_fields';
		}
		asort($keys);
        $data['hmac_fields'] = implode(',', $keys);
        
        $data['hmac'] = $this->signData($this->serializeData($data));
        $data['locale'] = $language;

        return $data;
    }

    /**
      * Verifies return data
      *
      * Expects following data as input:
      *
      * for successful and failed payments:
      *
      * array(
      *  'account_id' => account id in EveryPay system,
      *  'amount' => amount to pay,
      *  'api_username' => api username,
      *  'nonce' => return nonce,
      *  'order_reference' => order reference number,
      *  'payment_reference' => payment reference number,
      *  'payment_state' => payment state,
      *  'timestamp' => timestamp,
      *  'transaction_result' => transaction result,
      *  'hmac_fields' => string which contains all fields (keys) that are going to be used in hmac calculation, separated by comma.
      * );
      *
      * for cancelled payments:
      *
      * array(
      *  'api_username' => api username,
      *  'nonce' => return nonce,
      *  'order_reference' => order reference number,
      *  'payment_state' => payment state,
      *  'timestamp' => timestamp,
      *  'transaction_result' => transaction result
      * );
      *

      * @param $data array
      *
      * @return int 1 - verified successful payment, 2 - cancelled, 3 - failed
      * @throws Exception
    */

    public function verify(array $data)
    {
        if ($data['api_username'] !== $this->api_username) {
            return $this->p->log_error(__FILE__, __LINE__, 'Invalid username.');#added
			#throw new Exception('Invalid username.');
        }

        $now = time();
        if (($data['timestamp'] > $now) || ($data['timestamp'] < ($now - 600))) {
            return $this->p->log_error(__FILE__, __LINE__, 'Response outdated.');#added
			#throw new Exception('Response outdated.');
        }

        /**
          * Refer to the Integration Manual for more information about 'order_reference' validation.
        */

        if (!$this->verifyNonce($data['nonce'])) {
            return $this->p->log_error(__FILE__, __LINE__, 'Nonce is already used.');#added
			#throw new Exception('Nonce is already used.');
        }

        /**
          * Refer to the Integration Manual for more information about 'nonce' uniqueness validation.
        */

        $status = $this->statuses($data['payment_state']);

        $verify = array();
        $hmac_fields = explode(',', $data["hmac_fields"]);

        foreach ($hmac_fields as $value) {
            $verify[$value] = isset($data[$value]) ? $data[$value] : '';
        }

        $hmac = $this->signData($this->serializeData($verify));
        if ($data['hmac'] != $hmac) {
            return $this->p->log_error(__FILE__, __LINE__, 'Invalid HMAC.');#added
			#throw new Exception('Invalid HMAC.');
        }

        return $status;
    }

    protected function getNonce()
    {
        return uniqid(true);
    }

    protected function verifyNonce($nonce)
    {
        return true;
    }

    private function statuses($state) {
        switch ($state) {
            case 'settled':
            case 'authorised':
                return self::_VERIFY_SUCCESS;
            case 'cancelled':
            case 'waiting_for_3ds_response':
                return self::_VERIFY_CANCEL;
            case 'failed':
                return self::_VERIFY_FAIL;
        }
    }

    /**
      * Prepare data package for signing
      *
      * @param array $data
      * @return string
    */

    private function serializeData(array $data)
    {
        $arr = array();
        ksort($data);
        foreach ($data as $k => $v) {
            $arr[] = $k . '=' . $v;
        }
        return implode('&', $arr);
    }

    private function signData($data)
    {
        return hash_hmac('sha1', $data, $this->api_secret);
    }
	#</Original EveryPay code>
	} # </class::banklink_everypay>
#2019-11-18 ?>