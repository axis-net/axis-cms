<?php #18.05.2008
return array(
	'module_title'=>'Ettevõte',
	'form_profiles_lbl'=>'Profiilid',
	'form_vat_lbl'=>'Käibemaksu tüübid',
	'form_currency_lbl'=>'Rahaühikud',
	'form_units_lbl'=>'Kaubaühikud',
	
	'currency_id_lbl'=>'id',
	'currency_currency_lbl'=>'nimetus',
	'currency_disabled_lbl'=>'mitteaktiivne',
	'currency_updated_lbl'=>'muudetud',
	'currency_save_lbl'=>'salvesta',
	
	'vat_id_lbl'=>'id',
	'vat_code_lbl'=>'kood',
	'vat_rank_lbl'=>'järjestus',
	'vat_vat_lbl'=>'maksumäär',
	'vat_comment_lbl'=>'kommentaar',
	'vat_disabled_lbl'=>'mitteaktiivne',
	'vat_updated_lbl'=>'muudetud',
	'vat_save_lbl'=>'salvesta',
	
	'units_id_lbl'=>'id',
	'units_lang_lbl'=>'keel',
	'units_rank_lbl'=>'järjestus',
	'units_label_lbl'=>'nimetus',
	'units_comment_lbl'=>'kommentaar',
	'units_disabled_lbl'=>'mitteaktiivne',
	'units_updated_lbl'=>'muudetud',
	'units_save_lbl'=>'salvesta',
	
	'add_lbl'=>'Lisa uus',
	'add_confirm_txt'=>'Kas soovid lisada uue nimetuse?',
	'edit_lbl'=>'muuda',
	'top_lbl'=>'üles',
	'back_lbl'=>'tagasi',
	
	'msg_field'=>'Palun täitke väli',
	);
#16.03.2009 ?>