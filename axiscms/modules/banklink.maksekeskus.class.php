<?php #2019-12-30
class banklink_maksekeskus {
	static $banks=array(
		'MAKSEKESKUS'=>array(
			'class'=>'',
			'title'=>array('en'=>'Maksekeskus', ),
			'comment'=>array('en'=>'Banklink payment', 'en'=>'Pangalingi makse', ),
			'home'=>'https://www.maksekeskus.ee',
			'banklink_address'=>'https://payment.maksekeskus.ee/pay/1/signed.html',
			'currency'=>array('EUR', ),
			'charset'=>array('UTF-8',),
			'language'=>array('en'=>'en', 'et'=>'et', 'fi'=>'fi', ),
			'form'=>'banklink',
			# $GATEWAY = 'https://payment.maksekeskus.ee';
			),
		'MAKSEKESKUS-test'=>array(
			'class'=>'test',
			'title'=>array('en'=>'Maksekeskus', ),
			'comment'=>array('en'=>'Banklink payment TEST', 'en'=>'Pangalingi makse TEST', ),
			'home'=>'https://www.maksekeskus.ee',
			'banklink_address'=>'https://payment-test.maksekeskus.ee/pay/1/signed.html',
			'currency'=>array('EUR', ),
			'charset'=>array('UTF-8',),
			'language'=>array('en'=>'en', 'et'=>'et', 'fi'=>'fi', ),
			'form'=>'banklink',
			# $GATEWAY = 'https://payment-test.maksekeskus.ee';
			),
		);
	public $form=array(
		'banklink'=>array(
			'shop'=>array('k'=>'my_id', 'len'=>255, 'req'=>1, ),
			'amount'=>array('k'=>'amount', 'len'=>12, 'req'=>1, ),
			'reference'=>array('k'=>'order_nr', 'len'=>255, 'req'=>1, ),
			'country'=>array('k'=>'l', 'len'=>2, 'req'=>1, 'val'=>'ee', ),
			'locale'=>array('k'=>'l', 'len'=>2, 'req'=>1),
			#'return_url'=>array('k'=>'return_url', 'len'=>7000, 'req'=>1),
			#'notification_url'=>array('k'=>'notification_url', 'len'=>7000, 'req'=>1),
			#'cancel_url'=>array('k'=>'cancel_url', 'len'=>7000, 'req'=>1),
			),
		);
	static $status=array('CREATED'=>'WAIT', 'PENDING'=>'WAIT', 'CANCELLED'=>'CANCELLED', 'EXPIRED'=>'FAIL', 'APPROVED'=>'WAIT', 'COMPLETED'=>'OK', 'PART_REFUNDED'=>'FAIL', 'REFUNDED'=>'FAIL', );
	function __construct($data) {
		$this->bank=array_merge(self::$banks[$data['bank']], $data);
		$this->form=$this->form[$this->bank['form']];
		} #</__construct()>
	function form_html($order_data) {
		$fields=array();
		foreach ($this->form as $k=>$v) $fields[$k]=$this->p->values[$v['k']];
		$this->p->form_validate($fields, $this->form, __FILE__, __LINE__);
		$fields['country']='ee';
		$fields['transaction_url']=array();
		foreach (array('return_url'=>'', 'notification_url'=>'', 'cancel_url'=>'') as $k=>$v) {
			$fields['transaction_url'][$k]=$order_data[$k];
			//$fields.='	<input type="hidden" name="'.$k.'" value="'.htmlspecialchars($order_data[$k]).'" />'."\n";
			}
		$json=json_encode($fields);
		#UPPERCASE(HEX(SHA-512(string(JSON) + string(Secret Key))))
		$mac=strtoupper(hash('SHA512', $json.$this->bank['my_private_key'], false));
		return
		'<form class="banklink '.htmlspecialchars($this->bank['class']).'" method="post" action="'.htmlspecialchars($this->bank['banklink_address']).'" enctype="application/x-www-form-urlencoded">'."\n".
		'	<input name="json" type="hidden" value="'.htmlspecialchars($json, ENT_QUOTES).'" />'."\n".
		'	<input name="mac" type="hidden" value="'.$mac.'" />'."\n".
		//$fields.
		'	<button type="submit">'.$this->bank['logo'].'</button>'."\n".
		'	<script>'."\n".
		'		var banklinkSubmit=document.querySelector(\'form.banklink\');'."\n".
		'		if (!banklinkSubmit.classList.contains(\'test\')) banklinkSubmit.submit();'."\n".
		'	</script>'."\n".
		'</form>'."\n";
		} #</form_html()>
	function reply_get(&$reply) {
		#<Reply data>
		$data=json_decode($_POST['json'], true);
		foreach (array('order_nr'=>'reference', 'msg'=>'transaction', 'amount'=>'amount', ) as $k=>$v) {
			if (isset($data[$v])) $reply[$k]=$data[$v];
			}
		$mac=strtoupper(hash('SHA512', $data.$this->bank['my_private_key'], false));
		#</Reply data>
		#<Reply authentication>
		if ($_POST['mac']===$mac) {
			$reply['authentic']=1;
			$reply['status']=$self::$status[$data['status']];
			}
		else $this->p->error[]='Banklink authentication failed: bank="'.$this->bank['bank'].'" status="'.$reply['status'].'" file:'.__FILE__.'('.__LINE__.')';
		#</Reply authentication>
		return $data;
		} #</reply_get()>
	} # </class::banklink_maksekeskus>
#2019-11-18 ?>