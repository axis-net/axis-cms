<?php #2018-10-21
class axs_business_orders {
	var $module_base='business';
	var $module='orders';
	static $d='module.business_orders/';
	var $dir='module.business_orders/';
	static $lang='en';
	static $img=array('w'=>100, 'h'=>100, );
	var $cols=array(
		'header'=>array(
			'id'=>'', 'time'=>'', 'profile_id'=>'', 'client_id'=>'', 'client_name'=>'', 'client_address'=>'', 'client_postcode'=>'', 'client_email'=>'', 'client_phone'=>'', 'client_ip'=>'',
			'payment_method'=>'', 'currency'=>'', 'revenue'=>'', 'invoice_id'=>'',
			'compiler_id'=>'', 'compiler_name'=>'', 'status'=>array(),
			'updated'=>'', 'updated_uid'=>'',
			),
		'rows'=>array(
			'nr'=>'', 'product_id'=>'', 'product_form'=>'', 'text'=>'', 'price'=>'', 'vat'=>'', 'amount'=>'', 'amount_unit'=>'', 'updated'=>'', 'updated_uid'=>'',
			),
		);
	static $client_fields=array(
		'name'=>array('type'=>'text', 'size'=>50, 'maxlength'=>255, 'required'=>true, ),
		'address'=>array('type'=>'text', 'size'=>50, 'maxlength'=>255, 'required'=>true, ),
		'postcode'=>array('type'=>'text', 'size'=>20, 'maxlength'=>255, 'required'=>true, ),
		'email'=>array('type'=>'email', 'size'=>50, 'maxlength'=>255, 'required'=>true, 'multiple'=>'multiple', ),
		'phone'=>array('type'=>'tel', 'size'=>50, 'maxlength'=>255, 'required'=>true, ),
		);
	static $statuses=array('-'=>'-', 'accept'=>'accept', 'draft'=>'draft', 'problem'=>'problem', 'complete'=>'complete', 'cancel'=>'cancel', );
	var $data=array();
	function __construct($site_nr=1, $l=false, $tr=true) {
		global $axs;
		$this->site_nr=$site_nr;
		$this->db=$axs['cfg']['site'][$this->site_nr]['db'];
		$this->px=$axs['cfg']['site'][$this->site_nr]['prefix'];
		$this->l=($l!==false) ? $l:$axs['l'];
		$this->tr=$tr;
		if ($this->tr===true) {
			$this->tr=new axs_tr(dirname(__FILE__).'/', $this->module_base.'.'.$this->module.'.class.tr', $this->l, self::$lang);
			$this->l=$this->tr->l;
			}
		if (is_array($this->tr)) $this->tr=new axs_tr($this->tr);
		if (!is_dir($d=$this->dir=axs_dir('content').$this->dir)) mkdir($d);
		# make shure http access is blocked
		if (!file_exists($tmp=$d.'.htaccess')) {
			if (file_put_contents($tmp, 'Order allow,deny'."\n".'Deny from all')===false) axs_log(__FILE__, __LINE__, 'chmod', 'Error writing "'.$tmp.'"', true);
			}
		} #</__construct()>
	
	#<IP>
	function ip_get() {
		$ipaddr=getenv('HTTP_X_FORWARDED_FOR');
		if (!$ipaddr) $ipaddr=$_SERVER['REMOTE_ADDR'];
		return $ipaddr;
		} # </ip_get()>
	function ip_to_number($ip=false) { # Convert IP to integer
		if ($ip===false) $ip=self::ip_get();
		$ip=ip2long($ip);
		if ($ip==-1 or $ip===false) return 0;
		return sprintf("%010u", $ip);
		} # </ip_to_number()>
	function ip_from_number($number) {
		$oct4=($number-fmod($number, 16777216))/16777216;
		$oct3=(fmod($number, 16777216)-(fmod($number, 16777216)%65536))/65536;
		$oct2=(fmod($number, 16777216)%65536-(fmod($number, 16777216)%65536%256))/256;
		$oct1=fmod($number, 16777216)%65536%256;
		$ip=$oct1.'.'.$oct2.'.'.$oct3.'.'.$oct4;
		return $ip;
		} # </ip_from_number()>
	#</IP>
	
	#<Nr>
	# get order id and timestamp out of order_nr
	static function nr_get($order_nr) {
		$id=substr($order_nr, 0, strlen($order_nr)-10)+0;
		$time=substr($order_nr, -10, 10)+0;
		//$token=str_replace(array('.','/','\\'), '', strtoupper(substr($order_nr, -4)));
		return array('id'=>$id, 'time'=>$time, 'nr'=>$order_nr+0);
		}
	function nr_split($order_nr) { # get order id and timestamp out of order_nr
		$id=substr($order_nr, 0, strlen($order_nr)-10)+0;
		$time=substr($order_nr, -10, 10)+0;
		//$token=str_replace(array('.','/','\\'), '', strtoupper(substr($order_nr, -4)));
		return array('id'=>$id, 'time'=>$time);
		} # </nr_split()>
	function nr_make($cl, $time=false) { # make new order_nr
		if (!is_array($cl)) $cl=array('id'=>$cl, 'time'=>$time);
		$cl['id']+=0;
		if (!$cl['id']) return '';
		return $cl['id'].substr($cl['time']+10000000000, 1);
		} # </nr_make()>
	#</Nr>
	
	#<Get data>
	function doc_format(array $data) {
		global $axs;
		$data['header']=$this->doc_format_header($data['header']);
		foreach ($sum=array('', '.vat', '+vat') as $v) $data['header']['sum'.$v]=0.00;
		if ((isset($data['rows'])) && (is_array($data['rows']))) {
			$dir=axs_dir('content').$this->dir;
			$nr=1;
			foreach ($data['rows'] as $id=>$cl) {
				$cl['nr']=$nr++;
				$cl['img']=(file_exists($tmp=$dir.$data['header']['id'].'-'.$cl['id'].'.jpg')) ? $tmp:'';
				$cl['price+vat']=$cl['price']+$cl['price']/100*$cl['vat'];
				$cl['sum']=$cl['price']*$cl['amount'];
				$cl['sum.vat']=($cl['sum']/100)*$cl['vat'];
				$cl['sum+vat']=$cl['sum']+$cl['sum.vat'];
				$cl['amount.fmt']=preg_replace('/\.0+/', '', $cl['amount']);
				foreach (array('price', 'sum', 'sum.vat', 'sum+vat') as $v) $cl[$v.'.fmt']=number_format($cl[$v], 2, '.', ' ');
				$cl['vat.fmt']=($cl['vat']>0) ? $cl['vat'].'%':'-';
				$data['header']['sum']+=$cl['sum'];
				$data['header']['sum.vat']+=$cl['sum.vat'];
				$data['header']['sum+vat']+=$cl['sum']+$cl['sum.vat'];
				$cl['sum.vat']=round($cl['sum.vat'], 2);
				$data['rows'][$id]=$cl;
				}
			}
		foreach ($sum as $v) if (isset($cl['sum'.$v])) {
			$cl['sum'.$v]=round($cl['sum'.$v], 2);
			$cl['sum'.$v.'.fmt']=number_format($cl['sum'.$v], 2, '.', ' ');
			}
		return $data;
		} #</doc_format_header()>
	function doc_format_header(array $cl) {
		global $axs;
		$cl['doc_id']=$cl['id'];
		$cl['nr']=$cl['order_nr']=self::nr_make($cl);
		$cl['date']=date('d.m.Y', $cl['time']);
		$cl['time.fmt']=date('d.m.Y H:i', $cl['time']);
		$cl['status.fmt']=$this->tr->t('status.'.$cl['status'].'.lbl');
		$cl['revenue.fmt']=number_format($cl['revenue'], 2, '.', ' ');
		$cl['order_url']=$axs['http'].'://'.$_SERVER['SERVER_NAME'].axs_dir('site', 'http').'?'.axs_url(array('c'=>'eshop', 'l'=>$axs['l'], 'order_nr'=>$cl['nr'], ), array(), false);
		return $cl;
		} #</doc_format_header()>
	function list_get(array $sort=array('time'=>0, 'status'=>1, ), array $limit=array(), array $search=array()){
		$select=array();
		$search=($search) ? "\n	WHERE ".implode(' AND ', $search):'';
		foreach ($sort as $k=>$v) {
			if (!strlen($v)) $v=1;
			$sort[$k]="`".$k."` ".(($v) ? 'ASC':'DESC');
			}
		$sort=($sort) ? "\n	ORDER BY ".implode(', ', $sort):'';
		foreach ($limit as $k=>$v) $limit[$k]=intval($v);
		$limit=($limit) ? "\n	LIMIT ".implode(',', $limit):'';
		$data=axs_db_query("SELECT t.*, b.`comment_".$this->l."` AS `payment_method.fmt`\n".
		"	FROM `".$this->px."business_orders` AS t LEFT JOIN `".$this->px."banklink` AS b ON b.`id`=t.`payment_method`".$search.$sort.$limit,
		'k', $this->db, __FILE__, __LINE__);
		foreach ($data as $id=>$cl) $data[$id]=$this->doc_format_header($cl);
		return $data;
		} #</list_get()>
	function order_get($site, $l, $id, $time=false, $labels=false) {
		global $axs;
		$id+=0;
		$data=array(
			'id'=>0,
			'invoice_id'=>0,
			'header'=>array('sum'=>0.00, 'sum.vat'=>0.00, 'sum+vat'=>0.00, ),
			'rows'=>array(),
			'org'=>array(),
			'labels'=>($labels) ? $labels:$this->tr->get(),//(array)include('business.orders.class.tr.'.$l.'.php'),
			'l'=>$l,
			);
		if (!$id) return $data;
		$px=$axs['cfg']['site'][$site]['prefix'];
		$db=$axs['cfg']['site'][$site]['db'];
		$qry='';
		$h=array('time', 'currency', 'invoice_id', 'status', 'client_id', 'client_name', 'client_address', 'client_postcode', 'client_email','client_phone', 'payment_method', 'revenue', 'profile_id', );
		$r=array('id', 'nr', '_parent_id', 'product_id', 'product_form', 'text', 'price', 'vat', 'amount', 'amount_unit', );
		if ($time!==false) $qry.=" AND h.time='".$time."'";
		//if ($token!==false) $qry.=' AND h.token=\''.$token.'\'';
		$result=axs_db_query("SELECT h.id AS order_id, h.".implode(', h.', $h).",r.*, u.label_".$l." AS `amount_unit.txt`\n".
		"	FROM `".$px."business_orders` AS h LEFT JOIN `".$px."business_orders_table` AS r ON r._parent_id=h.id\n".
		"	LEFT JOIN `".$px."business_units` AS u ON u.id=r.amount_unit\n".
		"	WHERE h.id='".$id."'".$qry, 1, $db, __FILE__, __LINE__);
		foreach (array('id'=>'order_id', 'invoice_id'=>'invoice_id', ) as $k=>$v) $data[$k]=$result[0][$v];
		$data['header']['id']=$result[0]['order_id'];
		$nr=1;
		foreach ($result as $cl) {
			foreach ($h as $v) unset($cl[$v]);
			$cl['nr']=$nr++;
			$cl['img']=(file_exists($tmp=axs_dir('content').self::$d.$data['header']['id'].'-'.$cl['id'].'.jpg')) ? $tmp:'';
			$cl['sum']=$cl['price']*$cl['amount'];
			$cl['sum.vat']=($cl['sum']/100)*$cl['vat'];
			$cl['sum+vat']=$cl['sum']+$cl['sum.vat'];
			$cl['amount.fmt']=preg_replace('/\.0+/', '', $cl['amount']);
			foreach (array('price', 'sum.vat', 'sum', 'sum+vat') as $v) $cl[$v.'.fmt']=number_format($cl[$v], 2, '.', ' ');
			$cl['vat.fmt']=($cl['vat']>0) ? $cl['vat'].'%':'-';
			$data['header']['sum']+=$cl['sum'];
			$data['header']['sum.vat']+=$cl['sum.vat'];
			$data['header']['sum+vat']+=$cl['sum']+$cl['sum.vat'];
			$cl['sum.vat']=round($cl['sum.vat'], 2);
			$data['rows'][$cl['id']]=$cl;
			}
		foreach ($h as $k=>$v) $data['header'][$v]=$result[0][$v];
		$data['nr']=$data['header']['order_nr']=self::nr_make($result[0]['order_id'], $result[0]['time']);
		$data['header']['date']=date('d.m.Y H:i', $result[0]['time']);
		$data['header']['status.txt']=$data['labels']['status.'.$data['header']['status'].'.lbl'];
		//exit(dbg($data['labels']));
		$data['header']['sum.vat']=round($data['header']['sum.vat'], 2);
		$data['header']['sum+vat']=round($data['header']['sum+vat'], 2);
		$data['header']['sum.total']=$data['header']['sum+vat'];
		foreach (array('', '.vat', '+vat') as $v) $data['header']['sum'.$v.'.fmt']=number_format($data['header']['sum'.$v], 2, '.', ' ');
		$data['header']['order_url']=$axs['http'].'://'.$_SERVER['SERVER_NAME'].$axs['http_root'].'?'.axs_url(array('c'=>'eshop', 'l'=>$axs['l'], 'order_nr'=>$data['header']['order_nr'], ));
		
		# <Org data>
		//require_once($axs['f_px'].'business.class.php');
		$org=new axs_business($site);
		$data['org']=$org->org_get($data['header']['profile_id'], $l, 'org.');
		# </Org data>
		return $data;
		} #</order_get()>
	function order_get_img($order_id, $id) { # Output product image
		$id+=0;
		header('Content-type: image/jpeg');
		axs_exit(readfile($this->dir.$order_id.'-'.$id.'.jpg'));
		} # </img()>
	function order_get_img_html($order_id, $id, $url) { # Get product image HTML code
		$id+=0;
		return (file_exists($tmp=$this->dir.$order_id.'-'.$id.'.jpg')) ? '<img src="'.$url.'" alt="'.$order_id.'-'.$id.'" />':'';
		} #</img_html()>
	#</Get data>
	
	# output order in txt, html or pdf format
	function doc_output($data, $format, $stream=false) {
		return self::order_output($data, $format, $stream=false);
		} #</doc_output()>
	static function order_output($data, $format, $stream=false) {
		global $axs;
		if (!$data['header']['id']) return false;
		$formats=array(
			'txt'=>array('mime'=>'text/plain', 'tpl'=>'txt'),
			'html'=>array('mime'=>'text/html', 'tpl'=>'tpl'),
			'pdf'=>array('mime'=>'application/pdf', 'tpl'=>'tpl'),
			);
		$row_tpl=axs_tpl(false, 'business.orders.export.row.'.$formats[$format]['tpl']);
		$doc=array('charset'=>$axs['cfg']['charset'], 'l'=>$data['l'], 'base_href'=>realpath(AXS_SITE_ROOT).'/', 'rows'=>'');
		/*if ($format==='txt') {
			include_once(axs_dir('lib').'class.stringgrid.php');
			$h=array('nr'=>3, 'text'=>32, 'amount'=>6, 'price'=>8, 'vat'=>3, 'sum'=>8);
			$tabArray = array();
			$tabArray['colWidth']  = array(3,	32,	6,	8,	3,	8);
			$tabArray['hAlign']    = array(2,	0,	); # align 0->left, 1->center, 2 -> right
			$tabArray['vAlign']    = array(0, 0, 0,  0, 2, 2); # align 0->top,  1->center, 2 -> bottom
			$cs = new stringGrid();
			$doc['rows'] = $cs->GetCollumned($data['rows'], $tabArray, '|', '-', "\n");
			}
		else*/ foreach ($data['rows'] as $k=>$v) {
			$v['text']=nl2br($v['text']);
			$v['img_html']=($v['img']) ? '<img src="'.htmlspecialchars($v['img']).'" alt="'.htmlspecialchars(basename($v['img'])).'" />':'';
			$doc['rows'].=axs_tpl_parse($row_tpl, $v);
			}
		$doc=axs_tpl_parse(axs_tpl(false, 'business.orders.export.'.$formats[$format]['tpl']), $doc+$data['header']+$data['org']+$data['labels']);
		if ($stream===true) $stream=self::doc_output_filename($data['labels']['order_nr.lbl'], $data['header']['order_nr'], $format);
		if ($format=='txt') $doc=html_entity_decode(strip_tags($doc), ENT_QUOTES, $axs['cfg']['charset']);
		if ($format=='pdf') {
			require_once(axs_dir('plugins').'dompdf/dompdf_config.inc.php');
			$pdf=new DOMPDF();
			$pdf->set_protocol('file://');
			$pdf->load_html($doc);
			$pdf->render();
			if ($stream) exit($pdf->stream($stream));
			$doc=$pdf->output();
			}
		if ($stream) {
			header('Content-type: '.$formats[$format]['mime']);
			header('Content-Disposition: attachment; filename="'.$stream.'"');
			exit($doc);
			}
		return $doc;
		} #</order_output()>
	static function doc_output_filename($lbl, $nr, $format) {
		return strtolower(axs_valid::f_name(preg_replace('/^www\./', '', $_SERVER['SERVER_NAME']).'-'.$lbl.'-'.$nr.'.'.$format));
		} #</doc_output_filename()>
	
	static function save() {
		# <order header>
		//$token=strtoupper(substr(uniqid('-'), -4));
		$qry=array();
		foreach (self::$client_fields as $k=>$v) $qry[$k]="`client_".$k."`='".addslashes($client[$k])."'";
		$cl['order_id']=axs_db_query("INSERT INTO `".$this->px."business_orders`\n".
		"	SET time='".$axs['time']."', `profile_id`='".$vr['profile_id']."', `client_id`='".$axs_user['id']."', `status`='-', `client_ip`='".self::ip_to_number()."',\n".
		implode(', ', $qry).",\n".
		"	`currency`='".axs_eshop::$cfg['currency']."'", 'insert_id', axs_eshop::$db, __FILE__, __LINE__);
		# <order rows>
		$fs=new axs_filesystem_img('', axs_eshop::$site_nr);
		$form->structure_set_dir($this->dir);
		$nr=1;
		foreach ($cart as $k=>$v) {
			//$v['label']=html_entity_decode(strip_tags($v['label']), ENT_QUOTES, $axs['cfg']['charset']);	
			$id=axs_db_query("INSERT INTO `".axs_eshop::$px."orders_table`\n".
			"	SET `nr`='".$nr."', `_parent_id`='".($cl['order_id']+0)."', `product_id`='".($v['product_id']+0)."',\n".
			"	`product_form`='".addslashes($v['product_form'])."', `label`='".addslashes($v['label'])."', `price`='".($v['price']+0)."',\n".
			"	`vat`='".($v['vat']+0)."', `amount`='".($v['amount']+0)."', `amount_unit`='".($v['amount_unit_type']+0)."'",
			'insert_id', axs_eshop::$db, __FILE__, __LINE__);
			if (file_exists($v['product_img_file'])) $form->fs->img_proc(
				$v['product_img_file'], $cl['order_id'].'-'.$id.'.jpg', array('dir_read'=>'', )+self::$img
				);
			//copy($v['product_img_file'], axs_dir('content').$this->dir;
		//.$cl['order_id'].'-'.$id.'.jpg');
			$nr++;
			}
		} #</save()>
	} #</class::axs_business_orders>
/*
--
-- Table structure for table `axs_business_orders`
--
CREATE TABLE IF NOT EXISTS `axs_business_orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `time` int(10) unsigned NOT NULL DEFAULT '0',
  `profile_id` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '=business_profiles.id',
  `compiler_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'ateendant id (=users.id)',
  `client_id` int(10) unsigned NOT NULL DEFAULT '0',
  `client_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_estonian_ci NOT NULL DEFAULT '',
  `client_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_estonian_ci NOT NULL DEFAULT '',
  `client_postcode` varchar(255) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `client_email` varchar(255) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `client_phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_estonian_ci NOT NULL DEFAULT '',
  `client_ip` int(10) unsigned NOT NULL DEFAULT '0',
  `currency` char(3) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `payment_method` tinyint(3) unsigned DEFAULT NULL COMMENT '=banklink.id',
  `revenue` decimal(7,2) NOT NULL,
  `status` enum('-','accept','problem','complete','cancel','draft') CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '-',
  `invoice_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '=invoice.id',
  `updated` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'modified timestamp',
  `updated_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '=users.id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
--
-- Table structure for table `axs_business_orders_table`
--
CREATE TABLE IF NOT EXISTS `axs_business_orders_table` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nr` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `_parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0',
  `product_form` enum('','eshop','products') CHARACTER SET ascii COLLATE ascii_bin NOT NULL COMMENT 'which table is the product id from',
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_estonian_ci NOT NULL,
  `price` decimal(7,2) NOT NULL DEFAULT '0.00',
  `vat` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'vat %',
  `amount` decimal(12,2) unsigned NOT NULL DEFAULT '0.00',
  `amount_unit` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `updated` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'modified timestamp',
  `updated_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '=users.id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
*/
#2008-12-10 ?>