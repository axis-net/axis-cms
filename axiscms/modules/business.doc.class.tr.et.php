<?php #2015-07-12
return array(
	'client_lbl'=>'Saaja',
	'client_address_lbl'=>'Aadress',
	'invoice_nr_lbl'=>'Arve nr',
	'order_nr_lbl'=>'Tellimuse nr',
	'date_lbl'=>'Kuupäev',
	'payment_period_lbl'=>'Maksetähtaeg',
	'payment_period_unit_lbl'=>'päeva',
	'payment_period_prepayment_lbl'=>'ettemaks',
	'payment_method_lbl'=>'Makseviis',
	'payment_method_bank_lbl'=>'pangaülekanne',
	'payment_method_card_lbl'=>'kaardimakse',
	'payment_method_cash_lbl'=>'sularaha',
	'nr_lbl'=>'nr',
	'item_name_lbl'=>'Kauba / teenuse nimetus',
	'unit_lbl'=>'Ühik',
	'amount_lbl'=>'Kogus',
	'price_lbl'=>'Hind',
	'vat_lbl'=>'KM',
	'sum_lbl'=>'Summa',
	
	'sum_footing_lbl'=>'Kokku',
	'sum_vat_lbl'=>'KM summa',
	'sum_total_lbl'=>'Tasuda',
	'compiler_name_lbl'=>'Arve koostas',
	);
#2008-12-09 ?>