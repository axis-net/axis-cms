<?php #24.04.2016
if (!defined('AXS_PATH_EXT')) exit(require('../axiscms/log.php'));

# <Structure>
//$m->header=new axs_form_edit($m->site_nr, $m->url, false, '', $_POST);
/*$f->header->sql_header_get($m->name, $m->form);
$f->header->structure['save']=array('type'=>'submit', 'label'=>$m->tr->s('save_lbl'), 'label.html'=>$m->tr->s('save_lbl'), );
$f->header->structure_set();
$f->thead=$f->header->structure_get_thead();*/
# </Structure>

require_once($m->f_path.$m->f_px.$f->name.'.class.php');
$bs=new axs_business($f->site_nr);

# <Edit>
if (isset($_GET[$f->header->key_edit])) return $f->editor();

# Perform database query
$result=axs_db_query('SELECT SQL_CALC_FOUND_ROWS t.*, m.`user` AS `updated_text`'."\n".
'	FROM `'.$f->table.'` AS t LEFT JOIN `'.$axs['cfg']['db'][1]['px'].'users` AS m ON m.id=t.updated_uid ORDER BY `id` ASC',
1, $f->db, __FILE__, __LINE__);

# Display result rows
$th=$f->thead;
unset($th['id']);
$table='';
foreach ($result as $cl) {
	$cl['class']=($cl['disable']) ? ' inactive':'';
	$table.=
	'      <table id="id'.$cl['id'].'" class="table table_data vertical'.$cl['class'].'">'."\n".
	'       <caption>'.$f->thead['id']['label'].'#'.$cl['id'].' <a href="'.'?'.axs_url($f->url, array('id'=>$cl['id'], $f->header->key_edit=>$cl['id'], 'b'=>'?'.$_SERVER['QUERY_STRING'].'#id'.$cl['id'])).'">'.$m->tr('edit_lbl').'</a></caption>';
	foreach ($th as $k=>$v) {
		$table.='       <tr class="'.$k.'"><th scope="row">'.$v['label'].'</th><td class="'.$k.'">'.$f->header->value_display($k, $v, $cl).'</td></tr>'."\n";
		}
	$table.='      </table>'."\n".'&nbsp;'."\n";
	}
unset($result);

$pager=$f->pager(axs_get('p', $_GET));
return $pager."\n".$f->add_new_form(array($f->header->key_edit=>'', 'b'=>'?'.$_SERVER['QUERY_STRING']))."\n".$table."\n".$pager;
/*
CREATE TABLE IF NOT EXISTS `axs_business_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `disable` tinyint(4) NOT NULL DEFAULT '0',
  `name_en` text COLLATE utf8_estonian_ci,
  `brand_en` varchar(255) COLLATE utf8_estonian_ci DEFAULT NULL,
  `reg_nr` text COLLATE utf8_estonian_ci NOT NULL,
  `vat_nr` text COLLATE utf8_estonian_ci NOT NULL,
  `address` text COLLATE utf8_estonian_ci NOT NULL,
  `phone` text COLLATE utf8_estonian_ci NOT NULL,
  `fax` text COLLATE utf8_estonian_ci NOT NULL,
  `email` text COLLATE utf8_estonian_ci NOT NULL,
  `web` text COLLATE utf8_estonian_ci NOT NULL,
  `bank` text COLLATE utf8_estonian_ci NOT NULL,
  `iban` text COLLATE utf8_estonian_ci NOT NULL,
  `swift` text COLLATE utf8_estonian_ci NOT NULL,
  `invoice_nr_period` char(1) CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
  `invoice_nr_format` varchar(255) COLLATE utf8_estonian_ci NOT NULL,
  `updated` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Last update timestamp',
  `updated_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '=users.id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_estonian_ci AUTO_INCREMENT=3 ;

INSERT INTO `axs_business_profiles` VALUES(1, 0, 'Firma OÜ', 'Firma', '1234567890', 'EE123456', 'Aadr', '+372 123 1234', '', '', 'www.site.ee', 'AS SEB Pank\r\nBank address: Tornimäe 2, 15010 Tallinn\r\nPhone: +372 665 5100', 'EE111127361573672672 AS SEB Pank', 'EEUHEF2X', 'y', '{$y}-{$m}-{$d}.{$nr}', 0, 0);
*/
#05.11.2009 ?>