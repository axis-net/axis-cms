<?php #2018-09-12
class axs_business_module extends axs_db_edit {
	function structure(){
		global $axs;
		parent::structure();
		if ($this->parent->f==='invoices') $this->fn=new axs_business_invoices($this->site_nr=1, $axs['l'], $this->form->tr);
		} #</_init()>
	function list_() {
		global $axs;
		if ((isset($this->fn)) && (!empty($_GET['pdf']))) $this->fn->export_pdf(intval($_GET['pdf']), true);
		return parent::list_();
		} #</list_()>
	function list_item(&$cl, &$vr=array()) {
		if ($this->parent->f==='invoices') $cl['nr']=$this->fn->nr_display($cl);
		parent::list_item($cl, $vr);
		if (isset($this->fn)) $vr['_tools']['pdf']=array('url'=>'?'.axs_url($this->url, array('pdf'=>$cl['id'], ), false), 'label'=>'PDF', );
		} #</list_item()>
	function edit_vars() {
		global $axs;
		parent::edit_vars();
		if (!implode($this->form->vl['time'])) {
			$this->form->vl['time']=array('date'=>date('Y-m-d'), );
			if (!empty($this->post['_add'])) {
				$this->post['_'][key($this->post['_add'])]['time']=$this->form->vl['time'];
				$this->post['_'][key($this->post['_add'])]['status']='draft';
				}
			}
		if ((empty($this->form->vl['nr'])) && (!empty($this->form->vl['profile_id']))) $this->form->vl['nr']=$this->fn->nr_get($this->form->vl)+1;
		} #</edit_vars()>
	function edit_end() {
		$tf=array('price'=>0, 'vat'=>0, 'sum'=>0, );
		foreach ($this->form->vl['rows'] as $k=>$v) {
			foreach (array('price'=>'', 'amount'=>'', 'vat'=>'', ) as $kk=>$vv) $v[$kk]=floatval($v[$kk]);
			$v['sum']=$v['price']*$v['amount'];
			$v['vat']=$v['sum']/100*$v['vat'];
			$v['sum']+=$v['vat'];
			$this->form->vl['rows'][$k]['sum']=number_format($v['sum'], 2, '.', '');
			foreach ($tf as $kk=>$vv) $tf[$kk]+=$v[$kk];
			}
		foreach ($tf as $k=>$v) $this->form->vl['rows']['_tfoot'][$k]=number_format($v, 2, '.', '');
		} #</edit_end()>
	} #</class::axs_business_module>
#2017-09-05 ?>