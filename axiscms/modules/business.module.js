//2019-10-25
axs.business={
	formRows:'form.edit #rows',
	tf:{'price':0,'vat':0,'sum':0},
	init:function(){
		var t=$(this.formRows);
		if(t.length) {
			for(k in this.tf) $(this.formRows+' tfoot td.'+k).html('<input type="number" class="'+k+'" value="'+$(this.formRows+' tfoot td.'+k).html()+'" readonly="readonly" />');
			this.calcAttach();
			}
		},//</init()>
	calc:function(){
		//console.log(this.value);
		var o=axs.business;
		var tr=$(o.formRows+' tbody tr:not(.delete)');
		var tf={'price':0,'vat':0,'sum':0};
		for(var i=0, l=tr.length; i<l; i++){
			var n={'price':0,'amount':0,'vat':0};
			for(k in n) {
				var v=$(tr[i]).find('td.element.'+k+'>input[type="number"]');
				if (v.length){
					v=Number(v.val());
					if ((typeof(v)==='number')&&(!isNaN(v))) n[k]=v;
					}
				}
			var calc={'price':n.price*n.amount};
			calc.vat=calc.price/100*n.vat;
			calc.sum=calc.price+calc.vat;
			$(tr[i]).find('td.element.sum input[type="number"]:first').val(calc.sum.toFixed(2));
			for (k in calc) tf[k]+=calc[k];
			}
		for (k in tf) $(o.formRows+' tfoot td.'+k+'>input[type="number"]').val(tf[k].toFixed(2));
		},//</calc()>
	calcAttach:function(){
		var s=axs.business.formRows;
		$(s+' td.price input[type="number"]:not(.calcEnabled), '+s+' td.amount input[type="number"]:not(.calcEnabled), '+s+' td.vat input[type="number"]:not(.calcEnabled)').addClass('calcEnabled').on('keyup',axs.business.calc).on('change',axs.business.calc);
		$(s+' ._add input[type="submit"]:not(.calcEnabled)').addClass('calcEnabled').click(axs.business.calcAttach);
		$(s+' td._del input[type="checkbox"]:not(.calcEnabled)').addClass('calcEnabled').on('change',axs.business.calc);
		$(s+' tbody td.price:not(.calcPerc), '+s+' tfoot td.price:not(.calcPerc)').append('<input class="calcPerc" type="button" value="x%" data-value="1" />');
		$(s+' tbody td.price:not(.calcPerc) input[type="button"].calcPerc, '+s+' tfoot td.price:not(.calcPerc) input[type="button"].calcPerc').on('click',axs.business.calcPercCreate);
		$(s+' tbody td.price:not(.calcPerc), '+s+' tfoot td.price:not(.calcPerc)').addClass('calcPerc');
		},//</calcAttach()>
	calcPerc:function(){
		var perc=Number($('#calcPerc input.perc').val());
		var num=Number($('#calcPerc input.num').val());
		$('#calcPerc input.result').val((num+num/100*perc).toFixed(2));
		$('#calcPerc').siblings('input[type="button"].calcPerc').data('value',perc);
		},//</calcPerc()>
	calcPercCreate:function(){
		$('#calcPerc').remove();
		$(this).after(
			'<fieldset id="calcPerc">'+
			'<legend lang="en"><span class="visuallyhidden">%</span><a href="#"><abbr title="close">X</abbr></a></legend>'+
			'<label><span lang="en" class="visuallyhidden">number</span><input type="number" class="num" step="0.01" /></label>+'+
			'<label><input type="number" class="perc" step="any" />%</label>'+
			'=<input type="number" class="result" lang="en" title="copy" value="" readonly="readonly" />'
			);
		$('#calcPerc input.perc, #calcPerc input.num').on('change',axs.business.calcPerc).on('keyup',axs.business.calcPerc);
		$('#calcPerc input.perc').val($('#calcPerc').siblings('input[type="button"].calcPerc').data('value'));
		$('#calcPerc input.num').val($('#calcPerc').siblings('input[type="number"]').val());
		$('#calcPerc input.result').click(function(){
			this.select();
			document.execCommand("copy");
			$(this).closest('td').find('input[type="number"]:first').val($(this).val());
			axs.business.calc();
			$('#calcPerc').remove();
			});
		$('#calcPerc legend a').on('click',function(e){	e.preventDefault();	$('#calcPerc').remove();	});
		axs.business.calcPerc();
		}//</calcPercCreate()>
	}//</class::axs.business>
//2019-03-25