<?php #2018-11-22
return array(
	'nr.lbl'=>'Tellimuse number',
	'search.lbl'=>'Otsi',
	'time.lbl'=>'Aeg',
	'payment_method.lbl'=>'Makseviis',
	'revenue.lbl'=>'Laekunud',
	'status.lbl'=>'Staatus',
	'status.-.lbl'=>'-',
	'status.accept.lbl'=>'töös',
	'status.problem.lbl'=>'probleem',
	'status.complete.lbl'=>'valmis',
	'status.cancel.lbl'=>'tühistatud',
	'invoice_id.lbl'=>'arve',
	
	'content.lbl'=>'Tellimuse sisu',
	'price.lbl'=>'Hind',
	'amount.lbl'=>'Kogus',
	'amount_unit.lbl'=>'Ühik',
	'vat.lbl'=>'Käibemaks',
	'vat.abbr'=>'KM%',
	'sum.lbl'=>'Summa',
	'sum.vat.lbl'=>'KM summa',
	'sum+vat.lbl'=>'Kokku',
	
	'client_name.lbl'=>'Tellija nimi',
	'client_address.lbl'=>'Aadress',
	'client_postcode.lbl'=>'Postiindeks',
	'client_phone.lbl'=>'Telefon',
	'client_email.lbl'=>'E-post',
	'order_nr.lbl'=>'Tellimuse number',
	
	'ref.txt'=>'Hiljem saate soovi korral tellimuse juurde tagasi p&ouml;&ouml;rduda kasutades tellimuse numbrit.',
	);
#2008-12-09 ?>