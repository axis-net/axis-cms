<?php # AXIS CMS system config file (modified by adm @ 30.07.2021 19:21:01)
return array( #<system config />
	'cfg_ver'=>'2.0.0-dev45 (build 2021-07-30)',
	'emails'=>array('support.axis@hot.ee'), #<system e-mail addresses />
	'dontmail'=>array('login','loginsend','url','site_post','filemonitor','axs_dbg'), #<message types, that should not be notified by e-mail />
	'admin_contact'=>'', #<on feedback form />
	'login_update'=>600, #<Update login session after n seconds />
	'logsize'=>5, #<number of logfiles to keep (+1MB buffer space needed) />
	'loguser'=>100, #<number of log records to keep about user actions />
	'filemonitor'=>array('admin/ x:.'), #<monitor changes in these files/folders />
	'charset'=>'utf-8', #<system character set />
	'timezone'=>'Europe/Tallinn', #<system default timezone />
	'cms_lang'=>'et', #<CMS default language />
	'cms_skin'=>'', #<CMS template set />
	'cms_http'=>'', #<CMS HTTP path />
	'ftp'=>'', #<custom FTP URL />
	'image_convert_util'=>'', #<ImageMagic/GraphicsMagic path />
	'smtp_host'=>'', #<SMTP host />
	'smtp_port'=>'', #<SMTP port />
	'smtp_encryption'=>'', #<SMTP encryption />
	'smtp_user'=>'', #<SMTP user />
	'smtp_pass'=>'', #<SMTP password />
	'groups'=>array( #<user groups />
		'cms'=>'cms',
		'admin'=>'admin',
		'dev'=>'developer',
		'guest'=>'guest',
		),
	'roles'=>array( #<user roles />
		'r'=>'read',
		'w'=>'write',
		'e'=>'export',
		),
	'db'=>array( #<databases config />
		1=>array('type'=>'', 'host'=>'localhost', 'user'=>'root', 'pass'=>'', 'db'=>'', 'collation'=>'utf8mb4', ),
		),
	'site'=>array( #<sites config />
		1=>array('title'=>'Veebileht1', 'timezone'=>'', 'recycle'=>25, 'db'=>0, 'prefix'=>'axs_', 'url_fake'=>false, 'domain'=>'', 'dir'=>'', 'dir_fs_root'=>'', 'langs'=>array('et'=>array('l'=>'eesti keeles','abbr'=>'EST','h'=>false), ), ),
		),
	);
?>