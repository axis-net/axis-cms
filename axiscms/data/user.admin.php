<?php # AXIS CMS user account file (modified by adm @ 16.04.2021 13:34:03)
return array(
	'user'=>'admin', # username
	'pass'=>'$2y$10$YHPdE6rxIZjojLs.2b.MPOAKuBWxGnjcRYmyr6r3ujnRT8HJ5o2D.', # password
	'lock'=>0, # account disabled
	'fstname'=>'Eesnimi', # user's first name
	'lstname'=>'Perenimi', # user's last name
	'email'=>'', # e-mail address(es)
	'phone'=>'', # user phone(es)
	'g'=>array('admin'=>true, ), # group membership
	'homedirs'=>array( # home directorys
		1=>array('site'=>1, 'dir'=>'', ),
		),
	);
?>