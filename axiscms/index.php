<?php #2022-04-29
if (floatval(PHP_VERSION)<5.5) echo '<p class="msg" lang="en">Cannot run on PHP '.PHP_VERSION.' - PHP 5.5 or newer required!</p>';
#<AXIS NET Framework>
define('AXS_START_TIME', array_sum(explode(' ', microtime())));
define('AXS_ADMIN', true);
if (!defined('AXS_PATH_SYS')) define('AXS_PATH_SYS', '../'); # <Path to SYSTEM root />
if (!defined('AXS_SITE_ROOT')) define('AXS_SITE_ROOT', '../'); # <Path to SITE root />
if (!defined('AXS_PATH_CMS')) define('AXS_PATH_CMS', __DIR__.'/'); # <Path to SYSTEM root />
//if (!defined('AXS_PATH_CMS_HTTP')) define('AXS_PATH_CMS_HTTP', AXS_PATH_CMS); # <http path to CMS />
require_once(AXS_PATH_CMS.'lib/axs.php');
if (isset($_REQUEST['axs']['cms_ver_get'])) exit(AXS_CMS_VER);
$axs['ob']=axs_ob_catch(); #<START output buffering for error logging />
#</AXIS NET Framework>

$axs['page']=array('head'=>array(), 'title'=>array(), 'title_path'=>array(), 'code_head'=>'', 'class'=>array(), 'header'=>'', 'menu'=>'', 'breadcrumb'=>'', );
$axs['url']=array();
if (isset($axs['get']['section'])) $axs['url']['axs']['section']=$axs['get']['section'];

if (isset($_POST['axs']['login'])) axs_user_login::login($_POST);
if (isset($_GET['axs']['login']['out'])) axs_user_login::logout($_GET['axs']['login']['out']);
if (isset($_GET['axs']['login']['keepalive'])) axs_user::keepalive();
if (strlen(axs_get('popup', $_GET))) $axs['get']['gw']=$_GET['popup']; #Legacy
if (isset($axs['get']['gw'])) require(AXS_PATH_CMS.'index.gateway.php');
if (!defined('AXS_LOGINCHK')) axs_user::init();

$axs['editor']=new axs_admin(AXS_PATH_CMS, 'index'); #<Initialize editor functions />
$axs['editor']->user_home_get(axs_get('h', $_GET), axs_get('l', $_GET)); #<Homedirs and content menu for current user, content language />
$axs['editor']->structure=array(
	'dashboard'=>array(
		'url'=>'./',
		'label'=>$axs['editor']->tr('menu.dashboard'),
		),
	'content'=>array(
		'url'=>'#cms_menu_content',
		'label'=>$axs['editor']->tr('menu.content'), 'txt'=>$axs['editor']->tr('menu.content.comment'),
		'accesskey'=>'', 'attribs'=>array('id'=>'cms_menu_content'),
		'show'=>array('m'=>'', 'd'=>'', ),
		'submenu'=>$axs['content_menu'],
		),
	'db'=>array(
		'url'=>'?'.axs_url($axs['url'], array('e'=>'db', )),
		'label'=>$axs['editor']->tr('menu.db'), 'txt'=>$axs['editor']->tr('menu.db.comment'), 
		'accesskey'=>'',
		'show'=>array('m'=>'', 'd'=>'', ),
		),
	'user'=>array(
		'url'=>'?'.axs_url($axs['url'], array('e'=>'user')),
		'label'=>$axs['editor']->tr('menu.user'), 'txt'=>$axs['editor']->tr('menu.user.comment'), 
		'show'=>array('d'=>'', ),
		'include'=>'edit-users',
		),
	'users'=>array(
		'url'=>'?'.axs_url($axs['url'], array('e'=>'users')),
		'label'=>$axs['editor']->tr('menu.users'),
		'txt'=>$axs['editor']->tr('menu.users.comment'), 
		'accesskey'=>'',
		'show'=>array('m'=>'', 'd'=>'', ),
		),
	'system'=>array(
		'url'=>'?'.axs_url($axs['url'], array('e'=>'system', 's1'=>'cfg')), 
		'label'=>$axs['editor']->tr('menu.system'),
		'txt'=>$axs['editor']->tr('menu.system.comment'), 
		'accesskey'=>'',
		'attribs'=>array('id'=>'cms_menu_system'),
		'show'=>array('m'=>'', 'd'=>'', ),
		'submenu'=>array(
			'cfg'=>array('url'=>'?'.axs_url($axs['url'], array('e'=>'system', 's1'=>'cfg')), 'label'=>$axs['editor']->tr('menu.system.cfg'),'accesskey'=>'', ),
			'logview'=>array('url'=>'?'.axs_url($axs['url'], array('e'=>'system', 's1'=>'logview')), 'label'=>$axs['editor']->tr('menu.system.log'), 'accesskey'=>'', ),
			'modules'=>array('url'=>'?'.axs_url($axs['url'], array('e'=>'system', 's1'=>'modules')), 'label'=>$axs['editor']->tr('menu.system.modules'), 'accesskey'=>'', ),
			'filemonitor'=>array('url'=>'?'.axs_url($axs['url'], array('e'=>'system', 's1'=>'filemonitor')), 'label'=>$axs['editor']->tr('menu.system.filemonitor'), 'accesskey'=>'', ),
			
			'sql'=>array('url'=>'?'.axs_url($axs['url'], array('e'=>'system', 's1'=>'sql')), 'label'=>$axs['editor']->tr('menu.system.sql'), 'accesskey'=>'', ),
			'sysinfo'=>array('url'=>'?'.axs_url($axs['url'], array('e'=>'system', 's1'=>'sysinfo')), 'label'=>$axs['editor']->tr('menu.system.sysinfo'), 'accesskey'=>'', ),
			),
		),
	'doc'=>array(
		'url'=>'?'.axs_url($axs['url'], array('e'=>'doc')),
		'label'=>$axs['editor']->tr('menu.doc'), 'txt'=>$axs['editor']->tr('menu.doc.comment'), 
		'accesskey'=>'',
		'show'=>array('m'=>'', 'd'=>'', ),
		),
	'feedback'=>array(
		'url'=>'#feedback_form',
		'label'=>$axs['editor']->tr('menu.feedback'), 'txt'=>$axs['editor']->tr('menu.feedback.comment'), 
		'accesskey'=>'',
		'attribs'=>array('id'=>'feedback_menu', ),
		'show'=>array('m'=>'', 'd'=>'', ),
		),
	'download'=>array(),
	'ftp'=>array(),
	'files'=>array('include'=>'edit-content'),
	);
if (AXS_LOCALHOST) $tmp=axs_fn::array_el_move($axs['editor']->structure, 'doc', 1);
if (isset($axs['editor']->structure[axs_get('e', $_GET)])) $axs['e']=$_GET['e'];
else {	$axs['e']=((isset($_GET['c'])) && (isset($_GET['l']))) ? 'content':key($axs['editor']->structure);	}
$axs['include']=(!empty($axs['editor']->structure[$axs['e']]['include'])) ? $axs['editor']->structure[$axs['e']]['include']:'edit-'.$axs['e'];
$axs['include_ext']=false;
$axs['page']['class']['e']='edit-'.$axs['e'];
#$axs['url']=array();
$axs['url']['e']=$axs['editor']->url['e']=$axs['e'];
#$axs['editor']=new axs_admin(AXS_PATH_CMS, 'index'); #<Initialize editor functions />
$axs['editor']->get=(isset($axs['get']['section'])) ? $axs['get']['section']:false;

if (file_exists(AXS_PATH_CMS.'data/index.customize.php')) include(AXS_PATH_CMS.'data/index.customize.php');
/*if (!defined('AXS_PATH_EXT')) { # <Path to intranet />
	if (@file_exists(AXS_SITE_ROOT.'admin/index.php')) define('AXS_PATH_EXT', (basename(dirname(__FILE__)==='admin') ? './':'../admin/'));
	}
if (defined('AXS_PATH_EXT')) {
	if (file_exists($tmp=AXS_PATH_EXT.$axs['f_px'].'config.php')) include($tmp);
	if (!defined('AXS_PATH_EXT_HTTP')) define('AXS_PATH_EXT_HTTP', AXS_PATH_EXT);
	}*/

foreach ($axs['editor']->structure as $k=>$v) {
	if (!isset($v['act'])) $axs['editor']->structure[$k]['act']=$axs['e'];
	if (isset($v['submenu'])) foreach ($v['submenu'] as $kk=>$vv) {
		if (!isset($vv['act'])) $axs['editor']->structure[$k]['submenu'][$kk]['act']=axs_get('s1', $_GET);
		}
	}	

$axs['editor']->breadcrumb=array();
if (isset($axs['editor']->structure[$axs['e']])) {
	$axs['editor']->breadcrumb[$axs['e']]=$axs['editor']->structure[$axs['e']];
	if (!empty($axs['editor']->breadcrumb[$axs['e']]['submenu'])) foreach ($axs['editor']->breadcrumb[$axs['e']]['submenu'] as $k=>$v) {
		if ($k===$v['act']) $axs['editor']->breadcrumb[$axs['e'].'.'.$k]=$v;
		}
	}

$axs['include']=axs_dir('editors').$axs['include'].'.php';
# <Include an external website to AXIS CMS admin environment if needed />
//if (defined('AXS_PATH_EXT')) {	if (file_exists(AXS_PATH_EXT.'axs_index.php')) include(AXS_PATH_EXT.'axs_index.php');	}

if ($axs['include']) $axs['page']['content']=$axs['editor']->load_editor($axs['include']);
if (isset($axs['get']['ajax'])) exit('-');

if (!$axs['editor']->get) { #<Header and CMS menu />
	$axs['editor']->initialize('index', AXS_PATH_CMS);
	# <Header>
	$axs['editor']->tpl_set('index_header',
	'<header id="axs_cms_header">'."\n".
	'	<a class="logo" href="./"><span></span><span></span><span></span><img src="{$AXS_PATH_CMS}gfx/logo.svg" alt="AXIS CMS" title="AXIS CMS {$title}" width="30" /></a>'."\n".
	'	<script>axs.animationcontrol.logo("#axs_cms_header>.logo",{runEvent:"doc"});</script>'."\n".
	'	<nav id="axs_cms_menu" class="axs menu horizontal mobile_toggle mobile_toggleout dropdown dropout animation expand-down">'."\n".
	'		<h2 class="title"><a href="#axs_cms_menu" class="toggle-switch"><span class="icon" title="{$index.menu.title}"></span></a></h2>'."\n".
	'{$menu}'."\n".
	'	</nav>'."\n".
	'	<script>axs.menu.attach("axs_cms_menu");</script>'."\n".
	'	<nav class="site_link"><a href="{$AXS_SITE_ROOT}">{$home}</a></nav>'."\n".
	'{$user}'."\n".
	'</header>'."\n\n");
	$axs['editor']->vars=array(
		'AXS_PATH_CMS'=>AXS_PATH_CMS_HTTP,
		'AXS_DOMAIN'=>AXS_DOMAIN,
		'menu'=>'',
		'title'=>$axs['editor']->tr('title.txt'),
		'AXS_SITE_ROOT'=>AXS_SITE_ROOT,
		'home'=>trim($_SERVER['SERVER_NAME'].$axs['http_root'], '/'),
		'user'=>'',
		);
	if (AXS_LOGINCHK==1) {
		# <CMS Menu. Include menu bar if logged in and user is member of "admin" or "cms" group>
		if (axs_user::permission_get(array('cms'=>'', 'admin'=>'', 'dev'=>'', ))) {
			$axs['editor']->menu=array();
			foreach ($axs['editor']->structure as $k=>$v) {	if (isset($v['show']['m'])) $axs['editor']->menu[$k]=$v;	}
			foreach ($axs['editor']->menu as $k=>$v) {
				if (!empty($v['submenu'])) foreach ($v['submenu'] as $kk=>$vv) {
					if ($kk===$vv['act']) $axs['page']['title']=array_merge(array(0=>$vv['label']), $axs['page']['title']);
					}
				if ($k===$v['act']) $axs['page']['title']=array_merge(array(0=>$v['label']), $axs['page']['title']);
				}
			$axs['page']['menu']=$axs['editor']->vars['menu']=$axs['editor']->menu_build($axs['editor']->menu, '', array());
			}
		# </CMS Menu>
		$axs['editor']->vars['user']=
		'			<nav id="axs_user" class="axs menu mobile_toggle mobile_toggleout screen_toggle screen_toggleout animation expand-down">'."\n".
		'				<h2 class="title">'."\n".
		'					<a href="#axs_user" class="toggle-switch">'."\n".
		'						<img class="avatar" src="'.AXS_PATH_CMS_HTTP.'gfx/ui.user.png" alt="'.axs_html_safe($axs_user['fstname'].' '.$axs_user['lstname']).'" width="30" />'."\n".
		'						<span>'."\n".
		'							<span class="name">'.axs_html_safe($axs_user['fstname'].' '.$axs_user['lstname']).'</span>'."\n".
		'							<span class="user">'.axs_html_safe($axs_user['user']).'</span>'."\n".
		'						</span>'."\n".
		'					</a>'."\n".
		'				</h2>'."\n".
		'				<ul>'."\n".
		'					<li><a href="'.$axs['editor']->structure['user']['url'].'">'.$axs['editor']->tr('menu.user').'</a></li>'."\n".
		'					<li><a href="?'.urlencode('axs[login][out]').'='.urlencode('./').'">'.$axs['editor']->tr('logout.txt').'</a></li>'."\n".
		'				</ul>'."\n".
		'				<script>axs.menu.attach("axs_user");</script>'."\n".
		'				<script src="?'.urlencode('axs[login][keepalive]').'=0"></script>'."\n".
		'			</nav>'."\n";
		}
	//dbg($axs['editor']->templates['index_header']);
	$axs['page']['header']=axs_tpl_parse($axs['editor']->templates['index_header'], $axs['editor']->vars);
	# </Header>
	#<Breadcrumb>
	$axs['page']['breadcrumb']=
	'<nav id="breadcrumb" class="axs menu horizontal dropdown dropout">'."\n".
	'	'.$axs['editor']->menu_build($axs['editor']->breadcrumb, ' ', array())."\n".
	'	<script>axs.menu.attach("breadcrumb");</script>'."\n".
	'</nav>'."\n";
	#</Breadcrumb>
	}

$axs['page']['title']=axs_html_safe(implode(' < ', array_reverse($axs['page']['title'])));
if (!empty($axs['page']['title'])) $axs['page']['title'].=' - ';

if ($axs['ob']) axs_ob_catch($axs['ob']);
define('AXS_OUTPUT_START', true);
?><!DOCTYPE html>
<html lang="<?php echo $axs['editor']->l; ?>" class="<?php echo implode(' ', $axs['page']['class']); ?>">
<head>
<meta charset="<?php echo $axs['cfg']['charset']; ?>" />
<title><?php echo $axs['page']['title']; ?>AXIS CMS</title>
<?php if (AXS_LOCALHOST!==true) echo '<meta name="robots" content="noindex,nofollow" />'."\n"; ?>
<meta name="description" content="cms, sisuhaldus, sisuhaldussüsteem, intranet, andmebaasid, veebilahendused, kodulehed, kodulehtede valmistamine" />
<meta name="generator" content="AXIS CMS" />
<meta name="HandheldFriendly" content="True" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<link type="image/x-icon" rel="shortcut icon" href="<?php echo AXS_PATH_CMS_HTTP; ?>gfx/favicon.svg" />
<link type="text/css" rel="stylesheet" href="<?php echo axs_dir('lib', 'http'); ?>axs.css?v=<?php echo filemtime(axs_dir('lib').'axs.css'); ?>" media="screen" />
<link type="text/css" rel="stylesheet" href="<?php echo AXS_PATH_CMS_HTTP; ?>index.css?v=<?php echo filemtime(__DIR__.'/index.css'); ?>" title="AXIS CMS screen stylesheet" media="screen" />
<link type="text/css" rel="stylesheet" href="<?php echo AXS_PATH_CMS_HTTP; ?>index.print.css" media="print" />
<script src="?axs%5Bgw%5D=jquery.js"></script>
<script src="?axs%5Bgw%5D=axs.js"></script>
<script src="?axs%5Bgw%5D=axs.menu.js"></script>
<script src="<?php echo axs_dir('lib.js', 'http'); ?>axs.animationcontrol.js"></script>
<script src="<?php echo AXS_PATH_CMS_HTTP; ?>index.js"></script>
<?php
unset($axs['page']['head']['jquery.js']);
echo implode('', $axs['page']['head']);
if (AXS_LOGINCHK!==1) {
	if (($_SERVER['SERVER_NAME']===AXS_DOMAIN) && (!isset($_POST['axs_login'])) && (axs_get('e', $_GET)!=='download')) {
		$axs['editor']->log_visit=true;
		echo "<script>\n".
		"	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){\n".
		"		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\n".
		"		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\n".
		"		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');\n".
		"	ga('create', 'UA-8043349-1', 'auto');\n".
		"	ga('send', 'pageview');\n".
		"	</script>";
		}
	}
?>
</head>

<body id="axiscms">
<script>axs.mediaState();</script>
<?php
if (axs_function_ok('session_write_close')) session_write_close();
flush();
echo implode("\n", $axs['debug']).$axs['page']['header'].$axs['page']['breadcrumb'];
unset($axs['page']['header'], $axs['page']['menu'], $axs['page']['breadcrumb']);
?>
<main id="content" class="<?php echo implode(' ', $axs['page']['class']); ?>">
<?php
if ($axs['page']['content']) echo $axs['page']['content'];
else {
	$axs['editor']->load_editor($axs['include']);
	if ($axs['include_ext']) include($axs['include_ext']);
	}
?>
</main>

<?php
# footer section
if (!$axs['editor']->get) {
	$axs['editor']->tpl_set('index_footer',
	'<footer id="axs_cms_footer">'."\n".
	'	<small class="ver">AXIS CMS v{$cms_ver} <a href="https://{$AXS_DOMAIN}/{$user}" title="AXIS MULTIMEDIA Ltd." target="_blank">AXIS MULTIMEDIA 2005-2021</a></small>'."\n".
	'	<div class="benchmark">{$exec}</div>'."\n".
	'	<nav class="axs inpage top" lang="en"><a href="#" title="Go to top">Go to top</a></nav>'."\n".
	'	<script>axs.menu.inpageTopNavInit(document.querySelector("nav.inpage.top"));</script>'."\n".
	'</footer>'."\n\n");
	$axs['editor']->vars=array('cms_ver'=>AXS_CMS_VER, 'AXS_DOMAIN'=>AXS_DOMAIN, 'user'=>(AXS_LOGINCHK==1) ? '?u='.$axs_user['user']:'', 'exec'=>'', );
	if (AXS_LOGINCHK==1) { // End measure script execution time
		$axs['editor']->vars['exec']='exec:'.number_format((array_sum(explode(' ', microtime()))-AXS_START_TIME), 3).'s ';
		if (function_exists('memory_get_usage')) $axs['editor']->vars['exec'].='mem:'.number_format(memory_get_usage()/1024, 2, '.', ',').'K ';
		if (function_exists('memory_get_peak_usage')) $axs['editor']->vars['exec'].='peak:'.number_format(memory_get_peak_usage()/1024, 2, '.', ',').'K';
		//$axs['editor']->vars['exec'].='<a href="?'.htmlspecialchars($_SERVER['QUERY_STRING']).'&amp;templates_dump#templates_dump">dump templates</a>';
		}
	//else $axs['editor']->vars['exec']='<a href="http://validator.w3.org/check?uri=referer" target="_blank"><img src="'.AXS_PATH_CMS_HTTP.'gfx/w3c-valid-xhtml5.png" alt="Valid XHTML 5" /></a>';
	//'<a href="http://jigsaw.w3.org/css-validator/check/referer" target="_blank"><img src="'.AXS_PATH_CMS_HTTP.'gfx/w3c-vcss.gif" alt="Valid CSS!" /></a>';
	echo axs_tpl_parse($axs['editor']->templates['index_footer'], $axs['editor']->vars);
	}

# feedback system form
if (AXS_LOGINCHK==1) {
	foreach ($axs['msg'] as $k=>$v) if (is_array($v)) $axs['msg'][$k]=$axs['editor']->array_dump($v);
	if ($axs['e']!='feedback') echo '<form id="feedback_form" action="'.'?'.axs_url($axs['url'], array('e'=>'feedback', 'axs'=>array('section'=>'content'))).'" method="post" target="feedbackwin">'."\n".
	' <p><input name="feedback" type="hidden" value="'.htmlspecialchars(
		'Request: http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']."\n".
		'Msg: '.implode("\r\n", $axs['msg'])."\n".
		'Post: '."\n".
		$axs['editor']->array_dump($_POST)."\n".
		'Referer: '.axs_get('HTTP_REFERER', $_SERVER)
		).'" />'."\n".
	' <input id="feedback_button" name="popup" type="submit" value="'.$axs['editor']->tr('menu_feedback').'" /></p>'."\n".
	' <script>axs.feedback_form.init();</script>'."\n".
	'</form>'."\n";
	}
/*if (isset($_GET['dump_templates'])) {
	echo '<pre id="templates_dump">'."\n";
	foreach ($axs['templates_dump'] as $k=>$v) echo "\n".
		'------------------------------------------------------'."\n".
		'<b>'.$k.'.tpl:</b>'."\n".
		htmlspecialchars($v);
	echo '</pre>'."\n";
	}*/
?>
</body>
</html><?php
if (isset($axs['editor']->log_visit)) axs_log(__FILE__, __LINE__, 'visitcms', '', false);
#2005-03 ?>