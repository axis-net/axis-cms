<?php #2021-08-11
defined('AXS_PATH_CMS') or exit(require('index.php'));
if (is_array($axs['get']['gw'])) {
	$key=key($axs['get']['gw']);
	$val=$axs['get']['gw'][$key];
	}
else $key=$val=$axs['get']['gw'];
if (substr($key, -3)==='.js') header('Content-type: text/javascript');
if (substr($key, -4)==='.css') header('Content-type: text/css');
switch ($key) {
	case 'axs.js': axs_exit(
		file_get_contents(axs_dir('lib').'axs.js')."\n".
		'axs.conf({SITE_ROOT:"'.$axs['http_root'].'",PATH_CMS:"'.AXS_PATH_CMS_HTTP.'",logURL:"?axs%5Bgw%5D=js.log"});'
		);
	case 'axs.menu.js': axs_exit(file_get_contents(axs_dir('lib.js').'axs.menu.js'));
	case 'jquery.js': axs_exit(file_get_contents(axs_dir('lib.js').'jquery.js'));
	case 'axs.css': axs_exit(file_get_contents(axs_dir('lib').'axs.css'));
	case 'captcha': axs_exit(axs_form_captcha::img($_GET['axs_captcha_img']));
	case 'progress':
		header('Content-type: application/json');
		session_start();
		axs_exit(json_encode($_SESSION['axs']['progress'][$axs['get']['gw']['progress']]));
	case 'fup_progress': # For PHP 5.4 users
		error_reporting(0);
		if ((!$axs['session()']) or (!ini_get('session.upload_progress.enabled'))) axs_exit('-');
		session_start();
		$progress_key=ini_get('session.upload_progress.prefix').$axs['get']['upl_id']; # upload request ID
		$json=array();
		foreach ($_SESSION[$progress_key]['files'] as $k=>$v) $json[]='"'.$v['field_name'].'.'.$v['name'].'":'.$v['bytes_processed'];
		axs_exit('{'.implode(',', $json).'}');
	case 'server-time': axs_exit(($val==='date') ? date('c'):$axs['time']);
	case 'js.log':
		if (!empty($axs_user['user'])) axs_log(__FILE__, __LINE__, 'js.log', '', true, '1');
		break;
	default:
		$url=htmlspecialchars($axs['get']['gw']);
		$alt=axs_html_safe(axs_get('alt', $_GET));
		axs_exit('<!DOCTYPE html>'."\n".
		'<html id="popup_window">'."\n".
		'<head>'."\n".
		'<meta charset="'.$axs['cfg']['charset'].'" />'."\n".
		'<title>'.$alt.'</title>'."\n".
		'<style type="text/css">'."\n".
		'	<!--'."\n".
		'	html {'."\n".
		'		margin:0;'."\n".
		'		padding:0;'."\n".
		'		min-height:100.50%;'."\n".
		'		}'."\n".
		'	body {'."\n".
		'		margin:0;'."\n".
		'		padding:0;'."\n".
		'		}'."\n".
		'	figure {'."\n".
		//'		display:inline-block;'."\n".
		//'		vertical-align:middle;'."\n".
		'		margin:0;'."\n".
		'		border:none;'."\n".
		'		padding:0;'."\n".
		'		}'."\n".
		'	figure img {'."\n".
		'			margin:0;'."\n".
		'			padding:0;'."\n".
		'			display:block;'."\n".
		'			}'."\n".
		'	figure figcaption {	display:none;	}'."\n".
		'	-->'."\n".
		'</style>'."\n".
		'<script src="?axs%5Bgw%5D=axs.js"></script>'."\n".
		'<script>'."\n".
		'	<!--'."\n".
		'	window.onload=function() {'."\n".
		'		var img=document.querySelector("img");'."\n".
		'		var s=((img.width>2)&&(img.height>2)) ? {"iw":img.width,"ih":img.height}:{};'."\n".
		'		axs.window_resize(s);'."\n".
		'	}'."\n".
		'	//-->'."\n".
		'</script>'."\n".
		'</head>'."\n".
		'<body>'."\n".
		'	<figure class="popup"><img src="'.$url.'" alt="'.htmlspecialchars(basename($url)).'" /><figcaption title="'.$alt.'">'.$alt.'</figcaption></figure>'."\n".
		'<script>'."\n".
		'	var el=document.querySelector("#axs_overlay");'."\n".
		'	if (el){'."\n".
		'		el.querySelector("figure img").addEventListener("load",axs.overlay.setSize);'."\n".
		'		axs.class_add(el,"content-img");'."\n".
		'		}'."\n".
		'</script>'."\n".
		'</body>'."\n".
		'</html>');
	} #</switch ($axs['get']['gw'])>
#2007 ?>