<?php #2018-08-25
date_default_timezone_set('Europe/Tallinn');
$dirs=array(''=>'./');
$files=array();
$handle=opendir('.');
while (($f=readdir($handle))!==false) { #Read directory cycle
	if (is_dir($f) && $f[0]!='.') {
		$files[$f.'/']='<a href="?d='.urlencode($f.'/').'">'.$f.'/</a>';
		$dirs[$f.'/']=$f.'/';
		}
	}
closedir($handle);
$get_d=(!empty($_GET['d'])) ? $_GET['d']:'';
$d=(isset($dirs[$get_d])) ? $dirs[$get_d]:current($dirs);
if ($get_d) $files=array();
$title='Kujunduse kavandid';
$css=$favicon=$tpl='';
$tpl_back='<p>&nbsp;<a href="./">&lt;&lt;tagasi</a></p>'."\n";
$nr=(isset($_GET['nr'])) ? substr($_GET['nr'], 0, 2):'';
if ($nr) {
	if (file_exists($f='index.css')) $css.="\n".'<link href="'.$f.'" rel="stylesheet" type="text/css" />'."\n";
	if (($d) && (file_exists($f=$d.'index.css'))) $css.="\n".'<link href="'.$f.'" rel="stylesheet" type="text/css" />'."\n";
	if (file_exists($f=$d.$nr.'.css')) $css.="\n".'<link href="'.$f.'" rel="stylesheet" type="text/css" />'."\n";
	if (file_exists($f='favicon.png')) $favicon="\n".'<link href="'.$f.'" rel="shortcut icon" type="image/x-icon" />'."\n";
	if (file_exists($f=$d.'favicon.png')) $favicon="\n".'<link href="'.$f.'" rel="shortcut icon" type="image/x-icon" />'."\n";
	if (file_exists($f=$d.$nr.'.favicon.png')) $favicon="\n".'<link href="'.$f.'" rel="shortcut icon" type="image/x-icon" />'."\n";
	if (file_exists($f='index.tpl')) $tpl=implode('', file($f));
	elseif (file_exists($f=$d.'index.tpl')) $tpl=implode('', file($f));
	elseif (file_exists($f=$d.$nr.'.tpl')) $tpl=implode('', file($f));
	else $tpl=' <div><img src="{$dir}{$nr}.png" alt="{$dir}{$nr}.png" /></div>';
	$tpl=str_replace(array('{$dir}', '{$nr}'), array($d, $nr), $tpl);
	if ($d=='./') $d='';
	$title=$d.' '.$nr;
	}
else {
	$handle=opendir($d);
	if ($d=='./') $d='';
	while (($f=readdir($handle))!==false) { #Read directory cycle
		if (substr($f, 2, 5)=='.png') $files[$f]='<a href="?d='.urlencode($d).'&amp;nr='.$f[0].$f[1].'">'.$d.' '.$f[0].$f[1].'</a>';
		}
	closedir($handle);
	ksort($files);
	$tpl='<ul>';
	foreach ($files as $v) $tpl.="\n".'	<li>'.$v.'</li>';
	$tpl.="\n".'</ul>'."\n".
	'<ul>'."\n".
	'	<li><a href="?test=device-info">Seadme info</a></li>'."\n".
	'	<li><a href="?test=res">Ekraani suuruse test</a></li>'."\n".
	'   <li><a href="?test=help">Kavandite lisamise juhend</a></li>'."\n".
	'</ul>'."\n".
	$tpl_back;
	} 

if (isset($_GET['test'])) {
	$test=$_GET['test'];
	$title=htmlspecialchars($test);
	switch ($test) {
		case 'help':
			$tpl.='<div style="margin:0px 100px; 20px 100px;">
			<p>Lisa kavandite versioonid kujul 01.png, 02.png jne (soovitavalt 24bit png). </p>
			<p>Kui soovid pilti joondada keskele v&otilde;i paremale, seada tausta vmt, siis tee selleks fail index.css. Kui m&otilde;ni kavand vajab teistest erinevat paigutust, tausta vmt, tee sellele t&auml;iendav css fail kujul nr.css (nt 01.css, 02.css jne). </p>
			<p>Soovi korral v&otilde;ib kavandeid r&uuml;hmitada ka eraldi kaustadesse. <br />
			<strong>NB!</strong> Kaustade nimed peavad vastama URLide ja failinimede moodustamise standardile, st ei tohi sisaldada t&uuml;hikuid ega rahvuslikke s&uuml;mboleid (&uuml;, &otilde;, &ouml;, &auml; jmt). </p>
			</div>';
			break;
		case 'device-info':
			$tpl='     <dl>'."\n".
			'      <dt>software info</dt><dd>'.htmlspecialchars($_SERVER['HTTP_USER_AGENT']).'</dd>'."\n".
			//'<script src="../../axiscms/?axs[gw]=fn.api.js"></script>'."\n".
			//'<script src="../../axiscms/?axs[gw]=fn.api.js"></script>'."\n".
			'<script>'."\n".
			'<!--'."\n".
			'var win=window_width_get();'."\n".
			'document.write('."\n".
			'	"      <dt>screen size</dt><dd>"+screen.width+"&times;"+screen.height+"px</dd>"+'."\n".
			'	"      <dt>window size</dt><dd>"+win.w+"&times;"+win.h+"px</dd>"+'."\n".
			'	"      <dt>pixel ratio</dt><dd>"+win.pxr+"</dd>"'."\n".
			'	);'."\n".
			'//-->'."\n".
			'</script>'."\n".
			'<noscript><dt>JavaScript</dt><dd>JavaScript is disabled</dd></noscript>'."\n".
			'     </dl>'."\n".
			$tpl_back;
			break;
		default:
			$reso=array(
				'400-400'=>array('w'=>400, 'h'=>400),
				'600-400'=>array('w'=>600, 'h'=>400),
				'640-480'=>array('w'=>640, 'h'=>480),
				'800-600'=>array('w'=>800, 'h'=>600),
				'1024-768'=>array('w'=>1024, 'h'=>768),
				);
			$res=(!empty($_GET['res'])) ? $_GET['res']:key($reso);
			$r=explode('-', $res);
			$r=array('w'=>intval($r[0]), 'h'=>intval($r[1]));
			$tpl='     <ul style="margin:0; border:solid 1px #000; padding:0; width:'.$r['w'].'px; height:'.$r['h'].'px;">'."\n";
			foreach ($reso as $k=>$v) {
				if ($k===$res) {	$st='<strong>';	$et='</strong>';	}
				else $st=$et='';
				$tpl.='      <li>&nbsp;<a href="?test=res&amp;res='.$k.'">'.$st.str_replace('-', '&times;', $k).$et.'</a> <script>document.write(\'<a href="#" onclick="return axs_resize('.$v['w'].','.$v['h'].');">resize();</a>\');</script></li>'."\n";
				}
			$tpl.=
			'      <li>&nbsp;'.$tpl_back.'</li>'."\n".
			'     </ul>';
		break;
		}
	}
?><!DOCTYPE html>
<html id="nr<?php echo $nr; ?>">
<head>
<meta charset="utf-8" />
<title><?php print $title; ?></title>
<?php print $favicon; ?>
<script>
<!--
function axs_resize(w,h) {
	window.resizeTo(w,h);
	window.moveTo(0, 0);
	return false;
	}
function window_width_get() {
	var winW = 0, winH = 0;
	if (document.body && document.body.offsetWidth) {
		winW = document.body.offsetWidth;
		winH = document.body.offsetHeight;
		}
	if (document.compatMode=='CSS1Compat' && document.documentElement && document.documentElement.offsetWidth) {
		winW = document.documentElement.offsetWidth;
		winH = document.documentElement.offsetHeight;
		}
	if (window.innerWidth && window.innerHeight) {
		winW = window.innerWidth;
		winH = window.innerHeight;
		}
	winPxR=(window.devicePixelRatio)?window.devicePixelRatio:'?';
	return {	'w':winW,	'h':winH,	'pxr':winPxR	};
	}
// -->
</script>
<style type="text/css">
<!--
html {
	height:100.20%;
	}
body {
	margin:0;
	padding:0;
	}
dl {	margin:50px;	}
	dl dt {	font-weight:bold;	}
-->
</style>
<?php echo $css; ?>
</head>
<body>
<?php echo $tpl; ?> 
</body>
</html>
<?php #2008-04-01 ?>