<?php #updated adm@20.06.2014 18:45:13
return array(
	'data.lbl'=>'kasutaja andmed',
	'user_login.lbl'=>'logi sisse',
	'user.lbl'=>'e-post',
	'pass.lbl'=>'parool',
	'user_remember.lbl'=>'pea mind meeles',
	'pass_send.lbl'=>'Unustasid parooli? Saada parool',
	'submit.lbl'=>'edasi',
	'logout.lbl'=>'logi välja',
	'msg_failed'=>'Sisselogimine ebaõnnestus!',
	'msg_denied'=>'Ligipääs piiratud!',
	'msg_pass_send_fail'=>'Sellist kasutajat ei leitud.',
	'msg_pass_send_ok'=>'Parool saadetud kasutaja e-posti aadressile.',
	'user_edit.lbl'=>'minu profiil',
	'fstname.lbl'=>'eesnimi',
	'lstname.lbl'=>'perenimi',
	'email.lbl'=>'e-post 2',
	'phone.lbl'=>'telefon',
	'user_data.lbl'=>'teie kasutajakonto andmed',
	'user_save.lbl'=>'salvesta',
	
	'msg.user_invalid'=>'Kasutaja/e-post ei ole korrektne!',
	'msg.user_exists'=>'Selline Kasutaja/e-post on juba registreeritud! Palun kasutage login vormil olevat linki "Unustasid parooli?"',
	'msg.pass_invalid'=>'Parool sisaldab keelatud sümboleid!',
	'msg.pass_short'=>'Parooli pikkus peab olema vähemalt {$min} tähemärki!',
	'msg.pass_repeat'=>'Paroolid ei klapi!',
	'msg.email'=>'E-post 2 sisaldab keelatud sümboleid (mitme aadressi puhul kasutage eraldajana koma)!',
	
	'msg_value_required'=>'Välja täitmine on kohustuslik.',
	'msg_value_invalid'=>'Sisestatud väärtus ei ole sobiv.',
	'msg_value_unique'=>'Väärtus peab olema kordumatu.',
	);
?>