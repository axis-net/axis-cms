<?php #updated adm@11.01.2014 20:28:40
return array(
	''=>array(
		'mailto'=>'',
		'mail_name_field'=>'name',
		'posts_save'=>'',
		'posts_order'=>'desc',
		'tr'=>array(
			'msg'=>'Teade saadetud! Teate kättesaamise kinnituseks saadetakse koopia Teie e-posti aadressile.',
			'msg_value_required'=>'välja täitmine on kohustuslik!',
			'msg_value_invalid'=>'sisestatud väärtus ei ole sobiv!',
			'msg_value_unique'=>'väärtus peab olema kordumatu!',
			), 
		'locked'=>false,
		), 
	'name'=>array(
		'type'=>'text',
		'label'=>'nimi',
		'value'=>'',
		'size'=>50,
		'required'=>1,
		'placeholder'=>'',
		'comment'=>'',
		'msg'=>'',
		'txt'=>'',
		), 
	'email'=>array(
		'type'=>'email',
		'label'=>'e-post',
		'size'=>50,
		'required'=>1,
		), 
	'message'=>array(
		'type'=>'textarea',
		'label'=>'teade',
		'required'=>1,
		'size'=>40,
		'rows'=>10,
		), 
	'post'=>array(
		'type'=>'submit',
		'label'=>'saada',
		), 
	);
?>